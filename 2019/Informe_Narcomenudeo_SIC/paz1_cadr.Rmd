---
title: Causas Resueltas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## 1a. Cat - Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestran las resoluciones dictadas por el organismo, excluyéndose aquellas dictadas en materia de **violencia** que aparecen en la próxima tabla. Esta actualización del indicador, desagregándo las resoluciones emitidas en materia de violencia -conforme resolución del STJER-, se efectúa a partir del 01/06/19 por lo que las cantidades informadas en boletines anteriores para los meses de febrero-abril pueden variar.  

```{r}
resoluciones_paz(
  poblacion = jdos_paz,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Resoluciones y Vencimientos") %>%
    row_spec(0, angle = 90)  %>%
  landscape()

```

## Movimientos y Resoluciones en materia de Violencia `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan movimientos en materia de Violencia informadas por el REJUCAV dependiente del Centro Judicial de Género (conforme disposición de Presidencia de fecha 22/10/18). Según la información provista por el registro en la tabla se cuentan las *nuevas denuncias, medidas y resoluciones/sentencias* informadas por cada juzgado.


```{r}
movimientos_violencias(jdos_paz, start_date, end_date) %>%
  filter(str_detect(juzgado, "Primera|Ramirez (Segunda)")) %>%
  janitor::adorn_totals("row") %>%
  outputTable(caption = "Movimientos en materia de Violencia") %>%
  landscape()

```

