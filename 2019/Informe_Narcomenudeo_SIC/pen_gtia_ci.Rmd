---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}

inic_gti_mes <- function(start_d, end_d) {
  
  resultado <- iniciadas_pen(poblacion = oga, start_date = start_d, end_date = end_d) %>% 
    mutate(fecha = start_d, mes = lubridate::month(start_d, label = T, abbr = T), 
           narcom = str_detect(tipo_proceso, "ESTUPEFACIENT|10566|10.566|NARCOMENUD"), 
           narcom_tipo = word(tipo_proceso, 1)) %>% 
    select(circunscripcion, organismo, fecha, mes, narcom, narcom_tipo, everything(), -durac_hec_inic) %>% 
    filter(circunscripcion != "Total")
    
   resultado 
}

feb <- inic_gti_mes("2019-02-01", "2019-03-01")
mar <- inic_gti_mes("2019-03-01", "2019-04-01")
abr <- inic_gti_mes("2019-04-01", "2019-05-01")
may <- inic_gti_mes("2019-05-01", "2019-06-01")
jun <- inic_gti_mes("2019-06-01", "2019-07-01")

narcom_1s <- bind_rows(feb, mar, abr, may, jun)


```


## Causas Iniciadas Total- `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciadas_pen(poblacion = oga,
              start_date = start_date,
              end_date = end_date) %>% 
  filter(circunscripcion != "Total") %>% 
  select(-durac_hec_inic) %>% 
  group_by(circunscripcion, organismo) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  janitor::adorn_totals("row") %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
  
```

\pagebreak


<!-- ## Causas Iniciadas por Tipo de Proceso - `r getDataIntervalStr(start_date, end_date)`    -->

<!-- En esta tabla usted puede ver las causas o legajos iniciados en Garantía según el informe suministrado por cada organismo.     -->

<!-- Las causas se agrupan por tipo de proceso, agregándose el total de casos por cada tipo.    -->

<!-- Además, se presenta la duración promedio entre el hecho que origina el legajo y el inicio del mismo en OGA. Para los tipo de proceso con un solo caso iniciado el dato de duración no constituye un promedio, sino su valor inicial. En los legajos donde no está registrada la fecha del hecho no se dispone de esta medición.     -->

<!-- **OBSERVACION:** Data los distintos criterios de registración sobre los tipos de proceso/delito en las distitnas circunscripciones se proponen las siguientes pautas prácticas de registración para homogeneizar este dato tan importante:    -->

<!-- + *DENUNCIA/SU DENUNCIA*: evitar el uso de este descriptor. Emplear solo provisoriamente hasta tanto se disponga de un tipo de proceso conforme CPP.    -->
<!-- + Registrar todos los delitos del legajo. En el campo principal de *Tipo Proceso* (caratula Lex-Doctor) registrar el delito de mayor pena y en el campo complementario *Otros tipos de proceso* registrar todos los demás delitos.    -->

<!-- ```{r} -->
<!-- inic_gtia %>% -->
<!--   kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                  full_width = F, font_size = 10)  %>% -->
<!--   # outputTable(caption = "Causas Iniciadas") %>% -->
<!--   row_spec(0, angle = 90) %>%  -->
<!--   landscape() -->

<!-- ``` -->

## Causas Iniciadas Generales y sobre Narcomenudeo - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede ver por Circunscripción Judicial y Mes la cantidad de causas iniciadas vinculadas a la Ley de Narcomenudeo 10566 y los demás tipos de proceso que se registran en instancia de Garantías por las Oficinas de Gestión de Audiencias provinciales.

```{r}
narcom_1s %>%
  mutate(narcom = ifelse(narcom, "narcomenudeo", "otros")) %>% 
  group_by(circunscripcion, organismo, mes, narcom) %>% 
  summarise(causas_iniciadas_garantia = sum(cantidad, na.rm = T)) %>% 
  spread(narcom, causas_iniciadas_garantia) %>% 
  mutate(narcomenudeo = ifelse(is.na(narcomenudeo), 0, narcomenudeo)) %>% 
  group_by(circunscripcion, organismo) %>% 
  do(janitor::adorn_totals(.)) %>% 
  janitor::adorn_totals("col") %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
  
```

\pagebreak


## Causas sobre Narcomenudeo por Tipo- `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar los tipos de causa asociadas a narcomenudeo.


```{r}
narcom_1s %>%
  filter(narcom) %>% 
  group_by(circunscripcion, narcom_tipo) %>% 
  summarise(causas_iniciadas_garantia = sum(cantidad, na.rm = T)) %>% 
  mutate(narcom_tipo = ifelse(narcom_tipo == "ACTUACIONES", "otros", narcom_tipo )) %>%
  spread(narcom_tipo, causas_iniciadas_garantia, fill = 0) %>% 
  select(circunscripcion, COMERCIO, TENENCIA, SUMINISTRO, otros) %>% 
  janitor::adorn_totals("col") %>% 
  janitor::adorn_totals("row") %>% 
  arrange(desc(Total)) %>% 
  kable(caption = "Causas sobre Narcomenudeo por Tipo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
  
```

\pagebreak


## Causas Iniciadas sobre Narcomenudeo por Tipo- `r getDataIntervalStr(start_date, end_date)`


```{r}
narcom_1s %>%
  filter(narcom) %>% 
  group_by(mes) %>% 
  summarise(causas_iniciadas_garantia = sum(cantidad, na.rm = T)) %>% 
  ggplot(aes(x = mes, y = causas_iniciadas_garantia)) + 
  geom_bar(stat="identity") +
  geom_text(aes(label=causas_iniciadas_garantia), vjust = -0.5) +
  theme_minimal() +
  labs(x = " ", y = "Iniciados") +
  theme(axis.text.x = element_text(hjust = 2, size = 12), 
        strip.text = element_text(size = 18)) +
  labs(title = "Causas Iniciadas Narcomenudeo", x = "",
       caption = "APGE-STJER")
  
```

\pagebreak



## Declaración Metodológica

### Aspectos Específicos del Informe sobre Narcomenudeo

La información procesada para este informe surge de declaraciones de datos digitales emitidas por los sistemas judiciales y presentadas por parte de cada responsable o director de OGA. 

No están incluidos en este informe todas aquellas actuaciones asociadas a narcomenudeo que ingresaron al Ministerio Público Fiscal y no tuvieron ingreso judicial en instancia de Garantías. 

En la tabla 2. los datos sobre meses faltantes en circunscripciones judiciales se debe a falta de la presentación de información primaria correspondiente.

La identificación de los casos bajo estudio (ie vinculados al delito de narcomenudeo -cfrme Ley 10566-) se realizó a partir del campo "tipo de proceso" (a través de expresiones regulares) registrado en OGA al inicio de todo legajo penal. 
En tal caso, no podemos dejar de señalar que existe la posibilidad de fallas de registración que estén afectando (subestimando) el indicador. Sin perjuicio de ello la relación que, en principio, guardan los resultados entre sí y con otras variables (ej. población y otros delitos) abonaría su consistencia.    


### Aspectos Generales de la Estadística Pública Judicial STJER

Las Nuevas Estadísticas del Poder Judicial de Entre Ríos surgieron en el 2018 como resultado del Plan de Modernización Estadística (resolución TS 277/16, concluido el 21/09/18) y están sujetas al Reglamento de Estadística vigente –cfr. Res. Tribunal Superintendencia Nº234/17, http://www.jusentrerios.gov.ar/estadistica/reglamento-de-estadistica-19-09-17/.  Este nuevo modelo estadístico está integrado por un conjunto de operaciones diseñadas e implementadas por el Área de Planificación Gestión y Estadística y constituye un sistema coherente de definiciones sustantivas, metodologías, procedimientos y herramientas dirigido a la producción de información de calidad.    

Para el mantenimiento y actualización del sistema se realizan consultas a magistrados y funcionarios –autorizadas por las Salas del STJER- y se celebran encuentros de trabajo donde se recogen ideas y definiciones relevantes para la actualización y construcción de indicadores judiciales. Al mismo tiempo, se trabaja en la revisión de indicadores comparados con distintos Poderes Judiciales provinciales y documentos técnicos sobre estadística judicial. Todo esto tiene como resultado un set de indicadores aprobados institucionalmente y en permanente actualización. 

Los procedimientos empleados en el relevamiento de datos usted los puede consultar (por órgano, instancia y fuero) en las Guías de Estadística accesibles a través de la Página Web del Superior Tribunal de Justicia, en la sección del Área de Planificación Gestión y Estadística, accediendo a la opción *RECURSOS*, y luego *GUÍAS SOBRE ESTADÍSTICA JUDICIAL (NUEVO)*. O bien accediendo directamente a la dirección: https://drive.google.com/drive/folders/1HjJ8E82mcIVIck4H0giFWwTmOVLbK2NZ eligiendo la guía correspondiente. A través de los siguientes enlaces puede acceder a dichas guías directamente:

+ [Juzgados Civil y Comercial, y Paz 1ªCat.](https://drive.google.com/open?id=1EOJggtPkUTgHQRuQesBuzmEZMoYe2UO3juRUVjiVFNw)
+ [Juzgados de Familia](https://drive.google.com/open?id=1nW02icHcRpLz8w_5X9a_uEYCsyQ6Nzf_dNzsNljpxXU)
+ [Juzgados Laborales](https://drive.google.com/open?id=1I051W6JH2lQ0kH3QH_5fW0xXYhkLGehoJpCLtmdOBuc)
+ [Juzgados de Paz 2ª y 3ª Cat.](https://drive.google.com/open?id=1uLoT22Kr0SmnNCQD2J3LbapuJQUs3vBvjYX4fno_I5A)
+ [Camaras Civiles](https://drive.google.com/open?id=1OhqQbi1dRkAyISQuC5pkNtEKe467-fB1icK1AQrh2bE)
+ [Camaras Laborales](https://drive.google.com/open?id=1uJNgds2fREkteS7c5OVGypRD5J9nvjwjsm2RGBJh8e4)
+ [Camaras Contencioso Administrativas](https://drive.google.com/open?id=1EJmsBq1Wzg5sLKXmgS3VXak2duu4Q75vydyVZergmtQ)
+ [Camaras de Casación Penal](https://drive.google.com/open?id=1V6pz_sW2v-vbyJxufaoMtgqUiiJ-6REoB97BNnlMYFA)
+ [Oficinas de Gestión de Audiencia Penales](https://drive.google.com/open?id=1mRQbflTQeORMGGUWOhz8Q6i0zCnJox5HL67PVlTPGu4)
+ [Superior Tribunal de Justicia - Contencioso Administrativo](https://drive.google.com/open?id=1Ic4eI79cWCAgckpjjU_N3kqCZKJ8biFoYkVOJe8mzOQ)
+ [Sala I de Procedimientos Constitucionales y Penal del STJER](https://drive.google.com/open?id=1ZislKECWEEXkLAbyEmgCWNiYGzBhKqB59b1LUZSR6kM)
+ [Sala II Civil y Comercial STJER](https://drive.google.com/open?id=1Qcptj4TrXvfcRYcZ9iSuI3O4sIefcJ2dsruttqGAdtA)
+ [Sala III del Trabajo STJER](https://drive.google.com/open?id=1SX8JuqWFR0xXOK3Y2R6HqcOwN_sLtsgNoI5ZQODP8yA)

Las rutinas de procesamiento estadístico empleadas en el cálculo de indicadores usted las puede consultar seleccionando la operación correspondiente del repositorio accesible en el siguiente [link](https://bitbucket.org/apgye/apgyeoperationsjuser/src/master/R/).   

## Calidad Acreditada de la Información Estadística  

La calidad de la información elaborada por el APGE se acredita a través una estructura de operaciones públicas, que contempla distintos niveles de validación y revisión de datos por parte de nuestros organos informantes.    

Dichas validaciones y revisiones se cumplen antes, durante y después de la producción de indicadores calculados:    

+ **antes**: la información requerida a los órganos judiciales surge de guías de estadística y su conformidad con ellas se controla por cada organismo judicial -cfrme.Reglamento de Estadística. En el proceso de entrega de información judicial el APGE implementa validaciones de consistencia en datos críticos (ej, validación de codificación apropiadas, validación de no-declaración de fechas a futuro, entre otras) y ante la detección de no-conformidades comunica al organismo las mismas para su reprocesamiento,    
+ **durante**: al momento de efectuar una presentación de datos cada órgano informante recibe un **pre-informe** con los resultados de sus indicadores al que deben controlar antes de la confirmación de entrega de información, y   
+ **después**: nuestros informantes reciben mensualmente (los días 13 -órganos no penales- y 18 -órganos penales-) un *Informe para Revisión* con los indicadores calculados del mes para efectuar las correcciones necesarias ante la detección de errores o inconsistencias en las declaraciones de datos.     

El APGE está sujeto a una normativa estricta en materia estadística: Reglamento de Estadística (Res. T.S. Nº234/17), art.20 de la LOPJ, principio de acceso a información pública (Art. 13 de la Constitución Provincial y 1º, 33, 41, 42 y 72 inc. 22 de la Constitución Nacional), Ley Nacional 25326 de Protección de Datos Personales, entre otras.    

Además, sigue los lineamientos del “CÓDIGO DE BUENAS PRÁCTICAS DE LAS ESTADÍSTICAS JUDICIALES de las Provincias Argentinas y de la Ciudad Autónoma de Buenos Aires” aprobado por JUFEJUS, que propone los siguientes principios orientadores para los órganos de estadística: la independencia profesional, el trato imparcial a todos nuestros usuarios, la objetividad, la fiabilidad, la confidencialidad estadística y la relación costo-eficacia. 

