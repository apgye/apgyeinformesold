---
title: "Superior Tribunal de Justicia"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      toc: TRUE
      toc_depth: 3
      number_sections: TRUE
      includes:
        in_header: header.tex
        before_body: before_body.tex
geometry: margin=1.2cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
header-includes: \renewcommand{\contentsname}{Contenido del Documento}
params:
  circunscripcion:
    label: "Circunscripción (separar con coma sin espacio o 'Todas')"
    input: text
    value: "Todas"
  ieps: 
    label: "Organismo (sd = sin discriminar)"
    input: text
    value: "sd"
  fuero:
    label: "Fuero:Civil,Familia,Paz,Laboral,Penal,CAdm,ECQ"
    input: text
    value: "Civil"
  instancia1: FALSE
  instancia2: FALSE
  instancia3: FALSE
  start_date: 
    label: "Fecha_inicio_inf"
    input: text
    value: "2018-02-01"
  end_date:
    label: "Fecha_fin_inf"
    input: text
    value: "2018-12-01" 
  desagregacion_mensual: TRUE
  CausasIniciadas: FALSE 
  CausasResueltas: FALSE
  CausasEnTramite: FALSE
  CausasEnTramite_detalle: FALSE
  CausasyMovimientos: FALSE
  SentenciasTipo: FALSE
  Audiencias: FALSE
  Audiencias_familia_detalle: FALSE
  Audiencias_laboral_detalle: FALSE
  Duracion_Procesos_xorg: FALSE
  Duracion_Procesos_interanual: FALSE
  CausasArchivadas: FALSE 
  Personal: FALSE
  Primarias: FALSE
  Consideraciones_Finales: FALSE
  metodologia: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
knitr::opts_chunk$set(fig.width=12, fig.height=10) 
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
source("codconver.R")
```


```{r echo=FALSE, include=FALSE}
# parámetros del informe

if (params$circunscripcion == "Todas"){
  circ <- NA
} else {
  circ <- unlist(str_split(params$circunscripcion, ","))
}

if (params$ieps == "sd"){
  ieps <- NA
} else {
  ieps <- unlist(str_split(params$ieps, ","))
}

fuero <- unlist(str_split(params$fuero, ",")) 
instancia1 <- params$instancia1
instancia2 <- params$instancia2
instancia3 <- params$instancia3
start_date <- params$start_date
end_date <- params$end_date
desagregacion_mensual <- params$desagregacion_mensual
ci <- params$CausasIniciadas
ca <- params$CausasArchivadas
cr <- params$CausasResueltas
ce <- params$CausasEnTramite
ced <- params$CausasEnTramite_detalle
cm <- params$CausasyMovimientos
s <- params$SentenciasTipo
a <- params$Audiencias
afd <- params$Audiencias_familia_detalle
ald <- params$Audiencias_laboral_detalle
d <- params$Duracion_Procesos_xorg
di <- params$Duracion_Procesos_interanual
p <- params$Personal
cf <- params$Consideraciones_Finales
metodologia <- params$metodologia
prim <- params$Primarias
```


```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam", "sal")) 

if (exists("circ")) {
  if(is.na(circ)) {
    poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>% 
    filter(circunscripcion %in% circ)
  }
}

if (exists("ieps")) {
    if(is.na(ieps)) {
    poblacion_total <- poblacion_total
    } else {
    poblacion_total <- poblacion_total %>%
    filter(organismo %in% ieps)
    poblacion_total
    }
}


# Sub-poblaciones
jdos_cco <- poblacion_total %>% 
  filter(grepl("jdocco", organismo)) %>% 
  filter(str_detect(materia, "cco"), categoria == "1" | is.na(categoria)) 

if (any(str_detect(jdos_cco$materia, "lab|fam"))) { 
  multifueros <- TRUE 
  } else {
  multifueros <- FALSE
  }

jdos_ecq <- poblacion_total %>% 
  filter(grepl("jdocco", organismo)) %>% 
  filter(str_detect(materia, "eje|cqb"), categoria == "1" | is.na(categoria))

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria)) 

jdos_paz <- poblacion_total %>% 
  filter(str_detect(materia, "paz"), 
         categoria == "1" | (categoria == "3" & organismo == "turvol0100pna"))

jdos_paz_23 <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria != "1" | is.na(categoria))

if (nrow(jdos_paz_23) != 0) { 
  paz_23 <- TRUE 
  } else {
  paz_23 <- FALSE
  }

jdos_lab <- poblacion_total %>% 
  filter(str_detect(organismo, "jdolab"), tipo != "cam")

cam_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "cam")

cam_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "cam")

cam_cad <- poblacion_total %>% 
  filter(str_detect(materia, "cad"), tipo == "cam") 

# pendientes Salas y otros

```

\pagebreak

`r if (instancia1 & "Civil" %in% fuero) '# Juzgados Civil y Comercial'`


```{r cco_cadur_interanual, child = 'cco_cadur_interanual.Rmd', eval= instancia1 & "Civil" %in% fuero & di}
```

\pagebreak

```{r cco1_cadur, child = 'cco1_cadur.Rmd', eval= instancia1 & "Civil" %in% fuero & d}
```

\pagebreak

```{r cco1_cetal, child = 'cco1_cetal.Rmd', eval= instancia1 & "Civil" %in% fuero & ce}
```

\pagebreak

```{r cco1_cetal_detalle, child = 'cco1_cetal_detalle.Rmd', eval= instancia1 & "Civil" %in% fuero & ced}
```


\pagebreak

```{r cco1_cixgpo, child = 'cco1_cixgpo.Rmd', eval= instancia1 & "Civil" %in% fuero & ci}
```

\pagebreak

```{r cco1_cixgpo_graf, child = 'cco1_cixgpo_graf.Rmd', eval= instancia1 & "Civil" %in% fuero & ci}
```

\pagebreak

```{r cco1_cixgpo_multifueros, child = 'cco1_cixgpo_multifueros.Rmd', eval = multifueros & "Civil" %in% fuero & ci}
```

\pagebreak

```{r cco1_cadr, child = 'cco1_cadr.Rmd', eval= instancia1 & "Civil" %in% fuero  & cr}
```

\pagebreak

```{r cco1_sentxtres, child = 'cco1_sentxtres.Rmd', eval= instancia1 & "Civil" %in% fuero & s}
```

 \pagebreak

```{r cco1_audic, child = 'cco1_audic.Rmd', eval= instancia1 & "Civil" %in% fuero & a}
```

\pagebreak

```{r cco1_camov, child = 'cco1_camov.Rmd', eval= instancia1 & "Civil" %in% fuero & cm}
```

\pagebreak
 
```{r cco_carch, child = 'cco_carch.Rmd', eval= instancia1 & "Civil" %in% fuero & ca}
```


<!-- ###### -->

`r if (instancia1 & "ECQ" %in% fuero) '# Juzgados Ejecuciones y Concursos-Quiebras'`

\pagebreak

```{r ecq1_cetal, child = 'ecq1_cetal.Rmd', eval= instancia1 & "ECQ" %in% fuero & ce}
```

\pagebreak

```{r ecq1_cixgpo, child = 'ecq1_cixgpo.Rmd', eval= instancia1 & "ECQ" %in% fuero & ci}
```

\pagebreak

```{r ecq1_cixgpo_graf, child = 'ecq1_cixgpo_graf.Rmd', eval= instancia1 & "ECQ" %in% fuero & ci}
```

\pagebreak

```{r ecq1_cadr, child = 'ecq1_cadr.Rmd', eval= instancia1 & "ECQ" %in% fuero  & cr}
```

\pagebreak

```{r ecq1_sentxtres, child = 'ecq1_sentxtres.Rmd', eval= instancia1 & "ECQ" %in% fuero & s}
```

\pagebreak

```{r ecq1_audic, child = 'ecq1_audic.Rmd', eval= instancia1 & "ECQ" %in% fuero & a}
```

\pagebreak

```{r ecq1_camov, child = 'ecq1_camov.Rmd', eval= instancia1 & "ECQ" %in% fuero & cm}
```

\pagebreak
 
```{r ecq_carch, child = 'ecq_carch.Rmd', eval= instancia1 & "ECQ" %in% fuero & ca}
```


\pagebreak
 
`r if (instancia1 & "Familia" %in% fuero) '# Juzgados de Familia'`
 
 
```{r fam_cadur_interanual, child = 'fam_cadur_interanual.Rmd', eval= instancia1 & "Familia" %in% fuero & di}
```

\pagebreak

```{r fam_cadur, child = 'fam_cadur.Rmd', eval= instancia1 & "Familia" %in% fuero & d}
```


\pagebreak

```{r fam1_cetal, child = 'fam1_cetal.Rmd', eval= instancia1 & "Familia" %in% fuero & ce }
```

\pagebreak

```{r fam1_cixgpo, child = 'fam1_cixgpo.Rmd', eval= instancia1 & "Familia" %in% fuero & ci }
```

\pagebreak

```{r fam1_cixgpo_graf, child = 'fam1_cixgpo_graf.Rmd', eval= instancia1 & "Familia" %in% fuero & ci }
```

\pagebreak

```{r fam1_cadr, child = 'fam1_cadr.Rmd', eval= instancia1 & "Familia" %in% fuero  & cr}
```

<!-- \pagebreak -->

<!-- ```{r fam1_sentxtres, child = 'fam1_sentxtres.Rmd', eval= instancia1 & "Familia" %in% fuero & s } -->
<!-- ``` -->

\pagebreak

```{r fam1_audif, child = 'fam1_audif.Rmd', eval= instancia1 & "Familia" %in% fuero & a}
```

\pagebreak

```{r fam1_audif_detalle, child = 'fam1_audif_detalle.Rmd', eval= instancia1 & "Familia" %in% fuero & afd}
```

\pagebreak

```{r fam1_camov, child = 'fam1_camov.Rmd', eval= instancia1 & "Familia" %in% fuero & cm}
```

\pagebreak

 
```{r fam_carch, child = 'fam_carch.Rmd', eval= instancia1 & "Familia" %in% fuero & ca}
```

\pagebreak

```{r fam1_cadr_prim, child = 'fam1_cadr_prim.Rmd', eval= instancia1 & "Familia" %in% fuero  & prim}
```


\pagebreak

`r if (instancia1 & "Paz" %in% fuero) '# Juzgados Paz 1 Categoría'`


```{r paz1_cetal, child = 'paz1_cetal.Rmd', eval= instancia1 & "Paz" %in% fuero & ce}
```

\pagebreak

```{r paz1_cixgpo, child = 'paz1_cixgpo.Rmd', eval= instancia1 & "Paz" %in% fuero & ci}
```

\pagebreak

```{r paz1_cadr, child = 'paz1_cadr.Rmd', eval= instancia1 & "Paz" %in% fuero  & cr}
```

\pagebreak

```{r paz1_camov, child = 'paz1_camov.Rmd', eval= instancia1 & "Paz" %in% fuero & cm}
```

\pagebreak

```{r paz_carch, child = 'paz_carch.Rmd', eval= instancia1 & "Paz" %in% fuero & ca}
```

\pagebreak


`r if (instancia1 & "Paz" %in% fuero & paz_23 & ci & cr) '# Juzgados Paz 2a. y 3a. Categoría'` 


```{r paz23_ci, child = 'paz23_ci.Rmd', eval= instancia1 & "Paz" %in% fuero & ci & paz_23}
```

\pagebreak

```{r paz23_cadr, child = 'paz23_cadr.Rmd', eval= instancia1 & "Paz" %in% fuero & cr & paz_23 }
```

\pagebreak

`r if (instancia1 & "Laboral" %in% fuero) '# Juzgados Laborales'`


```{r lab_cadur_interanual, child = 'lab_cadur_interanual.Rmd', eval= instancia1 & "Laboral" %in% fuero & di}
```

\pagebreak

```{r lab1_cetal, child = 'lab1_cetal.Rmd', eval= instancia1 & "Laboral" %in% fuero & ce}
```

\pagebreak

```{r lab1_cixgpo, child = 'lab1_cixgpo.Rmd', eval= instancia1 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab1_cixgpo_graf, child = 'lab1_cixgpo_graf.Rmd', eval= instancia1 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab1_cadr, child = 'lab1_cadr.Rmd', eval= instancia1 & "Laboral" %in% fuero & cr}
```

\pagebreak

```{r lab1_sentxtres, child = 'lab1_sentxtres.Rmd', eval= instancia1 & "Laboral" %in% fuero & s}
```

 \pagebreak

```{r lab1_audil, child = 'lab1_audil.Rmd', eval= instancia1 & "Laboral" %in% fuero & a}
```

 
\pagebreak

```{r lab1_audil_detalle, child = 'lab1_audil_detalle.Rmd', eval= instancia1 & "Laboral" %in% fuero & ald}
```

\pagebreak

```{r lab1_camov, child = 'lab1_camov.Rmd', eval= instancia1 & "Laboral" %in% fuero & cm}
```

\pagebreak

```{r lab_carch, child = 'lab_carch.Rmd', eval= instancia1 & "Laboral" %in% fuero & ca}
```

\pagebreak
 
`r if (instancia2 & "Civil" %in% fuero) '# Segunda Instancia Civil y Comercial'`
 

```{r cco2_ci, child = 'cco2_ci.Rmd', eval= instancia2 & "Civil" %in% fuero & ci }
```

\pagebreak

```{r cco2_cadr, child = 'cco2_cadr.Rmd', eval= instancia2 & "Civil" %in% fuero  & cr}
```

\pagebreak

```{r cco2_cadr_prim, child = 'cco2_cadr_prim.Rmd', eval= instancia2 & "Civil" %in% fuero  & prim}
```


\pagebreak
 
`r if (instancia2 & "Laboral" %in% fuero) '# Segunda Instancia Laboral'`
 

```{r lab2_ci, child = 'lab2_ci.Rmd', eval= instancia2 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab2_cadr, child = 'lab2_cadr.Rmd', eval= instancia2 & "Laboral" %in% fuero & cr }
```

\pagebreak

`r if (instancia1 & "CAdm" %in% fuero) '# Cámaras en lo Contencioso-Administrativo'`
 

```{r cad1_ci, child = 'cad1_ci.Rmd', eval= instancia1 & "CAdm" %in% fuero & ci}
```

\pagebreak

```{r cad1_cadr, child = 'cad1_cadr.Rmd', eval= instancia1 & "CAdm" %in% fuero  & cr}
```

`r if (cf) '# Consideraciones Finales'`

\pagebreak

```{r consideraciones_finales, child = 'consideraciones_finales.Rmd', eval= cf}
```


`r if (p) '# Personal'`

\pagebreak

```{r personal_cco, child = 'personal_cco.Rmd', eval = instancia1 & "Civil" %in% fuero & p}
```

\pagebreak

```{r personal_paz, child = 'personal_paz1.Rmd', eval= instancia1 & "Paz" %in% fuero & p}
```


\pagebreak

```{r metodologia, child = 'metodologia.Rmd', eval = metodologia }
```



***
\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}
Director: Lic. Sebastian Castillo    
Equipo: Lic. Marcos Londero y Srta. Emilce Leones  
0343-4209405/410 – ints. 396 y 305  
http://www.jusentrerios.gov.ar/area-planificacion-y-estadisticas-stj/  
correos: apge@jusentrerios.gov.ar - estadistica@jusentrerios.gov.ar   
Laprida 250, Paraná, Entre Ríos  
