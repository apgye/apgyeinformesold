---
title: Causas Iniciadas ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Causas Iniciadas por Grupo de Procesos en materia Civil y Comercial - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura.

Se destaca que el Juzgado de Concordia, conformado por dos secretarías, informa este indicador de manera conjunta por contar con una misma base de datos de Lex-Doctor para ambas. Esta configuración es distinta de todos los demás órganos del fuero que cuentan con una base de datos por Organismo y Secretaría.


```{r }
iniciados_ecq(poblacion = jdos_ecq,
              start_date = start_date,
              end_date = end_date) %>% 
  iniciados_ecq_comparativo() %>% 
  ungroup() %>% 
  mutate(organismo = str_replace_all(organismo, "-sec1", 
                         "ambas sec")) %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```


<!-- ## Causas Iniciadas: Detalle Mensual- `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se muestra el detalle particular de causas iniciadas por tipo de proceso y por mes. -->

<!-- ```{r } -->
<!-- iniciados_ecq_xmes(poblacion = jdos_ecq, -->
<!--               start_date = start_date, -->
<!--               end_date = end_date) %>% -->
<!--   ungroup() %>%  -->
<!--   mutate(organismo = str_replace_all(organismo, "-sec1",  -->
<!--                          "-ambas sec")) %>%  -->
<!--   mutate(organismo = str_sub(organismo, 1,23)) %>%  -->
<!--   kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(2, "5cm") %>% -->
<!--   row_spec(0, angle = 90) %>%  -->
<!--   landscape() -->

<!-- ``` -->

<!-- ## Causas Iniciadas: Detalle Mensual de Quiebras Iniciadas en el Juzgado Civil y Comercial Nº9- `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se muestra el detalle particular de Quiebras iniciadas por mes en el organismo. -->

<!-- ```{r } -->
<!-- iniciados_ecq_xmes(poblacion = jdos_ecq, -->
<!--               start_date = start_date, -->
<!--               end_date = end_date) %>% -->
<!--   filter(circunscripcion != "subtotal") %>%  -->
<!--   filter(str_detect(organismo, "9")) %>%  -->
<!--   filter(str_detect(tipo_proceso, "^CONCURSO|^QUIEBRA|^PEDIDO DE QUIEBRA")) %>%  -->
<!--   janitor::adorn_totals("row") %>%  -->
<!--   kable(caption = "Quiebras Iniciadas", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(2, "5cm") %>% -->
<!--   row_spec(0, angle = 90) %>%  -->
<!--   landscape() -->

<!-- ``` -->


<!-- ## Listado de Causas Iniciadas en el Organismo por mes `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_ecq_prim(poblacion = jdos_ecq, -->
<!--               start_date = start_date, -->
<!--               end_date = end_date) %>%  -->
<!--   filter(str_detect(organismo, "9")) %>%  -->
<!--   kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(3, "8cm") %>% column_spec(6, "5cm") %>% -->
<!--   row_spec(0, angle = 90) %>%  -->
<!--   landscape() -->

<!-- ``` -->

