---
title: Audiencias - Estados, resultados y tiempos en audiencia
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Audiencias según sus distintos estados finales - `r getDataIntervalStr(start_date, end_date)`

Según definición del STJER sobre este indicador los estados finales de audiencia son los siguientes con su correspondiente definición:

+ 1 = Audiencia Fijada: es la audiencia fijada y notificada en una causa o proceso;    
+ 2 = Audiencia Realizada: audiencia efectivamente celebrada;    
+ 3 = Audiencia No Realizada: audiencias que no se realiza por incomparencia de parte;    
+ 4 = Audiencia Cancelada: audiencia que estaba fijada y se dejó sin efecto a pedido de parte;  
+ 5 = Audiencia Reprogramada: audiencia fijada que se suspende, fijándose nuevo día y hora para su realización.  
  
La circunscripción Paraná inició la registración en octubre 2017 (prueba piloto) y las demás circunscripciones en marzo 2018. Por tal motivo los órganos judiciales que están fuera Paraná presentan datos a partir de marzo.


```{r}
audilab_prim(jdos_lab, 
             start_date, 
             end_date) %>% 
  audilab_xestado(desagregacion_mensual = T) %>% 
  select(-fijada) %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```

## Audiencias Realizadas y sus Resultados `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se muestran las audiencias de Conciliación y Vista de Causa realizadas y sus resultados por organismo.   

Los resultados posibles según definición institucional son:

+ Conciliación Total  
+ Conciliación Parcial   
+ Sin conciliación 

La circunscripción Paraná inició la registración en octubre 2017 (prueba piloto) y las demás circunscripciones en marzo 2018. 

```{r}
audilab_prim(jdos_lab,
             start_date,
             end_date) %>%
  audilab_realizadas_xresultado(desagregacion_mensual = T) %>%
  outputTable(caption = "Audiencias Realizadas y sus Resultados") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```

## Audiencias realizadas por Tipo `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presenta información de las audiencias realizadas para los distintos tipos de audiencia. 

La circunscripción Paraná inició la registración en octubre 2017 (prueba piloto) y las demás circunscripciones en marzo 2018. 

```{r}
audilab_prim(jdos_lab,
             start_date,
             end_date) %>%
  audilab_realizadas_xtipo(desagregacion_mensual = T) %>%
  mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>%
  outputTable(caption = "Audiencias Realizadas por Tipo") %>%
  column_spec(2, "3cm") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```
