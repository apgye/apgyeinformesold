Jornada del Fuero de Paz - STJER - marzo 2019
========================================================
author: Lic. Sebastián Castillo (APGE)
date: 
autosize: true

Idea Fuerte
========================================================

Si la carga de trabajo (causas iniciadas) es dinámica (tiempo, territorio y caso) la estructura judicial debe ser lo **suficientemente flexible** para adaptarse a ese dinamismo.

Estructura y Causas Iniciadas - Fuero Paz 2018
========================================================

Los juzgados de paz se agrupan por categoría en 1a., 2a, y 3a. que -en muchos casos- no tiene una relación directa (positiva) con la cantidad de causas iniciadas.  [Mapa](http://justat.jusentrerios.gov.ar:8787/files/apgyeinformes/2019/Informe_FueroPaz_Jornadamarzo2019/mapas.pdf)

Causas Iniciadas Fuero de Paz 2018
========================================================
[Causas_iniciadas_Paz_2018](graf_inic_paz_2018.html) 

Causas Iniciadas 1a Categoría por grupo 2018
========================================================
[Causas_iniciadas_Paz1C](Inic_1c_xgrupo.html) 

Causas Iniciadas 2a y 3a Categoría por grupo 2018
========================================================
[Causas_iniciadas_Paz23C](inic_paz23_xgrpo.html) 

Monitorios y Ejecuciones Iniciados Fuero Civil y Comercial 2018
========================================================
[Monitorios_Ejecuciones](monitorios_ejecuciones_2018.html) 

Escenario de Redistribución de Procesos Monitorios y Ejecutivos 
========================================================
Base Año 2018  
[Escenario Redistribucion](escenario_redistribucion.html) 

Sistema Estadístico del STJER
========================================================

Pilar: declaración primaria de datos!   
Objetivo: calidad en base a un compromiso colectivo   

+ Juzgado de Paz de Ramirez (Marilin Cornago y Flavia, 1er_Lex9)
+ Juzgado de Paz de Villaguay (Nestor Minata, representacion)
+ Juzgdo de Paz de San Salvador (María Dinora Moulins, representacion turnovol)
+ Juzgado de Paz de Ubajay (Juan de Santiago, correccion listados)
+ Juzgado de Paz de Cerrito (Carolina Figueroa, tipo de proceso desclasific)
+ Juzgado de Paz de Villa Urquiza (Patricia Martin, acceso a sistema)
+ Juzgado de Paz de Gobernador Mansilla (Viviana PAPPALARDO,error clasificacion (residual))
+ Juzgado de Paz de Viale (representaciones (error formatos listados dbf))
+ Juzgado de Paz de Viale (representaciones (error formatos listados dbf))
+ Juzgado de Paz de Galarza (Andrea Corfield y Viviana Chaparro, error 2o vencim)
+ Juzgado de Paz de Oro Verde (Ignacio Basaldúa, Gracias! propuesta)
+ Juzgado de Paz de Pueblo Brugo (Fernanda Schoenfeld, Gracias! sugerencias)
+ Juzgado de Paz de Chajarí (Silvana Maidana, consolidación datos -a revisar-) 

