---
title: Causas y Trámites iniciados ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Causas Iniciadas por Grupo de Procesos  - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura.


```{r }
iniciados_paz_procxgpo(poblacion = jdos_paz,
              start_date = start_date,
              end_date = end_date) %>% 
  iniciados_paz_proc_comparativo() %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) 
```

\pagebreak

## Promedio Mensual de Trámites Iniciados - `r getDataIntervalStr(lubridate::make_date(2018,09,01), end_date)`

En esta tabla se muestra el promedio mensual de trámite voluntarios iniciados por juzgado. El registro sistemático y normalizado de trámites voluntarios se inicia a partir de septiembre 2018.

```{r }
promedio_iniciados_paz_tramxgpo(poblacion = jdos_paz,
              start_date = start_date,
              end_date = end_date) %>%
  promedio_iniciados_paz_tram_comparativo() %>%
  kable(caption = "Trámites Iniciados por Grupo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) 
```

