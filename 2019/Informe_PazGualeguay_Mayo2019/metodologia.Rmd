---
title: Metodología - Estadística Pública Judicial STJER
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


# Metodología - Estadística Pública Judicial STJER

Las Nuevas Estadísticas del Poder Judicial de Entre Ríos surgieron en el 2018 como resultado del Plan de Modernización Estadística (resolución TS 277/16, concluido el 21/09/18) y están sujetas al Reglamento de Estadística vigente –cfr. Res. Tribunal Superintendencia Nº234/17, http://www.jusentrerios.gov.ar/estadistica/reglamento-de-estadistica-19-09-17/.  Este nuevo modelo estadístico está integrado por un conjunto de operaciones diseñadas e implementadas por el Área de Planificación Gestión y Estadística y constituye un sistema coherente de definiciones sustantivas, metodologías, procedimientos y herramientas dirigido a la producción de información de calidad.    

Para el mantenimiento y actualización del sistema se realizan consultas a magistrados y funcionarios –autorizadas por las Salas del STJER- y se celebran encuentros de trabajo donde se recogen ideas y definiciones relevantes para la actualización y construcción de indicadores judiciales. Al mismo tiempo, se trabaja en la revisión de indicadores comparados con distintos Poderes Judiciales provinciales y documentos técnicos sobre estadística judicial. Todo esto tiene como resultado un set de indicadores aprobados institucionalmente y en permanente actualización. 

Los procedimientos empleados en el relevamiento de datos usted los puede consultar (por órgano, instancia y fuero) en las Guías de Estadística accesibles a través de la Página Web del Superior Tribunal de Justicia, en la sección del Área de Planificación Gestión y Estadística, accediendo a la opción *RECURSOS*, y luego *GUÍAS SOBRE ESTADÍSTICA JUDICIAL (NUEVO)*. O bien accediendo directamente a la dirección: https://drive.google.com/drive/folders/1HjJ8E82mcIVIck4H0giFWwTmOVLbK2NZ eligiendo la guía correspondiente. A través de los siguientes enlaces puede acceder a dichas guías directamente:

+ [Juzgados Civil y Comercial, y Paz 1ªCat.](https://drive.google.com/open?id=1EOJggtPkUTgHQRuQesBuzmEZMoYe2UO3juRUVjiVFNw)
+ [Juzgados de Familia](https://drive.google.com/open?id=1nW02icHcRpLz8w_5X9a_uEYCsyQ6Nzf_dNzsNljpxXU)
+ [Juzgados Laborales](https://drive.google.com/open?id=1I051W6JH2lQ0kH3QH_5fW0xXYhkLGehoJpCLtmdOBuc)
+ [Juzgados de Paz 2ª y 3ª Cat.](https://drive.google.com/open?id=1uLoT22Kr0SmnNCQD2J3LbapuJQUs3vBvjYX4fno_I5A)
+ [Camaras Civiles](https://drive.google.com/open?id=1OhqQbi1dRkAyISQuC5pkNtEKe467-fB1icK1AQrh2bE)
+ [Camaras Laborales](https://drive.google.com/open?id=1uJNgds2fREkteS7c5OVGypRD5J9nvjwjsm2RGBJh8e4)
+ [Camaras Contencioso Administrativas](https://drive.google.com/open?id=1EJmsBq1Wzg5sLKXmgS3VXak2duu4Q75vydyVZergmtQ)
+ [Camaras de Casación Penal](https://drive.google.com/open?id=1V6pz_sW2v-vbyJxufaoMtgqUiiJ-6REoB97BNnlMYFA)
+ [Oficinas de Gestión de Audiencia Penales](https://drive.google.com/open?id=1mRQbflTQeORMGGUWOhz8Q6i0zCnJox5HL67PVlTPGu4)
+ [Superior Tribunal de Justicia - Contencioso Administrativo](https://drive.google.com/open?id=1Ic4eI79cWCAgckpjjU_N3kqCZKJ8biFoYkVOJe8mzOQ)
+ [Sala I de Procedimientos Constitucionales y Penal del STJER](https://drive.google.com/open?id=1ZislKECWEEXkLAbyEmgCWNiYGzBhKqB59b1LUZSR6kM)
+ [Sala II Civil y Comercial STJER](https://drive.google.com/open?id=1Qcptj4TrXvfcRYcZ9iSuI3O4sIefcJ2dsruttqGAdtA)
+ [Sala III del Trabajo STJER](https://drive.google.com/open?id=1SX8JuqWFR0xXOK3Y2R6HqcOwN_sLtsgNoI5ZQODP8yA)

Las rutinas de procesamiento estadístico empleadas en el cálculo de indicadores usted las puede consultar seleccionando la operación correspondiente del repositorio accesible en el siguiente [link](https://bitbucket.org/apgye/apgyeoperationsjuser/src/master/R/).   

## Calidad Acreditada de la Información Estadística  

La calidad de la información elaborada por el APGE se acredita a través una estructura de operaciones públicas, que contempla distintos niveles de validación y revisión de datos por parte de nuestros organos informantes.    

Dichas validaciones y revisiones se cumplen antes, durante y después de la producción de indicadores calculados:    

+ **antes**: la información requerida a los órganos judiciales surge de guías de estadística y su conformidad con ellas se controla por cada organismo judicial -cfrme.Reglamento de Estadística. En el proceso de entrega de información judicial el APGE implementa validaciones de consistencia en datos críticos (ej, validación de codificación apropiadas, validación de no-declaración de fechas a futuro, entre otras) y ante la detección de no-conformidades comunica al organismo las mismas para su reprocesamiento,    
+ **durante**: al momento de efectuar una presentación de datos cada órgano informante recibe un **pre-informe** con los resultados de sus indicadores al que deben controlar antes de la confirmación de entrega de información, y   
+ **después**: nuestros informantes reciben mensualmente (los días 13 -órganos no penales- y 18 -órganos penales-) un *Informe para Revisión* con los indicadores calculados del mes para efectuar las correcciones necesarias ante la detección de errores o inconsistencias en las declaraciones de datos.     

El APGE está sujeto a una normativa estricta en materia estadística: Reglamento de Estadística (Res. T.S. Nº234/17), art.20 de la LOPJ, principio de acceso a información pública (Art. 13 de la Constitución Provincial y 1º, 33, 41, 42 y 72 inc. 22 de la Constitución Nacional), Ley Nacional 25326 de Protección de Datos Personales, entre otras.    

Además, sigue los lineamientos del “CÓDIGO DE BUENAS PRÁCTICAS DE LAS ESTADÍSTICAS JUDICIALES de las Provincias Argentinas y de la Ciudad Autónoma de Buenos Aires” aprobado por JUFEJUS, que propone los siguientes principios orientadores para los órganos de estadística: la independencia profesional, el trato imparcial a todos nuestros usuarios, la objetividad, la fiabilidad, la confidencialidad estadística y la relación costo-eficacia.      

## Observaciones sobre formatos de datos 

+ **Datos de Fechas**: elegimos el formato fecha "Año-Mes-Día" pues dado su posibilidad efectiva de ordenamiento cronológico  permite una recuperación más rápida de información. Este formado es el que contempla la norma ISO 8601 sobre interoperabilidad de fechas a nivel internacional. 

## Observaciones sobre Indicadores Estadísticos

El principal desafío tecnológico en la producción de indicadores judiciales se encuentra en las limitaciones que tiene la herramienta de Gestión de Causas del STJER Lex-Doctor 9. Dicho sistema carece de funcionalidades adecuadas para el registro y producción de información estadística, y en general presenta configuraciones abiertas para el registro de datos que facilitan la propagación de errores de registro. Por estas razones permanentemente se están fortaleciendo y revisando los procedimientos de validación de datos para subsanar anomalías. 

Sin perjuicio de lo anterior, cada indicador estadístico tiene un tratammiento específico, cuyas notas sobresalientes publicitamos aquí para una adecuada valoración de su alcance. 

+ **Causas Iniciadas**: para el cómputo de este indicador se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, tipo de proceso y fecha de inicio. Dadas las dificultades en materia de registración antes apuntadas se realizan exclusiones de datos anómalos por fuero (e.g. exclusiones en materia civil en razón de registros mal cargados como "causa iniciada" dado que sus tipos de proceso no se vinculan con tipos procesales vigentes o directamente porque no constituyen causas: OFICIO, EXHORTO, SUMARIO, COMPENSACION, DESARCHIVO, INFORME, DENUNCIA, CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO, PERSONAL, entre otros). El listado completo de exclusiones por materia puede consultarse en el siguiente [link](https://docs.google.com/document/d/1olfVtyeh-6v71HYxLaNaEIaRon4w38WjLyZmn25O_rc/edit?usp=sharing). Para los trámites voluntarios de los órganos de paz se prescinde del dato sobre número de expediente atento al criterio de registración que emmplean estos organismos. Para las causas iniciadas en el período febrero-mayo 2018 se extrajo información desagregada del listado de Causas en Trámite en ese período. A partir de junio-2018 se habilita una operción particular para los causas iniciadas en primera instancia y en diciembre-2018 en segunda instancia.  

+ **Causas Resueltas**: para el cómputo de este indicador se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, y fecha de resolución dentro del período correspondiente. Este indicador regularmente se acompaña del indicador de **causas a despacho del mes** que representa a la cantidad de procesos que se pusieron a despacho en el mes que se está informando (no están incluidos en este número la cantidad de causas a despacho proveniente de meses anteriores y por tal razón en muchas ocasiones la cantidad de casos resueltos es mayor a la cantidad de casos ingresados a despacho). Una resolución registrada fuera del período al que corresponde (e.g. sentencia de octubre 2018 registrada y declarada en el listado de causas resueltas en noviembre 2018) no se computa como declaración válida. Todos los intervalos temporales se computan en días corridos. Las fechas de vencimiento corresponden a plazos calculados por cada órgano judicial.    

+ **Causas en Trámite**: para el cómputo de este indicador se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, fecha de inicio y fecha del último acto procesal registrado. Esta última restricción es muy importante antento a que si el registro no tiene valor de al menos un acto procesal registrado en el sistema no se computa como causa en tramite. Ello es así a fin de evitar contar registros donde no se dispone de información suficiente para asumir que existe una causa iniciada.

+ **Audiencias**: para el cómputo de audiencias realizadas se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, tipo de audiencia, estado de audiencia realizada y fecha de audiencia en el período correspondiente. En general el trabajo con este indicador supone la corrección de diversas anomalías (ej.se eliminan las referencias a minutos en el campo de duración en minutos (duracm) sin reimputación de datos por parte del área. 

+ **Trámites Voluntarios (Juzgados de Paz)**: La registración de los trámites voluntarios en Lex-Doctor está prevista en el Reglamento para Juzgados Civiles y Comerciales Acuerdo Especial 02/07/2008 (pt.3.1, inc.3). Sin perjuicio de ello, en el relevameinto de este indicador se constató  diversidad de criterios y prácticas de registración en los equipos de trabajo, lo cual obligó a efectuar comunicaciónes aclaratorias específicas para su instrumentación. En tal sentido, los trámites iniciados comienzan a relevarse a través de su operación específica por listado en junio 2018 (comunicación efectuada el 14/06/18), y ante las consultas efectuadas en los meses siguientes el 03/09/18 se emite una circular explicitando la metodología específica para informar trámites del turno voluntario.     

+ **Causas Pendientes de Resolución**: indicador que presenta la cantidad de causas a despacho sin resolución (i.e. *pendientes*) en el período seleccionado. El indicador *pendientes vencidas* incluye a los casos con plazo vencido, considerando el 1º vencimiento para el dictado de resolución (computado al último día hábil del período seleccionado). Estos plazos son establecidos por cada organismo según lo que fijan las normas aplicables al proceso.   

+ **Confirmaciones, Revocaciones y Nulidades**: indicador cuantitativo que refiere a la interacción entre instancias judiciales a propósito de la resolución de casos sometidos a decisión. Aunque la bibliografía sobre estadística judicial ha vinculado estos indicadores con la *calidad de las decisiones* (especialmente el de *nulidades* -cfr.CEJA, *Cifrar y Descifrar Vol.II: Indicadores Judiciales para las Américas.*,  2005, p.36, accesible de manera pública, on line en: http://biblioteca.cejamericas.org/handle/2015/3843), discrepamos con esta lectura entendiendo que solo puede evaluarse calidad cuando se dispone de atributos identificados y cuantificables de los eventos que se analizan. Aspecto -este último- que no forma parte del Sistema de Indicadores Estadísticos vigentes del STJER. Dicho esto, consideramos útil el indicador pues brinda información inmediata a la Primera Instancia sobre los decisiones adoptadas en casos apelados, promoviendo una rápida transferencia de conocimiento entre órganos judiciales.    

## Datos del sistema y ambiente de producción al `r Sys.Date()`

```{r, comment=""}
kable(as.data.frame(Sys.info()))
```

```{r, comment=""}
sessionInfo()
```

```{r}

tribble(
  ~valor,  ~parametro, 
  circ, "Circunscripción",
  ieps, "Organismo_particular", 
  fuero, "Fuero", 
  instancia1, "instancia1",
  instancia2, "instancia2",
  instancia3 ,"instancia3",
  start_date ,"start_date",
  end_date ,"end_date",
  desagregacion_mensual ,"desagregacion_mensual",
  ci ,"CausasIniciadas",
  ca ,"CausasArchivadas",
  cr ,"CausasResueltas",
  ce ,"CausasEnTramite",
  cra, "ConfirmaRevocaAnula",
  ced ,"CausasEnTramite_detalle",
  cm ,"CausasyMovimientos",
  s ,"SentenciasTipo",
  afd ,"Audiencias_familia_detalle",
  ald ,"Audiencias_laboral_detalle",
  acd ,"Audiencias_civil_detalle",
  ap ,"Audiencias_penales",
  d ,"Duracion_Procesos_xorg",
  di ,"Duracion_Procesos_interanual",
  cf ,"Consideraciones_Finales",
  p ,"Personal",
  pend ,"Pendientes",
  prim ,"Primarias",
  metodologia ,"Metodología",
  presentaciones ,"Presentaciones",
  bo ,"Boletin_intro") %>% 
  mutate(valor = ifelse(is.na(valor), FALSE, valor)) %>% 
  filter(valor != "FALSE") %>% 
  kable()

```


`r if (presentaciones) '## Datos Primarios disponibles en el período'`

```{r, presentaciones, eval = presentaciones, echo=FALSE, size = 'tiny' }
listar_presentaciones() %>%
  outputTable(caption = "Tabla de presentaciones efectuadas por órgano") %>%
  column_spec(4, width = "14cm") %>%
  row_spec(0, angle = 90) %>%
  landscape()

```


