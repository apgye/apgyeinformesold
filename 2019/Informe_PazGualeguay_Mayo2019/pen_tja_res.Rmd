---
title: Resoluciones en Tribunal de Juicio y Apelación
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Resoluciones en Tribunal de Juicio - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar las resoluciones por integración.   

Se agrega al final de la tabla el dato de personas sobreseidas, condenadas y absueltos según el registro de justiciables efectuado en cada legajo.     

Finalmente, la fila total muestra la cantidad de resoluciones por circunscripción.      

```{r, tjui, echo = FALSE, message = FALSE, warning = FALSE}


if (desagregacion_mensual){
resoluciones_pen(poblacion = tja,
              start_date = start_date,
              end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>%  
    select(-sobreseidos, -condenados, -absueltos) %>% #                 OJO REACTIVAR
    outputTable(caption = "Resoluciones en Tribunal de Juicio") %>%
    row_spec(0, angle = 90) %>%
    column_spec(3, "12cm") %>%
    column_spec(10, border_right = T) %>%
    landscape()

} else {
resoluciones_pen(poblacion = tja,
              start_date = start_date,
              end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>%   
    outputTable(caption = "Resoluciones en Tribunal de Juicio") %>%
    row_spec(0, angle = 90) %>%
    column_spec(2, "12cm") %>%
    column_spec(9, border_right = T) %>%
    landscape()
}
```


## Resoluciones en Tribunal de Apelación - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar las resoluciones por integración.   


```{r, apel, echo = FALSE, message = FALSE, warning = FALSE}

resultado_tapel %>%
  outputTable(caption = "Resoluciones en Tribunal de Apelación") %>%
  row_spec(0, angle = 90) %>%
  landscape()

```

