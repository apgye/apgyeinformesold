newenv <- function(...) {
  envinitialization <- rlang::enexprs(...)
  env = new.env()
  for(var in names(envinitialization)) {
    env[[var]] = eval(envinitialization[[var]])
  }
  env
}

outputTable <- function (table, caption, row_group_label_position = "identity") {
  if(row_group_label_position == "identity") {
    total_rows <-  c(which(table[, 1] == "Total"), which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  } else {
    total_rows <-  c(which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  }
  table %>% rename_all(.funs = stringr::str_replace_all, pattern = "_", replacement=" ") %>% 
    kable("latex", caption = caption,
          align = 'c', longtable = TRUE, booktabs = T ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), latex_options = c("repeat_header"),
                  full_width = F, font_size = 10) %>% 
    row_spec(total_rows, bold = T) %>% 
    collapse_rows(columns = 1:2, row_group_label_position = row_group_label_position)
  
}

getDataIntervalStr <- function(data_interval_start, data_interval_end){
  year <- lubridate::year(data_interval_start);
  year_end <- lubridate::year(data_interval_end);
  month <- lubridate::month(data_interval_start);
  month_end <- lubridate::month(lubridate::as_date(data_interval_end)  - lubridate::days(1));
  monthDiff <- lubridate::interval(data_interval_start %>% as.Date(), data_interval_end %>% as.Date()) %/% months(1)
  period <- format(lubridate::make_date(year, month, 1), '%B %Y')
  
  period[monthDiff == 12] <- paste("anual", year[monthDiff == 12])
  period[monthDiff < 12] <- paste(format(lubridate::make_date(year, month, 1), '%B'), '-', format(lubridate::make_date(year, month_end, 1), '%B'), year[monthDiff < 12])
  period[monthDiff > 12] <- paste(format(lubridate::make_date(year, month, 1), '%B %Y'), '-', format(lubridate::make_date(year_end, month_end, 1), '%B %Y'))
  
  period[monthDiff == 6 & month == 1] <- paste("primer semestre", year[monthDiff == 6 & month == 1])
  period[monthDiff == 6 & month == 7] <- paste("segundo semestre", year[monthDiff == 6 & month == 7])
  period[monthDiff == 1] <- paste(format(lubridate::make_date(year[monthDiff == 1], month[monthDiff == 1], 1), '%B %Y'))
  
  
  period
}

resolverconvertidos <- function(df) {
  df <- df %>% 
    mutate(iep = ifelse(iep == "jdofam0101con", "jdofam0100con", iep)) %>% 
    mutate(iep = ifelse(iep == "jdofam0102con", "jdofam0300con", iep)) %>% 
    mutate(iep = ifelse(iep == "jdofam0201con", "jdofam0200con", iep)) %>% 
    mutate(iep = ifelse(iep == "jdofam0202con", "jdofam0300con", iep)) %>% 
    mutate(iep = ifelse(iep == "jdofam0001gch", "jdofam0100gch", iep)) %>%
    mutate(iep = ifelse(iep == "jdofam0002gch", "jdofam0200gch", iep)) %>%
    mutate(iep = ifelse(iep == "jdofam0001uru", "jdofam0200uru", iep)) %>%
    mutate(iep = ifelse(iep == "jdofam0002uru", "jdofam0200uru", iep))
  df
}


redistribuirxgpo <- function(dfinic, perdedores, ganadores, grupos, inversagrupo = FALSE) {
  
  #dfinic toma salida de iniciados_cco(jdos_cco,start_date, end_date)
  #perdedores pierden grupo
  #inversa de grupo significa todos menos el grupo
  
  dfinic <- dfinic %>% 
    filter(!tipo_proceso == "subtotal")
  
  if(diffgrupo){
    casos_aredistribuir <- dfinic %>% 
      filter(organismo %in% perdedores$organismo_descripcion) %>% 
      filter(!grupo_proceso %in% grupos) 
    casos_aredistribuir
  } else {
    casos_aredistribuir <- dfinic %>% 
      filter(organismo %in% perdedores$organismo_descripcion) %>% 
      filter(grupo_proceso %in% grupos) 
    casos_aredistribuir
  }
  
  
  if(nrow(ganadores) == 1) {
    casos_redistribuidos <- casos_aredistribuir %>% 
      group_by(circunscripcion, grupo_proceso) %>% 
      summarise(tipo_proceso = "-", 
                organismo = ganadores$organismo_descripcion,
                cantidad = sum(cantidad, na.rm = T)) %>% 
      select(colnames(dfinic)) 
    casos_redistribuidos
  } else {
    casos_redistribuidos <- casos_aredistribuir %>% 
      group_by(circunscripcion, grupo_proceso) %>% 
      summarise(tipo_proceso = "-", 
                organismo = NA, 
                cantidad = round(sum(cantidad/nrow(ganadores), na.rm = T))) %>% 
      select(colnames(dfinic)) %>% 
      bind_rows(replicate(nrow(ganadores)-1, ., simplify = FALSE)) %>% 
      mutate(organismo = rep(ganadores$organismo_descripcion, 
                             each = length(unique(casos_aredistribuir$grupo_proceso))))
    casos_redistribuidos
  }
  
  dfredistribuidos <- dfinic %>% 
    anti_join(casos_aredistribuir, by = c("organismo", "grupo_proceso", "tipo_proceso")) %>% 
    bind_rows(casos_redistribuidos) %>% 
    group_by(circunscripcion, organismo, grupo_proceso) %>% 
    arrange(circunscripcion, organismo, grupo_proceso, desc(cantidad)) %>% 
    #do(janitor::adorn_totals(.)) %>% 
    ungroup()
  
  dfredistribuidos
  
}


agrupartproc <- function(df, materia) {
  if (materia ==  "civil") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      mutate(gtproc_forma = case_when(
        str_detect(tproc, "^ORDINARIO|^ORDINARIO |USUCAP") ~ "ORDINARIOS", #
        str_detect(tproc, "^SUMARISIMO|^SUMARISIMO ") ~ "SUMARISIMO",  #
        str_detect(tproc, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES",
        str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR ") ~ "CAUTELARES",
        str_detect(tproc, "MONITORIO") ~ "MONITORIOS",
        str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO") ~ "EJECUCIONES", #
        str_detect(tproc, "^INTERDICT") ~ "INTERDICTOS",
        str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTIT.",
        str_detect(tproc, "^PREPARACION ") ~ "PREPARATORIOS",
        str_detect(tproc, "^SUCESORIO") ~ "SUCESORIOS", #
        str_detect(tproc, "^BENEFICIO") ~ "BENEF.LITIG.SG.", #
        str_detect(tproc, "^HOMOLOGACION") ~ "HOMOLOGACIONES", 
        # Anomalías: desclasificados, no procesos y errores
        str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA") ~ "noprocesos",
        str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA") ~ "desclasificados",
        str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL") ~ "error",
        # Otros Fueros Laboral y Familia
        str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION") ~ "desclasificados", 
        str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON") ~ "desclasificados",
        str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU ") ~ "desclasificados",
        str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO") ~ "desclasificados",
        TRUE ~ "OTROS"
      )) %>% 
      filter(!gtproc_forma %in% c("noprocesos", "desclasificados", "error"))
    df
  } else if(materia == "familia") {
    df <- df %>%
      mutate(tproc = toupper(tproc)) %>%
      mutate(gtproc_forma = case_when(
        str_detect(tproc, "VIOLENCIA FAMILIAR") ~ "VIOLENCIA FAMILIAR", #
        str_detect(tproc, "VIOLENCIA DE GENERO") ~ "VIOLENCIA DE GENERO", #
        str_detect(tproc, "HOMOLOGACION") ~ "HOMOLOGACIONES", 
        str_detect(tproc, "DIVORCIO") ~ "DIVORCIOS", 
        str_detect(tproc, "ALIMENTO") ~ "ALIMENTOS",  #
        str_detect(tproc, "^EJECU|^PROCESO DE EJECU") ~ "EJECUCIONES",
        str_detect(tproc, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES",
        str_detect(tproc, "^MEDIDA") ~ "MEDIDAS",
        str_detect(tproc, "^RESTRICCIONES") ~ "RESTRICCIONES CAPAC.",
        str_detect(tproc, "^ORDINARIO|^ORDINARIO ") ~ "ORDINARIOS", #
        str_detect(tproc, "^INTERNACION") ~ "INTERNACIONES", #
        str_detect(tproc, "^AUTORIZACION") ~ "AUTORIZACIONES", #
        str_detect(tproc, "^BENEFICIO") ~ "BENEF.LITIG.SG.", #
        str_detect(tproc, "^REGIMEN") ~ "REGIMEN COMUNIC.", #
        str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTIT.",
        # Anomalías: desclasificados, no procesos y errores
        str_detect(tproc, "^SU |^EXHORTO|^OFICIO|DESARCHIVO|^INFORME|^DENUNCIA|^TESTIMONIO") ~ "noprocesos",
        str_detect(tproc, "CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION") ~ "desclasificados",
        str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL") ~ "error",
        str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION") ~ "desclasificados",
        TRUE ~ "OTROS")) %>% 
      filter(!gtproc_forma %in% c("noprocesos", "desclasificados", "error")) 
    df
  } else if(materia == "laboral") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      mutate(gtproc_forma = case_when(
        str_detect(tproc, "^COBRO") ~ "COBRO DE PESOS", #
        str_detect(tproc, "^ACCIDENTE") ~ "ACCIDENTE DE TRABAJO", #
        str_detect(tproc, "^APELACION DIC") ~ "APELACION COMISION MEDICA", #
        str_detect(tproc, "^HOMOLOGACION") ~ "HOMOLOGACIONES", #
        str_detect(tproc, "^EJEC|^PROCESO DE EJECU|^APREMIO") ~ "EJECUCIONES", #
        str_detect(tproc, "^ORDINARIO|^ORDINARIO ") ~ "ORDINARIOS", #
        str_detect(tproc, "^SUMARISIMO|^SUMARISIMO ") ~ "SUMARISIMO",  #
        str_detect(tproc, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES",
        str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR ") ~ "CAUTELARES",
        str_detect(tproc, "^MEDIDAS PREP") ~ "MEDIDAS PREPARAT.",
        str_detect(tproc, "^BENEFICIO") ~ "BENEF.LITIG.SG.", #
        str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTIT.",
        # Anomalías: desclasificados, no procesos y errores
        str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA") ~ "noprocesos",
        str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION") ~ "desclasificados",
        str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|MONITORI|PERSONAL") ~ "error",
        str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON") ~ "desclasificados",
        str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU ") ~ "desclasificados",
        str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO|^DESALOJO|^APELACION JUZGADO|^DIVISION DE|^SEGUNDO TEST") ~ "desclasificados",
        TRUE ~ "OTROS")) %>% 
      filter(!gtproc_forma %in% c("noprocesos", "desclasificados", "error")) 
    df
  } else if(materia == "pazproc") {
      df <- df %>% 
        mutate(tproc = toupper(tproc)) %>%
        mutate(gtproc_forma = case_when(
          str_detect(tproc, "^ORDINARIO|^ORDINARIO |USUCAP") ~ "ORDINARIOS", #
          str_detect(tproc, "^SUMARISIMO|^SUMARISIMO ") ~ "SUMARISIMO",  #
          str_detect(tproc, "^COBRO DE PESOS") ~ "COBRO DE PESOS",  #
          str_detect(tproc, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES", #
          str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR ") ~ "CAUTELARES", #
          str_detect(tproc, "MONITORIO") ~ "MONITORIOS", #
          str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO") ~ "EJECUCIONES", #
          str_detect(tproc, "^INTERDICT") ~ "INTERDICTOS",
          str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTIT.",
          str_detect(tproc, "^PREPARACION ") ~ "PREPARATORIOS",
          str_detect(tproc, "^SUCESORIO") ~ "SUCESORIOS", #
          str_detect(tproc, "^BENEFICIO") ~ "BENEF.LITIG.SG.", #
          str_detect(tproc, "^HOMOLOGACION") ~ "HOMOLOGACIONES", 
          # Anomalías: desclasificados, no procesos y errores
          str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA|^SUPERVIVENCIA|^AUTORIZACION") ~ "noprocesos",
          str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|PAZ") ~ "desclasificados",
          str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL|^LICENCIAS|^ARANCELES") ~ "error",
          # Otros Fueros Laboral y Familia
          str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION") ~ "desclasificados", 
          str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON") ~ "desclasificados",
          str_detect(tproc, "AMENAZA|^LESIONES|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU ") ~ "desclasificados",
          str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO") ~ "desclasificados",
          TRUE ~ "OTROS"
        )) %>% 
        filter(!gtproc_forma %in% c("noprocesos", "desclasificados", "error"))
      df
  } else if(materia == "paztram") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      mutate(gtproc_forma = case_when(
        str_detect(tproc, "^CERTIFICACION") ~ "CERTIFICACIONES",  #
        str_detect(tproc, "^AUTORIZACION") ~ "AUTORIZACIONES", #
        str_detect(tproc, "^CARTA ") ~ "CARTAS PODER/POBREZA",  #
        str_detect(tproc, "^DECLARACION JURADA") ~ "DECLARACION JURADA",  #
        str_detect(tproc, "^FORMULARIO") ~ "FORMULARIOS (ANSES,DDJJ,LEY3011,etc)",  #
        str_detect(tproc, "INFORMACION SUMARIA") ~ "INFORMACION SUMARIA",
        str_detect(tproc, "MEDIDAS ALTERNATIVAS") ~ "CONTROL MED.ALTERNAT.",
        str_detect(tproc, "RUBRICA DE LIBROS") ~ "RUBRICA DE LIBROS",
        TRUE ~ "OTROS"
      )) %>% 
      filter(!gtproc_forma %in% c("noprocesos", "desclasificados", "error"))
    df
  }
  df
}

listar_presentaciones <- function() {
  
  fuero <- str_c(str_sub(tolower(fuero), 1,3), collapse = "|") %>% str_replace("civ", "cco")
  
  if (instancia1 & instancia2 & instancia3) {org <- "jdo|cam|sal"} else if (instancia1 & instancia2) {org <- "jdo|cam"} else if (instancia1) {org <- "jdo|camcad"}
  
  if (is.na(circ)) {
    circ <- "Gualeguaychú|Uruguay|Concordia|Paraná|Diamante|Federación|Federal|San Salvador|Tala|Victoria|Islas del Ibicuy|Chajarí|Colón|Gualeguay|La Paz|Nogoyá|Villaguay|Feliciano" 
  } else {
    circ <- circ
  }
  
  poblacion_informada <- poblacion_total %>% 
    filter(str_detect(organismo, !!fuero)) %>% 
    filter(str_detect(organismo, !!org)) %>% 
    filter(str_detect(circunscripcion, !!circ))
  
  presentaciones_justat <- DB_PROD() %>% tbl("submission") %>% 
    select(-input) %>% filter(data_interval_start >= start_date, data_interval_end <= end_date) %>%
    filter(enabled == TRUE) %>% filter(iep %in% poblacion_informada$organismo) %>%
    collect() %>% 
    mutate(mes_informado =  lubridate::month(data_interval_start)) %>% 
    mutate(año_informado = lubridate::year(data_interval_start)) %>% 
    arrange(jurisdiction, iep, año_informado, mes_informado) %>% 
    rowwise() %>% 
    mutate(periodo_informado = str_c(año_informado, mes_informado, sep = "-")) %>% 
    ungroup() %>% 
    group_by(iep, operation) %>% 
    summarise(presentaciones = n(), 
              periodos_informados = str_c(periodo_informado, collapse = ", ")) %>% 
    left_join(apgyeJusEROrganization::OPERATION_DESCRIPTION, 
              by = c("operation" = "operacion")) %>% 
    left_join(poblacion_total[, c("organismo", "organismo_descripcion", 
                                  "circunscripcion")], by = c("iep" = "organismo")) %>% 
    ungroup() %>% 
    select(circunscripcion, organismo_descripcion, operacion_descripcion, 
           'periodos informados (año-mes)' = periodos_informados, presentaciones, -iep) %>% 
    group_by(circunscripcion, organismo_descripcion) %>% 
    arrange(circunscripcion, organismo_descripcion, operacion_descripcion) %>% 
    do(janitor::adorn_totals(.)) %>% 
    ungroup() %>% 
    mutate(organismo_descripcion = str_sub(organismo_descripcion, 1, 23))
  
  presentaciones_justat
}


