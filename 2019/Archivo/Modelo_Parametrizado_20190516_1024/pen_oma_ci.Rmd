---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Legajos Iniciados en OMA por Responsable - `r getDataIntervalStr(start_date, end_date)`

La Oficina comienza a informar a través de listados primarios a partir de marzo 2019.

```{r}
iniciados_oma(poblacion = oma,
              start_date = start_date,
              end_date = end_date) %>% 
  outputTable(caption = "Legajos Iniciados") 
  
```
