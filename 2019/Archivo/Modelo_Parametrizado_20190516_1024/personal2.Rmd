---
title: "Personal: Planta Ocupada y Condición de Revista"
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1.5cm
mainfont: Liberation Sans
sansfont: Liberation Sans
df_print: kable
params:
    poblacion: "jdocco1001pna"
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```



```{r}
poblacion <- jdos_cco
```

\blandscape

### Personal

Información suministrada por el Área de Gestión Humana del STJER-

```{r}
personal(poblacion = poblacion) %>% 
  # trabajo con planta ocupada y revista
  .$planta_ocupada_revista %>% 
  left_join(poblacion %>% select(-categoria), by=c("organismoAPGyE"="organismo")) %>% 
  ungroup() %>% select(circunscripcion, organismo=organismo_descripcion, categoria, revista, cantidad) %>% 
  group_by(circunscripcion, organismo) %>% 
  do(janitor::adorn_totals(.)) %>% 
  # función sobre planta ocupada
  # personalPorCategoria(poblacion = poblacion) %>% 
  #   filter(!grepl("JUEZ", categoria)) %>% 
  #   tidyr::spread(key = categoria, value = cantidad ) %>% 
  outputTable(caption = "Planta Ocupada")  %>% 
  row_spec(0, angle = 90)
```

\elandscape

