---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r, echo = FALSE, warning=FALSE, message=FALSE}
inic_tjui <- iniciadas_pen(poblacion = tja,
              start_date = start_date,
              end_date = end_date)
```

## Causas Iniciadas Totales

### Tribunal de Juicio - `r getDataIntervalStr(start_date, end_date)`

```{r}
inic_tjui %>%
  filter(circunscripcion != "Total") %>% 
  group_by(circunscripcion, organismo) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  janitor::adorn_totals("row") %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  kable(caption = "Causas Iniciadas total", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
  
```

### Tribunal de Apelacion- `r getDataIntervalStr(start_date, end_date)`

```{r}
inic_ap %>%
  filter(circunscripcion != "Total") %>% 
  group_by(circunscripcion, organismo) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  janitor::adorn_totals("row") %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  kable(caption = "Causas Iniciadas total", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
  
```

\pagebreak 

## Causas Iniciadas por Tipo de Proceso en Tribunal de Juicio- `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede ver las causas o legajos iniciados en el Tribunal de Juicio según el informe suministrado por OGA.    

Las causas se agrupan por tipo de proceso, agregándose el total de casos por cada tipo.   

```{r}
inic_tjui %>%
  # kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  # kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
  #                full_width = F, font_size = 10)  %>%
  # row_spec(0, angle = 90) %>%
  outputTable(caption = "Causas Iniciadas") %>%
  column_spec(2, "5cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
  
```


## Causas Iniciadas por Tipo de Proceso en Tribunal de Apelación- `r getDataIntervalStr(start_date, end_date)`

```{r}
inic_ap %>% 
  # kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  # kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
  #                full_width = F, font_size = 10)  %>%
  outputTable(caption = "Causas Iniciadas") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
  
```