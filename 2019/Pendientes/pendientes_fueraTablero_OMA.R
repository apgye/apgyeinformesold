library(dplyr)
library(readr)

entidades <- read_csv("..//apgyeJusEROrganization/inst/apgye/entidades.csv")
# View(entidades)

entidades <- entidades %>% 
  filter(grepl("oma|equ", organismo))

realizadas <- presentaciones_justat %>% # al 26/07/2019 12.18
  filter(iep %in% entidades$organismo)

pend <- apgyeJusEROrganization::presentaciones_pendientes(submissions = realizadas)

operaciones <- apgyeJusEROrganization::OPERATION_DESCRIPTION

prePending <- pend %>%
  left_join(entidades) %>% 
  left_join(operaciones, by = c("operacion", "operacion")) %>% 
  select(organismo_descripcion, localidad, operacion_descripcion, periodo_vencido) %>% 
  group_by(organismo_descripcion, localidad, operacion_descripcion) %>%
  summarise(periodos = stringr::str_c(periodo_vencido, collapse = ", ")) %>%
  mutate(debe = stringr::str_c(operacion_descripcion, " (", periodos, ")")) %>%
  summarise(Deuda = stringr::str_c(debe, collapse = ", ")) %>% 
  rename(Organismo = organismo_descripcion, Localidad = localidad)
  
prePending <- prePending %>% filter(!is.na(Organismo))
View(prePending)
  
setwd("~/apgyeinformes/2019/Pendientes")
write.table(prePending, "Pendientes_Administrativo.csv", sep = ",", row.names = FALSE, fileEncoding = "latin1")
# Enviado hoy actualizado a Asuntos Administrativo. mail: jueves 25/07/2019 10:18

  



# pendientes <- pendientes %>% 
#   select(-organismo,-email_oficial)
# 
# pend_penal <- pendientes %>% 
#   filter(fuero == "Penal") %>% 
#   select(-fuero)
# pend_civil <- pendientes %>% 
#   filter(fuero == "Civil y Comercial") %>% 
#   select(-fuero)
# pend_laboral <- pendientes %>% 
#   filter(fuero == "Laboral") %>% 
#   select(-fuero)
# 
# periodos_2017 <- pend %>%
#   filter(grepl("-17", pend$periodo_vencido))
# 
# View(periodos_2017)
