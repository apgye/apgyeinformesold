start_date <- "2018-05-01"
end_date <- "2018-06-01"
poblacion <- "jdofam0001col"


resultado <- DB_PROD() %>% 
  apgyeTableData("CADR1C") %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep == poblacion) %>% 
  collect()


navenc2 <- is.na(resultado$fvenc2) 
no_mmp <- resultado$tres != "0"


start_date <- "2018-12-01"
end_date <- "2019-01-01"
poblacion <- "jdofam0100pna"


resultado <- DB_PROD() %>% 
  apgyeTableData("CADR1C") %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep == poblacion) %>% 
  mutate(fres = dmy(fres), fvenc2 = dmy(fvenc2)) %>% 
  collect()  
  

no_mmp <- resultado$tres != "0"
navenc2 <- is.na(resultado$fvenc2) 

NA > as.Date("2018-12-03")


sum(resultado$fres[no_mmp & navenc2] > resultado$fvenc2[no_mmp & navenc2], na.rm = T)





 
