---
title: Causas en Trámite
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Causas en trámite `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestran las causas en trámite totales y las causas en trámite activas por organismo. Las causas en trámite activas son aquellas que presetaron al menos un movimiento procesal en los últimos dos años.

```{r}
entramite(poblacion = jdos_paz, operacion_inic = "CINC1C") %>%
  enTramite_gral() %>%
  outputTable(caption = "Causas en Trámite Totales y Causas en Trámite Activas)",
                row_group_label_position = "identity")

```
