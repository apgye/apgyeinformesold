start_date = "2019-02-01"
end_date = "2019-05-01"

# Prueba Camara Civil Sala II
resultado <- DB_PROD() %>% 
  apgyeTableData("CADR2C") %>% 
  apgyeDSL::interval("2019-02-01", "2019-05-01") %>% 
  filter(iep == "camcco0202pna") %>% 
  mutate(fdesp1 = dmy(fdesp1)) %>% 
  collect() %>% 
  mutate(mes = month(fdesp1)) %>% 
  group_by(iep, mes) %>% 
  summarise(cantidad = n())

resultado_sumario <- DB_PROD() %>% 
  apgyeTableData("CADR2C") %>% 
  apgyeDSL::interval("2019-02-01", "2019-05-01") %>% 
  filter(iep == "camcco0202pna") %>% 
  mutate(fdesp1 = dmy(fdesp1)) %>% 
  mutate(mes = date_part('month', fdesp1)) %>% 
  group_by(iep, mes) %>% 
  summarise(cantidad = n()) %>% 
  collect() %>% 
  filter(mes >= lubridate::month(start_date), mes < lubridate::month(end_date))
  
resultado_sumario_cco1 <- DB_PROD() %>% 
  apgyeTableData("CADR1C") %>% 
  apgyeDSL::interval("2019-02-01", "2019-05-01") %>% 
  filter(iep == "jdocco0100pna") %>% 
  mutate(fdesp = dmy(fdesp)) %>% 
  mutate(mes = date_part('month', fdesp)) %>% 
  group_by(iep, mes) %>% 
  summarise(cantidad = n()) %>% 
  collect() %>% 
  filter(mes >= lubridate::month(start_date), mes < lubridate::month(end_date))



resultado_sumario_cco1 <- resultado_sumario %>% 
  mutate(mes = lubridate::month(mes, label = T, abbr = F))
