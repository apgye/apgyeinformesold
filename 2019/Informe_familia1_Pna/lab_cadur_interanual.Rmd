---
title: Duracion de Procesos - Comparación Interanual
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Duracion de Procesos en el Fuero Laboral: Comparación Año 2018 - Último Trimestre 2017

La duración de los procesos es una medición importante de la actividad judicial con un gran impacto en la valoración social del servicio de justicia. Por esa razón, y para favorecer el análisis de las duraciones, expondremos a continuación la metodología empleada para este resumen comparativo:

+ Los tipos de proceso incluidos en la tabla se obtuvieron seleccionando a los procesos resueltos por sentencia en cada organismo con materia laboral. Para el año 2017 se trabajó con una cantidad acotada de casos resueltos entre octubre y diciembre debido a que el nuevo Sistema Estadístico del STJER se puso a prueba en Paraná a partir 01/10/2017. Para el año 2018 se trabajó con los procesos resueltos a partir de abril, fecha de entrada en vigencia en todas las circunscripciones judiciales (nótese que estos casos son un subconjunto de los procesos resueltos totales).        
+ Los tipos de proceso excluidos de este análisis son los siguientes: Incidentes, Cautelares, Ejecutivos, Procesos Constitucionales, Beneficios de Litigar Sin Gastos, Homologaciones, Preparación de Vías, y tipos no acordes a la legislación vigente. Muchos de estos procesos tiene trámites especiales que ameritan ser tratados por separado en ulteriores estudios. Para consolidar la información se practicaron reimputación de tipos de procesos con errores de registración.   
+ La duración de los procesos resueltos a través del dictado de sentencia definitiva se calcula como la diferencia en días corridos entre la fecha de inicio de la causa y la fecha del dictado de la sentencia según declaración del organismo.    
+ Finalmente, dado que el fin de este informe es proveer una visión general, para evitar datos sesgados por valores extremos se optó por la mediana (en lugar de la media) para el cálculo de duraciones, y se excluyeron aquellos tipos de proceso con menos de *10 casos*. Aquí también podría profundizarse este análisis considerando especialmente los valores extremos y su correlación con otras variables (e.g. otros datos de la causa, datos del organismo, etc).   


### Tabla de Tipos de Proceso, sus Duraciones y la cantidad de Casos Considerados

```{r, echo=FALSE, message=F, warning=FALSE}
durac_interanual(jdos_lab, operacion = "CADR1L", materia = "laboral", 
                 start_date, end_date) 

durac_interanual %>% 
  kable(caption = "Tabla de Casos", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) 
```

\pagebreak

### Tabla de Varición Interanual

```{r, echo=FALSE, message=F, warning=FALSE}
durac_interanual_tabla %>% 
  kable(caption = "Tabla de Variación Interanual", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  column_spec(4, "2cm") %>% column_spec(5, "2cm")
```

\pagebreak

### Gráfico Comparativo

```{r, echo=FALSE, message=F, warning=FALSE}
grafico 
```



