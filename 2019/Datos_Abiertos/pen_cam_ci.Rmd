---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Causas Iniciadas en Cámara de Casación- `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciadas_pen(poblacion = cam_pen,
              start_date = start_date,
              end_date = end_date) %>%
  outputTable(caption = "Causas Iniciadas en Casación") %>%
  row_spec(0, angle = 90) %>% 
  #column_spec(3, "12cm") %>%  
  # column_spec(9, border_right = T) %>% 
  landscape()
  
```

