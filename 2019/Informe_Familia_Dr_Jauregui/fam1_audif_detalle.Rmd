---
title: Audiencias - Estados, resultados y tiempos en audiencia
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Audiencias según sus distintos estados finales - `r getDataIntervalStr(start_date, end_date)`

Según definición del STJER sobre este indicador los estados finales de audiencia son los siguientes con su correspondiente definición:

+ 1 = Audiencia Fijada: es la audiencia fijada y notificada en una causa o proceso;  
+ 2 = Audiencia Realizada: audiencia efectivamente celebrada;   
+ 3 = Audiencia Fracasada por incomparencia de partes;  
+ 4 = Audiencia Fracasada por incomparencia terceros (ej. COPNAF)  
+ 5 = Audiencia Cancelada: audiencia que estaba fijada y se dejó sin efecto a pedido de parte.  
+ 6 = Audiencia No Realizada por falta de notificación.   

```{r}
audifam_prim(jdos_fam, 
             start_date, 
             end_date) %>% 
  audifam_xestado(desagregacion_mensual = T) %>% 
  select(-fijada) %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```

## Audiencias Realizadas y sus Resultados

En la siguiente tabla se muestran las audiencias realizadas y sus resultados por organismo.   

Los resultados posibles según definición institucional son:

En procesos con contradicción:

+ Conciliación Total  
+ Conciliación Parcial   
+ Sin conciliación y sin Apelación  
+ Sin conciliación y con Apelación  

Además de los anteriores se registra un tipo especial de conciliación en procesos sin contradicción que aparece discriminado. 

```{r}
audifam_prim(jdos_fam, 
             start_date, 
             end_date) %>% 
  audifam_realizadas_xresultado(desagregacion_mensual = T) %>% 
  outputTable(caption = "Audiencias Realizadas y sus Resultados") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```

## Audiencias realizadas por Tipo

En esta tabla se presenta información de las audiencias realizadas para los distintos tipos de audiencia. Para las audiencias relativas a Violencia Familiar y de Genero los datos se informan hasta octubre, fecha a partir de la cual el registro asociado a estos procesos se concentra en el Centro Judicial de Genero conforme Res.22/10/18 (se está trabajando en la integración de dicha información).


```{r}
audifam_prim(jdos_fam, 
             start_date, 
             end_date) %>% 
  audifam_realizadas_xtipo(desagregacion_mensual = T) %>% 
  mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Audiencias Realizadas por Tipo") %>%
  column_spec(2, "3cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape()
```


