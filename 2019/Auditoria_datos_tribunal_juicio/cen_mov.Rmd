---
title: Movimientos en Legajos de Mediadores
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Movimientos en Legajos de Mediadores - `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se muestran la cantidad de legajos que tuvieron al menos un movimiento procesal en el mes. 

```{r}
movimientos_cen(cen, 
         start_date = start_date,
         end_date = end_date) %>% 
  outputTable(caption = "Movimientos") %>% 
  column_spec(4, "3cm") %>% column_spec(5, "3cm") %>% column_spec(6, "3cm")

```



