---
title: Resolucioens por Tipo 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}

res_list <- resoluciones_cad1_xtres(cam_cad,
                        operacion = "CADRCAD",
                        start_date = start_date,
                        end_date = end_date)


```

## Resoluciones por Tipo `r getDataIntervalStr(start_date, end_date)`

```{r}
res_list$res_xtipo %>%
  select(-circunscripcion) %>%
  outputTable(caption = "Sentencias por Tipo") %>%
  row_spec(0, angle = 90)
```

\pagebreak

## Tipos de Sentencias Dictadas

```{r}
res_list$res_xtipo_S
```

\pagebreak

## Resoluciones por Objeto de Demanda `r getDataIntervalStr(start_date, end_date)`

```{r}
res_list$res_xobjetodem %>%
  select(-circunscripcion) %>%
  outputTable(caption = "Resoluciones por Objeto de Demanda") %>%
  row_spec(0, angle = 90)
```

\pagebreak

## Objetos de Demanda

```{r}
res_list$res_xobjetodem_g
```

\pagebreak

## Resoluciones por Demandado y Resultado `r getDataIntervalStr(start_date, end_date)`

```{r}
res_list$tdemo_xtres %>%
  select(-circunscripcion) %>% 
  outputTable(caption = "Demandado y Resultado") %>%
  #column_spec(1, "3cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```

\pagebreak

## Resoluciones en Recursos  `r getDataIntervalStr(start_date, end_date)`

```{r}
res_list$recurso %>%
  select(-circunscripcion, -Sin_dato, -Total) %>% 
  outputTable(caption = "Resoluciones en Recursos") %>%
  row_spec(0, angle = 90)
```


## Sentencias Dictadas y Recursos  `r getDataIntervalStr(start_date, end_date)`

```{r}
res_list$sentencias_recurso %>%
  outputTable(caption = "Sentencias y Recursos") %>%
  row_spec(0, angle = 90)
```




