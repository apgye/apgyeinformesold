---
title: "Pre-Informe de Oralidad: Audiencias de Diciembre"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
options(knitr.kable.NA = '')
```

```{r}
source("informe_diciembre.R")
source("utils.R")
```


```{r}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tibble)
library(lubridate)
```

# Meses Informados: `r getDataIntervalStr(start_date, end_date)`


En este pre-informe se puede consultar los datos consolidados de las Audiencias Preliminares y de Vista de Causa realizadas por los organismos incluidos en el Proyecto de Oralidad.

Solicitamos la revisión de estos datos para controlar su conformidad con la efectuado por cada organismo. En caso de divergencia toda corrección debe realizarse a través de la modificación correspondiente del listado de audiencias suministrada vía sistema JUSTAT. 

Las correcciones deben realizarse hasta el día **14/02/19 a las 13 hs.**.

Recordamos que: **cuartos intermedios** para las audiencias que se realizan en más de un encuentro o bloque (suceda en el mismo día o en días distintos) se debe realizar SIEMPRE UN SOLO REGISTRO, conteniendo toda la información de duración (como la suma de los minutos y bloques que tuvo la audiencia), su resultado y demás datos establecidos en la Guía.De esta manera, por ejemplo, una audiencia preliminar realizada en tres encuentros (todos en distintos días, ej: 1/10/18, 20/10/18 y el 25/10/18) debe informarse a través de un solo registro completando su fecha de realización aquella fecha del último encuentro (25/10/18), su duración en minutos como la suma de las duraciones de cada encuentro, sus bloques como la cantidad de encuentros que demandó la audiencias (en el ejemplo: 3) y su resultado como el resultado final alcanzado. Esta forma de registración mantiene la unidad de información y simplifica la registración. 

Saludo a usted cordialmente y quedo a su disposición, 

<!-- Lic. Sebastián Castillo -->
Lic. Marcos Londero

Técnico Informático - APGE - STJER

\pagebreak

## Órganos con Presentaciones Pendientes


```{r}
kable(organismo_sin_informar, caption = "Presentaciones Pendientes", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0) 
```

\pagebreak

## PRELIMINARES  `r getDataIntervalStr(start_date, end_date)`


```{r}
audic_prelim_raw %>% select(-caratula, -tproc) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                           "circunscripcion")], by = c("iep" = "organismo")) %>% 
  select(circunscripcion, organismo_descripcion, everything(), -iep) %>% 
  arrange(circunscripcion, organismo_descripcion) %>% 
  kable(caption = "Audiencias Preliminares", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>%
  landscape() 
```


\pagebreak

## VISTAS DE CAUSA `r getDataIntervalStr(start_date, end_date)`

```{r}

audic_vcausa_raw %>% select(-caratula, -tproc) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                           "circunscripcion")], by = c("iep" = "organismo")) %>% 
  select(circunscripcion, organismo_descripcion, everything(), -iep) %>% 
  arrange(circunscripcion, organismo_descripcion) %>%
  kable(caption = "Audiencias Vista Causa", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>%
  landscape() 
```



