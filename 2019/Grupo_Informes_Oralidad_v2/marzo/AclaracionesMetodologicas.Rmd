---
title: "Aclaraciones Metodológicas"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
options(knitr.kable.NA = '')
```

```{r}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tibble)
```


# Aclaraciones Metodológicas

La información procesada para este informe surge de listados primarios de datos presentados por cada organismo de manera digital.

Las categorías de audiencia contempladas en el sistema estadístico del STJER son:

+ Audiencia Fijada: audiencia fijada;  
+ Audiencia Realizada: audiencia efectivamente celebrada;  
+ Audiencia No Realizada: audiencias que no se realiza por incomparencia de parte;  

En estos momentos se está revisando la clasificación para mejorar el relevamiento. 

Para los casos de Audiencia en procesos con contradicción se clasifican sus resultados de la siguiente manera:

+ Conciliación Total     
+ Conciliación Parcial    
+ Sin Conciliación    

A los fines del procesamiento de datos, y según la definición vigente para el Ministerio de Justicia y DDHH de la Nación, la categoría de *Conciliación Parcial* queda subsumida bajo la categoría *Sin Conciliación* atendiendo a la contiuación del proceso.   

Asimismo, y según solicitud del Dr. Martin Alfandari se reimputaron todos los casos con conciliación total (tanto en audiencia preliminar como vista de causa) como casos con **Conciliación** informada en la columna: *forma_deresolución*.    

Se presentan indicadores de Satisfaccion de Usuarios y Abogados en Audiencia, según las variables y procedimientos fijados por el documento remitido por el Ministerio en fecha 16/07/18 denominado **Encuestas de satisfacción – Instructivo y formularios – Marzo 2018**. 

En archivos adjuntos se envían los datos primarios procesados para la presentación. También agregamos, conforme lo realizado en las primeras oportunidades, el archivo de procesamiento estadístico en lenguaje libre R, para una completa reproducibilidad de los resultados que se muestran en el Informe de Avance. 

En el archivo denominado **oralidad_consolidado_x** se encuentran las siguientes variables con su correspondiente significado:

```{r}

oralidad_consolidado <- readr::read_csv("~/apgyeinformes/2019/Grupo_Informes_Oralidad/data/oralidad_consolidado_febrero.csv") %>% 
  select(-iep, -as)

variables_informadas <- as.data.frame(noquote(names(oralidad_consolidado))) %>% 
  mutate(descripcion = c(
    "circunscripcion del organimo",
    "organismo descripcion denominación",
    "número del organismo", 
    "número de la causa asignado por el organismo", 
    "caratula con disosciación de nombres de personas - solo iniciales",
    "tipo de proceso registrado por el organismo", 
    "fecha de inicio de la causa en el organismo", 
    "fecha de efectivización de audiencia preliminar", 
    "estado de la audiencia preliminar: 'realizada' o 'no realizada'", 
    "resultado de la audiencia preliminar: 'Conciliación (T)otal' o '(S)in Conciliacion'",
    "fecha de apertura a prueba",
    "fecha programada para la realización de la Audic. Vista de Causa",
    "fecha de efectivización de la Audic. Vista de Causa",
    "estado de la audiencia vista de causa: 'realizada' o 'no realizada'",
    "resultado de la audiencia vista de causa: 'Conciliación (T)otal' o '(S)in Conciliacion'",
    "fecha de puesta a despacho para resolver", 
    "fecha de resolución", 
    "forma de resolucion de causa: Sentencia Definitiva, Conciliación, Transacción, otros.",
    "sinVC_PrInfDoc: proceso sin Audiencia de Vista de Causa por contener prueba Informativa/Documental")) %>% 
  rename(variables = 1)

kable(variables_informadas, caption = "Variables y Descripción", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>%
  landscape() 

```



Lic. Sebastián Castillo
Director