#utils
# Listado de Magistrados y Funcionarios
magistrados_y_secretarios <- DB_PROD() %>% apgyeDSL::apgyeTableData('personal_planta_ocupada') %>%
  left_join(DB_PROD() %>% apgyeDSL::apgyeTableData('personal_personas') %>%
              select(idagente, categoria, apellido, nombres), by=c("idagente")
  ) %>%
  filter(grepl("JUEZ|SECRETARIO|VOCAL|FISCAL|DEFENSOR", categoria)) %>%
  collect()
magist_func_id_agentes <- magistrados_y_secretarios$idagente
# add "0" ti IDs for External Agent
magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"


newenv <- function(...) {
  envinitialization <- rlang::enexprs(...)
  env = new.env()
  for(var in names(envinitialization)) {
    env[[var]] = eval(envinitialization[[var]])
  }
  env
}

outputTable <- function (table, caption, row_group_label_position = "identity") {
  if(row_group_label_position == "identity") {
    total_rows <-  c(which(table[, 1] == "Total"), which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  } else {
    total_rows <-  c(which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  }
  table %>% rename_all(.funs = stringr::str_replace_all, pattern = "_", replacement=" ") %>% 
    kable("latex", caption = caption,
          align = 'c', longtable = TRUE, booktabs = T ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), latex_options = c("repeat_header"),
                  full_width = F, font_size = 10) %>% 
    row_spec(total_rows, bold = T) %>% 
    collapse_rows(columns = 1:2, row_group_label_position = row_group_label_position)
  
}

getDataIntervalStr <- function(data_interval_start, data_interval_end){
  year <- lubridate::year(data_interval_start);
  year_end <- lubridate::year(data_interval_end);
  month <- lubridate::month(data_interval_start);
  month_end <- lubridate::month(lubridate::as_date(data_interval_end)  - lubridate::days(1));
  monthDiff <- lubridate::interval(data_interval_start %>% as.Date(), data_interval_end %>% as.Date()) %/% months(1)
  period <- format(lubridate::make_date(year, month, 1), '%B %Y')
  
  period[monthDiff == 12] <- paste("anual", year[monthDiff == 12])
  period[monthDiff < 12] <- paste(format(lubridate::make_date(year, month, 1), '%B'), '-', format(lubridate::make_date(year, month_end, 1), '%B'), year[monthDiff < 12])
  period[monthDiff > 12] <- paste(format(lubridate::make_date(year, month, 1), '%B %Y'), '-', format(lubridate::make_date(year_end, month_end, 1), '%B %Y'))
  
  period[monthDiff == 6 & month == 1] <- paste("primer semestre", year[monthDiff == 6 & month == 1])
  period[monthDiff == 6 & month == 7] <- paste("segundo semestre", year[monthDiff == 6 & month == 7])
  period[monthDiff == 1] <- paste(format(lubridate::make_date(year[monthDiff == 1], month[monthDiff == 1], 1), '%B %Y'))
  
  
  period
}

entidades <- apgyeJusEROrganization::listar_organismos()


sin_informar <- function(){
  
  presentaciones_justat <- DB_PROD() %>% tbl("submission") %>% select(-input) %>% collect()
  
  base_computo <<- presentaciones_justat %>% 
    filter(data_interval_start == as.Date(start_date), 
           iep %in% jdos_civiles$organismo,
           str_detect(operation, "CINC1C|AUDIC|SAT"), 
           enabled == T) %>% 
    group_by(iep, operation) %>% 
    summarise(presentaciones = n()) %>% 
    left_join(apgyeJusEROrganization::OPERATION_DESCRIPTION, 
              by = c("operation" = "operacion")) %>% 
    left_join(entidades[, c("organismo", "organismo_descripcion", 
                            "circunscripcion")], 
              by = c("iep" = "organismo")) %>% 
    ungroup() %>% 
    select(circunscripcion, iep, organismo_descripcion, operacion_descripcion, presentaciones) %>% 
    arrange(desc(circunscripcion)) %>% 
    janitor::adorn_totals("row")
  
  
  organismo_sin_informar <-jdos_civiles[!jdos_civiles$organismo %in% 
                                                    unique(base_computo$iep), ] 
  
  organismo_sin_informar
  
}



consolidar <- function(df_casos_base, audic_prelim_raw, audic_vcausa_raw) {
  
  # 1 Casos incluidos y preliminares-------------------------------------------
  
  # A los casos consolidados en octubre se agregan las fijadas y realizadas en nuevo período (ej.noviembre)
  # Se deben consolidar los casos pues hay duplicados. Cuando sea así se resuelve:
  # agrupar por (iep, nro) + filtrar descartando fecha audiencia preliminar de fecha menor
  
  consolidado1 <- df_casos_base %>% 
    bind_rows(audic_prelim_raw) %>% 
    group_by(iep, nro) %>% 
    slice(which.max(!is.na(fecha_efect_preliminar))) %>% # esto resuelve la llegada de información diferida
                                                          # pues primero ingresa como informada y luego como 
                                                          # realizada siempre quedando el dato de la última fecha
    ungroup()
  
  # 2 Audiencias Vista de Causa-------------------------------------------------
  
  # tengo que actualizar oralidad con nueva informacion en 
  # casos ya informados +
  # incorporacion nuevos casos
  # 2.1.
  # 1 crear temporal con los casos que estan en oralidad y tienen match en vcraw (semi_join)
  # 2 completarlo con los datos de vcausa [elimiando columnas vcausa] temporal (left_join)
  # 3 volver a aunir
  # 2.2. 
  # 1 determar que casos estan en vcraw sin match en oralidad.
  # 2. juntar esos casos con oralidad (bind_row)
  
  temp_vc_conmatch <- consolidado1 %>% 
    semi_join(audic_vcausa_raw, by = c( "iep", "nro")) %>% 
    select(-fecha_efect_vcausa, -resultado_vcausa, -estado_vcausa) %>% 
    left_join(audic_vcausa_raw %>% select(-caratula, -tproc, -fecha_ingreso),
              by = c( "iep", "nro"))
  
  temp_vc_simatch <- audic_vcausa_raw %>% 
    anti_join(consolidado1, by = c( "iep", "nro"))
  
  consolidado1 <- consolidado1 %>% 
    anti_join(audic_vcausa_raw, by = c( "iep", "nro")) %>%
    bind_rows(temp_vc_conmatch) %>% 
    bind_rows(temp_vc_simatch) 
  
  # 3 Resoluciones-----------------------------------------------------------
  
  temp_res_conmatch <- consolidado1 %>% 
    # si un organismo concilió total un caso y luego lo puso a sentencia
    # aparecerá informado dos veces: como conciliación total y como sentencia conciliacion
    semi_join(resoluciones, by = c( "iep", "nro")) 
  
  temp_res_conmatch_ya_informado <- temp_res_conmatch %>% 
    filter(!is.na(temp_res_conmatch$forma_deresolucion))
  
  if(nrow(temp_res_conmatch_ya_informado) != 0) {
    temp_res_conmatch <- temp_res_conmatch %>% 
      filter(is.na(fecha_resolucion)) %>% 
      select(-fecha_despacho, -fecha_resolucion, -as, -forma_deresolucion) %>% 
      left_join(resoluciones %>% select(-caratula, -tproc, -mes, -codigo_juez),
                by = c( "iep", "nro")) %>% 
      rename(fecha_despacho = fdesp, fecha_resolucion = fres, forma_deresolucion = tres) %>% 
      bind_rows(temp_res_conmatch_ya_informado)
    temp_res_conmatch
  } else{
    temp_res_conmatch <- temp_res_conmatch %>% 
      select(-fecha_despacho, -fecha_resolucion, -as, -forma_deresolucion) %>% 
      left_join(resoluciones %>% select(-caratula, -tproc, -mes, -codigo_juez),
                by = c( "iep", "nro")) %>% 
      rename(fecha_despacho = fdesp, fecha_resolucion = fres, forma_deresolucion = tres)
    temp_res_conmatch
  }
  
  # No procede un "temp_res_sinmatch" pues las sentencias dictadas en casos 
  # donde no se informó preliminar, vista de causa o no integro df_1ros_casos
  # no se informan en oralidad
  
  consolidado <- consolidado1 %>% 
    anti_join(resoluciones, by = c( "iep", "nro")) %>%
    bind_rows(temp_res_conmatch) 
  
  consolidado
  
}









