circunscripcion	organismo_descripcion	mes	nro	fecha_efect_preliminar	fprogramada_vista_causa	oralidad	motivos_no_prog
Paran�	Jdo Civ y Com 5	julio	29713	05/07/2018	sin_dato	NO	La audiencia preliminar fue fijada con anterioridad a la implementaci�n del Reglamento (3� causal)
Paran�	Jdo Civ y Com 8	agosto	19084	02/08/2018	sin_dato	NO	Expte no alcanzado por Protocolo Oralidad
Paran�	Jdo Civ y Com 8	agosto	19219	23/08/2018	sin_dato	SI	En la AP se orden� solamente la producci�n de prueba instrumental e informativa
Paran�	Jdo Civ y Com 8	agosto	19397	07/08/2018	sin_dato	NO	Expte no alcanzado por Protocolo Oralidad
Uruguay	Jdo Civ y Com 2	agosto	8724	02/08/2018	sin_dato	Si	2/8 se suspendio hasta el 8/8 donde se produjo conciliacion
Uruguay	Jdo Civ y Com 2	agosto	8323	14/08/2018	sin_dato	Si	Se declar� la cuesti�n de puro derecho
Uruguay	Jdo Civ y Com 2	agosto	8614	09/08/2018	sin_dato	Si	Se postergo AVC para 23/8 por pedido de las partes por posibilidad de arribar a un acuerdo
Uruguay	Jdo Civ y Com 2	agosto	8818	08/08/2018	sin_dato	Si	Solo prueba documental e informativa. No se fijo AVC
Uruguay	Jdo Civ y Com 2	octubre	7765	10/10/2018	sin_dato	Si	A pedido de parte se suspendio hasta 18/10 por posibilidad de arribar a acuerdo extrajudicial
Uruguay	Jdo Civ y Com 2	octubre	8511	30/10/2018	sin_dato	Si	Prueba documental e instrumental. No se fijo AVC
Uruguay	Jdo Civ y Com 2	octubre	8169	23/10/2018	sin_dato	Si	AVC fijada para 19/2/19. Error de carga al registrar. Corregido
Uruguay	Jdo Civ y Com 2	septiembre	7765	20/09/2018	sin_dato	Si	Se posterg� preliminar para 10/10
Uruguay	Jdo Civ y Com 2	septiembre	8775	18/09/2018	sin_dato	Si	Solo prueba documental e informativa
Uruguay	Jdo Civ y Com 2	septiembre	8088	12/09/2018	sin_dato	Si	Se supedit� la fijaci�n de AVC al resultado de prueba gen�tica
Paran�	Jdo Civ y Com 7	agosto	19463	14/08/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	agosto	19463	14/08/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	agosto	19463	14/08/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	agosto	19775	07/08/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	agosto	19413	02/08/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	agosto	19846	24/08/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	agosto	19944	16/08/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	julio	19685	26/07/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Paran�	Jdo Civ y Com 7	julio	19544	03/07/2018	sin_dato	no	respuesta enviada a Vramirez Amable
Nogoy�	Jdo Civ y Com 1 -ccomp.Laboral	agosto	8341	31/08/2018	sin_dato	si	no tiene fijada fecha para la vista de causa, porque la �nica prueba ordenada fue la constataci�n de un inmueble
Nogoy�	Jdo Civ y Com 1 -ccomp.Laboral	octubre	8919	02/10/2018	sin_dato	si	no se fij� la misma porque la prueba dispuesta fue solamente documental e instrumental
Nogoy�	Jdo Civ y Com 1 -ccomp.Laboral	septiembre	8816	25/09/2018	sin_dato	si	no se fij� la misma porque la prueba dispuesta fue solamente documental e instrumental
Gualeguaych�	Jdo Civ y Com 2	julio	11380	26/07/2018	sin_dato	NO	Preliminar celebrada antes de la implementaci�n de la oralidad.
Gualeguaych�	Jdo Civ y Com 2	octubre	11360	25/10/2018	sin_dato	SI	AVC fijada para el d�a 06/02/2019.
Gualeguaych�	Jdo Civ y Com 2	septiembre	11392	06/09/2018	sin_dato	SI	Prueba prove�da: documental, informativa e instrumental �nicamente.
Gualeguaych�	Jdo Civ y Com 3	octubre	6745	10/10/2018	sin_dato	si	Cuarto intermedio a pedido de partes � Acompa�aron acuerdo el que se homolog� el 07/11/18
Gualeguaych�	Jdo Civ y Com 3	octubre	7148	24/10/2018	sin_dato	si	Cuarto intermedio a pedido de parte � Sin acuerdo, se abri� a prueba el 06/11, AVC para el 18/12/18
Gualeguaych�	Jdo Civ y Com 3	octubre	7132	31/10/2018	sin_dato	si	Cuarto intermedio a pedido de parte, plazo excedido se pidi� aclaraci�n a las partes sobre continuidad del tr�mite, respuesta pendiente
Gualeguaych�	Jdo Civ y Com 3	septiembre	6434	11/09/2018	sin_dato	no	Hubo que reprogramar fecha preliminar por fallecimiento del demandado
Gualeguaych�	Jdo Civ y Com 3	septiembre	6914	25/09/2018	sin_dato	si	Cuarto intermedio a pedido de partes � Sin acuerdo, se abri� a prueba el 26/11/18. AVC para el 26/02/19
Nogoy�	Jdo Civ y Com 2 -ccomp.Laboral	julio	5133	31/07/2018	sin_dato	SI	error de carga
Nogoy�	Jdo Civ y Com 2 -ccomp.Laboral	julio	5283	25/07/2018	sin_dato	SI	error de carga
Federal	Jdo Civ y Com Lab	agosto	18663	09/08/2018	sin_dato	no	18663:  Por incontestaci�n de la demanda y la ausencia de hechos controvertidos, con la conformidad de la parte actora, se consider� innecesario abrir a pruebas el proceso en atenci�n a que la prueba ya estaba agregada y no fue cuestionada por la parte demandada por lo que se concluy� el proceso. Fecha de sentencia:10/10/2018
Federal	Jdo Civ y Com Lab	septiembre	18767	27/09/2018	sin_dato	no	18767: Por incontestaci�n de la demanda por parte de los codemandados y la ausencia de pruebas pendientes de producci�n, con la conformidad de las partes, se consider� innecesario abrir a pruebas el proceso en atenci�n a que la prueba consist�a en las constancias que ya estaban agregadas y no cuestionadas por la parte contraria, renunciando al derecho de alegar en la preliminar por lo que correspondi� concluir el proceso y llamar autos para sentencia de acuerdo con lo previsto por el art�culo 348 del CPCC. Fecha desentencia: 11/10/2018.
Concordia	Jdo Civ y Com 4	agosto	9093	09/08/2018	sin_dato	NO	NO ALCANZADO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	agosto	9323	23/08/2018	sin_dato	SI	SE ENCONTRABA REGISTRADA LA FECHAPREVISTA PARA LA AVC
Concordia	Jdo Civ y Com 4	agosto	9535	16/08/2018	sin_dato	NO	NO ALCANZADO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	agosto	9500	21/08/2018	sin_dato	NO	NO ALCANZADO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	agosto	8590	15/08/2018	sin_dato	NO 	NO ALCANZAO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	agosto	8590	10/08/2018	sin_dato	NO 	NO ALCANZADO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	agosto	9573	02/08/2018	sin_dato	NO	NO ALCANZADO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	agosto	9083	07/08/2018	sin_dato	NO	NO ALCANZADO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	julio	8714	31/07/2018	sin_dato	NO	NO ALCANZADO POR PROTOCOLO
Concordia	Jdo Civ y Com 4	octubre	9544	04/10/2018	sin_dato	SI 	ERROR DE CARGA DE LA FECHA PREVISTA PARA LA AVC
Concordia	Jdo Civ y Com 4	octubre	9057	11/10/2018	sin_dato	SI	ERROR DE CARGA DE LA FECHA PREVISTA PARA LA AVC
Concordia	Jdo Civ y Com 4	octubre	9597	09/10/2018	sin_dato	SI	ERROR DE CARGA DE LA FECHA PREVISTA PARA LA AVC
Concordia	Jdo Civ y Com 4	octubre	9291	16/10/2018	sin_dato	SI	ERROR DE CARGA DE LA FECHA PREVISTA PARA LA AVC
Concordia	Jdo Civ y Com 4	septiembre	9463	27/09/2018	sin_dato	SI	ERROR DE CARGA DE LA FECHA PREVISTA PARA LA AVC
Tala	Jdo Civ y Com -ccomp.Laboral	septiembre	8529	18/09/2018	sin_dato	SI	SOLO PRUEBA INFORMATIVA
Col�n	Jdo Civ y Com 2 -ccomp.Laboral	agosto	13569	08/08/2018	sin_dato	NO	FIJADA ANTES DE VIGENCIA ORALIDAD
Col�n	Jdo Civ y Com 2 -ccomp.Laboral	agosto	13343	01/08/2018	sin_dato	NO	FIJADA ANTES DE VIGENCIA ORALIDAD
Col�n	Jdo Civ y Com 2 -ccomp.Laboral	agosto	13141	22/08/2018	sin_dato	SI	SE DECLARA CUESTION DE PURO DERECHO
Col�n	Jdo Civ y Com 2 -ccomp.Laboral	agosto	13396	07/08/2018	sin_dato	NO	FIJADA ANTES DE VIGENCIA ORALIDAD
Col�n	Jdo Civ y Com 2 -ccomp.Laboral	julio	12164	24/07/2018	sin_dato	NO	FIJADA ANTES DE VIGENCIA ORALIDAD
Col�n	Jdo Civ y Com 2 -ccomp.Laboral	septiembre	12272	05/09/2018	sin_dato	SI	OMISI�N DE CARGA FPROG
Concordia	Jdo Civ y Com 1	agosto	12262	02/08/2018	sin_dato	NO	Fecha anterior
Concordia	Jdo Civ y Com 1	agosto	12039	01/08/2018	sin_dato	No	No se celebr� audiencia
Concordia	Jdo Civ y Com 1	agosto	11780	07/08/2018	sin_dato	no	Fecha anterior
Concordia	Jdo Civ y Com 1	agosto	12204	09/08/2018	sin_dato	no	Fecha anterior
Concordia	Jdo Civ y Com 1	julio	12127	03/07/2018	sin_dato	no	Fecha anterior
Concordia	Jdo Civ y Com 1	julio	11744	31/07/2018	sin_dato	no	Fecha anterior
