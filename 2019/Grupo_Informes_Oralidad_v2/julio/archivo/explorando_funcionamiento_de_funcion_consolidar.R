# 1 Casos incluidos y preliminares-------------------------------------------

# A los casos consolidados en octubre se agregan las fijadas y realizadas en nuevo período (ej.noviembre)
# Se deben consolidar los casos pues hay duplicados. Cuando sea así se resuelve:
# agrupar por (iep, nro) + filtrar descartando fecha audiencia preliminar de fecha menor

# consolidado1 <- df_casos_base %>% 
#   bind_rows(audic_prelim_raw) %>% 
#   group_by(iep, nro) %>% 
#   slice(which.max(!is.na(fecha_efect_preliminar))) %>% # esto resuelve la llegada de información diferida
#   # pues primero ingresa como informada y luego como 
#   # realizada siempre quedando el dato de la última fecha
#   ungroup()

# 1.1. nuvo consolidado1

consolidado1 <- df_casos_base %>% 
  bind_rows(audic_prelim_raw)


# 2 Audiencias Vista de Causa-------------------------------------------------

# tengo que actualizar oralidad con nueva informacion en 
# casos ya informados +
# incorporacion nuevos casos
# 2.1.
# 1 crear temporal con los casos que estan en oralidad y tienen match en vcraw (semi_join)
# 2 completarlo con los datos de vcausa [elimiando columnas vcausa] temporal (left_join)
# 3 volver a aunir
# 2.2. 
# 1 determar que casos estan en vcraw sin match en oralidad.
# 2. juntar esos casos con oralidad (bind_row)

temp_vc_conmatch <- consolidado1 %>% 
  semi_join(audic_vcausa_raw, by = c( "iep", "nro")) %>% 
  select(-fecha_efect_vcausa, -resultado_vcausa, -estado_vcausa) %>% 
  left_join(audic_vcausa_raw %>% select(-caratula, -tproc, -fecha_ingreso),
            by = c( "iep", "nro"))

temp_vc_simatch <- audic_vcausa_raw %>% 
  anti_join(consolidado1, by = c( "iep", "nro"))

consolidado1 <- consolidado1 %>% 
  anti_join(audic_vcausa_raw, by = c( "iep", "nro")) %>%
  bind_rows(temp_vc_conmatch) %>% 
  bind_rows(temp_vc_simatch) 

# 3 Resoluciones-----------------------------------------------------------

temp_res_conmatch <- consolidado1 %>% 
  # si un organismo concilió total un caso y luego lo puso a sentencia
  # aparecerá informado dos veces: como conciliación total y como sentencia conciliacion
  semi_join(resoluciones, by = c( "iep", "nro")) 

temp_res_conmatch_ya_informado <- temp_res_conmatch %>% 
  filter(!is.na(temp_res_conmatch$forma_deresolucion))

if(nrow(temp_res_conmatch_ya_informado) != 0) {
  temp_res_conmatch <- temp_res_conmatch %>% 
    filter(is.na(fecha_resolucion)) %>% 
    select(-fecha_despacho, -fecha_resolucion, -as, -forma_deresolucion) %>% 
    left_join(resoluciones %>% select(-caratula, -tproc, -mes, -codigo_juez),
              by = c( "iep", "nro")) %>% 
    rename(fecha_despacho = fdesp, fecha_resolucion = fres, forma_deresolucion = tres) %>% 
    bind_rows(temp_res_conmatch_ya_informado)
  temp_res_conmatch
} else{
  temp_res_conmatch <- temp_res_conmatch %>% 
    select(-fecha_despacho, -fecha_resolucion, -as, -forma_deresolucion) %>% 
    left_join(resoluciones %>% select(-caratula, -tproc, -mes, -codigo_juez),
              by = c( "iep", "nro")) %>% 
    rename(fecha_despacho = fdesp, fecha_resolucion = fres, forma_deresolucion = tres)
  temp_res_conmatch
}

# No procede un "temp_res_sinmatch" pues las sentencias dictadas en casos 
# donde no se informó preliminar, vista de causa o no integro df_1ros_casos
# no se informan en oralidad

consolidado <- consolidado1 %>% 
  anti_join(resoluciones, by = c( "iep", "nro")) %>%
  bind_rows(temp_res_conmatch) 

consolidado



# revision---------------------


consolidado1_06 <- consolidado1 %>% 
  filter(iep == "jdocco0600pna")

resoluciones_06 <- resoluciones %>% 
  filter(iep == "jdocco0600pna")

consolidado1_06$nro[consolidado1_06$nro %in% resoluciones_06$nro]

# 15748 resuelto el por S el 2019-02-12






