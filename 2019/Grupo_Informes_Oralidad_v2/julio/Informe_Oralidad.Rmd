---
title: "Informe General sobre Oralidad"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: pdf_document
params:
  consolidado_hasta:
    label: "Consolidado al:"
    input: text
    value: "2019-03-01"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
options(knitr.kable.NA = '')
```

```{r echo=FALSE, include=FALSE}
# parámetros del informe
end_date <- params$consolidado_hasta
```


```{r}
source("informe.R")
```


# Datos de Avance del Proyecto de Oralidad a la fecha: `r end_date`

## Datos Generales del conjunto de procesos sometidos al Protocolo de Oralidad.

En esta tabla se presenta información sobre el conjunto de procesos sometidos al Protocolo de Oralidad e informados al área en los datos presentados de julio a la fecha.

En el computo de cada indicador se empleó el siguiente criterio de conteo:

+ **cantidad procesos en oralidad informados**: casos con audiencia preliminar fijada desde de julio 2018 a la fecha. Estos casos sometidos a oralidad se consolidaron a través de declaración de los organismos.   

+ **cantidad_preliminares_realizadas**: conjunto de casos con audiencia preliminar realizada en el período considerado. Se excluyen del conteo los casos duplicados.   

+ **cantidad_preliminares_no_realizadas**: conjunto de casos con audiencia preliminar no_realizada en el período considerado.   

+ **cantidad_vista_causa_programadas**: conjunto de casos con audiencia de vista de causa programada e informada al momento de celebración de la preliminar. Se excluyen del conteo los casos duplicados.      

+ **cantidad_vista_causa_realizadas**: conjunto de casos con audiencia de vista de causa celebrada a partir de *septiembre 2018*. Se excluyen del conteo los casos duplicados y los casos con multiples registros de audiencia para un mismo mes.    

+ **cantidad_conciliaciones**: conjunto de casos con conciliación total registrada en audiencia preliminar, en audiencia de vista de causa o conciliación presentada y homologada a través de sentencia.         

+ **cantidad_sentencias**: conjunto de casos con sentencia dictada en procesos sometidos al Protocolo de Oralidad que se informaron al área.   


## Actualizaciones/Correcciones    

**2019-06-01**:       

+ Se modificó la denominación de la columna **cantidad_conciliaciones_enAPoAVC** por la denominación **cantidad_conciliaciones** por cuanto en el cálcuo de este indicador se cuentan los casos de conciliación en audiencia tanto como la resolución que homologa un acuerdo de conciliación.   
+ Se subsanó una omisión incorporando dentro de la población relevada al juzgado multifuero de la circunscripción Feliciano.       

**2019-05-17**:      

+ Con el fin de garantizar a nuestros órganos informantes el cómputo actualizado de sus indicadores se modificaron las rutinas de procesamiento de información para incluir: a) datos del mes que se informa, y b) modificaciones/reimputaciones de datos de meses anteriores (con tope al 01/02/2019). Esta variación fue comunicada el martes 14/05/2019 11:27 al grupo de coordinación del Proyecto de Oralidad y el MINJUS.    
+ Asimismo, a partir de las observaciones efectuadas por la Dra. Silvina Rufanatch (JdoCCO6_Pna), a fin de prevenir el doble conteo de casos (comunmente producido por errores de registración en audiencias o bien por la práctica judicial de poner a despacho y homologar por sentencia casos de conciliación total) se ajustó el procesamiento para tener como motivo y fecha de resolución de la causa la fecha de audiencia con conciliación total.    

**2019-08-15**:      

+ Informamos que, mediante solicitud de revisión de datos por parte de la Dra. Rufanacht del Juzgado Civil y Comercial Nº 6 de Paraná en base a diferencias encontradas para sus indicadores, se realizó una auditoría de datos desde éste Área actualizando la información consolidada del año 2018. Las diferencias auditadas fueron dos (2) Audiencias Preliminares de menos Realizadas: Exptes. 16035 y 15879 declaradas en fechas 07/05/2019 y 17/05/2019 y dos (2) Audiencias Vistas de Causa de menos Realizadas: Exptes. 15468 y 15748 declaradas en fechas 07/05/2019 y 17/05/2019. Ambos casos suman a las existentes.

```{r}
kable(resumen, caption = "Resumen", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90)
```

**ATENCION**:    

+ En ANEXO 1 se agrega la tabla completa empleada para el resumen precedente.    

\pagebreak

# Audiencias de Vista de Causa Programadas a partir de `r end_date`

```{r}

oralidad_vista_causa <- consolidado_informesSTJ %>% 
  select(circunscripcion, organismo_abbrev, nro_expte, fprogramada_vista_causa) %>% 
  filter(!is.na(fprogramada_vista_causa) & fprogramada_vista_causa >= end_date) %>% 
  mutate(mes = as.character(lubridate::month(fprogramada_vista_causa)), 
         año = as.character(lubridate::year(fprogramada_vista_causa))) %>%
  group_by(circunscripcion, organismo_abbrev, año, mes) %>% 
  summarise(cantidad_audiencias = n(), 
            nros_expte_y_fechaAudiencia = paste(nro_expte, fprogramada_vista_causa,
                                sep = ": ", collapse = "; ")) %>% 
  ungroup() %>% 
  arrange(circunscripcion, organismo_abbrev)  %>% 
  janitor::adorn_totals("row")

  
outputTable(oralidad_vista_causa, 
            caption = "Vista de Causa Programadas por organismo") %>%
  column_spec(6, "5cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```


# Procesos sin programación de Vista de Causa en el mes del informe

En esta tabla se observa por organismo procesos (indentificados por el número de ingreso) que tuvieron audiencia preliminar y no registraron programación de audiencia de vista de causa.

```{r}

outputTable(sinprogVC, 
            caption = "Sin datos registrados sobre programación de Vista de Causa") %>%
  #column_spec(8, "4cm") %>% column_spec(6, "2cm") %>% 
  row_spec(0, angle = 90) 

```

\pagebreak

<!-- # Procesos sin programación de Vista de Causa desde inicio del Proyecto de Oralidad -cfrme. Ac.Gral 18/18. -->

<!-- En esta tabla se observa por organismo procesos (indentificados por el número de ingreso) que tuvieron audiencia preliminar y no registraron programación de audiencia de vista de causa. -->

<!-- ```{r} -->

<!-- outputTable(sinprogVC_casosproblema,  -->
<!--             caption = "Sin datos registrados sobre programación de Vista de Causa") %>% -->
<!--   #column_spec(8, "4cm") %>% column_spec(6, "2cm") %>%  -->
<!--   row_spec(0, angle = 90)  -->

<!-- # kable(oralidad_vista_causa, caption = "Vista de Causa Programadas por organismo", -->
<!-- #       align = 'c', longtable = TRUE ) %>% -->
<!-- #   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!-- #                 full_width = F, font_size = 10, latex_options = "repeat_header")  %>% -->
<!-- #   column_spec(5, "8cm") %>% -->
<!-- #   row_spec(0, angle = 90) %>%  -->
<!-- #   landscape()  -->

<!-- ``` -->


<!-- **OBSERVACIONES SOBRE PROGRMACIÓN DE VISTA DE CAUSA**: A partir del mes de diciembre se implementó el registro asociado a la no programación de Audiencia de Vista de Causa (i.e. "Sin Vista de Causa por Producción de Prueba Informativa y Documental"), que procura realizar un control exaustivo de estos eventos. Respecto del saneamiento de los datos históricos referidos a los casos incluidos en este seguimiento (un total de 65 casos sin fecha programada de vita de causa, excluyendo aquellos procesos donde ya se cumplió la audiencia VC o se dictó sentencia), para no agravar el trabajo de registración considerando la restricciones tecnológicas de los organimos, se completará su respectivo estado con las sucesivas actualizaciones mensuales.  -->


<!-- \pagebreak -->

# Encuestas de Satisfacción (implementadas a partir del 01/08/18)

La escala de medición de "Satisfacción" para todas las variables consideradas (tanto en la Audiencia Preliminar como la de Vista de Causa) es ordinal y se conforma de la siguiente manera:

4 = Muy Bien,  
3 = Bien,   
2 = Regular, y   
1 = Mal

\pagebreak

## Audiencia Preliminar

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*depurac_prueba* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en la depuración de la prueba en este proceso?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
kable(ap_sat_ab_resumen, caption = "Satisfaccion de Abogados según Dimensión Evaluada",
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()


```

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia?  

```{r}
kable(ap_sat_us_resumen, caption = "Satisfaccion de Usuarios según Dimensión Evaluada",
       align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()

```

\pagebreak

## Audiencia de Vista de Causa

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*plazo_resol* = ¿Cuál es su grado de satisfacción respecto del plazo de resolución de este proceso?
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
kable(avc_sat_ab_resumen, caption = "Satisfaccion de Abogados según Dimensión Evaluada",
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()


```

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*duracion* = ¿Cuál es su grado de satisfacción respecto de la duración de este proceso?
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia?  

```{r}
kable(avc_sat_us_resumen, caption = "Satisfaccion de Usuarios según Dimensión Evaluada",
       align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()

```

Lic. Sebastián Castillo

Área de Planificación Gestión y Estadística    

`r Sys.Date()`


\pagebreak

# ANEXO 1

## Datos particulares del conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

```{r, size = 'tiny'}
kable(consolidado_informesSTJ, caption = "Tabla General", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 8, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>%
  landscape() 
```



