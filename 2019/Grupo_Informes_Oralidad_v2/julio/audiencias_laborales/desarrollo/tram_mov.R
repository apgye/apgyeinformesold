
resultado <- entramite_movimientos(jdos_ecq, año_movs = 2018) 

resultado_movxcausa <- resultado %>% 
  group_by(circunscripcion, organismo, nro, caratula, tproc) %>% 
  summarise(cantidad_movimientos = n()) %>% 
  mutate(nivel_actividad = case_when(
    cantidad_movimientos < 40 ~ "baja", #"menos40mov_anuales",
    cantidad_movimientos >= 40 & cantidad_movimientos <= 100 ~ "media", #entre40_100mov_anuales",
    cantidad_movimientos > 100 ~ "alta")) #mas100mov_anuales"

resultado_nivel_actividad <- resultado_movxcausa %>% 
  ungroup() %>% 
  group_by(circunscripcion, organismo, nivel_actividad) %>% 
  summarise(cantidad_causas = n(),
            cantidad_movimientos = sum(cantidad_movimientos, na.rm = T)) %>% 
  mutate('% causas' = str_c(round(cantidad_causas / sum(cantidad_causas) * 100, digits = 2), " %")) %>% 
  mutate('% movimientos' = str_c(round(cantidad_movimientos / sum(cantidad_movimientos) * 100, digits = 2), " %")) %>% 
  select(circunscripcion, organismo, nivel_actividad, cantidad_causas, '% causas', cantidad_movimientos, '% movimientos')

           
# resultado_nivel_actividad %>% 
#   filter(organismo == "jdocco0901pna") %>% 
#   ggplot(aes(y = nro, x = cantidad_movimientos)) +
#   geom_histogram(stat = "identity") 

