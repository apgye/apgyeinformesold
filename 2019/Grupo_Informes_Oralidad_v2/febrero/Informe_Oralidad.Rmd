---
title: "Informe General sobre Oralidad"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: pdf_document
params:
  start_date: 
    label: "Fecha_inicio_inf"
    input: text
    value: "2019-02-01"
  end_date:
    label: "Fecha_fin_inf"
    input: text
    value: "2019-03-01"
  meses_prev_inf:
    label: "Mes del informe inmediato anterior"
    input: text
    value: "febrero"  
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
options(knitr.kable.NA = '')
```

```{r echo=FALSE, include=FALSE}
# parámetros del informe
start_date <- params$start_date
end_date <- params$end_date
meses_prev_inf <- params$meses_prev_inf

```


```{r}
source("informe.R")
```


# Actualización de Datos correspondiente al período: `r getDataIntervalStr(start_date, end_date)`


## Datos Generales del conjunto de procesos sometidos al Protocolo de Oralidad.

En esta tabla se presenta información sobre el conjunto de procesos sometidos al Protocolo de Oralidad e informados al área en los datos presentados de julio a la fecha.

En el computo de cada indicador se empleó el siguiente criterio de conteo:

+ **cantidad procesos en oralidad informados**: casos con audiencia preliminar fijada desde de julio 2018 a la fecha. Estos casos sometidos a oralidad se consolidaron a través de declaración de los organismos.   
+ **cantidad_preliminares_realizadas**: conjunto de casos con audiencia preliminar realizada en el período considerado. Se excluyen del conteo los casos duplicados. 
+ **cantidad_preliminares_no_realizadas**: conjunto de casos con audiencia preliminar no_realizada en el período considerado. 
+ **cantidad_vista_causa_programadas**: conjunto de casos con audiencia de vista de causa programada e informada al momento de celebración de la preliminar. Se excluyen del conteo los casos duplicados.      
+ **cantidad_vista_causa_realizadas**: conjunto de casos con audiencia de vista de causa celebrada a partir de *septiembre 2018*. Se excluyen del conteo los casos duplicados y los casos con multiples registros de audiencia para un mismo mes.     
+ **cantidad_conciliaciones_enAPoAVC**: conjunto de casos con conciliación total registrada en audiencia preliminar o en audiencia de vista de causa.         
+ **cantidad_sentencias**: conjunto de casos con sentencia dictada en procesos sometidos al Protocolo de Oralidad que se informaron al área.   


```{r}
kable(resumen, caption = "Resumen", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90)
```

**ATENCION**: En ANEXO 1 se agrega la tabla completa empleada para el resumen precedente. 

\pagebreak

# Audiencias de Vista de Causa Programadas a partir de `r end_date`

```{r}

oralidad_vista_causa <- consolidado_informesSTJ %>% 
  select(circunscripcion, organismo_descripcion, nro_expte, fprogramada_vista_causa) %>% 
  filter(!is.na(fprogramada_vista_causa) & fprogramada_vista_causa >= end_date) %>% 
  mutate(mes = as.character(lubridate::month(fprogramada_vista_causa)), 
         año = as.character(lubridate::year(fprogramada_vista_causa))) %>%
  group_by(circunscripcion, organismo_descripcion, año, mes) %>% 
  summarise(cantidad_audiencias = n(), 
            nros_expte_y_fechaAudiencia = paste(nro_expte, fprogramada_vista_causa,
                                sep = ": ", collapse = "; ")) %>% 
  ungroup() %>% 
  arrange(circunscripcion, organismo_descripcion)  %>% 
  janitor::adorn_totals("row")

  
outputTable(oralidad_vista_causa, 
            caption = "Vista de Causa Programadas por organismo") %>%
  column_spec(6, "5cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```


# Procesos sin programación de Vista de Causa en el mes del informe

En esta tabla se observa por organismo procesos (indentificados por el número de ingreso) que tuvieron audiencia preliminar y no registraron programación de audiencia de vista de causa.

```{r}

outputTable(sinprogVC, 
            caption = "Sin datos registrados sobre programación de Vista de Causa") %>%
  #column_spec(8, "4cm") %>% column_spec(6, "2cm") %>% 
  row_spec(0, angle = 90) 

```

\pagebreak

<!-- # Procesos sin programación de Vista de Causa desde inicio del Proyecto de Oralidad -cfrme. Ac.Gral 18/18. -->

<!-- En esta tabla se observa por organismo procesos (indentificados por el número de ingreso) que tuvieron audiencia preliminar y no registraron programación de audiencia de vista de causa. -->

<!-- ```{r} -->

<!-- outputTable(sinprogVC_casosproblema,  -->
<!--             caption = "Sin datos registrados sobre programación de Vista de Causa") %>% -->
<!--   #column_spec(8, "4cm") %>% column_spec(6, "2cm") %>%  -->
<!--   row_spec(0, angle = 90)  -->

<!-- # kable(oralidad_vista_causa, caption = "Vista de Causa Programadas por organismo", -->
<!-- #       align = 'c', longtable = TRUE ) %>% -->
<!-- #   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!-- #                 full_width = F, font_size = 10, latex_options = "repeat_header")  %>% -->
<!-- #   column_spec(5, "8cm") %>% -->
<!-- #   row_spec(0, angle = 90) %>%  -->
<!-- #   landscape()  -->

<!-- ``` -->


<!-- **OBSERVACIONES SOBRE PROGRMACIÓN DE VISTA DE CAUSA**: A partir del mes de diciembre se implementó el registro asociado a la no programación de Audiencia de Vista de Causa (i.e. "Sin Vista de Causa por Producción de Prueba Informativa y Documental"), que procura realizar un control exaustivo de estos eventos. Respecto del saneamiento de los datos históricos referidos a los casos incluidos en este seguimiento (un total de 65 casos sin fecha programada de vita de causa, excluyendo aquellos procesos donde ya se cumplió la audiencia VC o se dictó sentencia), para no agravar el trabajo de registración considerando la restricciones tecnológicas de los organimos, se completará su respectivo estado con las sucesivas actualizaciones mensuales.  -->


<!-- \pagebreak -->

# Encuestas de Satisfacción (implementadas a partir del 01/08/18)

La escala de medición de "Satisfacción" para todas las variables consideradas (tanto en la Audiencia Preliminar como la de Vista de Causa) es ordinal y se conforma de la siguiente manera:

4 = Muy Bien,  
3 = Bien,   
2 = Regular, y   
1 = Mal

\pagebreak

## Audiencia Preliminar

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*depurac_prueba* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en la depuración de la prueba en este proceso?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
kable(ap_sat_ab_resumen, caption = "Satisfaccion de Abogados según Dimensión Evaluada",
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()


```

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia?  

```{r}
kable(ap_sat_us_resumen, caption = "Satisfaccion de Usuarios según Dimensión Evaluada",
       align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()

```

\pagebreak

## Audiencia de Vista de Causa

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*plazo_resol* = ¿Cuál es su grado de satisfacción respecto del plazo de resolución de este proceso?
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
kable(avc_sat_ab_resumen, caption = "Satisfaccion de Abogados según Dimensión Evaluada",
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()


```

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*duracion* = ¿Cuál es su grado de satisfacción respecto de la duración de este proceso?
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia?  

```{r}
kable(avc_sat_us_resumen, caption = "Satisfaccion de Usuarios según Dimensión Evaluada",
       align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()

```

Lic. Sebastián Castillo
Área de Planificación Gestión y Estadística    
`r Sys.Date()`


\pagebreak

# ANEXO 1

## Datos particulares del conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

```{r}
kable(consolidado_informesSTJ, caption = "Tabla General", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  column_spec(2, "3cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape() 
```



