library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tibble)


# Metadatos
entidades <- listar_organismos()

start_date <- "2018-07-01"
end_date <- "2018-11-01"

jdos_civiles <- entidades %>% 
  filter(tipo == "jdo", str_detect(materia, "cco" )) %>% 
  filter(!organismo %in% c("jdocco0501con", "jdocco0502con",  "jdocco0301uru", 
                           "jdocco0302uru", "jdocco0901pna", "jdocco0902pna", 
                           "jdocco1001pna", "jdocco1002pna"))


presentaciones_justat <- DB_PROD() %>% tbl("submission") %>% select(-input) %>% collect()

base_computo <- presentaciones_justat %>% 
  filter(data_interval_start == as.Date(start_date), iep %in% jdos_civiles$organismo,
         str_detect(operation, "CINC1C|AUDIC|SAT"), enabled == T) %>% 
  group_by(iep, operation) %>% 
  summarise(presentaciones = n()) %>% 
  left_join(apgyeJusEROrganization::OPERATION_DESCRIPTION, 
            by = c("operation" = "operacion")) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, iep, organismo_descripcion, operacion_descripcion, presentaciones) %>% 
  arrange(desc(circunscripcion)) %>% 
  janitor::adorn_totals("row")


organismo_sin_informar <-jdos_civiles$organismo[!jdos_civiles$organismo %in% 
                                               unique(base_computo$iep)] 

# Conjunto de casos comprendidos en el Protocolo de Procesos por Audiencias
# Casos con fecha audiencia preliminar > julio
# Casos iniciados > julio e indentificados como proceso por audiencia.


# Causas Iniciadas
cinc1c <- apgyeRStudio::primariasOrganismo("jdocco*", start_date, end_date)$CINC1C_v2$tabla() %>% 
  filter(iep %in% jdos_civiles$organismo ) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect() %>% 
  ungroup() %>% 
  filter(str_detect(fmov_treg_fres, "Proceso por Audiencia")) %>% 
  select(iep, nro, caratula, tproc, finicio)

cinc1c_vpa <- apgyeRStudio::primariasOrganismo("jdocco*", start_date, end_date)$CINC1C_v2_L8$tabla() %>% 
  filter(iep %in% jdos_civiles$organismo ) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect() %>% 
  ungroup() %>% 
  filter(str_detect(fmov_treg_, "Proceso por Audiencia")) %>% 
  select(iep, nro, caratula, tproc, finicio)

cinc1c_comb  <- bind_rows(cinc1c, cinc1c_vpa)

# Audiencias

audic_prelim_raw <- apgyeRStudio::primariasOrganismo("jdocco*", start_date, end_date)$AUDIC$tabla() %>%
  filter(iep %in% jdos_civiles$organismo ) %>% 
  mutate(fea = dmy(fea)) %>% 
  # filtro de fea en {data_interval_start, data_interval_end}
  filter(fea >= data_interval_start, fea < data_interval_end) %>% 
  filter(esta %in% c("2", "3"), ta == "1") %>%
  collect() %>% ungroup() %>% 
  mutate(ra = toupper(ra), 
         faprueb = as.Date(faprueb, format="%d/%m/%Y"),
         fprog = as.Date(fprog, format="%d/%m/%Y")) %>% 
  mutate(esta = case_when(esta == "2" ~ "realizada",
                          esta == "3" ~ "no_realizada", TRUE ~ "sd"), 
         ta = case_when(ta == "1" ~ "preliminar", #ta == "2" ~ "vista_causa", 
                        TRUE ~ "sd")) %>% 
  # filtro casos duplicados 
  distinct(iep, nro, caratula, tproc, fea, .keep_all = T) %>% 
  select(iep, nro, caratula, tproc, fecha_ingreso = finicio,
         fecha_efect_preliminar = fea, estado_preliminar = esta, 
         resultado_preliminar = ra, apertura_prueba = faprueb, 
         fprogramada_vista_causa = fprog,  
         videofilmada = audvid, data_interval_start)


# Completar info con vista_causa y resueltos_sentencia

# Audiencia Vista Causa
audic_vcausa_raw <- apgyeRStudio::primariasOrganismo("jdocco*", "2018-09-01", end_date)$AUDIC$tabla() %>%
  mutate(fea = dmy(fea)) %>% 
  # filtro de fea en {data_interval_start, data_interval_end}
  filter(fea >= data_interval_start, fea < data_interval_end) %>% 
  filter(esta %in% c("2", "3"), ta == "2") %>%
  collect() %>% ungroup() %>% 
  mutate(ra = toupper(ra), 
         faprueb = as.Date(faprueb, format="%d/%m/%Y"),
         fprog = as.Date(fprog, format="%d/%m/%Y")) %>% 
  mutate(esta = case_when(esta == "2" ~ "realizada",
                          esta == "3" ~ "no_realizada", TRUE ~ "sd"), 
         ta = case_when(ta == "2" ~ "vista_causa", 
                        TRUE ~ "sd")) %>% 
  # filtro casos duplicados 
  distinct(iep, nro, caratula, tproc, fea, .keep_all = T) %>% 
  # Las Vistas de Causas no han sucedido, entoces lo que están registrando con 
  # el código 2 es la audiencia de prueba. Por esa razón se procederá a computar 
  # las vista de causa a partir de septiembre. # Se eliminó el filtrado por duplicados 
  # que se puede encontrar en el mes de octubre.
  select(iep, nro, caratula, tproc, fecha_ingreso = finicio,
         fecha_efect_vcausa = fea, estado_vcausa = esta, 
         resultado_vcausa = ra,  
         videofilmada_vcausa = audvid, 
         data_interval_start_vcausa = data_interval_start)

# audic_vcausa_raw_sindup <- audic_vcausa_raw %>% 
#   group_by(iep, nro, fea) %>% 
#   filter(row_number() == n())
  
# Consolidado Audiencias
# audic_consolidado <- audic_prelim_raw  %>%
#   left_join(audic_vcausa_raw[, c("iep", "nro", "fecha_efect_vcausa", "estado_vcausa", 
#                               "resultado_vcausa", "videofilmada_vcausa")],
#                              by = c("nro" , "iep"))

exclusiones <- tribble(  ~iep, ~nro,
  "jdocco0100pna", 17349,
  "jdocco0100pna", 16639,
  "jdocco0100pna", 17427,
  "jdocco0100pna", 17264, 
  "jdocco0100pna", 17191, 
  "jdocco0100pna", 17207, 
  "jdocco0500pna", 29572, 
  "jdocco0500pna", 28900) %>% 
  mutate(nro = as.character(nro))


audic_consolidado <- audic_prelim_raw  %>%
  full_join(audic_vcausa_raw, by = c("nro" , "iep", "caratula",
                                     "tproc", "fecha_ingreso")) %>%
  anti_join(exclusiones, by = c("iep", "nro")) %>% 
  mutate(resultado_preliminar = str_sub(resultado_preliminar, 1, 1))
  # Validaciones solicitadas por el Ministerio
  # mutate(resultado_preliminar = ifelse(estado_preliminar == "no_realizada", "S", resultado_preliminar))

# Agregar Resoluciones
# Listado de Magistrados y Funcionarios
magistrados_y_secretarios <- DB_PROD() %>% apgyeDSL::apgyeTableData('personal_planta_ocupada') %>%
  left_join(DB_PROD() %>% apgyeDSL::apgyeTableData('personal_personas') %>%
              select(idagente, categoria, apellido, nombres), by=c("idagente")
  ) %>%
  filter(grepl("JUEZ|SECRETARIO|VOCAL|FISCAL|DEFENSOR", categoria)) %>%
  collect()
magist_func_id_agentes <- magistrados_y_secretarios$idagente
# add "0" ti IDs for External Agent
magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"


primera_inst_cco <- apgyeRStudio::primariasOrganismo("jdocco*", start_date, end_date)$CADR1C$tabla() %>%
  filter(iep %in% jdos_civiles$organismo) %>% 
  mutate(fres = dmy(fres)) %>% 
  mutate(fdesp = dmy(fdesp)) %>%
  filter(fres >= data_interval_start, fres < data_interval_end) %>% 
  filter(is.na(mat) | toupper(mat) == "C") %>% # validación para causas civiles
  filter(toupper(as) == "S", !is.na(fres)) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect() %>% ungroup() %>% 
  distinct(iep, nro, caratula, tproc, fres, .keep_all = T) %>% 
  mutate(mes = months(data_interval_start)) %>% 
  select(iep, mes, nro, caratula, tproc, fdesp, fres, as, tres, codigo_juez) %>% 
  mutate(tres = ifelse(is.na(tres), "SD", tres)) %>% 
  mutate(tres = case_when(
    tres == "1" ~ "Conciliación",
    tres == "2" ~ "Transacción",
    tres == "3" ~ "Caducidad",
    tres == "4" ~ "Desistimiento",
    tres == "5" ~ "Allanamiento",
    tres == "10" ~ "Sent.Definitiva",
    tres == "SD" ~ "Sent.Definitiva", 
    TRUE ~ "otras")) %>% 
  filter(tres != "otras")


oralidad_consolidado <- audic_consolidado %>% 
  left_join(primera_inst_cco[ , c("iep", "nro","fdesp", "fres", "as", "tres")], 
            by = c("iep", "nro")) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], by = c("iep" = "organismo")) %>% 
  select(circunscripcion, organismo_descripcion, organismo = iep, everything()) %>% 
  mutate(organismo = str_remove_all(organismo, "[a-z,0]"),
         caratula = abbreviate(caratula, minlength = 1, named = FALSE)) %>%  
  mutate(fres = as.character(fres)) %>% 
  # reimputación solicitada por el ministerio siempre que resultado preliminar T, 
  # forma de resolucion = Conciliacion y fecha de efectivizacion
  mutate(tres =  ifelse(resultado_preliminar == "T" & !is.na(resultado_preliminar), "Conciliación", tres), 
         fres =  ifelse(resultado_preliminar == "T" & is.na(fres), as.character(fecha_efect_preliminar), fres)) %>% 
  mutate(tres =  ifelse(resultado_vcausa == "T" & !is.na(resultado_vcausa), "Conciliación", tres), 
         fres =  ifelse(resultado_vcausa == "T" & is.na(fres), as.character(fecha_efect_vcausa), fres)) %>% 
  rename(forma_deresolucion = tres) %>% 
  select(-videofilmada, -data_interval_start, -data_interval_start_vcausa,
         -videofilmada_vcausa) %>% 
  rename(fecha_despacho = fdesp, fecha_resolucion = fres) %>% 
  arrange(circunscripcion, organismo_descripcion)


oralidad_resumen <- oralidad_consolidado %>% 
  group_by(circunscripcion, organismo_descripcion) %>% 
  summarise(
    # Conjunto de casos comprendidos en el Protocolo de Procesos por Audiencias
    # Casos con fecha audiencia preliminar > julio, y
    # Casos iniciados a partir de agosto indentificados como proceso por audiencia.
    cantidad_procesos_en_oralidad_informados = n(),
    cantidad_preliminares_realizadas = sum(!is.na(fecha_efect_preliminar) &
                                                   estado_preliminar == "realizada"),
    cantidad_vista_causa_programadas = sum(!is.na(fprogramada_vista_causa)), 
    cantidad_vista_causa_realizadas = sum(!is.na(fecha_efect_vcausa) &
                                            estado_vcausa == "realizada"), 
    cantidad_conciliaciones_enAPoAVC = sum(forma_deresolucion == "Conciliación", na.rm = T),
    cantidad_sentencias_procesos_en_oralidad = sum(!is.na(fecha_resolucion) & 
                                                       forma_deresolucion != "Conciliación")) %>% 
  select(circunscripcion, organismo_descripcion, everything()) %>% 
  arrange(circunscripcion, organismo_descripcion)  %>% 
  janitor::adorn_totals("row")


# Consolidado parcial para el informe a juzgados

oralidad_consolidado_parcial <- oralidad_consolidado %>% 
  filter(estado_preliminar == "realizada" | estado_vcausa == "realizada") %>% 
  select(circunscripcion, organismo_descripcion, nro_expte = nro,  
         fecha_ingreso, fecha_efect_preliminar, resultado_preliminar, apertura_prueba, 
         fprogramada_vista_causa, fecha_efect_vcausa, resultado_vcausa, fecha_resolucion, as,
         forma_deresolucion)


# Archivo enviado el MINJUS

write.table(oralidad_consolidado, 
            "~/apgyeInformes/2018/Grupo Informes Oralidad Civil/noviembre/oralidad_consolidado.csv", 
            row.names = F, col.names = T, sep = ",")


# Encuestas de Satisfacción
ap_sat_ab <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                              start_date,
                                              end_date)$AP_SAT_AB$obtener()

ap_sat_ab_resumen <- ap_sat_ab %>% 
  select(iep, trato, depurac_prueba, intent_concil, instalaciones) %>% 
  tidyr::gather(key = "dimension_evaluada", value = "calificacion",
                -iep) %>% 
  mutate(calificacion = case_when(
    calificacion == "4" ~ "Muy_Bien", 
    calificacion == "3" ~ "Bien", 
    calificacion == "2" ~ "Regular", 
    calificacion == "1" ~ "Mal",
    TRUE ~ "NS_NC"
  )) %>% 
  mutate(nro = 1:length(iep), cantidad = 1, 
         calificacion = factor(calificacion, 
                               levels= c("Muy_Bien", "Bien",
                                         "Regular", "Mal", "NS_NC"))) %>% 
  tidyr::spread(calificacion, cantidad, fill = 0, drop = F) %>% 
  group_by(iep, dimension_evaluada) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE ) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, iep, dimension_evaluada, 
         Muy_Bien, Bien, Regular, Mal, NS_NC, -iep) %>% 
  arrange(circunscripcion)%>% 
  janitor::adorn_totals("row")

colnames(ap_sat_ab_resumen) <- chartr("_", " ",
                                      colnames(ap_sat_ab_resumen))

ap_sat_us <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                              start_date,
                                              end_date)$AP_SAT_US$obtener() 


ap_sat_us_resumen <- ap_sat_us %>% 
  select(iep, trato, comprension = compresion,
         escucha, instalaciones) %>% 
  tidyr::gather(key = "dimension_evaluada", value = "calificacion",
                -iep) %>% 
  mutate(calificacion = case_when(
    calificacion == "4" ~ "Muy_Bien", 
    calificacion == "3" ~ "Bien", 
    calificacion == "2" ~ "Regular", 
    calificacion == "1" ~ "Mal",
    TRUE ~ "NS_NC"
  )) %>% 
  mutate(nro = 1:length(iep), cantidad = 1, 
         calificacion = factor(calificacion, 
                               levels= c("Muy_Bien", "Bien",
                                         "Regular", "Mal", "NS_NC"))) %>% 
  tidyr::spread(calificacion, cantidad, fill = 0, drop = F) %>% 
  group_by(iep, dimension_evaluada) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE ) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, dimension_evaluada, 
         Muy_Bien, Bien, Regular, Mal, NS_NC, -iep) %>% 
  arrange(circunscripcion)%>% 
  janitor::adorn_totals("row")

colnames(ap_sat_us_resumen) <- chartr("_", " ",
                                      colnames(ap_sat_us_resumen))

## Encuestas Satisfaccion Vista de Causa

# ABOGADOS

avc_sat_ab <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                              start_date,
                                              end_date)$AVC_SAT_AB$obtener()
# incorporo villa paranacito

avc_sat_abl8 <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                               start_date,
                                               end_date)$AVC_SAT_AB_L8$obtener()
colnames(avc_sat_abl8) <- colnames(avc_sat_ab)
avc_sat_ab <- bind_rows(avc_sat_ab, avc_sat_abl8)
rm(avc_sat_abl8)

# proceso resumen

avc_sat_ab_resumen <- avc_sat_ab %>% 
  select(iep, trato, intent_concil, plazo_resol, instalaciones) %>% 
  tidyr::gather(key = "dimension_evaluada", value = "calificacion",
                -iep) %>% 
  mutate(calificacion = case_when(
    calificacion == "4" ~ "Muy_Bien", 
    calificacion == "3" ~ "Bien", 
    calificacion == "2" ~ "Regular", 
    calificacion == "1" ~ "Mal",
    TRUE ~ "NS_NC"
  )) %>% 
  mutate(nro = 1:length(iep), cantidad = 1, 
         calificacion = factor(calificacion, 
                               levels= c("Muy_Bien", "Bien",
                                         "Regular", "Mal", "NS_NC"))) %>% 
  tidyr::spread(calificacion, cantidad, fill = 0, drop = F) %>% 
  group_by(iep, dimension_evaluada) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE ) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, iep, dimension_evaluada, 
         Muy_Bien, Bien, Regular, Mal, NS_NC, -iep) %>% 
  arrange(circunscripcion)%>% 
  janitor::adorn_totals("row")

colnames(avc_sat_ab_resumen) <- chartr("_", " ",
                                      colnames(avc_sat_ab_resumen))


# USUARIO

avc_sat_us <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                              start_date,
                                              end_date)$AVC_SAT_US$obtener() 

# incorporo villa paranacito
avc_sat_usl8 <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                                 start_date,
                                                 end_date)$AVC_SAT_US_L8$obtener()
colnames(avc_sat_usl8) <- colnames(avc_sat_us)
avc_sat_us <- bind_rows(avc_sat_us, avc_sat_usl8)
rm(avc_sat_usl8)

# proceso resumen

avc_sat_us_resumen <- avc_sat_us %>% 
  select(iep, trato, comprension = compresion,
         escucha, duracion, instalaciones) %>% 
  tidyr::gather(key = "dimension_evaluada", value = "calificacion",
                -iep) %>% 
  mutate(calificacion = case_when(
    calificacion == "4" ~ "Muy_Bien", 
    calificacion == "3" ~ "Bien", 
    calificacion == "2" ~ "Regular", 
    calificacion == "1" ~ "Mal",
    TRUE ~ "NS_NC"
  )) %>% 
  mutate(nro = 1:length(iep), cantidad = 1, 
         calificacion = factor(calificacion, 
                               levels= c("Muy_Bien", "Bien",
                                         "Regular", "Mal", "NS_NC"))) %>% 
  tidyr::spread(calificacion, cantidad, fill = 0, drop = F) %>% 
  group_by(iep, dimension_evaluada) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE ) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, dimension_evaluada, 
         Muy_Bien, Bien, Regular, Mal, NS_NC, -iep) %>% 
  arrange(circunscripcion)%>% 
  janitor::adorn_totals("row")

colnames(avc_sat_us_resumen) <- chartr("_", " ",
                                      colnames(avc_sat_us_resumen))


#setwd("~/justat/PRODUCTOS/2018/Grupo Informes Oralidad Civil/noviembre")

write.table(ap_sat_ab_resumen, 
            "~/apgyeInformes/2018/Grupo Informes Oralidad Civil/noviembre/preliminar_satisfaccion_abogados_v2.csv", 
            col.names = T, row.names = F, sep = ",")

write.table(ap_sat_us_resumen,
            "~/apgyeInformes/2018/Grupo Informes Oralidad Civil/noviembre/preliminar_satisfaccion_usuarios_v2.csv", 
            col.names = T, row.names = F, sep = ",")

write.table(avc_sat_ab_resumen, 
            "~/apgyeInformes/2018/Grupo Informes Oralidad Civil/noviembre/avistacausa_satisfaccion_abogados_v2.csv", 
            col.names = T, row.names = F, sep = ",")

write.table(avc_sat_us_resumen, 
            "~/apgyeInformes/2018/Grupo Informes Oralidad Civil/noviembre/avistacausa_satisfaccion_usuarios_v2.csv", 
            col.names = T, row.names = F, sep = ",")



##############################################################################

# OBSERVACIONES

# obvservación correcta 
# oralidad_consolidado_minjus %>% filter(!is.na(fecha_efect_preliminar)) %>% 
#   filter(resultado_preliminar %in% c("S" , "P")) %>% 
#   filter(is.na(fprogramada_vista_causa)) %>% View()

oralidad_consolidado_minjus_observaciones <- oralidad_consolidado %>% 
  mutate(fprogramada_vista_causa = as.character(fprogramada_vista_causa)) %>%
  mutate(fprogramada_vista_causa = ifelse(!is.na(fecha_efect_preliminar) & 
                                            resultado_preliminar != "T" &
                                            is.na(fprogramada_vista_causa),
                                          "sin_dato", fprogramada_vista_causa)) %>% 
  filter(fprogramada_vista_causa == "sin_dato") %>% 
  mutate(mes = months(fecha_efect_preliminar)) %>% 
  group_by(circunscripcion, organismo_descripcion, mes) %>% 
  summarise(casos = n(), 
            nro_de_causa_sin_declarac_fprog = str_c(nro, collapse = ";")) %>% 
  janitor::adorn_totals("row")
  
  
oralidad_consolidado_minjus_observaciones_tabla <- oralidad_consolidado %>% 
  mutate(fprogramada_vista_causa = as.character(fprogramada_vista_causa)) %>%
  mutate(fprogramada_vista_causa = ifelse(!is.na(fecha_efect_preliminar) & 
                                            resultado_preliminar != "T" &
                                            is.na(fprogramada_vista_causa),
                                          "sin_dato", fprogramada_vista_causa)) %>% 
  filter(fprogramada_vista_causa == "sin_dato") %>% 
  mutate(mes = months(fecha_efect_preliminar)) %>% 
  select(circunscripcion, organismo_descripcion, mes, nro, 
         fecha_efect_preliminar, fprogramada_vista_causa) %>% 
  mutate('¿en oralidad? si_no' = " ",
         'razones para la no fijación de AVC' = " ") %>% 
  arrange(circunscripcion, organismo_descripcion, mes)


write.table(oralidad_consolidado_minjus_observaciones_tabla, 
            "~/apgyeInformes/2018/Grupo Informes Oralidad Civil/noviembre/Formulario_Respuesta.csv", 
            col.names = T, row.names = F, sep = ",")


