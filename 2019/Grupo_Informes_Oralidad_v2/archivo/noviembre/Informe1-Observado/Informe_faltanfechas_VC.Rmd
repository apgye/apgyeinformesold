---
title: "Informe sobre Observaciones del MINJUS"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
options(knitr.kable.NA = '')
```

```{r}
source("informe.R")
source("utils.R")
```


```{r}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tibble)
library(lubridate)
```


# Falta de Declaración de fechas de programación de Audiencias de Vista de Causa: `r getDataIntervalStr(start_date, end_date)`

La siguiente tabla muestra por organismo la cantidad y causa específica donde no se registraron fechas de programación de vista de causa. 


```{r}
kable(oralidad_consolidado_minjus_observaciones, caption = "Sin Fechas de Programación Vista de Causa", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  column_spec(4, "1cm") %>% 
  row_spec(0) %>% 
  landscape()
  
```

