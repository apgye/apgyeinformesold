---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}
existepresP <- existepresentacion(jdo_pna, start_date, end_date, "CIN3P")
```


## Causas Iniciadas en Sala- `r getDataIntervalStr(start_date, end_date)`

```{r, eval = existepresP}
iniciadas_pen(poblacion = sal_pen,
              start_date = start_date,
              end_date = end_date) %>%
  outputTable(caption = "Causas Iniciadas en Sala") %>%
  row_spec(0, angle = 90) %>% 
  #column_spec(3, "12cm") %>%  
  # column_spec(9, border_right = T) %>% 
  landscape()
  
```

```{r, eval= !existepresP}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "Registra información pendiente de entrega en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, color = "red") %>%
  column_spec(2, width = "30em")
    
```


