
audienciasPenales <- function(poblacion, start_date = "2018-10-01", end_date="2019-04-01", desagregacion_mensual = TRUE) {
  
  if(any(str_detect(poblacion, "oga"))) {
  
  resultado <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData('AUDIP') %>%
    apgyeDSL::interval(start_date , end_date) %>%
    filter(iep %in% poblacion$organismo) %>% 
    left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                mutate(
                  magistrado = apellido,
                  #magistrado = paste0(apellido, ', ', nombres),
                  iep_actuante = iep
                ) %>%
                select(idagente, iep_actuante, magistrado), by=c("jvact"="idagente") ) 
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, magistrado, data_interval_start, data_interval_end) 
    }  else {
      resultado <- resultado %>%  group_by(iep, magistrado) 
    }
  
  resultado <- resultado %>% process()
  
  if(desagregacion_mensual) {
    
    realizadas <- resultado %>% .$result %>%
      filter(audiencias_realizadas != 0) %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                        "junio", "julio", "agosto", "septiembre", 
                                        "octubre", "noviembre", "diciembre", "Total"),
                        ordered = TRUE)) %>% 
      filter(is.na(audorg) | audorg==1) %>%
      filter(!is.na(magistrado)) %>% 
      left_join(poblacion %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
      mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>%
      select(circunscripcion, mes, magistrado, cantidad = audiencias_realizadas, "bloques de audiencia" = presencia_xbloques, 
             "horas en audiencia" = presencia_enhoras, "minutos en audiencia" = presencia_enminutos) %>% 
      arrange(circunscripcion, mes) %>% 
      #janitor::adorn_totals("col") %>% 
      group_by(circunscripcion) %>% 
      do(janitor::adorn_totals(.))
    
    realizadas_xtipo <- resultado %>%  .$result %>%
      filter(audiencias_realizadas != 0) %>% 
      ungroup() %>% 
      mutate(mes = as.character(lubridate::month(data_interval_start, label=T, abbr = F))) %>% 
      filter(is.na(audorg) | audorg==1) %>%
      tidyr::separate_rows(realizadas_articuloCPP, sep = " - ") %>%  
      #mutate(magistrado = forcats::fct_explicit_na(magistrado)) %>% 
      codconver("AUDIP", "realizadas_articuloCPP") %>% 
      filter(realizadas_articuloCPP != "sd") %>% 
      left_join(poblacion %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
      group_by(circunscripcion, magistrado, mes, realizadas_articuloCPP) %>% 
      summarise(cantidad_realizadas = n()) %>% 
      tidyr::spread(key = realizadas_articuloCPP, value = cantidad_realizadas, fill = 0) %>% 
      janitor::adorn_totals("col") %>% 
      ungroup() %>% 
      group_by(circunscripcion, magistrado) %>% 
      do(janitor::adorn_totals(.))
    
    realizadas_xtipo <<- realizadas_xtipo
    
    realizadas
  } else {
    realizadas <- resultado %>% .$result %>%
      filter(audiencias_realizadas != 0) %>% 
      filter(is.na(audorg) | audorg==1) %>%
      filter(!is.na(magistrado)) %>% 
      left_join(poblacion %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
      mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>%
      select(circunscripcion, magistrado, cantidad = audiencias_realizadas, "bloques de audiencia" = presencia_xbloques, 
               "horas en audiencia" = presencia_enhoras, "minutos en audiencia" = presencia_enminutos) %>% 
      arrange(circunscripcion) %>% 
      # janitor::adorn_totals("col") %>% 
      group_by(circunscripcion) %>% 
      do(janitor::adorn_totals(.))
    
    realizadas_xtipo <- resultado %>%  .$result %>%
      filter(audiencias_realizadas != 0) %>% 
      ungroup() %>% 
      filter(is.na(audorg) | audorg==1) %>%
      tidyr::separate_rows(realizadas_articuloCPP, sep = " - ") %>%  
      #mutate(magistrado = forcats::fct_explicit_na(magistrado)) %>% 
      codconver("AUDIP", "realizadas_articuloCPP") %>% 
      filter(realizadas_articuloCPP != "sd") %>% 
      left_join(poblacion %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
      group_by(circunscripcion, magistrado, realizadas_articuloCPP) %>% 
      summarise(cantidad_realizadas = n()) %>% 
      tidyr::spread(key = realizadas_articuloCPP, value = cantidad_realizadas, fill = 0) %>% 
      janitor::adorn_totals("col") %>% 
      ungroup() %>% 
      group_by(circunscripcion, magistrado) %>% 
      do(janitor::adorn_totals(.)) 
      
      realizadas_xtipo <<- realizadas_xtipo
      
      realizadas
  }
  
  } 
  else if(any(str_detect(poblacion, "tja"))) {
    
        poblacion_infte <- oga # organo informante OGA, organo informado Tribunal de Juicio

        resultado <- DB_PROD() %>%
          apgyeDSL::apgyeTableData('AUDIPTJ') %>%
          apgyeDSL::interval(start_date , end_date) %>%
          filter(iep %in% poblacion_infte$organismo)

        if(desagregacion_mensual) {
          resultado <- resultado %>%  group_by(iep, magistrado, data_interval_start, data_interval_end)
        } else {
          resultado <- resultado %>%  group_by(iep, magistrado)
        }

        resultado <- resultado %>% process()


        if(desagregacion_mensual) {


          realizadas <- resultado %>% .$result %>% ungroup() %>%
            mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>%
            left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>%
            select(circunscripcion, mes, everything()) %>% 
            filter(audiencias_realizadas != 0)

          jvact_final <- realizadas %>% as.data.frame() %>%
            select(iep, jvact_final) %>%
            dplyr::mutate(rn = row_number()) %>% separate_rows(jvact_final) %>%
            left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                        mutate(
                          magistrado = apellido,
                          iep_actuante = iep
                        ) %>%
                        select(idagente, iep_actuante, magistrado), by=c("jvact_final"="idagente"), copy = T) %>%
            group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
            left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
            ungroup() %>%
            mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>% # traducir código html
            .$magistrado

          realizadas$jvact_final <- jvact_final

          realizadas <- realizadas %>%
            select(-audorg, -data_interval_start, -data_interval_end, -calidad_primaria, -calidad) %>%
            rename(magistrado = jvact_final) %>% 
            mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", " * ")) %>% # código no confirmado SGP
            mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo",
                                                "junio", "julio", "agosto", "septiembre",
                                                "octubre", "noviembre", "diciembre", "Total"),
                                ordered = TRUE)) %>%
            select(circunscripcion, mes, magistrado, cantidad = audiencias_realizadas, "bloques de audiencia" = presencia_xbloques, 
                 "horas en audiencia" = presencia_enhoras, "minutos en audiencia" = presencia_enminutos) %>% 
            arrange(circunscripcion, mes) %>% 
            group_by(circunscripcion) %>% 
            do(janitor::adorn_totals(.))
          
          # Realizadas por tipo de artículo código
          
          realizadas_xtipo <- resultado %>%  .$result %>%
            ungroup() %>% 
            mutate(mes = as.character(lubridate::month(data_interval_start, label=T, abbr = F))) %>% 
            filter(is.na(audorg) | audorg == 2) %>% 
            filter(audiencias_realizadas != 0)
          
          
          jvact_final_xt <- realizadas_xtipo %>% as.data.frame() %>%
            select(iep, jvact_final) %>%
            dplyr::mutate(rn = row_number()) %>% separate_rows(jvact_final) %>%
            left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                        mutate(
                          magistrado = apellido,
                          iep_actuante = iep
                        ) %>%
                        select(idagente, iep_actuante, magistrado), by=c("jvact_final"="idagente"), copy = T) %>%
            group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
            left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
            ungroup() %>%
            mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>% # traducir código html
            .$magistrado
          
          realizadas_xtipo$jvact_final <- jvact_final_xt
          
          
          realizadas_xtipo <- realizadas_xtipo %>% 
            rename(magistrado = jvact_final) %>% 
            mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", " * ")) %>% # código no confirmado SGP
            tidyr::separate_rows(realizadas_articuloCPP, sep = ",") %>%  
            #mutate(magistrado = forcats::fct_explicit_na(magistrado)) %>% 
            codconver("AUDIP", "realizadas_articuloCPP") %>% 
            #filter(realizadas_articuloCPP != "sd") %>% 
            left_join(poblacion_infte %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
            group_by(circunscripcion, magistrado, mes, realizadas_articuloCPP) %>% 
            summarise(cantidad_realizadas = n()) %>% 
            tidyr::spread(key = realizadas_articuloCPP, value = cantidad_realizadas, fill = 0) %>% 
            janitor::adorn_totals("col") %>% 
            ungroup() %>% 
            mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo",
                                                "junio", "julio", "agosto", "septiembre",
                                                "octubre", "noviembre", "diciembre", "Total"),
                                ordered = TRUE)) %>%
            arrange(circunscripcion, magistrado, mes) %>% 
            group_by(circunscripcion, magistrado) %>% 
            do(janitor::adorn_totals(.)) 
          
          
          realizadas_xtipo <<- realizadas_xtipo
          
          realizadas
          
        } else {
          
          realizadas <- resultado %>% .$result %>% ungroup() %>%
            left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>%
            select(circunscripcion, everything()) %>% 
            filter(audiencias_realizadas != 0)
          
          jvact_final <- realizadas %>% as.data.frame() %>%
            select(iep, jvact_final) %>%
            dplyr::mutate(rn = row_number()) %>% separate_rows(jvact_final) %>%
            left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                        mutate(
                          magistrado = apellido,
                          iep_actuante = iep
                        ) %>%
                        select(idagente, iep_actuante, magistrado), by=c("jvact_final"="idagente"), copy = T) %>%
            group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
            left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
            ungroup() %>%
            mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>% # traducir código html
            .$magistrado
          
          realizadas$jvact_final <- jvact_final
          
          realizadas <- realizadas %>%
            select(-audorg, -calidad_primaria, -calidad) %>%
            rename(magistrado = jvact_final) %>% 
            mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", " * ")) %>% # código no confirmado SGP
            select(circunscripcion, magistrado, cantidad = audiencias_realizadas, "bloques de audiencia" = presencia_xbloques, 
                   "horas en audiencia" = presencia_enhoras, "minutos en audiencia" = presencia_enminutos) %>% 
            arrange(circunscripcion) %>% 
            group_by(circunscripcion) %>% 
            do(janitor::adorn_totals(.))
          
          # Realizadas por tipo de artículo código
          
          realizadas_xtipo <- resultado %>%  .$result %>%
            ungroup() %>% 
            filter(is.na(audorg) | audorg == 2) %>% 
            filter(audiencias_realizadas != 0)
          
          
          jvact_final_xt <- realizadas_xtipo %>% as.data.frame() %>%
            select(iep, jvact_final) %>%
            dplyr::mutate(rn = row_number()) %>% separate_rows(jvact_final) %>%
            left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                        mutate(
                          magistrado = apellido,
                          iep_actuante = iep
                        ) %>%
                        select(idagente, iep_actuante, magistrado), by=c("jvact_final"="idagente"), copy = T) %>%
            group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
            left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
            ungroup() %>%
            mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>% # traducir código html
            .$magistrado
          
          realizadas_xtipo$jvact_final <- jvact_final_xt
          
          realizadas_xtipo <- realizadas_xtipo %>% 
            rename(magistrado = jvact_final) %>% 
            mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", " * ")) %>% # código no confirmado SGP
            tidyr::separate_rows(realizadas_articuloCPP, sep = ",") %>%  
            #mutate(magistrado = forcats::fct_explicit_na(magistrado)) %>% 
            codconver("AUDIP", "realizadas_articuloCPP") %>% 
            #filter(realizadas_articuloCPP != "sd") %>% 
            left_join(poblacion_infte %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
            group_by(circunscripcion, magistrado, realizadas_articuloCPP) %>% 
            summarise(cantidad_realizadas = n()) %>% 
            tidyr::spread(key = realizadas_articuloCPP, value = cantidad_realizadas, fill = 0) %>% 
            janitor::adorn_totals("col") %>% 
            ungroup() %>% 
            group_by(circunscripcion, magistrado) %>% 
            do(janitor::adorn_totals(.))
          
          
          
          realizadas_xtipo <<- realizadas_xtipo
          
          realizadas
          
          
        }

  }
  else if(any(str_detect(poblacion, "campen"))) {
    
    
    resultado <- DB_PROD() %>% apgyeDSL::apgyeTableData('AUDIPC') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep %in% poblacion$organismo) 
    
    if(desagregacion_mensual) {
      
      resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      
      resultado <- resultado %>% group_by(iep) 
      
    }
    
    resultado <- resultado %>% 
      process(AUDIPC) %>% .$result %>% ungroup()
    
    
    if(desagregacion_mensual) {
      
      realizadas <- resultado %>% 
        mutate(mes = as.character(lubridate::month(data_interval_start, label=T, abbr = F)))  %>% 
        select(organismo = iep, mes, everything()) %>%
        left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
        select(-organismo, -data_interval_start, -data_interval_end, -calidad, -calidad_primaria) %>% 
        select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
        mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                            "junio", "julio", "agosto", "septiembre", 
                                            "octubre", "noviembre", "diciembre", "Total"),
                            ordered = TRUE)) %>% 
        arrange(circunscripcion, organismo, mes) 
      
      realizadas
      
    } else {
      
      realizadas <- resultado %>% 
        select(organismo = iep, everything()) %>%
        left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
        select(-organismo, -calidad, -calidad_primaria) %>% 
        select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
        arrange(circunscripcion, organismo) 
      
      realizadas
    }
    realizadas
  }
  else if(any(str_detect(poblacion, "jdopna"))) {
    
    resultado <- DB_PROD() %>% 
      apgyeDSL::apgyeTableData('AUDIPM') %>%
      apgyeDSL::interval(start_date , end_date) %>%
      filter(iep %in% poblacion$organismo) %>% 
      left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                  mutate(
                    magistrado = apellido,
                    #magistrado = paste0(apellido, ', ', nombres),
                    iep_actuante = iep
                  ) %>%
                  select(idagente, iep_actuante, magistrado), by=c("jaud"="idagente") ) 
    
    
    if(desagregacion_mensual) {
      resultado <- resultado %>%  group_by(iep, magistrado, data_interval_start, data_interval_end) 
    }  else {
      resultado <- resultado %>%  group_by(iep, magistrado) 
    }
    
    resultado <- resultado %>% process(AUDIPM)
    
    if(desagregacion_mensual) {
      realizadas <- resultado %>% .$result %>%
        mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
        mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                            "junio", "julio", "agosto", "septiembre", 
                                            "octubre", "noviembre", "diciembre", "Total"),
                            ordered = TRUE)) %>% 
        filter(!is.na(magistrado)) %>% 
        left_join(poblacion %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
        mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>%
        mutate(prom_duracm = round(prom_duracm, digits = 1)) %>% 
        select(circunscripcion, mes, magistrado, cantidad = aud_realizadas, "promedio de bloques" = prom_duracb,
               "promedio de minutos" = prom_duracm) %>%
        arrange(circunscripcion, mes) %>% 
        janitor::adorn_totals("row")

      realizadas
    } else {
      realizadas <- resultado %>% .$result %>%
        filter(!is.na(magistrado)) %>% 
        left_join(poblacion %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo") ) %>%
        mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>%
        select(circunscripcion, mes, magistrado, cantidad = aud_realizadas, "promedio de bloques" = prom_duracb,
               "promedio de minutos" = prom_duracm) %>%
        mutate(prom_duracm = round(prom_duracm, digits = 1)) %>% 
        arrange(circunscripcion) %>% 
        janitor::adorn_totals("row")
      
      realizadas
    }
    
  } 
  

  else if(any(str_detect(poblacion, "oma"))) {
    
    resultado <- DB_PROD() %>% 
      apgyeTableData("IGMP") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep %in% poblacion$organismo) %>% 
      mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
    
    resultado <- resultado %>% 
      tidyr::separate_rows(fmov_treg_fres, sep = "\\$%") %>% # resolver casos de multiples registros
      filter(str_detect(fmov_treg_fres, "APGE")) %>%        # eliminar registros no apge
      mutate(index = row_number()) %>%
      select(index, everything()) %>% 
      tidyr::separate_rows(fmov_treg_fres, sep = "\\$") %>% 
      tidyr::separate(fmov_treg_fres, c("campo", "dato"), sep = ":") %>% 
      mutate(dato = ifelse(dato == "", NA, dato)) %>% 
      tidyr::spread(campo, dato) %>% 
      mutate(resp = toupper(str_remove_all(resp, "DR.|Dr."))) %>% 
      mutate(f1 = lubridate::dmy(f1), f2 = lubridate::dmy(f2),
             f3 = lubridate::dmy(f3), fmr = lubridate::dmy(fmr), 
             n1 = as.integer(n1), n2 = as.integer(n2)) %>% 
      mutate(tr = tolower(tr))
    
    audic_result <- list()
    
    audic_result$audic_oma_prim <- resultado 
    
    if(desagregacion_mensual) {
      
      audic_result$audic_xestado <- resultado %>%
        mutate(mes = as.character(lubridate::month(data_interval_start, label=T, abbr = F)), NA) %>%
        mutate(fijadas_periodo = (f1 >= data_interval_start & f1 < data_interval_end & str_detect(tr, "fijad")),
               programadas_periodo = (f2 >= data_interval_start & f2 < data_interval_end & str_detect(tr, "fijad")),
               realizadas_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "1", 
               frac_nodomic_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "0", 
               frac_incompdte_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "2", 
               rac_incompddo_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "3") %>% # Concordia Cargó todo como fijada
        group_by(circunscripcion, mes, resp) %>%
        summarise(fijadas = sum(fijadas_periodo, na.rm = T),
                  programadas = sum(programadas_periodo, na.rm = T),
                  realizadas = sum(realizadas_periodo, na.rm = T), 
                  fracasada_inexist_domicil = sum(frac_nodomic_periodo, na.rm = T), 
                  fracasada_incomparec_denunciante = sum(frac_incompdte_periodo, na.rm = T), 
                  fracasada_incomparec_denunciado = sum(rac_incompddo_periodo, na.rm = T)) %>%
        mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>% 
        janitor::adorn_totals("row")
      
      
      audic_resultado <- resultado %>%
        filter(str_detect(tr, "realiz")) %>%
        mutate(mes = as.character(lubridate::month(data_interval_start, label=T, abbr = F)), NA) %>%
        filter(f2 >= data_interval_start, f2 < data_interval_end) %>% 
        rename(fraud = f2, estaud = t1, ra = t2, vmed = t3, tmed = t4,
               duracm = n1, duracb = n2) %>%
        #codconver("IGMP", "estaud") %>%
        codconver("IGMP", "ra") %>%
        codconver("IGMP", "vmed") %>%
        codconver("IGMP", "tmed") %>% 
        mutate(resp = ifelse(is.na(resp), "sin dato", resp))
      
      
      audic_result$audic_resultado_acsac <- audic_resultado %>% 
        group_by(circunscripcion, mes, resp, ra) %>%
        summarise(cantidad_audiencias = n()) %>% 
        tidyr::spread(ra, cantidad_audiencias, fill = 0)
      
      audic_result$audic_resultado_vmed <- audic_resultado %>% 
        group_by(circunscripcion, mes, resp, vmed) %>%
        summarise(cantidad_audiencias = n()) %>% 
        tidyr::spread(vmed, cantidad_audiencias, fill = 0)
      
      audic_result$audic_resultado_tmed <- audic_resultado %>% 
        group_by(circunscripcion, mes, resp, tmed) %>%
        summarise(cantidad_audiencias = n()) %>% 
        tidyr::spread(tmed, cantidad_audiencias, fill = 0)
      
      realizadas <- audic_result
      
      
    } else {
      
      audic_result$audic_xestado <- resultado %>%
        mutate(fijadas_periodo = (f1 >= data_interval_start & f1 < data_interval_end & str_detect(tr, "fijad")),
               programadas_periodo = (f2 >= data_interval_start & f2 < data_interval_end & str_detect(tr, "fijad")),
               realizadas_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "1", 
               frac_nodomic_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "0", 
               frac_incompdte_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "2", 
               rac_incompddo_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
                 str_detect(tr, "realizad") & t1 == "3") %>% # Concordia Cargó todo como fijada
        group_by(circunscripcion, resp) %>%
        summarise(fijadas = sum(fijadas_periodo, na.rm = T),
                  programadas = sum(programadas_periodo, na.rm = T),
                  realizadas = sum(realizadas_periodo, na.rm = T), 
                  fracasada_inexist_domicil = sum(frac_nodomic_periodo, na.rm = T), 
                  fracasada_incomparec_denunciante = sum(frac_incompdte_periodo, na.rm = T), 
                  fracasada_incomparec_denunciado = sum(rac_incompddo_periodo, na.rm = T)) %>%
        mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>% 
        janitor::adorn_totals("row")
      
      
      audic_resultado <- resultado %>%
        filter(str_detect(tr, "realiz")) %>%
        filter(f2 >= data_interval_start, f2 < data_interval_end) %>% 
        rename(fraud = f2, estaud = t1, ra = t2, vmed = t3, tmed = t4,
               duracm = n1, duracb = n2) %>%
        #codconver("IGMP", "estaud") %>%
        codconver("IGMP", "ra") %>%
        codconver("IGMP", "vmed") %>%
        codconver("IGMP", "tmed") %>% 
        mutate(resp = ifelse(is.na(resp), "sin dato", resp))
      
      
      audic_result$audic_resultado_acsac <- audic_resultado %>% 
        group_by(circunscripcion, resp, ra) %>%
        summarise(cantidad_audiencias = n()) %>% 
        tidyr::spread(ra, cantidad_audiencias, fill = 0)
      
      audic_result$audic_resultado_vmed <- audic_resultado %>% 
        group_by(circunscripcion,  resp, vmed) %>%
        summarise(cantidad_audiencias = n()) %>% 
        tidyr::spread(vmed, cantidad_audiencias, fill = 0)
      
      audic_result$audic_resultado_tmed <- audic_resultado %>% 
        group_by(circunscripcion, resp, tmed) %>%
        summarise(cantidad_audiencias = n()) %>% 
        tidyr::spread(tmed, cantidad_audiencias, fill = 0)
      
      realizadas <- audic_result
      
    }
    
  }

  if(exists("realizadas_xtipo")){
    realizadas_xtipo <<- realizadas_xtipo
  }
  realizadas
  
  
}
