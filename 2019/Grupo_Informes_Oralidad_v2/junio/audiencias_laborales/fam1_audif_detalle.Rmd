---
title: Audiencias - Estados y Resultados
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}

audif <- audifam_prim(jdos_fam, 
             start_date, 
             end_date)

audifam_realizadas_xtipo(audif, desagregacion_mensual = desagregacion_mensual)

```


## Audiencias según sus distintos estados finales - `r getDataIntervalStr(start_date, end_date)`

Según definición del STJER sobre este indicador los estados finales de audiencia son los siguientes con su correspondiente definición:

+ 1 = Audiencia Fijada: es la audiencia fijada y notificada en una causa o proceso;  
+ 2 = Audiencia Realizada: audiencia efectivamente celebrada;   
+ 3 = Audiencia Fracasada por incomparencia de partes;  
+ 4 = Audiencia Fracasada por incomparencia terceros (ej. COPNAF)  
+ 5 = Audiencia Cancelada: audiencia que estaba fijada y se dejó sin efecto a pedido de parte.  
+ 6 = Audiencia No Realizada por falta de notificación.   

```{r}
audifam_xestado(audif, desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```

## Audiencias Realizadas y sus Resultados `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se muestran las audiencias realizadas y sus resultados por organismo.   

Los resultados posibles según definición institucional son:

En procesos con contradicción:

+ Conciliación Total  
+ Conciliación Parcial   
+ Sin conciliación y sin Apelación  
+ Sin conciliación y con Apelación  

Además de los anteriores se registra un tipo especial de conciliación en procesos sin contradicción que aparece discriminado. 

```{r}
audifam_realizadas_xresultado(audif, desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias Realizadas y sus Resultados") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```

## Audiencias realizadas por Tipo `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presenta información de las audiencias realizadas para los distintos tipos de audiencia. Para las audiencias relativas a Violencia Familiar y de Genero los datos se informan hasta octubre, fecha a partir de la cual el registro asociado a estos procesos se concentra en el Centro Judicial de Genero conforme Res.22/10/18 (se está trabajando en la integración de dicha información).


```{r}
audifam_realizadas_xtipo(audif, desagregacion_mensual = desagregacion_mensual) %>% 
  mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Audiencias Realizadas por Tipo") %>%
  column_spec(2, "3cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape()
```

## Minutos en Audiencia - `r getDataIntervalStr(start_date, end_date)`
 
En esta tabla se presenta información de los minutos en audiencia por magistrado.

```{r}
minutos_audiencias %>% 
  mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Minutos en Audiencia del Magistrado") %>%
  column_spec(1, "2cm") %>% column_spec(2, "2cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape()
```


## Audiencias Videofilmadas `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan las audiencias videofilmadas a partir del mes de octubre 2018, mes a partir del cual entró en vigencia el Protocolo de Oralidad en Familia -cfrme. Ac.Gral 30/18.

```{r}
audifam_realizadas_video_xtipo(audif, desagregacion_mensual = desagregacion_mensual) %>% 
  mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Audiencias Videofilmadas Realizadas") %>%
  column_spec(2, "3cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape()
```

## Audiencias materia Violencia `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan las audiencias en materia de Violencia informadas por el REJUCAV dependiente del Centro Judicial de Género. Este información comenzó a relevarse por parte del registro a partir de noviembre 2018. 

```{r}
audifam_violencias(jdos_fam, start_date, end_date) %>% 
  outputTable(caption = "Audiencias en materia de Violencia") %>% 
  landscape()
  
```

