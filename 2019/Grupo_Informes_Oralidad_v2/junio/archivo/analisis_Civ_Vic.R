library(readr)
library(dplyr)
# Caso Civil Victoria

# Causas en Agenda provista por ellos vía imagen por mail del miércoles 05/06/2019 13:27
agenda_CivVic <- c("14057","13684","13225","12345","11877",
                   "13084","12812","12466","12914","12284",
                   "12523","12184","13343","13415","13709","11473","13968")

# Información en Justat obtenida para el Informe de Oralidad
oralidad_consolidado_abril <- read_csv("2019/Grupo_Informes_Oralidad/mayo/oralidad_consolidado_abril.csv")
oralidad_consolidado_abril <- oralidad_consolidado_abril %>% filter(iep == "jdocco0000vic")

# Informadas por ellos en Justat
aud_informadas <- oralidad_consolidado_abril$nro


#Cuales de las de la Agenda, no está informada en Justat:
agenda_CivVic[!agenda_CivVic %in% aud_informadas]


# Audiencias del Informe que no tienen fecha de Realización de Preliminar
oralidad_consolidado_abril$nro[is.na(oralidad_consolidado_abril$fecha_efect_preliminar)]



# comandos varios
aud_informadas %in% agenda_CivVic
View(oralidad_consolidado_abril)

