---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}
existepres <- existepresentacion(jdo_pna, start_date, end_date, "CIPM")
```


```{r, eval = existepres}
inic_pna <- iniciadas_pen(poblacion = jdo_pna,
              start_date = start_date,
              end_date = end_date)
```

## Causas Iniciadas `r getDataIntervalStr(start_date, end_date)`

```{r, eval = existepres}
inic_pna %>%
  filter(circunscripcion != "Total") %>% 
  group_by(circunscripcion, organismo) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  janitor::adorn_totals("row") %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
```

```{r, eval= !existepres}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "No registra información presentada en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, color = "red") %>%
  column_spec(2, width = "30em")
    
```



