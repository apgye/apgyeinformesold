start_date = "2018-02-01"
end_date = "2019-01-01"
poblacion <- jdos_fam
desagregacion_mensual <- T

table(resultado$ra)
table(resultado$ta)

ta <- resultado$ta

nchar(ta)
resultado_taanomalo <- resultado %>% 
  mutate(nchar = nchar(ta)) %>% 
  filter(nchar > 3 )


resultado_limpio <- resultado %>% 
  mutate(ta = str_replace_all(ta, "[:blank:]", "")) %>% 
  mutate(ta = str_replace_all(ta, "[:alpha:]|[:punct:]", "#")) %>% 
  mutate(ta = str_trim(ta)) %>% 
  tidyr::separate_rows(ta, sep = "#")
 
  
resultado_limpio <- resultado %>% 
  mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>% 
  mutate(duracm = str_trim(duracm))

as.data.frame(table(resultado$duracm))
as.data.frame(table(resultado_limpio$duracm))



resultado <- DB_PROD() %>% 
  apgyeTableData("AUDIF") %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% poblacion$organismo) %>% 
  filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>% 
  mutate(fea = dmy(fea)) %>% mutate(audvid = as.integer(audvid)) %>% 
  filter(fea > data_interval_start, fea < data_interval_end) %>% 
  select(-data_interval_start, -data_interval_end, -justiables) %>% 
  collect() %>% 
  mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>% 
  mutate(duracm = str_trim(duracm)) %>% 
  mutate(mat = str_sub(toupper(mat), 1,1)) %>% 
  mutate(ra = str_sub(toupper(ra), 1,1)) %>% 
  mutate(ta = str_replace_all(ta, "[:blank:]", "")) %>% 
  mutate(ta = str_replace_all(ta, "[:alpha:]|[:punct:]", "#")) %>% 
  mutate(ta = str_trim(ta)) %>% 
  mutate(indice = row_number()) %>% 
  tidyr::separate_rows(ta, sep = "#")  
 


audifam_prim <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02"){
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIF") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% poblacion$organismo) %>%
    filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
    mutate(fea = dmy(fea)) %>% mutate(audvid = as.integer(audvid)) %>%
    filter(fea > data_interval_start, fea < data_interval_end) %>%
    select(-data_interval_start, -data_interval_end, -justiables) %>%
    collect() %>%
    mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
    mutate(duracm = str_trim(duracm)) %>%
    mutate(mat = str_sub(toupper(mat), 1,1)) %>%
    mutate(ra = str_sub(toupper(ra), 1,1)) %>%
    mutate(ta = str_replace_all(ta, "[:blank:]", "")) %>%
    mutate(ta = str_replace_all(ta, "[:alpha:]|[:punct:]", "#")) %>%
    mutate(ta = str_trim(ta)) 
  
  resultado
}


#audiencias <- audifam_prim(poblacion, start_date, end_date)


audifam_xestado <- function(df, desagregacion_mensual = TRUE) {
  
  df <- df %>% 
    mutate(esta = case_when(esta == "1" ~ "fijada", 
                            esta == "2" ~ "realizada",
                            esta == "3" ~ "fracasada_incomp_parte",
                            esta == "4" ~ "fracasada_incomp_terceros",
                            esta == "5" ~ "cancelada",
                            esta == "6" ~ "no_realizada_xfalta_notific",
                            TRUE ~ "sd"))
  
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>%
      group_by(organismo, mes, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes,
             fijada, realizada, fracasada_incomp_parte, fracasada_incomp_terceros, 
             no_realizada_xfalta_notific, cancelada, -organismo) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col") 
    
    resultado
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, 
             fijada, realizada, fracasada_incomp_parte, fracasada_incomp_terceros, 
             no_realizada_xfalta_notific, cancelada, -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audiest <- audifam_prim(poblacion, start_date, end_date) %>% 
  audifam_xestado(desagregacion_mensual = F)


audifam_realiz_xmateria <- function(df, desagregacion_mensual = TRUE) {
  
  df <- df %>% 
    filter()
    mutate(mat = case_when(mat == "F" ~ "familia",
                           mat == "P" ~ "penal",
                           TRUE ~ "sd")) 
  
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>%
      group_by(organismo, mes, mat) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(mat, cantidad, drop = F) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes,
            everything(), -organismo) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col") 
    
    resultado
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, mat) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(mat, cantidad) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, 
             everything(), -organismo) %>%  
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}


audimat <- audiencias <- audifam_prim(poblacion, start_date, end_date) %>% 
  audifam_xmateria(desagregacion_mensual = F) 


audimat_2 <- audiencias <- audifam_prim(poblacion, start_date, end_date) %>% 
  audifam_realizadas_xmateria(desagregacion_mensual = F) 



# audifam_estado_resultado <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02", desagregacion_mensual = TRUE) {
#   
#    resultado <- DB_PROD() %>% 
#      apgyeTableData("AUDIF") %>% 
#      apgyeDSL::interval(start_date, end_date) %>% 
#      filter(iep %in% poblacion$organismo) %>% 
#      filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>% 
#      mutate(fea = dmy(fea)) %>% mutate(audvid = as.integer(audvid)) %>% 
#      filter(fea > data_interval_start, fea < data_interval_end) %>% 
#      select(-data_interval_start, -data_interval_end, -justiables) %>% 
#      collect() %>% 
#      mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>% 
#      mutate(duracm = str_trim(duracm)) %>% 
#      mutate(mat = str_sub(toupper(mat), 1,1)) %>% 
#      mutate(ra = str_sub(toupper(ra), 1,1)) %>% 
#      mutate(ta = str_replace_all(ta, "[:blank:]", "")) %>% 
#      mutate(ta = str_replace_all(ta, "[:alpha:]|[:punct:]", "#")) %>% 
#      mutate(ta = str_trim(ta)) %>% 
#      # muy importante estoy multiplicando casos informados por acumulacion de codigos en ta, obliga a consolidar
#      mutate(indice = row_number()) %>% 
#      tidyr::separate_rows(ta, sep = "#") %>% 
#      mutate(ta = case_when(
#        ta == "1" ~ "Control de Legalidad Internaciones/Externaciones",
#        ta == "2" ~  "Escucha Niño/Niñas/Adolescentes",
#        ta == "3" ~  "Escucha Personas c/Capacidad Restringida",
#        # ta == "4" ~  "Escucha al Denunciante Violencia Familiar o de Género",
#        # ta == "5" ~  "Escucha al Denunciado",
#        ta == "6" ~  "Control de Legalidad Medidas Excepcionales",
#        ta == "7" ~  "Adoptabilidad",
#        ta == "8" ~  "Proceso de Vinculación",
#        ta == "9" ~  "Guarda con Fines de Adopción",
#        ta == "10" ~  "Adopción",
#        ta == "11" ~  "Audiencia de Conciliación",
#        ta == "12" ~  "Audiencia Art.70 Preliminar(ej. Filiación)",
#        ta == "13" ~  "Audiencias en otros procesos voluntarios",
#        ta == "14" ~  "Audiencia de Divorcio",
#        ta == "15" ~  "Juicio Oral",
#        ta == "16" ~  "Audiencia de Alimentos",
#        # ta == "17" ~  "Otras",
#        ta == "20" ~ "Formulación/Imputación", 
#        ta == "21" ~ "Audiencia Conclusiva",
#        ta == "22" ~ "Suspensión de Juicio a Prueba",
#        ta == "23" ~ "Medidas",
#        ta == "24" ~ "Remisión a Juicio",
#        ta == "25" ~ "Juicio Abreviado",
#        ta == "26" ~ "Remisión Judicial",
#        ta == "27" ~ "Juicio Oral",
#        ta == "28" ~ "Integración de Sentencia",
#        # ta == "29" ~ "Otra",
#        TRUE ~ "Otras"), 
#        esta = case_when(esta == "1" ~ "fijada", 
#                         esta == "2" ~ "realizada",
#                         esta == "3" ~ "fracasada_incomp_parte",
#                         esta == "4" ~ "fracasada_incomp_terceros",
#                         esta == "5" ~ "cancelada",
#                         esta == "6" ~ "no_realizada_xfalta_notific",
#                         TRUE ~ "sd"),
#        ra = case_when(ra == "T" ~ "conciliacion_total",
#                       ra == "P" ~ "conciliacion_parcial", 
#                       ra == "S" ~ "sin_conciliacion(sin apelacion)",
#                       ra == "A" ~ "sin_conciliacion(con apelacion)", 
#                       ra == "C" ~ "conciliacion(en_proceso_sin_contradiccion)"), 
#        mat = case_when(mat == "F" ~ "familia",
#                        mat == "P" ~ "penal",
#                        TRUE ~ "sd")) 
#      
#    if(desagregacion_mensual) {
#      resultado <- resultado %>% 
#        mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>% 
#        left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
#                                                                  "circunscripcion")], 
#                  by = "organismo") %>%
#        group_by(organismo, mes)
#    } else {
#      resultado <- resultado %>% 
#        mutate(organismo = iep) %>% 
#        left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
#                                                                  "circunscripcion")], 
#                  by = "organismo") %>%
#        group_by(organismo)
#    } 
#    
#    
#      if(desagregacion_mensual) {
#      
#     # filtro por  indice dado que multiplique los casos para los informes acumulados de ta
#     # consolidacion
#        
#      resultado_xmateria <- resultado %>% 
#        ungroup() %>% distinct(indice, .keep_all = T) %>% 
#        group_by(circunscripcion, organismo_descripcion, mes, mat) %>% 
#        summarise(cantidad = n()) %>%
#        tidyr::spread(mat, cantidad) %>% 
#        select(circunscripcion, organismo_descripcion, mes, everything()) %>% 
#        group_by(circunscripcion, organismo_descripcion) %>% 
#        do(janitor::adorn_totals(.)) %>%
#        ungroup() %>% 
#        janitor::adorn_totals("col")
#      
#      
#     
#      
#      
#      resultado_xresultado <<- resultado %>% 
#        ungroup() %>% distinct(indice, .keep_all = T) %>%
#        group_by(circunscripcion, organismo_descripcion, mes, ra) %>% 
#        summarise(cantidad = n()) %>%
#        tidyr::spread(ra, cantidad) %>% 
#        select(circunscripcion, organismo_descripcion, mes, everything()) %>% 
#        group_by(circunscripcion, organismo_descripcion) %>% 
#        do(janitor::adorn_totals(.)) %>%
#        ungroup() %>% 
#        janitor::adorn_totals("col")
#      
#      
#      # no filtro por indice 
#      
#      resultado_xtipo <<- resultado %>% 
#        arrange(circunscripcion, organismo_descripcion, mes) %>% 
#        group_by(circunscripcion, organismo_descripcion, mes, ta) %>% 
#        summarise(cantidad = n()) %>%
#        tidyr::spread(ta, cantidad) %>% 
#        select(circunscripcion, organismo_descripcion, mes, everything(), -indice) %>% 
#        group_by(circunscripcion, organismo_descripcion) %>% 
#        do(janitor::adorn_totals(.)) %>%
#        ungroup() %>% 
#        janitor::adorn_totals("col")
#      
#      
#      # trabajo con audvid generado df_temp de fracciones de duraciones para los ta acumulados
#      
#      df_temp <- resultado %>% 
#        filter(audvid == 1) %>% 
#        group_by(indice) %>% 
#        summarise(n = n(), duracm = unique(duracm)) %>% 
#        mutate(duracm = as.integer(duracm)) %>% 
#        rowwise() %>% 
#        mutate(durac_consolidado = duracm / n) %>% 
#        select(indice, durac_consolidado)  
#        
#      resultado_xaudvid <- resultado %>% 
#        filter(audvid == 1) %>% 
#        select(-audvid) %>% 
#        left_join(df_temp, by = "indice")
#      
#     
#      
#        
#        
#        
#        tidyr::spread(ta, cantidad) %>% 
#        select(circunscripcion, organismo_descripcion, mes, everything(), -indice) %>% 
#        group_by(circunscripcion, organismo_descripcion) %>% 
#        do(janitor::adorn_totals(.)) %>%
#        ungroup() %>% 
#        janitor::adorn_totals("col")
#      
#      
#    } else {
#      
#     
#    }
#    
#    resultado_xmateria
#    
#    resultado_xestado
#    
#    resultado_xresultado
#    
#    resultado_xtipo
#    
# #   
# # solo con videofilmada calculo minutos 
#   
# }


audiresultado <- audifam_prim(poblacion, start_date, end_date) 
df <- audiresultado



audifam_prim(poblacion, start_date, end_date) %>% 
  audifam_realizadas_xtipo(desagregacion_mensual = T) %>% View()



