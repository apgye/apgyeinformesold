---
title: Causas Iniciadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se detallan por tipos de proceso los casos ingresados en las Cámaras de Paraná y Uruguay, incluyendo a los casos vinculados a la Ley Responsabilidad del Estado y cuestiones de Competencia.    


```{r }
iniciados_camcad(poblacion = cam_cad, 
                 start_date = start_date,
                 end_date = end_date) %>%
  outputTable(caption = "Causas Iniciadas") %>%
  column_spec(2, "10cm") %>% 
  row_spec(0) 

```

