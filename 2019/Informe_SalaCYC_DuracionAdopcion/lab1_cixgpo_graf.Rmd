---
title: Causas Iniciadas ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

### Causas Iniciadas: Grafico `r getDataIntervalStr(start_date, end_date)`

```{r, echo=FALSE}
iniciados_lab(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date) %>%
  iniciados_lab_xg() %>%
  inic_lab_graf_xg()

```
