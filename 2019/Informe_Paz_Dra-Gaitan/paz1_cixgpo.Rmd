---
title: Causas y Trámites iniciados ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")

if (nrow(jdos_paz) == 0) {
  NoPopulation <- TRUE
} else {
  NoPopulation <- FALSE
}
```


## Causas Iniciadas por Grupo de Procesos  - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura.

Hacemos notar que se incorpora en esta sección del Boletín Juzgados de Paz de 2a y 3a categoría debido a la conversión de su estructura de datos similar a un juzgado de paz de 1a. categoría.

Adicionalmente se detallan los Órganos que migraron de versión de Lex: el Juzgado de Paz de **Crespo** informa en ésta sección desde Junio 2019 y los Juzgados de Paz de **Oro Verde** y **San Benito** desde Julio 2019. Los períodos anteriores se representan en la Sección correspondiente a los Juzgados de Paz de 2da y 3er Cat.

```{r }

iniciados_paz_procxgpo(poblacion = jdos_paz,
                                            start_date = start_date,
                                            end_date = end_date) %>% 
  filter(tipo_proceso !=  "subtotal") %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
  
```

\pagebreak

`r if (ymd(start_date) >= make_date(2019,01,01)) '## Causas Iniciadas - competencia en Familia (a partir de jun-2019)'`

Órganos con competencia asignada en 2019.

```{r }
iniciados_paz_fam(poblacion = jdos_paz,
                                  start_date = start_date,
                                  end_date = end_date) %>% 
  filter(tipo_proceso !=  "subtotal") %>% 
  kable(caption = "Causas Iniciadas en materia Familia", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
  
```

## Trámites Iniciados - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestra el total de trámites voluntarios iniciados por juzgado.

```{r }
iniciados_paz_tramxgpo(poblacion = jdos_paz,start_date = start_date,
                       end_date = end_date) %>% 
  filter(tipo_proceso !=  "subtotal") %>% 
  kable(caption = "Trámites Iniciados por Grupo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(3, "4cm") %>% #column_spec(4, "3cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
  
```

