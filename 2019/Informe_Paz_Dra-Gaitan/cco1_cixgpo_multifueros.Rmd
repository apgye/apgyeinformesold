---
title: Causas Iniciadas ordenadas por Grupos en Órganos Multifueros
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")

multifueros_fam <- any(jdos_cco$organismo %in% c("jdocco0000fed", "jdocco0000ssa", "jdocco0000vpa"))


resultado <- iniciados_multifuero_labfam(poblacion = jdos_cco,
              start_date = start_date,
              end_date = end_date)
```

## Causas Iniciadas en Órganos Multifuero - `r getDataIntervalStr(start_date, end_date)`


```{r, inic_multifuero, eval = ci}
resultado$inic_multifuero %>% 
  iniciados_multifuero_comparativo("laboral", cfecha = desagregacion_mensual) %>%
  kable(caption = "Causas Iniciadas Agrupadas materia Laboral", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```


```{r, cco1_cixgrpo_multifuero_fam, eval = ci & multifueros_fam, echo=FALSE }
resultado$inic_multifuero %>% 
  iniciados_multifuero_comparativo("familia", cfecha = desagregacion_mensual) %>%
  kable(caption = "Causas Iniciadas Agrupadas materia Familia", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```


```{r, cco1_cixgpo_multifuero_tsci, eval = tsci}
resultado$inic_ccolab_pc %>% 
  tsci_g(finicio, org_facet = org_facet)

```


```{r, cco1_cixgrpo_multifuero_fam_tsci, eval = multifueros_fam & tsci, echo=FALSE }
resultado$inic_multi_famlab_pc  %>% 
  tsci_g(finicio, org_facet = org_facet)

```


```{r, inic_ccolab_pc, eval = pc}
resultado$inic_ccolab_pc %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(4, "6cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```


```{r, inic_multi_famlab_pc, eval =  multifueros_fam & pc}
resultado$inic_multi_famlab_pc %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(4, "6cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```
