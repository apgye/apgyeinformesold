jdos_civiles <- listar_organismos() %>% 
  filter(tipo == "jdo", str_detect(materia, "cco" )) %>% 
  filter(!organismo %in% c("jdocco0501con", "jdocco0502con",  "jdocco0301uru", 
                           "jdocco0302uru", "jdocco0901pna", "jdocco0902pna", 
                           "jdocco1001pna", "jdocco1002pna"))

# Procesos de Conocimiento
tipos_proceso <- apgyeOperationsJusER::Tipo_procesos %>%
  filter(materia == "civil y comercial") %>% 
  select(tipo_de_procesos,proceso_conocimiento) 

iniciados_conocimiento <- iniciados_cco(poblacion = jdos_civiles, start_date = start_date, end_date = end_date) %>%  
  filter(tipo_proceso != "subtotal") %>% 
  left_join(tipos_proceso, by=c("tipo_proceso"="tipo_de_procesos")) 

iniciados_noconocimiento_xgpo <- iniciados_conocimiento %>% 
  filter(!proceso_conocimiento) %>% 
  group_by(grupo_proceso) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  bind_rows(tribble(~grupo_proceso, ~cantidad_iniciados, "otros", sum(.$cantidad_iniciados[.$cantidad_iniciados < 10]))) %>% 
  filter(cantidad_iniciados >= 10) %>% 
  janitor::adorn_totals("row") %>% 
  left_join(janitor::adorn_percentages(., "col"), by = "grupo_proceso") %>% 
  rename(cantidad_iniciados = cantidad_iniciados.x, porcentaje = cantidad_iniciados.y) %>% 
  mutate(porcentaje = str_c(round(porcentaje * 100, digits = 2), " %"))

iniciados_noconocimiento <- iniciados_conocimiento %>% 
  filter(!proceso_conocimiento) %>% 
  group_by(tipo_proceso) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  # bind_rows(tribble(~tipo_proceso, ~cantidad_iniciados, "otros", sum(.$cantidad_iniciados[.$cantidad_iniciados < 10]))) %>% 
  # filter(cantidad_iniciados >= 10) %>% 
  janitor::adorn_totals("row") %>% 
  left_join(janitor::adorn_percentages(., "col"), by = "tipo_proceso") %>% 
  rename(cantidad_iniciados = cantidad_iniciados.x, porcentaje = cantidad_iniciados.y) %>% 
  mutate(porcentaje = str_c(round(porcentaje * 100, digits = 2), " %"))

iniciados_conocimiento_resumen <- iniciados_conocimiento %>% 
  group_by(proceso_conocimiento) %>% 
  summarise(cantidad = sum(cantidad, na.rm =T)) %>% 
  rename(tipo_de_proceso = proceso_conocimiento) %>% 
  mutate(tipo_de_proceso = ifelse(tipo_de_proceso, "proc. de conocimiento", "otros tipos de proceso")) %>% 
  mutate(tipo_de_proceso = ifelse(is.na(tipo_de_proceso), "sin_dato", tipo_de_proceso)) %>% 
  janitor::adorn_totals("row") %>% 
  left_join(janitor::adorn_percentages(., "col"), by = "tipo_de_proceso") %>% 
  rename(cantidad_iniciados = cantidad.x, porcentaje = cantidad.y) %>% 
  mutate(porcentaje = str_c(round(porcentaje * 100, digits = 2), " %"))


iniciados_conocimiento_2018 <- iniciados_cco(poblacion = jdos_civiles, "2018-07-01", "2019-01-01") %>%  
  filter(tipo_proceso != "subtotal") %>% 
  left_join(tipos_proceso, by=c("tipo_proceso"="tipo_de_procesos")) 

iniciados_noconocimiento_xgpo_2018 <- iniciados_conocimiento_2018 %>% 
  filter(!proceso_conocimiento) %>% 
  group_by(grupo_proceso) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  bind_rows(tribble(~grupo_proceso, ~cantidad_iniciados, "otros", sum(.$cantidad_iniciados[.$cantidad_iniciados < 10]))) %>% 
  filter(cantidad_iniciados >= 10) %>% 
  janitor::adorn_totals("row") %>% 
  left_join(janitor::adorn_percentages(., "col"), by = "grupo_proceso") %>% 
  rename(cantidad_iniciados = cantidad_iniciados.x, porcentaje = cantidad_iniciados.y) %>% 
  mutate(porcentaje = str_c(round(porcentaje * 100, digits = 2), " %"))

iniciados_noconocimiento_2018 <- iniciados_conocimiento_2018 %>% 
  filter(!proceso_conocimiento) %>% 
  group_by(tipo_proceso) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  # bind_rows(tribble(~tipo_proceso, ~cantidad_iniciados, "otros", sum(.$cantidad_iniciados[.$cantidad_iniciados < 10]))) %>% 
  # filter(cantidad_iniciados >= 10) %>% 
  janitor::adorn_totals("row") %>% 
  left_join(janitor::adorn_percentages(., "col"), by = "tipo_proceso") %>% 
  rename(cantidad_iniciados = cantidad_iniciados.x, porcentaje = cantidad_iniciados.y) %>% 
  mutate(porcentaje = str_c(round(porcentaje * 100, digits = 2), " %"))

iniciados_conocimiento_resumen_2018 <- iniciados_conocimiento_2018 %>% 
  group_by(proceso_conocimiento) %>% 
  summarise(cantidad = sum(cantidad, na.rm =T)) %>% 
  rename(tipo_de_proceso = proceso_conocimiento) %>% 
  mutate(tipo_de_proceso = ifelse(tipo_de_proceso, "proc. de conocimiento", "otros tipos de proceso")) %>% 
  mutate(tipo_de_proceso = ifelse(is.na(tipo_de_proceso), "sin_dato", tipo_de_proceso)) %>% 
  janitor::adorn_totals("row") %>% 
  left_join(janitor::adorn_percentages(., "col"), by = "tipo_de_proceso") %>% 
  rename(cantidad_iniciados = cantidad.x, porcentaje = cantidad.y) %>% 
  mutate(porcentaje = str_c(round(porcentaje * 100, digits = 2), " %"))

