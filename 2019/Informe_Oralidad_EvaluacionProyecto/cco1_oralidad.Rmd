---
title: Oralidad
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
source("~/apgyeinformes/2019/Grupo_Informes_Oralidad_v2/utilsO.R")
source("oralidad.R")
```

```{r}
## Casos Oralizados
jdos_civiles <- listar_organismos() %>% 
  filter(tipo == "jdo", str_detect(materia, "cco" )) %>% 
  filter(!organismo %in% c("jdocco0501con", "jdocco0502con",  "jdocco0301uru", 
                           "jdocco0302uru", "jdocco0901pna", "jdocco0902pna", 
                           "jdocco1001pna", "jdocco1002pna"))

#Casos2018
df_casos_base <- readRDS("~/apgyeinformes/2019/Grupo_Informes_Oralidad_v2/data/oralidad2018.rds")
#Preliminares
audic_prelim_raw <- preliminaresO(jdos_civiles, "2019-02-01", end_date)
# Audiencia Vista Causa
audic_vcausa_raw <- vcausaO(jdos_civiles, "2019-02-01", end_date)
# Resoluciones
resoluciones <- resolucionesO(jdos_civiles, "2019-02-01", end_date)
# Consolidado
# imputs df_1ros_casos, audic_prelim_raw, audic_vcausa_raw
consolidado <- consolidar(df_casos_base, audic_prelim_raw, audic_vcausa_raw, resoluciones)


consolidado_cfmeMINJUS <- consolidado %>% 
  left_join(jdos_civiles[, c("organismo", "organismo_descripcion", 
                           "circunscripcion")], by = c("iep" = "organismo")) %>% 
  select(circunscripcion, organismo_descripcion, everything()) %>% 
  mutate(organismo = str_remove_all(iep, "[a-z,0]")) %>%  
  mutate(fecha_resolucion = as.character(fecha_resolucion)) %>%
  #reimputación solicitada por el ministerio siempre que resultado preliminar T, 
  #forma de resolucion = Conciliacion y fecha de efectivizacion
  mutate(forma_deresolucion =  ifelse(resultado_preliminar == "T" & 
                                        !is.na(resultado_preliminar), 
                                      "Conciliación", forma_deresolucion), 
         fecha_resolucion =  ifelse(resultado_preliminar == "T" & 
                                      is.na(fecha_resolucion), 
                                    as.character(fecha_efect_preliminar), 
                                    fecha_resolucion)) %>% 
  mutate(forma_deresolucion =  ifelse(resultado_vcausa == "T" & 
                                        !is.na(resultado_vcausa), 
                                      "Conciliación", forma_deresolucion),
         fecha_resolucion =  ifelse(resultado_vcausa == "T" & 
                                      is.na(fecha_resolucion), 
                                    as.character(fecha_efect_vcausa), 
                                    fecha_resolucion)) %>% 
   select(circunscripcion, organismo_descripcion, iep, organismo, everything()) %>% 
   arrange(circunscripcion, organismo_descripcion)  

resumen <- consolidado_cfmeMINJUS %>% 
  group_by(circunscripcion, organismo_descripcion) %>% 
  summarise(cantidad_procesos_en_oralidad_informados = length(unique(nro)),
    cantidad_preliminares_realizadas = sum(!is.na(fecha_efect_preliminar) &
                                             estado_preliminar == "realizada"),
    cantidad_preliminares_no_realizadas = sum(!is.na(fecha_efect_preliminar) &
                                             estado_preliminar == "no_realizada"),
    cantidad_vista_causa_programadas = sum(!is.na(fprogramada_vista_causa)),
    cantidad_vista_causa_realizadas = sum(!is.na(fecha_efect_vcausa) &
                                            estado_vcausa == "realizada"),
    cantidad_vista_causa_no_realizadas = sum(!is.na(fecha_efect_vcausa) &
                                            estado_vcausa == "no_realizada"), 
    cantidad_conciliaciones_enAPoAVC = sum(forma_deresolucion == "Conciliación", na.rm = T),
    cantidad_sentencias_procesos_en_oralidad = sum(!is.na(fecha_resolucion) &
                                                     forma_deresolucion != "Conciliación")) %>% 
  select(circunscripcion, organismo_descripcion, everything()) %>% 
  arrange(circunscripcion, organismo_descripcion)  %>% 
  janitor::adorn_totals("row")

consolidado_informesSTJ <- consolidado_cfmeMINJUS %>%
  filter(estado_preliminar == "realizada" | estado_vcausa == "realizada") %>%
  select(circunscripcion, organismo_abbrev = organismo_descripcion, nro_expte = nro,
          fecha_ingreso, fecha_efect_preliminar, resultado_preliminar, apertura_prueba,
          fprogramada_vista_causa, sinVC_PrInfDoc, fecha_efect_vcausa, resultado_vcausa,
          fecha_resolucion, as, forma_deresolucion) %>% 
  mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6), 
         organismo_abbrev = abbreviate(organismo_abbrev, minlength = 13)) 


consolidado_comparativo <- consolidado %>% 
  agrupartproc(materia = "civil") %>% 
  group_by(iep, gtproc_forma, tproc) %>% 
  summarise(cantidad = n()) %>% 
  arrange(gtproc_forma, desc(cantidad)) %>% 
  do(janitor::adorn_totals(.)) %>% 
  ungroup() %>% 
  mutate(tproc = ifelse(gtproc_forma == "-", "subtotal", tproc)) %>% 
  left_join(jdos_civiles %>% select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  select(circunscripcion, organismo, grupo_proceso = gtproc_forma, tipo_proceso = tproc, everything(), -iep) %>% 
  iniciados_cco_comparativo() 


# segunda instancia

resoluciones_cco2_prim <- function(poblacion, operacion = "CADR1C", start_date, end_date) {
  operacion = rlang::enexpr(operacion)
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    resolverconvertidos() %>% 
    mutate(finicio = dmy(finicio), fres = dmy(fres), fdesp1 = dmy(fdesp1)) %>% 
    collect() %>% 
    mutate(as= toupper(as)) %>% 
    filter(!str_detect(tproc, "AMPARO|VIOLENCIA"))
    
  resultado <- resultado %>%
    filter(!is.na(fres)) %>% 
    mutate(control_intervalo =  fres >= data_interval_start & fres < data_interval_end) %>% 
    filter(tres != "0") %>%
    select(organismo = iep, nro, caratula, finicio, fecha_despacho = fdesp1, sentencia_camara = fres, as_camara = as) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], by = "organismo") %>% 
    select(circunscripcion, organismo_descripcion, everything(), iep = organismo)
    
  
  resultado
}
resoluciones_cco2 <- resoluciones_cco2_prim(poblacion = cam_civil, operacion = "CADR2C",
                      start_date = start_date, end_date = end_date) 

agregar_inic_cam <- function(df, poblacion) {
  
  inic_cam <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData("CIE2") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!cam_civil$organismo) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(fecha = dmy(fecha)) %>%
    filter(fecha >= start_date, fecha < end_date ) %>% 
    select(iep, nro1, nro2, caratula, tproc, ibs, fecha)  %>% 
    filter(ibs == "1") %>% 
    collect() %>% 
    rename(nro = nro2) 
    
  df <- df %>% 
    #left_join(inic_cam %>% select(nro, caratula_1w, fecha), by = c("nro", "caratula_1w")) %>%
    left_join(inic_cam %>% select(nro, iep, fecha), by = c("nro", "iep")) %>% 
    rename(fecha_ingreso = fecha) %>% 
    filter(fecha_ingreso < sentencia_camara)
    
  df
  
}

resoluciones_cco2_cfinic <- agregar_inic_cam(resoluciones_cco2, cam_civil)

durac_cam <- resoluciones_cco2_cfinic %>% 
  mutate(duracion = sentencia_camara - fecha_ingreso) %>% 
  group_by(as_camara) %>% 
  summarise(meses_duracion = round(median(as.integer(duracion)/30, na.rm = T)), 
            #casos_nro = str_c(nro, collapse = ", "),
            cantidad_casos = n()) %>% 
  rename(resolucion = as_camara) %>% 
  filter(!is.na(resolucion)) %>% 
  mutate(resolucion = ifelse(resolucion == "A", "Autos", "Sentencias"))

# duraciones
duraciones_prim <- consolidado_cfmeMINJUS %>% 
  filter(fecha_ingreso > make_date(2000,01,01)) %>% # se trabaja con los casos iniciados en el 2018
  mutate(tproc = ifelse(str_detect(tproc, "DAÑOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "(CIVIL)"), "ORDINARIO", tproc)) %>%
  mutate(resultado_preliminar = toupper(str_sub(resultado_preliminar, 1, 1))) %>% 
  mutate(resultado_vcausa = toupper(str_sub(resultado_vcausa, 1, 1))) %>% 
  mutate(fecha_resolucion = ymd(fecha_resolucion)) 

# Conciliados en AP
durac_conciliados_enAP <- duraciones_prim %>% 
  filter(estado_preliminar == "realizada", resultado_preliminar == "T") %>% 
  mutate(duracion = fecha_efect_preliminar - fecha_ingreso) %>% 
  group_by(circunscripcion, organismo_descripcion, iep) %>% 
  summarise(meses_duracion = round(median(as.integer(duracion)/30, na.rm = T)), 
            casos_nro = str_c(nro, collapse = ", "),
            cantidad_casos = n()) %>% 
  mutate(tipo_finalizacion = "conciliación en APreliminar")

durac_conciliados_enAP_resumen <- duraciones_prim %>% 
  filter(estado_preliminar == "realizada", resultado_preliminar == "T") %>% 
  mutate(duracion = fecha_efect_preliminar - fecha_ingreso) %>% 
  summarise(meses_duracion = round(median(as.integer(duracion)/30, na.rm = T)), 
            cantidad_casos = n()) %>% 
  mutate(tipo_finalizacion = "conciliación en APreliminar")

# Conciliados en VC
  
durac_conciliados_enVC <- duraciones_prim %>% 
  filter(estado_vcausa == "realizada", resultado_vcausa == "T") %>% 
  mutate(duracion = fecha_efect_vcausa - fecha_ingreso) %>% 
  group_by(circunscripcion, organismo_descripcion, iep) %>% 
  summarise(meses_duracion = round(median(as.integer(duracion)/30, na.rm = T)), 
            casos_nro = str_c(nro, collapse = ", "),
            cantidad_casos = n()) %>% 
  mutate(tipo_finalizacion = "conciliación en VCausa")
  

durac_conciliados_enVC_resumen <- duraciones_prim %>% 
  filter(estado_vcausa == "realizada", resultado_vcausa == "T") %>% 
  mutate(duracion = fecha_efect_vcausa - fecha_ingreso) %>% 
  summarise(meses_duracion = round(median(as.integer(duracion)/30, na.rm = T)), 
            cantidad_casos = n()) %>% 
  mutate(tipo_finalizacion = "conciliación en VCausa")

# Resueltos en Sentencia
durac_sentencias <- duraciones_prim %>% 
  filter((resultado_vcausa == "S" | is.na(resultado_vcausa)) &
         (resultado_preliminar == "S" | is.na(resultado_vcausa))) %>% 
  filter(!is.na(fecha_resolucion)) %>% 
  mutate(duracion = fecha_resolucion - fecha_ingreso) %>% 
  group_by(circunscripcion, organismo_descripcion, iep) %>% 
  summarise(meses_duracion = round(median(as.integer(duracion)/30, na.rm = T)), 
            casos_nro = str_c(nro, collapse = ", "),
            cantidad_casos = n()) %>% 
  mutate(tipo_finalizacion = "sentencia")
  
durac_sentencias_resumen <- duraciones_prim %>% 
  filter(resultado_vcausa == "S" & resultado_preliminar == "S" ) %>% 
  filter(!is.na(fecha_resolucion)) %>% 
  mutate(duracion = fecha_resolucion - fecha_ingreso) %>% 
  summarise(meses_duracion = round(median(as.integer(duracion)/30, na.rm = T)), 
            cantidad_casos = n()) %>% 
  mutate(tipo_finalizacion = "sentencia")


# Consolidados
duraciones_oralidad_resumen <- bind_rows(durac_conciliados_enAP_resumen, 
                                 durac_conciliados_enVC_resumen,
                                 durac_sentencias_resumen) %>% 
  select(tipo_finalizacion, everything()) %>%
  mutate(casos = paste0(round(cantidad_casos/sum(cantidad_casos), digit = 2) * 100, "%"))

duraciones_oralidad_long <- bind_rows(durac_conciliados_enAP, 
                                 durac_conciliados_enVC, 
                                 durac_sentencias) %>% 
  select(circunscripcion, organismo = organismo_descripcion, tipo_finalizacion, everything(), -iep) %>% 
  arrange(circunscripcion, organismo, tipo_finalizacion)

duraciones_oralidad_wide <- duraciones_oralidad_long %>% 
  select(circunscripcion, organismo, meses_duracion, tipo_finalizacion) %>% 
  tidyr::spread("tipo_finalizacion", "meses_duracion", fill = "sd")


```


## Procesos "Oralizables" 

En las próximas tablas se presentan totales de causas iniciadas en los órganos civiles y comerciales (excluyendo a juzgados de Ejecución, Concursos y Quiebras) clasificadas en procesos de conocimiento y otros tipos de proceso.    

Con esta información procuramos precisar el alcance actual y potencial que tiene la oralización de procesos civiles, en diálogo con la propuesta elevada por las coordinadoras de Oralidad pto.4.3.    

### Período `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados_conocimiento_resumen %>% 
  kable(caption = "Procesos de Conocimiento Iniciados en el Período", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

*** 

```{r}
iniciados_noconocimiento_xgpo %>% 
  kable(caption = "Detallle por grupos de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

***

```{r}
iniciados_noconocimiento %>% 
  kable(caption = "Detallle desagrupado de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)%>% 
  landscape()
```

\pagebreak

### Período julio - diciembre 2018

```{r}
iniciados_conocimiento_resumen_2018 %>% 
  kable(caption = "Procesos de Conocimiento Iniciados en el Período", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

*** 

```{r}
iniciados_noconocimiento_xgpo_2018 %>% 
  kable(caption = "Detallle por grupos de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

***

```{r}
iniciados_noconocimiento_2018 %>% 
  kable(caption = "Detallle desagrupado de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0) %>% 
  landscape()
```

\pagebreak

## Datos de Avance del Proyecto de Oralidad a la fecha: `r end_date`

```{r}
kable(resumen, caption = "Resumen", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90)
```

\pagebreak


## Duración de Procesos en Oralidad

Para el cálculo de duraciones de los procesos informados en Oralidad se adoptó la siguiente metodología:

+ para excluir valores anómalos en las duraciones se excluyeron casos informados en oralidad con fecha de inicio del proceso anterior al año 2000,   
+ para contar con una medida de referencia no sesgada de la duraciones de procesos por organismo se emplea -como es habitual- la mediana estadística (resistente a valores extresmo), y finalmente,     
+ La duración es una equivalencia en meses de los días corridos que tuvo el caso desde su fecha de inicio hasta la conciliación en audiencia (preliminar o vista de causa) o sentencia. Su formula es *dias_corridos/30*.   


#### Resumen Provincial de la Duración de Procesos en Oralidad Primera Instancia

```{r}
duraciones_oralidad_resumen %>% 
  kable(caption = "Meses de duración del proceso desde inicio hasta conciliación o sentencia)", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  
```

#### Resumen Provincial de la Duración de Procesos en Segunda Instancia

```{r}
durac_cam %>% 
  kable(caption = "Meses de duración del proceso desde su ingreso en Cámara hasta resolución", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  
```


### Meses de duración de procesos: Tabla Resumen por Organismo 

```{r}
duraciones_oralidad_wide %>% 
  kable(caption = "Meses de duracion del proceso desde inicio hasta conciliación o sentencia", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90)
```

\pagebreak

### Meses de duración de procesos: Tabla General por Organismo y número de expediente

En la próxima tabla se puede consultar por organismo y número/s de expediente las duraciones en meses que se informan. 


```{r}
duraciones_oralidad_long %>% 
  kable(caption = "Meses de duracion del proceso desde inicio hasta conciliación o sentencia", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>% 
  column_spec(5, "6cm") %>% 
  landscape()
```

\pagebreak

## Conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

```{r, size = 'tiny'}
kable(consolidado_informesSTJ, caption = "Tabla General", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 8, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>%
  landscape() 
```


