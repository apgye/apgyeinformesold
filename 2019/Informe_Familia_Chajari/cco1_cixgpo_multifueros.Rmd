---
title: Causas Iniciadas ordenadas por Grupos en Órganos Multifueros
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")

multifueros_fam <- any(jdos_cco$organismo %in% c("jdocco0000fed", "jdocco0000ssa", "jdocco0000vpa"))

```

## Causas Iniciadas en Órganos Multifuero - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura.

```{r}
iniciados_multifuero_labfam(poblacion = jdos_cco,
              start_date = start_date,
              end_date = end_date) %>%
  iniciados_multifuero_comparativo("laboral") %>%
  kable(caption = "Causas Iniciadas Agrupadas materia Laboral", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```


```{r, conditional_block, eval = multifueros_fam, echo=FALSE }
iniciados_multifuero_labfam(poblacion = jdos_cco,
              start_date = start_date,
              end_date = end_date) %>%
  iniciados_multifuero_comparativo("familia") %>%
  kable(caption = "Causas Iniciadas Agrupadas materia Familia", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```




