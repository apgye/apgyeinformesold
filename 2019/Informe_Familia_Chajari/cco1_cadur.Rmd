---
title: Duracion de Procesos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Duracion de Procesos por Organismo: Resumen Comparativo - `r getDataIntervalStr(start_date, end_date)`

La duración de los procesos es una medición importante de la actividad judicial cuyo análisis requiere precisión. A tal fin, expondremos a continuación la metodología empleada para este resumen comparativo:

+ Los casos incluidos en la tabla comprenden a todos los procesos resueltos por cada organismo en el período considerado, con exclusión de los siguientes procesos: Incidentales, Cautelares, Sucesorios, Procesos Constitucionales, Beneficios de Litigar Sin Gastos, Interdictos, Homologaciones, Preparación de Vias, Segundo Testimonio, Oficios y Exhortos, y tipos no acordes a la legislación vigente (eg.CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO, PERSONAL).    
+ Se practicaron reimputación de tipos de procesos por errores de registración de los organismos (e.g. el *ORDINARIO CIVIL* se reimputó como *ORDINARIO*).   
+ La duración de los procesos resueltos por cada organismo a través del dictado de sentencia definitiva se calcula como la diferencia en días corridos entre la fecha de inicio de la causa y la fecha del dictado de la sentencia según declaración del organismo.    
+ Finalmente, a fin de evitar datos sesgados por la presencia de valores extremos se optó por la mediana estadística para este análisis y se excluyeron procesos por organismo con menos de dos casos resueltos. 

Ulteriores análisis podrían requerir el detalle particular de las duraciones de todos los procesos resueltos mediante sentencia por órganismo.  

```{r}
jdos_cco_solo <- jdos_cco %>% 
  filter(!str_detect(materia, "eje|cqb"))
         
duracion(jdos_cco_solo, 
         "CADR1C", 
         start_date = start_date,
         end_date = end_date,  
         desagregacion_mensual = F, 
         desagregacion_xorg = T) %>% 
  filter(cantidad_casos > 2) %>% 
  durac_subselec("civil") %>% 
  select(-cantidad_casos) %>% 
  tidyr::spread(tproc, mediana_duracion) %>% 
  # kable(caption = "Duración de Procesos", align = 'c', longtable = TRUE ) %>% 
  # kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
  #               full_width = F, font_size = 10)  %>% 
  # column_spec(3, "10cm") %>%
  outputTable(caption = "Duración de Procesos") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```

## Duracion de Procesos por Organismo: Detalle General - `r getDataIntervalStr(start_date, end_date)`

```{r}
jdos_cco_solo <- jdos_cco %>% 
  filter(!str_detect(materia, "eje|cqb"))
         
duracion(jdos_cco_solo, 
         "CADR1C", 
         start_date = start_date,
         end_date = end_date,  
         desagregacion_mensual = F, 
         desagregacion_xorg = T) %>% 
    # kable(caption = "Duración de Procesos", align = 'c', longtable = TRUE ) %>% 
  # kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
  #               full_width = F, font_size = 10)  %>% 
  # column_spec(3, "10cm") %>%
  outputTable(caption = "Duración de Procesos") %>% 
  row_spec(0, angle = 90) 
```


