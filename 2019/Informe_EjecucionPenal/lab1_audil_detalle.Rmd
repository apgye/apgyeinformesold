---
title: Audiencias - Estados, resultados y tiempos en audiencia
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r}

audil <- audilab_prim(jdos_lab, 
             start_date, 
             end_date) 
audilab_realizadas_xtipo_cantidad_minutos(audil, desagregacion_mensual = desagregacion_mensual) 
```

## Audiencias según sus distintos estados finales - `r getDataIntervalStr(start_date, end_date)`


```{r}

audilab_xestado(audil, desagregacion_mensual = T) %>% 
  select(-fijada) %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```


## Audiencias realizadas por Tipo - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presenta información de las audiencias realizadas para los distintos tipos de audiencia. Más abajo se detallan los resultados obtenidos por tipo de audiencia.

```{r}
cantidad_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Cantidad de Audiencias") %>%
  landscape()
```

## Minutos en Audiencia - `r getDataIntervalStr(start_date, end_date)`
 
En esta tabla se presenta información de los minutos en audiencia por magistrado.

```{r}
minutos_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Minutos en Audiencia del Magistrado") %>%
  landscape()
```


## Audiencias Realizadas y sus Resultados `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se muestran las audiencias de Conciliación y Vista de Causa realizadas y sus resultados por organismo.   

Los resultados posibles según definición institucional son:

+ Conciliación Total  
+ Conciliación Parcial   
+ Sin conciliación 

```{r}
audilab_realizadas_xtipoyresultado(audil, desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Audiencias Realizadas y sus Resultados") %>%
  landscape()
```
