
iniciadas_pen <- function(poblacion, start_date="2019-02-01", end_date = "2019-03-01") {
  
  if(any(str_detect(poblacion, "oga"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIGTIA') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio), fhecho = dmy(fhecho)) %>%
      filter(iep %in% poblacion$organismo) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      mutate(organismo = "Garantías")
      
    
    inic_prim <<- inic
    
    inic <- inic %>% 
      group_by(circunscripcion, organismo, tproc) %>%
      summarise(cantidad = n(), durac_hec_inic= median(finicio-fhecho, na.rm = T)) %>%
      mutate(durac_hec_inic = ifelse(durac_hec_inic > 0 , as.character(durac_hec_inic), NA) ) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(desc(cantidad)) %>% 
      do(janitor::adorn_totals(.)) 
    
    inic
    
  } 
  else if(any(str_detect(poblacion, "tja"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIJUI') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(fingj = dmy(fingj), ffdebab = dmy(ffdebab), frdebab = dmy(frdebab)) %>%
      collect() %>% 
      left_join(poblacion_penal %>% 
                  select(iep=organismo, circunscripcion), by="iep") %>% 
      left_join(poblacion_penal %>% 
                  filter(str_detect(organismo, "tja")) %>% 
                  select(organismo_descripcion, circunscripcion), by="circunscripcion") %>% 
      rename(organismo = organismo_descripcion)
    
    inic_prim <<- inic
    
    inic <- inic %>% 
      group_by(circunscripcion, organismo, tproc) %>%
      summarise(cantidad = n()) %>%  
                # durac_ingreso_fij_dbte = median(ffdebab-fingj, na.rm = T)) %>% 
               #durac_fij_realizacion_dbte = median(frdebab-ffdebab, na.rm = T)) %>%
      #mutate(durac_ingreso_fij_dbte = ifelse(durac_ingreso_fij_dbte > 0 , as.character(durac_ingreso_fij_dbte), NA)) %>%
      #mutate(durac_fij_realizacion_dbte = ifelse(durac_fij_realizacion_dbte > 0 , as.character(durac_fij_realizacion_dbte), NA)) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(desc(cantidad)) %>% 
      do(janitor::adorn_totals(.)) 
  
    inic_ap <- DB_PROD() %>% 
      apgyeTableData('CIAP') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(fiap  = dmy(fiap)) %>%
      collect() %>% 
      left_join(poblacion_penal %>% 
                  select(iep=organismo, circunscripcion), by="iep") %>% 
      left_join(poblacion_penal %>% 
                  filter(str_detect(organismo, "tja")) %>% 
                  select(organismo_descripcion, circunscripcion), by="circunscripcion") %>% 
      rename(organismo = organismo_descripcion) %>% 
      group_by(circunscripcion, organismo, tproc) %>%
      summarise(cantidad = n()) %>%  
      # durac_ingreso_fij_dbte = median(ffdebab-fingj, na.rm = T)) %>% 
      #durac_fij_realizacion_dbte = median(frdebab-ffdebab, na.rm = T)) %>%
      #mutate(durac_ingreso_fij_dbte = ifelse(durac_ingreso_fij_dbte > 0 , as.character(durac_ingreso_fij_dbte), NA)) %>%
      #mutate(durac_fij_realizacion_dbte = ifelse(durac_fij_realizacion_dbte > 0 , as.character(durac_fij_realizacion_dbte), NA)) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(desc(cantidad)) %>% 
      do(janitor::adorn_totals(.)) 
    
    inic_ap <<- inic_ap
    
    inic
    
    
  } 
  
  else if(any(str_detect(poblacion, "campen"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData("CINC3P") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      left_join(poblacion %>% 
                    select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
    
      inic_prim <<- inic
      
      inic <- inic %>% 
        group_by(circunscripcion, organismo, tdelito) %>%
        summarise(cantidad = n()) %>%
        arrange(desc(cantidad)) %>% 
        do(janitor::adorn_totals(.)) 
      
     inic
  } 
  else if(any(str_detect(poblacion, "sal"))) {
    
    inicp <- DB_PROD() %>% 
      apgyeTableData("CIN3P") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      mutate(materia = "penal")
    
    inicc <- DB_PROD() %>% 
      apgyeTableData("CIN3PC") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      left_join(poblacion %>% 
                    select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      mutate(materia = "constitucional")
   
     inic <- bind_rows(inicc, if(exists("inicp")) inicp)
       
     inic_prim <<- inic
       
     inic <- inic %>% 
         group_by(circunscripcion, organismo, materia, tproc) %>%
         summarise(cantidad = n()) %>%
         arrange(desc(cantidad)) %>% 
         do(janitor::adorn_totals(.)) 
       
     inic
    }
 
  if(exists("inic_ap")){
    inic_ap <<- inic_ap
    
  }
  
  inic_prim <<- inic_prim
  
  inic
}
