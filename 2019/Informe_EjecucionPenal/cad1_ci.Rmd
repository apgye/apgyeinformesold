---
title: Causas Iniciadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

Las Cámaras comienzan a informar bajo el nuevo sistema estadístico a partir de junio 2018.


```{r }
iniciados_camcad(poblacion = cam_cad, 
                 start_date = start_date,
                 end_date = end_date) %>%
  outputTable(caption = "Causas Iniciadas") %>%
  column_spec(2, "10cm") %>% 
  row_spec(0) 

```

