---
title: Audiencias
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
audip_oma <- audienciasPenales(poblacion = oma, 
             start_date = make_date(2019,08,01), 
             end_date = end_date, 
             desagregacion_mensual = desagregacion_mensual) 
```


## Audiencias según Estados Finales - `r getDataIntervalStr(make_date(2019,08,01), end_date)`

```{r}
audip_oma$audic_xestado %>% 
  outputTable(caption = "Audiencias Realizadas y Duración") %>%
  row_spec(0, angle = 90) 
  # landscape()
```

## Audiencias realizadas por Resultado - `r getDataIntervalStr(make_date(2019,08,01), end_date)`

```{r}
audip_oma$audic_resultado_acsac %>% 
  select(-sin_dato) %>% 
  outputTable(caption = "Audiencias OMA") %>% 
  row_spec(0, angle = 90)
  # landscape()
```

## Audiencias desagregadas según manifestación de voluntad sobre mediacion - `r getDataIntervalStr(make_date(2019,08,01), end_date)`

```{r}
audip_oma$audic_resultado_vmed %>% 
  select(-sin_dato) %>% 
  outputTable(caption = "Audiencias OMA") %>% 
  row_spec(0, angle = 90)
  # landscape()
```

## Audiencias desagregadas según complejidad - `r getDataIntervalStr(make_date(2019,08,01), end_date)`

La complejidad de la audiencia se asocia a la cantidad de partes intervinientes:

+ **Mediación Compleja**: más de 4 partes
+ **Mediación Simple**: menos de 4 partes

```{r}
audip_oma$audic_resultado_tmed %>% 
  select(-sin_dato) %>% 
  outputTable(caption = "Audiencias OMA") %>% 
  row_spec(0, angle = 90) 
  # landscape()
```



