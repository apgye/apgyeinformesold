---
title: Resoluciones
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## Resoluciones - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_pen(poblacion = jdo_pep,
              start_date = start_date,
              end_date = end_date, 
              desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = str_c("Resoluciones"," (", getDataIntervalStr(start_date, end_date), ")")) %>%
  row_spec(0, angle = 90) %>% 
  #column_spec(3, "12cm") %>%  
  # column_spec(9, border_right = T) %>% 
  landscape()
  
```

