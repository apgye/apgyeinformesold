# modelo 

audifam <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02", desagregacion_mensual = TRUE) {
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIF") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(fea = dmy(fea)) %>% 
    filter(fea > data_interval_start, fea < data_interval_end) %>% 
    filter(esta == "2") %>%
    collect() 
  
  resultado <- resolverconvertidos(resultado)
  
  resultado <- resultado  %>% 
    mutate(tipo_audiencia = case_when(
      ta == "1" ~ "Control de Legalidad Internaciones/Externaciones",
      ta == "2" ~  "Escucha Niño/Niñas/Adolescentes",
      ta == "3" ~  "Escucha Personas c/Capacidad Restringida",
      ta == "4" ~  "Escucha al Denunciante Violencia Familiar o de Género",
      ta == "5" ~  "Escucha al Denunciado",
      ta == "6" ~  "Control de Legalidad Medidas Excepcionales",
      ta == "7" ~  "Adoptabilidad",
      ta == "8" ~  "Proceso de Vinculación",
      ta == "9" ~  "Guarda con Fines de Adopción",
      ta == "10" ~  "Adopción",
      ta == "11" ~  "Audiencia de Conciliación",
      ta == "12" ~  "Audiencia Art.70 Preliminar(ej. Filiación)",
      ta == "13" ~  "Audiencias en otros procesos voluntarios",
      ta == "14" ~  "Audiencia de Divorcio",
      ta == "15" ~  "Juicio Oral (proyectada)",
      ta == "16" ~  "Audiencia de Alimentos",
      # ta == "17" ~  "Otras",
      ta == "20" ~ "Formulación/Imputación", 
      ta == "21" ~ "Audiencia Conclusiva",
      ta == "22" ~ "Suspensión de Juicio a Prueba",
      ta == "23" ~ "Medidas",
      ta == "24" ~ "Remisión a Juicio",
      ta == "25" ~ "Juicio Abreviado",
      ta == "26" ~ "Remisión Judicial",
      ta == "27" ~ "Juicio Oral",
      ta == "28" ~ "Integración de Sentencia",
      # ta == "29" ~ "Otra",
      TRUE ~ "Otras"
    ))
 
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo") %>%
      group_by(organismo, mes, tipo_audiencia)
  } else {
    resultado <- resultado %>% 
      mutate(organismo = iep) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo") %>%
      group_by(organismo, tipo_audiencia)
  } 
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo_descripcion, mes) %>% 
      group_by(circunscripcion, organismo_descripcion, mes, tipo_audiencia) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(tipo_audiencia, cantidad) %>% 
      select(circunscripcion, organismo_descripcion, mes, everything()) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col")
  } else {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      group_by(circunscripcion, organismo_descripcion, tipo_audiencia) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(tipo_audiencia, cantidad) %>% 
      select(circunscripcion, organismo_descripcion, everything()) %>% 
      janitor::adorn_totals("col")
  }
  
  resultado
  
}

audifam_resumen <- function(audifamDF) {
  audifamDF %>%
    filter(!str_detect(circunscripcion, "Total")) %>%
    group_by(tipo_audiencia) %>%
    summarise(audiencias_realizadas = sum(cantidad, na.rm = T)) %>%
    ungroup() %>% 
    mutate(porcentaje = stringr::str_c(round( audiencias_realizadas / sum(audiencias_realizadas, na.rm = T) * 100), ' %')) %>% 
    arrange(desc(audiencias_realizadas))
}

audifam_resumen_xjdo <- function(audifamDF) {
  audifamDF %>%
    filter(!str_detect(circunscripcion, "Total")) %>%
    arrange(circunscripcion, organismo) %>% 
    group_by(circunscripcion, organismo, tipo_audiencia) %>%
    summarise(audiencias_realizadas = sum(cantidad, na.rm = T)) %>%
    do(janitor::adorn_totals(.)) %>%
    ungroup() %>% 
    mutate(circunscripcion = ifelse(circunscripcion == "-", "Total", circunscripcion)) %>% 
    ungroup() 
    
}


# reeelaboacion para informes específicos

audifam_prim <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02"){
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIF") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% poblacion$organismo) %>%
    filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
    mutate(fea = dmy(fea)) %>% mutate(audvid = as.integer(audvid)) %>%
    filter(fea > data_interval_start, fea < data_interval_end) %>%
    select(-data_interval_start, -data_interval_end, -justiables) %>%
    collect() 
  
  resultado <- resolverconvertidos(resultado)
  
  resultado <- resultado %>%
    mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
    mutate(duracm = str_trim(duracm)) %>%
    mutate(mat = str_sub(toupper(mat), 1,1)) %>%
    mutate(ra = str_sub(toupper(ra), 1,1)) %>%
    mutate(ta = str_replace_all(ta, "[:blank:]", "")) %>%
    mutate(ta = str_replace_all(ta, "[:alpha:]|[:punct:]", "#")) %>%
    mutate(ta = str_trim(ta)) %>% 
    mutate(esta = str_remove_all(esta, "[:alpha:]|[:punct:]")) %>%
    mutate(esta = str_trim(esta)) %>% 
    codconver("AUDIF", "esta") %>% 
    codconver("AUDIF", "ra") %>% 
    codconver("AUDIF", "mat") 
    
  resultado
}

audifam_xestado <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), -organismo) %>% 
      group_by(circunscripcion, organismo_descripcion)
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audifam_realizadas_xmateria <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") 
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, mat) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(mat, cantidad, drop = F) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), -organismo) 
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, mat) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(mat, cantidad) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, 
             everything(), -organismo) %>%  
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audifam_realizadas_xresultado <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") 
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes,
             conciliacion_total, conciliacion_parcial, sin_conciliacion_y_sin_apelacion,
             sin_conciliacion_con_apelacion, conciliacion_en_proceso_sin_contradiccion, everything(), -organismo) 
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, 
             conciliacion_total, conciliacion_parcial, sin_conciliacion_y_sin_apelacion,
             sin_conciliacion_con_apelacion, conciliacion_en_proceso_sin_contradiccion, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audifam_realizadas_xtipo <- function(df, desagregacion_mensual = TRUE) {

  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    # importante estoy multiplicando casos informados por acumulacion de codigos en ta, obliga a consolidar
    mutate(indice = row_number()) %>% 
    tidyr::separate_rows(ta, sep = "#") %>% 
    filter(!(ta %in% c("4", "5") & fea > as.Date("2018-10-01"))) %>% # exclusion de casos de violencia
    codconver("AUDIF", "ta")
  
  #trabajo df_temp de fracciones de duraciones para los ta acumulados

  df_temp <- df %>% 
    group_by(indice) %>%
    summarise(n = n(), duracm = unique(duracm)) %>%
    mutate(duracm = as.integer(duracm)) %>%
    rowwise() %>%
    mutate(durac_consolidado = duracm / n) %>%
    select(indice, durac_consolidado)
  
  df <- df %>% 
    left_join(df_temp, by = "indice")

  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
  } else {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    
   
  }
 
  minutos_audiencias <<- minutos_audiencias
  cantidad_audiencias <<- cantidad_audiencias
 
}

audifam_realizadas_video_xtipo <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    filter(audvid == 1) %>%
    # importante estoy multiplicando casos informados por acumulacion de codigos en ta, obliga a consolidar
    mutate(indice = row_number()) %>% 
    tidyr::separate_rows(ta, sep = "#") %>% 
    codconver("AUDIF", "ta") 
    
  #trabajo df_temp de fracciones de duraciones para los ta acumulados
  
  df_temp <- df %>% 
    group_by(indice) %>%
    summarise(n = n(), duracm = unique(duracm)) %>%
    mutate(duracm = as.integer(duracm)) %>%
    rowwise() %>%
    mutate(durac_consolidado = duracm / n) %>%
    select(indice, durac_consolidado)
  
  df <- df %>% 
    left_join(df_temp, by = "indice")
  
  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      filter(fea > as.Date("2018-10-01")) %>%  # fecha a partir de la cual se aprueba protocol familia
      group_by(organismo, mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      filter(fea > as.Date("2018-10-01")) %>%  # fecha a partir de la cual se aprueba protocol familia
      group_by(organismo, mes, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
    
  } else {
    
    cantidad_audiencias <<- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias <<- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    
    
  }
  
  minutos_audiencias <<- minutos_audiencias
  cantidad_audiencias <<- cantidad_audiencias
  
}

audifam_violencias <- function(poblacion, start_date, end_date) {

  start_date <- as.Date(start_date, format = "%Y-%m-%d")
  inicio <- paste0("'", strftime(start_date, "%d-%m-%Y"), "'")
  
  end_date <- as.Date(end_date, format = "%Y-%m-%d") - 1
  fin <- paste0("'", strftime(end_date, "%d-%m-%Y"), "'")
  
  query1 <- paste("select * from audiencias_concretadas_9198(",inicio,",",fin,")")
  
  result1 <- DB_REJUCAV() %>% 
    dbGetQuery(query1) %>% 
    mutate(audiencia_tipo = "violencia_familiar")

  query2 <- paste("select * from audiencias_concretadas_10058(",inicio,",",fin,")")
  
  result2 <-  DB_REJUCAV() %>% 
    dbGetQuery(query2) %>% 
    mutate(audiencia_tipo = "violencia_c-mujer")
  
  if(str_detect(poblacion$organismo, "fam")) {
    resul <- result1 %>% 
      bind_rows(result2) %>% 
      filter(str_detect(tolower(juzgado), "fam")) %>% 
      select(jurisdiccion, juzgado, audiencia_tipo, everything()) %>% 
      rename(cantidad_audiencias = cantidad) %>% 
      mutate(juzgado = str_to_title(juzgado), jurisdiccion = str_to_title(jurisdiccion)) %>% 
      arrange(jurisdiccion, juzgado, desc(cantidad_audiencias)) %>% 
      group_by(jurisdiccion, juzgado) %>% 
      do(janitor::adorn_totals(.))
  }  else if(str_detect(poblacion$organismo, "paz")) {
    resul <- result1 %>% 
      bind_rows(result2) %>% 
      filter(str_detect(tolower(juzgado), "paz")) %>% 
      select(jurisdiccion, juzgado, audiencia_tipo, everything()) %>% 
      rename(cantidad_audiencias = cantidad) %>% 
      mutate(juzgado = str_to_title(juzgado), jurisdiccion = str_to_title(jurisdiccion)) %>% 
      arrange(jurisdiccion, juzgado, desc(cantidad_audiencias)) %>% 
      group_by(jurisdiccion, juzgado) %>% 
      do(janitor::adorn_totals(.))
    resul
  }  else if(str_detect(poblacion$organismo, "cco")) {
    resul <- result1 %>%
      bind_rows(result2) %>%
      filter(str_detect(tolower(juzgado), "juzgado civil"), str_detect(jurisdiccion, "SALVADOR|FEDERACION|ISLAS")) %>%
      select(jurisdiccion, juzgado, audiencia_tipo, everything()) %>%
      rename(cantidad_audiencias = cantidad) %>%
      mutate(juzgado = str_to_title(juzgado), jurisdiccion = str_to_title(jurisdiccion)) %>%
      arrange(jurisdiccion, juzgado, desc(cantidad_audiencias)) %>%
      group_by(jurisdiccion, juzgado) %>%
      do(janitor::adorn_totals(.))
    resul
  }
  
  resul

}



