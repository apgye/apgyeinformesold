write.table(jdos_paz_23, "jdospaz.csv", row.names = F, col.names = T, sep = ",")

library(ggplot2)
library(ggmap)
library(RColorBrewer)
library(purrr)
library(stringr)
library(tibble)

#https://rpubs.com/Grolthas/maps
# Unidades Territoriales
# https://infra.datos.gob.ar/catalog/modernizacion/dataset/7/distribution/7.3/download/departamentos.json

# https://infra.datos.gob.ar/catalog/modernizacion/dataset/7/distribution/7.3/download/departamentos.json
# library(RJSONIO)
# library(RCurl)
# # grab the data
# raw_data <- getURL("https://infra.datos.gob.ar/catalog/modernizacion/dataset/7/distribution/7.3/download/departamentos.json")
# # Then covert from JSON into a list in R
# data <- fromJSON(raw_data)
# length(data)
# # We can coerce this to a data.frame
# final_data <- do.call(rbind, data)
# str(final_data)
# # Then write it to a flat csv file
# write.csv(final_data, "final_data.csv")


# # resolvì errores de instalacion: sudo apt-get install libproj-dev libgdal-dev
# install.packages("rgdal")
# # resolvi problema instalando units: sudo apt-get install libudunits2-dev
# install.packages("sf")
# 
# library(tidyverse)
# library(sf)
# require(devtools)
# devtools::install_github("dkahle/ggmap", ref = "tidyup")
# library(ggmap)

#https://www.datanalytics.com/libro_r/ejemplos-1.html
# HoustonMap +
# stat_density2d(aes(x = lon, y = lat, fill = ..level.., alpha = ..level..),
#                size = 2, data = crimes.houston,
#                geom = "polygon"
# )

# Trabajando con provincia y dptos
# https://datosgobar.github.io/datos-lab/radios-censales-1/#fn1
# https://www.indec.gov.ar/ftp/cuadros/territorio/codgeo/Codgeo_Entre_Rios_con_datos.zip
# unzip("Codgeo_Entre_Rios_con_datos.zip")
# erios <- list.files()[[4]]
# radios_entrerios <- st_read(erios)
# radios_entrerios <- st_transform(radios_entrerios, crs = 4326)
# summary(radios_entrerios)
# 
# ggplot() + geom_sf(data = radios_entrerios) +
#   labs(title = "Radios Censales de Entre Ríos",
#        subtitle = "Fuente: INDEC")
# 
# library(leaflet)
# 
# leaflet() %>%
#   addTiles() %>%
#   addProviderTiles("CartoDB.Positron") %>%
#   # Mapeando los poligonos de Cordoba
#   addPolygons(data = radios_entrerios,
#               color = "grey", weight = 1, smoothFactor = 0.5,
#               opacity = 1.0)
# 

#https://journal.r-project.org/archive/2013-1/kahle-wickham.pdf

 

#############################################################################################

# descarga de archivo poligonos departamentos de Entre Rios
#https://analisisydecision.es/mapas-municipales-de-argentina-con-r/
# argentina = readRDS("gadm36_ARG_2_sp.rds")
# erios = argentina[argentina$NAME_1=="Entre Ríos",]
# 
# # guardar entre rios y convertirlo a vector 
# library(rgdal)
# saveRDS(erios, file = "erios.rds")
# 
# erios <- as.data.frame(erios)
# 
# causas_iniciadas_fueropaz_2018 <- causas_iniciadas_fueropaz_2018 %>% 
#   mutate(circunscripcion = str_replace_all(circunscripcion, "Chajarí", "Federación")) %>% 
#   group_by(circunscripcion) %>% 
#   summarise(cantidad_inicidados = sum(Total, na.rm = T)) %>% 
#   rename(NAME_2 = circunscripcion)
# 
# erios@data <- plyr::join(erios@data, causas_iniciadas_fueropaz_2018)
# 
# pal <- colorQuantile("YlGn", NULL, n = 5)
# state_popup <- paste0("<strong>Estado: </strong>", 
#                       argentina$NAME_1, 
#                       "<br><strong>Femicidios: </strong>", 
#                       argentina$Femicidios)
# 
# leaflet(data = erios) %>%
#   addProviderTiles("CartoDB.Positron") %>%
#   addPolygons(fillColor = ~pal(cantidad_inicidados), 
#               fillOpacity = 0.8, 
#               color = "#BDBDC3", 
#               weight = 1)
# 
# HoustonMap +geom_point(aes(x = lon, y = lat, 
#                            size = offense,colour = offense), data = violent_crimes )
ggmap(get_googlemap())  

# Consulta exitosa

ggmap(get_map("Entre Ríos, Argentina", source = "stamen", zoom = 7, maptype = "terrain")) 

jdospaz <- iniciadas_fueropaz_2018 %>% 
  left_join(poblacion_total %>% select(circunscripcion, localidad, organismo = organismo_descripcion, iep = organismo), 
            by = c("circunscripcion", "organismo")) %>% 
  mutate(provincia = "Entre Ríos") %>% 
  mutate(refgeo = str_c(localidad, circunscripcion, provincia, sep= ", " ))  %>% 
  mutate(iep = str_sub(iep, 11,13)) %>% 
  mutate(index = 1:length(iep)) %>% 
  mutate(categoria = as.factor(categoria))
  

#error_gname <- c("Bovril", "Estancia Grande", "Segui")
# jdospaz <- jdospaz %>% 
#   filter(!localidad %in% error_gname)

jgcode <- geocode(jdospaz$refgeo)



jdospaz <- jdospaz %>% 
  bind_cols(jgcode)

# Viale y Crespo gcode error
jgcode_manual <- tribble(
  ~localidad, ~lon,  ~lat,
  "Crespo", -60.307135,  -32.029790,
  "Viale", -60.008084,  -31.867907
)


jdospaz <- jdospaz %>% 
  left_join(jgcode_manual, by = "localidad") %>% 
  mutate(lon.x = ifelse(is.na(lon.y), lon.x, lon.y),
         lat.x = ifelse(is.na(lat.y), lat.x, lat.y)) %>% 
  select(-lon.y, -lat.y) %>% 
  rename(lon = lon.x, lat = lat.x)

# Construyendo todo
entrerios <- c(lon = -59.228658, lat = -31.872508)
ermap <- ggmap(get_googlemap(center = entrerios, source = "google",  maptype = "terrain", color = "bw", zoom = 7, 
                             size = c(640, 640), scale = 2, darken = c(0.1, "white")))  
#ermap <- ggmap(get_map(entrerios, source = "google",  maptype = "roadmap", color = "bw", zoom = 7))  
#ermap <- ggmap(get_map(entrerios, source = "stamen",  maptype = "watercolor", color = "bw", zoom = 7))  
#ermap <- ggmap(get_map(entrerios, source = "google",  maptype = "satellite", color = "bw", zoom = 7))  


# mapa de estructura jdos paz

library(plotly)
library(ggrepel) 

# Estructura
paz_estructura <- ermap + 
  geom_point(aes(x = lon, y = lat, shape = categoria, alpha = organismo, colour =categoria), data = jdospaz,
             alpha = 1, size = 2)+
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()) +
  labs(title = "Organos del Fuero de Paz 2018 (STJER-APGE)")
  
  
# mapa cantidad iniciados excluyendo Paraná y Concordia 1 cat
jdospaz_sinpna_conc <- jdospaz %>% 
  filter(Total <= 2500) %>% 
  rename(iniciados = Total)

# ermap +  
#   geom_point(aes(x = lon, y = lat, colour = Total, shape = categoria), data = jdospaz_sinpna_conc,
#              alpha = .5, size = 3) +
#   scale_colour_gradient(low = "yellow", high = "red") +
#   geom_text_repel(data = jdospaz, aes(x = lon, y = lat, label = index), 
#                   size = 3, vjust = 0, hjust = -0.5) +
#   theme(axis.title.x=element_blank(),
#         axis.text.x=element_blank(),
#         axis.ticks.x=element_blank()) +
#   labs(title = "Puntos calientes según cantidad de Causas Iniciadas en Juzgados de Paz 2018", 
#        subtitle = "(Excluye Jdos Paraná y Concordia)")

paz_iniciados <- ermap +  
  geom_point(data = jdospaz_sinpna_conc, aes(x = lon, y = lat, shape = categoria,
                                             colour =categoria, size = iniciados), 
             alpha = 1) +
  scale_size(range=c(0,10)) +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank()) +
  labs(title = "Causas Iniciadas Fuero de Paz 2018 (STJER-APGE)")
        

library(gridExtra)

grid.arrange(paz_estructura, paz_iniciados, nrow = 1)


movimientos_violencias(jdos_fam, "2018-01-01", "2019-01-01")


movimientos_violencias <- function(poblacion, start_date, end_date) {
  
  #Retorna la cantidad de movimientos Ley 9198 por jurisdicción y juzgado, según la fecha de los mismos, 
  # considerando las nuevas denuncias, medidas y resoluciones/sentencias.
  
  # Retorna la cantidad de movimientos Ley 10058 por jurisdicción y juzgado, según la fecha de los mismos, 
  # considerando las nuevas denuncias, medidas y resoluciones/sentencias.
  
  start_date <- as.Date(start_date, format = "%Y-%m-%d")
  inicio <- paste0("'", strftime(start_date, "%d-%m-%Y"), "'")
  
  end_date <- as.Date(end_date, format = "%Y-%m-%d") - 1
  fin <- paste0("'", strftime(end_date, "%d-%m-%Y"), "'")
  
  query1 <- paste("select * from movimientos_9198(",inicio,",",fin,")")
  
  result1 <- DB_REJUCAV() %>% 
    dbGetQuery(query1) %>% 
    mutate(movimientos_tipo = "violencia_familiar")
  
  query2 <- paste("select * from movimientos_10058(",inicio,",",fin,")")
  
  result2 <-  DB_REJUCAV() %>% 
    dbGetQuery(query2) %>% 
    mutate(movimientos_tipo = "violencia_c-mujer")
  
  #if(str_detect(poblacion$organismo, "fam")) {
  resul <- result1 %>% 
    bind_rows(result2) %>% 
    select(jurisdiccion, juzgado, movimientos_tipo, everything()) %>% 
    rename(cantidad_movimientos = cantidad) %>% 
    mutate(juzgado = str_to_title(juzgado), jurisdiccion = str_to_title(jurisdiccion)) %>% 
    arrange(jurisdiccion, juzgado, desc(cantidad_movimientos)) 
    
    
  resul
  
}

#############################################################################
# mapa violencias

violencias <-  movimientos_violencias(poblacion_total, "2018-01-01", "2019-01-01")

violencias_circunscripcion <- violencias %>% 
  group_by(jurisdiccion, movimientos_tipo) %>% 
  summarise(cantidad_movimientos = sum(cantidad_movimientos, na.rm = T)) %>% 
  filter(!is.na(jurisdiccion)) %>% 
  mutate(refgeo = str_c("Argentina, Entre Ríos", jurisdiccion, sep= ", " )) 


jgcode <- geocode(violencias_circunscripcion$refgeo)

violencias_circunscripcion <- violencias_circunscripcion %>% 
  bind_cols(jgcode)

