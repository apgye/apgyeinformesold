
audienciasCiviles <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02", desagregacion_mensual = TRUE) {
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIC") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(fea = dmy(fea)) %>% 
    filter(fea >= data_interval_start, fea < data_interval_end) %>% 
    filter(esta == "2") %>% # no canceladas
    collect() %>% ungroup() %>% 
    mutate(ra = toupper(ra)) %>%
    distinct(iep, nro, caratula, tproc, fea, .keep_all = T) %>% # Filtro Casos Duplicados
    mutate(ta = case_when(ta == 1 ~ "preliminar", 
                          ta == 2 & fea < lubridate::make_date(2018,09,01) ~ "prueba",
                          ta == 2 & fea >= lubridate::make_date(2018,09,01) ~ "vista_causa(inicio 01-09-18)",
                          ta == 3 ~ "conciliación", 
                          ta == 4 ~ "otras",
                          TRUE ~ "sin_dato")) %>% 
    filter(!str_detect(ta, "prueba")) %>% 
    mutate(ra = case_when(
      ra == "T" ~ "Conciliacion_Total",
      ra == "P" ~ "Conciliacion_Parcial",
      ra == "S" ~ "Sin_Conciliacion", 
      TRUE ~ "sin_dato")) %>% 
    mutate(tipo = ta) 
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo")
  } else {
    resultado <- resultado %>% 
      mutate(organismo = iep) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo")
  } 
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo_descripcion, mes) %>% 
      group_by(circunscripcion, organismo_descripcion, mes, tipo) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(tipo, cantidad, drop = F, fill = NA) %>% 
      select(circunscripcion, organismo_descripcion, mes, preliminar, 'vista_causa(inicio 01-09-18)',
             conciliación, everything()) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col")
    } else {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        group_by(circunscripcion, organismo_descripcion, tipo) %>% 
        summarise(cantidad = n()) %>%
        tidyr::spread(tipo, cantidad, drop = F, fill = NA) %>% 
        select(circunscripcion, organismo_descripcion,preliminar, 'vista_causa(inicio 01-09-18)',
               conciliación, everything()) %>% 
        janitor::adorn_totals("col")
    }
  
  resultado
}


# reeelaboacion para informes específicos

audicco_prim <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02"){
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIC") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% poblacion$organismo) %>%
    filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
    mutate(fea = dmy(fea), fprog = dmy(fprog)) %>% 
    filter(fea > data_interval_start, fea < data_interval_end) %>%
    select(-data_interval_start, -data_interval_end, -justiables) %>%
    collect() %>% 
    distinct(iep, nro, caratula, tproc, fea, .keep_all = T) # Filtro Casos Duplicados
  
  resultado <- resolverconvertidos(resultado)
  
  resultado <- resultado %>%
    mutate(audvid = as.integer(audvid)) %>%
    mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
    mutate(duracm = str_trim(duracm)) %>%
    mutate(ra = str_sub(toupper(ra), 1,1)) %>%
    mutate(ta = str_trim(ta)) %>% 
    mutate(esta = str_remove_all(esta, "[:alpha:]|[:punct:]")) %>%
    mutate(esta = str_trim(esta)) %>% 
    codconver("AUDIC", "esta") %>% 
    codconver("AUDIC", "ra") %>% 
    codconver("AUDIC", "ta") 
  
  resultado
}

audicco_xestado <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), -organismo) 
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audicco_realizadas_xtipo_cantidad_minutos <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    mutate(duracm = as.integer(duracm))
  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
      
    minutos_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
    
  } else {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    cantidad_audiencias
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias
    
  }
  
  minutos_audiencias <<- minutos_audiencias
  cantidad_audiencias <<- cantidad_audiencias
  
}

audicco_realizadas_xtipoyresultado <- function(df, desagregacion_mensual = TRUE) {
  
  df <- df %>% 
    filter(esta == "realizada") 
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>%
      group_by(organismo, mes, ta, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), -organismo) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col") 
    
    resultado
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion,  everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audicco_realizadas_video_xtipo <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    filter(audvid == 1) %>% 
    mutate(duracm = as.integer(duracm))
  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F))) %>%  
      group_by(organismo, mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep, mes = as.character(lubridate::month(fea, label=T, abbr = F)))  %>% 
      group_by(organismo, mes, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) 
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
    
  } else {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = NA) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    
    
  }
  
  minutos_audiencias <<- minutos_audiencias
  cantidad_audiencias <<- cantidad_audiencias
  
}

audiccco_vc_program <- function(df) {
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    filter(ta == "preliminar") %>% 
    filter(ra != "conciliacion_total") %>% 
    filter(!is.na(fprog) & fprog > end_date) %>% 
    rename(fprogramada_vista_causa = fprog, organismo = iep, nro_expte = nro) %>% 
    mutate(mes = as.character(lubridate::month(fprogramada_vista_causa)), 
           año = as.character(lubridate::year(fprogramada_vista_causa))) %>%
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                              "circunscripcion")],
              by = "organismo") %>%
    group_by(circunscripcion, organismo_descripcion, año, mes) %>% 
    mutate(fprogramada_vista_causa = str_replace_all(fprogramada_vista_causa, "-", "\\/")) %>% 
    summarise(cantidad_audiencias = n(), 
              nros_expte_y_fechaAudiencia = paste(nro_expte, fprogramada_vista_causa,
                                                  sep = ": ", collapse = "; ")) %>% 
    ungroup() %>% 
    arrange(circunscripcion, organismo_descripcion)  %>% 
    janitor::adorn_totals("row") 
  
  df
  
}