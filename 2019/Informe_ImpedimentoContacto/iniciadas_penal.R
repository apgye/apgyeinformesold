
iniciadas_pen <- function(poblacion, start_date="2019-02-01", end_date = "2019-03-01") {
  
  if(any(str_detect(poblacion, "oga"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIGTIA') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio), fhecho = dmy(fhecho)) %>%
      filter(iep %in% poblacion$organismo) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      mutate(organismo = "Garantías")
      
    
    inic_prim <<- inic
    
    inic <- inic %>% 
      filter(
        !str_detect(tproc, "^ADMINISTRATIVO|^ARCHIVO|^AUTORIZACION|CONCURSO CERRADO"),
        !str_detect(tproc, "^CONCURSOS|^DESIGNACION|^EFECTOS SECUESTRADOS|^EXHORTO|^INFORMES"),
        !str_detect(tproc, "^LEGAJO|^OFICIO|^PERSONAL|^SENTENCIA|^SU PRESENTACION|^SU SITUACION"),
        !str_detect(tproc, "^EJECUCION DE SENTENCIA|^EJECUTIVO|FALLECIMIENTO|^INCIDENTE|INSTRUCCION"),
        !str_detect(tproc, "^LIBRAMIENTO DE CHEQUE|^MEDIDA CAUTELAR|^OTRO|^SOLICITUD"),
        !str_detect(tproc, "^VARIOS|^TESTIMONIOS|Y OTROS"),
        !str_detect(tproc, "^JURISPRUDENCIA|LEY")) %>%
      group_by(circunscripcion, organismo, tproc) %>%
      summarise(cantidad = n(), durac_hec_inic= median(finicio-fhecho, na.rm = T)) %>%
      mutate(durac_hec_inic = ifelse(durac_hec_inic > 0 , as.character(durac_hec_inic), NA) ) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(desc(cantidad)) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc) 
    
    inic
    
  } 
  else if(any(str_detect(poblacion, "tja"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIJUI') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(fingj = dmy(fingj), ffdebab = dmy(ffdebab), frdebab = dmy(frdebab)) %>%
      collect() %>% 
      left_join(poblacion_penal %>% 
                  select(iep=organismo, circunscripcion), by="iep") %>% 
      filter(circunscripcion %in% tja$circunscripcion) %>% 
      left_join(tja %>% select(organismo_descripcion, circunscripcion), by="circunscripcion") %>% 
      rename(organismo = organismo_descripcion)
    
    inic_prim <<- inic
    
    inic <- inic %>% 
      group_by(circunscripcion, organismo, tproc) %>%
      summarise(cantidad = n()) %>%  
                # durac_ingreso_fij_dbte = median(ffdebab-fingj, na.rm = T)) %>% 
               #durac_fij_realizacion_dbte = median(frdebab-ffdebab, na.rm = T)) %>%
      #mutate(durac_ingreso_fij_dbte = ifelse(durac_ingreso_fij_dbte > 0 , as.character(durac_ingreso_fij_dbte), NA)) %>%
      #mutate(durac_fij_realizacion_dbte = ifelse(durac_fij_realizacion_dbte > 0 , as.character(durac_fij_realizacion_dbte), NA)) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(desc(cantidad)) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc) %>% 
      ungroup() %>% 
      mutate(organismo = str_sub(organismo, 1, 32))
    
  
    inic_ap <- DB_PROD() %>% 
      apgyeTableData('CIAP') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(fiap  = dmy(fiap)) %>%
      collect() %>% 
      left_join(poblacion_penal %>% 
                  select(iep=organismo, circunscripcion), by="iep") %>% 
      filter(circunscripcion %in% tja$circunscripcion) %>% 
      left_join(tja %>% select(organismo_descripcion, circunscripcion), by="circunscripcion") %>% 
      rename(organismo = organismo_descripcion) %>% 
      group_by(circunscripcion, organismo, tproc) %>%
      summarise(cantidad = n()) %>%  
      # durac_ingreso_fij_dbte = median(ffdebab-fingj, na.rm = T)) %>% 
      #durac_fij_realizacion_dbte = median(frdebab-ffdebab, na.rm = T)) %>%
      #mutate(durac_ingreso_fij_dbte = ifelse(durac_ingreso_fij_dbte > 0 , as.character(durac_ingreso_fij_dbte), NA)) %>%
      #mutate(durac_fij_realizacion_dbte = ifelse(durac_fij_realizacion_dbte > 0 , as.character(durac_fij_realizacion_dbte), NA)) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(desc(cantidad)) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc) 
    
    inic_ap <<- inic_ap
    
    inic
    
    
  } 
  else if(any(str_detect(poblacion, "campen"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData("CINC3P") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      left_join(poblacion %>% 
                    select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
    
      inic_prim <<- inic
      
      inic <- inic %>% 
        mutate(tdelito = stringr::str_trunc(tdelito, 60)) %>% 
        group_by(circunscripcion, organismo, tdelito) %>%
        summarise(cantidad = n()) %>%
        arrange(desc(cantidad)) %>% 
        do(janitor::adorn_totals(.))  %>% rename(tipo_delito = tdelito) 
      
     inic
  } 
  else if(any(str_detect(poblacion, "sal"))) {
    
    inicp <- DB_PROD() %>% 
      apgyeTableData("CIN3P") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      mutate(materia = "penal")
    
    inicc <- DB_PROD() %>% 
      apgyeTableData("CIN3PC") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      left_join(poblacion %>% 
                    select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      mutate(materia = "constitucional")
   
     inic <- bind_rows(inicc, if(exists("inicp")) inicp)
       
     inic_prim <<- inic
       
     inic <- inic %>% 
         group_by(circunscripcion, organismo, materia, tproc) %>%
         summarise(cantidad = n()) %>%
         arrange(desc(cantidad)) %>% 
         do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc) 
       
     inic
    }
  else if(any(str_detect(poblacion, "pep"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData("IGEP") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
      filter(finicio >= data_interval_start, finicio < data_interval_end ) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
    
    inic_prim <<- inic
    
    inic <- inic %>% 
      mutate(ottproc = str_replace_all(ottproc, "EJECUCION DE PENA ", "")) %>% 
      mutate(tpena = toupper(tpena)) %>% 
      mutate(tpena =  case_when(
        tpena == "PL" ~ "Privativa de libertad",
        tpena == "PLI" ~ "Privativa de libertad con internación",
        tpena == "PD" ~ "Prisión domiciliaria", 
        tpena == "SD" ~ "Semidetención tanto diurna como nocturna", 
        tpena == "LC" ~ "Libertad Condicional", 
        tpena == "LA" ~ "Libertad Asistida", 
        tpena == "SP" ~ "Sustitución de Penas", 
        tpena == "MSA" ~ "Medidas de Seguridad Ambulatoria", 
        tpena == "MS" ~ "Medida Seguridad alojado en UP", 
        tpena == "MSI" ~ "Medida Seguridad con internación", 
        tpena == "O" ~ "Situaciones no contempladas", 
        TRUE ~ "sin_dato")) %>% 
      select(circunscripcion, organismo, everything()) 
    
    inic_tpena <<- inic %>% 
      group_by(circunscripcion, organismo, tpena) %>% 
      summarise(cantidad_iniciados = n()) %>% 
      arrange(circunscripcion, desc(cantidad_iniciados)) %>% 
      rename(tipo_pena = tpena) %>% 
      do(janitor::adorn_totals(.))
    
    
    inic <- inic %>% 
      mutate(ottproc = str_sub(ottproc, 1, 60)) %>% 
      group_by(circunscripcion, organismo, ottproc) %>% 
      summarise(cantidad_iniciados = n()) %>% 
      arrange(circunscripcion, desc(cantidad_iniciados)) %>% 
      rename(tipo_proceso = ottproc) %>% 
      do(janitor::adorn_totals(.))
    
  }
  else if(any(str_detect(poblacion, "pna"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIPM') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(iep %in% poblacion$organismo) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep")
    
    inic_prim <<- inic
    
    inic <- inic %>% 
      filter(
        !str_detect(tproc, "^ADMINISTRATIVO|^ARCHIVO|^AUTORIZACION|CONCURSO CERRADO"),
        !str_detect(tproc, "^CONCURSOS|^DESIGNACION|^EFECTOS SECUESTRADOS|^EXHORTO|^INFORMES"),
        !str_detect(tproc, "^LEGAJO|^OFICIO|^PERSONAL|^SENTENCIA|^SU PRESENTACION|^SU SITUACION"),
        !str_detect(tproc, "^EJECUCION DE SENTENCIA|^EJECUTIVO|FALLECIMIENTO|^INCIDENTE|INSTRUCCION"),
        !str_detect(tproc, "^LIBRAMIENTO DE CHEQUE|^MEDIDA CAUTELAR|^OTRO|^SOLICITUD"),
        !str_detect(tproc, "^VARIOS|^TESTIMONIOS|Y OTROS"),
        !str_detect(tproc, "^JURISPRUDENCIA|LEY")) %>%
      group_by(circunscripcion, organismo, tproc) %>%
      summarise(cantidad = n()) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(desc(cantidad)) %>% 
      do(janitor::adorn_totals(.)) %>% rename(tipo_proceso = tproc) 
    
    inic
    
    
  }
  
  
  if(exists("inic_ap")){inic_ap <<- inic_ap}
  
  if(exists("inic_tpena")){inic_tpena <<- inic_tpena}
  
  inic_prim <<- inic_prim
  
  inic
}

iniciados_oma <- function(poblacion, start_date="2019-03-01", end_date = "2019-04-01") {
  
  inic <- DB_PROD() %>% 
    apgyeTableData("IGMP") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
    filter(finicio >= data_interval_start, finicio < data_interval_end ) %>% 
    filter(finicio >= lubridate::make_date(2019,03,01)) %>% 
    collect() %>% 
    left_join(poblacion %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
  
    #inic_prim <<- inic
    
    inic <- inic %>% 
      mutate(resp = toupper(resp)) %>% 
      select(circunscripcion, resp, everything()) %>% 
      group_by(circunscripcion, resp) %>% 
      summarise(cantidad_iniciados = n()) %>% 
      arrange(circunscripcion, desc(cantidad_iniciados)) %>% 
      rename(responsable = resp) %>% 
      mutate(responsable = ifelse(is.na(responsable), "sin dato", responsable)) %>% 
      do(janitor::adorn_totals(.))
    
    inic

}

