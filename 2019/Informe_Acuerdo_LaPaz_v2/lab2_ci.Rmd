---
title: Causas Ingresadas, que Bajan a Juzgado y que Suben al Superior Tribunal
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
resultado <- iniciados_cam(poblacion = cam_lab, 
              start_date = start_date,
              end_date = end_date, estadistico = "conteo") 
```


## Causas Ingresadas, que Bajan a Juzgado y que Suben al Superior Tribunal - `r getDataIntervalStr(start_date, end_date)`

```{r, lab2_ci, eval= (ci & t) }

resultado$inic %>% 
  iniciados_cam_comparativo(cfecha = desagregacion_mensual) %>% 
  kable( caption = str_c("Causas Ingresadas, que Bajan a Juzgado y que Suben al Superior Tribunal "," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  landscape()

```

