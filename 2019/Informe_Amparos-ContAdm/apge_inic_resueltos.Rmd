---
title: Causas Resueltas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r}
resultado <- apge_tareas()
```

## Introducción

En esta sección mostramos los indicadores del Área de Planificación Gestión y Estadística con el fin de ofrecer a nuestros usuarios y órganos informantes herramientas para la evaluación de nuestro desempeño.      

También damos acceso a los registros primarios de actividad para la supervición de nuestro funcionamiento por parte de los órganos de control y de nuestros usuarios. Este aspecto resulta muy importante atento a la obligación de imparcialidad y objetividad que tiene el área y que implica *elaborar y difundir estadísticas respetando la independencia científica y hacerlo de forma objetiva, profesional y transparente, dando el mismo tratamiento a todas las fuentes de información, y tratando de satisfacer a los usuarios destinatarios de las estadísticas por igual.* -cfrme “CÓDIGO DE BUENAS PRÁCTICAS DE LAS ESTADÍSTICAS JUDICIALES" JUFEJUS.      

En tal sentido para acceder al registro on line de nuestras actividades, orden de ingreso y atención debe ingresar a:     

<https://tree.taiga.io/project/zojeda-justat/timeline>

El protocolo que se sigue para la registración de requerimientos puede consultarse en:

<https://bit.ly/2M97r8E>

Ante cualquier consulta no dude en comunicarse con nosotros al teléfono: 0343-4209405/410 – ints. 396 y 305.   

\pagebreak

## Casos Resueltos y Plazo de Resolución - Año `r year(Sys.Date())`

En esta tabla se cuentan los requerimientos (ie. informes, actualizaciones, revisiones, entre otros) resueltos por el área.    

```{r}

resultado$apge_resueltos %>% 
  kable(caption = "Requerimientos Resueltos", align = 'c', longtable = TRUE ) %>%
  add_header_above(c(" " = 2, "Plazo de Resolución" = 3)) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  
```

## Casos Pendientes Año `r year(Sys.Date())`

En esta tabla se cuentan los requerimientos pendientes de resolución y su estado de tratamiento.     

```{r}

resultado$apge_iniciados %>% 
  kable(caption = "Resumen de Requerimientos Pendientes", align = 'c', longtable = TRUE ) %>%
   add_header_above(c(" " = 2, "Estado de tratamiento" = 3)) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  
```


```{r}
resultado$apge_iniciados_cref %>% 
  # kable(caption = "Requerimientos Pendientes y numero de Referencia para consulta", 
  #       align = 'c', longtable = TRUE ) %>%
  # kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
  #               full_width = F, font_size = 10)
  outputTable(caption = "Requerimientos Pendientes y numero de Referencia para consulta")
```
