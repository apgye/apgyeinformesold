---
title: Audiencias - Estados y Resultados
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}
audic <- audicco_prim(jdos_cco, 
             start_date, 
             end_date) 

audicco_realizadas_xtipo_cantidad_minutos(audic, desagregacion_mensual = desagregacion_mensual) 

multifueros_lab <- any(str_detect(jdos_cco$materia, "lab"))


```

## Audiencias Civiles según sus distintos estados finales - `r getDataIntervalStr(start_date, end_date)`

La categoría *audiencia realizada no concluida* (ie.audiencia celebrada en la que se dispone un cuarto intermedio -cfrme.Sala Civ.yCom. STJER-) entró en vigencia a partir del 01/06/19. Las cantidades que figuran en la tabla correspondientes a meses anteriores a junio corresponden a *audiencias reprogramadas* (categoría derogada).   

```{r}
audicco_xestado(audic, desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  landscape()
```

## Audiencias Civiles realizadas por Tipo - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presenta información de las audiencias realizadas para los distintos tipos de audiencia. Más abajo se detallan los resultados obtenidos por tipo de audiencia.

```{r}
cantidad_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Cantidad de Audiencias") %>%
  landscape()
```

## Minutos en Audiencia Civiles - `r getDataIntervalStr(start_date, end_date)`
 
En esta tabla se presenta información de los minutos en audiencia por magistrado.

```{r}
minutos_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Minutos en Audiencia del Magistrado") %>%
  landscape()
```


## Audiencias Civiles Realizadas y sus Resultados - `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se muestran las audiencias realizadas en cada organismo agrupadas por sus tipos y los resultados obtenidos para cada tipo de audiencia. Los resultados posibles según definición institucional son:

+ Conciliación Total  
+ Conciliación Parcial   
+ Sin conciliación 

```{r}
audicco_realizadas_xtipoyresultado(audic, desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias Realizadas y sus Resultados") %>%
  landscape()
```


## Audiencias Civiles Videofilmadas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan las audiencias realizdas videofilmadas.

```{r}
audicco_realizadas_video_xtipo(audic, desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias Videofilmadas") %>%
  landscape()
```


## Audiencias Civiles Vista de Causa Programadas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan las audiencias de Vista de Causa Programadas para los próximos meses.

```{r}
audiccco_vc_program(audic) %>% 
  outputTable(caption = "Audiencias de Vista de Causa Programadas") %>% 
  column_spec(6, "5cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape()
```

```{r, eval = multifueros_lab}
audil <- audilab_prim(jdos_cco,  start_date, end_date) 
audilab_realizadas_xtipo_cantidad_minutos(audil, desagregacion_mensual = desagregacion_mensual) 
```


`r if (multifueros_lab) '## Audiencias Laborales en Órganos Multifuero -igual período-'`

```{r, eval = multifueros_lab}
audilab_xestado(audil, desagregacion_mensual = T) %>% 
  outputTable(caption = "Audiencias Laborales segun sus Estados") %>%
  row_spec(0, angle = 90) 
```

```{r, eval = multifueros_lab}
cantidad_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Cantidad de Audiencias Laborales") 
```

```{r, eval = multifueros_lab}
minutos_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Minutos en Audiencia Laborales del Magistrado") 
```

```{r, eval = multifueros_lab}
audilab_realizadas_xtipoyresultado(audil, desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Audiencias Laborales Realizadas y sus Resultados") %>% 
  landscape()
```
