---
title: Consideraciones Finales
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Consideraciones Finales 

De la lectura de los indicadores precedentes se encuentra evidencia de la importancia relativa que tienen procesos específicos en la cantidad de movimientos que reliza el Juzgado Civil 9 -según se informa en el oficio de fs.42-:  50 causas (8%) de las que se tramitaron en el 2018 generaron el 42% de la actividad realizada por la Secretaría 1, y 42 causas (10%) el 45% de la actividad realizada por la secretaría 2.   

No se encuentra evidencia en los registros informados por el organismo del crecimiento en la cantidad de quiebras ingresadas que se informan a fs. 42 vta., segúndo párrafo.-

El organismo presenta altos porcentajes de causas con resolución lugo del 1er plazo de vencimiento, aunque casi en su totalidad se encuentran antes del segundo vencimiento.   