start_date = "2018-01-01"
end_date = "2019-01-01"
poblacion <- cam_civil


resultado <- DB_PROD() %>% 
  apgyeTableData("CADR2C") %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% poblacion$organismo) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% 
  mutate(fdesp1 = dmy(fdesp1)) %>% 
  filter(grepl("AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION", tproc)) %>% 
  collect() %>% ungroup()

resultado <- resultado %>% 
  filter(!is.na(fdesp1)) %>% 
  select(nro, caratula, tproc, as, finicio, fdesp1, fvenc1, fres, jact, iep) %>% 
  mutate(organismo = iep, mes = lubridate::month(fdesp1, label=T, abbr = F))  %>% 
  left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                            "circunscripcion")], by = "organismo") %>% 
  arrange(circunscripcion, organismo_descripcion, mes) %>% 
  group_by(circunscripcion, organismo_descripcion, mes) %>% 
  summarise(cantidad = n()) %>%
  group_by(circunscripcion, organismo_descripcion) %>% 
  do(janitor::adorn_totals(.)) %>%
  ungroup()


amparos_2(cam_civil, "CADR2C", "2018-01-01", "2019-01-01", desagregacion_mensual = T) %>% View

amparos_2(cam_lab, "CADR2L", "2018-01-01", "2019-01-01", desagregacion_mensual = T) %>% View


  
  