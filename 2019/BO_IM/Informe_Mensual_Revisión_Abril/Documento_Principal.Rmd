---
title: "Superior Tribunal de Justicia"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      toc: TRUE
      toc_depth: 3
      number_sections: TRUE
      includes:
        in_header: header.tex
        before_body: before_body.tex
geometry: margin=1.2cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
header-includes: \renewcommand{\contentsname}{Contenido del Documento}
params:
  circunscripcion:
    label: "Circunscripción (separar con coma sin espacio o 'Todas')"
    input: text
    value: "Todas"
  ieps: 
    label: "Organismo (sd = sin discriminar)"
    input: text
    value: "sd"
  fuero:
    label: "Fuero o Materia:Civil,Familia,Paz,Laboral,CAdm,ECQ,Penal,PEjec,PMediación"
    input: text
    value: "Civil"
  instancia1: TRUE
  instancia2: FALSE
  instancia3: FALSE
  start_date: 
    label: "Fecha_inicio_inf"
    input: text
    value: "2019-02-01"
  end_date:
    label: "Fecha_fin_inf"
    input: text
    value: "2019-03-01" 
  desagregacion_mensual: TRUE
  CausasIniciadas: FALSE 
  CausasResueltas: FALSE
  CausasEnTramite: FALSE
  CausasEnTramite_detalle: FALSE
  CausasyMovimientos: FALSE
  SentenciasTipo: FALSE
  ConfirmaRevocaAnula: FALSE
  Audiencias: FALSE
  Audiencias_civil_detalle: FALSE
  Audiencias_familia_detalle: FALSE
  Audiencias_laboral_detalle: FALSE
  Audiencias_penales: FALSE
  Duracion_Procesos_xorg: FALSE
  Duracion_Procesos_interanual: FALSE
  CausasArchivadas: FALSE 
  Consideraciones_Finales: FALSE
  Pendientes: FALSE
  Personal: FALSE
  Primarias: FALSE
  Boletin_intro: FALSE
  Metodología: TRUE
  Presentaciones: FALSE
  
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
knitr::opts_chunk$set(fig.width=12, fig.height=10) 
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
source("codconver.R")
```


```{r parametros, echo=FALSE, include=FALSE}
# parámetros del informe

if (params$circunscripcion == "Todas"){
  circ <- NA
} else {
  circ <- unlist(str_split(params$circunscripcion, ","))
}

if (params$ieps == "sd"){
  ieps <- NA
} else {
  ieps <- unlist(str_split(params$ieps, ","))
}

fuero <- unlist(str_split(params$fuero, ",")) 
instancia1 <- params$instancia1
instancia2 <- params$instancia2
instancia3 <- params$instancia3
start_date <- params$start_date
end_date <- params$end_date
desagregacion_mensual <- params$desagregacion_mensual
ci <- params$CausasIniciadas
ca <- params$CausasArchivadas
cr <- params$CausasResueltas
ce <- params$CausasEnTramite
ced <- params$CausasEnTramite_detalle
cm <- params$CausasyMovimientos
s <- params$SentenciasTipo
a <- params$Audiencias
afd <- params$Audiencias_familia_detalle
ald <- params$Audiencias_laboral_detalle
acd <- params$Audiencias_civil_detalle
ap <- params$Audiencias_penales
cra <- params$ConfirmaRevocaAnula
d <- params$Duracion_Procesos_xorg
di <- params$Duracion_Procesos_interanual
cf <- params$Consideraciones_Finales
p <- params$Personal
pend <- params$Pendientes
prim <- params$Primarias
metodologia <- params$Metodología
presentaciones <- params$Presentaciones
bo <- params$Boletin_intro
```


```{r poblacion, echo=FALSE, include=FALSE}
# Poblacion

# Entidades completo
poblacion_er <- apgyeJusEROrganization::listar_organismos() 

# Fuero Civil-Comercial y Laboral
poblacion_total <- poblacion_er %>% 
  filter(tipo %in% c("jdo", "cam", "sal")) 

if (exists("circ")) {
  if(is.na(circ)) {
    poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>% 
    filter(circunscripcion %in% circ)
  }
}

if (exists("ieps")) {
    if(is.na(ieps)) {
    poblacion_total <- poblacion_total
    } else {
    poblacion_total <- poblacion_total %>%
    filter(organismo %in% ieps)
    poblacion_total
    }
}

# Sub-poblaciones
jdos_cco <- poblacion_total %>% 
  filter(grepl("jdocco|jdopen0000fel", organismo)) %>% 
  filter(str_detect(materia, "cco"), categoria == "1" | is.na(categoria)) 

if (any(str_detect(jdos_cco$materia, "lab|fam"))) { 
  multifueros <- TRUE 
  } else {
  multifueros <- FALSE
  }

jdos_ecq <- poblacion_total %>% 
  filter(grepl("jdocco", organismo)) %>% 
  filter(str_detect(materia, "eje|cqb"), categoria == "1" | is.na(categoria))

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria)) 

jdos_paz <- poblacion_total %>% 
  filter(str_detect(materia, "paz"), 
         categoria == "1" | (categoria == "3" & organismo == "turvol0100pna") 
         |  organismo == "jdopaz0000ram" )

jdos_paz_23 <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria != "1" | is.na(categoria))

if (nrow(jdos_paz_23) != 0) { 
  paz_23 <- TRUE 
  } else {
  paz_23 <- FALSE
  }

jdos_lab <- poblacion_total %>% 
  filter(str_detect(organismo, "jdolab"), tipo != "cam")

cam_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "cam")

cam_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "cam")

sal_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "sal")

sal_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "sal")

# Fuero Penal

poblacion_penal <- poblacion_er %>% 
  filter(str_detect(fuero, "Penal")) 

if (exists("circ")) {
  if(is.na(circ)) {
    poblacion_penal <- poblacion_penal
  } else {
    poblacion_penal <- poblacion_penal %>% 
    filter(circunscripcion %in% circ)
  }
}

if (exists("ieps")) {
    if(is.na(ieps)) {
    poblacion_penal <- poblacion_penal
    } else {
    poblacion_penal <- poblacion_penal %>%
    filter(organismo %in% ieps)
    poblacion_penal
    }
}

oga <- poblacion_penal %>% 
  filter(str_detect(organismo, "oga|jez"))

oma <- poblacion_penal %>% 
  filter(str_detect(organismo, "oma|equ"))

tja <- poblacion_penal %>% 
  filter(str_detect(organismo, "tja"))

jdo_pna <- poblacion_penal %>% 
  filter(str_detect(organismo, "jdopna"))

jdo_pep <- poblacion_penal %>% 
  filter(str_detect(organismo, "jdopep"))

cam_pen <- poblacion_penal %>% 
  filter(str_detect(organismo, "campen"), str_detect(materia, "pen"))

sal_pen <- poblacion_penal %>% 
  filter(str_detect(organismo, "sal"))

# Fuero Cont_Adm
cam_cad <- poblacion_er %>% 
  filter(str_detect(materia, "cad"), tipo == "cam") 

stj_cad <- poblacion_er %>% 
  filter(str_detect(materia, "cad"), tipo == "stj") 

```

\pagebreak

`r if (bo) '# Informe Mensual para Revisión'`

```{r bo, child = 'boletin_intro.Rmd', eval = bo}
```

\pagebreak

`r if (instancia1 & "Civil" %in% fuero) '# Juzgados Civil y Comercial'`


```{r cco_cadur_interanual, child = 'cco_cadur_interanual.Rmd', eval= instancia1 & "Civil" %in% fuero & di}
```

\pagebreak

```{r cco1_cadur, child = 'cco1_cadur.Rmd', eval= instancia1 & "Civil" %in% fuero & d}
```

\pagebreak

```{r cco1_cetal, child = 'cco1_cetal.Rmd', eval= instancia1 & "Civil" %in% fuero & ce}
```

\pagebreak

```{r cco1_cetal_detalle, child = 'cco1_cetal_detalle.Rmd', eval= instancia1 & "Civil" %in% fuero & ced}
```

\pagebreak

```{r cco1_cixgpo, child = 'cco1_cixgpo.Rmd', eval= instancia1 & "Civil" %in% fuero & ci}
```

\pagebreak

```{r cco1_cixgpo_graf, child = 'cco1_cixgpo_graf.Rmd', eval= instancia1 & "Civil" %in% fuero & ci}
```

\pagebreak

```{r cco1_cixgpo_multifueros, child = 'cco1_cixgpo_multifueros.Rmd', eval = instancia1 & multifueros & "Civil" %in% fuero & ci}
```

\pagebreak

```{r cco1_cadr, child = 'cco1_cadr.Rmd', eval= instancia1 & "Civil" %in% fuero  & cr}
```

\pagebreak

```{r cco1_sentxtres, child = 'cco1_sentxtres.Rmd', eval= instancia1 & "Civil" %in% fuero & s}
```

 \pagebreak

```{r cco1_audic, child = 'cco1_audic.Rmd', eval= instancia1 & "Civil" %in% fuero & a}
```

\pagebreak

```{r cco1_audic_detalle, child = 'cco1_audic_detalle.Rmd', eval= instancia1 & "Civil" %in% fuero & acd}
```

\pagebreak

```{r cco1_camov, child = 'cco1_camov.Rmd', eval= instancia1 & "Civil" %in% fuero & cm}
```

\pagebreak
 
```{r cco_carch, child = 'cco_carch.Rmd', eval= instancia1 & "Civil" %in% fuero & ca}
```

\pagebreak

```{r cco1_cadr_prim, child = 'cco1_cadr_prim.Rmd', eval= instancia1 & "Civil" %in% fuero  & prim}
```

\pagebreak

```{r cco1_pendientes, child = 'cco1_pendientes.Rmd', eval= instancia1 & "Civil" %in% fuero  & pend}
```

\pagebreak


`r if (instancia1 & "ECQ" %in% fuero) '# Juzgados Ejecuciones y Concursos-Quiebras'`

\pagebreak

```{r ecq1_cetal, child = 'ecq1_cetal.Rmd', eval= instancia1 & "ECQ" %in% fuero & ce}
```

\pagebreak

```{r ecq1_cixgpo, child = 'ecq1_cixgpo.Rmd', eval= instancia1 & "ECQ" %in% fuero & ci}
```

\pagebreak

```{r ecq1_cixgpo_graf, child = 'ecq1_cixgpo_graf.Rmd', eval= instancia1 & "ECQ" %in% fuero & ci}
```

\pagebreak

```{r ecq1_cadr, child = 'ecq1_cadr.Rmd', eval= instancia1 & "ECQ" %in% fuero  & cr}
```

\pagebreak

```{r ecq1_sentxtres, child = 'ecq1_sentxtres.Rmd', eval= instancia1 & "ECQ" %in% fuero & s}
```

\pagebreak

```{r ecq1_audic, child = 'ecq1_audic.Rmd', eval= instancia1 & "ECQ" %in% fuero & a}
```

\pagebreak

```{r ecq1_camov, child = 'ecq1_camov.Rmd', eval= instancia1 & "ECQ" %in% fuero & cm}
```

\pagebreak
 
```{r ecq_carch, child = 'ecq_carch.Rmd', eval= instancia1 & "ECQ" %in% fuero & ca}
```

\pagebreak

```{r ecq_cadr_prim, child = 'ecq_cadr_prim.Rmd', eval= instancia1 & "ECQ" %in% fuero & prim}
```

\pagebreak

```{r ecq1_pendientes, child = 'ecq1_pendientes.Rmd', eval= instancia1 & "ECQ" %in% fuero  & pend}
```

\pagebreak
 
`r if (instancia1 & "Familia" %in% fuero) '# Juzgados de Familia'`
 
 
```{r fam_cadur_interanual, child = 'fam_cadur_interanual.Rmd', eval= instancia1 & "Familia" %in% fuero & di}
```

\pagebreak

```{r fam_cadur, child = 'fam_cadur.Rmd', eval= instancia1 & "Familia" %in% fuero & d}
```


\pagebreak

```{r fam1_cetal, child = 'fam1_cetal.Rmd', eval= instancia1 & "Familia" %in% fuero & ce }
```

\pagebreak

```{r fam1_cixgpo, child = 'fam1_cixgpo.Rmd', eval= instancia1 & "Familia" %in% fuero & ci }
```

\pagebreak

```{r fam1_cixgpo_graf, child = 'fam1_cixgpo_graf.Rmd', eval= instancia1 & "Familia" %in% fuero & ci }
```

\pagebreak

```{r fam1_cadr, child = 'fam1_cadr.Rmd', eval= instancia1 & "Familia" %in% fuero  & cr}
```

<!-- \pagebreak -->

<!-- ```{r fam1_sentxtres, child = 'fam1_sentxtres.Rmd', eval= instancia1 & "Familia" %in% fuero & s } -->
<!-- ``` -->

\pagebreak

```{r fam1_audif, child = 'fam1_audif.Rmd', eval= instancia1 & "Familia" %in% fuero & a}
```

\pagebreak

```{r fam1_audif_detalle, child = 'fam1_audif_detalle.Rmd', eval= instancia1 & "Familia" %in% fuero & afd}
```

\pagebreak

```{r fam1_camov, child = 'fam1_camov.Rmd', eval= instancia1 & "Familia" %in% fuero & cm}
```

\pagebreak

 
```{r fam_carch, child = 'fam_carch.Rmd', eval= instancia1 & "Familia" %in% fuero & ca}
```

\pagebreak

```{r fam1_cadr_prim, child = 'fam1_cadr_prim.Rmd', eval= instancia1 & "Familia" %in% fuero  & prim}
```

\pagebreak

```{r fam1_pendientes, child = 'fam1_pendientes.Rmd', eval= instancia1 & "Familia" %in% fuero  & pend}
```

\pagebreak

`r if (instancia1 & "Paz" %in% fuero) '# Juzgados de Paz 1a. Categoría'`


```{r paz1_cetal, child = 'paz1_cetal.Rmd', eval= instancia1 & "Paz" %in% fuero & ce}
```

\pagebreak

```{r paz1_cixgpo, child = 'paz1_cixgpo.Rmd', eval= instancia1 & "Paz" %in% fuero & ci}
```

\pagebreak

```{r paz1_cadr, child = 'paz1_cadr.Rmd', eval= instancia1 & "Paz" %in% fuero  & cr}
```

\pagebreak

```{r paz1_camov, child = 'paz1_camov.Rmd', eval= instancia1 & "Paz" %in% fuero & cm}
```

\pagebreak

```{r paz_carch, child = 'paz_carch.Rmd', eval= instancia1 & "Paz" %in% fuero & ca}
```

\pagebreak

```{r paz1_cadr_prim, child = 'paz1_cadr_prim.Rmd', eval= instancia1 & "Paz" %in% fuero  & prim}
```

\pagebreak

```{r paz1_pendientes, child = 'paz1_pendientes.Rmd', eval= instancia1 & "Paz" %in% fuero  & pend}
```

\pagebreak

`r if (instancia1 & "Paz" %in% fuero & paz_23 & (ci | cr)) '# Juzgados Paz 2a. y 3a. Categoría'` 


```{r paz23_ci, child = 'paz23_ci.Rmd', eval= instancia1 & "Paz" %in% fuero & ci & paz_23}
```

\pagebreak

```{r paz23_cadr, child = 'paz23_cadr.Rmd', eval= instancia1 & "Paz" %in% fuero & cr & paz_23 }
```

\pagebreak

```{r paz23_cadr_prim, child = 'paz23_cadr_prim.Rmd', eval= instancia1 & "Paz" %in% fuero  & prim}
```


\pagebreak

```{r paz123_ci_graf, child = 'paz123_ci_graf.Rmd', eval= instancia1 & "Paz" %in% fuero  & ci}
```


\pagebreak

`r if (instancia1 & "Laboral" %in% fuero) '# Juzgados Laborales'`


```{r lab_cadur_interanual, child = 'lab_cadur_interanual.Rmd', eval= instancia1 & "Laboral" %in% fuero & di}
```

\pagebreak

```{r lab1_cetal, child = 'lab1_cetal.Rmd', eval= instancia1 & "Laboral" %in% fuero & ce}
```

\pagebreak

```{r lab1_cixgpo, child = 'lab1_cixgpo.Rmd', eval= instancia1 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab1_cixgpo_graf, child = 'lab1_cixgpo_graf.Rmd', eval= instancia1 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab1_cadr, child = 'lab1_cadr.Rmd', eval= instancia1 & "Laboral" %in% fuero & cr}
```

\pagebreak

```{r lab1_sentxtres, child = 'lab1_sentxtres.Rmd', eval= instancia1 & "Laboral" %in% fuero & s}
```

 \pagebreak

```{r lab1_audil, child = 'lab1_audil.Rmd', eval= instancia1 & "Laboral" %in% fuero & a}
```

 
\pagebreak

```{r lab1_audil_detalle, child = 'lab1_audil_detalle.Rmd', eval= instancia1 & "Laboral" %in% fuero & ald}
```

\pagebreak

```{r lab1_camov, child = 'lab1_camov.Rmd', eval= instancia1 & "Laboral" %in% fuero & cm}
```

\pagebreak

```{r lab_carch, child = 'lab_carch.Rmd', eval= instancia1 & "Laboral" %in% fuero & ca}
```

\pagebreak

```{r lab1_cadr_prim, child = 'lab1_cadr_prim.Rmd', eval= instancia1 & "Laboral" %in% fuero  & prim}
```

\pagebreak


```{r lab1_pendientes, child = 'lab1_pendientes.Rmd', eval= instancia1 & "Laboral" %in% fuero  & pend}
```

\pagebreak


`r if (instancia1 & "Penal" %in% fuero) '# Garantías'`


```{r pen_gtia_ci, child = 'pen_gtia_ci.Rmd', eval= instancia1 & "Penal" %in% fuero & ci}
```

\pagebreak

```{r pen_gtia_res, child = 'pen_gtia_res.Rmd', eval= instancia1 & "Penal" %in% fuero & cr}
```

\pagebreak

```{r pen_gtia_audip, child = 'pen_gtia_audip.Rmd', eval= instancia1 & "Penal" %in% fuero & ap}
```

\pagebreak


`r if (instancia1 & "Penal" %in% fuero) '# Tribunal de Juicio'`


```{r pen_tja_ci, child = 'pen_tja_ci.Rmd', eval= instancia1 & "Penal" %in% fuero & ci}
```

\pagebreak

```{r pen_tja_res, child = 'pen_tja_res.Rmd', eval= instancia1 & "Penal" %in% fuero & cr}
```

\pagebreak

```{r pen_tjui_audip, child = 'pen_tjui_audip.Rmd', eval= instancia1 & "Penal" %in% fuero & ap}
```

\pagebreak


`r if (instancia1 & "Penal" %in% fuero & (is.na(circ) | circ %in% c("Paraná"))) '# Penal de Menores - Niños y Adolescentes -'`


```{r pen_pna_ci, child = 'pen_pna_ci.Rmd', eval= instancia1 & "Penal" %in% fuero & ci & (is.na(circ) | circ %in% c("Paraná"))}
```

\pagebreak

```{r pen_pna_res, child = 'pen_pna_res.Rmd', eval= instancia1 & "Penal" %in% fuero & cr & (is.na(circ) | circ %in% c("Paraná"))}
```

\pagebreak

```{r pen_pna_audip, child = 'pen_pna_audip.Rmd', eval= instancia1 & "Penal" %in% fuero & ap & (is.na(circ) | circ %in% c("Paraná"))}
```

\pagebreak


`r if (instancia1 & "PEjec" %in% fuero & (is.na(circ) | circ %in% c("Paraná", "Gualeguaychú"))) '# Ejecución de Penas y Medidas de Seguridad'`


```{r pen_pep_ci, child = 'pen_pep_ci.Rmd', eval= instancia1 & "PEjec" %in% fuero & (is.na(circ) | circ %in% c("Paraná", "Gualeguaychú")) & ci}
```

\pagebreak

```{r pen_pep_res, child = 'pen_pep_res.Rmd', eval= instancia1 & "PEjec" %in% fuero & (is.na(circ) | circ %in% c("Paraná", "Gualeguaychú")) & cr}
```

\pagebreak

`r if (instancia1 & "PMediación" %in% fuero & (ci | cr)) '# Oficina de Medios Alternativos - Penal'`


```{r pen_oma_ci, child = 'pen_oma_ci.Rmd', eval= instancia1 & "PMediación" %in% fuero & ci}
```

\pagebreak

```{r pen_oma_audip, child = 'pen_oma_audip.Rmd', eval= instancia1 & "PMediación" %in% fuero & ap}
```


\pagebreak
 
`r if (instancia2 & "Civil" %in% fuero) '# Cámaras en lo Civil y Comercial'`
 

```{r cco2_ci, child = 'cco2_ci.Rmd', eval= instancia2 & "Civil" %in% fuero & ci }
```

\pagebreak

```{r cco2_cadr, child = 'cco2_cadr.Rmd', eval= instancia2 & "Civil" %in% fuero  & cr}
```


\pagebreak

```{r cco2_revoca_conf_anula, child = 'cco2_revoca_conf_anula.Rmd', eval= instancia2 & "Civil" %in% fuero  & cra }
```


\pagebreak

```{r cco2_cadr_prim, child = 'cco2_cadr_prim.Rmd', eval= instancia2 & "Civil" %in% fuero  & prim}
```

\pagebreak

```{r cco2_pendientes, child = 'cco2_pendientes.Rmd', eval= instancia2 & "Civil" %in% fuero  & pend}
```

\pagebreak
 
`r if (instancia2 & "Laboral" %in% fuero) '#Cámaras en lo Laboral'`
 

```{r lab2_ci, child = 'lab2_ci.Rmd', eval= instancia2 & "Laboral" %in% fuero & ci}
```

\pagebreak

```{r lab2_cadr, child = 'lab2_cadr.Rmd', eval= instancia2 & "Laboral" %in% fuero & cr }
```

\pagebreak

```{r lab2_revoca_conf_anula, child = 'lab2_revoca_conf_anula.Rmd', eval= instancia2 & "Laboral" %in% fuero  & cra }
```


\pagebreak

```{r lab2_cadr_prim, child = 'lab2_cadr_prim.Rmd', eval= instancia2 & "Laboral" %in% fuero  & prim}
```

\pagebreak

```{r lab2_pendientes, child = 'lab2_pendientes.Rmd', eval= instancia2 & "Laboral" %in% fuero  & pend}
```

\pagebreak


`r if (instancia1 & "CAdm" %in% fuero) '# Cámaras en lo Contencioso-Administrativo'`
 

```{r cad1_ci, child = 'cad1_ci.Rmd', eval= instancia1 & "CAdm" %in% fuero & ci}
```

\pagebreak

```{r cad1_cadr, child = 'cad1_cadr.Rmd', eval= instancia1 & "CAdm" %in% fuero  & cr}
```

\pagebreak

```{r cad1_cadr_prim, child = 'cad1_cadr_prim.Rmd', eval= instancia2 & "CAdm" %in% fuero  & prim}
```


`r if (instancia2 & "Penal" %in% fuero) '# Cámaras de Casación'`
 

```{r pen_cam_ci, child = 'pen_cam_ci.Rmd', eval= instancia2 & "Penal" %in% fuero & ci}
```

\pagebreak

```{r pen_cam_res, child = 'pen_cam_res.Rmd', eval= instancia2 & "Penal" %in% fuero & cr}
```


\pagebreak

```{r pen_cam_audip, child = 'pen_cam_audip.Rmd', eval= instancia2 & "Penal" %in% fuero & ap}
```


\pagebreak
 
`r if (instancia3 & "Civil" %in% fuero) '# Última Instancia Civil y Comercial'`
 
```{r cco3_ci, child = 'cco3_ci.Rmd', eval= instancia3 & "Civil" %in% fuero & ci }
```

\pagebreak

```{r cco3_cadr, child = 'cco3_cadr.Rmd', eval= instancia3 & "Civil" %in% fuero  & cr}
```

\pagebreak

```{r cco3_cadr_prim, child = 'cco3_cadr_prim.Rmd', eval= instancia3 & "Civil" %in% fuero  & prim}
```

\pagebreak

```{r cco3_sentxtres, child = 'cco3_sentxtres.Rmd', eval = instancia3 & "Civil" %in% fuero  & s}
```

\pagebreak

```{r cco3_pendientes, child = 'cco3_pendientes.Rmd', eval= instancia3 & "Civil" %in% fuero  & pend}
```

\pagebreak


`r if (instancia3 & "Laboral" %in% fuero) '# Última Instancia Laboral'`

```{r lab3_ci, child = 'lab3_ci.Rmd', eval= instancia3 & "Laboral" %in% fuero & ci }
```

\pagebreak

```{r lab3_cadr, child = 'lab3_cadr.Rmd', eval= instancia3 & "Laboral" %in% fuero  & cr}
```

\pagebreak

```{r lab3_cadr_prim, child = 'lab3_cadr_prim.Rmd', eval= instancia3 & "Laboral" %in% fuero  & prim}
```

\pagebreak

```{r lab3_sentxtres, child = 'lab3_sentxtres.Rmd', eval = instancia3 & "Civil" %in% fuero  & s}
```

\pagebreak

```{r lab3_pendientes, child = 'lab3_pendientes.Rmd', eval= instancia3 & "Laboral" %in% fuero  & pend}
```

\pagebreak

`r if (instancia3 & "Penal" %in% fuero) '# Última Instancia Constitucional y Penal'`

\pagebreak

```{r pen_sal_ci, child = 'pen_sal_ci.Rmd', eval = instancia3 & "Penal" %in% fuero & ci}
```

\pagebreak

```{r pen_sal_res, child = 'pen_sal_res.Rmd', eval = instancia3 & "Penal" %in% fuero & cr}
```

\pagebreak

`r if (instancia3 & "CAdm" %in% fuero) '# Última Instancia Contencioso Administrativo'`


```{r cad3_ci, child = 'cad3_ci.Rmd', eval= instancia3 & "CAdm" %in% fuero & ci}
```

\pagebreak

```{r cad3_cadr, child = 'cad3_cadr.Rmd', eval= instancia3 & "CAdm" %in% fuero  & cr}
```

\pagebreak

`r if (cf) '# Consideraciones Finales'`

\pagebreak

```{r consideraciones_finales, child = 'consideraciones_finales.Rmd', eval= cf}
```

`r if (p) '# Personal'`

\pagebreak

```{r personal_cco, child = 'personal_cco.Rmd', eval = instancia1 & "Civil" %in% fuero & p}
```

\pagebreak

```{r personal_paz, child = 'personal_paz1.Rmd', eval = instancia1 & "Paz" %in% fuero & p}
```


\pagebreak

```{r metodologia, child = 'metodologia.Rmd', eval = metodologia}
```


***
\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}
Director: Lic. Sebastian Castillo    
Equipo: Lic. Marcos Londero y Srta. Emilce Leones  
0343-4209405/410 – ints. 396 y 305  
http://www.jusentrerios.gov.ar/area-planificacion-y-estadisticas-stj/  
correos: apge@jusentrerios.gov.ar - estadistica@jusentrerios.gov.ar   
Laprida 250, Paraná, Entre Ríos  
