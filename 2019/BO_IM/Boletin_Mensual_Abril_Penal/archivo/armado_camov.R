primaria <- function (organismo, operacion, start_date, end_date) {
  resultado <- DB_PROD() %>% 
    apgyeTableData(!!operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep == organismo) %>% 
    filter(!is.na(finicio), !is.na(fmov)) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
    select(iep, nro, caratula, tproc, finicio, fmov, movt, data_interval_start, data_interval_end) %>% 
    collect()
}




df <- primaria("jdocco0100cha", "CAMOV", "2018-02-01", "2018-03-01")

df_proc <- df %>%
  tidyr::separate_rows(movt, sep="%")  %>%
  tidyr::separate(movt, into = c("fmovinterno", "descripcion"), sep="\\$") %>% 
  filter(!is.na(fmovinterno)) %>%
  mutate(fmovinterno = as.Date(fmovinterno, format = "%d/%m/%Y")) %>% 
  filter(fmovinterno >= data_interval_start, fmovinterno < data_interval_end) %>% 
  mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) 


length(unique(df_proc$nro))


