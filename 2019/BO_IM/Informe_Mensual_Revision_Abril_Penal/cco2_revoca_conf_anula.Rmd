---
title: Confirmaciones, Revocaciones y Nulidades
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
library(scales)
library(ggplot2)

```


```{r}
revocaciones(
  poblacion = cam_civil, 
  operacion = "CADR2C", 
  start_date = start_date, 
  end_date = end_date)
```


## Confirmaciones, Revocaciones y Nulidades - `r getDataIntervalStr(start_date, end_date)`

En estas tablas usted podrá encontrar información sobre las resoluciones adoptadas en Segunda Instancia respecto de la resoluciones dictadas en Primera Instancia.

La identificación de magistrados de primera instancia se realiza en base al código de identificación brindado por el SGP y registrado por cada órgano de Segunda Instancia.    


```{r}
resultado_gral <- lista_resultados$resultado_gral %>% 
  t() %>% as.data.frame() %>% 
  rownames_to_column() %>% 
  rename(Resolucion_Tipo = rowname, cantidad = V1)


resultado_percent <- resultado_gral %>% 
  janitor::adorn_percentages("col") %>% 
  mutate(cantidad = round(cantidad * 100, digits = 1)) %>% 
  rename('%' = cantidad)

resultado_gral %>% 
  bind_cols(resultado_percent %>% select('%')) %>% 
  kable(caption = "Resoluciones por Tipo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)

```


\pagebreak

### Resoluciones por órgano de Segunda Instancia `r getDataIntervalStr(start_date, end_date)`


```{r}
lista_resultados$resultado_xadquem %>% 
  kable(caption = "Cámaras", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) %>% 
  row_spec(0, angle = 90)
 
```

\pagebreak

### Resoluciones por órgano de Segunda y Primera Instancia `r getDataIntervalStr(start_date, end_date)`


```{r}
lista_resultados$resultado_xaquo %>% 
  kable(caption = "Cámaras y Magistrados/as", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) %>%
  row_spec(0, angle = 90) %>%
  landscape()

```


### Identificación de Casos  `r getDataIntervalStr(start_date, end_date)`

```{r}
lista_resultados$resultado_base %>% 
  kable(caption = "Cantidad de Casos y su identificación", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) %>%
  row_spec(0, angle = 90) %>%
  column_spec(5, "15cm") %>% 
  landscape()

```

