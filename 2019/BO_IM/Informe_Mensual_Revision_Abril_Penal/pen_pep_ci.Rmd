---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Causas Iniciadas en Juzgado de Ejecución- `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciadas_pen(poblacion = jdo_pep,
              start_date = start_date,
              end_date = end_date) %>%
  kable(caption = "Causas Iniciadas por Tipo de Delito en Ejecución Penal", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
  
```

## Causas Iniciadas por Modalidad de la Pena - `r getDataIntervalStr(start_date, end_date)`

```{r}

inic_tpena %>% 
  kable(caption = "Causas Iniciadas por Modalidad de la Pena", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
  
```





