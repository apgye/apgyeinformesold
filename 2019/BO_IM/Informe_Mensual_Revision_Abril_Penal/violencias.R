#Script de acceso a Servidor de Producción

# install.packages("RPostgreSQL")
require("RPostgreSQL")

# Guardar el Password para poder posteriormente eliminarlo
#pw 

# Leer el driver de PostgreSQL
drv <- dbDriver("PostgreSQL")

# Crear la conexión con la base de datos
con <- dbConnect(drv, dbname = "rejucav",
                 host = "192.168.254.210", port = 5432,
                # user = "apge", 
                 password = pw)


# Ejemplo de consulta
inicio <- "'01-10-2018'"
fin <- "'31-10-2018'"
query <- paste("select * from cantidad_expedientes(",inicio,",",fin,")")
result <- dbGetQuery(con, query)

inicio <- "'01-10-2018'"
fin <- "'31-12-2018'"
query <- paste("select * from audiencias_concretadas_10058(",inicio,",",fin,")")
result <- dbGetQuery(con, query)



# Ejemplo de consulta
inicio <- "'01-02-2019'"
fin <- "'28-02-2019'"
query <- paste("select * from movimientos_9198(",inicio,",",fin,")")
result1 <- dbGetQuery(con, query)

inicio <- "'01-02-2019'"
fin <- "'28-02-2019'"
query <- paste("select * from movimientos_10058(",inicio,",",fin,")")
result2 <- dbGetQuery(con, query)




# Consultas

#Cantidad de expedientes

select * from cantidad_expedientes(inicio, fin);

#Es una función de Postgresql que recibe como parámetros el período comprendido entre las fechas indicadas en la consulta. Retorna la cantidad de expedientes por jurisdicción, juzgado y tipo de expediente (Ley 9198 y Ley 10058), según la fecha de inicio del expediente.

# Movimientos Ley 9198

select * from movimientos_9198(inicio, fin);
# Es una función de Postgresql que recibe como parámetros el período comprendido entre las fechas indicadas en la consulta. Retorna la cantidad de movimientos Ley 9198 por jurisdicción y juzgado, según la fecha de los mismos, considerando las nuevas denuncias, medidas y resoluciones/sentencias.

# Movimientos Ley 10058

select * from movimientos_10058(inicio, fin);

#Es una función de Postgresql que recibe como parámetros el período comprendido entre las fechas indicadas en la consulta. Retorna la cantidad de movimientos Ley 10058 por jurisdicción y juzgado, según la fecha de los mismos, considerando las nuevas denuncias, medidas y resoluciones/sentencias.

#Audiencias Concretadas Ley 9198

select * from audiencias_concretadas_9198(inicio,fin);

#Es una función de Postgresql que recibe como parámetros el período comprendido entre las fechas indicadas en la consulta. Retorna la cantidad de audiencias concretadas en expedientes Ley 9198 por jurisdicción y juzgado.

#Audiencias Concretadas Ley 10058

select * from audiencias_concretadas_10058(inicio,fin);

#Es una función de Postgresql que recibe como parámetros el período comprendido entre las fechas indicadas en la consulta. Retorna la cantidad de audiencias concretadas en expedientes Ley 10058 por jurisdicción y juzgado.
