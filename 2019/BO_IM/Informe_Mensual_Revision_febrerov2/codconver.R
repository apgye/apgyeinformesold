# Conversor de códigos de variables
codconver <- function(df, operacion, variable) {
  
  if (operacion == "AUDIF" & variable == "ta") {
    df <- df %>% 
      mutate(ta = case_when(
        ta == "1" ~ "Control Legalidad Intern/Ext.",
        ta == "2" ~  "Escucha Niño/Niñas/Adol.",
        ta == "3" ~  "Escucha Personas c/Capac.Rest.",
        ta == "4" ~  "Escucha Denunciante V.Fam. o Género",
        ta == "5" ~  "Escucha Denunciado V.Fam. o Género",
        ta == "6" ~  "Control de Legalidad Med.Excep.",
        ta == "7" ~  "Adoptabilidad",
        ta == "8" ~  "Proceso de Vinculación",
        ta == "9" ~  "Guarda con Fines de Adopción",
        ta == "10" ~  "Adopción",
        ta == "11" ~  "Audiencia de Conciliación",
        ta == "12" ~  "Audiencia Art.70 Preliminar",
        ta == "13" ~  "Audiencias en otros proc.volunt.",
        ta == "14" ~  "Audiencia de Divorcio",
        ta == "15" ~  "Juicio Oral",
        ta == "16" ~  "Audiencia de Alimentos",
        ta == "17" ~  "Otras",
        ta == "20" ~ "Formulación/Imputación",
        ta == "21" ~ "Audiencia Conclusiva",
        ta == "22" ~ "Suspensión de Juicio a Prueba",
        ta == "23" ~ "Medidas",
        ta == "24" ~ "Remisión a Juicio",
        ta == "25" ~ "Juicio Abreviado",
        ta == "26" ~ "Remisión Judicial",
        ta == "27" ~ "Juicio Oral",
        ta == "28" ~ "Integración de Sentencia",
        ta == "29" ~ "Otras",
        TRUE ~ "sd"))
    
  } else if (operacion == "AUDIF" & variable == "esta") {
    
    df <- df %>% 
      mutate(esta = case_when(esta == "1" ~ "fijada", 
                            esta == "2" ~ "realizada",
                            esta == "3" ~ "fracasada_incomp_parte",
                            esta == "4" ~ "fracasada_incomp_terceros",
                            esta == "5" ~ "cancelada",
                            esta == "6" ~ "no_realizada_xfalta_notific",
                            TRUE ~ "sd")) %>% 
      mutate(esta = factor(esta, 
                         levels = c("fijada", 
                                    "realizada",
                                    "fracasada_incomp_parte",
                                    "fracasada_incomp_terceros",
                                    "cancelada",
                                    "no_realizada_xfalta_notific",
                                    "sd"), ordered = TRUE))
      
    
  } else if (operacion == "AUDIF" & variable == "ra") {
    df <- df %>% 
      mutate(ra = case_when(ra == "T" ~ "conciliacion_total",
                          ra == "P" ~ "conciliacion_parcial",
                          ra == "S" ~ "sin_conciliacion_y_sin_apelacion",
                          ra == "A" ~ "sin_conciliacion_con_apelacion",
                          ra == "C" ~ "conciliacion_en_proceso_sin_contradiccion",
                          TRUE ~ "sd")) %>% 
      mutate(ra = factor(ra, 
                         levels = c("conciliacion_total", 
                                    "conciliacion_parcial", 
                                    "sin_conciliacion_y_sin_apelacion",
                                    "sin_conciliacion_con_apelacion", 
                                    "conciliacion_en_proceso_sin_contradiccion",
                                    "sd"), ordered = TRUE))
    
  } else if (operacion == "AUDIF" & variable == "mat") {
    
    df <- df %>% 
      mutate(mat = case_when(mat == "F" ~ "familia",
                           mat == "P" ~ "penal",
                           TRUE ~ "sd")) 
    
  } else if (operacion == "AUDIL" & variable == "esta") {
    
    df <- df %>% 
      mutate(esta = case_when(esta == "1" ~ "fijada", 
                              esta == "2" ~ "realizada",
                              esta == "3" ~ "no_realizada",
                              esta == "4" ~ "cancelada",
                              esta == "5" ~ "reprogramada",
                              TRUE ~ "sd")) %>% 
      mutate(esta = factor(esta, levels = c("fijada", 
                                            "realizada",
                                            "no_realizada",
                                            "cancelada",
                                            "reprogramada",
                                            "sd")))
      
  } else if (operacion == "AUDIL" & variable == "ta") {
  
    df <- df %>% 
      mutate(ta = case_when(ta == "P" ~ "conciliacion", 
                          ta == "V" ~ "vista_causa", 
                          ta == "O" ~ "otras", 
                          TRUE ~ "sd")) %>% 
      mutate(ta = factor(ta, levels = c("conciliacion", 
                                      "vista_causa", 
                                      "otras",
                                      "sd"), ordered = TRUE))
      
  } else if (operacion == "AUDIL" & variable == "ra") {
  
    df <- df %>% 
      mutate(ra = case_when(
        ra == "T" ~ "Conciliacion_Total",
        ra == "P" ~ "Conciliacion_Parcial",
        ra == "S" ~ "Sin_Conciliacion", 
        TRUE ~ "sin_dato")) %>% 
      mutate(ra = factor(ra, levels = c("Conciliacion_Total", 
                                        "Conciliacion_Parcial", 
                                        "Sin_Conciliacion",
                                        "sin_dato"), ordered = TRUE))
  
  } else if (operacion == "AUDIC" & variable == "ra") {
    
    df <- df %>% 
      mutate(ra = case_when(
        ra == "T" ~ "conciliacion_total",
        ra == "P" ~ "conciliacion_parcial",
        ra == "S" ~ "sin_conciliacion", 
        TRUE ~ "sin_dato")) %>% 
      mutate(ra = factor(ra, levels = c("conciliacion_total", 
                                        "conciliacion_parcial", 
                                        "sin_conciliacion",
                                        "sin_dato"), ordered = TRUE))
    
  } else if (operacion == "AUDIC" & variable == "ta") {
    
    df <- df %>% 
      mutate(ta = case_when(ta == "1" ~ "preliminar", 
                            ta == "2" ~ "vista_causa", 
                            ta == "3" ~ "conciliación",
                            ta == "4" ~ "otras", 
                            TRUE ~ "sd")) %>% 
      mutate(ta = factor(ta, levels = c("preliminar", 
                                        "vista_causa",
                                        "conciliación",
                                        "otras",
                                        "sd"), ordered = TRUE))
    
  } else if (operacion == "AUDIC" & variable == "esta") {
    
    df <- df %>% 
      mutate(esta = case_when(esta == "1" ~ "fijada", 
                              esta == "2" ~ "realizada",
                              esta == "3" ~ "no_realizada",
                              esta == "4" ~ "cancelada",
                              esta == "5" ~ "reprogramada",
                              TRUE ~ "sd")) %>% 
      mutate(esta = factor(esta, levels = c("fijada", 
                                            "realizada",
                                            "no_realizada",
                                            "cancelada",
                                            "reprogramada",
                                            "sd")))
  } else if (operacion == "CIE2" & variable == "ibs") {
    
    df <- df %>% 
      mutate(ibs = case_when(ibs == "1" ~ "ingresa", 
                             ibs == "2" ~ "baja_a_1ainst",
                             ibs == "3" ~ "sube_ultima_instanc",
                              TRUE ~ "sid_dato")) %>% 
      mutate(ibs = factor(ibs, levels = c("ingresa", 
                                            "baja_a_1ainst",
                                            "sube_ultima_instanc",
                                            "sid_dato")))
  }
  
  df
}







#########################################################################


# df de operaciones y variables
# operavar <- tibble::tribble(
#   ~operacion, ~variable,  ~activa, ~finicio, ~ffin, ~codigo, ~significado,
#   "AUDIF", "ta", 1, "2019-01-01", NA, "1", "Control de Legalidad Internaciones/Externaciones",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "2",  "Escucha Niño/Niñas/Adolescentes",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "3",  "Escucha Personas c/Capacidad Restringida",
#   "AUDIF", "ta", 0, "2019-01-01", "2018-10-22", "4",  "Escucha al Denunciante Violencia Familiar o de Género",
#   "AUDIF", "ta", 0, "2019-01-01", "2018-10-22", "5",  "Escucha al Denunciado",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "6",  "Control de Legalidad Medidas Excepcionales",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "7",  "Adoptabilidad",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "8",  "Proceso de Vinculación",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "9",  "Guarda con Fines de Adopción",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "10",  "Adopción",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "11",  "Audiencia de Conciliación",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "12",  "Audiencia Art.70 Preliminar(ej. Filiación)",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "13",  "Audiencias en otros procesos voluntarios",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "14",  "Audiencia de Divorcio",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "15",  "Juicio Oral",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "16",  "Audiencia de Alimentos",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "17",  "Otras",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "20", "Formulación/Imputación", 
#   "AUDIF", "ta", 1, "2019-01-01", NA, "21", "Audiencia Conclusiva",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "22", "Suspensión de Juicio a Prueba",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "23", "Medidas",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "24", "Remisión a Juicio",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "25", "Juicio Abreviado",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "26", "Remisión Judicial",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "27", "Juicio Oral",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "28", "Integración de Sentencia", 
#   "AUDIF", "ta", 1, "2019-01-01", NA, "29",  "Otras"
# )



