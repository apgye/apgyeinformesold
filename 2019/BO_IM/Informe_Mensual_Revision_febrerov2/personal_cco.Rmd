---
title: "Planta Ocupada en el trimestre"
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Planta Ocupada Últimos Tres Meses 

Para conocer la cantidad de personal que efectivamente se desempeñó en el organismo (es decir: su planta ocupada real) en el último trimeste la siguiente tabla muestra la cantidad de personas por cargo en los últimos tres meses. 

```{r}
personal_trimestre(poblacion = jdos_cco) %>%
    outputTable(caption = "Planta Ocupada en el último trimestre") %>%
    row_spec(0, angle = 90)
```

## Dias de Trabajo y Días de Licencia por Cargo (aprox.)

En esta tabla se muestran los días laborables (*diasDisponibles*), días de licencia por cargo (*dias_licencias*) y porcentaje de días trabajados (*disponibilidad_ptje*). No se tuvieron en cuenta las declaraciones de inhábil judicial por lo que el porcentaje de día trabajados es una aproximación. 


```{r}
disponibilidad_trimestre(poblacion = jdos_cco) %>%
    outputTable(caption = "Dias Trabajados por Cargo y Días de Licencia") %>%
    row_spec(0, angle = 90)
```


