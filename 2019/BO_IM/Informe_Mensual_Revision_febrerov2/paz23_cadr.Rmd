---
title: Causas Resueltas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## 2a y 3a Cat - Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_paz_23(
  poblacion = jdos_paz_23,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Resoluciones y Vencimientos") %>%
    row_spec(0, angle = 90)  %>%
  landscape()
```

## Movimientos y Resoluciones en materia de Violencia `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan movimientos en materia de Violencia informadas por el REJUCAV dependiente del Centro Judicial de Género. Según la información provista por el registro en la tabla se muestran las *nuevas denuncias, medidas y resoluciones/sentencias.*

```{r}
movimientos_violencias(jdos_paz_23, start_date, end_date) %>%
  outputTable(caption = "Movimientos en materia de Violencia") %>%
  landscape()

```

