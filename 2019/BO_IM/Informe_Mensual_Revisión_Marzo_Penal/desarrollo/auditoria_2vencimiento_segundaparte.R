
jdocco1pna_dic <- primaria("jdofam0100pna", "CADR1C", "2018-12-01", "2019-01-01")

poblacion <- jdos_fam %>% 
  filter(organismo == "jdofam0100pna")

resoluciones_fam_prim <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", 
                                  end_date = "2018-09-02") {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    resolverconvertidos() %>% 
    mutate(fres = dmy(fres), fvenc1 = dmy(fvenc1), fvenc2 = dmy(fvenc2)) %>% 
    collect()
  
  
    resultado <- resultado %>%
      filter(!is.na(fres)) %>% 
      mutate(control_intervalo =  fres >= data_interval_start & fres < data_interval_end) %>% 
      mutate(control_mmp = tres != 0 | is.na(tres)) %>% 
      mutate(
      atermino = 
        (fres <= fvenc1 & 
           control_intervalo & 
           control_mmp), 
      res_luego1venc = 
        (fres > fvenc1 &
           fres <= fvenc2 & 
           control_intervalo &
           control_mmp), 
      res_luego2venc = 
        (fres > fvenc2 &
           control_intervalo &
           control_mmp)) %>% 
      rowwise() %>% 
      mutate(inconsistencia = sum(atermino, res_luego1venc, res_luego2venc, na.rm = T)) %>% 
      rowwise() %>% 
      mutate(atermino = ifelse(atermino, 1, NA),
             res_luego1venc = ifelse(res_luego1venc, 1, NA), 
             res_luego2venc = ifelse(res_luego2venc, 1, NA), 
             inconsistencia = ifelse(inconsistencia == 2, 1, NA)) %>% 
      mutate(caratula = str_sub(caratula, 1,10)) %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
      select(organismo = iep, mes, nro, caratula, fvenc1, fvenc2, fres, atermino, res_luego1venc, 
             res_luego2venc, inconsistencia2venc = inconsistencia) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], by = "organismo") %>% 
      select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo) %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      janitor::adorn_totals("row")
    
  
  resultado
  
}

jdocco1pna_eval <- resoluciones_fam_prim(poblacion, 
                      "CADR1C", 
                      "2018-02-01", 
                      "2019-01-01")



jdocco1pna_eval <- jdocco1pna_eval %>% 
  rowwise() %>% 
  mutate(inconsistencia = sum(atermino, res_luego1venc, res_luego2venc, na.rm = T)) %>% 
  rowwise() %>% 
  mutate(atermino = ifelse(atermino, 1, NA),
         res_luego1venc = ifelse(res_luego1venc, 1, NA), 
         res_luego2venc = ifelse(res_luego2venc, 1, NA), 
         inconsistencia = ifelse(inconsistencia == 2, 1, NA))
  
  







