---
title: Audiencias - Estados y Resultados
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}
audic <- audicco_prim(jdos_cco, 
             start_date, 
             end_date) 

audicco_realizadas_xtipo_cantidad_minutos(audic, desagregacion_mensual = desagregacion_mensual) 

```

## Audiencias según sus distintos estados finales - `r getDataIntervalStr(start_date, end_date)`

```{r}
audicco_xestado(audic, desagregacion_mensual = desagregacion_mensual) %>% 
  select(-fijada) %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  landscape()
```

## Audiencias realizadas por Tipo - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presenta información de las audiencias realizadas para los distintos tipos de audiencia. Más abajo se detallan los resultados obtenidos por tipo de audiencia.

```{r}
cantidad_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Cantidad de Audiencias") %>%
  landscape()
```

## Minutos en Audiencia - `r getDataIntervalStr(start_date, end_date)`
 
En esta tabla se presenta información de los minutos en audiencia por magistrado.

```{r}
minutos_audiencias %>% 
  #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>% 
  outputTable(caption = "Minutos en Audiencia del Magistrado") %>%
  landscape()
```


## Audiencias Realizadas y sus Resultados - `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se muestran las audiencias realizadas en cada organismo agrupadas por sus tipos y los resultados obtenidos para cada tipo de audiencia. Los resultados posibles según definición institucional son:

+ Conciliación Total  
+ Conciliación Parcial   
+ Sin conciliación 

```{r}
audicco_realizadas_xtipoyresultado(audic, desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias Realizadas y sus Resultados") %>%
  landscape()
```


## Audiencias Videofilmadas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan las audiencias realizdas videofilmadas.

```{r}
audicco_realizadas_video_xtipo(audic, desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias Videofilmadas") %>%
  landscape()
```


## Audiencias Vista de Causa Programadas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan las audiencias de Vista de Causa Programadas para los próximos meses.

```{r}
audiccco_vc_program(audic) %>% 
  outputTable(caption = "Audiencias de Vista de Causa Programadas") %>% 
  landscape()
```



