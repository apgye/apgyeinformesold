---
title: Causas y Trámites iniciados ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Causas Iniciadas por Grupo de Procesos  - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura.


```{r }
iniciados_paz_procxgpo(poblacion = jdos_paz,
              start_date = start_date,
              end_date = end_date) %>% 
  iniciados_paz_proc_comparativo() %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) 
```

\pagebreak

## Trámites Iniciados - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestra el total de trámites voluntarios iniciados por juzgado.       

Recordamos, conforme lo señalado en diversas oportunidades, que según lo prevé la Guía de Estadística –pto.1, página 4- para esta información debe tenerse presente que: “Los Juzgados de Paz 1a categoría deberán registrar e informar también los casos correspondientes al Turno Voluntario, actuaciones que tendrán un conteo particular independiente a los “procesos judiciales”. A tal fin deben producir el listado de causas iniciadas que se explica en este apartado posicionándose en la solapa “Todos” de tal forma de incluir las actuaciones iniciadas en Turno Voluntario.”     

Eventualmente si el juzgado detecta un error en este punto debe corregirlo de la siguiente manera: “representar” vía JUSTAT los períodos correspondiente, produciendo el listado de Causas y Trámites iniciados conforme a la metodología acostumbrada con atención de posicionarse en la solapa “Todos” conforme se expresa en el párrafo anterior.      

```{r }
iniciados_paz_tramxgpo(poblacion = jdos_paz,
              start_date = start_date,
              end_date = end_date) %>%
  iniciados_paz_tram_comparativo() %>%
  kable(caption = "Trámites Iniciados por Grupo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) 
```

