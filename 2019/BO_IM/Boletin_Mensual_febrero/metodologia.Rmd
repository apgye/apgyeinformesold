---
title: Metodología - Estadística Pública Judicial STJER
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


# Metodología - Estadística Pública Judicial STJER

Las Nuevas Estadísticas del Poder Judicial de Entre Ríos sugieron en el 2018 como resultado del Plan de Modernización Estadística (resolución TS 277/16, concluido el 21/09/18) y están sujetas al Reglamento de Estadística vigente –cfr. Res. Tribunal Superintendencia Nº234/17, http://www.jusentrerios.gov.ar/estadistica/reglamento-de-estadistica-19-09-17/.  Este nuevo modelo estadístico está integrado por un conjunto de operaciones diseñadas e implementadas por el Área de Planificación Gestión y Estadística y constituye un sistema coherente de definiciones sustantivas, metodologías, procedimientos y herramientas dirigido a la producción de información de calidad.    

Para el mantenimiento y actualización del sistema se realizan consultas a magistrados y funcionarios –autorizadas por las Salas del STJER- y se celebran encuentros de trabajo donde se recogen ideas y definiciones relevantes para la actualización y construcción de indicadores judiciales. Al mismo tiempo, se trabaja en la revisión de indicadores comparados con distintos Poderes Judiciales provinciales y documentos técnicos sobre estadística judicial. Todo esto tiene como resultado un set de indicadores aprobados institucionalmente y en permanente actualización. 

Los procedimientos empleados en el relevamiento de datos usted los puede consultar (por órgano, instancia y fuero) en las Guías de Estadística accesibles a través de la Página Web del Superior Tribunal de Justicia, en la sección del Área de Planificación Gestión y Estadística, accediendo a la opción *RECURSOS*, y luego *GUÍAS SOBRE ESTADÍSTICA JUDICIAL (NUEVO)*. O bien accediendo directamente a la dirección: https://drive.google.com/drive/folders/1HjJ8E82mcIVIck4H0giFWwTmOVLbK2NZ eligiendo la guía correspondiente. A través de los siguientes enlaces puede acceder a dichas guías directamente:

+ [Juzgados Civil y Comercial, y Paz 1ªCat.](https://drive.google.com/open?id=1EOJggtPkUTgHQRuQesBuzmEZMoYe2UO3juRUVjiVFNw)
+ [Juzgados de Familia](https://drive.google.com/open?id=1nW02icHcRpLz8w_5X9a_uEYCsyQ6Nzf_dNzsNljpxXU)
+ [Juzgados Laborales](https://drive.google.com/open?id=1I051W6JH2lQ0kH3QH_5fW0xXYhkLGehoJpCLtmdOBuc)
+ [Juzgados de Paz 2ª y 3ª Cat.](https://drive.google.com/open?id=1uLoT22Kr0SmnNCQD2J3LbapuJQUs3vBvjYX4fno_I5A)
+ [Camaras Civiles](https://drive.google.com/open?id=1OhqQbi1dRkAyISQuC5pkNtEKe467-fB1icK1AQrh2bE)
+ [Camaras Laborales](https://drive.google.com/open?id=1uJNgds2fREkteS7c5OVGypRD5J9nvjwjsm2RGBJh8e4)
+ [Camaras Contencioso Administrativas](https://drive.google.com/open?id=1EJmsBq1Wzg5sLKXmgS3VXak2duu4Q75vydyVZergmtQ)
+ [Camaras de Casación Penal](https://drive.google.com/open?id=1V6pz_sW2v-vbyJxufaoMtgqUiiJ-6REoB97BNnlMYFA)
+ [Oficinas de Gestión de Audiencia Penales](https://drive.google.com/open?id=1mRQbflTQeORMGGUWOhz8Q6i0zCnJox5HL67PVlTPGu4)
+ [Superior Tribunal de Justicia - Contencioso Administrativo](https://drive.google.com/open?id=1Ic4eI79cWCAgckpjjU_N3kqCZKJ8biFoYkVOJe8mzOQ)
+ [Sala I de Procedimientos Constitucionales y Penal del STJER](https://drive.google.com/open?id=1ZislKECWEEXkLAbyEmgCWNiYGzBhKqB59b1LUZSR6kM)
+ [Sala II Civil y Comercial STJER](https://drive.google.com/open?id=1Qcptj4TrXvfcRYcZ9iSuI3O4sIefcJ2dsruttqGAdtA)
+ [Sala III del Trabajo STJER](https://drive.google.com/open?id=1SX8JuqWFR0xXOK3Y2R6HqcOwN_sLtsgNoI5ZQODP8yA)

Las rutinas de procesamiento estadístico empleadas en el cálculo de indicadores usted las puede consultar seleccionando la operación correspondiente del repositorio accesible en el siguiente [link](https://bitbucket.org/apgye/apgyeoperationsjuser/src/master/R/).   


## Observaciones sobre Indicadores Estadísticos

El principal desafío tecnológico en la producción de indicadores judiciales se encuentra en las limitaciones que tiene la herramienta de Gestión de Causas del STJER Lex-Doctor 9. Dicho sistema carece de funcionalidades adecuadas para el registro y producción de información estadística, y en general presenta configuraciones abiertas para el registro de datos que facilitan la propagación de errores de registro. Por estas razones permanentemente se están fortaleciendo y revisando los procedimientos de validación de datos para subsanar anomalías. 

Sin perjuicio de lo anterior, cada indicador estadístico tiene un tratammiento específico, cuyas notas sobresalientes publicitamos aquí para una adecuada valoración de su alcance. 

+ **Causas Iniciadas**: para el cómputo de este indicador se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, tipo de proceso y fecha de inicio. Dadas las dificultades en materia de registración antes apuntadas se realizan exclusiones de datos anómalos por fuero (e.g. exclusiones en materia civil en razón de registros mal cargados como "causa iniciada" dado que sus tipos de proceso no se vinculan con tipos procesales vigentes o directamente porque no constituyen causas: OFICIO, EXHORTO, SUMARIO, COMPENSACION, DESARCHIVO, INFORME, DENUNCIA, CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO, PERSONAL, entre otros). El listado completo de exclusiones por materia puede consultarse en el siguiente [link](https://docs.google.com/document/d/1olfVtyeh-6v71HYxLaNaEIaRon4w38WjLyZmn25O_rc/edit?usp=sharing). Para los trámites voluntarios de los órganos de paz se prescinde del dato sobre número de expediente atento al criterio de registración que emmplean estos organismos. Para las causas iniciadas en el período febrero-mayo 2018 se extrajo información desagregada del listado de Causas en Trámite en ese período. A partir de junio-2018 se habilita una operción particular para los causas iniciadas en primera instancia y en diciembre-2018 en segunda instancia.  

+ **Causas Resueltas**: para el cómputo de este indicador se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, y fecha de resolución dentro del período correspondiente. Una resolución registrada fuera del período al que corresponde (e.g. sentencia de octubre 2018 registrada y declarada en el listado de causas resueltas en noviembre 2018) no se computa como declaración válida. Todos los intervalos temporales se computan en días corridos. Las fechas de vencimiento corresponden a plazos calculados por cada órgano judicial.    

+ **Causas en Trámite**: para el cómputo de este indicador se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, fecha de inicio y fecha del último acto procesal registrado. Esta última restricción es muy importante antento a que si el registro no tiene valor de al menos un acto procesal registrado en el sistema no se computa como causa en tramite. Ello es así a fin de evitar contar registros donde no se dispone de información suficiente para asumir que existe una causa iniciada.

+ **Audiencias**: para el cómputo de audiencias realizadas se evalúan los registros informados que disponen de datos sobre número de expediente, carátula, tipo de audiencia, estado de audiencia realizada y fecha de audiencia en el período correspondiente. En general el trabajo con este indicador supone la corrección de diversas anomalías (ej.se eliminan las referencias a minutos en el campo de duración en minutos (duracm) sin reimputación de datos por parte del área. 

+ **Trámnites del Turno Voluntario**: La registración de los trámites voluntarios en Lex-Doctor está prevista en el Reglamento para Juzgados Civiles y Comerciales Acuerdo Especial 02/07/2008 (pt.3.1, inc.3). Sin perjuicio de ello, en el relevameinto de este indicador se constató  diversidad de criterios y prácticas de registración en los equipos de trabajo, lo cual obligó a efectuar comunicaciónes aclaratorias específicas para su instrumentación. En tal sentido, los trámites iniciados comienzan a relevarse a través de su operación específica por listado en junio 2018 (comunicación efectuada el 14/06/18), y ante las consultas efectuadas en los meses siguientes el 03/09/18 se emite una circular explicitando la metodología específica para informar trámites del turno voluntario.     


## Datos Primarios disponibles en el período `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se detallan los listados primarios de datos disponibles. 

```{r}
listar_presentaciones() %>% 
  outputTable(caption = "Listados presentados por órgano") %>%
  column_spec(3, width = "7cm") %>% column_spec(4, width = "7cm") %>%
  row_spec(0) %>%
  landscape()

```
