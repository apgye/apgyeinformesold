---
title: Duracion de Procesos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Duracion de Procesos por Organismo: Resumen Comparativo - `r getDataIntervalStr(start_date, end_date)`

```{r}
duracion(stj_cad, 
         "CADRCADS", 
         start_date = start_date,
         end_date = end_date,  
         desagregacion_mensual = F, 
         desagregacion_xorg = T) %>% 
  filter(cantidad_casos > 2) %>% 
  # kable(caption = "Duración de Procesos", align = 'c', longtable = TRUE ) %>% 
  # kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
  #               full_width = F, font_size = 10)  %>% 
  # column_spec(3, "10cm") %>%
  outputTable(caption = "Duración de Procesos") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```



## Primaria


```{r}

resultado <- DB_PROD() %>% 
  apgyeTableData("CADRCADS") %>% 
  mutate(finicio = dmy(finicio), fres = dmy(fres)) %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% !!stj_cad$organismo) %>% 
  group_by(iep) %>% collect() %>% 
  filter(!is.na(fres), toupper(as) == "S") %>% 
  mutate(durac = fres - finicio) %>% 
  select(organismo = iep, nro, caratula, tproc, fecha_inicio = finicio, fecha_resolucion = fres,
         duracion = durac) 

resultado %>% 
  mutate(caratula = word(caratula,1, 1)) %>% ungroup() %>% 
  select(-organismo) %>% 
  mutate(observacion = " ") %>% 
  kable(caption = "Duracion de Procesos", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  # column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()


```

