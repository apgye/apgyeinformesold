---
title: Oralidad
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
source("~/apgyeinformes/2019/Grupo_Informes_Oralidad_v2/utilsO.R")
source("oralidad.R")
```

```{r}
## Casos Oralizados
jdos_civiles <- listar_organismos() %>% 
  filter(tipo == "jdo", str_detect(materia, "cco" )) %>% 
  filter(!organismo %in% c("jdocco0501con", "jdocco0502con",  "jdocco0301uru", 
                           "jdocco0302uru", "jdocco0901pna", "jdocco0902pna", 
                           "jdocco1001pna", "jdocco1002pna"))

#Casos2018
df_casos_base <- readRDS("~/apgyeinformes/2019/Grupo_Informes_Oralidad_v2/data/oralidad2018.rds")
#Preliminares
audic_prelim_raw <- preliminaresO(jdos_civiles, "2019-02-01", end_date)
# Audiencia Vista Causa
audic_vcausa_raw <- vcausaO(jdos_civiles, "2019-02-01", end_date)
# Resoluciones
resoluciones <- resolucionesO(jdos_civiles, "2019-02-01", end_date)
# Consolidado
# imputs df_1ros_casos, audic_prelim_raw, audic_vcausa_raw
consolidado <- consolidar(df_casos_base, audic_prelim_raw, audic_vcausa_raw, resoluciones)


consolidado_cfmeMINJUS <- consolidado %>% 
  left_join(jdos_civiles[, c("organismo", "organismo_descripcion", 
                           "circunscripcion")], by = c("iep" = "organismo")) %>% 
  select(circunscripcion, organismo_descripcion, everything()) %>% 
  mutate(organismo = str_remove_all(iep, "[a-z,0]")) %>%  
  mutate(fecha_resolucion = as.character(fecha_resolucion)) %>%
  #reimputación solicitada por el ministerio siempre que resultado preliminar T, 
  #forma de resolucion = Conciliacion y fecha de efectivizacion
  mutate(forma_deresolucion =  ifelse(resultado_preliminar == "T" & 
                                        !is.na(resultado_preliminar), 
                                      "Conciliación", forma_deresolucion), 
         fecha_resolucion =  ifelse(resultado_preliminar == "T" & 
                                      is.na(fecha_resolucion), 
                                    as.character(fecha_efect_preliminar), 
                                    fecha_resolucion)) %>% 
  mutate(forma_deresolucion =  ifelse(resultado_vcausa == "T" & 
                                        !is.na(resultado_vcausa), 
                                      "Conciliación", forma_deresolucion),
         fecha_resolucion =  ifelse(resultado_vcausa == "T" & 
                                      is.na(fecha_resolucion), 
                                    as.character(fecha_efect_vcausa), 
                                    fecha_resolucion)) %>% 
   select(circunscripcion, organismo_descripcion, iep, organismo, everything()) %>% 
   arrange(circunscripcion, organismo_descripcion)  

durac_proc_conciliados <- consolidado_cfmeMINJUS %>% 
  filter(fecha_ingreso > make_date(2018,08,01)) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "DAÑOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "(CIVIL)"), "ORDINARIO", tproc)) %>%
  #filter(estado_preliminar == "realizada", resultado_preliminar == "T") %>%
  filter(estado_preliminar == "realizada", resultado_preliminar %in% c("T", "S")) %>% 
  mutate(mediana_duracion_conciliados = fecha_efect_preliminar - fecha_ingreso) %>% 
  group_by(tproc, resultado_preliminar) %>% 
  summarise(mediana_duracion = round(median(as.integer(mediana_duracion_conciliados), na.rm = T)), 
            cantidad_casos = n()) %>% 
  filter(cantidad_casos > 2) %>% 
  mutate(duracion_en_meses = round(mediana_duracion/30)) %>% 
  mutate(resultado_preliminar = ifelse(resultado_preliminar == "T", "conciliado en oralidad", "con sentencia en oralidad")) %>% 
  rename(grupo = resultado_preliminar)
  
  

jdos_cco_solo <- jdos_cco %>% 
  filter(!str_detect(materia, "eje|cqb"))
         
durac <- duracion(jdos_cco_solo, "CADR1C", start_date = "2017-07-01", end_date = "2018-01-01",  
         desagregacion_mensual = F, desagregacion_xorg = F) %>% 
  mutate(duracion_en_meses = round(mediana_duracion/30)) %>% 
  rename(tproc = tipo_proceso) %>% 
  mutate(grupo = "previo a la oralidad") %>% 
  semi_join(durac_proc_conciliados, by = "tproc")
  

durac_comparativo <- durac_proc_conciliados %>% 
  bind_rows(durac)




```


## Duración de Procesos sometidos a Oralidad `r getDataIntervalStr(start_date, end_date)`

En la próxima tabla se presenta la duración de procesos sometidos al Protocolo de Oralidad.   

La duración se mide en días corridos desde el inicio de la causa hasta la fecha de la Audiencia Preliminar en la que se alcanzó un Acuerdo Conciliatorio.    


```{r}
durac_proc_conciliados %>% 
  kable(caption = "Procesos de Conocimiento Iniciados en el Período", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```


```{r}

durac_comparativo %>% 
  ggplot(aes(x = tproc, y = mediana_duracion, fill=grupo)) +
  geom_bar(aes(group = grupo), stat='identity', position = 'dodge') +
  geom_text(aes(x = tproc, y = mediana_duracion , label = mediana_duracion, group = grupo), 
    hjust = -2, vjust = 0.5, 
    position = position_dodge(width = 1), inherit.aes = TRUE, size = 3) +
  geom_bar(stat="identity", position = "dodge2") +
  coord_flip() +
  theme(axis.title.x=element_blank(), axis.title.y=element_blank(),
        axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
  labs(title = "Comparación de las duraciones de procesos según aplicación del Protocolo de Oralidad",
       caption = "APGE-STJER") 
```

