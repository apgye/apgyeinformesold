---
title: Causas Iniciadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r, cad2_ci, eval= (ci & t)}
iniciados_cam(poblacion = cam_civil, 
              start_date = start_date,
              end_date = end_date) %>%
  outputTable(caption = "Causas Iniciadas") %>%
  landscape()

```

