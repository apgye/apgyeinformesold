---
title: Resoluciones en Garatías 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Resoluciones en Garantías - Actos Conclusivos y Resoluciones - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar las resoluciones penales dictadas por cada magistrado de garatías (incluyendo las subrogaciones) según registro efectuado por OGA.   

Para una fácil consulta se agruparon los Actos Conclusivos y demás Resoluciones en dos tablas separadas. También, en la Tabla de los Actos Conclusivos, se agrega al final de la tabla el dato de personas sobreseidas y condenadas según el registro de justiciables efectuado en cada legajo.     

Finalmente, la columna total de cada tabla muestra la cantidad de resoluciones emitidas por juez y el total de la circunscripción.    


```{r}

resoluciones <- resoluciones_pen(poblacion = oga,
                                 start_date = start_date,
                                 end_date = end_date,
                                 desagregacion_mensual = desagregacion_mensual)


  # Tabla de Actos Conclusivos
  cant_rows <- nrow(resoluciones$ac) - 1
  resoluciones$ac %>% 
    select(-fecha) %>% 
    kable("latex", longtable = T, booktabs = T, caption = "Actos Conclusivos") %>%
      add_header_above(c(" " = 3, "Sobreseimientos" = 8,
                         "Juicios Abreviados" = 4, " " = 3)) %>%
      kable_styling(latex_options = c("repeat_header")) %>% 
      row_spec(0, angle = 90) %>%
      row_spec(1:cant_rows, hline_after = T) %>% 
      column_spec(3, "2,5cm") %>%
      column_spec(c(3,11,15,16), border_right = T) %>%
      landscape()
      
  # Tabla de Resoluciones
  cant_rows <- nrow(resoluciones$res) - 1
  resoluciones$res %>% 
    select(-fecha) %>% 
    kable("latex", longtable = T, booktabs = T, caption = "Resoluciones") %>%
      kable_styling(latex_options = c("repeat_header")) %>% 
      row_spec(0, angle = 90) %>%
      row_spec(1:cant_rows, hline_after = T) %>% 
      column_spec(3, "2,5cm") %>%
      column_spec(c(3,17), border_right = T) %>%
      landscape()
  
```
