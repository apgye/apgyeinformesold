---
title: Resoluciones en Tribunal de Juicio y Apelación
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Resoluciones en Tribunal de Juicio - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar las resoluciones por tipo de condena en cada circunscripción.  

```{r, tjui, echo = FALSE, message = FALSE, warning = FALSE}
resoluciones_pen(poblacion = tja,
              start_date = start_date,
              end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>%
  filter(circunscripcion != "Total") %>% 
  select(-magistrado, -sobreseidos, -condenados, -absueltos) %>% 
  group_by(circunscripcion, mes) %>% 
  summarise_all(funs(sum)) %>% ungroup() %>% 
  group_by(circunscripcion) %>% 
  do(janitor::adorn_totals(.)) %>% 
  outputTable(caption = "Resoluciones en Tribunal de Juicio") %>%
  row_spec(0, angle = 90) %>%
  landscape()

```


## Resoluciones por Tipo de Proceso - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar las resoluciones dictadas por tribunal y tipo de proceso.

```{r, apel, echo = FALSE, message = FALSE, warning = FALSE}

actos_conclusivos_tjui_xtproc %>%
  group_by(circunscripcion, mes, tproc) %>% 
  summarise(cantidad_casos = n()) %>% 
  mutate(tproc = str_sub(tproc, 1,60)) %>% 
  outputTable(caption = "Resoluciones mensuales en Tribunal de Juicio agrupadas por Tipo de Proceso") %>%
  row_spec(0, angle = 90) %>%
  #column_spec(3, "20cm") %>% 
  landscape()

```

