---
title: Audiencias - Estados y Resultados
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}
audic <- audicco_prim(jdos_cco, 
             start_date, 
             end_date) 

audiccco <- audicco_xestado(audic, desagregacion_mensual = desagregacion_mensual) %>% 
  ungroup() %>% select(-fijada, -sd, -Total) %>% 
  rename(realizadas = realizada, no_realizadas = no_realizada, canceladas = cancelada, reprogramadas = reprogramada) 

# audicco_realizadas_xtipo_cantidad_minutos(audic, desagregacion_mensual = desagregacion_mensual) 

audicpen <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData('AUDIP') %>%
  apgyeDSL::interval(start_date , end_date) %>%
  group_by(iep, data_interval_start, data_interval_end) %>% 
  process() %>% .$result %>% 
  left_join(oga %>% select(circunscripcion, organismo, organismo_descripcion), by=c("iep"="organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, audiencias_realizadas, 
         audiencias_fracasadas, audiencias_canceladas,  audiencias_reprogramadas) %>% 
  group_by(circunscripcion) %>% 
  summarise(realizadas = sum(audiencias_realizadas, na.rm = T),
             fracasadas = sum(audiencias_fracasadas, na.rm = T), 
             canceladas = sum(audiencias_canceladas, na.rm = T),  
             reprogramadas = sum(audiencias_reprogramadas, na.rm = T)) %>% 
  janitor::adorn_totals("row") %>% 
  rowwise() %>% 
  mutate(audic_totales = realizadas + fracasadas + canceladas + reprogramadas) %>% 
  mutate(tasa_realizacion = stringr::str_c(round(realizadas/audic_totales*100), ' %')) %>% 
  mutate(organismo_descripcion = str_c("OGA ", circunscripcion)) %>% 
  select(circunscripcion, organismo_descripcion, everything())
  
  
```

# Aclaración Metodológica

Los estados de audiencia aunque presentan variaciones nominales guardan similitudes en su definición.   

En el caso de las audiencias penales sus estados se definen como:    

+ Audiencia Realizada: audiencia efectivamente celebrada;
+ Audiencia Fracasada: audiencias que no se realiza por incomparencia de imputados o partes; 
+ Audiencia Cancelada: audiencia que estaba fijada y se dejó sin efecto a pedido de parte;
+ Audiencia Reprogramada: audiencia fijada que se suspende, fijándose nuevo día y hora para su realización.   

En el caso de las audiencias civiles sus estados se definen como:    

+ Audiencia Realizada: audiencia efectivamente celebrada;
+ Audiencia No Realizada: audiencias que no se realiza por incomparencia de parte; 
+ Audiencia Cancelada: audiencia que estaba fijada y se dejó sin efecto a pedido de parte; 
+ Audiencia Reprogramada: audiencia fijada que se suspende, fijándose nuevo día y hora para su realización.   

Cabe resaltar que a la fecha de producción de este informe está pendiente de implementación nuevas categorías y definiciones de audiencia aplicables a los fueros Civil y Comercial y Laboral, cuya experiencia en la implementación de oralidad evidencia una dinámica de estados de audiencias más rica que la admitida por la clasificación precedente (e.g.Audiencias con cuartos intermedios).      

Lic. Sebastián Castillo

\pagebreak


# Audiencias Civiles `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan los totales de audiencias entre febrero y abril, y la tasa de realización calculada como la proporción de audiencias realizadas respecto del total de los estados de audiencia. 

```{r}
audiccco %>% 
  ungroup() %>% 
  rowwise() %>% 
  mutate(audic_totales = realizadas + no_realizadas + canceladas + reprogramadas) %>% 
  mutate(tasa_realizacion = stringr::str_c(round(realizadas/audic_totales*100), ' %')) %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  landscape()
```

# Audiencias Penales `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan los totales de audiencias entre febrero y abril, y la tasa de realización calculada como la proporción de audiencias realizadas respecto del total de los estados de audiencia. 

```{r}
audicpen %>% 
  outputTable(caption = "Audiencias segun sus Estados") %>%
  landscape()
```







<!-- ## Audiencias realizadas por Tipo - `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se presenta información de las audiencias realizadas para los distintos tipos de audiencia. Más abajo se detallan los resultados obtenidos por tipo de audiencia. -->

<!-- ```{r} -->
<!-- cantidad_audiencias %>%  -->
<!--   #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>%  -->
<!--   outputTable(caption = "Cantidad de Audiencias") %>% -->
<!--   landscape() -->
<!-- ``` -->

<!-- ## Minutos en Audiencia - `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se presenta información de los minutos en audiencia por magistrado. -->

<!-- ```{r} -->
<!-- minutos_audiencias %>%  -->
<!--   #mutate(organismo_descripcion = abbreviate(organismo_descripcion, minlength = 10)) %>%  -->
<!--   outputTable(caption = "Minutos en Audiencia del Magistrado") %>% -->
<!--   landscape() -->
<!-- ``` -->


<!-- ## Audiencias Realizadas y sus Resultados - `r getDataIntervalStr(start_date, end_date)` -->

<!-- En la siguiente tabla se muestran las audiencias realizadas en cada organismo agrupadas por sus tipos y los resultados obtenidos para cada tipo de audiencia. Los resultados posibles según definición institucional son: -->

<!-- + Conciliación Total   -->
<!-- + Conciliación Parcial    -->
<!-- + Sin conciliación  -->

<!-- ```{r} -->
<!-- audicco_realizadas_xtipoyresultado(audic, desagregacion_mensual = desagregacion_mensual) %>%  -->
<!--   outputTable(caption = "Audiencias Realizadas y sus Resultados") %>% -->
<!--   landscape() -->
<!-- ``` -->


<!-- ## Audiencias Videofilmadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se presentan las audiencias realizdas videofilmadas. -->

<!-- ```{r} -->
<!-- audicco_realizadas_video_xtipo(audic, desagregacion_mensual = desagregacion_mensual) %>%  -->
<!--   outputTable(caption = "Audiencias Videofilmadas") %>% -->
<!--   landscape() -->
<!-- ``` -->


<!-- ## Audiencias Vista de Causa Programadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se presentan las audiencias de Vista de Causa Programadas para los próximos meses. -->

<!-- ```{r} -->
<!-- audiccco_vc_program(audic) %>%  -->
<!--   outputTable(caption = "Audiencias de Vista de Causa Programadas") %>%  -->
<!--   column_spec(6, "5cm") %>%  -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape() -->
<!-- ``` -->


<!-- ## Órganos Multifuero: Audiencias materia Violencia `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se presentan las audiencias en materia de Violencia informadas por el REJUCAV dependiente del Centro Judicial de Género. Este información comenzó a relevarse por parte del registro a partir de noviembre 2018.  -->

<!-- ```{r} -->
<!-- audifam_violencias(jdos_cco, start_date, end_date) %>% View() -->
<!--   outputTable(caption = "Audiencias en materia de Violencia") %>%  -->
<!--   landscape() -->

<!-- ``` -->

