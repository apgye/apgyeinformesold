
# Procesos con Movimientos

movimientos <- function(poblacion, start_date = "2018-07-01", end_date = "2018-09-02") {
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("CAMOV") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    filter(!is.na(finicio), !is.na(fmov)) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
    select(iep, nro, caratula, tproc, finicio, fmov, movt, data_interval_start, data_interval_end) %>% 
    collect() %>% 
    resolverconvertidos() %>% 
    tidyr::separate_rows(movt, sep="%")  %>%
    tidyr::separate(movt, into = c("fmovinterno", "descripcion"), sep="\\$") %>% 
    filter(!is.na(fmovinterno)) %>%
    mutate(fmovinterno = as.Date(fmovinterno, format = "%d/%m/%Y")) %>% 
    filter(fmovinterno >= data_interval_start, fmovinterno < data_interval_end) %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) 
  
  
   cantidad_causas_con_movimientoprocesal <- resultado %>% 
    distinct(iep, nro, caratula, tproc, mes) %>% 
    group_by(iep, mes) %>% 
    summarise(ncausa = n())
   
   cantidad_movimientos <- resultado %>% 
    group_by(iep, mes) %>% 
    summarise(nmov = n())
   
   mov <- cantidad_causas_con_movimientoprocesal %>% 
    left_join(cantidad_movimientos) %>% 
    arrange(iep, mes) %>% 
    group_by(iep) %>% 
    do(janitor::adorn_totals(.)) %>% 
    mutate('promedio movimientos por causa' = round(nmov/ncausa, digits = 1)) %>% 
    rename(organismo = iep, 'cantidad movimientos registrados' = nmov, 
           'causas con al menos un movimiento procesal' = ncausa) %>% 
    ungroup() %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], by = "organismo") %>% 
    select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo) %>% 
    mutate(circunscripcion = ifelse(mes == "-", "Total", circunscripcion))
  
  
  mov 
    
}


movimientos_oma <- function(poblacion, start_date = "2018-07-01", end_date = "2018-09-02") {
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("IGMP") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
    collect() %>% 
    left_join(poblacion %>% 
                select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
  
  
  movimientos_oma <- resultado %>% 
    mutate(index = 1:length(caratula)) %>% 
    tidyr::separate_rows(movt, sep="%")  %>%
    tidyr::separate(movt, into = c("fmovinterno", "descripcion"), sep="\\$") %>% 
    filter(!is.na(fmovinterno)) %>%
    mutate(fmovinterno = as.Date(fmovinterno, format = "%d/%m/%Y")) %>% 
    filter(fmovinterno >= data_interval_start, fmovinterno < data_interval_end) %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
    codconver("IGMP", "descripcion")  %>% 
    filter(tmov != "sin_dato") %>% # dado que el lagajo es compartido con MPF y OGA debo filtrar estricto
    mutate(cantidad_mov = 1) %>% 
    select(iep, resp, tmov, cantidad_mov) %>% 
    group_by(iep, tmov) %>% # elimine referencia a responsable campo mal registrado
    summarise(cantidad_movimientos = sum(cantidad_mov, na.rm = T)) %>% 
    tidyr::spread(tmov, cantidad_movimientos, fill = 0, drop = F) %>% 
    ungroup() %>% rename(organismo = iep) %>% 
    left_join(poblacion[, c("organismo", "organismo_descripcion", "circunscripcion")], by = "organismo") %>% 
    select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo) %>% 
    janitor::adorn_totals("col") %>% 
    janitor::adorn_totals("row")
  
  
  movimientos_oma 
  
}






