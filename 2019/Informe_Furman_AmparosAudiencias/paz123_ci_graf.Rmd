---
title: Causas Iniciadas - Gráfico
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}
inic_paz1 <- iniciados_paz_procxgpo(poblacion = jdos_paz, 
                                    start_date = start_date,  end_date = end_date) %>% 
  iniciados_paz_proc_comparativo() %>% 
  select(circunscripcion, organismo, Total) %>% 
  filter(circunscripcion != "Total") 


if (nrow(jdos_paz_23) != 0) {
  inic_paz23 <- iniciados_paz_23c(poblacion = jdos_paz_23,
                  start_date = start_date, end_date = end_date) %>%
  iniciados_paz_comparativo_23() %>%
  select(circunscripcion, organismo, proceso) %>% 
  rename(Total = proceso) %>% 
  filter(circunscripcion != "Total")

}


inic_fuero_proc <- inic_paz1 %>% 
  bind_rows(if(exists("inic_paz23")) inic_paz23) %>% 
  left_join(poblacion_total %>%
                  select(organismo=organismo_descripcion, circunscripcion, categoria), 
            by= c("circunscripcion","organismo")) %>% 
  select(circunscripcion, organismo, Total, categoria) %>% 
  arrange(desc(Total))


inic_paz1_tram <- iniciados_paz_tramxgpo(poblacion = jdos_paz,
              start_date = start_date,
              end_date = end_date) %>%
  iniciados_paz_tram_comparativo() %>% 
  select(circunscripcion, organismo, Total) %>% 
  filter(circunscripcion != "Total") 

if (nrow(jdos_paz_23) != 0) {
  inic_paz23_tram <- iniciados_paz_23c(poblacion = jdos_paz_23,
                  start_date = start_date, end_date = end_date) %>%
  iniciados_paz_comparativo_23() %>%
  select(circunscripcion, organismo, tramite) %>% 
  rename(Total = tramite) %>% 
  filter(circunscripcion != "Total")
}


inic_fuero_tram <- inic_paz1_tram %>% 
  bind_rows(if(exists("inic_paz23_tram")) inic_paz23_tram) %>% 
  left_join(poblacion_total %>%
                  select(organismo=organismo_descripcion, circunscripcion, categoria), 
            by= c("circunscripcion","organismo")) %>% 
  select(circunscripcion, organismo, Total, categoria) %>% 
  arrange(desc(Total))


```

 
# Causas Iniciadas en Juzgados 1ª, 2ª y 3ª categoría - Gráficos

## Causas Iniciadas

```{r}
inic_fuero_proc %>% 
  mutate(categoria = as.factor(categoria)) %>% 
  filter(Total > 0) %>% 
  ggplot(aes(x = reorder(organismo, Total), y = Total, colour = categoria)) +
  geom_bar(aes(fill = categoria), stat = "identity") +
  #scale_color_brewer(palette="Greys") +
  #scale_y_continuous(breaks=seq(0,2300,100), position = "top") +
  geom_text(aes(label = Total), size = 3, colour = "black") +
  ylab("Cantidad de Causas Iniciadas") + 
  xlab("Juzgados") +
  ggtitle("Causas Iniciadas") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  #geom_hline(yintercept=200, color = "red")
  coord_flip()
  
```

## Trámites Voluntarios 

```{r}
inic_fuero_tram %>% 
  mutate(categoria = as.factor(categoria)) %>% 
  filter(Total > 0) %>% 
  ggplot(aes(x = reorder(organismo, Total), y = Total, colour = categoria)) +
  geom_bar(aes(fill = categoria), stat = "identity") +
  #scale_color_brewer(palette="Greys") +
  #scale_y_continuous(breaks=seq(0,2300,100), position = "top") +
  geom_text(aes(label = Total), size = 3, colour = "black") +
  ylab("Cantidad de Trámites Voluntarios") + 
  xlab("Juzgados") +
  ggtitle("Trámites Voluntarios") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  #geom_hline(yintercept=200, color = "red")
  coord_flip()
  
```


