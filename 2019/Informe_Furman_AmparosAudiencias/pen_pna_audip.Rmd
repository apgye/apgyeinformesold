---
title: Audiencias Realizadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r}
existepres <- existepresentacion(jdo_pna, start_date, end_date, "AUDIPM")
```


## Audiencias Realizadas y sus Duraciones - `r getDataIntervalStr(start_date, end_date)`

```{r, eval = existepres}
audienciasPenales(poblacion = jdo_pna, 
             start_date = start_date, 
             end_date = end_date, 
             desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias Realizadas y Duración") %>%
  landscape()
```

```{r, eval= !existepres}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "No registra información presentada en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, bold = T, color = "red") %>%
  column_spec(2, width = "30em")
    
```

## Audiencias realizadas por Tipo según artículo CPP - `r getDataIntervalStr(start_date, end_date)`

```{r, eval = existepres }
realizadas_xtipo %>% 
  outputTable(caption = "Audiencias Realizadas por Tipo según artículo CPP") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```

```{r, eval= !existepres}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "No registra información presentada en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, color = "red") %>%
  column_spec(2, width = "30em")
    
```


