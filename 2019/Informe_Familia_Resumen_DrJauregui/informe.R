# Informe 
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tidyr)
library(tibble)
library(ggplot2)
library(RColorBrewer)
library(colorRamps)
library(lubridate)

source('iniciadas_v3.R')
source('iniciadas_penal.R')
source('entramite_v2.R')
source('resoluciones_v3.R')
source('resoluciones_penales_v2.R')
source('audienciasciviles.R')
source('audienciaslaborales.R')
source('audienciasfam.R')
source('audienciaspenales.R')
source('personal.R')
source('duracion.R')
source('archivadas.R')
source('movimientos.R')
source('pendientes.R')
source('revocaciones.R')
source('apge_tareas.R')
source('poblacion.R')


# Listado de Magistrados y Funcionarios
magistrados_y_secretarios <- DB_PROD() %>% apgyeDSL::apgyeTableData('personal_planta_ocupada') %>%
  left_join(DB_PROD() %>% apgyeDSL::apgyeTableData('personal_personas') %>%
              select(idagente, categoria, apellido, nombres), by=c("idagente")
  ) %>%
  filter(grepl("JUEZ|SECRETARIO|VOCAL|FISCAL|DEFENSOR", categoria)) %>%
  collect()
magist_func_id_agentes <- magistrados_y_secretarios$idagente
# add "0" ti IDs for External Agent
magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"



