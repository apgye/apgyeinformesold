# Conversor de códigos de variables
codconver <- function(df, operacion, variable) {
  
  if (operacion == "AUDIF" & variable == "ta") {
    df <- df %>% 
      mutate(ta = case_when(
        ta == "1" ~ "Control Legalidad Intern/Ext.",
        ta == "2" ~  "Escucha Niño/Niñas/Adol.",
        ta == "3" ~  "Escucha Personas c/Capac.Rest.",
        ta == "4" ~  "Escucha Denunciante V.Fam. o Género",
        ta == "5" ~  "Escucha Denunciado V.Fam. o Género",
        ta == "6" ~  "Control de Legalidad Med.Excep.",
        ta == "7" ~  "Adoptabilidad",
        ta == "8" ~  "Proceso de Vinculación",
        ta == "9" ~  "Guarda con Fines de Adopción",
        ta == "10" ~  "Adopción",
        ta == "11" ~  "Audiencia de Conciliación",
        ta == "12" ~  "Audiencia Art.70 Preliminar",
        ta == "13" ~  "Audiencias en otros proc.volunt.",
        ta == "14" ~  "Audiencia de Divorcio",
        ta == "15" ~  "Juicio Oral",
        ta == "16" ~  "Audiencia de Alimentos",
        ta == "17" ~  "Otras",
        ta == "20" ~ "Formulación/Imputación",
        ta == "21" ~ "Audiencia Conclusiva",
        ta == "22" ~ "Suspensión de Juicio a Prueba",
        ta == "23" ~ "Medidas",
        ta == "24" ~ "Remisión a Juicio",
        ta == "25" ~ "Juicio Abreviado",
        ta == "26" ~ "Remisión Judicial",
        ta == "27" ~ "Juicio Oral",
        ta == "28" ~ "Integración de Sentencia",
        ta == "29" ~ "Otras",
        TRUE ~ "sd"))
    
  } 
  else if (operacion == "AUDIF" & variable == "esta") {
    
    df <- df %>% 
      mutate(esta = case_when(esta == "1" ~ "fijada", 
                            esta == "2" ~ "realizada",
                            esta == "3" ~ "fracasada_incomp_parte",
                            esta == "4" ~ "fracasada_incomp_terceros",
                            esta == "5" ~ "cancelada",
                            esta == "6" ~ "no_realizada_xfalta_notific",
                            TRUE ~ "sd")) %>% 
      mutate(esta = factor(esta, 
                         levels = c("fijada", 
                                    "realizada",
                                    "fracasada_incomp_parte",
                                    "fracasada_incomp_terceros",
                                    "cancelada",
                                    "no_realizada_xfalta_notific",
                                    "sd"), ordered = TRUE))
      
    
  } 
  else if (operacion == "AUDIF" & variable == "ra") {
    df <- df %>% 
      mutate(ra = case_when(ra == "T" ~ "conciliacion_total",
                          ra == "P" ~ "conciliacion_parcial",
                          ra == "S" ~ "sin_conciliacion_y_sin_apelacion",
                          ra == "A" ~ "sin_conciliacion_con_apelacion",
                          ra == "C" ~ "conciliacion_en_proceso_sin_contradiccion",
                          TRUE ~ "sd")) %>% 
      mutate(ra = factor(ra, 
                         levels = c("conciliacion_total", 
                                    "conciliacion_parcial", 
                                    "sin_conciliacion_y_sin_apelacion",
                                    "sin_conciliacion_con_apelacion", 
                                    "conciliacion_en_proceso_sin_contradiccion",
                                    "sd"), ordered = TRUE))
    
  } 
  else if (operacion == "AUDIF" & variable == "mat") {
    
    df <- df %>% 
      mutate(mat = case_when(mat == "F" ~ "familia",
                           mat == "P" ~ "penal",
                           TRUE ~ "sd")) 
    
  } 
  else if (operacion == "AUDIL" & variable == "esta") {
    
    df <- df %>% 
      mutate(esta = case_when(esta == "1" ~ "fijada", 
                              esta == "2" ~ "realizada",
                              esta == "3" ~ "no_realizada",
                              esta == "4" ~ "cancelada",
                              esta == "5" ~ "realizada_no_concluida",
                              TRUE ~ "sd")) %>% 
      mutate(esta = factor(esta, levels = c("fijada", 
                                            "realizada",
                                            "no_realizada",
                                            "cancelada",
                                            "realizada_no_concluida",
                                            "sd")))
      
  } 
  else if (operacion == "AUDIL" & variable == "ta") {
  
    df <- df %>% 
      mutate(ta = case_when(ta == "P" ~ "conciliacion", 
                          ta == "V" ~ "vista_causa", 
                          ta == "O" ~ "otras", 
                          TRUE ~ "sd")) %>% 
      mutate(ta = factor(ta, levels = c("conciliacion", 
                                      "vista_causa", 
                                      "otras",
                                      "sd"), ordered = TRUE))
      
  } 
  else if (operacion == "AUDIL" & variable == "ra") {
  
    df <- df %>% 
      mutate(ra = case_when(
        ra == "T" ~ "Conciliacion_Total",
        ra == "P" ~ "Conciliacion_Parcial",
        ra == "S" ~ "Sin_Conciliacion", 
        TRUE ~ "sin_dato")) %>% 
      mutate(ra = factor(ra, levels = c("Conciliacion_Total", 
                                        "Conciliacion_Parcial", 
                                        "Sin_Conciliacion",
                                        "sin_dato"), ordered = TRUE))
  
  } 
  else if (operacion == "AUDIC" & variable == "ra") {
    
    df <- df %>% 
      mutate(ra = case_when(
        ra == "T" ~ "conciliacion_total",
        ra == "P" ~ "conciliacion_parcial",
        ra == "S" ~ "sin_conciliacion", 
        TRUE ~ "sin_dato")) %>% 
      mutate(ra = factor(ra, levels = c("conciliacion_total", 
                                        "conciliacion_parcial", 
                                        "sin_conciliacion",
                                        "sin_dato"), ordered = TRUE))
    
  } 
  else if (operacion == "AUDIC" & variable == "ta") {
    
    df <- df %>% 
      mutate(ta = case_when(ta == "1" ~ "preliminar", 
                            ta == "2" ~ "vista_causa", 
                            ta == "3" ~ "conciliación",
                            ta == "4" ~ "otras", 
                            TRUE ~ "sd")) %>% 
      mutate(ta = factor(ta, levels = c("preliminar", 
                                        "vista_causa",
                                        "conciliación",
                                        "otras",
                                        "sd"), ordered = TRUE))
    
  } 
  else if (operacion == "AUDIC" & variable == "esta") {
    
    df <- df %>% 
      mutate(esta = case_when(esta == "1" ~ "fijada", 
                              esta == "2" ~ "realizada",
                              esta == "3" ~ "no_realizada",
                              esta == "4" ~ "cancelada",
                              esta == "5" ~ "realizada_no_concluida",
                              TRUE ~ "sd")) %>% 
      mutate(esta = factor(esta, levels = c("fijada", 
                                            "realizada",
                                            "no_realizada",
                                            "cancelada",
                                            "realizada_no_concluida",
                                            "sd")))
  } 
  else if (operacion == "CIE2" & variable == "ibs") {
    
    df <- df %>% 
      mutate(ibs = case_when(ibs == "1" ~ "ingresa", 
                             ibs == "2" ~ "baja_a_1ainst",
                             ibs == "3" ~ "sube_ultima_instanc",
                              TRUE ~ "sid_dato")) %>% 
      mutate(ibs = factor(ibs, levels = c("ingresa", 
                                            "baja_a_1ainst",
                                            "sube_ultima_instanc",
                                            "sid_dato")))
  }
  else if (operacion == "IGEP" & variable == "tres") {
    
    df <- df %>% 
      mutate(tres = case_when(tres == 1 ~ "Libertad condicional",
                              tres == 2 ~ "Libertad asistida" ,
                              tres == 3 ~ "Salidas Socio-Familiares" ,
                              tres == 4 ~ "Salidas Socio-Laborales" ,
                              tres == 5 ~ "Salidas por estudio" ,
                              tres == 6 ~ "Salidas excepcionales" ,
                              tres == 7 ~ "Prisión domiciliaria" ,
                              tres == 8 ~ "Apelación Sanción discip ante Jdo Ejec" ,
                              tres == 9 ~ "Régimen de Semi-detención" ,
                              tres == 10 ~ "Sustitución de Penas" ,
                              tres == 11 ~ "Recurso de Hábeas Corpus" ,
                              tres == 12 ~ "Recurso de Amparo" ,
                              tres == 13 ~ "Recurso Apelación ante Sala Casación" ,
                              tres == 14 ~ "Medidas de Seguridad" ,
                              tres == 15 ~ "Intervención Equipo Técnico Internos por delitos c/integr" ,
                              tres == 16 ~ "Intervención Equipo Técnico Internos por violencia de género" ,
                              tres == 17 ~ "Otros",
                              TRUE ~ "sin_dato")) 
     
    
  }
  else if (operacion == "IGEP" & variable == "resul") {
    
    df <- df %>% 
      mutate(resul = case_when(resul == 1 ~ "Concesión" ,
                        resul == 2 ~ "Denegación" ,
                        resul == 3 ~ "Revocación" ,
                        resul == 4 ~ "Suspensión" ,
                        resul == 5 ~ "Anulación" ,
                        resul == 6 ~ "Dec.Abstracto" ,
                        resul == 7 ~ "Otras",
                        TRUE ~ "sin_dato"))
    
    
  }
  else if (operacion == "IGEP" & variable == "tsp") {
    
    df <- df %>% 
      mutate(tsp = case_when(tsp == 1 ~ "Trabajos Comunitarios" ,
                         tsp == 2 ~ "Internación en centros de Rehabilitación" ,
                         tsp == 3 ~ "Escolarización" ,
                         tsp == 4 ~ "Tratamiento Psicológico" ,
                         tsp == 5 ~ "Otras" ,
                         TRUE ~ "sin_dato"))
  }
  else if (operacion == "IGEP" & variable == "tterm") {
    
    df <- df %>% 
      mutate(tterm = case_when(tterm == 1 ~ "Archivado por cumplimiento condena" ,
                           tterm == 2 ~ "Archivado por cese medida de seguridad" ,
                           tterm == 3 ~ "Muerte" ,
                           tterm == 4 ~ "Prescripción de la Pena" ,
                           tterm == 5 ~ "Incompetencia" ,
                           tterm == 6 ~ "Otros" ,
                           TRUE ~ "sin_dato"))
  }
  
  else if (operacion == "AUDIP" & variable == "realizadas_articuloCPP") {
    
    df <- df %>% 
      mutate(realizadas_articuloCPP = case_when(
        realizadas_articuloCPP == "240" ~ "FORMULACION DE CARGOS-Art.240",
        realizadas_articuloCPP == "241" ~ "AUDIENCIA CONCLUSIVA-Art.241",
        realizadas_articuloCPP == "394" ~ "SUSP P.PRUEBA-76bisdelCP y 394CPP",
        realizadas_articuloCPP == "350" ~ "MEDIDAS DE COERCION-Art.349 y 350",
        realizadas_articuloCPP == "353" ~ "PRISION PREVENTIVA-Art.353",
        realizadas_articuloCPP == "391" ~ "JUICIO ABREVIADO-Art.391",
        realizadas_articuloCPP == "405" ~ "APERTURA O RECHAZO DEL JUICIO -Art.405",
        realizadas_articuloCPP == "24270" ~ "RESTABLEC.DE CONTACTO-Ley 24270",
        realizadas_articuloCPP == "395" ~ "SOLICITUD DE SOBRESEIMIENTO-Art.395",
        realizadas_articuloCPP == "470" ~ "AUDIENCIA DE CONCILIACION-Art.470",
        realizadas_articuloCPP == "471" ~ "AUDIENCIA PRELIMINAR-Art.471",
        realizadas_articuloCPP == "366" ~ "REVISION DE MEDIDAS DE COERCION-Art.366",
        realizadas_articuloCPP == "564" ~ "REVISION DE PROBATION-Art.564",
        realizadas_articuloCPP == "76" ~ "PROTEC-INHIBITORIA U ORDENAT-Art.76 CPP",
        realizadas_articuloCPP == "406" ~ "JUICIO ORAL Art.406",
        realizadas_articuloCPP == "507" ~ "APELACION Art. 507",
        realizadas_articuloCPP == "600" ~ "OTRAS",
        TRUE ~ "sd")) %>% 
      mutate(realizadas_articuloCPP = factor(realizadas_articuloCPP, 
                                             levels = 
                                               c("FORMULACION DE CARGOS-Art.240",
                                                 "AUDIENCIA CONCLUSIVA-Art.241",
                                                 "SUSP P.PRUEBA-76bisdelCP y 394CPP",
                                                 "MEDIDAS DE COERCION-Art.349 y 350",
                                                 "PRISION PREVENTIVA-Art.353",
                                                 "JUICIO ABREVIADO-Art.391",
                                                 "APERTURA O RECHAZO DEL JUICIO -Art.405",
                                                 "RESTABLEC.DE CONTACTO-Ley 24270",
                                                 "SOLICITUD DE SOBRESEIMIENTO-Art.395",
                                                 "AUDIENCIA DE CONCILIACION-Art.470",
                                                 "AUDIENCIA PRELIMINAR-Art.471",
                                                 "REVISION DE MEDIDAS DE COERCION-Art.366",
                                                 "REVISION DE PROBATION-Art.564",
                                                 "PROTEC-INHIBITORIA U ORDENAT-Art.76 CPP",
                                                 "JUICIO ORAL Art.406",
                                                 "APELACION Art. 507",
                                                 "OTRAS",
                                                 "sd"), ordered = TRUE))
    
  }
  
  else if (operacion == "IGMP" & variable == "estaud") {
    
    df <- df %>% 
      mutate(estaud = case_when(estaud == "0" ~ "Audiencia Fracasada x Inexistencia de Domicilio" ,
                               estaud == "1" ~ "Audiencia Realizada" ,
                               estaud == "2" ~ "Audiencia Fracasada x Incomp. Denunciante" ,
                               estaud == "3" ~ "Audiencia Fracasada x Incomp. Denunciado" ,
                               TRUE ~ "sin_dato"))
  }
  else if (operacion == "IGMP" & variable == "ra") {
    
    df <- df %>% 
      mutate(ra = case_when(ra == "0" ~ "Sin Acuerdo",
                            ra == "1" ~ "Con Acuerdo",
                            # ra == "2" ~ "No Aplica",
                            TRUE ~ "sin_dato"))
  }
  else if (operacion == "IGMP" & variable == "vmed") {
    
    df <- df %>% 
      mutate(vmed = case_when(vmed == "1" ~ "decisión de Parte/s respecto de No Mediar" ,
                            vmed == "2" ~ "decisión del Mediador respecto de No Mediar",
                            vmed == "3" ~ "decisión del Fiscal respecto de No Mediar",
                            vmed == "4" ~ "decisión de Parte/s solicitando  Archivo del Legajo",
                            TRUE ~ "sin_dato"))
  }
  else if (operacion == "IGMP" & variable == "tmed") {
    
    df <- df %>% 
      mutate(tmed = case_when(tmed == "0" ~ "Mediación Simple" ,
                              tmed == "1" ~ "Mediación Compleja",
                              TRUE ~ "sin_dato"))
  }
  else if (operacion == "IGMP" & variable == "descripcion") {
    
    df <- df %>% 
      mutate(tmov = case_when(str_detect(descripcion, "^11") ~ "Datos Libros",
                              str_detect(descripcion, "^21") ~ "Caratula de Legajo",
                              str_detect(descripcion, "^31") ~ "Audiencia Entrevista",
                              str_detect(descripcion, "^32") ~ "Gestión Telefónica",
                              str_detect(descripcion, "^41") ~ "Planilla de Aportes",
                              str_detect(descripcion, "^42") ~ "Planilla de Trabajo Comunitario",
                              str_detect(descripcion, "^43") ~ "Planilla Control/Seguimiento",
                              str_detect(descripcion, "^51") ~ "Nota Institución Trabajo Comunitario",
                              str_detect(descripcion, "^52") ~ "Nota Institución Aportes",
                              str_detect(descripcion, "^53") ~ "Nota Institución Curso o Escolaridad",
                              str_detect(descripcion, "^54") ~ "Nota Institución Tratamiento",
                              str_detect(descripcion, "^55") ~ "Nota Institución Protocolo",
                              str_detect(descripcion, "^56") ~ "Nota Institución Terminó",
                              str_detect(descripcion, "^57") ~ "Solicitando datos de la victima",
                              str_detect(descripcion, "^58") ~ "Constancia para presentar en el trabajo",
                              str_detect(descripcion, "^61") ~ "Audiencia de Comparendo",
                              str_detect(descripcion, "^71") ~ "Comunicando Rest/Prohib.Div.Antecedente",
                              str_detect(descripcion, "^72") ~ "Comunicando Rest/Prohib.Comisaria",
                              str_detect(descripcion, "^73") ~ "Remisión expediente Juzgado de Paz",
                              str_detect(descripcion, "^81") ~ "Citación",
                              str_detect(descripcion, "^91") ~ "Cumplimiento",
                              str_detect(descripcion, "^92") ~ "Incumplimiento",
                              str_detect(descripcion, "^93") ~ "Cumplimiento después de Incumplimient.",
                              str_detect(descripcion, "^94") ~ "Mail al Defensor p/ hacer comparecer",
                              str_detect(descripcion, "^95") ~ "Respuesta de Informe",
                              str_detect(descripcion, "^96") ~ "Solicitud de Informe",
                              str_detect(descripcion, "^97") ~ "Cambio de regla",
                              str_detect(descripcion, "^98") ~ "Al Director",
                              TRUE ~ "sin_dato"))
  } 
  else if (operacion == "CADRCAD" & variable == "tres") {
    
    df <- df %>% 
      mutate(tipo_resolucion = case_when(
        tres == "1" ~ "Rechazo",
        tres == "2" ~ "Hacer Lugar", 
        tres == "3" ~ "Hacer Lugar Parcialmente",
        tres == "4" ~ "Admisibilidad",
        tres == "5" ~ "Admisibilidad Parcial",
        tres == "6" ~ "Inadmisibilidad",
        tres == "7" ~ "Concede RIL",
        tres == "8" ~ "Deniega RIL",
        tres == "9" ~ "Abstracto",
        tres == "10" ~ "Concede Med.Cautelar", 
        tres == "11" ~ "Rechazo Med.Cautelar",
        tres == "12" ~ "Otros", TRUE ~ "Sin_dato")) %>%
      mutate(tipo_resolucion = 
               factor(tipo_resolucion, levels = c("Rechazo","Hacer Lugar", 
                                                  "Hacer Lugar Parcialmente","Admisibilidad","Admisibilidad Parcial",
                                                  "Inadmisibilidad","Concede RIL","Deniega RIL","Abstracto",
                                                  "Concede Med.Cautelar","Rechazo Med.Cautelar", "Otros",
                                                  "Sin_dato", "Total"), ordered = TRUE)) 
    
  } 
  else if (operacion == "CADRCAD" & variable == "odem") {
    
    df <- df %>% 
      mutate(odem = case_when(
        odem == "1" ~ "Empleo Público",
        odem == "2" ~ "Previsional", 
        odem == "3" ~ "Contrato",
        odem == "4" ~ "Tributario",
        odem == "5" ~ "Colegios Profesionales",
        odem == "6" ~ "Responsabilidad del Estado",
        odem == "7" ~ "Multas",
        odem == "8" ~ "Acción Colectiva",
        odem == "9" ~ "Servicio Público",
        odem == "10" ~ "Poder de Policía - Sanciones - Multas",
        odem == "11" ~ "Otros", TRUE ~ "Sin_dato")) 
    
  } 
  else if  (operacion == "CADRCAD" & variable == "tdemo") {
    
    df <- df %>% 
      mutate(tdemo = case_when(
        tdemo == "1" ~ "Estado Provincial  y Ente Autarquico",
        tdemo == "2" ~ "Municipalidad", 
        tdemo == "3" ~ "Particulares",
        tdemo == "4" ~ "Combinación de Anteriores",
        tdemo == "5" ~ "Otros", TRUE ~ "Sin_dato"))
    
    
  } 
  else if  (operacion == "CADRCAD" & variable == "rec") {
    
    df <- df %>% 
      mutate(rec = case_when(
        rec == "1" ~ "RIL - Concedido",
        rec == "2" ~ "RIL - Denegado", 
        rec == "3" ~ "Reposición Concedido",
        rec == "4" ~ "Reposición Denegado",
        rec == "5" ~ "Nulidad Concedido",
        rec == "6" ~ "Nulidad Denegado",
        rec == "7" ~ "Revisión Concedido",
        rec == "8" ~ "Revisión Denegado",
        rec == "9" ~ "Otros Concedidos",
        rec == "10" ~ "Otros Denegados",
         TRUE ~ "Sin_dato")) 
  
  }
  
    
  df
}

 




#########################################################################


# df de operaciones y variables
# operavar <- tibble::tribble(
#   ~operacion, ~variable,  ~activa, ~finicio, ~ffin, ~codigo, ~significado,
#   "AUDIF", "ta", 1, "2019-01-01", NA, "1", "Control de Legalidad Internaciones/Externaciones",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "2",  "Escucha Niño/Niñas/Adolescentes",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "3",  "Escucha Personas c/Capacidad Restringida",
#   "AUDIF", "ta", 0, "2019-01-01", "2018-10-22", "4",  "Escucha al Denunciante Violencia Familiar o de Género",
#   "AUDIF", "ta", 0, "2019-01-01", "2018-10-22", "5",  "Escucha al Denunciado",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "6",  "Control de Legalidad Medidas Excepcionales",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "7",  "Adoptabilidad",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "8",  "Proceso de Vinculación",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "9",  "Guarda con Fines de Adopción",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "10",  "Adopción",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "11",  "Audiencia de Conciliación",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "12",  "Audiencia Art.70 Preliminar(ej. Filiación)",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "13",  "Audiencias en otros procesos voluntarios",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "14",  "Audiencia de Divorcio",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "15",  "Juicio Oral",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "16",  "Audiencia de Alimentos",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "17",  "Otras",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "20", "Formulación/Imputación", 
#   "AUDIF", "ta", 1, "2019-01-01", NA, "21", "Audiencia Conclusiva",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "22", "Suspensión de Juicio a Prueba",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "23", "Medidas",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "24", "Remisión a Juicio",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "25", "Juicio Abreviado",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "26", "Remisión Judicial",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "27", "Juicio Oral",
#   "AUDIF", "ta", 1, "2019-01-01", NA, "28", "Integración de Sentencia", 
#   "AUDIF", "ta", 1, "2019-01-01", NA, "29",  "Otras"
# )


gtia_pna <- tibble::tribble(
  ~iep_pna, ~idagente,
  "jezpen0100pna", "237",
  "jezpen0100pna", "591",
  "jezpen0200pna", "57",
  "jezpen0300pna", "79",
  "jezpen0400pna", "910",
  "jezpen0500pna", "1033",
  "jezpen0600pna", "225"
)
