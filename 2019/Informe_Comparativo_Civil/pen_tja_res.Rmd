---
title: Resoluciones en Tribunal de Juicio y Apelación
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')

if (existepresentacion(oga, start_date, end_date, "RESOLPT") | 
    existepresentacion(oga, start_date, end_date, "RESOLPA")){
  resol_tja <- resoluciones_pen(poblacion = tja,
                                start_date = start_date,
                                end_date = end_date,
                                desagregacion_mensual = desagregacion_mensual)
} else {
  cat(msg_NoData)
}
```

## Resoluciones en Tribunal de Juicio - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar las resoluciones por integración.   

Se agrega al final de la tabla el dato de personas sobreseidas, condenadas y absueltos según el registro de justiciables efectuado en cada legajo.     

Finalmente, la fila total muestra la cantidad de resoluciones por circunscripción.      

```{r, tjui, echo = FALSE, message = FALSE, warning = FALSE}
if (length(resol_tja$tjui) > 0){
  resol_tja$tjui %>%  
      outputTable(caption = str_c("Resoluciones"," (", getDataIntervalStr(start_date, end_date), ")")) %>%
      row_spec(0, angle = 90) %>%
      column_spec(3, "12cm") %>%
      column_spec(10, border_right = T) %>%
      landscape()
} else {
  cat(msg_NoData)
}

```


## Resoluciones en Tribunal de Apelación - `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede consultar las resoluciones por integración.

```{r, apel, echo = FALSE, message = FALSE, warning = FALSE}
if (length(resol_tja$resultado_tapel) > 0){
  resol_tja$resultado_tapel %>%
    outputTable(caption = str_c("Resoluciones"," (", getDataIntervalStr(start_date, end_date), ")")) %>%
    row_spec(0, angle = 90) %>%
    landscape()
} else {
  cat(msg_NoData)
}

```

