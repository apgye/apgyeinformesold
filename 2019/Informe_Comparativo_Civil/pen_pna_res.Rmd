---
title: Resoluciones en Penal de Menores
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
existepres <- existepresentacion(jdo_pna, start_date, end_date, "CADRPM")
```


## Resoluciones en Penal de Menores `r getDataIntervalStr(start_date, end_date)`

```{r, eval = existepres}

if (desagregacion_mensual){
  
  resoluciones_pen(poblacion = jdo_pna,
              start_date = start_date,
              end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = str_c("Resoluciones"," (", getDataIntervalStr(start_date, end_date), ")")) %>%
  row_spec(0, angle = 90) %>% 
  column_spec(3, "2,5cm")
  # column_spec(10, border_right = T) %>% 
  # column_spec(24, border_right = T) %>% 
  # landscape()
  
} else {
  
  resoluciones_pen(poblacion = jdo_pna,
              start_date = start_date,
              end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = str_c("Resoluciones"," (", getDataIntervalStr(start_date, end_date), ")")) %>%
  row_spec(0, angle = 90) %>% 
  column_spec(2, "2,5cm") 
  # column_spec(9, border_right = T) %>% 
  # column_spec(23, border_right = T) %>% 
  # landscape()
  
}

```


```{r, eval= !existepres}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "No registra información presentada en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, color = "red") %>%
  column_spec(2, width = "30em")
    
```



