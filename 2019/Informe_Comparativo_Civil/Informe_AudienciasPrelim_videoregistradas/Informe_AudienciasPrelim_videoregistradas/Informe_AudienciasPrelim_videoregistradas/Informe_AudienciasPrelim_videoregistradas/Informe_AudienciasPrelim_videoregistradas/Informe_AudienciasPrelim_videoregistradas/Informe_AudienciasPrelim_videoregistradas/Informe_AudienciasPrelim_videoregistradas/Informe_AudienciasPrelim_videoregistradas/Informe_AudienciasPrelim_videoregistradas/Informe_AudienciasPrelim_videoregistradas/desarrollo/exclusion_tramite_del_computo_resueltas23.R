resultado2 <- DB_PROD() %>% 
  apgyeTableData(CADRA23) %>% 
  mutate(fres = dmy(fres)) %>% 
  mutate(fdesp = dmy(fdesp)) %>%
  mutate(fvenc1 = dmy(fvenc1)) %>%
  mutate(fvenc2 = dmy(fvenc2)) %>%
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% poblacion$organismo) %>% 
  filter(!grepl("CERTIFICACION|PROBATION|DILIGENCIAS|INFORMACION SUMARIA|SU ", tproc)) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% collect()

paz23tram <- apgyeOperationsJusER::Tipo_procesos %>%
  filter(materia == "paz23c") %>% 
  select(tipo_de_procesos, Tipo_tramites_turnovoluntario) %>% 
  filter(Tipo_tramites_turnovoluntario == 1)
  
paz23tram$tipo_de_procesos

resultado <- resultado %>% 
  mutate(tproc = toupper(tproc)) %>% 
  mutate(tproc = str_remove_all(tproc, "[[:punct:]]")) %>% 
  mutate(tproc = str_trim(tproc)) %>% 
  mutate(tproc = ifelse(tproc == "EJECUTIVO", "EJECUTIVOS", tproc)) %>% 
  mutate(proceso = !(tproc %in% paz23tram$tipo_de_procesos)) %>% 
  select(nro, caratula, tproc, proceso, everything())

"CERTIFICACION|PROBATION|DILIGENCIAS|INFORMACION SUMARIA|"





