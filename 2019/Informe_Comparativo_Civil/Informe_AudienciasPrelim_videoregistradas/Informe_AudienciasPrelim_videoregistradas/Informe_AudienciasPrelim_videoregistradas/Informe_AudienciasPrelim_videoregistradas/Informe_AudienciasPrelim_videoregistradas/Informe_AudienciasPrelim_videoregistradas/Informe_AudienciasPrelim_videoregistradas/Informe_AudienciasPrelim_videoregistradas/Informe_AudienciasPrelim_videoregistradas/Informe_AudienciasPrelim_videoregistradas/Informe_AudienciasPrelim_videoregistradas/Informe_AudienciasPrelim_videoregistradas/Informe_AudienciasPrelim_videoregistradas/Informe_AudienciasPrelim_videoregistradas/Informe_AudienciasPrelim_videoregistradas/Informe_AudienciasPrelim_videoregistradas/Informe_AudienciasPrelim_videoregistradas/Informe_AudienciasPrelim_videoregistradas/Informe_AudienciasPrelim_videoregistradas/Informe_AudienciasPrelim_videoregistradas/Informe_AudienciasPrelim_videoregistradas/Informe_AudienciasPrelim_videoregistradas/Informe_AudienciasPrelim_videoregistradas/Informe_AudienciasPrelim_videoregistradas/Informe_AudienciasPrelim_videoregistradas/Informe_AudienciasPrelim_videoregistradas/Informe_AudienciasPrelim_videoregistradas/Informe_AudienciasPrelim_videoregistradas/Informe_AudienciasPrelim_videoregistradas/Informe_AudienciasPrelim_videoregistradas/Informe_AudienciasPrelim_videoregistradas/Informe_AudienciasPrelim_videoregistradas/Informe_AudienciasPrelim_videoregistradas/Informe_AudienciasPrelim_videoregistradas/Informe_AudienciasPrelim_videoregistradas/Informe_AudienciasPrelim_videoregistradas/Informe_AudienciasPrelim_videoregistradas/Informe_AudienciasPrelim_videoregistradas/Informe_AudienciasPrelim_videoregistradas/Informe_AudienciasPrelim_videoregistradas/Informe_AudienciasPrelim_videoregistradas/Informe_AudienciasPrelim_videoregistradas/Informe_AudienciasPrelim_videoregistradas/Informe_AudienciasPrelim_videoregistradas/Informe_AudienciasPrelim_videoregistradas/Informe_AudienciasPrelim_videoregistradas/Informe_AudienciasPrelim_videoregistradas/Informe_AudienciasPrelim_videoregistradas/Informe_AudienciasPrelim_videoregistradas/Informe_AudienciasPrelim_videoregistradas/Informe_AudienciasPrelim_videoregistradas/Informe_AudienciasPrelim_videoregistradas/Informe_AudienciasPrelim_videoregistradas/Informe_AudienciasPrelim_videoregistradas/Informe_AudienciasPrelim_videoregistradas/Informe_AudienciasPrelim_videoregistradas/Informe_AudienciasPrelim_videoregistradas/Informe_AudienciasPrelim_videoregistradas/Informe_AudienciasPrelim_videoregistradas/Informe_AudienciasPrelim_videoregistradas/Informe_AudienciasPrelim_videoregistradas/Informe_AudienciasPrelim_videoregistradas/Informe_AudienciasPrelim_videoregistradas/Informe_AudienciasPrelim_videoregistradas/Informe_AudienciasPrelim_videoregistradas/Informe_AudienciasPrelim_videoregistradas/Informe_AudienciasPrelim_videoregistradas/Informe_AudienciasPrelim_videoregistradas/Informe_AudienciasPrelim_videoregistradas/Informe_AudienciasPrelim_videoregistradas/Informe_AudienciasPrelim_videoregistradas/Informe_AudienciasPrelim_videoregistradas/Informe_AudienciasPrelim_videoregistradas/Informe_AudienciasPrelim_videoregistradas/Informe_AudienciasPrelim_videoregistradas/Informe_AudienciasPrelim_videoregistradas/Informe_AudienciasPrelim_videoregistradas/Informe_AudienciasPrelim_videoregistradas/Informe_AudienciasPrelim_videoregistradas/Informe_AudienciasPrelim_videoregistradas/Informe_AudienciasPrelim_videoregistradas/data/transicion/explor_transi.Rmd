---
title: "Informe Juzgados de Transicion"
author: "Área de Planificación Gestión y Estadística"
date: "2/8/2019"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
setwd("~/apgyeinformes/2019/Modelo_Parametrizado/data/transicion")
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tidyr)
library(tibble)
library(ggplot2)
library(RColorBrewer)
library(colorRamps)
library(lubridate)
source('explor_transi.R')

```

# Etapa Diagnóstico (b): Informe Exploratorio

En este informe exploratorio se ordena la información recibida según las fuentes de datos y clasificaciones existentes en los sistemas de ambos organismos.   

Los listados empleados para el informe son:     

+ **Juzgdo de Transición 1:** `r files` (listados ametrizados) 
+ **Juzgdo de Transición 2:** `r files2` (modelos parametrizados)

No se dispone de datos desagregados del Juzgdo de Transición Nº1 conforme modelo oportunamente propuesto en el Plan de Relevamiento (punto a).
   
# Resumen de Información Presentada

```{r}
instruccion1_resumen %>% 
  kable(caption = "Juzgado Transición Nº1 Legajos Totales", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  
```

```{r}
instruccion2_tramite_resumen %>% 
  janitor::adorn_totals("row") %>% 
  kable(caption = "Juzgado Transición Nº2 Legajos en Trámite", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) 
  
```

Los datos de Causas en Trámite del Juzgado de Transición Nº2 aparecen desagregados en las próximas tablas.    
\pagebreak

# Información Desagregada

## Transición 2: Exploración de Causas Activas

La siguiente tabla muestra los legajos que no tinene movimientos registrados en Lex-Dotor. Pese a ello, como algunos sin tienen asientos "en letra" cerrados, agregamos el año del asiento para tratar de indentificar si el legajo está activo. 

```{r}
instruccion2_tramite_sinfechamov %>% 
  janitor::adorn_totals("row") %>% 
  kable(caption = "Legajos en trámite sin movimientos registrados", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) 
  
```


```{r}
instruccion2_tramite_confechamov %>% 
  janitor::adorn_totals("row") %>% 
  kable(caption = "Legajos en trámite con movimientos registrados", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) 
  
```


## Transición 2: Datos Generales Presentados

```{r}
instruccion2_resumen %>% 
  kable(caption = "Cantidad de Legajos según Estado", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) 
  
```

## Transición 2: Resumen de Legajos en Trámite y su estado según registro de "letra"

```{r}
instruccion2_tramite_xletra %>% 
  kable(caption = "Cantidad de Legajos según Estado en Letra", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) 
  
```
