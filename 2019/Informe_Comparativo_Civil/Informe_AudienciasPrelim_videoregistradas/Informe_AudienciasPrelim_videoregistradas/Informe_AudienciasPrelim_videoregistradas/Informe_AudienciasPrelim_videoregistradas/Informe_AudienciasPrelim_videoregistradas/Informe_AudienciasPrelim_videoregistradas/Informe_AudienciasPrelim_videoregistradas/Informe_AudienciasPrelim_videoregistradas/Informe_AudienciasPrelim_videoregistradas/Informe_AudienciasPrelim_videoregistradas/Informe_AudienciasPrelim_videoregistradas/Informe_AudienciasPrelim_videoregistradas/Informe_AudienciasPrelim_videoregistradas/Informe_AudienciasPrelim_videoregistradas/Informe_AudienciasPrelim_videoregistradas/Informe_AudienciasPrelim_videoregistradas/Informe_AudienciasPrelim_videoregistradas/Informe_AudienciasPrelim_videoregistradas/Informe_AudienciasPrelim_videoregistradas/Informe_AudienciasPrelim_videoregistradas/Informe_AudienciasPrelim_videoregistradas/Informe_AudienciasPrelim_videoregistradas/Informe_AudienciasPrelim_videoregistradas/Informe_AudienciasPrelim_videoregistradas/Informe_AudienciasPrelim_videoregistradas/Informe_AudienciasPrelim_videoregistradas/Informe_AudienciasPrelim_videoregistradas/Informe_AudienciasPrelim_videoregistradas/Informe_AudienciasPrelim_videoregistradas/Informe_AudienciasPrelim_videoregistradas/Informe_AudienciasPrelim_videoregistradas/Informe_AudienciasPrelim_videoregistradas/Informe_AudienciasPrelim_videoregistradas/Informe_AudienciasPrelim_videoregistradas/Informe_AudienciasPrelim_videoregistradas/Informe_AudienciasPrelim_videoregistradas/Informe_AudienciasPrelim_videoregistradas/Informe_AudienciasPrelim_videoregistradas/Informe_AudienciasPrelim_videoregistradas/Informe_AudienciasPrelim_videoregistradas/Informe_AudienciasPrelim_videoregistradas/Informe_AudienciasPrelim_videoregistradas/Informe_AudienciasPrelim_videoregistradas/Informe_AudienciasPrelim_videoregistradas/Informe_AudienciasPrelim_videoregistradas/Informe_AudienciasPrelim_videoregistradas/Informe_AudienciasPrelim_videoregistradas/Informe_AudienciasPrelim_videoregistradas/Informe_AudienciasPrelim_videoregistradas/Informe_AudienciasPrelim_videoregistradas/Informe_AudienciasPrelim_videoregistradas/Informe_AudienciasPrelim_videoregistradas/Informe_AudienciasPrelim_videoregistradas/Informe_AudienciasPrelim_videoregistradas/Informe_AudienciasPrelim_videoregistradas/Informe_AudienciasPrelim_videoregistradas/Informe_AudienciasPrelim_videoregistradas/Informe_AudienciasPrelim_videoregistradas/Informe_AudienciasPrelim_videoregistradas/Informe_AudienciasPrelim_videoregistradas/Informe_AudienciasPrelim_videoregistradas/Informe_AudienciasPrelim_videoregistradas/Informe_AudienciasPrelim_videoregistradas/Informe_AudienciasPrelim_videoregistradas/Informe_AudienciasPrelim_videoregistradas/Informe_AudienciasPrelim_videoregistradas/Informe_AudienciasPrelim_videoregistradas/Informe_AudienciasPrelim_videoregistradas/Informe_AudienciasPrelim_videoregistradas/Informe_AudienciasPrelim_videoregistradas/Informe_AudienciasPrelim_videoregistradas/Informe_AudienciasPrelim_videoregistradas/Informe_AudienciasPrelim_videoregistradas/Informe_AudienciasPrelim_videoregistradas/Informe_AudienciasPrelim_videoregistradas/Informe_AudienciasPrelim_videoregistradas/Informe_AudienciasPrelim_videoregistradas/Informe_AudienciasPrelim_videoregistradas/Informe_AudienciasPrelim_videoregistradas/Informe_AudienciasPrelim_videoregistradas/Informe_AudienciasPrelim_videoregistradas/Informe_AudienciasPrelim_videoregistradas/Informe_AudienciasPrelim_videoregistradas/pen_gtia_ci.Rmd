---
title: Casos Iniciados 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
resultado <- iniciadas_pen(poblacion = oga,
              start_date = start_date,
              end_date = end_date)
```

## Casos Iniciados - `r getDataIntervalStr(start_date, end_date)`

`r if (ci & t) '### Agrupados por Organismo'`

```{r, pen_gtia_ci_t, eval = (ci & t)}
resultado$inic_xcirc %>% 
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  landscape()
  
```


`r if (ci & td) '### Agrupados por Organismo y Tipo de Proceso'`

```{r, pen_gtia_ci_pc_intro, eval= (ci & td),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("En esta tabla usted puede ver las causas o legajos iniciados en Garantía según el informe suministrado por cada organismo.Las causas se agrupan por tipo de proceso, agregándose el total de casos por cada tipo.Además, se presenta la duración promedio entre el hecho que origina el legajo y el inicio del mismo en OGA. Para los tipo de proceso con un solo caso iniciado el dato de duración no constituye un promedio, sino su valor inicial. En los legajos donde no está registrada la fecha del hecho no se dispone de esta medición. **OBSERVACION:** Data los distintos criterios de registración sobre los tipos de proceso/delito en las distitnas circunscripciones se proponen las siguientes pautas prácticas de registración para homogeneizar este dato tan importante: *DENUNCIA/SU DENUNCIA*: evitar el uso de este descriptor. Emplear solo provisoriamente hasta tanto se disponga de un tipo de proceso conforme CPP. Registrar todos los delitos del legajo. En el campo principal de *Tipo Proceso* (caratula Lex-Doctor) registrar el delito de mayor pena y en el campo complementario *Otros tipos de proceso* registrar todos los demás delitos. ")
```

```{r, pen_gtia_ci_pc_xtproc, eval = (ci & td)}
resultado$inic_xtproc %>%
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape()
  
```