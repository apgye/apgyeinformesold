---
title: Causas Iniciadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
params:
   poblacion: "jdo*"
   start_date: "2018-02-01"
   end_date: "2018-11-01"
   desagregacion_mensual: FALSE
   separar_pconocimiento: TRUE
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r}
  if(!exists("poblacion")) {
    organismoEXPR <- utils::glob2rx(params$poblacion)
    poblacion <- apgyeJusEROrganization::listar_organismos() %>% filter(grepl(organismoEXPR, organismo))
  }
  if(!exists("start_date")) {
    start_date <- params$start_date
  }
  if(!exists("end_date")) {
    end_date <- params$end_date
  }
  if(!exists("desagregacion_mensual")) {
    desagregacion_mensual <- params$desagregacion_mensual
  }
  if(!exists("separar_pconocimiento")) {
    separar_pconocimiento <- params$separar_pconocimiento
  }

```


### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}

# armar condicional que aplique a civil, laboral, paz, familia


iniciados_cco(poblacion = poblacion, 
              start_date = start_date,  
              end_date = end_date) %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>% 
  column_spec(2, "5cm") %>% column_spec(4, "10cm") %>% 
  row_spec(0) %>% 
  landscape()

```


<!-- ```{r results='asis'} -->
<!-- if(separar_pconocimiento) { -->
<!--   cat("#### Procesos de conocimiento") -->
<!-- } else { -->
<!--   cat("#### Causas Iniciadas") -->
<!-- } -->
<!-- ``` -->

<!-- ```{r} -->
<!-- if(separar_pconocimiento) { -->
<!--   iniciadosCCO %>% -->
<!--     top_n_iniciados(proceso_conocimiento = T, desagregacion_mensual = desagregacion_mensual) %>% -->
<!--     outputTable(caption = "Iniciadas Procesos de Conocimiento") %>% -->
<!--     row_spec(0, angle = 90) -->
<!-- } else { -->
<!--   iniciadosCCO %>% -->
<!--     top_n_iniciados(desagregacion_mensual = desagregacion_mensual) %>% -->
<!--     outputTable(caption = "Iniciadas por tipo de proceso") %>% -->
<!--     row_spec(0, angle = 90) -->
<!-- } -->
<!-- ``` -->


<!-- ```{r results='asis'} -->
<!-- if(separar_pconocimiento) { -->
<!--   cat("#### Procesos No de conocimiento") -->
<!-- } -->
<!-- ``` -->
<!-- ```{r} -->
<!-- if(separar_pconocimiento) { -->
<!--   iniciadosCCO %>% -->
<!--     top_n_iniciados(proceso_conocimiento = F, desagregacion_mensual = desagregacion_mensual) %>% -->
<!--     outputTable(caption = "Iniciadas No Procesos de Conocimiento") %>% -->
<!--     row_spec(0, angle = 90) -->
<!-- } -->
<!-- ``` -->


