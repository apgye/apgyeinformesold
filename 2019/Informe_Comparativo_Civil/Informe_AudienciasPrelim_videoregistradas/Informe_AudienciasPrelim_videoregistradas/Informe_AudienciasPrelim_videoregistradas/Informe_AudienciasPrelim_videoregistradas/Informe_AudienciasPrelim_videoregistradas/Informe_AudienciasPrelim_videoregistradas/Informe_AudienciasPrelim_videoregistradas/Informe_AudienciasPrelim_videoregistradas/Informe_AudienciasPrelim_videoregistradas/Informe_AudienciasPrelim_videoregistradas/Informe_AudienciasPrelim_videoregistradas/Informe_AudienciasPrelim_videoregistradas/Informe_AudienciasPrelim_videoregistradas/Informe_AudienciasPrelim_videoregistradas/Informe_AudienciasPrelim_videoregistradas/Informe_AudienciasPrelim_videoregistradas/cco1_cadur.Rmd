---
title: Duracion de Procesos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
# https://www.kaggle.com/studentar/data-cleaning-challenge-outliers-r
# Validación de datos: valores extremos y anomalías
# Regla1: se eliminan las duracones negativas
# Regla2: más de tres desviaciones estándar es un outlier
# Regla3: se eliminan NA
# http://r-statistics.co/Loess-Regression-With-R.html

jdos_cco_solo <- jdos_cco %>% 
  filter(!str_detect(materia, "eje|cqb"))

duraciones <- duracion_prim(jdos_cco_solo, "CADR1C",  start_date = "2017-10-01", end_date = end_date)

analisi_durac <- duracion_A(jdos_cco_solo, duraciones)

```


## Duracion de Procesos 

La duración de los procesos es una medición importante de la actividad judicial, con un alto impacto en la valoración social sobre el servicio de justicia. Por ello presentamos a continuación los primeros datos sistemáticos sobre este indicador relevados desde el inicio del Nuevo Sistema Estadístico del STJER (octubre 2017) a la fecha.      

Para una valoración compleja de este indicador remitimos a las observaciones metodológicas de este apartado, y enunciamos aquí aspectos sobresalientes de la misma:    

+ los procesos incluidos en este análisis son procesos resueltos por Juzgados Civiles y Comerciales (excluyendo a órganos especializados -ej.Juzgados de Ejecución-),     
+ los casos estudiados son los *procesos de conocimiento* excluyendo otros grupos (que serán objeto de tratamiento particular),              
+ empleamos la mediana estadística para evitar la distorsión asociada a valores extremos (exclusión que hacemos constar en una tabla en la sección metdologócia), y              
+ las duraciones se miden en días corridos desde el inicio de la causa registrada en los sistemas de gestión de causas de los órganos judiciales hasta la fecha de sentencia definitiva.     

Los diversos interrogantes que pueden surgir de este análisis son de gran importancia para el Área de Planificación, Gestión y Estadística por lo que esperamos sus comentarios. Así también, ponemos a disposición todo el material técnico y datos empleado en nuestro análisis a fin de contribuir a la reproducibilidad de nuestros hallazgos.    

\pagebreak

## Duracion General de Procesos de Conocimiento: Variación Interanual     

Gráfico que muestra la duración general de procesos de conocimiento en los organismos del fuero. Dicha duración se expresa en la mediana estadística obtenida de las duraciones particulares de los casos comprendidos en cada año.      

```{r}
analisi_durac$g_general
```

\pagebreak

## Duracion General de Procesos de Conocimiento según Tipos Específicos      

### Variación en los últimos dos años

Gráfico que muestra los procesos en donde disminuyeron (azul) o aumentaron (rojo) las duraciones de proceso, y la cantidad de días de diferencia.         

```{r}
analisi_durac$g_duraciones_variacion
```

\pagebreak

### Tabla de Duraciones anuales y variación interanual últimos dos años    

Tabla que muestra las medianas de duraciones de procesos por año para cada tipo de proceso. 

```{r}
analisi_durac$duracion_ts_tabla_xaño %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  
```

\pagebreak

## Grafico de Duraciones por Tipos de Proceso y Años

```{r}
analisi_durac$g_duraxtipoxaño
```

\pagebreak

## Evolución de Duraciones según Tipos de Procesos

En esta serie temporal usted puede ver la evolución de las duraciones de procesos (medianas) en el período considerado.    

```{r}
analisi_durac$gts_duracxproc
```

\pagebreak

## Evolución de Duraciones por Tipo de Proceso

En esta serie temporal usted puede ver la evolución de las duraciones de procesos (medianas) en el período considerado.    

```{r}
analisi_durac$gts_duracxproc_facet
```

\pagebreak

## Observaciones metodológicas

Se pone a disposición todo el material de procesamiento estadístico diseñada en R-Statistcal Computing.   

+ Los tipos de procesos seleccionados son los 10 tipos con mayor cantidad de casos relevados en la muestras de causas con sentencia entre las fechas del relevamiento.
+ Los casos incluidos en la tabla comprenden a todos los procesos resueltos por cada organismo en el período considerado, con exclusión de los siguientes procesos: Incidentales, Cautelares, Sucesorios, Procesos Constitucionales, Beneficios de Litigar Sin Gastos, Interdictos, Homologaciones, Preparación de Vías, Segundo Testimonio, Oficios y Exhortos, y tipos no acordes a la legislación vigente (eg.CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO, PERSONAL).    
+ Se practicaron reimputación de tipos de procesos por errores de registración de los organismos (e.g. el *ORDINARIO CIVIL* se reimputó como *ORDINARIO*). Estas reimputaciones están a disposición en la rutina de procesamiento estadístico.      
+ Se excluyeron valores extremos en cada tipo de proceso mayores o menores a 3 desviaciones estándar. Estos valores extremos pueden estar asociados a errores de registración. No obstante ello atento el carácter crítico de estos casos y, eventualmente, la necesidad de un análisis detenido de los mismos agregamos la tabla de valores extremos más abajo.     
+ La duración de los procesos resueltos por cada organismo a través del dictado de sentencia definitiva se calcula como la diferencia en días corridos entre la fecha de inicio de la causa y la fecha del dictado de la sentencia según declaración del organismo.    
+ Para las líneas de tendencia empleamos la Regresión Local para vincular duraciones y el contexto interno y externo.    
+ Finalmente, a fin de evitar datos sesgados por la presencia de valores extremos se optó por la mediana estadística para este análisis y se excluyeron procesos por organismo con menos de dos casos resueltos.    

### Casos con valores extremos o anómalos

En esta tabla usted puede encontrar ordenado por organismo los casos con valores extremos o anómalos identificados en el estudio. Se entiende por *anomalía* en este contexto a todo valor de duración mayor o menor a 3 desviaciones estandar.     

```{r}
analisi_durac$duracion_outliers %>% 
  kable(caption = "Casos Extremos o Anómalos", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  column_spec(5, "5cm") %>% 
  landscape()
  
```

### Base Completa de Casos Considerados

```{r}
analisi_durac$duraciones_tidy_subselec %>% 
  group_by(circunscripcion, organismo, tipo_proceso) %>% 
  summarise(cantidad = n()) %>% 
  tidyr::spread(tipo_proceso, cantidad, fill = 0) %>%
  janitor::adorn_totals("row") %>% 
  janitor::adorn_totals("col") %>% 
  kable(caption = "Base de casos considerados por Proceso y Organismo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```


