
iep_op <- apgyeJusEROrganization::listar_operaciones_por_organismo()
iep <- apgyeJusEROrganization::listar_organismos()

iep %>% View()
iep_op %>% View()


start_date = "2019-05-01"
end_date = "2019-10-01"
poblacion <- jdos_cco %>% filter(str_detect(organismo, "pna"))
operacion = "CADR1C"


cadesp <- DB_PROD() %>% apgyeTableData(!! operacion) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) %>%
  mutate(fdesp = dmy(fdesp)) %>%
  filter(fdesp >= data_interval_start, fdesp < data_interval_end) %>% 
  mutate(mes = date_part('month', fdesp)) %>% #collect() %>% View()
  group_by(iep, mes) %>%
  summarise(causas_adespacho = n()) %>%
  collect() %>%
  filter(mes >= lubridate::month(start_date),
         mes <= lubridate::month(as.Date(end_date) - 1)) %>%
  mutate(mes = lubridate::month(mes, label = T, abbr = F))



# Solución
cadesp <- DB_PROD() %>% apgyeTableData(!! operacion) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) %>%
  mutate(fdesp = dmy(fdesp)) %>% 
  filter(fdesp >= data_interval_start, fdesp < data_interval_end) %>% 
  mutate(mes = date_part('month', fdesp)) %>% mutate(año = date_part('year', fdesp)) %>%
  mutate(fecha = paste0(año, "-", mes, "-", "01")) %>% 
  group_by(iep, mes, fecha) %>%
  summarise(causas_adespacho = n()) %>%
  collect() %>% ungroup() %>% 
  filter(mes >= lubridate::month(start_date),
         mes <= lubridate::month(as.Date(end_date) - 1)) %>%
  mutate(mes = lubridate::month(mes, label = T, abbr = F)) %>% 
  mutate(fecha = ymd(fecha)) %>% 
  mutate(fecha = ceiling_date(fecha, "month") - days(1))

# Prueba------------------------------------------------------
cadesp_prim <- DB_PROD() %>% apgyeTableData(!! operacion) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) %>%
  mutate(fdesp = dmy(fdesp)) %>% 
  #filter(fdesp >= data_interval_start, fdesp < data_interval_end) %>% 
  mutate(mes = date_part('month', fdesp)) %>% mutate(año = date_part('year', fdesp)) %>%
  mutate(fecha = paste0(año, "-", mes, "-", "01")) %>% collect() 


cadesp_old <- DB_PROD() %>% apgyeTableData(!! operacion) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) %>%
  mutate(fdesp = dmy(fdesp)) %>%
  mutate(mes = date_part('month', fdesp)) %>%
  group_by(iep, mes) %>%
  summarise(causas_adespacho = n()) %>%
  collect() %>%
  filter(mes >= lubridate::month(start_date),
         mes <= lubridate::month(as.Date(end_date) - 1)) %>%
  mutate(mes = lubridate::month(mes, label = T, abbr = F))




