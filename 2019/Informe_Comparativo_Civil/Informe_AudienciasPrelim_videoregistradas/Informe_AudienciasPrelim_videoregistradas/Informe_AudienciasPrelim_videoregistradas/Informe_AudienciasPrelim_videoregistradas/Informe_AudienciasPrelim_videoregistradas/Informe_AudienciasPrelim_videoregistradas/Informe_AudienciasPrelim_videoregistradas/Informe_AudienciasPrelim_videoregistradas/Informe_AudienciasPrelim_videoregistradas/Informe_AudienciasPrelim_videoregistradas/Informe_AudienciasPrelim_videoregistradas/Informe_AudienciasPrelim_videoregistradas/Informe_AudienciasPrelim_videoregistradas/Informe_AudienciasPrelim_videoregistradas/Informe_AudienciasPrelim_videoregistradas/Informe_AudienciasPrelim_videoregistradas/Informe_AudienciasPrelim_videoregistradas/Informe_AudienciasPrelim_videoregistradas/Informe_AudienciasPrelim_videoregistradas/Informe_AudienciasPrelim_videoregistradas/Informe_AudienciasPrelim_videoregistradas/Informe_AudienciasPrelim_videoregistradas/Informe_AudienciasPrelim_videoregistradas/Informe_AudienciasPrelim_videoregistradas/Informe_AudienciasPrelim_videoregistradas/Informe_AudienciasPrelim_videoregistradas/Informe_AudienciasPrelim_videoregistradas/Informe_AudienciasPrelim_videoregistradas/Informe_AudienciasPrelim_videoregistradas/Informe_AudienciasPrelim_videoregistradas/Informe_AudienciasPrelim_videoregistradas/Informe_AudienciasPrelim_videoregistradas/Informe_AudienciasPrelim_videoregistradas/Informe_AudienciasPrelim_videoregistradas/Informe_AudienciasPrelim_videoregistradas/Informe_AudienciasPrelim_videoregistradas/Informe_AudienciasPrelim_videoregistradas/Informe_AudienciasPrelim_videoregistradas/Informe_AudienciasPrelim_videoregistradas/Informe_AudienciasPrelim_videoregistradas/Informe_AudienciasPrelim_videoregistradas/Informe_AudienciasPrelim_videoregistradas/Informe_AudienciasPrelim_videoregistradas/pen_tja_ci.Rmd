---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r, echo = FALSE, warning=FALSE, message=FALSE}
resultado <- iniciadas_pen(poblacion = tja,
              start_date = start_date,
              end_date = end_date)
```

## Causas Iniciadas `r getDataIntervalStr(start_date, end_date)`

`r if (ci & t) '### Agrupados por Órgano de Juicio'`

```{r, pen_tja_ci_jui_t, eval = t}
resultado$inic_tjui_circ %>% 
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Total Causas Iniciadas en Juicio"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
  
```

`r if (ci & t) '### Agrupados por Órgano de Apelación'`

```{r, pen_tja_ci_ap_t, eval = t}
resultado$inic_ap_circ %>% 
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Total Causas Iniciadas en Apelación"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
  
```

\pagebreak 

`r if (ci & td) '### Agrupados por Organismo y Tipo de Proceso en Juicio-'`

```{r, pen_tja_ci_jui_intro, eval= (ci & td),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("En esta tabla usted puede ver las causas o legajos iniciados en el Tribunal de Juicio según el informe suministrado por OGA. Las causas se agrupan por tipo de proceso, agregándose el total de casos por cada tipo.")
```


```{r, pen_tja_ci_jui_td , eval= (ci & td)}
resultado$inic_tjui_tproc %>% 
  kable(caption = str_c("Causas Iniciadas agrupadas por tipo de proceso/delito"," (", getDataIntervalStr(start_date, end_date), ")"),
        align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  column_spec(2, "5cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
  
```


`r if (ci & td) '### Agrupados por Organismo y Tipo de Proceso en Apelación-'`


```{r, pen_tja_ci_ap_intro, eval= (ci & td),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("En esta tabla usted puede ver las causas o legajos iniciados en el Tribunal de Juicio según el informe suministrado por OGA. Las causas se agrupan por tipo de proceso, agregándose el total de casos por cada tipo.")
```


```{r, pen_tja_ci_ap_td , eval= (ci & td)}
resultado$inic_ap_tproc %>% 
  kable(caption = str_c("Causas Iniciadas agrupadas por tipo de proceso/delito"," (", getDataIntervalStr(start_date, end_date), ")"),
        align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
  
```