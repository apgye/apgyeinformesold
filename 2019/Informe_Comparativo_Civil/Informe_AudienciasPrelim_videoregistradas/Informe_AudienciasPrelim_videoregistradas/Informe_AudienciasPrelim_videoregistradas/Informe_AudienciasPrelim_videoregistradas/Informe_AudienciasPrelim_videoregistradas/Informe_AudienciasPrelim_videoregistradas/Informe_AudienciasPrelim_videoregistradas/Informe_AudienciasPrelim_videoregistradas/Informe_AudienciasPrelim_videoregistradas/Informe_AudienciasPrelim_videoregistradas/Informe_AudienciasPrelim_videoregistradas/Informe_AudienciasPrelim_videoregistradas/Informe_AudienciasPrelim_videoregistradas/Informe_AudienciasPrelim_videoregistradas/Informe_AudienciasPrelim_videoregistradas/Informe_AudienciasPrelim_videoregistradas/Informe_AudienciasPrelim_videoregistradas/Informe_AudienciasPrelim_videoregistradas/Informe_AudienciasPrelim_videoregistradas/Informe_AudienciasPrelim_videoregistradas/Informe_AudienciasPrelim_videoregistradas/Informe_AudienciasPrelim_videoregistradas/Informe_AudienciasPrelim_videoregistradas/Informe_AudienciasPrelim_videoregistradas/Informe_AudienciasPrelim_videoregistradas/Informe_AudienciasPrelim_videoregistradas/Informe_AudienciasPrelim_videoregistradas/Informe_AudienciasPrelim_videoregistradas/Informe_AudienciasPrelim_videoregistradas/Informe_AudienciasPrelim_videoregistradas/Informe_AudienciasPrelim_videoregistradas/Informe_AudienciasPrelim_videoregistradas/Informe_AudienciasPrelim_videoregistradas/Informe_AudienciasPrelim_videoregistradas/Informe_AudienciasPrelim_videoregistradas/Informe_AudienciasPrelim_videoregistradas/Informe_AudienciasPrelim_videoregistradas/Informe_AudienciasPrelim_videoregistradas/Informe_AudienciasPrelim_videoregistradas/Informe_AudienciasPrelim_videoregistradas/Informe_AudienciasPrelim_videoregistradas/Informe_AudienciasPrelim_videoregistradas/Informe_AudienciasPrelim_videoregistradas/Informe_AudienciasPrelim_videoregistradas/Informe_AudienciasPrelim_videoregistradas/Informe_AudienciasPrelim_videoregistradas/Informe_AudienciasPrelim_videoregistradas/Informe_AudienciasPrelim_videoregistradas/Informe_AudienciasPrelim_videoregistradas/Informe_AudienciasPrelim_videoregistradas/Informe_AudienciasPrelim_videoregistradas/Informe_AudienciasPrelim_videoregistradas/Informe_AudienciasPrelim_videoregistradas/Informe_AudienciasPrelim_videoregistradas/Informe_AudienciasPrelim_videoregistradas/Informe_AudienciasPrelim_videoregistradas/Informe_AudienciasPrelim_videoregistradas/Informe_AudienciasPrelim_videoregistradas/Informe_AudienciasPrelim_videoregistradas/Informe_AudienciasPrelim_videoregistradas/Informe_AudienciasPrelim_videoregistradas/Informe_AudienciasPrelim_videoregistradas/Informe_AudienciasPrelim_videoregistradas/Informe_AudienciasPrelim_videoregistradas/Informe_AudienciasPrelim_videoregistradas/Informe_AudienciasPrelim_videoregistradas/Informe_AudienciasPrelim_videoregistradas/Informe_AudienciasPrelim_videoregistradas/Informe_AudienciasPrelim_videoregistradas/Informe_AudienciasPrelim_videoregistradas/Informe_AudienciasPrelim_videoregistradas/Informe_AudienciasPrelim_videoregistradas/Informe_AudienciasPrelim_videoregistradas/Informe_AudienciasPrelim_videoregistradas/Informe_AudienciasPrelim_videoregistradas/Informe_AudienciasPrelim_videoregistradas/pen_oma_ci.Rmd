---
title: Legajos Iniciados
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
resultado <- iniciados_oma(poblacion = oma,
              start_date = start_date,
              end_date = end_date) 
```


## Legajos Iniciados en OMA  `r getDataIntervalStr(start_date, end_date)`

`r if (ci & t) '### Agrupados por Circunscripción'`

```{r, pen_oma_ci_t, eval= (ci & t)}
resultado$inic_xcirc %>% 
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Legajos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
  
```

`r if (ci & t) '### Agrupados por Responsable'`

```{r, pen_oma_ci_tr, eval = (ci & t)}
resultado$inic_xresp %>%
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Legajos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)
```

`r if (ci & pc) '### Listado Primario'`

```{r, pen_oma_ci_pc, eval = (ci & pc)}
resultado$inic_oma_pc %>%
  kable(caption = str_c("Legajos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```

