library(CGPfunctions)


iniciados_paz_23c(poblacion = jdos_paz_23,
                  start_date = start_date,
                  end_date = end_date) %>%
  iniciados_paz_comparativo() %>% View()


start_date = "2018-08-01"
end_date = "2019-07-01"
poblacion <- jdos_paz_23
desagregacion_mensual = T


inic <- apgyeRStudio::primariasOrganismo("jdopaz*", start_date, end_date)$CITIPP23$tabla() %>% 
  filter(iep %in% !!poblacion$organismo) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  process(CITIPP23) %>% .$primarias %>% 
  reimputaciones()

if(desagregacion_mensual) {
  inic <- inic %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), 
           fecha =  as.Date(data_interval_end) - 1)  
}

inicp <- inic %>% 
  select(iep, mes, fecha, tipo_procesos, iniciados, proceso, tramite, otro) %>% 
  rename(tproc = tipo_procesos) %>% 
  tidyr::gather(key = "grupo_proceso", value = "inic", 6:8, -iniciados, -tproc) %>% 
  filter(inic) %>% select(-inic) %>% 
  mutate(grupo_proceso = factor(grupo_proceso, levels = c("proceso", 
                                                          "tramite", 
                                                          "otro"),
                                ordered = TRUE)) %>% 
  group_by(iep, mes, fecha, grupo_proceso, tproc) %>% 
  summarise(cantidad = sum(iniciados, na.rm = T)) %>% 
  arrange(grupo_proceso, desc(cantidad)) %>% 
  do(janitor::adorn_totals(.)) %>% 
  mutate(tproc = ifelse(grupo_proceso == "-", "subtotal", tproc)) %>% 
  #filter(grupo_proceso != "otro") %>% 
  ungroup() %>% 
  left_join(poblacion %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  select(circunscripcion, organismo, grupo_proceso, tipo_proceso = tproc, everything(), -iep) 

inic_gotros <- inicp %>% 
  filter(grupo_proceso == "otro") %>% 
  group_by(circunscripcion, organismo, grupo_proceso, mes, fecha) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
  mutate(tipo_proceso = "OTROS-desclasif.")

inic_consolidado <- inicp %>% 
  ungroup() %>%
  filter(tipo_proceso != "subtotal") %>%
  filter(grupo_proceso != "otro") %>% 
  bind_rows(inic_gotros)


inic_ts_ <- inic_consolidado %>% 
  group_by(circunscripcion, organismo, grupo_proceso, fecha) %>% 
  summarise(iniciados = sum(cantidad, na.rm = T)) %>% 
  mutate(fecha = ymd(fecha))

inic_ts_fuero <- inic_consolidado %>% 
  group_by(grupo_proceso, fecha) %>% 
  summarise(iniciados = sum(cantidad, na.rm = T)) %>% 
  mutate(fecha = ymd(fecha))


inic_ts_fuero %>% 
  filter(grupo_proceso != "otro") %>% 
  mutate(mes = floor_date(fecha, "month")) %>% 
  ggplot(aes(x = mes, y = iniciados)) + 
  geom_point() +
  geom_text(aes(label=iniciados),hjust= 0.5, vjust= 1.5) +
  geom_line(stat = "identity") +
  geom_smooth(se = F) +
  theme_minimal() +
  labs(x = " ", y = "Iniciados") +
  scale_x_date(date_breaks = "1 month", date_labels = "%b-%y" ) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  facet_wrap(~ grupo_proceso, scales = "free_y") +
  labs(title = "Procesos y Trámites Voluntarios Iniciados en los Juzgados de Paz",
       subtitle = "Total Provincial", x = "",
       caption = "APGE-STJER")


# Grafico Trámites Iniciados por Organismo

inic_ts_ %>% 
  ungroup() %>% 
  filter(grupo_proceso == "tramite") %>% 
  mutate(mes = floor_date(fecha, "month")) %>% 
  ggplot(aes(x = mes, y = iniciados)) + 
  geom_point() +
  #geom_text(aes(label=iniciados),hjust= 0.5, vjust= 1.5) +
  geom_line(stat = "identity") +
  geom_smooth(se = F) +
  theme_minimal() +
  labs(x = " ", y = "Iniciados") +
  scale_x_date(date_breaks = "3 month", date_labels = "%b-%y" ) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  facet_wrap(~ organismo, scales = "free_y") +
  labs(title = "Trámites Voluntarios Iniciados por Juzgados de Paz",
       subtitle = paste0(start_date, "/", end_date), x = "",
       caption = "APGE-STJER")

# 7.3 Slope graphs
# https://rkabacoff.github.io/datavis/Time.html
# inic_ts_fuero %>% 
#   filter(grupo_proceso != "otro") %>% 
#   mutate(fecha = as.factor(fecha)) %>% 
#   newggslopegraph(fecha, iniciados, grupo_proceso, 
#                   YTextSize = 4,
#                   DataLabelPaddin = 0.1,
#                   WiderLabels = TRUE) +
#   labs(title="Casos Iniciados por tipo en Juzgados de Paz de 2a y 3a Cat.", 
#        subtitle="Total Provincial", 
#        caption="APGE")  


# slopgrafxcir <- function(df, circ) {
#   graf <- df %>%
#     ungroup() %>%
#     filter(circunscripcion == circ) %>%
#     filter(grupo_proceso == "tramite") %>%
#     mutate(fecha = as.factor(fecha)) %>%
#     newggslopegraph(fecha, iniciados, organismo, 
#                     YTextSize = 4,
#                     DataLabelPaddin = 0.5,
#                     WiderLabels = TRUE) +
#     labs(title="Casos Iniciados por tipo en Juzgados de Paz de 2a y 3a Cat.",
#          subtitle= str_c("Circunscripción ", circ),
#          caption="APGE")
#     graf
# }
# slopgrafxcir(inic_ts_, "Paraná")

  
iniciados_paz_23c(poblacion = jdos_paz_23,
                  start_date = start_date,
                  end_date = end_date, desagregacion_mensual = T) %>% 
  ts_iniciados_paz_23() %>% 
  tsg_p23("tramite")

