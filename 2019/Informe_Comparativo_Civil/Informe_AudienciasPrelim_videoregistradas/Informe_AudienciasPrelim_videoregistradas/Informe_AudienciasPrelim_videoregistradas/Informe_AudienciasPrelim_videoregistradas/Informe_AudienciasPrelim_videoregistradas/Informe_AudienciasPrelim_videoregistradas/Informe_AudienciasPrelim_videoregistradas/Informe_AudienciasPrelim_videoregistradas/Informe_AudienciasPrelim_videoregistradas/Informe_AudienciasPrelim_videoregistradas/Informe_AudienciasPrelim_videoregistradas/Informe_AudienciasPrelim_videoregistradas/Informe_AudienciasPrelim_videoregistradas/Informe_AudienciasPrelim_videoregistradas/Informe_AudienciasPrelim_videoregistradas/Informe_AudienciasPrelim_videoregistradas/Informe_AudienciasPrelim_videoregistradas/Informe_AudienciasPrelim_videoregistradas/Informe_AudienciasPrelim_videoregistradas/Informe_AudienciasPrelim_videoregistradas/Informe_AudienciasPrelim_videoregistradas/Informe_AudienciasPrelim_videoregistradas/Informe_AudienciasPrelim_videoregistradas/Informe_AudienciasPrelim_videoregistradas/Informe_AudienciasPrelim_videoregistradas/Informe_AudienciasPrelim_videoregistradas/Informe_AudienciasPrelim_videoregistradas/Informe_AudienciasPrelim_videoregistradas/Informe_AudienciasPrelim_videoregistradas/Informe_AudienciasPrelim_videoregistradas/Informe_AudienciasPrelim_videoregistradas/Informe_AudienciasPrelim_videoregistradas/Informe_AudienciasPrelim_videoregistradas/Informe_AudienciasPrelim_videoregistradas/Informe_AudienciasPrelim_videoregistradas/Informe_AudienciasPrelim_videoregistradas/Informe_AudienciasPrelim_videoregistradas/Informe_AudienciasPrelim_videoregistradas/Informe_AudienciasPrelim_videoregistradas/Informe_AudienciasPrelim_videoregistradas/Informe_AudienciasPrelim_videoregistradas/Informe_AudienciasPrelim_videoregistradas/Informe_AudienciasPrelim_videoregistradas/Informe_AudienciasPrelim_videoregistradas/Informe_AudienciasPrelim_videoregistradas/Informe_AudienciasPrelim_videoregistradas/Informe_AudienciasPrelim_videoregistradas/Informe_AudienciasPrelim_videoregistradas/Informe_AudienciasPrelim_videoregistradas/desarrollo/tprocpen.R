library(tibble)

inic <- DB_PROD() %>% 
  apgyeTableData('CIGTIA') %>% 
  apgyeDSL::interval("2018-07-01", "2019-03-01") %>% 
  mutate(finicio = dmy(finicio), fhecho = dmy(fhecho)) %>%
  filter(iep %in% oga$organismo) %>% 
  collect() 


inic_xtproc <- inic %>% 
  filter(!is.na(tproc)) %>% 
  group_by(tproc) %>% 
  summarise(cantidad_casos = n()) %>% 
  mutate(index = row_number())



inic_xtproc <- inic_xtproc %>% 
  # mutate(tentativa = str_detect(tproc, " EN GRADO DE TENTATIVA| TENTATIVA|TENTATIVA")) %>% 
  # mutate(tproc = str_remove_all(tproc, " EN GRADO DE TENTATIVA|EN GRADO DE TENTATIVA")) %>% 
  # mutate(tproc = str_remove_all(tproc, " TENTATIVA")) %>% 
  # mutate(tproc = str_remove_all(tproc, "TENTATIVA")) %>% 
  # mutate(tproc = ifelse(tproc == "", NA, tproc)) %>% 
  filter(!is.na(tproc), 
         !str_detect(tproc, "^ADMINISTRATIVO|^ARCHIVO|^AUTORIZACION|CONCURSO CERRADO"),
         !str_detect(tproc, "^CONCURSOS|^DESIGNACION|^EFECTOS SECUESTRADOS|^EXHORTO|^INFORMES"),
         !str_detect(tproc, "^LEGAJO|^OFICIO|^PERSONAL|^SENTENCIA|^SU PRESENTACION|^SU SITUACION"), 
         !str_detect(tproc, "^EJECUCION DE SENTENCIA|^EJECUTIVO|FALLECIMIENTO|IMPEDIMENTO DE CONTACTO|^INCIDENTE|INSTRUCCION"),
         !str_detect(tproc, "^LIBRAMIENTO DE CHEQUE|^MEDIDA CAUTELAR|^OTRO|^SOLICITUD"),
         !str_detect(tproc, "^VARIOS|^TESTIMONIOS|Y OTROS"),
         !str_detect(tproc, "^JURISPRUDENCIA|LEY")) 
  
  
  
  
  mutate(tproc = str_detect(tproc, "9.198|9198", "VIOLENCIA FAMILIAR", tproc)) %>% 
  mutate(tproc = str_replace_all(tproc, "SU DENUNCIA|DENUNCIA|SU DENUNCIA RECIPROCAS|", "DENUNCIAS")) %>% 
  mutate(tproc = str_replace_all(tproc, "VIOLENCIA DE GÉNERO", "VIOLENCIA DE GENERO")) %>% 
  mutate(tproc = str_replace_all(tproc, "IMPEDIMIENTO DE CONTACTO", "INFRACCION LEY 24270")) %>%
  mutate(tproc = str_replace_all(tproc, "INFRACCION A LA LEY Nº 24.270 (Impedimento de Contacto)", "INFRACCION LEY 24270")) %>%
  mutate(tproc = str_replace_all(tproc, "INFRACCION A LA LEY Nº 13.944 (Incump. Deberes Asistencia Fliar.)", "INFRACCION LEY 13944")) %>%
  mutate(tproc = str_replace_all(tproc, "DELITOS LEY 23737 - ESTUPEFACIENTES", "COMERCIO DE ESTUPEFACIENTES")) %>%
  mutate(tproc = str_replace_all(tproc, "INFRACCION A LA LEY Nº 23.737", "COMERCIO DE ESTUPEFACIENTES")) %>%
  mutate(tproc = str_replace_all(tproc, "ACTUACIONES PARA ESTABLECER FORMAS Y CIRCUNSTANCIAS|
                                 ACTUACIONES PARA ESTABLECER PROCEDENCIA|ACTUACIONES PARA ESTABLECER PARADERO|INVESTIGACION DE OFICIO",
                                 "ACTUACIONES DE OFICIO")) %>%
  
    


# AGREGAR EN tprocpenal





inic_xtproc <- inic_xtproc %>% 
  mutate(cantidad_palabras = str_count(tproc, '\\w+'))

# Preprocesameinto del diccionario
tproc <- apgyeOperationsJusER::Tipo_procesos
 
ptproc <- tproc %>% 
  filter(str_detect(materia, "penal")) %>%
  distinct(tipo_de_procesos, .keep_all = T)

masptproc <- ptproc[1:9, ]
masptproc$tipo_de_procesos <- c("ALLANAMIENTO",
                                "VIOLENCIA FAMILIAR",
                                "INFRACCION LEY 24270",
                                "COMERCIO DE ESTUPEFACIENTES",
                                "SUMINISTRO GRATUITO DE ESTUPEFACIENTES",
                                "TENENCIA DE ESTUPEFACIENTES CON FINES DE COMERCIALIZACIÓN",
                                "TENENCIA DE ESTUPEFACIENTES PARA CONSUMO PERSONAL",
                                "TENENCIA SIMPLE DE ESTUPEFACIENTES",
                                "ACTUACIONES DE OFICIO")
masptproc$materia <- "penal"
masptproc$codigo_procesos <- c(1804:1812)
masptproc$Penal_instanciayTipo_delito <- "LEYES ESPECIALES"

ptproc <- bind_rows(ptproc, masptproc)

saveRDS(tprocpenal, "tprocpenal.rds")
# 

# 
# tprocpenal <- tprocpenal %>% 
#   mutate(tipo_de_procesos = str_trim(tipo_de_procesos),
#          tipo_de_procesos = toupper(tipo_de_procesos),
#          tipo_de_procesos = str_remove_all(tipo_de_procesos, "[[:punct:]]"), 
#          tipo_de_procesos = str_remove_all(tipo_de_procesos, "º"))
# 

# tprocpenal <- tprocpenal %>% 
#   filter(!str_detect(tipo_de_procesos, "Y OTR"))
# Clasificación por tipos de procesos individuales

tprocpenal <- readRDS("~/apgyeinformes/2019/Modelo_Parametrizado/data/tprocpenal.rds")


listresul <- list()

for (i in seq_along(inic_xtproc$tproc)) {
  
  matches <- tprocpenal$tipo_de_procesos[str_detect(inic_xtproc$tproc[[i]], tprocpenal$tipo_de_procesos)]
  
  contenido <- vector()
  
  for (j in seq_along(matches)) {
    if (sum(str_detect(matches, matches[j])) > 1) {
      contenido[j] <- TRUE
    } else {
      contenido[j] <- FALSE
    }
    contenido
  }
  
  listresul[[i]] <- matches[!contenido]
 
}

listresul <- purrr::map(listresul, str_c, collapse = ",")


for (i in seq_along(listresul)) {
  if (length(listresul[[i]]) == 0) {
    listresul[[i]] <- "sin_match"
  } else {
    listresul[[i]] 
  }
}

normtproc <- purrr::flatten_chr(listresul)

inic_xtproc <- inic_xtproc %>% 
  mutate(normtproc =  normtproc)


