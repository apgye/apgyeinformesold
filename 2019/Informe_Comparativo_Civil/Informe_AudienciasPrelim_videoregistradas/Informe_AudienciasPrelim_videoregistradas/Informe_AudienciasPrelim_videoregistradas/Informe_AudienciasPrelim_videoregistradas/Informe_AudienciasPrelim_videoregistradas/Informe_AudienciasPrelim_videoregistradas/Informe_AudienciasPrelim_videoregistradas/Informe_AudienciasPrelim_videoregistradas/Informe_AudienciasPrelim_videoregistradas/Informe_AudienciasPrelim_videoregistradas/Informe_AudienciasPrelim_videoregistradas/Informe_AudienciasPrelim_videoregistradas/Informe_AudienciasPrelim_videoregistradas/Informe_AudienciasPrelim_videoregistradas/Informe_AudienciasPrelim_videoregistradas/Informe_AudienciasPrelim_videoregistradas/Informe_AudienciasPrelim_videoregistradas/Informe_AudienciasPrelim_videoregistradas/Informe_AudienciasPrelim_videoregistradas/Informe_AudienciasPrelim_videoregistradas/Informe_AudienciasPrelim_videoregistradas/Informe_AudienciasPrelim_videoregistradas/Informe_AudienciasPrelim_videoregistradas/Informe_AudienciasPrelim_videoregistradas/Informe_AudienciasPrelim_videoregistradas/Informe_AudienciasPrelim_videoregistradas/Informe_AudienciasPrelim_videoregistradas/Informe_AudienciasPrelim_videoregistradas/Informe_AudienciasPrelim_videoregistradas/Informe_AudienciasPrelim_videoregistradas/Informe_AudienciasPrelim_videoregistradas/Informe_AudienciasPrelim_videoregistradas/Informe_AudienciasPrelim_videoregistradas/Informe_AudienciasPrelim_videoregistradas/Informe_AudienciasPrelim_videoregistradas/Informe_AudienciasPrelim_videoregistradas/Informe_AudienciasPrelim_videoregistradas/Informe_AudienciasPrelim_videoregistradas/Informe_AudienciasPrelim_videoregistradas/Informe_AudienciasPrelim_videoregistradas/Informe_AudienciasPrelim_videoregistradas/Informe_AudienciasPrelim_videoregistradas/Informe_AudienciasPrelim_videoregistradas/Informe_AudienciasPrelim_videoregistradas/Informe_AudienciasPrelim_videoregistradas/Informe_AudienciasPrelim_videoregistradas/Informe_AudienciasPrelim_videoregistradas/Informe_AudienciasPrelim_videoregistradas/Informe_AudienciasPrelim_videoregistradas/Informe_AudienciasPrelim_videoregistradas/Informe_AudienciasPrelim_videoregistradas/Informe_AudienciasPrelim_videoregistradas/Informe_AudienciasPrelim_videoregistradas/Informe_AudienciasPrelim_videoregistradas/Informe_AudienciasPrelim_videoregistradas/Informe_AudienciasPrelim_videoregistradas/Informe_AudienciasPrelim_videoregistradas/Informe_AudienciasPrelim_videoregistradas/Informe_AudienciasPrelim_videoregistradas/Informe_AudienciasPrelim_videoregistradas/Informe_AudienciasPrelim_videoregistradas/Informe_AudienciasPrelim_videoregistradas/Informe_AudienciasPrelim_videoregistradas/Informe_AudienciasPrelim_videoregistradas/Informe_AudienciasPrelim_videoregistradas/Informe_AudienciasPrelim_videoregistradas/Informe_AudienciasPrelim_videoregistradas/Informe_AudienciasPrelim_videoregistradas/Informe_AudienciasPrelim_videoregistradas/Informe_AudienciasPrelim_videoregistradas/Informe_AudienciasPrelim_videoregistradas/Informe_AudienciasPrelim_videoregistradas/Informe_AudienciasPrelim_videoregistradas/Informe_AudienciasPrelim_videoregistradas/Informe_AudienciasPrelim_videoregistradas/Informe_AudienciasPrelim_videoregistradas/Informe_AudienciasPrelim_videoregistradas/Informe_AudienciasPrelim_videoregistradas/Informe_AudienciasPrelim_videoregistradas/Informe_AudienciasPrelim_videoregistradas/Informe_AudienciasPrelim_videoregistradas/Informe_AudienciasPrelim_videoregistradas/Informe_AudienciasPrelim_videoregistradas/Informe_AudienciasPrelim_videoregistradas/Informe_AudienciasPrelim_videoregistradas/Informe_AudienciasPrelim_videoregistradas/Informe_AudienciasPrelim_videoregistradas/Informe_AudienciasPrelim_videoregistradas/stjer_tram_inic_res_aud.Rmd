---
title: "Superior Tribunal de Justicia"
subtitle: "Producto Bruto Judicial Año 2018"
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      toc: TRUE
      toc_depth: 3
      number_sections: TRUE
      includes:
        in_header: header.tex
        before_body: before_body.tex
geometry: margin=1.2cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
header-includes: \renewcommand{\contentsname}{Contenido del Documento}
params:
  circunscripcion:
    label: "Circunscripción (separar con coma sin espacio o 'Todas')"
    input: text
    value: "Todas"
  ieps: 
    label: "Organismo (sd = sin discriminar)"
    input: text
    value: "sd"
  fuero:
    label: "Fuero o Materia:Civil,Familia,Paz,Laboral,CAdm,ECQ,Penal,PEjec,PMediación"
    input: text
    value: "Civil"
  instancia1: TRUE
  instancia2: FALSE
  instancia3: FALSE
  start_date: 
    label: "Fecha_inicio_inf"
    input: text
    value: "2018-01-01"
  end_date:
    label: "Fecha_fin_inf"
    input: text
    value: "2019-01-01" 
  desagregacion_mensual: TRUE
  CausasIniciadas: FALSE 
  CausasResueltas: FALSE
  CausasEnTramite: FALSE
  CausasEnTramite_detalle: FALSE
  CausasyMovimientos: FALSE
  SentenciasTipo: FALSE
  Audiencias: FALSE
  Audiencias_civil_detalle: FALSE
  Audiencias_familia_detalle: FALSE
  Audiencias_laboral_detalle: FALSE
  Audiencias_penales: FALSE
  Duracion_Procesos_xorg: FALSE
  Duracion_Procesos_interanual: FALSE
  CausasArchivadas: FALSE 
  Consideraciones_Finales: FALSE
  Pendientes: FALSE
  Personal: FALSE
  Primarias: FALSE
  Boletin_intro: FALSE
  Metodología: TRUE
  Presentaciones: FALSE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
knitr::opts_chunk$set(fig.width=12, fig.height=10) 
options(knitr.kable.NA = 'na')
source("informe.R")
source("utils.R")
source("codconver.R")
library(readr)
```

```{r parametros, echo=FALSE, include=FALSE}
# parámetros del informe

if (params$circunscripcion == "Todas"){
  circ <- NA
} else {
  circ <- unlist(str_split(params$circunscripcion, ","))
}

if (params$ieps == "sd"){
  ieps <- NA
} else {
  ieps <- unlist(str_split(params$ieps, ","))
}

fuero <- unlist(str_split(params$fuero, ",")) 
instancia1 <- params$instancia1
instancia2 <- params$instancia2
instancia3 <- params$instancia3
start_date <- params$start_date
end_date <- params$end_date
desagregacion_mensual <- params$desagregacion_mensual
ci <- params$CausasIniciadas
ca <- params$CausasArchivadas
cr <- params$CausasResueltas
ce <- params$CausasEnTramite
ced <- params$CausasEnTramite_detalle
cm <- params$CausasyMovimientos
s <- params$SentenciasTipo
a <- params$Audiencias
afd <- params$Audiencias_familia_detalle
ald <- params$Audiencias_laboral_detalle
acd <- params$Audiencias_civil_detalle
ap <- params$Audiencias_penales
d <- params$Duracion_Procesos_xorg
di <- params$Duracion_Procesos_interanual
cf <- params$Consideraciones_Finales
p <- params$Personal
pend <- params$Pendientes
prim <- params$Primarias
metodologia <- params$Metodología
presentaciones <- params$Presentaciones
bo <- params$Boletin_intro
```


```{r poblacion, echo=FALSE, include=FALSE}
# Poblacion

# Entidades completo
poblacion_er <- apgyeJusEROrganization::listar_organismos() 

# Fuero Civil-Comercial y Laboral
poblacion_total <- poblacion_er %>% 
  filter(tipo %in% c("jdo", "cam", "sal")) 

if (exists("circ")) {
  if(is.na(circ)) {
    poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>% 
    filter(circunscripcion %in% circ)
  }
}

if (exists("ieps")) {
    if(is.na(ieps)) {
    poblacion_total <- poblacion_total
    } else {
    poblacion_total <- poblacion_total %>%
    filter(organismo %in% ieps)
    poblacion_total
    }
}

# Sub-poblaciones
jdos_cco <- poblacion_total %>% 
  filter(grepl("jdocco|jdopen0000fel", organismo)) %>% 
  filter(str_detect(materia, "cco"), categoria == "1" | is.na(categoria)) 

if (any(str_detect(jdos_cco$materia, "lab|fam"))) { 
  multifueros <- TRUE 
  } else {
  multifueros <- FALSE
  }

jdos_ecq <- poblacion_total %>% 
  filter(grepl("jdocco", organismo)) %>% 
  filter(str_detect(materia, "eje|cqb"), categoria == "1" | is.na(categoria))

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria)) 

jdos_paz <- poblacion_total %>% 
  filter(str_detect(materia, "paz"), 
         categoria == "1" | (categoria == "3" & organismo == "turvol0100pna") 
         |  organismo == "jdopaz0000ram" )

jdos_paz_23 <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria != "1" | is.na(categoria))

if (nrow(jdos_paz_23) != 0) { 
  paz_23 <- TRUE 
  } else {
  paz_23 <- FALSE
  }

jdos_lab <- poblacion_total %>% 
  filter(str_detect(organismo, "jdolab"), tipo != "cam")

cam_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "cam")

cam_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "cam")

sal_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "sal")

sal_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "sal")

# Fuero Penal

poblacion_penal <- poblacion_er %>% 
  filter(str_detect(fuero, "Penal")) 

if (exists("circ")) {
  if(is.na(circ)) {
    poblacion_penal <- poblacion_penal
  } else {
    poblacion_penal <- poblacion_penal %>% 
    filter(circunscripcion %in% circ)
  }
}

if (exists("ieps")) {
    if(is.na(ieps)) {
    poblacion_penal <- poblacion_penal
    } else {
    poblacion_penal <- poblacion_penal %>%
    filter(organismo %in% ieps)
    poblacion_penal
    }
}

oga <- poblacion_penal %>% 
  filter(str_detect(organismo, "oga|jez"))

oma <- poblacion_penal %>% 
  filter(str_detect(organismo, "oma|equ"))

tja <- poblacion_penal %>% 
  filter(str_detect(organismo, "tja"))

jdo_pna <- poblacion_penal %>% 
  filter(str_detect(organismo, "jdopna"))

jdo_pep <- poblacion_penal %>% 
  filter(str_detect(organismo, "jdopep"))

cam_pen <- poblacion_penal %>% 
  filter(str_detect(organismo, "campen"), str_detect(materia, "pen"))

sal_pen <- poblacion_penal %>% 
  filter(str_detect(organismo, "sal"))

# Fuero Cont_Adm
cam_cad <- poblacion_er %>% 
  filter(str_detect(materia, "cad"), tipo == "cam") 

stj_cad <- poblacion_er %>% 
  filter(str_detect(materia, "cad"), tipo == "stj") 

```


```{r}
# Período Anual 2018
start_date = "2018-01-01"
end_date = "2019-01-01"
General_VS_Matriz_Volcado_2018 <- readRDS(file = "General_VS_Matriz_Volcado_2018.rds")
resultado_GTIA_VS <- read_csv("~/apgyeinformes/2019/Modelo_Parametrizado/data/resultado_GTIA_VS.csv")
resultado_TRAN_VS <- read_csv("~/apgyeinformes/2019/Modelo_Parametrizado/data/resultado_TRAN_VS.csv")

```


```{r}
# Consulta por:
#   grupo de organismos
#   anual
#   indicador
# Juzgados Civil y Comercial-------------------------------------------------
entram <- entramite(poblacion = jdos_cco, operacion_inic = "CINC1C") %>% 
  enTramite_gral() %>%  ungroup() %>% 
  summarise(causas_en_tramite = sum(causas_en_tramite_ultimos_2años, na.rm = T))
 
inic <- iniciados_cco(poblacion = jdos_cco, start_date = start_date, end_date = end_date) %>% 
  filter(tipo_proceso == "subtotal") %>% 
  summarise(causas_iniciadas = sum(cantidad, na.rm = T))
 
resoluciones <- resoluciones_cco(poblacion = jdos_cco, start_date = start_date,
  end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>%
  filter(mes == "Total") %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audic <- audicco_prim(jdos_cco, start_date, end_date) %>%
  audicco_realizadas_xtipoyresultado(desagregacion_mensual = desagregacion_mensual) %>% 
  filter(circunscripcion == "Total") %>% 
  summarise(audiencias_conciliadas = sum(conciliacion_total)) 
  
jdos_cco <- bind_cols(entram, resoluciones, audic, inic) %>% 
  mutate(instancia_fuero = "Juzgados Civiles y Comerciales") %>% 
  select(instancia_fuero, everything())

# Juzgados Ejec-ConcQuiebras--------------------------------------------------
entram <- entramite(poblacion = jdos_ecq, operacion_inic = "CINC1C") %>%
  enTramite_gral() %>% ungroup() %>% 
  summarise(causas_en_tramite = sum(causas_en_tramite_ultimos_2años, na.rm = T))

inic <- iniciados_ecq(poblacion = jdos_ecq, start_date = start_date, end_date = end_date) %>% 
  filter(tipo_proceso == "subtotal") %>%  
  summarise(causas_iniciadas = sum(cantidad, na.rm = T))
 
resoluciones <- resoluciones_cco(poblacion = jdos_ecq, start_date = start_date,
  end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>%
  filter(mes == "Total") %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audic <- audicco_prim(jdos_ecq, start_date,  end_date) %>%
  audicco_realizadas_xtipoyresultado(desagregacion_mensual = desagregacion_mensual) %>% 
  filter(circunscripcion == "Total") %>% 
  summarise(audiencias_conciliadas = sum(conciliacion_total)) 
  
jdos_ecq <- bind_cols(entram, resoluciones, audic, inic) %>% 
  mutate(instancia_fuero = "Juzgados de Ejecuciones y Concursos-Quiebras") %>% 
  select(instancia_fuero, everything())

# Juzgados de Familia-----------------------------------------------------------
entram <- entramite(poblacion = jdos_fam, operacion_inic = "CINC1FC") %>%
  enTramite_gral() %>%  ungroup() %>% 
  summarise(causas_en_tramite = sum(causas_en_tramite_ultimos_2años, na.rm = T))

inic <- iniciados_fam(poblacion = jdos_fam, start_date = start_date, end_date = end_date) %>%
  filter(tipo_proceso == "subtotal") %>%  
  summarise(causas_iniciadas = sum(cantidad, na.rm = T))

resoluciones <- resoluciones_fam(poblacion = jdos_fam, start_date = start_date,
  end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>%
  filter(mes == "Total") %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audif_1 <- audifam_prim(jdos_fam, start_date, end_date) %>%
  audifam_realizadas_xresultado(desagregacion_mensual = desagregacion_mensual) %>%
  select(circunscripcion, conciliacion_total, conciliacion_en_proceso_sin_contradiccion) %>% 
  filter(circunscripcion == "Total") %>% rowwise() %>% 
  mutate(aud_conciliadas = sum(conciliacion_total, conciliacion_en_proceso_sin_contradiccion, na.rm = T)) 

audif <- tribble(~audiencias_conciliadas, sum(audif_1$aud_conciliadas))

jdos_fam <- bind_cols(entram, resoluciones, audif, inic) %>% 
  mutate(instancia_fuero = "Juzgados de Familia") %>% 
  select(instancia_fuero, everything())

# Juzgados de Paz---------------------------------------------------------------
entram <- entramite(poblacion = jdos_paz, operacion_inic = "CINC1C") %>%
  enTramite_gral() %>%  ungroup() %>% 
  summarise(causas_en_tramite = sum(causas_en_tramite_ultimos_2años, na.rm = T))

inic <- iniciados_paz_procxgpo(poblacion = jdos_paz, start_date = start_date, end_date = end_date) %>% 
  filter(tipo_proceso == "subtotal") %>%  
  summarise(causas_iniciadas = sum(cantidad, na.rm = T))

resoluciones <- resoluciones_paz(poblacion = jdos_paz, start_date = start_date,
  end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>%
  filter(mes == "Total") %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audic <- tribble(~audiencias_conciliadas, 0)
                                                   
jdos_paz1 <- bind_cols(entram, resoluciones, audic, inic) %>% 
  mutate(instancia_fuero = "Juzgados de Paz 1ª cat.") %>% 
  select(instancia_fuero, everything())

# Juzgados de Paz 2 y 3 Cat-----------------------------------------------------
entram <- tribble(~causas_en_tramite, 0)

inic <- iniciados_paz_23c(poblacion = jdos_paz_23 , start_date = start_date, end_date = end_date) %>% 
  filter(grupo_proceso == "proceso") %>%  
  summarise(causas_iniciadas = sum(cantidad, na.rm = T))

resoluciones <- resoluciones_paz_23(poblacion = jdos_paz_23, start_date = start_date,
  end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>%
  filter(mes == "Total") %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audic <- tribble(~audiencias_conciliadas, 0)
                                                   
jdos_paz23 <- bind_cols(entram, resoluciones, audic, inic) %>% 
  mutate(instancia_fuero = "Juzgados de Paz 2ª y 3ª cat.") %>% 
  select(instancia_fuero, everything())

# Juzgados Laborales------------------------------------------------------------
entram <- entramite(poblacion = jdos_lab, operacion_inic = "CINC1C") %>% 
  enTramite_gral() %>%  ungroup() %>% 
  summarise(causas_en_tramite = sum(causas_en_tramite_ultimos_2años, na.rm = T))
 
inic <- iniciados_lab(poblacion = jdos_lab, start_date = start_date,
              end_date = end_date) %>% 
  filter(tipo_proceso == "subtotal") %>% 
  summarise(causas_iniciadas = sum(cantidad, na.rm = T))
 
resoluciones <- resoluciones_lab(poblacion = jdos_lab, start_date = start_date,
  end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>%
  filter(mes == "Total") %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audil <- audilab_prim(jdos_lab, start_date, end_date) %>%
  audilab_realizadas_xtipoyresultado(desagregacion_mensual = desagregacion_mensual) %>% 
  filter(circunscripcion == "Total") %>% 
  summarise(audiencias_conciliadas = sum(Conciliacion_Total)) 
  
jdos_lab <- bind_cols(entram, resoluciones, audil, inic) %>% 
  mutate(instancia_fuero = "Juzgados Laborales") %>% 
  select(instancia_fuero, everything())

# Juzgado Penal de  Menores-----------------------------------------------------
# Primer Semestre informado por el Secretario del Organismo via correo, corresponde
# enteros consignados en las funciones de suma
entram <- tribble(~causas_en_tramite, 0)

inic <- iniciadas_pen(poblacion = jdo_pna, start_date = start_date, end_date = end_date) %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(causas_iniciadas = sum(cantidad + 25, na.rm = T))

resoluciones <-  resoluciones_pen(poblacion = jdo_pna, start_date = start_date, end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>% 
  filter(circunscripcion == "Total") %>% 
  summarise(sentencias = sum(sentencias + 28), autos = sum(autos + 21))

audip <- tribble(~audiencias_conciliadas, NA)
     
jdos_pna <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Juzgado Penal de Menores") %>% 
 select(instancia_fuero, everything())


# Juzgado de Ejecución de Penas-------------------------------------------------
# Primera Parte Feb:Sept ambos juzgados
# W:\DATABASE\PROCESADAS\VOLCADO\2018\FUERO PENAL
inic_jdopep_vs <- 170 + 236
autos_jdopep_fs <- 1082 + 369


entram <- tribble(~causas_en_tramite, 523+917) # obtenido de la decalración de datos de septiembre18

inic <- iniciadas_pen(poblacion = jdo_pep, start_date = start_date,end_date = end_date) %>%
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(causas_iniciadas = sum(cantidad_iniciados + inic_jdopep_vs, na.rm = T))

resoluciones <- resoluciones_pen(poblacion = jdo_pep, start_date = start_date, end_date = end_date, 
              desagregacion_mensual = desagregacion_mensual) %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(sentencias = NA, autos = sum(cantidad_resueltos + autos_jdopep_fs, na.rm = T))

audip <- tribble(~audiencias_conciliadas, NA) # no aplica

jdos_pep <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Juzgado Penal de Ejecución de Penas y MS") %>% 
 select(instancia_fuero, everything())

# Cámaras Civiles--------------------------------------------------------------
entram <- tribble(~causas_en_tramite, NA) # pendiente

#iniciados cámaras: revisar

inic <- iniciados_cam(poblacion = cam_civil, start_date = start_date,end_date = end_date) %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>%
  summarise(causas_iniciadas = sum(ingresa, na.rm = T))
 
resoluciones <- resoluciones_cco(poblacion = cam_civil, start_date = start_date, end_date = end_date,
  operacion = "CADR2C", desagregacion_mensual = desagregacion_mensual) %>% 
   filter(mes == "Total") %>% ungroup() %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))
  
audip <- tribble(~audiencias_conciliadas, NA) # no aplica

cam_civil <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Cámaras Civiles y Comerciales") %>% 
 select(instancia_fuero, everything())

# Camaras Laborales------------------------------------------------------------
entram <- tribble(~causas_en_tramite, NA) # pendiente

inic <- iniciados_cam(poblacion = cam_lab, start_date = start_date,end_date = end_date) %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>%
  summarise(causas_iniciadas = sum(ingresa, na.rm = T))
 
resoluciones <- resoluciones_lab(poblacion = cam_lab, start_date = start_date, end_date = end_date,
  operacion = "CADR2L", desagregacion_mensual = desagregacion_mensual) %>% 
   filter(mes == "Total") %>% ungroup() %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))
  
audip <- tribble(~audiencias_conciliadas, NA) # no aplica

cam_lab <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Cámaras Laborales") %>% 
 select(instancia_fuero, everything())

# Cámaras de Casación Penal---------------------------------------------------
entram <- tribble(~causas_en_tramite, NA) # pendiente

inic <- iniciadas_pen(poblacion = cam_pen, start_date = start_date, end_date = end_date) %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(causas_iniciadas = sum(cantidad, na.rm = T))

resoluciones <- resoluciones_pen(poblacion = cam_pen, start_date = start_date, end_date = end_date,
                                 desagregacion_mensual = desagregacion_mensual) %>%
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audip <- tribble(~audiencias_conciliadas, NA) # no aplica

cam_pen <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Cámaras Casación Penal") %>% 
 select(instancia_fuero, everything())

# Cámaras Contencioso Administrativas------------------------------------------
entram <- tribble(~causas_en_tramite, 0) # pendiente

# inic feb-may
inic_camcad_vs <- 98

inic <- iniciados_camcad(poblacion = cam_cad, start_date = start_date, end_date = end_date) %>%
  filter(tproc == "-") %>% ungroup() %>% 
  summarise(causas_iniciadas = sum(Cantidad_Iniciados,inic_camcad_vs, na.rm = T))

# resol feb_may
resoluciones_vs <- General_VS_Matriz_Volcado_2018 %>% 
  filter(str_detect(organismo, "camcad")) %>%
  summarise(sentencias = sum(s, na.rm = T), autos = sum(a, na.rm = T))
  
resoluciones <- resoluciones_cad_1(poblacion = cam_cad, operacion = "CADRCAD", 
                                   start_date = start_date, end_date = end_date,
                                   desagregacion_mensual = desagregacion_mensual) %>%
  filter(mes == "Total") %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

resoluciones <- resoluciones %>% 
  bind_rows(resoluciones_vs) %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audip <- tribble(~audiencias_conciliadas, NA)
     
cam_cad <- bind_cols(entram, resoluciones, audip, inic) %>% 
  mutate(instancia_fuero = "Cámaras Contencioso Administrativas") %>% 
  select(instancia_fuero, everything())

# OGAs-------------------------------------------------------------------------- 
entram <- tribble(~causas_en_tramite, 0) # pendiente
# Garantía----------------------------------------------------------------------
 #  Garantía Anual 2018
inic <- tribble(~inic, 10537) # dato procesado para SNEJ, viejo y nuevo sistema estadístico, 
 # Función que debe reactivarse para informar 2019.
 # inic <- iniciadas_pen(poblacion = oga, start_date = start_date, end_date = end_date) %% 
 #   filter(circunscripcion == "Total") %% ungroup() %% 
 #   summarise(causas_iniciadas = sum(cantidad, na.rm = T))
 
 # Garantía Resoluciones
 # Resuelto
 # GTIA_VS_RES_9_a1_sobreseimiento_extincion_accion_penal_x_prescr",
 # "GTIA_VS_RES_9_a2_sobreseimiento_extincion_accion_penal_x_fallec",
 # "GTIA_VS_RES_9_a3_sobreseimiento_extincion_accion_penal_x_otras",
 # "GTIA_VS_RES_9_b_sobreseimiento_76_ter_CP",
 # "GTIA_VS_RES_9_c_sobreseimiento_cumplimiento_acuerdo_mediacion_o",
 # "GTIA_VS_RES_9_d_sobreseimiento_causales_atipicidad_justificacio",
 # "GTIA_VS_RES_13_medidas_seguridad",
 # "GTIA_VS_RES_14_suspension_proceso_penal_a_prueba",
 # "GTIA_VS_RES_16_juicio_abreviado",
vs_regex_sent <- "_9_|13_|_14_|_16_"
 
gtia_sent <- resultado_GTIA_VS %>% 
   select(matches(vs_regex_sent)) %>% 
   summarise(sentencias = sum(., na.rm = T)) %>% .$sentencias
 
gtia_aut <- resultado_GTIA_VS %>% 
   select(9,10,11,12,15,17,18,19,20,21,22,23,24,25) %>% 
   summarise(sentencias = sum(., na.rm = T)) %>% .$sentencias
 
trans_aut <- resultado_TRAN_VS %>% 
   select(matches("resol")) %>% 
   summarise(autos = sum(., na.rm = T)) %>% .$autos
 
resoluciones <- resoluciones_pen(poblacion = oga, start_date = start_date,
               end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>% 
   filter(circunscripcion == "Total") %>% ungroup() %>%
   summarise(sentencias = sum(sobreseimientos, condena_efectiva, condena_efectiva_sustit, 
                            condena_condicional, medida_seguridad, 228, gtia_sent, na.rm = T), 
             # 228 surge de vsW:\DATABASE\PROCESADAS\VOLCADO\2018\FUERO PENAL\JUZGADOS DE GARANTÍAS\2018
             autos = sum(suspension_proc_prueba, orden_detencion, med_protec_victim, med_coercion,
                         prision_prev, sustitutiva_prision_prev, competencia, prorroga_ipp,
                         remision_jdo_menores, formulacion_cargos, revision_medidas, revision_reglas_conducta, 
                         allanamientos, otros, 1499, gtia_aut, trans_aut, na.rm = T))
             # 1499 surge vs. idem
 
audip <- tribble(~audiencias_conciliadas, NA) # no aplica
 
garantia <- bind_cols(entram, resoluciones, audip, inic) %>% 
  mutate(instancia_fuero = "Jueces Penales de Garantías") %>% 
  select(instancia_fuero, everything())


#   T-juicio--------------------------------------------------------------------
entram <- tribble(~causas_en_tramite, NA) # pendiente

# Tribunal de Justicia y Apelacion
inic <- tribble(~inic, 940) ## dato procesado para SNEJ, viejo y nuevo sistema estadístico
# inic <- iniciadas_pen(poblacion = tja, start_date = start_date, end_date = end_date) %>%
#   filter(circunscripcion == "Total") %>% ungroup() %>% 
#   summarise(causas_iniciadas = sum(cantidad, na.rm = T))
# inic_ap <- inic_ap %>% 
#   filter(circunscripcion == "Total") %>% ungroup() %>% 
#   summarise(causas_iniciadas = sum(cantidad, na.rm = T))
# inic <- inic %>% 
#   bind_rows(inic_ap) %>% 
#   summarise(causas_iniciadas = sum(causas_iniciadas, na.rm = T))

# Resoluciones
resultado_TJUI_VS <- read_csv("~/apgyeinformes/2019/Modelo_Parametrizado/data/resultado_TJUI_VS.csv")

# Tribunal Juicio
tjui_sent <- resultado_TJUI_VS %>% 
  select(matches("SENTENC")) %>% 
  summarise(sentencias = sum(., na.rm = T)) %>% .$sentencias

tjui_aut <- resultado_TJUI_VS %>% 
  select(matches("RESOL")) %>% 
  summarise(sentencias = sum(., na.rm = T)) %>% .$sentencias

resoluciones <- resoluciones_pen(poblacion = tja, start_date = start_date, end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(sentencias = sum(sobreseimientos, condena_efectiva, condena_efectiva_sustit, 
                             condena_condicional, absoluciones, medida_seguridad, tjui_sent, na.rm = T),
            autos = sum(otra, tjui_aut, na.rm = T))

resultado_tapel <- resultado_tapel %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(sentencias = sum(confirmatoria, revocatoria,nulidad, na.rm = T),
            autos = sum(queja, inadmisible, mal_concedido,desistimiento, otra, abstracto, na.rm = T))

resoluciones <- resoluciones %>% 
  bind_rows(resultado_tapel) %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

tjuicioya <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Tribunales de Juicio y Apelación") %>% 
 select(instancia_fuero, everything())

# Sala Penal


entram <- tribble(~causas_en_tramite, NA) # pendiente

# 323 + 29 + 33 Sala Penal - Penal (I)# Sala Penal - Constitucional, procesado SNEJ
# inic <- iniciadas_pen(poblacion = sal_pen, start_date = start_date, end_date = end_date) %>%
#   filter(circunscripcion == "Total") %>% ungroup() %>% 
#   summarise(causas_iniciadas = sum(cantidad, na.rm = T))

inic <- tribble(~inic, 385)
# feb-sep
resoluciones_vs <- General_VS_Matriz_Volcado_2018 %>% 
  filter(organismo == "salpen0100pna") %>%
  summarise(sentencias = sum(s, na.rm = T), autos = sum(a, na.rm = T))
  
# oct-dic
resolucionesp <- resoluciones_pen(poblacion = sal_pen, start_date = "2018-10-01", 
                                  end_date = end_date, desagregacion_mensual = desagregacion_mensual) %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

resolucionespc <- resultadopc %>% 
  filter(circunscripcion == "Total") %>% ungroup() %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

resoluciones <- resolucionesp %>% 
  bind_rows(resolucionespc, resoluciones_vs) %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))

audip <- tribble(~audiencias_conciliadas, NA)
     
sal_pen <- bind_cols(entram, resoluciones, audip, inic) %>% 
  mutate(instancia_fuero = "Sala Proc.Constituc. y Penal STJ") %>% 
  select(instancia_fuero, everything())

# Sala Civil y Comercial -------------------------------------------------------
entram <- tribble(~causas_en_tramite, NA) # pendiente

inic <- iniciados_sal(poblacion = sal_civil, start_date = start_date,end_date = end_date) %>% 
  select(Total) %>% ungroup() %>%
  rename(causas_iniciadas = Total) %>% 
  summarise(causas_iniciadas = sum(causas_iniciadas, na.rm = T))

 
resoluciones <- resoluciones_cco(poblacion = sal_civil, start_date = start_date, end_date = end_date,
  operacion = "CADR3C", desagregacion_mensual = desagregacion_mensual) %>% 
  filter(mes == "Total") %>% ungroup() %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))
  
audip <- tribble(~audiencias_conciliadas, NA) # no aplica

sal_civil <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Sala Civil y Comercial STJ") %>% 
 select(instancia_fuero, everything())

# Sala Laboral-----------------------------------------------------------------
entram <- tribble(~causas_en_tramite, NA) # pendiente

inic <- iniciados_sal(poblacion = sal_lab, start_date = start_date,end_date = end_date) %>% 
  select(Total) %>% ungroup() %>%
  rename(causas_iniciadas = Total) %>% 
  summarise(causas_iniciadas = sum(causas_iniciadas, na.rm = T))

 
resoluciones <- resoluciones_lab(poblacion = sal_lab, start_date = start_date, end_date = end_date,
  operacion = "CADR3L", desagregacion_mensual = desagregacion_mensual) %>% 
  filter(mes == "Total") %>% ungroup() %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))
  
audip <- tribble(~audiencias_conciliadas, NA) # no aplica

sal_lab <- bind_cols(entram, resoluciones, audip, inic) %>% 
 mutate(instancia_fuero = "Sala del Trabajo STJ") %>% 
 select(instancia_fuero, everything())

# STJ Contenc-Administrativo----------------------------------------------------
entram <- tribble(~causas_en_tramite, NA) # pendiente

inic <- iniciados_stjcad(poblacion = stj_cad,  start_date = start_date, end_date = end_date) %>% 
  filter(tproc == "-") %>% ungroup() %>% 
  summarise(causas_iniciadas = sum(Cantidad_Iniciados, na.rm = T))

resoluciones_vs <- General_VS_Matriz_Volcado_2018 %>% 
  filter(str_detect(organismo, "stjcad")) %>%
  summarise(sentencias = sum(s, na.rm = T), autos = sum(a, na.rm = T))

resoluciones <- resoluciones_cad_stj(poblacion = stj_cad, operacion = "CADRCADS", start_date = start_date,
                                     end_date = end_date,  desagregacion_mensual = desagregacion_mensual) %>% 
  filter(mes == "Total") %>% ungroup() %>%
  summarise(sentencias = sum(sentencias), autos = sum(autos))

resoluciones <- resoluciones %>% 
  bind_rows(resoluciones_vs) %>% 
  summarise(sentencias = sum(sentencias), autos = sum(autos))
  
audip <- tribble(~audiencias_conciliadas, NA)

stj_cad <- bind_cols(entram, resoluciones, audip, inic) %>% 
  mutate(instancia_fuero = "Superior Trib.Justicia -Cont.Administrativo") %>% 
  select(instancia_fuero, everything())


# Mediación Civil y Mediación Penal
# Comunicación de dato elaborado: miércoles 24/07/2019 11:05, Cdor. José Manuel Rapuzzi <jrapuzzi@jusentrerios.gov.ar>.
mediacionc <- tribble(~instancia_fuero, ~causas_en_tramite, ~sentencias, ~autos, 
                      ~audiencias_conciliadas, ~causas_iniciadas, 
                      "Centro Medios Alternativos Resol.Conflict. Civil", 
                      NA, NA, NA, 2659, NA)

# Comunicación por Whatsapp de Fabricio Amateis Foto Nota presentada en AAA en fecha 13/03/19 por Juarez
# copia almacenada en casilla de correo apge@jusentrerios.gov.ar
mediacionp <- tribble(~instancia_fuero, ~causas_en_tramite, ~sentencias, ~autos, 
                      ~audiencias_conciliadas, ~causas_iniciadas, 
                      "Centro Medios Alternativos Resol.Conflict. Penal", 
                      NA, NA, NA, 2482, NA)

# Armado df_final
stjer <- bind_rows(mediacionc, mediacionp, jdos_cco, jdos_fam, jdos_ecq, jdos_paz1, jdos_paz23, 
                   jdos_lab, jdos_pna, jdos_pep, garantia, tjuicioya, cam_civil, cam_lab, cam_pen, cam_cad,
                   sal_pen, sal_civil, sal_lab, stj_cad) %>% 
  select(unidades_producción = instancia_fuero, 
         causas_judiciales_con_sentencia = sentencias, 
         causas_judiciales_con_conciliacion = audiencias_conciliadas,
         causas_en_tramite_ultimos_2años = causas_en_tramite, 
         sentencias_interlocutorias_resoluciones = autos) %>% 
  janitor::adorn_totals("row") %>% 
  mutate(causas_judiciales_con_conciliacion = ifelse(causas_judiciales_con_conciliacion ==0,
                                                     "sd", causas_judiciales_con_conciliacion)) %>%
  mutate(causas_en_tramite_ultimos_2años = ifelse(causas_en_tramite_ultimos_2años ==0, "sd", 
                                                  causas_en_tramite_ultimos_2años)) 

```

\pagebreak

# Introducción

En este informe comunico al Sr. Contador el producto bruto judicial del año 2018, bienes y servicios finales e intermedios, conforme artículos 2º Ley 5140 y 5º del Dec-Ley 1836/96.     

Destaco que en el período de referencia el Superior Tribunal de Justicia implementó progresivamene un nuevo sistema de estadística judicial conforme Plan de Modernización Estadística aprobado por Tribunal de Superintendencia mediante resolución 277/16 y Reglamento de Estadística vigente –Res.Tribunal Superintendencia Nº234/17. Por ello, la elaboración de los indicadores aquí detallados demandó la integración de distintas operaciones e instrumentos para manetener la serie histórica a los fines de análisis y comparabilidad de la información.     

Asimismo, conforme a las mejoras propuestas por el área a los indicadores de evaluación que se emplean en la estructura programática del presupuesto de esta jurisdicción, se detallan nuevos indicadores de producción asociados con producción intermedia ("autos" o sentencias interlocutorias) y productos terminales ("conciliaciones") según definición del Módulo Guía para Medición de la Producción, Tipos de Producto y su Cuantificación, p.1, Oficina de Presupuesto, MEHF, Gobierno de Entre Ríos.    

En la sección de Metodología explicitamos los alcances y características de la información presentada.    

Finalmente señalamos que para este informe se tuvo especialmente en vista la satisfacción de los siguientes objetivos de la técnica de programación presupuestaria:     

+ reflejar los objetivos de los planes de Gobierno Judicial,     
+ facilitar el conocimiento público de la clase y magnitud de la producción de bienes y servicios judiciales, y    
+ contribuir al control de ejecución presupuestaria.   

Lic. Sebastián Castillo

\pagebreak

# Bienes y Servicios Terminales e Intermedios STJER 2018


```{r}
stjer %>% 
  kable(caption = "Producto Bruto Judicial 2018", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```


\pagebreak


# Metodología

La codificación de la tabla principal incluye: **na** = no aplica y **sd** = sin dato.     

Los indicadores de producción incluidos en este informe y su definición conforme *MODULO GUIA: MEDICIÓN DE LA PRODUCCIÓN*, p.1, MEHF, son:

1. Bienes y Servicios Terminales 
  i) Finalizados o Acabados
    + **Causas Judiciales con Sentencia**: causas donde se dictó sentencia definitiva o resolución equiparables a ella que concluye el conflicto.    
    + **Causas Judiciales con Conciliación**: causas que finalizan con acuerdo conciliatorio entre partes celebrados en audiencia.      
  ii) En proceso
    + **Causas en trámite últimos dos años**: causas con al menos un movimiento/actuación en los últimos dos años.  
2. Bienes y Servicios Intermedios
  i) Directos 
    + **Sentencias interlocutorias / resoluciones**: sentencias no definitivas dictadas en la sustanciación de un proceso.    

## Observaciones sobre datos por indicador

+ *causas en trámite últimos dos años*: la unidad de medida es la causa judicial. Los órganos sin dato a los cuales aplica este indicador (órganos de instancias inferiores donde se inician y finalizan los procesos judiciales) tienen sus operación estadísticas en elaboración. La principal dificultad para tratar la información primaria de base está en la heterogeneidad de los registros históricos que se encuentran en los sistema judiciales (aspecto ampliamente documentado por el área). 

+ *causas judiciales con sentencia*: la unidad de medida es la causa judicial. Este indicador toma como observable de referencia a la sentencia definitiva o equiparable a ella en su efecto conclusivo del proceso. Este evento marca el cambio de estado de la causa de "causa en trámte" a "causa finalizada o terminada" sin perjuicio de ulteriores actos judiciales (ej. actos de ejecución de sentencia). El último estado de una causa está dado por su archivo referenciándose como "causa archivada", que implica (salvo excepciones formales) la no realización de actividades ulteriores de producción para el caso. Por otro lado, se destaca que en materia de estadística judicial generalmente los causas judiciales con sentencia desagregadas por instancia no deben sumarse para evitar doble conteo de causas, la excepción en este caso estaría justificada por la necesidad de evaluar el cumplimiento de objetivos por instancia, dadando tratamiento a las sentencias de cada una como productos individuales de sus respectivas operaciones. 

+ *causas judiciales con concilación*: la unidad de medida es la causa judicial. Este indicador tomo como observable de referencia al acuerdo conciliatorio arribado en audiencia. En el caso de audiencias en órganos judiciales las mismas corresponden a actividad de magistrados, en el cado de los audiencia en órganos de Mediación Penal la autoridad actuante es el mediador penal y en el caso de la Mediación Civil es el abogado mediador de conformidad a la reglamentación aplicable (cfr. Oficina de Medios Alternativos de Resolución de Conflictos y Centro de Medios Alternativos de Resolución de Conflictos ambos del STJER).    

+ *sentencias interlocutorias*: la unidad de medida es la resolución judicial. Este indicador tomo como observale las resolución no definitivas dictada en el marco de la sustanciación de un proceso judicial. Los tipo de resoluciónes constituyen un heterogéneo grupo de decisiones que varían por organismo, fuero e instancia. Sin perjuicio de ello, representan decisiones equiparables a sentencias interlocutorias (cfrme. CPCC y CPP), no son proveídos simples. 

## Rutinas Públicas de Procesamiento

Las rutinas de procesmiento con las cuales se obtuvieron estos indicadores puede consultarse en esta dirección a través de la carpeta "Modelos_Parametrizados": https://bitbucket.org/apgye/apgyeinformes/src/master/. Estas rutinas permiten la reproducibilidad de los datos. Ante cualquier consulta comuníquese a los teláfonos que figuran al pie. 


***
\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}
Director: Lic. Sebastian Castillo    
Equipo: Lic. Marcos Londero y Srta. Emilce Leones  
0343-4209405/410 – ints. 396 y 305  
http://www.jusentrerios.gov.ar/area-planificacion-y-estadisticas-stj/  
correos: apge@jusentrerios.gov.ar - estadistica@jusentrerios.gov.ar   
Laprida 250, Paraná, Entre Ríos  
