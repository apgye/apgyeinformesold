---
title: Duracion de Procesos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')


durac_años_prev <- tribble(
  ~año_de_sentencia, ~promedio_duracion, ~cantidad_casos_considerados,
  2012, 1277, 6,
  2013, 1792, 61, 
  2014, 1597, 68, 
  2015, 2089, 112, 
  2016, 1475, 86, 
  2017, 1297, 118, 
  2018, 1253, 39)
```


```{r}
# https://www.kaggle.com/studentar/data-cleaning-challenge-outliers-r
# Validación de datos: valores extremos y anomalías
# Regla1: se eliminan las duracones negativas
# Regla2: más de tres desviaciones estándar es un outlier
# Regla3: se eliminan NA
# http://r-statistics.co/Loess-Regression-With-R.html

duraciones_prim <- duracion_prim(cam_cad, "CADRCAD",  start_date = "2018-10-01", end_date = end_date) %>% 
  filter(finicio > make_date(2015,01,01)) %>% 
  select(-circunscripcion) %>% 
  arrange(organismo, desc(duracion)) %>% 
  mutate(caratula = word(caratula,1,1)) 
  

duraciones_cad <- duraciones_prim %>% 
  filter(str_detect(tipo_proceso, "CONTENCIOSO ADMINISTRATIVO")) %>% 
  group_by(organismo) %>% 
  summarise('duracion promedio (en dias corridos)' = round(mean(duracion, na.rm = T)), 
            'cantidad de casos considerados' = n())

#analisi_durac <- duracion_A(cam_cad, duraciones)

```


## Duracion de Procesos 

Las duraciones informadas a continuación se miden en días corridos desde el inicio de la causa registrada en los sistemas de gestión de causas (Lex-Doctor) hasta la fecha de sentencia definitiva ("S").     

`r if (d & t) '### Resumen Duración de Procesos'` 

```{r, cad1_cadur_t, eval = (d & t)}
duraciones_cad %>% 
  kable(caption = str_c("Promedio de Duración de Causas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  

```

`r if (d & t) '### Duración de Procesos - Períodos 2012 - 2018'`

```{r, cad1_cadur_t_hist, eval = (d & t)}
durac_años_prev %>% 
  kable(caption = "Promedio de Duración de Causas - Períodos 2012/2018 ", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  

```


\pagebreak

`r if (d & pc) '### Listado de Causas Resueltas por Sentencia con sus respectivas duraciones en días corridos'`

```{r, cad1_cadur_pc, eval= (d & pc)}
duraciones_prim %>% 
  kable(caption = "Listado de Causas y sus respectivas duraciones", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  column_spec(3, "2cm") %>% column_spec(4, "8cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()

```

