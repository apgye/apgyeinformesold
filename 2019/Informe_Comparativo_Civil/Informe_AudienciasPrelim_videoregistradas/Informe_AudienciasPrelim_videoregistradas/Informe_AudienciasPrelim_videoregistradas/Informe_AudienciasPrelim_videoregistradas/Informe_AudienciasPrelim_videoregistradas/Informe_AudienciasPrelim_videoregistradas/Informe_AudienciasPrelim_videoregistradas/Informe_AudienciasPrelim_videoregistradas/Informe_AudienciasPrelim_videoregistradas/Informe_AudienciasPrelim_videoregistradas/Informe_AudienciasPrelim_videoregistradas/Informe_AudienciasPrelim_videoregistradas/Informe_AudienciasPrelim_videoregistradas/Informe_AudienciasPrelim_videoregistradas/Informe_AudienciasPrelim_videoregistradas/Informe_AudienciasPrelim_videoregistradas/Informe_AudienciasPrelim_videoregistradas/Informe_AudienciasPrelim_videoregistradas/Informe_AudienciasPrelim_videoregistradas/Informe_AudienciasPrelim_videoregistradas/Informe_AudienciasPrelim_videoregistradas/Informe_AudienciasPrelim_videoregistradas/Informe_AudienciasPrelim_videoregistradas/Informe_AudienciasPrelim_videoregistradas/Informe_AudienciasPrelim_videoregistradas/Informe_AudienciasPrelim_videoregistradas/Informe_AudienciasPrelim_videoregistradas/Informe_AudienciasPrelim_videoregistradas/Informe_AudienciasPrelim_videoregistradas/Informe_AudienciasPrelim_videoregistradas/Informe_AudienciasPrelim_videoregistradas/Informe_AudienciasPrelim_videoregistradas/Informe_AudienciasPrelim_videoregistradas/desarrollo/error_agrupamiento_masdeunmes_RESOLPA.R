#el problema está en el acumulado bimestral
start_date = "2019-02-01"
end_date = "2019-04-01"

# Revienta para dos meses
resolpa <- DB_PROD() %>% apgyeDSL::apgyeTableData('RESOLPA') %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% poblacion_infte$organismo) 

if (desagregacion_mensual) {
  resolpa <- resolpa %>%  group_by(iep, data_interval_start, data_interval_end)
} else {
  resolpa <- resolpa %>% group_by(iep)
}

resolpa <- resolpa %>%
  process(RESOLPA)