---
title: Causas Iniciadas  
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}




```

## Causas Iniciadas  `r getDataIntervalStr(start_date, end_date)`

```{r, cco1_ci_t, eval = (ci & t)}
resultado$inic %>% 
  iniciados_cco_comparativo(cfecha = desagregacion_mensual) %>% 
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```


```{r, cco1_ci_g, eval = (ci & g), echo=FALSE}
resultado$inic %>% 
  iniciados_cco_xg() %>%
  inic_cco_graf_xg()

```

```{r, cco1_ci_st, eval = (ci & st)}
resultado$inic_cco_pc %>% 
  tsci_g(finicio, org_facet = org_facet)

```


```{r, cco1_ci_pc, eval = (ci & pc)}
resultado$inic_cco_pc %>% 
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(4, "6cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```


<!--  Codigo Ad-Hoc------------------------------------------------------- -->


<!-- ## Tabla Desagregada por Grupos Específicos `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r } -->
<!-- iniciados_cco(poblacion = jdos_cco, -->
<!--               start_date = start_date, -->
<!--               end_date = end_date) %>%  -->
<!--   filter(str_detect(grupo_proceso, "MONITORIO|EJECUCIONES")) %>%  -->
<!--   group_by(circunscripcion, organismo, grupo_proceso, tipo_proceso) %>%  -->
<!--   summarise(cantidad = n()) %>% ungroup() %>%  -->
<!--   group_by(circunscripcion, organismo, grupo_proceso) %>%  -->
<!--   do(janitor::adorn_totals(.)) %>%  -->
<!--   outputTable(caption = "Causas Iniciadas Agrupadas") %>%  -->
<!--   # kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>% -->
<!--   # kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--   #               full_width = F, font_size = 10)  %>% -->
<!--   column_spec(4, "5cm") %>% -->
<!--   # row_spec(0, angle = 90) %>%  -->
<!--   landscape() -->

<!-- ``` -->



