
df <- apgyeJusEROrganization::listar_organismos()

estruc_logic_params <- df  %>% 
  group_by(fuero, tipo) %>% 
  summarise(iniciados = NA,
            archivados = NA,
            resueltos = NA,
            audiencias = NA,
            centramite = NA,
            duracion = NA) %>% 
  filter(str_detect(fuero, "Civil|ConAdm|Laboral|Penal")) %>%
  tidyr::gather("indicador", "disponible", -fuero, -tipo) %>% 
  arrange(fuero, tipo) %>% 
  mutate(tabla = NA, 
         tabla_desagregada = NA, 
         grafico = NA, 
         serie_temporal = NA,
         serie_temporal_org_facet = NA,
         primaria_consolidada = NA)

write.table(estruc_logic_params, "estruc_logic_params.csv", col.names = T, row.names = F, sep = ";")
