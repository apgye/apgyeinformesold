---
title: Resolucioens por Tipo 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}

res_list <- resoluciones_cad1_xtres(cam_cad,
                        operacion = "CADRCAD",
                        start_date = start_date,
                        end_date = end_date)


```

## Resoluciones por Tipo `r getDataIntervalStr(start_date, end_date)`

```{r, cad1_resxtres_t, eval= (s & t)}
res_list$res_xtipo %>%
  filter(as == "S") %>% 
  select(-4, -8, -9, -10, -11, -12, -14, -15) %>%
  outputTable(caption = "Sentencias por Tipo") %>%
  row_spec(0, angle = 90)
```

\pagebreak

`r if (s & g) '## Tipos de Sentencias Dictadas'`

```{r, cad1_resxtres_t_s, eval= (s & g)}
res_list$res_xtipo_S
```

\pagebreak

`r if (s & t) '## Resoluciones por Objeto de Demanda '`

```{r, cad1_resxtres_t_od, eval= (s & t)}
res_list$res_xobjetodem %>%
  select(-circunscripcion) %>%
  outputTable(caption = "Resoluciones por Objeto de Demanda") %>%
  row_spec(0, angle = 90)
```

\pagebreak

`r if (s & g) '## Objetos de Demanda'`

```{r, cad1_resxtres_graf, eval= (s & g)}
res_list$res_xobjetodem_g
```

\pagebreak

`r if (s & t) '## Resoluciones por Demandado y Resultado '`

```{r, cad1_resxtres_t_dr, eval= (s & t)}
res_list$tdemo_xtres %>%
  select(-circunscripcion) %>% 
  outputTable(caption = "Demandado y Resultado") %>%
  #column_spec(1, "3cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```

\pagebreak

`r if (s & t) '## Resoluciones en Recursos '`

```{r, cad1_resxtres_t_rec, eval= (s & t)}
res_list$recurso %>%
  select(-circunscripcion, -Sin_dato, -Total) %>% 
  outputTable(caption = "Resoluciones en Recursos") %>%
  row_spec(0, angle = 90)
```

\pagebreak

`r if (s & t) '## Sentencias Dictadas y Recursos'`

```{r, cad1_resxtres_t_srec, eval= (s & t)}
res_list$sentencias_recurso %>%
  outputTable(caption = "Sentencias y Recursos") %>%
  row_spec(0, angle = 90)
```




