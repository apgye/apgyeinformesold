---
title: Causas En Trámite
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  html_document:
    df_print: paged
  pdf_document:
    latex_engine: xelatex
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

\fontsize{14}{22}
\selectfont

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Causas en Trámite Agrupadas por Intervalos según último movimiento procesal informado

La siguiente tabla presenta las causas en trámite por organismo, agrupándolas por intervalos. 

+ **"mayor_10_años"**: causas cuyo último movimiento procesal informado al área es superior a los 10 años contados desde la fecha del último informe.
+ **"entre_2y10_años"**: causas cuyo último movimiento procesal informado al área es menor a los 10 años y superior a los 2 año contados desde la fecha del último informe.
+ **"menor_2_años"**: causas cuyo último movimiento procesal informado al área se encuentra dentro de los dos años. 


```{r}
entramite(poblacion = jdos_cco, operacion_inic = "CINC1C") %>% 
  enTramitePorGrupoyAño() %>%
  outputTable(caption = "Causas en Trámite Totales",
                row_group_label_position = "identity")

```
