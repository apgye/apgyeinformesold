---
title: Causas en Trámite
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


## Causas en trámite `r getDataIntervalStr(start_date, end_date)`


```{r, cco1_cetal_t_intro, eval= (ce & t),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("En esta tabla se muestran las causas en trámite totales y las causas en trámite activas que tenía un organismo al último día habil del año inmediato anterio. Las causas en trámite activas son aquellas que presetaron al menos un movimiento procesal en los últimos dos años.")
```


```{r, cco1_cetal_t, eval= (ce & t)}
entramite(poblacion = jdos_cco, operacion_inic = "CINC1C") %>%
  enTramite_gral() %>%
  outputTable(caption = "Causas en Trámite Totales y Causas en Trámite Activas)",
                row_group_label_position = "identity")

```

\pagebreak

`r if (ce & td) '## Causas en Trámite Agrupadas por Intervalos según último movimiento procesal informado'`

```{r, cco1_cetal_td_intro, eval= (ce & td),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("La siguiente tabla presenta las causas en trámite por organismo, agrupándolas por intervalos.   \n   \n1-mayor_10_años: causas cuyo último movimiento procesal informado al área es superior a los 10 años contados desde la fecha del último informe.   \n2-entre_2y10_años:causas cuyo último movimiento procesal informado al área es menor a los 10 años y superior a los 2 año contados desde la fecha del último informe.   \n3-menor_2_años:causas cuyo último movimiento procesal informado al área se encuentra dentro de los dos años.")
```

```{r, cco1_cetal_td, eval= (ce & td)}
entramite(poblacion = jdos_cco, operacion_inic = "CINC1C") %>% 
  enTramitePorGrupoyAño() %>%
  outputTable(caption = "Causas en Trámite Totales",
                row_group_label_position = "identity") %>% 
  landscape()

```


