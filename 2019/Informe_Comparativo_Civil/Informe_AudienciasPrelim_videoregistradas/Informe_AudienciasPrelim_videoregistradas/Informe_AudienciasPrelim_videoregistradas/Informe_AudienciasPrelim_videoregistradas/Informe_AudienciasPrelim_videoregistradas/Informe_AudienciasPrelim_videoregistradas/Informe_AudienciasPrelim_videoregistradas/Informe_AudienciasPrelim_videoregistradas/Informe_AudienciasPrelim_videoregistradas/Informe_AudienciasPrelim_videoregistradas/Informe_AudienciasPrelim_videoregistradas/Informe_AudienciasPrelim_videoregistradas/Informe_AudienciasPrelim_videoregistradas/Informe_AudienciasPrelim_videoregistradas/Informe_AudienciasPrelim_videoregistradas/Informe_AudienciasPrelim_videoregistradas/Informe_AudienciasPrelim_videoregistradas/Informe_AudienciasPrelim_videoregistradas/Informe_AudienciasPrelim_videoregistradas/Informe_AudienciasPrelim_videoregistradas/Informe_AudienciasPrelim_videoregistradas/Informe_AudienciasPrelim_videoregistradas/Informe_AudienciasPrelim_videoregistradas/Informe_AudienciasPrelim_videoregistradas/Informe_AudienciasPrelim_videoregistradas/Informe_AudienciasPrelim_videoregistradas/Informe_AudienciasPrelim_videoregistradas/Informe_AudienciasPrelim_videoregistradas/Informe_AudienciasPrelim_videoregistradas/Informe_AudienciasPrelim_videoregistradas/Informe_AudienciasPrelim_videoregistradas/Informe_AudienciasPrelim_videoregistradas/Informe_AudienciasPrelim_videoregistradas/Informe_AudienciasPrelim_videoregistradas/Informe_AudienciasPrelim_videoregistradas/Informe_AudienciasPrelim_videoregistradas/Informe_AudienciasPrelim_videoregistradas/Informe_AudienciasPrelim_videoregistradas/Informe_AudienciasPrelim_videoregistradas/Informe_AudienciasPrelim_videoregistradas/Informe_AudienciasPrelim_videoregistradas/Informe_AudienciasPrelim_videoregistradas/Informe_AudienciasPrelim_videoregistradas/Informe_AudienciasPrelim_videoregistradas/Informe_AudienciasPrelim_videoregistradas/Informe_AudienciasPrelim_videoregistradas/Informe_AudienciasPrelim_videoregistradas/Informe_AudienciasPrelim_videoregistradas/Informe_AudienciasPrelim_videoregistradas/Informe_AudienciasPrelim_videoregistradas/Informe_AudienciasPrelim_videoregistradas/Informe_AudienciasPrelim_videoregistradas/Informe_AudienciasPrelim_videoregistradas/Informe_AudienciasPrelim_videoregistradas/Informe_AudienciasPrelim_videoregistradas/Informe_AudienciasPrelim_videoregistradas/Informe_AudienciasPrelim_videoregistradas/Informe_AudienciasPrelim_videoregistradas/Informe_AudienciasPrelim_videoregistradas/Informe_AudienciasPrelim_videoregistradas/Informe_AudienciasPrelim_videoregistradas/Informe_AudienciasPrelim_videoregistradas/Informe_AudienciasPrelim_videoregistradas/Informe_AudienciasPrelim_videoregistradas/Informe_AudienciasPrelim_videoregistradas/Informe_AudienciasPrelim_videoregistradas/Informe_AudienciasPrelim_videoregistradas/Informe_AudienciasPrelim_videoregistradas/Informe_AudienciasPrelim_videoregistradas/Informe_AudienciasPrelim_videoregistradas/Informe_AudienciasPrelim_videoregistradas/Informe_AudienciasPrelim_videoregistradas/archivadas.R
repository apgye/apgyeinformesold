# FUNCIONES--------------------------------------------------------------------

archivadas <- function(poblacion, start_date="2018-07-01", end_date = "2018-12-01") {
 
  archivadas <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData("CARCH") %>% 
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!poblacion$organismo) %>% 
    mutate(finicio = dmy(finicio)) %>%
    select(iep, nro, caratula, tproc, finicio)  %>% 
    collect() 
  
  archivadas <- resolverconvertidos(archivadas)
  
  archivadas <- archivadas %>% 
    distinct(iep, nro, .keep_all = TRUE) %>% 
    mutate(ano_inicio_causa = lubridate::year(finicio)) %>% 
    group_by(iep, ano_inicio_causa) %>% 
    summarise(cantidad_causas_archivadas = n()) %>% 
    ungroup() %>% 
    left_join(poblacion %>% 
                select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
    select(circunscripcion, organismo, ano_inicio_causa, everything(), -iep) %>% 
    arrange(circunscripcion, organismo, ano_inicio_causa)
  
  if (any(poblacion$organismo == "jdocco0000vpa")) {
   
    archivadas8 <- DB_PROD() %>%
      apgyeDSL::apgyeTableData("CARCH_v2_L8") %>%
      filter(iep == "jdocco0000vpa") %>%
      mutate(finicio = dmy(finicio)) %>%
      select(iep, nro, caratula, tproc, finicio)  %>% 
      collect() %>% 
      distinct(iep, nro, .keep_all = TRUE) %>% 
      mutate(ano_inicio_causa = lubridate::year(finicio)) %>% 
      group_by(iep, ano_inicio_causa) %>% 
      summarise(cantidad_causas_archivadas = n()) %>% 
      ungroup() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      select(circunscripcion, organismo, ano_inicio_causa, everything(), -iep) %>% 
      arrange(circunscripcion, organismo, ano_inicio_causa)
  }
  
  archivadas <- bind_rows(archivadas, if(exists("archivadas8")) archivadas8) 
    
  archivadas
}


