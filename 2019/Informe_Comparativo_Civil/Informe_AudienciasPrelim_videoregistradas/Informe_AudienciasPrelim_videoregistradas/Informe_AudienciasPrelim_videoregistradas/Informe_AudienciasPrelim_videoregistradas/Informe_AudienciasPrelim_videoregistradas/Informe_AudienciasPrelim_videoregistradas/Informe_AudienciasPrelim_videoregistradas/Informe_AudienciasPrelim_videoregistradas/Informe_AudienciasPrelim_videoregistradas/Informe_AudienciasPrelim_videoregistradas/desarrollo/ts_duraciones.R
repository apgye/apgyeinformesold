

interanual <- interanual %>% 
  mutate(año = as.Date(str_c(año,"-12-31"))) 
  #mutate(tproc = str_replace_all(tproc, "ORDINARIO ", ""))
  



#--------------------

poblacion <- jdos_cco_solo
start_date <- "2018-08-01"
end_date <- "2019-08-01"

resultado <- DB_PROD() %>% 
  apgyeTableData("CADR1C") %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% !!poblacion$organismo) %>% 
  resolverconvertidos() %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% 
  process() %>%
  .$result %>%
  ungroup() 
  

library(outliers)

library(viridis)

library(ggalt)


duraciones <- duracion_prim(jdos_cco_solo, "CADR1C", start_date = start_date, end_date = end_date) 
   

# Validación de datos: valores extremos y anomalías
# Regla1: se eliminan las duracones negativas
# Regla2: más de tres desviaciones estándar es un outlier
# Regla3: se eliminan NA
#http://r-statistics.co/Loess-Regression-With-R.html



duraciones_tidy <- duraciones %>% 
  filter(duracion > 0) %>% 
  group_by(tipo_proceso) %>% 
  mutate(outlier_score = scores(duracion)) %>% 
  mutate(is_outlier = outlier_score > 3 | outlier_score < -3) %>% 
  filter(!is.na(is_outlier)) %>% 
  ungroup()

duraciones_tidy_subselec <- duraciones_tidy %>% 
  durac_subselec("civil") %>% rename(tipo_proceso = tproc) %>% 
  #filter(!is_outlier) %>% 
  add_count(tipo_proceso, name = "temp") %>%
  mutate(temp = dense_rank(desc(temp))) %>%
  filter(temp %in% 1:10) %>%
  select(-temp)

g_dts_outliers <- duraciones_tidy_subselec %>% 
  ggplot(aes(x = tipo_proceso, y = duracion)) +
  geom_boxplot() +
  coord_flip() +
  facet_wrap(~is_outlier) +
  labs(title = "Exclusión de valores anómalos en duraciones",  
       subtitle = "Valor anómalo: > 3 desviaciones estandar", y = "tipos_proceso", x = "", 
       caption = "APGE-STJER")

# Gráficos---------------------------------------------------------------

g_general <- duraciones_tidy_subselec %>%
  filter(!is_outlier) %>%
  mutate(año = year(fres)) %>%
  group_by(año) %>% 
  summarise(mediana_duracion = median(duracion, na.rm = T)) %>% 
  ggplot(aes(x = año, y = mediana_duracion, fill = as.factor(año))) + 
  geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
  scale_fill_brewer(palette="Set1") +
  geom_text(aes(label = mediana_duracion), position = position_dodge(0.9),
            vjust = -0.5, size = 5) +
  theme_minimal() +
  theme(axis.text.x = element_text(hjust = 1,size=14,face="bold"), axis.text.y = element_blank(),
        legend.position = "none", axis.title=element_text(size=14,face="bold")) +
  labs(title = "Duración General de Procesos de Conocimiento: Evolución Interanual",  
       subtitle = "Mediana calculada en días corridos de inicio hasta sentencia", y = "Duraciones", x = "", 
       caption = "APGE-STJER") 
  

duraciones_ts_tabla <- duraciones_tidy_subselec %>%
  filter(!is_outlier) %>%
  mutate(año = year(fres)) %>%
  group_by(tipo_proceso, año) %>% 
  summarise(mediana_duracion = round(mean(duracion, na.rm = T)), 
            cantidad_casos = n())


duracion_ts_tabla_xaño <- duraciones_ts_tabla %>% 
  select(-cantidad_casos) %>% 
  tidyr::spread(año, mediana_duracion) %>% 
  janitor::clean_names() %>%
  rowwise() %>% 
  mutate(variacion_ult2 = x2019-x2018) %>% 
  rowwise() 

names(duracion_ts_tabla_xaño) <-  str_remove_all(names(duracion_ts_tabla_xaño), "x")


g_duraciones_ts <- duraciones_tidy_subselec %>%
  filter(!is_outlier) %>%
  mutate(año = year(fres)) %>%
  group_by(tipo_proceso, año) %>% 
  summarise(mediana_duracion = round(mean(duracion, na.rm = T))) %>% 
  tidyr::spread(año, mediana_duracion) %>% select(1,3,4) %>% 
  janitor::clean_names() %>% 
  rowwise() %>% mutate(variacion_interanual = x2019-x2018) %>% rowwise() %>% 
  mutate(variacion = ifelse(variacion_interanual < 0, "disminucion", "aumento")) %>%
  arrange(desc(variacion_interanual)) %>% 
  ggplot(aes(x = reorder(tipo_proceso, -variacion_interanual), y = variacion_interanual)) +
  geom_bar(aes(fill = variacion), stat = 'identity') +  # color by class
  geom_text(aes(label = variacion_interanual), position = position_dodge(0.9),
            vjust = 1, hjust = 1, size = 5) +
  coord_flip() +  # horizontal bars
  theme_minimal() +
  geom_text(aes(y = 0, label = tipo_proceso, hjust = as.numeric(variacion_interanual > 0))) +  # label text based on value
  scale_fill_manual("Variación:", values = c("disminucion" = "blue","aumento" = "red")) +
  theme(axis.text.y = element_blank(),axis.text.x = element_text(hjust = 1,size=8),
        legend.position='top') +
  labs(title = "Variación Interanual de la Duración de Procesos",  
       subtitle = "Mediana en días corridos", y = "", x = "", 
       caption = "APGE-STJER") 


g_duraxtipoxaño <- duraciones_tidy_subselec %>% 
  filter(!is_outlier) %>%
  mutate(año = year(fres)) %>%
  group_by(tipo_proceso, año) %>%
  summarise(mediana_duracion = round(mean(duracion, na.rm = T))) %>%
  mutate(año = as.factor(año)) %>% 
  # rowwise() %>% mutate(tendencia = x2018-x2019 > 0) %>% rowwise() %>% 
  # mutate(tendencia = ifelse(tendencia, "disminucion", "aumento")) %>%
  ggplot(aes(x = tipo_proceso, y = mediana_duracion, fill = año)) + 
  geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
  geom_text(aes(label = mediana_duracion), position = position_dodge(0.9),
               vjust = -1, size = 3) +
  scale_fill_brewer(palette="Set1") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1), axis.text.y = element_blank(), 
         axis.title.x=element_blank(), axis.title.y=element_blank()) +
  labs(title = "Duraciones de proceso por Tipo y Año",  
       subtitle = "Mediana calculada en días corridos", y = "Duraciones", x = "", 
       caption = "APGE-STJER") 


# Graf Series Temporales

gts_duracxproc <- duraciones_tidy_subselec %>% 
  filter(!is_outlier) %>%
  ggplot(aes(x = fres, y = duracion, colour = tipo_proceso)) +
  geom_smooth(method = "auto", se = F) +
  theme_minimal() +
  scale_x_date(date_breaks = "2 month", date_labels = "%b-%y" ) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 12), 
      strip.text = element_text(size = 18)) +
  scale_color_viridis(discrete = TRUE, option = "D") +
  labs(title = "Evolución de la Duración de Procesos",  
       subtitle = "Línea de tendencia calculada por método de Regresion Local", 
       y = "Duraciones", x = "", caption = "APGE-STJER") 

# facetado

gts_duracxproc_facet <- duraciones_tidy_subselec %>% 
  filter(!is_outlier) %>%
  ggplot(aes(x = fres, y = duracion, colour = tipo_proceso)) +
  geom_smooth(method = "auto", se = F) +
  theme_minimal() +
  scale_x_date(date_breaks = "2 month", date_labels = "%b-%y" ) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 12), 
        strip.text = element_text(size = 18)) +
  scale_color_viridis(discrete = TRUE, option = "D") +
  facet_wrap(~ tipo_proceso, scales = "free_y") +
  theme(strip.background = element_blank(), strip.text.x = element_text(size = 8)) +
  labs(title = "Evolución de la Duración de Procesos",  
       subtitle = "Línea de tendencia calculada por método de Regresion Local", 
       y = "Duraciones", x = "", caption = "APGE-STJER") 



# dumbell graf

# duraciones_tidy_subselec %>% 
#   filter(!is_outlier) %>%
#   mutate(año = year(fres)) %>% 
#   group_by(tipo_proceso, año) %>% 
#   summarise(mediana_duracion = round(mean(duracion, na.rm = T))) %>% 
#   tidyr::spread(año, mediana_duracion) %>% select(1,3,4) %>% 
#   janitor::clean_names() %>% 
#   rowwise() %>% mutate(tendencia = x2018-x2019 > 0) %>% rowwise() %>% 
#   mutate(tendencia = ifelse(tendencia, "Disminución de Duración", "Aumento de Duración")) %>% 
#   ggplot(aes(y = reorder(tipo_proceso, x2019), x = x2018, xend = x2019)) +  
#   geom_dumbbell(size = 1.2, size_x = 2, size_xend = 4, colour = "grey", 
#                 colour_x = "blue", colour_xend = "red") +
#   theme_minimal() +
#   labs(title = "Comparación de duración de procesos años interanual",
#      subtitle = "2018 (azul) - 2019 (rojo)",  x = "Duraciones de Proceso", y = "") #+ 
#   #facet_wrap(~tendencia)

