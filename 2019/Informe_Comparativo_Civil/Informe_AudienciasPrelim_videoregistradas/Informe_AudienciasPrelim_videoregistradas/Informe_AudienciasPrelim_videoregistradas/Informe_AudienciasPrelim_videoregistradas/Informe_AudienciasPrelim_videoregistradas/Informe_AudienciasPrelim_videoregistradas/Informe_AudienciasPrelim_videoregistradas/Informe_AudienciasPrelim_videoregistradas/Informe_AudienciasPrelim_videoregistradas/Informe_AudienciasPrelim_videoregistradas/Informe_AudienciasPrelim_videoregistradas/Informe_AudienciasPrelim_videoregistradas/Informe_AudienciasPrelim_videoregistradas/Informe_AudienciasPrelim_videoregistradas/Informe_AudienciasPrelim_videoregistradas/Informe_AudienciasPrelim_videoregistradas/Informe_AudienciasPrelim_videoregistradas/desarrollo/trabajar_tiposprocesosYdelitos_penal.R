# N grams
# https://www.tidytextmining.com/ngrams.html#tokenizing-by-n-gram
# https://bergvca.github.io/2017/10/14/super-fast-string-matching.html adaptar ngram de caracteres a palabras

library(tidytext)
library(RCurl)
library(dplyr)
library(quanteda)
library(tm)
library(SnowballC)

setwd("~/apgyeinformes/2019/Modelo_Parametrizado/data")
download.file("http://datos.jus.gob.ar/dataset/d4a7a48d-d5c5-48e3-b820-0bc308d57e3c/resource/893269e4-e419-4683-8f50-b135a8850ae8/download/codificacion-delitos-codigo-penal-argentino-2018-08-03.csv", destfile="delitosCPN.csv",method="libcurl")
delitosCPN <- readr::read_csv("~/apgyeinformes/2019/Modelo_Parametrizado/data/delitosCPN.csv")

delitosCPN <- delitosCPN %>% 
  mutate(delito_descripcion = tolower(delito_descripcion))


inic <- DB_PROD() %>% 
  apgyeTableData('CIGTIA') %>% 
  apgyeDSL::interval("2018-07-01", "2019-03-01") %>% 
  mutate(finicio = dmy(finicio), fhecho = dmy(fhecho)) %>%
  filter(iep %in% oga$organismo) %>% 
  collect() 

tproc <- apgyeOperationsJusER::Tipo_procesos

inic_xtproc <- inic %>% 
  filter(!is.na(tproc)) %>% 
  group_by(tproc) %>% 
  summarise(cantidad_casos = n()) %>% 
  mutate(index = row_number())

# separar tentativa excluyéndola de delitos
inic_xtproc <- inic_xtproc %>% 
  mutate(tentativa = str_detect(tproc, " EN GRADO DE TENTATIVA| TENTATIVA|TENTATIVA")) %>% 
  mutate(tproc = str_remove_all(tproc, " EN GRADO DE TENTATIVA|EN GRADO DE TENTATIVA")) %>% 
  mutate(tproc = str_remove_all(tproc, " TENTATIVA")) %>% 
  mutate(tproc = str_remove_all(tproc, "TENTATIVA")) %>% 
  mutate(tproc = ifelse(tproc == "", NA, tproc))

# agregar/normalizar otros
# inic_xtproc <- inic_xtproc %>% 
#   mutate(tproc = ifelse(str_detect(tproc, "13.944|13944"), "INFRACCION LEY 13944", tproc),
#        tproc = ifelse(str_detect(tproc, "14.346|14346"), "INFRACCION LEY 14346", tproc),
#        tproc = ifelse(str_detect(tproc, "20.429|20429"), "INFRACCION LEY 20429", tproc), 
#        tproc = ifelse(str_detect(tproc, "22.362|22362"), "INFRACCION LEY 22362", tproc))

# str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTIT.",
# str_detect(tproc, "^ADMINISTRATIVO|^ARCHIVO|^AUTORIZACION|CONCURSO CERRADO") ~ "noproceso",
# str_detect(tproc, "^CONCURSOS|^DESIGNACION|^EFECTOS SECUESTRADOS|^EXHORTO|^INFORMES") ~ "noproceso",
# str_detect(tproc, "^LEGAJO|^OFICIO|^PERSONAL|^SENTENCIA|^SU PRESENTACION|^SU SITUACION") ~ "noproceso",
# str_detect(tproc, "^EJECUCION DE SENTENCIA|^EJECUTIVO|FALLECIMIENTO|IMPEDIMENTO DE CONTACTO|^INCIDENTE|INSTRUCCION") ~ "desclasificado",
# str_detect(tproc, "^LIBRAMIENTO DE CHEQUE|^MEDIDA CAUTELAR|^OTRO|^SOLICITUD") ~ "desclasificado",
# str_detect(tproc, "^VARIOS|^TESTIMONIOS|Y OTROS") ~ "desclasificado",
# str_detect(tproc, "^JURISPRUDENCIA|LEY") ~ "error",

# separar por grupos de cantidad de palabras
inic_xtproc <- inic_xtproc %>% 
  mutate(cantidad_palabras = str_count(tproc, '\\w+'))

#########################################################################
# Caso extremo de acumulacion de tipos
inic_xtproc172 <- inic_xtproc %>% filter(index == 172) %>% .$tproc
inic_xtproc172

ptproc <- tproc %>% 
  filter(materia == "penal") %>% 
  distinct(tipo_de_procesos, .keep_all = T)

<<<<<<< HEAD
<<<<<<< HEAD
# limpieza de vectores

# cleantproc <- function(df, names_var) {
# 
#   names_var = rlang::enexpr(names_var)
# 
#   df <- df %>%
#     mutate(names_var = !! names_var) %>%
#     mutate(names_var = str_trim(names_var),
#            names_var = toupper(names_var),
#            names_var = str_remove_all(names_var, "[[:punct:]]"))
# 
#   string <- tokens(df$names_var)
# 
#   #string <- tokens_remove(string, stopwords::data_stopwords_snowball$es)
# 
#   procesos <- list()
# 
#   for (i in seq_along(string)){
#      procesos[[i]] <- str_c(unlist(string[[i]], use.names=FALSE), collapse = " ")
#   }
# 
#   df$names_var <- as.character(procesos)
# 
#  df
# 
# }
# ptproc <- cleantproc(ptproc, tipo_de_procesos) 
# 
# inic_xtproc <- cleantproc(inic_xtproc, tproc) 


vecstring = ptproc$tipo_de_procesos[resul]

# implementacion
# listresul <- list()
# 
# for (i in seq_along(inic_xtproc$tproc)) {
#  
#   listresul[[i]] <- ptproc$names_var[str_detect(inic_xtproc$names_var[[i]], ptproc$names_var)]
#  
# }
# 
# inic_xtproc <- inic_xtproc %>% 
#   mutate(clasif = listresul)
#   
# inic_xtproc_resul <- inic_xtproc %>% select(tproc, cantidad_palabras, names_var, clasif)


# eliminar doble mach ej Robo Robo Agravado






##############################################################################################################
vecstring 
str_detect("DAÑO", "DAÑOS")
str_detect("DAÑOS", "DAÑO") # TRUE
length(vecstring)

sum(str_detect(vecstring, vecstring[4])) > 1 

vecstring = listresul[[45]]

=======
# prueba: funcionó
resul <- vector()
resul <- str_detect(inic_xtproc172, ptproc$tipo_de_procesos)

# implementacion
listresul <- list()

for (i in seq_along(inic_xtproc$tproc)) {
 
  listresul[[i]] <- ptproc$tipo_de_procesos[str_detect(inic_xtproc$tproc[[i]], ptproc$tipo_de_procesos)]
 
}

inic_xtproc <- inic_xtproc %>% 
  mutate(clasif = listresul)
  
str_detect("LESIONES", "LESIONES LEVES" )
>>>>>>> parent of d74b882... sigo trabajando procesos penales

vecstring

contenido <- vector()
for (i in seq_along(vecstring)) {
  
  if (sum(str_detect(vecstring, vecstring[i])) > 1) {
    contenido[i] <- TRUE
  } else {
    contenido[i] <- FALSE
  }
  contenido
}

length(contenido)

vecstring[!contenido]  




# Text Normalization
#https://kenbenoit.net/pdfs/text_analysis_in_R.pdf
#https://www.youtube.com/watch?v=IFhDlHKRHno&list=PL8eNk_zTBST8olxIRFoo0YeXxEOkYdoxi&index=4
inic_xtproc_matrix <- inic_xtproc %>% 
  mutate(tproc = str_trim(tproc)) %>% 
  mutate(tproc = removePunctuation(tproc))

# Tokenization
string <- tokens(inic_xtproc_matrix$tproc)
string[658]
# tokens_tolower
string <- tokens_tolower(string)
# tokens_wordstem : buscar spanish steemer
# string <- tokens_wordstem(string, language = "es")
# remove stopwords
string <- tokens_remove(string, stopwords::data_stopwords_snowball$es)


# create a DFM matrix
matriz <- dfm(string)
# weighting
matriz_w <- dfm_tfidf(matriz)

# Diccionario
myDict <- dictionary(list(as = c("abuso", "sexual", "acceso", "carnal", "agravado", "pluralidad", "autores")))
  
matrizfinal <- dfm_lookup(matriz, myDict, nomatch = "_unmatch")



# match perfecto = 36/668
inic_xtproc <- inic_xtproc %>% 
  left_join(delitosCPN, by = c("tproc" = "delito_descripcion"))







########################
table(inic_xtproc$cantidad_palabras)

# Pruebas de tokenizacion
unnest_tokens(inic_xtproc[12, ], gram2, tproc, token = "ngrams", n = 3)


