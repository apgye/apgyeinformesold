
audifam_prim <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02"){
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIF") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!poblacion$organismo) %>%
    filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
    mutate(fea = dmy(fea)) %>% mutate(audvid = as.integer(audvid)) %>%
    filter(fea >= data_interval_start, fea < data_interval_end) %>%
    select(-data_interval_start, -data_interval_end, -justiables) %>%
    collect() 
  
  resultado <- resolverconvertidos(resultado)
  
  resultado <- resultado %>%
    mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
    mutate(duracm = str_trim(duracm)) %>%
    mutate(mat = str_sub(toupper(mat), 1,1)) %>%
    mutate(ra = str_sub(toupper(ra), 1,1)) %>%
    mutate(ta = str_replace_all(ta, "[:blank:]", "")) %>%
    mutate(ta = str_replace_all(ta, "[:alpha:]|[:punct:]", "#")) %>%
    mutate(ta = str_trim(ta)) %>% 
    mutate(esta = str_remove_all(esta, "[:alpha:]|[:punct:]")) %>%
    mutate(esta = str_trim(esta)) %>% 
    codconver("AUDIF", "esta") %>% 
    codconver("AUDIF", "ra") %>% 
    codconver("AUDIF", "mat") 
    
  resultado
}

audifam_xestado <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      select(-fijada) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), -organismo) %>% 
      group_by(circunscripcion, organismo_descripcion)
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      select(-fijada) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audifam_realizadas_xmateria <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") 
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, mat) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(mat, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, mat) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(mat, cantidad) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, 
             everything(), -organismo) %>%  
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audifam_realizadas_xresultado <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") 
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes) %>% 
      select(circunscripcion, organismo_descripcion, año_mes,
             conciliacion_total, conciliacion_parcial, sin_conciliacion_y_sin_apelacion,
             sin_conciliacion_con_apelacion, conciliacion_en_proceso_sin_contradiccion, everything(), -organismo) 
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, 
             conciliacion_total, conciliacion_parcial, sin_conciliacion_y_sin_apelacion,
             sin_conciliacion_con_apelacion, conciliacion_en_proceso_sin_contradiccion, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audifam_realizadas_xtipo <- function(df, desagregacion_mensual = TRUE) {

  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    # importante estoy multiplicando casos informados por acumulacion de codigos en ta, obliga a consolidar
    mutate(indice = row_number()) %>% 
    tidyr::separate_rows(ta, sep = "#") %>% 
    filter(!(ta %in% c("4", "5") & fea > as.Date("2018-10-01"))) %>% # exclusion de casos de violencia
    codconver("AUDIF", "ta")
  
  #trabajo df_temp de fracciones de duraciones para los ta acumulados

  df_temp <- df %>% 
    group_by(indice) %>%
    summarise(n = n(), duracm = unique(duracm)) %>%
    mutate(duracm = as.integer(duracm)) %>%
    rowwise() %>%
    mutate(durac_consolidado = duracm / n) %>%
    select(indice, durac_consolidado)
  
  df <- df %>% 
    left_join(df_temp, by = "indice")

  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
  } else {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    
   
  }
 
  minutos_audiencias <<- minutos_audiencias
  cantidad_audiencias <<- cantidad_audiencias
 
}

audifam_realizadas_video_xtipo <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    filter(audvid == 1) %>%
    # importante estoy multiplicando casos informados por acumulacion de codigos en ta, obliga a consolidar
    mutate(indice = row_number()) %>% 
    tidyr::separate_rows(ta, sep = "#") %>% 
    codconver("AUDIF", "ta") 
    
  #trabajo df_temp de fracciones de duraciones para los ta acumulados
  
  df_temp <- df %>% 
    group_by(indice) %>%
    summarise(n = n(), duracm = unique(duracm)) %>%
    mutate(duracm = as.integer(duracm)) %>%
    rowwise() %>%
    mutate(durac_consolidado = duracm / n) %>%
    select(indice, durac_consolidado)
  
  df <- df %>% 
    left_join(df_temp, by = "indice")
  
  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      filter(fea >= as.Date("2018-10-01")) %>%  # fecha a partir de la cual se aprueba protocol familia
      group_by(organismo, año_mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      filter(fea >= as.Date("2018-10-01")) %>%  # fecha a partir de la cual se aprueba protocol familia
      group_by(organismo, año_mes, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
    
  } else {
    
    cantidad_audiencias <<- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias <<- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(durac_consolidado, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    
    
  }
  
  minutos_audiencias <<- minutos_audiencias
  cantidad_audiencias <<- cantidad_audiencias
  
}

audifam_violencias <- function(poblacion, start_date, end_date) {

  start_date <- as.Date(start_date, format = "%Y-%m-%d")
  inicio <- paste0("'", strftime(start_date, "%d-%m-%Y"), "'")
  
  end_date <- as.Date(end_date, format = "%Y-%m-%d") - 1
  fin <- paste0("'", strftime(end_date, "%d-%m-%Y"), "'")
  
  query1 <- paste("select * from audiencias_concretadas_9198(",inicio,",",fin,")")
  
  result1 <- DB_REJUCAV() %>% 
    dbGetQuery(query1) %>% 
    mutate(audiencia_tipo = "familiar")

  query2 <- paste("select * from audiencias_concretadas_10058(",inicio,",",fin,")")
  
  result2 <-  DB_REJUCAV() %>% 
    dbGetQuery(query2) %>% 
    mutate(audiencia_tipo = "mujer")
  
  resul <- result1 %>% 
    bind_rows(result2) %>% 
    select(jurisdiccion, juzgado, audiencia_tipo, cantidad) %>% 
    tidyr::pivot_wider(names_from = audiencia_tipo, values_from = cantidad) %>% 
    mutate(familiar = ifelse(is.na(familiar), 0, familiar),
           mujer = ifelse(is.na(mujer), 0, mujer)) %>% 
    rename("violencia_familiar" = "familiar",
           "violencia_c-mujer" = "mujer")
    
  if(any(str_detect(poblacion$organismo, "fam"))) {
    resul <- resul %>% 
      filter(str_detect(tolower(juzgado), "fam")) 
  }  else if(any(str_detect(poblacion$organismo, "paz"))) {
    resul <- resul %>% 
      filter(str_detect(tolower(juzgado), "paz"))
    resul
  }  else if(any(str_detect(poblacion$organismo, "cco"))) {
    resul <- resul %>% 
      filter(str_detect(tolower(juzgado), "juzgado civil"), str_detect(jurisdiccion, "SALVADOR|FEDERACION|ISLAS"))
    resul
  }
  
  resul <- resul %>% 
    mutate(juzgado = str_to_title(juzgado), jurisdiccion = str_to_title(jurisdiccion)) %>%
    do(janitor::adorn_totals(., c("row", "col"))) %>% 
    arrange(desc(Total))
  
  resul

}

audifam_prim_paz <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02"){
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIF") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!poblacion$organismo) %>%
    filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
    mutate(fea = dmy(fea)) %>% mutate(audvid = as.integer(audvid)) %>%
    filter(fea >= data_interval_start, fea < data_interval_end) %>%
    select(-data_interval_start, -data_interval_end, -justiables) %>%
    collect() 
  
  resultado <- resultado %>%
    mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
    mutate(duracm = str_trim(duracm)) %>%
    mutate(mat = str_sub(toupper(mat), 1,1)) %>%
    mutate(ra = str_sub(toupper(ra), 1,1)) %>%
    mutate(ta = str_replace_all(ta, "[:blank:]", "")) %>%
    mutate(ta = str_replace_all(ta, "[:alpha:]|[:punct:]", "#")) %>%
    mutate(ta = str_trim(ta)) %>% 
    mutate(esta = str_remove_all(esta, "[:alpha:]|[:punct:]")) %>%
    mutate(esta = str_trim(esta)) %>% 
    codconver("AUDIF", "esta") %>% 
    codconver("AUDIF", "ra") %>% 
    codconver("AUDIF", "mat") 
  
  resultado
}

