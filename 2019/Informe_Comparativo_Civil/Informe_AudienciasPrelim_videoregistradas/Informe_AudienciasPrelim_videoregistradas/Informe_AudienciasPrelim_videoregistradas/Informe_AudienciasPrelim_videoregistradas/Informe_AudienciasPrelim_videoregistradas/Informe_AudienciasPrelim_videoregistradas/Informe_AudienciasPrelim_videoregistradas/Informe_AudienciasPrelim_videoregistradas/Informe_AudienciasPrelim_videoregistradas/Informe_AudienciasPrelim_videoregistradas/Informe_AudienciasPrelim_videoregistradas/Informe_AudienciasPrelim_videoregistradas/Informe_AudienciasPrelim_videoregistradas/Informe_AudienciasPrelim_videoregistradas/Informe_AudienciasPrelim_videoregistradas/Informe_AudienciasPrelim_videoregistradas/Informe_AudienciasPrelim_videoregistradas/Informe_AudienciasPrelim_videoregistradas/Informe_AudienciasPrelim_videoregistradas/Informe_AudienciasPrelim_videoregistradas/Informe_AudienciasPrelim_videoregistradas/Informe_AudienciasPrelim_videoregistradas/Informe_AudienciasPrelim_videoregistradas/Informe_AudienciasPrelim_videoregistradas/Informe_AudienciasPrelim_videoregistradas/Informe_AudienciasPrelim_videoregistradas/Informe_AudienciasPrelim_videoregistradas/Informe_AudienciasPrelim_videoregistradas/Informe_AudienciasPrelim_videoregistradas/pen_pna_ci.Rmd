---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
resultado <- iniciadas_pen(poblacion = jdo_pna,
              start_date = start_date,
              end_date = end_date)
```

## Causas Iniciadas `r getDataIntervalStr(start_date, end_date)`

`r if (ci & t) '### Agrupados por Organismo'`

```{r, pen_pna_ci_t, eval=(ci & t)}
resultado$inic_xorg %>% 
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
```


`r if (ci & td) '### Agrupados por Tipo de Proceso'`

```{r, pen_pna_ci_td, eval=(ci & td)}
resultado$inic_xproc %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
```


`r if (ci & pc) '### Listado Primario'`

```{r, pen_pna_ci_pc, eval=(ci & pc)}
resultado$inic_jdopna_pc %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
```


