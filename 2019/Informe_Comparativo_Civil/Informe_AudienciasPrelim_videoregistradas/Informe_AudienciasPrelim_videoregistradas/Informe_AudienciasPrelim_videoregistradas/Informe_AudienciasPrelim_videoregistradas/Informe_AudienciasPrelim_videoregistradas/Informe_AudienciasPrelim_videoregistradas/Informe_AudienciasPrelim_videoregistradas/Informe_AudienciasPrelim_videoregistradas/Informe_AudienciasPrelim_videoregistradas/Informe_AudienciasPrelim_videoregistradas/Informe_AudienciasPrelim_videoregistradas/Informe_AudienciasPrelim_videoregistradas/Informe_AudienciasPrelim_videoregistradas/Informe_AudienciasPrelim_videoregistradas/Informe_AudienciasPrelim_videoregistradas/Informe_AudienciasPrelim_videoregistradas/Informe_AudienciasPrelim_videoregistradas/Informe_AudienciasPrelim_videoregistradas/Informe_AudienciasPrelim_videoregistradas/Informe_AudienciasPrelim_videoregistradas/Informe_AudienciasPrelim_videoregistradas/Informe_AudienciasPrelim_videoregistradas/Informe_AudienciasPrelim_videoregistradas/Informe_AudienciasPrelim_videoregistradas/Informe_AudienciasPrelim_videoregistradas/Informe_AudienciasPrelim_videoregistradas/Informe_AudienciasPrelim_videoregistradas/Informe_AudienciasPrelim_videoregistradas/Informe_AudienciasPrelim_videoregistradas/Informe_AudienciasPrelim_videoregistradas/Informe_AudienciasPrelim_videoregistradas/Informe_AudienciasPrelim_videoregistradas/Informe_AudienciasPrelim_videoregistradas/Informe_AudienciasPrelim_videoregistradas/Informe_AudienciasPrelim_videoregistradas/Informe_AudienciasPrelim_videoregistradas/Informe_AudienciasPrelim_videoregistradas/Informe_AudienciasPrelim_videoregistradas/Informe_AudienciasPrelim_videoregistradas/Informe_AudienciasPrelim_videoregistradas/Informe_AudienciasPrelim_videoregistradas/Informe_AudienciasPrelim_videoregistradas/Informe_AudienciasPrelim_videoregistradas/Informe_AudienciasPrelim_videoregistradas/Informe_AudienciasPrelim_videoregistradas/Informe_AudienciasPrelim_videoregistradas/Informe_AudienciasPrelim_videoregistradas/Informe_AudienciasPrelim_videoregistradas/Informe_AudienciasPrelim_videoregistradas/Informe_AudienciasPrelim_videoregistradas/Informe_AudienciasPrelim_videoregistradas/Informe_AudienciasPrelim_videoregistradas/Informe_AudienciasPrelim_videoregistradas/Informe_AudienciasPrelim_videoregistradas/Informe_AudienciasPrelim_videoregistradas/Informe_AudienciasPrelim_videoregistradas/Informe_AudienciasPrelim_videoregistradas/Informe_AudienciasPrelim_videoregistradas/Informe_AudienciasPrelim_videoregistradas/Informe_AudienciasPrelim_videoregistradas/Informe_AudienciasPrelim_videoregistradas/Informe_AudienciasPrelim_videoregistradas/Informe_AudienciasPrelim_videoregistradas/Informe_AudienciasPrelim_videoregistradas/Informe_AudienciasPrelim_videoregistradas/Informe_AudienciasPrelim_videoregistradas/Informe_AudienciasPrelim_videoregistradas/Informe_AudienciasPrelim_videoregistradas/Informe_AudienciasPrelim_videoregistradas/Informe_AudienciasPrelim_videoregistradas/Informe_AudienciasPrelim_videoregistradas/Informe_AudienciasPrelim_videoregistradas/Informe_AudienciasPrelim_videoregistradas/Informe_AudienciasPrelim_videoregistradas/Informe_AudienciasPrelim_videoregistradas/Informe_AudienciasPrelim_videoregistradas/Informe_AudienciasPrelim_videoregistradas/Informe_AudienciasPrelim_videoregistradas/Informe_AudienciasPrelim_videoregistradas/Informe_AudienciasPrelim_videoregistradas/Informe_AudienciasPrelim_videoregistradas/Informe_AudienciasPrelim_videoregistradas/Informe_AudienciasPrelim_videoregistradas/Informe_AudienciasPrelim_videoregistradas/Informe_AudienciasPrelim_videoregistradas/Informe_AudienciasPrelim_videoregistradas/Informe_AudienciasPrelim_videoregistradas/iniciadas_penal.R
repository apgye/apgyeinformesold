# Iniciados Penales
iniciadas_pen <- function(poblacion, start_date="2019-02-01", end_date = "2019-03-01") {
  
  if(any(str_detect(poblacion, "oga"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIGTIA') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio), fhecho = dmy(fhecho)) %>%
      filter(iep %in% !!poblacion$organismo) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
      mutate(organismo = "Garantías")
    
    # Ajuste para OGA Paraná que informa procesos constitucionales del tribunal de juicio por CIGTIA debido 
    # a que no puede discriminar en el Lex y no se justifica crear dos operaciones
    if(any(str_detect(inic$circunscripcion, "Paraná"))) {
      inic  <- inic %>% 
        filter(!(circunscripcion == "Paraná" & str_detect(radic, "Juicio")))
    }
    
    inic_prim <- inic
    
    inic <- inic %>% 
      mutate(año_mes = format(data_interval_start, "%Y-%m")) %>%
      filter(
        !str_detect(tproc, "^ADMINISTRATIVO|^ARCHIVO|^AUTORIZACION|CONCURSO CERRADO"),
        !str_detect(tproc, "^CONCURSOS|^DESIGNACION|^EFECTOS SECUESTRADOS|^EXHORTO|^INFORMES"),
        !str_detect(tproc, "^LEGAJO|^OFICIO|^PERSONAL|^SENTENCIA|^SU PRESENTACION|^SU SITUACION"),
        !str_detect(tproc, "^EJECUCION DE SENTENCIA|^EJECUTIVO|FALLECIMIENTO|^INCIDENTE|INSTRUCCION"),
        !str_detect(tproc, "^LIBRAMIENTO DE CHEQUE|^MEDIDA CAUTELAR|^OTRO|^SOLICITUD"),
        !str_detect(tproc, "^VARIOS|^TESTIMONIOS|Y OTROS"),
        !str_detect(tproc, "^JURISPRUDENCIA|LEY")) %>%
      group_by(circunscripcion, organismo, año_mes, tproc) %>%
      summarise(cantidad = n(), durac_hec_inic= median(finicio-fhecho, na.rm = T)) %>%
      mutate(durac_hec_inic = ifelse(durac_hec_inic > 0 , as.character(durac_hec_inic), NA)) %>% ungroup() %>% 
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(circunscripcion, organismo, año_mes, tproc) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc) 
    
    
    resultado <- list()
    
    resultado$inic_gtia_pc <- inic_prim %>%
      select(circunscripcion, organismo,  tipo_proceso = tproc, nro, finicio) %>% 
      arrange(circunscripcion, organismo, finicio) 
    
    resultado$inic_xtproc <-  inic 
    
    resultado$inic_xcirc <- inic %>% 
      filter(circunscripcion != "Total") %>% 
      group_by(circunscripcion, organismo, año_mes) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.))
    
    resultado
    
  } 
  else if(any(str_detect(poblacion, "tja"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIJUI') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(fingj = dmy(fingj), ffdebab = dmy(ffdebab), frdebab = dmy(frdebab)) %>%
      collect() %>% 
      left_join(poblacion_penal %>% 
                  select(iep=organismo, circunscripcion), by="iep") %>% 
      filter(circunscripcion %in% tja$circunscripcion) %>% 
      left_join(tja %>% select(organismo_descripcion, circunscripcion), by="circunscripcion") %>% 
      rename(organismo = organismo_descripcion)
    
    # Ajuste para OGA Paraná que informa procesos constitucionales del tribunal de juicio por CIGTIA debido 
    # a que no puede discriminar en el Lex y no se justifica crear dos operaciones
    proc_constit_pna <- DB_PROD() %>% 
      apgyeTableData('CIGTIA') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep == "ogapen0000pna") %>% 
      filter(grepl("AMPARO|HABEAS|ACCION DE PROHIBICION|ACCION DE EJECUCION", tproc)) %>% 
      filter(grepl("Juicio", radic)) %>%
      mutate(finicio = dmy(finicio), fhecho = dmy(fhecho)) %>%
      collect()  
    
    if(nrow(proc_constit_pna) != 0) {
      proc_constit_pna <- proc_constit_pna %>% 
        left_join(poblacion_penal %>% select(iep=organismo, circunscripcion), by="iep") %>% 
        left_join(tja %>% select(organismo_descripcion, circunscripcion), by="circunscripcion") %>% 
        rename(organismo = organismo_descripcion) %>% 
        group_by(circunscripcion, organismo, tproc, data_interval_start) %>%
        summarise(cantidad = n()) 
      proc_constit_pna
    }
    
    inic_prim <- inic %>% 
      bind_rows(if(exists("proc_constit_pna")) proc_constit_pna)
    
    inic_tjui <- inic %>% 
      bind_rows(if(exists("proc_constit_pna")) proc_constit_pna) %>% ungroup() %>% 
      mutate(año_mes = format(data_interval_start, "%Y-%m")) %>%
      group_by(circunscripcion, organismo, año_mes, tproc) %>%
      summarise(cantidad = n()) %>%  
                # durac_ingreso_fij_dbte = median(ffdebab-fingj, na.rm = T)) %>% 
               #durac_fij_realizacion_dbte = median(frdebab-ffdebab, na.rm = T)) %>%
      #mutate(durac_ingreso_fij_dbte = ifelse(durac_ingreso_fij_dbte > 0 , as.character(durac_ingreso_fij_dbte), NA)) %>%
      #mutate(durac_fij_realizacion_dbte = ifelse(durac_fij_realizacion_dbte > 0 , as.character(durac_fij_realizacion_dbte), NA)) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(circunscripcion, organismo, año_mes, tproc) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc)  %>% 
      ungroup() %>% 
      mutate(organismo = str_sub(organismo, 1, 32))
    
  
    inic_ap <- DB_PROD() %>% 
      apgyeTableData('CIAP') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(fiap  = dmy(fiap)) %>%
      collect() %>% 
      left_join(poblacion_penal %>% 
                  select(iep=organismo, circunscripcion), by="iep") %>% 
      filter(circunscripcion %in% tja$circunscripcion) %>% 
      left_join(tja %>% select(organismo_descripcion, circunscripcion), by="circunscripcion") %>% 
      rename(organismo = organismo_descripcion) 
    
    inic_ap_prim <- inic_ap
    
    inic_ap <- inic_ap %>% 
      mutate(año_mes = format(data_interval_start, "%Y-%m")) %>%
      group_by(circunscripcion, organismo, año_mes, tproc) %>%
      summarise(cantidad = n()) %>%  ungroup() %>% 
      # durac_ingreso_fij_dbte = median(ffdebab-fingj, na.rm = T)) %>% 
      #durac_fij_realizacion_dbte = median(frdebab-ffdebab, na.rm = T)) %>%
      #mutate(durac_ingreso_fij_dbte = ifelse(durac_ingreso_fij_dbte > 0 , as.character(durac_ingreso_fij_dbte), NA)) %>%
      #mutate(durac_fij_realizacion_dbte = ifelse(durac_fij_realizacion_dbte > 0 , as.character(durac_fij_realizacion_dbte), NA)) %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      arrange(circunscripcion, organismo, año_mes, tproc) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc)  
    
    
    resultado <- list()
    
    resultado$inic_tja_pc <- inic_prim %>%
      select(circunscripcion, organismo,  tipo_proceso = tproc, nro, fingj) %>% 
      arrange(circunscripcion, organismo, fingj) 
    
    resultado$inic_ap_pc <- inic_ap_prim %>% 
      select(circunscripcion, organismo,  tipo_proceso = tproc, nro, fiap) %>% 
      arrange(circunscripcion, organismo, fiap) 
    
    
    resultado$inic_tjui_tproc <-  inic_tjui 
    
    resultado$inic_ap_tproc <- inic_ap
    
    
    resultado$inic_tjui_circ <-  inic_tjui %>% 
      filter(circunscripcion != "Total") %>% 
      group_by(circunscripcion, organismo, año_mes) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.)) 
      
    
    resultado$inic_ap_circ <- inic_ap %>% 
      filter(circunscripcion != "Total") %>% 
      group_by(circunscripcion, organismo, año_mes) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.))
    
    
    resultado
    
    
  } 
  else if(any(str_detect(poblacion, "campen"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData("CINC3P") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      left_join(poblacion %>% 
                    select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
    
      inic_prim <- inic
      
      inic <- inic %>% 
        mutate(año_mes = format(data_interval_start, "%Y-%m")) %>%
        mutate(tdelito = stringr::str_trunc(tdelito, 60)) %>% 
        group_by(circunscripcion, organismo, año_mes, tdelito) %>%
        summarise(cantidad = n()) %>%
        arrange(circunscripcion, organismo, año_mes, tdelito) %>% 
        group_by(circunscripcion, organismo) %>% 
        do(janitor::adorn_totals(.))  %>% rename(tipo_delito = tdelito) 
      
     resultado <- list()
     
     resultado$inic_campen_pc <- inic_prim %>%
       select(circunscripcion, organismo,  tipo_proceso = tproc, nro, finicio) %>% 
       arrange(circunscripcion, organismo, finicio) 
     
     resultado$inic_xproc <- inic 
     
     resultado$inic_xorg <- inic %>% 
       filter(circunscripcion != "Total") %>% 
       group_by(circunscripcion, organismo, año_mes) %>% 
       summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
       group_by(circunscripcion, organismo) %>% 
       do(janitor::adorn_totals(.)) 
     
     
     resultado
     
  } 
  else if(any(str_detect(poblacion, "sal"))) {
    
    inicp <- DB_PROD() %>% 
      apgyeTableData("CIN3P") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect()
    
    if (nrow(inicp) > 0){
      inicp <- inicp %>%
        filter(!str_detect(tproc, "AMPARO")) %>% # Exclusión de amparos
        left_join(poblacion %>% 
                    select(iep=organismo, organismo=organismo_descripcion,
                           circunscripcion), by="iep") %>% 
        mutate(materia = "penal")
    }
    
    inicc <- DB_PROD() %>% 
      apgyeTableData("CIN3PC") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect()
    
    if (nrow(inicc) > 0){
      inicc <- inicc %>% 
        left_join(poblacion %>% 
                    select(iep=organismo, organismo=organismo_descripcion,
                           circunscripcion), by="iep") %>%
        mutate(materia = "constitucional")
    }
    
    inic <- bind_rows(if(exists("inicp")) inicp, if(exists("inicc")) inicc)
     
    inic_prim <- inic
  
    if (nrow(inic) > 0){
      inic <- inic %>% 
        mutate(año_mes = format(data_interval_start, "%Y-%m")) %>%
        group_by(circunscripcion, organismo, año_mes, materia, tproc) %>%
        summarise(cantidad = n()) %>%
        arrange(circunscripcion, organismo, año_mes, tproc) %>% 
        group_by(circunscripcion, organismo) %>% 
        do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc) 
    }
    
    resultado <- list()
    
    resultado$inic_sal_pc <- inic_prim %>%
      select(circunscripcion, organismo,  tipo_proceso = tproc, nro, finicio) %>% 
      arrange(circunscripcion, organismo, finicio) 
    
    resultado$inic_xmat_proc <- inic 
    
    resultado$inic_xorg <- inic %>% 
      filter(circunscripcion != "Total") %>% 
      group_by(circunscripcion, organismo, año_mes) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.)) 
    
    
    resultado
    
    }
  else if(any(str_detect(poblacion, "pep"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData("IGEP") %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
      filter(finicio >= data_interval_start, finicio < data_interval_end ) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
    
    inic_prim <- inic %>% 
      mutate(organismo = abbreviate(organismo, minlength = 8))
    
    inic <- inic %>% 
      mutate(ottproc = str_replace_all(ottproc, "EJECUCION DE PENA ", "")) %>% 
      mutate(tpena = toupper(tpena)) %>% 
      mutate(tpena =  case_when(
        tpena == "PL" ~ "Privativa de libertad",
        tpena == "PLI" ~ "Privativa de libertad con internación",
        tpena == "PD" ~ "Prisión domiciliaria", 
        tpena == "SD" ~ "Semidetención tanto diurna como nocturna", 
        tpena == "LC" ~ "Libertad Condicional", 
        tpena == "LA" ~ "Libertad Asistida", 
        tpena == "SP" ~ "Sustitución de Penas", 
        tpena == "MSA" ~ "Medidas de Seguridad Ambulatoria", 
        tpena == "MS" ~ "Medida Seguridad alojado en UP", 
        tpena == "MSI" ~ "Medida Seguridad con internación", 
        tpena == "O" ~ "Situaciones no contempladas", 
        TRUE ~ "sin_dato")) %>% 
      select(circunscripcion, organismo, everything()) %>% 
      mutate(año_mes = format(data_interval_start, "%Y-%m")) %>% 
      mutate(organismo = abbreviate(organismo, minlength = 8))
    
    inic_tpena <- inic %>% 
      group_by(circunscripcion, organismo, año_mes, tpena) %>% 
      summarise(cantidad = n()) %>% 
      arrange(circunscripcion, organismo, año_mes, tpena) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_pena = tpena) 
    
    inic_tproc <- inic %>% 
      mutate(ottproc = str_sub(ottproc, 1, 60)) %>% 
      group_by(circunscripcion, organismo, año_mes, ottproc) %>% 
      summarise(cantidad = n()) %>% 
      arrange(circunscripcion, organismo, año_mes, ottproc) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.)) %>% 
      rename(tipo_proceso = ottproc) 
    
    resultado <- list()
    
    resultado$inic_pep_pc <- inic_prim %>%
      select(circunscripcion, organismo,  tipo_proceso = tproc, nro, finicio) %>% 
      arrange(circunscripcion, organismo, finicio) 
    
    resultado$inic_tproc <- inic_tproc
    
    resultado$inic_tpena <- inic_tpena 
    
    resultado$inic_xorg <- inic_tproc %>% 
      filter(circunscripcion != "Total") %>% 
      group_by(circunscripcion, organismo, año_mes) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.)) 
    
    resultado
    
  }
  else if(any(str_detect(poblacion, "jdopna"))) {
    
    inic <- DB_PROD() %>% 
      apgyeTableData('CIPM') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(iep %in% !!poblacion$organismo) %>% 
      collect() %>% 
      left_join(poblacion %>% 
                  select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep")
    
    inic_prim <- inic
    
    inic <- inic %>% 
      filter(
        !str_detect(tproc, "^ADMINISTRATIVO|^ARCHIVO|^AUTORIZACION|CONCURSO CERRADO"),
        !str_detect(tproc, "^CONCURSOS|^DESIGNACION|^EFECTOS SECUESTRADOS|^EXHORTO|^INFORMES"),
        !str_detect(tproc, "^LEGAJO|^OFICIO|^PERSONAL|^SENTENCIA|^SU PRESENTACION|^SU SITUACION"),
        !str_detect(tproc, "^EJECUCION DE SENTENCIA|^EJECUTIVO|FALLECIMIENTO|^INCIDENTE|INSTRUCCION"),
        !str_detect(tproc, "^LIBRAMIENTO DE CHEQUE|^MEDIDA CAUTELAR|^OTRO|^SOLICITUD"),
        !str_detect(tproc, "^VARIOS|^TESTIMONIOS|Y OTROS"),
        !str_detect(tproc, "^JURISPRUDENCIA|LEY")) %>%
      mutate(año_mes = format(data_interval_start, "%Y-%m")) %>% 
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>% 
      group_by(circunscripcion, organismo, año_mes, tproc) %>%
      summarise(cantidad = n()) %>%
      arrange(circunscripcion, organismo, año_mes, tproc) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.))  %>% rename(tipo_proceso = tproc) 
      
    
    resultado <- list()
    
    resultado$inic_jdopna_pc <- inic_prim %>%
      mutate(tproc = stringr::str_trunc(tproc, 60)) %>%
      select(circunscripcion, organismo,  tipo_proceso = tproc, nro, finicio) %>% 
      arrange(circunscripcion, organismo, finicio) 
    
    resultado$inic_xproc <- inic 
    
    resultado$inic_xorg <- inic %>% 
      filter(circunscripcion != "Total") %>% 
      group_by(circunscripcion, organismo, año_mes) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.)) 
    
    
    resultado
    
    
  }
 
  resultado
}
iniciadas_pen_complemento <- function(df, desagregacion_mensual = T) {
  
  if(desagregacion_mensual) {
    
    df
    
  } else {
    
    df <- df %>% 
      filter(circunscripcion != "Total") %>% 
      group_by(circunscripcion, organismo) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
      janitor::adorn_totals("row") %>% 
      arrange(desc(cantidad))
    
    df
  }
  df
}
iniciados_oma <- function(poblacion, start_date="2019-03-01", end_date = "2019-04-01") {
  
  # revisar si fecha de inicio es un dato cargado en OMA o se cargó por MPF en períodos anterioes 21/05/19
  # si se carga finicio a través de registro cargar finicio, ffin, responsable y mediador/oficialprueba.
  
  inic <- DB_PROD() %>% 
    apgyeTableData("IGMP") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
    filter(finicio >= data_interval_start, finicio < data_interval_end ) %>% 
    filter(finicio >= lubridate::make_date(2019,03,01)) %>% 
    collect() %>% 
    left_join(poblacion %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 
  
  # Excepción de reimputación de valor para variable responsable jurisdicción:
  # Colón, Concordia, Chajarí y Federación cargan en el campo 4, no en 'fiscm'
  # Aux4 viene concatenado en 'resp'
  
  if (any(unique(inic$circunscripcion) %in% c("Colón", "Concordia", "Chajarí", "Federación"))) {
    
    inic_base <- inic %>% 
      filter(!circunscripcion %in% c("Colón", "Concordia", "Chajarí", "Federación"))
    
    inic_temp <- inic %>% 
      filter(circunscripcion %in% c("Colón", "Concordia", "Chajarí", "Federación"))
    
      
    inic_temp <- inic_temp %>% 
      tidyr::separate(resp, into = c(NA, "resp"), sep="\\$") %>% 
      mutate(resp = ifelse(resp=="", NA, resp)) %>% 
      left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                  select(idagente, apellido) %>% collect(), by=c("resp"="idagente")) %>% 
      mutate(resp = apellido) %>% select(-apellido)
    
    inic <- inic_base %>% 
      bind_rows(inic_temp)
    
    inic
    
  }
  
  
  inic <- inic %>% 
    mutate(año_mes = format(data_interval_start, "%Y-%m")) %>% 
    mutate(resp = toupper(resp)) %>% 
    select(circunscripcion, resp, everything()) 
  
  inic_prim <- inic %>% 
    select(circunscripcion, resp, nro_om, nro_og, finicio) %>% 
    arrange(circunscripcion, desc(finicio))
  
  inic <- inic %>% 
    mutate(resp = str_replace_all(resp, "Z___|DR. |DRA. ", "")) %>% 
    mutate(resp = ifelse(data_interval_start < make_date(2019,08,01) & 
                         circunscripcion %in% c("Colón", "Concordia", "Chajarí", "Federación"),
                         "sin dato", resp)) %>% 
    mutate(resp = ifelse(str_detect(resp, "MESA "), "sin dato", resp)) %>% 
    group_by(circunscripcion, resp, año_mes) %>% 
    summarise(cantidad = n()) %>% 
    arrange(circunscripcion, resp, año_mes) %>% 
    group_by(circunscripcion, resp) %>% 
    do(janitor::adorn_totals(.)) %>% ungroup() %>% 
    mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>% 
    rename(organismo = resp) 
    
  resultado <- list()
  
  resultado$inic_oma_pc <- inic_prim
  
  resultado$inic_xresp <- inic 
  
  resultado$inic_xcirc <- inic %>% 
    filter(circunscripcion != "Total") %>% 
    mutate(organismo = "OMA") %>% 
    group_by(circunscripcion, organismo, año_mes) %>% 
    summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
    group_by(circunscripcion, organismo) %>% 
    do(janitor::adorn_totals(.)) 
  
  
  resultado
  
}

