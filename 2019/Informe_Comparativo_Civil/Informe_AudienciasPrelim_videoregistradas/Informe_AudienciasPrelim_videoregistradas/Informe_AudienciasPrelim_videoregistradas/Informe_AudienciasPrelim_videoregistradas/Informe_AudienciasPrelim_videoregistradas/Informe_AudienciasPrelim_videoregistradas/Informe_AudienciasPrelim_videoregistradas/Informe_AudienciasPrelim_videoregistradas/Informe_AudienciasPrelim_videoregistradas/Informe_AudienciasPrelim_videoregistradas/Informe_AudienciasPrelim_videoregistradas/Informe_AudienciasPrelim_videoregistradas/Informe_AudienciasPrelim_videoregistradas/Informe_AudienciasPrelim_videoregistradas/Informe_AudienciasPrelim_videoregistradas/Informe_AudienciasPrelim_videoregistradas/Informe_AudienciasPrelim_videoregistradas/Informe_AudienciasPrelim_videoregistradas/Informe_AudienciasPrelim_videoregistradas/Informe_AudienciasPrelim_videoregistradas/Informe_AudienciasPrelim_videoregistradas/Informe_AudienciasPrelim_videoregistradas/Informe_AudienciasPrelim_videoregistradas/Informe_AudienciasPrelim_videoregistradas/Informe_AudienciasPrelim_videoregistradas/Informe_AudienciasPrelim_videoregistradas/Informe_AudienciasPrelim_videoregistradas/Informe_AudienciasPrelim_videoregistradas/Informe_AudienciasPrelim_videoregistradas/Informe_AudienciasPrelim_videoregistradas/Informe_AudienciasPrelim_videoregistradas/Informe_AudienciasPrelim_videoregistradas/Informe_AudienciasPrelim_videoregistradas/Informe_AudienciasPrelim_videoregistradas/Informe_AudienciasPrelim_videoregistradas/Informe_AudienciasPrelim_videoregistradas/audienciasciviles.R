
audicco_prim <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02"){
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIC") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !! poblacion$organismo) %>% collect()
    
  if (nrow(resultado) != 0){
    resultado <- resultado %>% 
      filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
      mutate(fea = dmy(fea), fprog = dmy(fprog), ffa = dmy(ffa)) %>% 
      filter(fea >= data_interval_start, fea < data_interval_end) %>%
      select(-data_interval_start, -data_interval_end, -justiables) %>%
      distinct(iep, nro, caratula, tproc, fea, ta, .keep_all = T) # Filtro Casos Duplicados
    
    resultado <- resolverconvertidos(resultado)
  
    resultado <- resultado %>%
      mutate(audvid = as.integer(audvid)) %>%
      mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
      mutate(duracm = str_trim(duracm)) %>%
      mutate(ra = str_sub(toupper(ra), 1,1)) %>%
      mutate(ta = str_trim(ta)) %>% 
      mutate(esta = str_remove_all(esta, "[:alpha:]|[:punct:]")) %>%
      mutate(esta = str_trim(esta)) %>% 
      codconver("AUDIC", "esta") %>% 
      codconver("AUDIC", "ra") %>% 
      codconver("AUDIC", "ta") 
  }
  
  resultado
}

audicco_xestado <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m")) %>%
      group_by(organismo, año_mes, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      select(-fijada) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audicco_realizadas_xtipo_cantidad_minutos <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    mutate(duracm = as.integer(duracm))
  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
      
    minutos_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
    
  } else {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    cantidad_audiencias
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias
    
  }
  
  resultado <- list()
  
  resultado$minutos_audiencias <- minutos_audiencias
  resultado$cantidad_audiencias <- cantidad_audiencias
  
  resultado
  
}

audicco_realizadas_xtipoyresultado <- function(df, desagregacion_mensual = TRUE) {
  
  df <- df %>% 
    filter(esta == "realizada") 
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col") 
    
    resultado
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion,  everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audicco_realizadas_video_xtipo <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    filter(audvid == 1) %>% 
    mutate(duracm = as.integer(duracm))
  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m")) %>%  
      group_by(organismo, año_mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>% 
      group_by(organismo, año_mes, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
    
  } else {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(minutos = sum(duracm, na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    
    
  }
  resultado <- list()
  
  resultado$minutos_audiencias <- minutos_audiencias
  resultado$cantidad_audiencias <- cantidad_audiencias
  
  resultado
  
}

audiccco_vc_program <- function(df) {
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    filter(ta == "preliminar") %>% 
    filter(ra != "conciliacion_total") %>% 
    filter(!is.na(fprog) & fprog > end_date) %>% 
    rename(fprogramada_vista_causa = fprog, organismo = iep, nro_expte = nro) %>% 
    mutate(mes = as.character(lubridate::month(fprogramada_vista_causa)), 
           año = as.character(lubridate::year(fprogramada_vista_causa))) %>%
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                              "circunscripcion")],
              by = "organismo") %>%
    group_by(circunscripcion, organismo_descripcion, año, mes) %>% 
    mutate(fprogramada_vista_causa = str_replace_all(fprogramada_vista_causa, "-", "\\/")) %>% 
    summarise(cantidad_audiencias = n(), 
              nros_expte_y_fechaAudiencia = paste(nro_expte, fprogramada_vista_causa,
                                                  sep = ": ", collapse = "; ")) %>% 
    ungroup() %>% 
    arrange(circunscripcion, organismo_descripcion)  %>% 
    janitor::adorn_totals("row") 
  
  df
  
}