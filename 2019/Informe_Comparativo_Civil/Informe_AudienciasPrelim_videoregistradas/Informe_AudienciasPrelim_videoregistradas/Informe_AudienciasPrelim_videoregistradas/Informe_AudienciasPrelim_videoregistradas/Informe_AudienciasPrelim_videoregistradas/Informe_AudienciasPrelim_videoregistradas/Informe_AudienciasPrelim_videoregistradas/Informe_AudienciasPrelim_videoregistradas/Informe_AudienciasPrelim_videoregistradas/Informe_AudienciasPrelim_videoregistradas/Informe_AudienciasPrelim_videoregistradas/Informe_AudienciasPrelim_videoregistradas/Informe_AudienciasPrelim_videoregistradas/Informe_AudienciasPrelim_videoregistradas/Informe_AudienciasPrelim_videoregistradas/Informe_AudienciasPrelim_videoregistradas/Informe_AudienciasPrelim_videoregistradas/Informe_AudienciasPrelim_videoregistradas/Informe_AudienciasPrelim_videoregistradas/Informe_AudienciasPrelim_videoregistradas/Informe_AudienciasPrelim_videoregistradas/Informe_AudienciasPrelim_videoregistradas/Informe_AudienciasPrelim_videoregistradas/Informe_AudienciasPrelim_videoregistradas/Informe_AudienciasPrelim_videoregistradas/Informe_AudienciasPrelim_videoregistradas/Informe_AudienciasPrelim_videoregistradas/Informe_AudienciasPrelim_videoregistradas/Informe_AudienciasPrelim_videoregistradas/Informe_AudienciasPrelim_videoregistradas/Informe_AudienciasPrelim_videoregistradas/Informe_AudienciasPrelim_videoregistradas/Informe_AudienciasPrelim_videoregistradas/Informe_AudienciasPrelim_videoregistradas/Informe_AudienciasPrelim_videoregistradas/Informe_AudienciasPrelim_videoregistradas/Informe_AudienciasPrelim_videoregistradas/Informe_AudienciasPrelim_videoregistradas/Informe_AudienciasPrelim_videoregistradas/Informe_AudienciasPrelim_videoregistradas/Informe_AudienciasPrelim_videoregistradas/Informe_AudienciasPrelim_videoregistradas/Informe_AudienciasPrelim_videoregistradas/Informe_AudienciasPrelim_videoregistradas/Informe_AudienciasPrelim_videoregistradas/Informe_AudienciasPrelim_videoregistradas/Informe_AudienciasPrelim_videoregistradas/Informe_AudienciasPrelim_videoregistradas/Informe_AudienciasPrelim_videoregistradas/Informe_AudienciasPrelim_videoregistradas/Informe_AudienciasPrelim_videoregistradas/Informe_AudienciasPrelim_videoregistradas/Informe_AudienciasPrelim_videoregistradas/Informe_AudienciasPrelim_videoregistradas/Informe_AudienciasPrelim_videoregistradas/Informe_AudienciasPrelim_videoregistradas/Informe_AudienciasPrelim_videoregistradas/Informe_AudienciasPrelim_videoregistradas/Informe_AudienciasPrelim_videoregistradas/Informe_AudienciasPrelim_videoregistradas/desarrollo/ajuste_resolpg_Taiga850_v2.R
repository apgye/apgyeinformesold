# avance por Taiga 850 - Reabierto 8/8/19.
#"../apgyeinformes/2019/Modelo_Parametrizado/desarrollo/ajuste_resolpg_Taiga850_v2.R"

poblacion <- oga
start_date <- "2019-06-01"
end_date <- "2019-08-01"
desagregacion_mensual <- TRUE

resolpg <- DB_PROD() %>% apgyeDSL::apgyeTableData(RESOLPG) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) %>% 
  left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
              mutate(
                magistrado = apellido,
                #magistrado = paste0(apellido, ', ', nombres),
                iep_actuante = iep
              ) %>%
              select(idagente, iep_actuante, magistrado), by=c("jact"="idagente") )

if(desagregacion_mensual) {
  resolpg <- resolpg %>% group_by(iep, magistrado, data_interval_start, data_interval_end) 
} else {
  resolpg <- resolpg %>% group_by(iep, magistrado) 
}
resolpg <- resolpg %>% 
  process()

if(desagregacion_mensual) {
  
  result <- resolpg %>% .$result
  if (nrow(result) != 0) {
  
  # Quitamos las columnas de Resoluciones
  resultado_actosConcl <- result %>% 
    select(-c("suspension_proc_prueba", "orden_detencion", "med_protec_victim",
              "med_coercion", "prision_prev", "sustitutiva_prision_prev",
              "competencia", "prorroga_ipp", "remision_jdo_menores", 
              "formulacion_cargos", "revision_medidas", "revision_reglas_conducta",
              "allanamientos", "otros"))
  
  # Quitamos las columnas de Actos Conclusivos
  resultado_resoluciones <- result %>% 
    select(-c("sobreseimientos", "juicios_abrev", "condena_efectiva",
              "condena_efectiva_sustit", "condena_condicional",
              "medida_seguridad", "elevacion_juicio", "sobreseidos", "condenados"))
  
  resultado_ac <- resultado_actosConcl %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%
    left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
    select(circunscripcion, mes, magistrado, everything(), -iep, -jact, -calidad_primaria, -calidad, 
           -data_interval_start, -data_interval_end) %>% 
    mutate(juicios_abrev = as.character(juicios_abrev), #Categorizamos para no totalizar
           sobreseidos = as.character(sobreseidos), condenados = as.character(condenados)) %>% 
    mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                        "junio", "julio", "agosto", "septiembre", 
                                        "octubre", "noviembre", "diciembre", "Total"),
                        ordered = TRUE)) %>% 
    arrange(circunscripcion, mes) %>% 
    janitor::adorn_totals("col") %>% 
    group_by(circunscripcion) %>% 
    do(janitor::adorn_totals(.)) %>% 
    ungroup() %>% mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6))
  
  resultado_res <- resultado_resoluciones %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%
    left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
    select(circunscripcion, mes, magistrado, everything(), -iep, -jact, -calidad_primaria, -calidad, 
           -data_interval_start, -data_interval_end) %>% 
    mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                        "junio", "julio", "agosto", "septiembre", 
                                        "octubre", "noviembre", "diciembre", "Total"),
                        ordered = TRUE)) %>% 
    arrange(circunscripcion, mes) %>% 
    janitor::adorn_totals("col") %>% 
    group_by(circunscripcion) %>% 
    do(janitor::adorn_totals(.)) %>% 
    ungroup() %>% mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6))
  
  }
  
  actos_conclusivos_gtia_xtproc <- resolpg %>% .$res_xac
  
  if (nrow(actos_conclusivos_gtia_xtproc) != 0) {
    
    actos_conclusivos_gtia_xtproc <- actos_conclusivos_gtia_xtproc %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%
      left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
      select(circunscripcion, mes, magistrado, everything(), -iep, -jact, -data_interval_start, -data_interval_end ) %>% 
      rename(tipo_actoconcl = codigos) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE)) %>% 
      arrange(circunscripcion, mes) 
    
  }
  
  
} else {
  
  resultado <- resolpg %>% .$result %>% 
    left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
    select(circunscripcion, magistrado, everything(), -iep, -jact, -calidad_primaria, -calidad) %>% 
    mutate(sobreseidos = as.character(sobreseidos), condenados = as.character(condenados)) %>% 
    janitor::adorn_totals("col") %>% 
    group_by(circunscripcion) %>% 
    do(janitor::adorn_totals(.)) %>% 
    ungroup() %>% mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6))
  
  
  actos_conclusivos_gtia_xtproc <- resolpg %>% .$res_xac %>% 
    
    if (nrow(actos_conclusivos_gtia_xtproc) != 0) {
      actos_conclusivos_gtia_xtproc <- actos_conclusivos_gtia_xtproc %>% 
        left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
        select(circunscripcion, magistrado, everything(), -iep, -jact) %>% 
        rename(tipo_actoconcl = codigos)
      
    }
}


actos_conclusivos_gtia_xtproc <<- actos_conclusivos_gtia_xtproc


if(desagregacion_mensual) { if(!cfecha) {resultado <- resultado %>% select(-fecha)} }

resultado <- list()

resultado$ac <- resultado_ac
resultado$res <- resultado_res

resultado

