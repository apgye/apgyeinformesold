---
title: Causas Archivadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


## Causas Archivadas (2018-06-01 -`r lubridate::ymd(end_date) - 1`)

En la siguiente tabla se muestran la cantidad de causas archivadas agrupadas por organismo. 

```{r}
archivadas(jdos_ecq, 
  start_date = start_date,
  end_date = end_date) %>%
  group_by(circunscripcion, organismo) %>% 
  summarise(cantidad_causas_archivadas = sum(cantidad_causas_archivadas)) %>% 
  janitor::adorn_totals("row") %>% 
  outputTable(caption = "Causas Archivadas") %>%
  row_spec(0) 
```
