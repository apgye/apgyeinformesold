---
title: Causas Resueltas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## 2a y 3a Cat - Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestran las resoluciones dictadas por el organismo, excluyéndose aquellas dictadas en materia de **violencia** que aparecen en la próxima tabla. Esta actualización del indicador, desagregándo las resoluciones emitidas en materia de violencia -conforme resolución del STJER-, se efectúa a partir del 01/06/19 por lo que las cantidades informadas en boletines anteriores para los meses de febrero-abril pueden variar.  

Adicionalmente se detallan los Órganos que migraron de versión de Lex: el Juzgado de Paz de **Crespo** informó en ésta sección hasta Mayo 2019 y los Juzgados de Paz de **Oro Verde** y **San Benito** hasta Junio 2019. Los períodos posteriores se representan en la Sección correspondiente a los Juzgados de Paz de 1er Cat.

```{r, paz23_cadr_t, eval= (cr & t)}
resoluciones_paz_23(
  poblacion = jdos_paz_23,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Resoluciones y Vencimientos") %>%
  column_spec(c(4,8,9,10), background = "#BBBBBB") %>%
  row_spec(0, angle = 90)  %>%
  landscape()
```

## Movimientos y Resoluciones en materia de Violencia `r getDataIntervalStr(start_date, end_date)`

En esta tabla se presentan movimientos en materia de Violencia informadas por el REJUCAV dependiente del Centro Judicial de Género. Según la información provista por el registro en la tabla se muestran las *nuevas denuncias, medidas y resoluciones/sentencias.*

```{r, paz23_cadr_vt, eval= (cr & t)}
movimientos_violencias(jdos_paz_23, start_date, end_date) %>%
  filter(str_detect(juzgado, "Segunda|Tercera|Oro")) %>% 
  janitor::adorn_totals("row") %>% 
  outputTable(caption = "Movimientos en materia de Violencia") %>%
  landscape()

```

\pagebreak

`r if (cr & pc) '## Listado Primario de Datos'`

```{r, paz23_cadr_pc_intro, eval= (cr & pc),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("En este listado usted puede encontrar todas las causas resueltas informadas al APGE en el período considerado. Esta consulta se realiza a la base de datos de información primaria del organismo, datos a los que usted puede acceder a través del módulo de presentación del sistema JUSTAT.Para una fácil revisión de las cantidades informadas agregamos columnas donde se identifica individualmente el proceso computado en cada uno de los indicadores asociados a causas resueltas: resueltas a termino, resueltas luego del 1er vencimiento y resueltas luego del 2do vencimiento. Finalmente agregamos una última columna que señaliza la presencia de error o inconsistencia en el registro del 2do vencimiento (normalmente asociado a la declaración de una fecha mayor a la declarada respecto del primero)")
```


```{r, paz23_cadr_pc, eval= (cr & pc)}

resoluciones_cco_prim(poblacion = jdos_paz_23,
                      operacion = "CADRA23",
                      start_date = start_date,
                      end_date = end_date) %>% 
  kable(caption = "Causas Resueltas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```

