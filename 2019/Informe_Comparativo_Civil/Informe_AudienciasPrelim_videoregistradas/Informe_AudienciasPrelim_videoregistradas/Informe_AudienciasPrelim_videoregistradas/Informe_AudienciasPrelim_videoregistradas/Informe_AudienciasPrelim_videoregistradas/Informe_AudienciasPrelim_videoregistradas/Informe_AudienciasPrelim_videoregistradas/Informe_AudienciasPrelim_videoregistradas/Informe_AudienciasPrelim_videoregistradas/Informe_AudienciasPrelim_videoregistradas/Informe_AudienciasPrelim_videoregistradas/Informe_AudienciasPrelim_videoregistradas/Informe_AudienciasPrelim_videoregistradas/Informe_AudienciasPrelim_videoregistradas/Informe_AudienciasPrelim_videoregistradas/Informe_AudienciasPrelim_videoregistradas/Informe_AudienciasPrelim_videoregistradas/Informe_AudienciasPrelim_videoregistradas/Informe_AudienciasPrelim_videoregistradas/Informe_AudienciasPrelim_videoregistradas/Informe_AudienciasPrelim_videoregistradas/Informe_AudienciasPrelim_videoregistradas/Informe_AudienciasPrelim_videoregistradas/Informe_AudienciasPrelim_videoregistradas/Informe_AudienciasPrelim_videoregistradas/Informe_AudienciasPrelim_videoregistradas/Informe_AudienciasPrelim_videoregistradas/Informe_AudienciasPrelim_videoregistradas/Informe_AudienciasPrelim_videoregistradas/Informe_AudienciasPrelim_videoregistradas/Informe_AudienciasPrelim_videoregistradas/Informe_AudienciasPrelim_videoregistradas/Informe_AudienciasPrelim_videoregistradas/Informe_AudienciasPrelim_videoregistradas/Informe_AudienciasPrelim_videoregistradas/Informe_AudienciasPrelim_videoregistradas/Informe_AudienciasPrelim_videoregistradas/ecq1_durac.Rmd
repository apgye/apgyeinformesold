---
title: Duración de Procesos de Inicio a Sentencia
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}

library(CGPfunctions)

dur <- duracion(poblacion = jdos_ecq, start_date = start_date, end_date = end_date)
```

## Duracion de Procesos desde su Inicio hasta la Sentencia `r getDataIntervalStr(start_date, end_date)`

Los siguientes gráficos con cortes mensuales muestran las medianas de duración de procesos EJECUTIVOS de cada organismo. Se toma como referencia de la duración la fecha de inicio del proceso y la fecha de la sentencia contada en días corridos.    


```{r}
dur %>% 
  filter(cantidad_casos > 10) %>% 
  filter(str_detect(tipo_proceso, "MONITORIO")) %>% 
  filter(!str_detect(organismo, "sec.CQuiebras")) %>% 
  #mutate(organismo_abb = paste0("Jdo", str_remove_all(organismo, "[[:alpha:]]+"))) %>% 
  mutate(organismo_abb = abbreviate(organismo, minlength = 8, use.classes = T)) %>% 
  filter(str_detect(tipo_proceso, "EJECUTIVO")) %>% 
  newggslopegraph(mes, mediana_duracion, organismo_abb) +
   labs(title=" ", 
        subtitle="", 
        caption=" ", 
        WiderLabels = T,
        DataTextSize = 1)
```

<!-- ### Apremios -->

<!-- ```{r} -->
<!-- dur %>%  -->
<!--   filter(str_detect(tipo_proceso, "APREMIO")) %>%  -->
<!--   newggslopegraph(mes, mediana_duracion, organismo_abb) + -->
<!--    labs(title="Duración de Procesos en días corridos desde Inicio hasta Sentencia",  -->
<!--         subtitle="Fuero Ejecución ",  -->
<!--         caption="APGE-STJER") -->
<!-- ``` -->

\pagebreak

## Relación entre cantidad de casos y su duración por organismo.

En este gráfico vemos la correlación entre cantidad de casos y sus respectivas duraciones por organismo. Como es de esperar, esta correlación es positiva y fuerte: a medida que la cantidad de casos aumenta aumentan las duraciones por proceso. En este caso la variable que opera sobre esta relación pasa por los *recursos*.   


```{r}
dur %>% 
  filter(cantidad_casos > 10) %>% 
  filter(str_detect(tipo_proceso, "MONITORIO")) %>% 
  filter(!str_detect(organismo, "sec.CQuiebras")) %>% 
  #mutate(organismo_abb = paste0("Jdo", str_remove_all(organismo, "[[:alpha:]]+"))) %>% 
  mutate(organismo_abb = abbreviate(organismo, minlength = 8, use.classes = T)) %>% 
  filter(str_detect(tipo_proceso, "EJECUTIVO")) %>% 
  ggplot(aes(x = cantidad_casos, y = mediana_duracion)) +
  geom_point(aes(colour = factor(organismo)), size = 4) +
  geom_smooth(method = "lm") +
  scale_color_brewer(palette="Spectral") 

```


\pagebreak

## Tabla de Duraciones Procesos (Monitorios) Ejecutivos `r getDataIntervalStr(start_date, end_date)`

Medianas en días corridos. Se descartaron tipos de procesos con menos de diez casos en cada mes.

```{r}

dur %>%
  mutate(tipo_proceso = str_sub(tipo_proceso, 1, 30)) %>% 
  arrange(organismo, mes, desc(mediana_duracion)) %>% 
  filter(cantidad_casos > 10) %>% 
  filter(str_detect(tipo_proceso, "MONITORIO")) %>% 
  filter(str_detect(tipo_proceso, "EJECUTIVO")) %>% 
  # kable(caption = "Duración de Procesos", align = 'c', longtable = TRUE ) %>%
  # kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
  #               full_width = F, font_size = 10)  %>%
  outputTable(caption = "Duración de Procesos") %>%
  row_spec(0, angle = 90)

```

