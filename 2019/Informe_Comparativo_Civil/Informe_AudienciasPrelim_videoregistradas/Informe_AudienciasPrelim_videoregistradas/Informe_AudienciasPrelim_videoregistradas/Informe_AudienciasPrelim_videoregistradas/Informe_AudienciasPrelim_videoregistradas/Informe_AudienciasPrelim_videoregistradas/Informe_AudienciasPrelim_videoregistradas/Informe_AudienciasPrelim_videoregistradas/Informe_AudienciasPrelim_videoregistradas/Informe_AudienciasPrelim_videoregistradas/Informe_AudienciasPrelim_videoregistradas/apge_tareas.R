# reporte anual de tareas TAIGA

apge_tareas <- function() {
  
  library(data.table)
  library(curl)
  url <- "https://api.taiga.io/api/v1/userstories/csv?uuid=7df09efbbfd742009a2f1479c6df23ed"
  apge <- fread(url)
  
  apge <- apge %>% 
    select(ref, status, is_closed, tags, created_date, modified_date, 
           finish_date, subject, description, assigned_to_full_name) %>% 
    mutate(created_date = lubridate::ymd(str_sub(created_date, 1,10)),
           modified_date = lubridate::ymd(str_sub(modified_date, 1,10)), 
           finish_date = lubridate::ymd(str_sub(finish_date, 1,10))) %>% 
    mutate(finish_date = if_else(is.na(finish_date), modified_date, finish_date)) %>% 
    filter(year(created_date) >= year(Sys.Date())) %>% # reporta solamente tareas del año de solicitud del informe
    mutate(created_m = lubridate::month(created_date, label = T), 
           finish_m = lubridate::month(finish_date, label = T),
           modified_m = lubridate::month(modified_date, label = T))
  
  resultado <- list()
  
  resultado$apge_resueltos <- apge %>% 
    filter(status == "Closed") %>% 
    mutate(plazo = case_when(created_date == finish_date ~ "24 hs.",
      finish_date - created_date <= 5 ~ "entre 2 y 5 días",
      finish_date - created_date > 5 ~ "más de 5 días",
      TRUE ~ "sin_dato")) %>% 
    group_by(finish_m, plazo) %>% 
    summarise(cantidad = n()) %>% 
    ungroup() %>% 
    spread(key = plazo, value = cantidad, fill = 0) %>% 
    janitor::adorn_totals("col") %>% 
    select(Mes = finish_m, 'Resueltos por mes' = Total, everything()) %>% 
    janitor::adorn_totals("row")
  
  resultado$apge_iniciados <- apge %>% 
    filter(status != "Closed") %>% 
    group_by(status, created_m) %>% 
    summarise(cantidad = n()) %>% 
    ungroup() %>% 
    spread(key = status, value = cantidad, fill = 0) %>% 
    janitor::adorn_totals("col") %>% 
    select(Mes = created_m, 'Iniciados por mes' = Total, Pendientes = Open,
           En_progreso = 'In Progress', En_validacion = Testing) %>% 
    janitor::adorn_totals("row")
             
  resultado$apge_iniciados_cref <- apge %>% 
    filter(status != "Closed") %>% 
    group_by(status, created_m) %>% 
    summarise(cantidad = n(), 
              referencia_TG = str_c(ref, collapse = ", ")) %>% 
    ungroup() %>% 
    mutate(status = case_when(status == 'In Progress' ~ "En_progreso", 
                              status == "Open" ~ "Pendientes", 
                              status == 'Testing' ~ "En_validacion")) %>% 
    select(estado = status, mes_inicio = created_m, cantidad, 
           'nro. de referencia para consulta on line' = referencia_TG)
  

  resultado
  
}

