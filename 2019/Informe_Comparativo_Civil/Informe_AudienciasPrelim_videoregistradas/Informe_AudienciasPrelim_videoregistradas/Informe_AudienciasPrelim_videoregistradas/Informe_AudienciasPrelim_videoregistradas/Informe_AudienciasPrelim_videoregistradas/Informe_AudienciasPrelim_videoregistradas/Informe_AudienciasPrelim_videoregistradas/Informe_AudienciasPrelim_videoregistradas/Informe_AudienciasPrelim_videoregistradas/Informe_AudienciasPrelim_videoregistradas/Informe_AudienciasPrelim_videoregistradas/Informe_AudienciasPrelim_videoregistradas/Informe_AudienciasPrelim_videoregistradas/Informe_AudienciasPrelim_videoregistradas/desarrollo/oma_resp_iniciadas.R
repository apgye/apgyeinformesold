# TAIGA 817 - "OMA - Cambiar el procesamiento"
library(dplyr)
library(stringr)

# Busqueda de Responsable de Iniciadas OMA
# En Colon y Concordia, por compartir el Lex, no registran en 'resp'.

# Dicen de buscar en campo auxiliar 4, pero no se encuentra.
# Se encontró dentro del registro "REGISTRO MODELO - MEDIACION", el apellido del 
# Mediador en el texto 6

poblacion <- oma
start_date <- "2019-06-01"
end_date <- "2019-07-01"

inic <- DB_PROD() %>% 
  apgyeTableData("IGMP") %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% !!poblacion$organismo) %>% 
  mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
  filter(finicio >= data_interval_start, finicio < data_interval_end ) %>% 
  filter(finicio >= lubridate::make_date(2019,03,01)) %>% 
  collect() %>% 
  left_join(poblacion %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") 

# CC = Colón, Concordia
inic_cc <- inic %>% 
  filter(grepl("omapen0000col|omapen0000con", iep))

# index by the original count
inic_cc$index <- row.names(inic_cc)
inic_cc_aux <- inic_cc
inic_cc_aux <- inic_cc_aux %>%
  mutate(new_resp = fmov_treg_fres) %>% 
  separate_rows(new_resp, sep="%") %>% 
  group_by(index) %>% 
  filter(grepl("REGISTRO MODELO - MEDIACION",new_resp)) %>% 
  ungroup() %>% 
  separate_rows(new_resp, sep="\\$") %>% 
  group_by(index) %>% 
  filter(grepl("t6",new_resp)) %>% 
  ungroup() %>%
  separate_rows(new_resp, sep="\\:") %>% 
  group_by(index) %>% 
  filter(!grepl("t6",new_resp))

View(inic_cc)


# Excepción de reimputación de valor para variable responsable jurisdicción Concordia y Colon
inic$resp[inic$circunscripcion == "Colón"] <- inic$fiscm[inic$circunscripcion == "Colón"]
inic$resp[inic$circunscripcion == "Concordia" & inic$iep == "omapen0000con"] <- 
  inic$fiscm[inic$circunscripcion == "Concordia" & inic$iep == "omapen0000con"]


inic <- inic %>% 
  mutate(resp = toupper(resp)) %>% 
  select(circunscripcion, resp, everything()) %>% 
  group_by(circunscripcion, resp) %>% 
  summarise(cantidad_iniciados = n()) %>% 
  arrange(circunscripcion, desc(cantidad_iniciados)) %>% 
  rename(responsable = resp) %>% 
  mutate(responsable = ifelse(is.na(responsable), "sin dato", responsable)) %>% 
  do(janitor::adorn_totals(.))

inic
