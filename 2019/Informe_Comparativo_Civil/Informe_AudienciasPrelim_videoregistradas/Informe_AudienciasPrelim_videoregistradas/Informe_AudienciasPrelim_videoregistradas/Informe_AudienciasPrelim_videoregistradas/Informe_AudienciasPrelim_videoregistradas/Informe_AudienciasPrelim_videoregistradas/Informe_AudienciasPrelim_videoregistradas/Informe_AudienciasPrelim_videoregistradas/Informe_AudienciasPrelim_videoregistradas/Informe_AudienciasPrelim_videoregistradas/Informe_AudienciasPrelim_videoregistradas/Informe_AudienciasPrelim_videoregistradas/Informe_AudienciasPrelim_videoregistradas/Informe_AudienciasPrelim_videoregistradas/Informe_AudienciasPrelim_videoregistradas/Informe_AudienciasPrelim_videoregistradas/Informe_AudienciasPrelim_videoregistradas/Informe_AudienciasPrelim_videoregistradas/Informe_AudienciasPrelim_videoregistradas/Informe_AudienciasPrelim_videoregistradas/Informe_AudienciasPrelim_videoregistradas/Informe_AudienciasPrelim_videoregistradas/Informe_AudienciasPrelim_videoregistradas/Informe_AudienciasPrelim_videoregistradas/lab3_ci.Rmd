---
title: Causas Ingresadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## Causas Ingresadas - `r getDataIntervalStr(start_date, end_date)`

```{r, lab3_ci, eval= (ci & t) }
iniciados_sal(poblacion = sal_lab, 
              start_date = start_date,
              end_date = end_date, estadistico = "conteo") %>%
  iniciados_sal_comparativo(cfecha = desagregacion_mensual) %>% 
  kable(caption = str_c("Causas Ingresadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90) 

```

