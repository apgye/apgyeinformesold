
inic_mui <- DB_PROD() %>%
  apgyeDSL::apgyeTableData("CITIPM") %>%
  filter(iep  == "muistj0100pna") %>% 
  apgyeDSL::interval("2018-02-01", "2019-01-01") %>% 
  collect() %>% 
  filter(str_detect(Radicación, "9")) %>% 
  mutate(mes = lubridate::month(data_interval_start))

# muestra 0901 de febrero
inic_0901_2_mui <- inic_mui %>% 
  filter(str_detect(Radicación, "N°1"), mes == 2)

summary_inic_mui <- inic_mui %>% 
  group_by(Radicación, `tipo de proceso`) %>% 
  summarise(cantidad_iniciados = n()) 


summary_inic_mui_quiebra <- inic_mui %>% 
  group_by(Radicación, `tipo de proceso`) %>% 
  filter(str_detect(`tipo de proceso`, "^CONCURSO|^QUIEBRA|^PEDIDO DE QUIEBRA")) %>% 
  summarise(cantidad_iniciados = n()) %>% 
  janitor::adorn_totals("row")
  

# Extraccion por proc
start_date = "2018-01-01"
end_date = "2019-01-01"

# jdo 901 mes de muestra febreror 

inic_0901_2 <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CETAL_XL") %>% 
  filter(iep %in% jdos_ecq$organismo) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(fmov = dmy(fmov)) %>%
  filter(finicio >= start_date, finicio < "2018-07-01" ) %>% 
  group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
  filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
  ungroup()  %>%                                # para los casos en que se hacen pases por competencia
  select(iep, nro, caratula, tproc, finicio)  %>% collect() %>% 
  filter(str_detect(iep, "0901")) %>% 
  mutate(mes_inic = lubridate::month(finicio)) %>% 
  filter(mes_inic == 2)


# Segunda muestra


# Mui

inic_0901_7_mui <- inic_mui %>% 
  filter(str_detect(Radicación, "N°1"), mes == 7)

inic_0901_8_mui <- inic_mui %>% 
  filter(str_detect(Radicación, "N°1"), mes == 8)

inic_0901_9_mui <- inic_mui %>% 
  filter(str_detect(Radicación, "N°1"), mes == 9)


# Por proc

start_date = "2018-01-01"
end_date = "2019-01-01"


inic_proc <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CETAL_XL") %>% 
  filter(iep %in% poblacion$organismo) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(fmov = dmy(fmov)) %>%
  filter(finicio >= start_date, finicio < "2018-07-01" ) %>% 
  group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
  filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
  ungroup()  %>%                                # para los casos en que se hacen pases por competencia
  select(iep, nro, caratula, tproc, finicio)  %>% 
  union(
    DB_PROD() %>% 
      apgyeDSL::apgyeTableData("CINC1C") %>% 
      filter(iep %in% poblacion$organismo) %>% 
      filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(!is.na(finicio)) %>% 
      filter(finicio >= start_date, finicio < end_date ) %>% 
      select(iep, nro, caratula, tproc, finicio)  
  ) %>% 
  collect() %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  filter(iep != "jdocco0502con") %>%   # Anulo presentación duplicada %>% 
  mutate(mes_inic = lubridate::month(finicio)) 


inic_901_2_proc <- inic_proc %>% 
  filter(str_detect(iep, "0901")) %>% 
  filter(mes_inic == 2)

inic_901_3_proc <- inic_proc %>% 
  filter(str_detect(iep, "0901")) %>% 
  filter(mes_inic == 3)

inic_901_4_proc <- inic_proc %>% 
  filter(str_detect(iep, "0901")) %>% 
  filter(mes_inic == 4)

inic_901_5_proc <- inic_proc %>% 
  filter(str_detect(iep, "0901")) %>% 
  filter(mes_inic == 5)


inic_901_7_proc <- inic_proc %>% 
  filter(str_detect(iep, "0901")) %>% 
  filter(mes_inic == 7)

inic_901_8_proc <- inic_proc %>% 
  filter(str_detect(iep, "0901")) %>% 
  filter(mes_inic == 8)



# Se analizó el resultado del script de procesamiento para extracion de iniciados
# vía CETAL, comparándolo con la declaración del organismo sobre causas iniciadas principal y no principales
# teniendo un coincidencia en los exacta en al número en los meses de marzo, abril, mayo (a partir de junio empiza informe por listado)
# la diferencia en el caso de febrero es de una causa. Por lo tanto el procedimiento es consistente.
# No aparece consiste y por eso se lo deja de lado al informe de cuasa iniciadas por tipo de proceso, y el informe de causas iniciadas
# por MUI (que no tiene número)








