library(ggplot2)
library(ggmap)
library(RColorBrewer)
library(purrr)
library(stringr)
library(tibble)
library(leaflet)

# Mapa de ER con departamentos
erios <- readRDS(erios, file = "~/apgyeinformes/2019/Modelo_Parametrizado/data/erios.rds")

leaflet() %>%
  addTiles() %>%
  addProviderTiles("CartoDB.Positron") %>%
  # Mapeando los poligonos de Cordoba
  addPolygons(data = erios,
              color = "grey", weight = 1, smoothFactor = 0.5,
              opacity = 1.0)
