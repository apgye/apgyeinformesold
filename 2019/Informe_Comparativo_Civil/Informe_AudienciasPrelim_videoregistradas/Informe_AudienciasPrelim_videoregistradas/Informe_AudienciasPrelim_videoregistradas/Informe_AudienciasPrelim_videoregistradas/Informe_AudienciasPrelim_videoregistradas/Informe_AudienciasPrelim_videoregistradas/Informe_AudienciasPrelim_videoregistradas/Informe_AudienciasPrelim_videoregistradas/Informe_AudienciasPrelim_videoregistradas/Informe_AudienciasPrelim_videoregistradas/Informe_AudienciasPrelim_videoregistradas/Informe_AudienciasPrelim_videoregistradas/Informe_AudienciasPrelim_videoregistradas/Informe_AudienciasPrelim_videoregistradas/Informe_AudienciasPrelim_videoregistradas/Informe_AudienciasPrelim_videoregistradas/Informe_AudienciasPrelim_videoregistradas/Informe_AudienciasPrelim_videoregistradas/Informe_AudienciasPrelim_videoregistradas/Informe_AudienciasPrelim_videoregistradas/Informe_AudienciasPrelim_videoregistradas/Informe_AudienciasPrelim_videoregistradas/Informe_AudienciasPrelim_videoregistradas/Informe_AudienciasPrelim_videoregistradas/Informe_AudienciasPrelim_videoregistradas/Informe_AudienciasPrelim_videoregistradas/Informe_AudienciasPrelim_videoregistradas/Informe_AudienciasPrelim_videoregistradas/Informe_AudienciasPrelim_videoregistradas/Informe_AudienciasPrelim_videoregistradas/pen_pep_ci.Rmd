---
title: Casos Iniciados 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
resultado <- iniciadas_pen(poblacion = jdo_pep,
              start_date = start_date,
              end_date = end_date)
```

## Casos Iniciadas en Juzgado de Ejecución - `r getDataIntervalStr(start_date, end_date)`

`r if (ci & t) '### Agrupados por Órgano de Ejecución'`

```{r, pen_pep_ci_t, eval= (ci & t)}
resultado$inic_xorg %>% 
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
  
```

\pagebreak

`r if (ci & td) '### Agrupados por Tipo de Delito'`

```{r, pen_pep_ci_td1, eval= (ci & td)}
resultado$inic_tproc %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  landscape()
  
```

`r if (ci & td) '### Agrupados por Tipo de Pena'`

```{r, pen_pep_ci_td2, eval= (ci & td)}
resultado$inic_tpena %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
  
```

\pagebreak
`r if (ci & pc) '### Listado Primario de Datos'`

```{r, pen_pep_ci_pc, eval= (ci & pc)}
resultado$inic_pep_pc %>% 
  kable(caption = str_c("Casos Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  column_spec(3, "8cm") 
  
```

