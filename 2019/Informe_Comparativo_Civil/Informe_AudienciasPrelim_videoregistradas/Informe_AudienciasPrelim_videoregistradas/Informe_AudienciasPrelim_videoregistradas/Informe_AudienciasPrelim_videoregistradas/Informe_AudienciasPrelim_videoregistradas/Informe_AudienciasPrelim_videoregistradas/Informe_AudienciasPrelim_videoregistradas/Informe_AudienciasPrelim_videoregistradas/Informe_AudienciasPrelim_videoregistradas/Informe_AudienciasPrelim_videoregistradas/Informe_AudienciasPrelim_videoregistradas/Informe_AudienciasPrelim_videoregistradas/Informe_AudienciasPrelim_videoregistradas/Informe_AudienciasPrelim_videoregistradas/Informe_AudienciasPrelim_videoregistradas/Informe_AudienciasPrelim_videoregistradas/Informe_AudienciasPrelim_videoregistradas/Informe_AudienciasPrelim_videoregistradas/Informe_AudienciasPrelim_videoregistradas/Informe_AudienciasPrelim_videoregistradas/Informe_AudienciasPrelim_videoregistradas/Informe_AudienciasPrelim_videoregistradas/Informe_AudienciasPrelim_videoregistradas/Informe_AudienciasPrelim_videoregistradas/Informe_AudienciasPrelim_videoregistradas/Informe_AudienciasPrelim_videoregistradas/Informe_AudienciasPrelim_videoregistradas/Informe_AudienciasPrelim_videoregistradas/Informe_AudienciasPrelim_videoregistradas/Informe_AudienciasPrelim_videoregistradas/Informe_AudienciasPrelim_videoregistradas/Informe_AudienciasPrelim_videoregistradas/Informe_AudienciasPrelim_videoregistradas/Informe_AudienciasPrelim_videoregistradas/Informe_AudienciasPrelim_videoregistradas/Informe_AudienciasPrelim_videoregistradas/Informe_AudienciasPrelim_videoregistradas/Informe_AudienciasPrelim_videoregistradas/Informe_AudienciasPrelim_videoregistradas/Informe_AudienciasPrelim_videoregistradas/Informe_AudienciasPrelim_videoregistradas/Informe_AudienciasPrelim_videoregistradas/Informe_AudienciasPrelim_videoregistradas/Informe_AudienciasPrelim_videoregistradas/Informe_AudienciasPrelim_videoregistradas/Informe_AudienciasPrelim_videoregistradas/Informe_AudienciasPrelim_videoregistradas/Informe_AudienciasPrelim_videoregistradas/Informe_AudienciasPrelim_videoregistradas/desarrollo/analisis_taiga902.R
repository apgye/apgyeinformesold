#Obtenemos inic luego de correr PROC manualmente.
library(dplyr)
View(poblacion)

poblacion <- jdos_cco[jdos_cco$circunscripcion == "Uruguay",]
start_date <- "2019-02-01"
end_date <- "2019-08-01"

if(lubridate::ymd(start_date) > lubridate::make_date(2018,07,01)) {
  inic <- inic_xcetal1sem18(poblacion, "CINC1C")
  #inic
} else {
  inic <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData("CINC1C") %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(finicio = dmy(finicio)) %>%
    filter(!is.na(finicio)) %>% 
    filter(finicio >= start_date, finicio < end_date ) %>% 
    select(iep, nro, caratula, tproc, finicio) %>% 
    collect() 
  inic
}

nrow(inic)
#[1] 446

inic <- inic %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  agrupartproc(materia = "civil") 

nrow(inic)
#[1] 436

inic_prim <- inic # actualizacion para obtener primarias para datos abiertos
View(inic)

inic_Uru_mes <- inic %>%
  mutate(fecha = lubridate::ceiling_date(finicio, "month") - lubridate::days(1)) %>%  
  group_by(iep, fecha) %>% 
  summarise(cant_mes = n()) %>% 
  left_join(poblacion %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  mutate(nroMes = month(fecha)) %>% 
  select(circunscripcion, organismo, nroMes, cant_mes, -iep, -fecha)

View(inic_Uru_mes)


inic <- inic %>% 
  mutate(fecha = lubridate::ceiling_date(finicio, "month") - lubridate::days(1)) %>%  
  group_by(iep, gtproc_forma, tproc, fecha) %>% 
  summarise(cantidad = n()) %>% 
  arrange(gtproc_forma, desc(cantidad)) %>% 
  do(janitor::adorn_totals(.)) %>% 
  ungroup() %>% 
  mutate(tproc = ifelse(gtproc_forma == "-", "subtotal", tproc)) %>% 
  left_join(poblacion %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  select(circunscripcion, organismo, grupo_proceso = gtproc_forma, tipo_proceso = tproc, everything(), -iep)


# inic_Uru_mes <- inic %>% 
#   ungroup() %>% 
#   filter(!grupo_proceso == "-") %>% 
#   group_by(fecha) %>% 
#   summarise(cant_mes = sum(cantidad))

View(inic)
