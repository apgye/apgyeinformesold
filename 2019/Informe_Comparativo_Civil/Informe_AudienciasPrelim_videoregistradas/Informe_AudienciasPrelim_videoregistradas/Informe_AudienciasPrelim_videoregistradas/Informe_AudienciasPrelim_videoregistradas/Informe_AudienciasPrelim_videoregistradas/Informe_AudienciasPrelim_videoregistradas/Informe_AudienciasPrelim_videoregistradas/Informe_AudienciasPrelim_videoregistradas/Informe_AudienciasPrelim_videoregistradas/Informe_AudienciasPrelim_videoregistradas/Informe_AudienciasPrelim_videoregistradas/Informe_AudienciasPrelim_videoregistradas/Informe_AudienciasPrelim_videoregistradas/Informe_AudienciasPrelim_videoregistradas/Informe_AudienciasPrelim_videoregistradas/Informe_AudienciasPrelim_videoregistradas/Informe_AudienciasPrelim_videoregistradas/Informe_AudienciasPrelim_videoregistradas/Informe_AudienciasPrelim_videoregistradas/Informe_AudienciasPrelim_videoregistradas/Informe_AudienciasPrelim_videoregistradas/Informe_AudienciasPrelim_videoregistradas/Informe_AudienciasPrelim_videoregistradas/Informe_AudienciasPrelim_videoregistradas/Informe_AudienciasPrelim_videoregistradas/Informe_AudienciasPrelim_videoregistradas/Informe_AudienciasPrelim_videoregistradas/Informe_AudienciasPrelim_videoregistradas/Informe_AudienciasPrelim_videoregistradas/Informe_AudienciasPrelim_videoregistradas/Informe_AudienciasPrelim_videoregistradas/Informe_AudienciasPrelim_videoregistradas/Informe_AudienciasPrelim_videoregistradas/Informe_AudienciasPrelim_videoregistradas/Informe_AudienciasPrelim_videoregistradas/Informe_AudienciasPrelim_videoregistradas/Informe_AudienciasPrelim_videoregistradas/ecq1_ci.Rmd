---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
resultado <- iniciados_ecq(poblacion = jdos_ecq,
              start_date = start_date,
              end_date = end_date, estadistico = "conteo")
```


## Causas Iniciadas en materia de Ejecuciones, Concursos y Quiebras - `r getDataIntervalStr(start_date, end_date)`

El Juzgado Civil y Comercial Nº5 de Concordia informa sus causas iniciadas desagregadas por secretaría a partir del mes de abril de 2019, para períodos anteriores las causas iniciadas corresponden a ambas secretarías.   

```{r, ecq1_ci, eval = (ci & t)}
resultado$inic %>% 
  iniciados_ecq_comparativo(cfecha = desagregacion_mensual) %>% 
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```


```{r, ecq1_st, eval = (ci & st)}
resultado$inic_ecq_pc %>% 
  tsci_g(finicio, org_facet = T)

```

\pagebreak 

```{r, ecq1_ci_g, eval = (ci & g), echo=FALSE}
resultado$inic %>% 
  iniciados_ecq_xg() %>%
  inic_ecq_graf_xg()
```


