---
title: MINJUS-STJER Datos Abiertos
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


# Situación Institucional en relación al Convenio de Datos Abiertos  

Pronunciamiento Pendiente del STJER sobre Versión 2 del Protocolo, nota elevada por Castillo en fecha 19/12/17.

El proyecto de acceso a BD de todas las jurisdicciones a cargo del Área de Informática (Acta Complementario Nº1 sobre Proyecto “TABLEROS DE CONTROL”, AG23/17 23-08-17 y acta suscripta por Dr. Emilio A. E. Castrillon en fecha 19/04/18)  está en un 60% avance: jurisdicciones accedidas Paraná, Gualeguaychú, Feliciano y La Paz. Perspectiva de finalización año 2020.

# Información Disponible (y su estado) para Entrega de Datos al MINJUS

Revisión de los requerimientos del PTDP2 y la disponibilidad de información que administra el APGE relevada a partir de las operaciones estadísticas vigentes. 

La entrega de informción en las condiciones de relevamiento actuales deberían ajustarse a las siguientes pautas:

+ Establecimiento de Ventana de Reimputación para órganos informantes limitada al último mes informado.   
+ Informar a dos meses vencidos.    
+ Validación rígida de datos críticos (tproc).   


**Tablas Estructurales:**      

+ A. TABLA DE DATOS ESTRUCTURALES (accesible)
+ B. TABLA DE DATOS RECURSOS HUMANOS (accesible)
+ C.1 TABLA DE DATOS DE PRESUPUESTO (accesible)
+ C.2. PRESUPUESTO - SALARIO MENSUAL BRUTO (**STJER no autoriza remisión de datos**)
+ D. TABLA DE DATOS DE MEDIACIÓN y CONCILIACIÓN (accesible)

**Tablas No Penales:**      

+ TABLA 3 CAUSAS INICIADAS NO PENALES: **acceso 75%**, con acceso incommpleto a las partes.   
+ TABLA 3.1. PERSONAS FÍSICAS Y JURÍDICAS: **acceso incompleto** a las partes.       
+ TABLA 4 MOVIMIENTOS PROCESALES NO PENALES: **acceso 100%**, considerando estado no-normalizado movimientos.   
+ TABLA 4.1 AUDIENCIAS: **acceso 100%**.       
+ TABLA 4.2 RECURSOS NO PENALES: **acceso 90%**, en diseño para implementar la recolección de "nros" en 1a y 2a instancia.       

**Tablas Penales:**    

+ TABLA 5 CAUSAS INICIADAS PENALES: **acceso 80%**, en diseño para implementar la recolección de "nros" MPF.       
+ TABLA 5.1 CAUSAS-DELITOS: **acceso incompleto y no normalizado**.    
+ TABLA 6 MOVIMIENTOS PROCESALES PENALES: **acceso 100%**, considerando clasificación propia (discrepa parcialmente PTDP2).    
+ TABLA 6.1 PERSONAS-CAUSA: **acceso 60%**, considerando la clasificación x sexo a partir de análisis.Sin acceso a edades (salvo menor o mayor edad).    
+ TABLA 6.2 RECURSOS PENALES: **acceso 100%**, sujeto a revisión de clasificación.      

# Cronograma de trabajo APGE

+ Junio: Informe Institucional JUFEJUS (y Actualizacion Operaciones Justat)
+ Julio: Informe MINJUS Oralidad (y Actualizacion Operaciones Justat)
+ Agosto: Tablero
+ Septiembre: Datos Abiertos (Script de Consulta)
+ Octubre: 1er. Entrega Datos Abiertos

\pagebreak

# Detalle Particular por tabla primarias:

## Tabla 3 CAUSAS INICIADAS NO PENALES

Variables: 

+ id_causa
+ materia: 
  + CP - Civil patrimonial [incluye minería]
  + CO - Comercial
  + CQ - Concursos y Quiebras
  + CA - Contencioso administrativo
  + TF - Tributario Fiscal
  + FC - Familia — procesos contenciosos
  + FP - Civil-Familia: personas, NNA — procesos de protección de derechos, voluntarios
  + FS - Familia — sucesiones
  + FV - Familia — violencia familiar y de genero
  + PZ - Paz
  + A - Amparo
  + LA - Laboral
+ id_circunscripcion
+ id_usoj
+ fecha_inicio
+ objeto_litigio
+ cantidad_actores      (SIN ACCESO A DATO, accesible: uno o múltiples (procesar caratula))
+ cantidad_demandados   (SIN ACCESO A DATO, accesible: uno o múltiples (procesar caratula))

## Tabla 3.1  PERSONAS FÍSICAS Y JURÍDICAS

Variables: 

+ id_causa
+ id_uosj
+ persona_tipo: PF = persona física o PJ = persona jurídica (DISPONIBLE, GRADO DE IMPLEMENTAC. INCIERTO)
+ partes: rol procesal: AC = actor, DE = demandado, CG = citado en garantía
+ descripcion1: edad o clasificación tipo de PJ (PODRÍA INSTRUMENTARSE X REGEX)
+ descripcion2: sexo o CUIT

# TABLA 4 MOVIMIENTOS PROCESALES NO PENALES   

Variables:   

+ id_causa
+ id_uosj
+ fecha_acto_procesal
+ acto_procesal
-   Demanda/contestacion demanda (x regex)
-   Mediacion
-   Audiencia Preliminar
-   Apertura a Prueba
-   Audiencia Vista Causa
-   Autos para Sentencia
-   Sentencia por tipo (cfrme. guía (ojo tabla minjus) y Sentencias Sucesorio y Quiebras)
-   Archivo
-   Mero Trámite (todo lo demás)
-   Apelación
+ estado_procesal
-   resuelto
-   en trámite
-   archivado
-   mediación previa obligatoria

## TABLA 4.1 AUDIENCIAS   

Variables:  

+ id_causa
+ id_uosj
+ fecha_convocatoria
+ fecha_realizacion
+ tipo_audiencia
+ estado_audiencia

## TABLA 4.2 RECURSOS NO PENALES

Variables

+ uosj_alzada
+ Id_causa_alzada (para revisar)
+ uosj_origen
+ id_causa_origen (para revisar)
+ descripcion_recurso
+ fecha_acto_procesal
+ acto_procesal
+ estado_procesal

## TABLA 5 CAUSAS INICIADAS PENALES

Variables:   

+ id_causa_mp (sin acceso)
+ id_unidad_mp (sin acceso)
+ id_causa
+ id_circunscripcion
+ id_uosj
+ fecha_hecho
+ fecha_inicio

## TABLA 5.1 CAUSAS-DELITOS

Variables:

+ id_causa
+ id_uosj
+ codigo_delito (acceso parcial, no normalizado)
+ tentativa

## TABLA 6 MOVIMIENTOS PROCESALES PENALES

Variables:

+ id_causa
+ id_uosj
+ item_causa
+ codigo_delito
+ tentativa
+ flagrancia
+ codigo_acto_procesal
+ fecha_acto_procesal

## TABLA 6.1 PERSONAS-CAUSA

Variables

+ id_causa
+ id_uosj 
+ item_causa
+ persona_sexo (sin acceso por clasificación, explorable por análisis)
+ persona_edad (sin acceso)

## TABLA 6.2 RECURSOS PENALES

Variables:   

+ uosj_alzada
+ Id_causa_alzada 
+ uosj _origen 
+ id_causa_origen 
+ descripcion_recurso 
+ codigo_delito 
+ acto_procesal 
+ fecha_acto_procesal
