---
title: Causas Resueltas - Listado Primario
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Listado Primario de Datos `r getDataIntervalStr(start_date, end_date)`

En este listado usted puede encontrar todas las causas resueltas informadas al APGE en el período considerado. Esta consulta se realiza a la base de datos de información primaria del organismo, datos a los que usted puede acceder a través del módulo de presentación del sistema JUSTAT.   

Para una fácil revisión de las cantidades informadas agregamos columnas donde se identifica individualmente el proceso computado en cada uno de los indicadores asociados a causas resueltas: resueltas a termino, resueltas luego del 1er vencimiento y resueltas luego del 2do vencimiento. Finalmente agregamos una última columna que señaliza la presencia de error o inconsistencia en el registro del 2do vencimiento (normalmente asociado a la declaración de una fecha mayor a la declarada respecto del primero)


```{r}

resoluciones_lab_prim(poblacion = cam_lab,
                      operacion = "CADR2L",
                      start_date = start_date,
                      end_date = end_date) %>% 
  kable(caption = "Causas Resueltas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```
