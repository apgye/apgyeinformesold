
resoluciones_pen <- function(poblacion, start_date="2019-02-01", end_date = "2019-03-01", desagregacion_mensual = TRUE, cfecha = F) {
  
  
  if(any(str_detect(poblacion, "oga"))) {
    
    
    resolpg <- DB_PROD() %>% apgyeDSL::apgyeTableData(RESOLPG) %>%
      apgyeDSL::interval(start_date, end_date) %>%
      filter(iep %in% !!poblacion$organismo) %>% 
      left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                  mutate(
                    magistrado = apellido,
                    #magistrado = paste0(apellido, ', ', nombres),
                    iep_actuante = iep
                  ) %>%
                  select(idagente, iep_actuante, magistrado), by=c("jact"="idagente") )
    
    if(desagregacion_mensual) {
      resolpg <- resolpg %>% group_by(iep, magistrado, data_interval_start, data_interval_end) 
    } else {
      resolpg <- resolpg %>% group_by(iep, magistrado) 
    }
    
    resolpg <- resolpg %>% 
      process()
    
    result <- resolpg %>% .$result
    
    # Quitamos las columnas de Resoluciones
    resultado_actosConcl <- result %>% 
      select(-c("suspension_proc_prueba", "orden_detencion", "med_protec_victim",
                "med_coercion", "prision_prev", "sustitutiva_prision_prev",
                "competencia", "prorroga_ipp", "remision_jdo_menores", 
                "formulacion_cargos", "revision_medidas", "revision_reglas_conducta",
                "allanamientos", "otros"))
    
    # Quitamos las columnas de Actos Conclusivos
    resultado_resoluciones <- result %>% 
      select(-c("sobreseimientos", "sob_desestimado", "sob_extinc_prescrip",
                "sob_extinc_fallecim", "sob_extinc_otras", "sob_suspension",
                "sob_mediacion", "sob_atip_justif", "sob_otras",
                "juicios_abrev", "condena_efectiva",
                "condena_efectiva_sustit", "condena_condicional",
                "medida_seguridad", "elevacion_juicio", "sobreseidos", "condenados"))
    
    ## Consultamos los Allanamientos por la operación ALLI 
    if (any(poblacion$organismo == "ogapen0000pna")) {
     all <- DB_PROD() %>% apgyeDSL::apgyeTableData(ALLI) %>%
      apgyeDSL::interval(start_date, end_date) %>%
      filter(iep %in% !!poblacion$organismo)
    
    if(desagregacion_mensual) {
      resol_all <- all %>% group_by(iep, magistrado, data_interval_start, data_interval_end) 
    } else {
      resol_all <- all %>% group_by(iep, magistrado) 
    }
    
    resol_all <- resol_all %>% 
      process(ALLI)
    
    resol_all <- resol_all$result
    
    allanam <- data.frame(
      "iep" = resol_all$iep,
      "magistrado" = "-",
      "data_interval_start" = resol_all$data_interval_start,
      "data_interval_end" = resol_all$data_interval_end,
      "jact" = 0,
      "suspension_proc_prueba" = 0,
      "orden_detencion" = 0,
      "med_protec_victim" = 0,
      "med_coercion" = 0, 
      "prision_prev" = 0,
      "sustitutiva_prision_prev" = 0,
      "competencia" = 0, 
      "prorroga_ipp" = 0,
      "remision_jdo_menores" = 0, 
      "formulacion_cargos" = 0, 
      "revision_medidas" = 0,
      "revision_reglas_conducta" = 0,
      "allanamientos" = resol_all$allanamientos,
      "otros" = 0,
      "calidad_primaria" = resol_all$calidad_primaria,
      "calidad" = resol_all$calidad_primaria
    )
    colnames(allanam) <- colnames(resultado_resoluciones)
    
    if(nrow(allanam) >= 1){
      resultado_resoluciones <- resultado_resoluciones %>%
        bind_rows(allanam)
    }
    }
    ## Fin consulta Allanamientos
    
    if(desagregacion_mensual) {
      
      if (nrow(result) != 0) {
        
        resultado_ac <- resultado_actosConcl %>%
          mutate(iep = as.character(iep)) %>% 
          mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%
          left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
          select(circunscripcion, mes, magistrado, everything(), -iep, -jact, -calidad_primaria, -calidad, 
                 -data_interval_start, -data_interval_end) %>% 
          mutate(sobreseimientos = as.character(sobreseimientos), #Categorizamos para no totalizar
                 juicios_abrev = as.character(juicios_abrev), 
                 sobreseidos = as.character(sobreseidos), condenados = as.character(condenados)) %>% 
          select(-sobreseimientos, -juicios_abrev) %>% # complejiza entendimiento de la tabla
          mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                              "junio", "julio", "agosto", "septiembre", 
                                              "octubre", "noviembre", "diciembre", "Total"),
                              ordered = TRUE)) %>% 
          arrange(circunscripcion, mes) %>% 
          janitor::adorn_totals("col") %>% 
          group_by(circunscripcion) %>% 
          do(janitor::adorn_totals(.)) %>% 
          ungroup() %>% mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6))
        
        resultado_res <- resultado_resoluciones %>% 
          mutate(iep = as.character(iep)) %>% 
          mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%
          left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
          select(circunscripcion, mes, magistrado, everything(), -iep, -jact, -calidad_primaria, -calidad, 
                 -data_interval_start, -data_interval_end) %>% 
          mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                              "junio", "julio", "agosto", "septiembre", 
                                              "octubre", "noviembre", "diciembre", "Total"),
                              ordered = TRUE)) %>% 
          arrange(circunscripcion, mes) %>% 
          janitor::adorn_totals("col") %>% 
          group_by(circunscripcion) %>% 
          do(janitor::adorn_totals(.)) %>% 
          ungroup() %>% mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6))
        
      }
      
      actos_conclusivos_gtia_xtproc <- resolpg %>% .$res_xac
      
      if (nrow(actos_conclusivos_gtia_xtproc) != 0) {
        
        actos_conclusivos_gtia_xtproc <- actos_conclusivos_gtia_xtproc %>% 
          mutate(iep = as.character(iep)) %>% 
          mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%
          left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
          select(circunscripcion, mes, magistrado, everything(), -iep, -jact, -data_interval_start, -data_interval_end ) %>% 
          rename(tipo_actoconcl = codigos) %>% 
          mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                              "junio", "julio", "agosto", "septiembre", 
                                              "octubre", "noviembre", "diciembre", "Total"),
                              ordered = TRUE)) %>% 
          arrange(circunscripcion, mes) 
        
      }
      
      
    } else {
      
      resultado_ac <- resultado_actosConcl %>%
        left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>%
        select(circunscripcion, magistrado, everything(), -iep, -jact, -calidad_primaria, -calidad) %>%
        mutate(sobreseimientos = as.character(sobreseimientos), #Categorizamos para no totalizar
               juicios_abrev = as.character(juicios_abrev), 
               sobreseidos = as.character(sobreseidos), condenados = as.character(condenados)) %>% 
        select(-sobreseimientos, -juicios_abrev) %>% # complejiza entendimiento de la tabla
        janitor::adorn_totals("col") %>%
        group_by(circunscripcion) %>%
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6))

      resultado_res <- resultado_resoluciones %>% 
        mutate(iep = as.character(iep)) %>% 
        left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
        select(circunscripcion, magistrado, everything(), -iep, -jact, -calidad_primaria, -calidad, 
               -data_interval_start, -data_interval_end) %>% 
        arrange(circunscripcion) %>% 
        janitor::adorn_totals("col") %>% 
        group_by(circunscripcion) %>% 
        do(janitor::adorn_totals(.)) %>% 
        ungroup() %>% mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6))
      
      actos_conclusivos_gtia_xtproc <- resolpg %>% .$res_xac %>%

        if (nrow(actos_conclusivos_gtia_xtproc) != 0) {
          actos_conclusivos_gtia_xtproc <- actos_conclusivos_gtia_xtproc %>%
            left_join(oga %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>%
            select(circunscripcion, magistrado, everything(), -iep, -jact) %>%
            rename(tipo_actoconcl = codigos)
        }
    }
    
    
    actos_conclusivos_gtia_xtproc <<- actos_conclusivos_gtia_xtproc
    
    
    #if(desagregacion_mensual) { if(!cfecha) {resultado <- resultado %>% select(-fecha)} }
    
    resultado <- list()
    resultado$ac <- resultado_ac
    resultado$res <- resultado_res
    
    resultado
    
    
  } 
  
  else if(any(str_detect(poblacion, "tja"))) {
   
   
    poblacion_infte <- oga # organo informante OGA, organo informado Tribunal de Juicio
    
    
    # tribunal de Juicio----------------------------------------------
    
    resolpt <- DB_PROD() %>% apgyeDSL::apgyeTableData(RESOLPT) %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep %in% !!poblacion_infte$organismo) 
    
    if(desagregacion_mensual) {
      resolpt <- resolpt %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      resolpt <- resolpt %>% group_by(iep) 
    }
    
    resolpt <- resolpt %>% 
      process(RESOLPT) 
    
    if(desagregacion_mensual) {
      
      resultado <- resolpt %>% .$result %>% ungroup() %>% 
        mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>% 
        select(iep, vact, sobreseimientos, condena_efectiva, 
               condena_efectiva_sustit, condena_condicional, absoluciones, 
               medida_seguridad, otra, sobreseidos, condenados, absueltos, mes, fecha) %>% 
        mutate(sobreseidos = as.character(sobreseidos), #Categorizamos para no totalizar
               condenados = as.character(condenados),
               absueltos = as.character(absueltos)) %>% 
        left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
        select(circunscripcion, mes, everything())
      
      vact <- resultado %>% as.data.frame() %>%
        select(iep, vact) %>%
        dplyr::mutate(rn = row_number()) %>% separate_rows(vact) %>%
        left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                    mutate(
                      magistrado = apellido,
                      iep_actuante = iep
                    ) %>%
                    select(idagente, iep_actuante, magistrado), by=c("vact"="idagente"), copy = T) %>%
        group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
        left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
        ungroup() %>%
        mutate(magistrado = paste0(magistrado, '(', circunscripcion, ')')) %>% # traducir código html
        .$magistrado
      
      resultado$vact <- vact 
      
      resultado <- resultado %>%
        rename(magistrado = vact) %>%
        mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", "*")) %>% # código no confirmado SGP
        select(-iep) %>% 
        mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                            "junio", "julio", "agosto", "septiembre", 
                                            "octubre", "noviembre", "diciembre", "Total"),
                            ordered = TRUE)) %>% 
        janitor::adorn_totals("col") %>% 
        group_by(circunscripcion, mes) %>% 
        do(janitor::adorn_totals(.))
      
      
      # Actos Conclusivos por Tipo de proceso
      
      actos_conclusivos_tjui_xtproc <- resolpt %>% .$res_xactoconcl %>% 
        mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>% 
        left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
        select(circunscripcion, mes, everything(), -data_interval_start, -data_interval_end)
      
      vact <- actos_conclusivos_tjui_xtproc %>% as.data.frame() %>%
        select(iep, vact) %>%
        dplyr::mutate(rn = row_number()) %>% separate_rows(vact) %>%
        left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                    mutate(
                      magistrado = apellido,
                      iep_actuante = iep
                    ) %>%
                    select(idagente, iep_actuante, magistrado), by=c("vact"="idagente"), copy = T) %>%
        
        group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
        left_join(apgyeJusEROrganization::listar_organismos(), by=c("iep"="organismo") ) %>%
        ungroup() %>%
        mutate(magistrado = paste0(magistrado, '(', circunscripcion, ')')) %>% # traducir código html
        .$magistrado
      
      actos_conclusivos_tjui_xtproc$vact <- vact
      
      actos_conclusivos_tjui_xtproc <- actos_conclusivos_tjui_xtproc %>%
        rename(magistrado = vact) %>% 
        mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", "*")) %>%  # código no confirmado SGP
        select(-iep) %>% 
        mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                            "junio", "julio", "agosto", "septiembre", 
                                            "octubre", "noviembre", "diciembre", "Total"),
                            ordered = TRUE)) %>% 
        arrange(circunscripcion, mes)
      
      actos_conclusivos_tjui_xtproc <<- actos_conclusivos_tjui_xtproc
      
      if(desagregacion_mensual) { if(!cfecha) {resultado <- resultado %>% select(-fecha)} }
      
      resultado  
      
    } else {
      
      resultado <- resolpt %>% .$result %>%
        select(iep, vact,  sobreseimientos, condena_efectiva, 
               condena_efectiva_sustit, condena_condicional, absoluciones, 
               medida_seguridad, otra, sobreseidos, condenados, absueltos) %>% 
        mutate(sobreseidos = as.character(sobreseidos), #Categorizamos para no totalizar
               condenados = as.character(condenados),
               absueltos = as.character(absueltos)) %>% 
        left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
        select(circunscripcion, everything())
      
      vact <- resultado %>% as.data.frame() %>%
        select(iep, vact) %>%
        dplyr::mutate(rn = row_number()) %>% separate_rows(vact) %>%
        left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                    mutate(
                      magistrado = apellido,
                      iep_actuante = iep
                    ) %>%
                    select(idagente, iep_actuante, magistrado), by=c("vact"="idagente"), copy = T) %>%
        group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
        left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
        ungroup() %>%
        mutate(magistrado = paste0(magistrado, '(', circunscripcion, ')')) %>% # traducir código html
        .$magistrado
      
      resultado$vact <- vact 
      
      resultado <- resultado %>%
        rename(magistrado = vact) %>%
        mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", "*")) %>%  # código no confirmado SGP
        select(-iep) %>% 
        janitor::adorn_totals("col") %>% 
        group_by(circunscripcion) %>% 
        do(janitor::adorn_totals(.))
      
      
      # Actos Conclusivos por Tipo de proceso
      
      actos_conclusivos_tjui_xtproc <- resolpt %>% .$res_xactoconcl %>% 
       left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>% 
        select(circunscripcion, everything())
      
      vact <- actos_conclusivos_tjui_xtproc %>% as.data.frame() %>%
        select(iep, vact) %>%
        dplyr::mutate(rn = row_number()) %>% separate_rows(vact) %>%
        left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                    mutate(
                      magistrado = apellido,
                      iep_actuante = iep
                    ) %>%
                    select(idagente, iep_actuante, magistrado), by=c("vact"="idagente"), copy = T) %>%
        
        group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
        left_join(apgyeJusEROrganization::listar_organismos(), by=c("iep"="organismo") ) %>%
        ungroup() %>%
        mutate(magistrado = paste0(magistrado, '(', circunscripcion, ')')) %>% # traducir código html
        .$magistrado
      
      actos_conclusivos_tjui_xtproc$vact <- vact
      
      actos_conclusivos_tjui_xtproc <- actos_conclusivos_tjui_xtproc %>%
        rename(magistrado = vact) %>% 
        mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", "*")) %>%  # código no confirmado SGP
        select(-iep) 
      
      actos_conclusivos_tjui_xtproc <<- actos_conclusivos_tjui_xtproc
      
      resultado 
    
    }
    
   
     # tribunal de Apelacion-----------------------------------------------
    if (!any(poblacion_infte$circunscripcion == "La Paz")) {
    
      resolpa <- DB_PROD() %>% apgyeDSL::apgyeTableData('RESOLPA') %>%
        apgyeDSL::interval(start_date, end_date) %>%
        filter(iep %in% !!poblacion_infte$organismo)
      
      if (desagregacion_mensual){
        resolpa <- resolpa %>%  group_by(iep, data_interval_start, data_interval_end)
        } else {
          resolpa <- resolpa %>% group_by(iep)
        }
      
      resolpa <- resolpa %>%
        process(RESOLPA)
  
      if(desagregacion_mensual) {
  
        resolpa <- resolpa %>% .$res_xvact %>% ungroup() %>%
          mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%
          select(iep, vact, confirmatoria, revocatoria, desistimiento,
                 abstracto, mal_concedido, inadmisible, nulidad,
                 queja, otra, resol_vencidas = vencidos, mes, fecha) %>%
          mutate(resol_vencidas = as.character(resol_vencidas)) %>% #Categorizamos para no totalizar
          left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>%
          select(circunscripcion, mes, everything())
  
        vact <- resolpa %>% as.data.frame() %>%
          select(iep, vact) %>%
          dplyr::mutate(rn = row_number()) %>% separate_rows(vact) %>%
          left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                      mutate(
                        magistrado = apellido,
                        iep_actuante = iep
                      ) %>%
                      select(idagente, iep_actuante, magistrado), by=c("vact"="idagente"), copy = T) %>%
          group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
          left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
          ungroup() %>%
          mutate(magistrado = paste0(magistrado, '(', circunscripcion, ')')) %>% # traducir código html
          .$magistrado
  
        resolpa$vact <- vact
  
        resultado_tapel <- resolpa %>%
          rename(magistrado = vact) %>%
          mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", "*")) %>%  # código no confirmado SGP
          select(-iep) %>%
          mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo",
                                              "junio", "julio", "agosto", "septiembre",
                                              "octubre", "noviembre", "diciembre", "Total"),
                              ordered = TRUE)) %>%
          janitor::adorn_totals("col") %>% 
          group_by(circunscripcion, mes) %>%
          do(janitor::adorn_totals(.))
        
        
        if(desagregacion_mensual) { if(!cfecha) {resultado_tapel <- resultado_tapel %>% select(-fecha)} }
        
  
        resultado_tapel
        # Apelación por juez apelado
    
    

    } else {

      resolpa <- resolpa %>% .$res_xvact %>% ungroup() %>%
        select(iep, vact, confirmatoria, revocatoria, desistimiento,
               abstracto, mal_concedido, inadmisible, nulidad,
               queja, otra, resol_vencidas = vencidos) %>%
        mutate(resol_vencidas = as.character(resol_vencidas)) %>% #Categorizamos para no totalizar
        left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>%
        select(circunscripcion, everything())

      vact <- resolpa %>% as.data.frame() %>%
        select(iep, vact) %>%
        dplyr::mutate(rn = row_number()) %>% separate_rows(vact) %>%
        left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                    mutate(
                      magistrado = apellido,
                      iep_actuante = iep
                    ) %>%
                    select(idagente, iep_actuante, magistrado), by=c("vact"="idagente"), copy = T) %>%
        group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
        left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
        ungroup() %>%
        mutate(magistrado = paste0(magistrado, '(', circunscripcion, ')')) %>% # traducir código html
        .$magistrado

      resolpa$vact <- vact

      resultado_tapel <- resolpa %>%
        rename(magistrado = vact) %>%
        mutate(magistrado = str_replace_all(magistrado, " NA|NA |NA\\(", "*")) %>%  # código no confirmado SGP
        select(-iep) %>%
        janitor::adorn_totals("col") %>% 
        group_by(circunscripcion) %>%
        do(janitor::adorn_totals(.))

      resultado_tapel 

    }
    
    resultado_tapel <<- resultado_tapel
    }
    resultado
    
  } # tres salidas: resultado = res tribunal de jucio, resultado_tapel = tribunal de apel, actos_conclusivos_tjui_xtproc
  
  else if(any(str_detect(poblacion, "campen"))) {
    
    
    resultado <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADRC_v2') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep %in% !!poblacion$organismo) 
    
    if(desagregacion_mensual) {
      
      resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      
      resultado <- resultado %>% group_by(iep) 
      
    }
    
    resultado <- resultado %>% 
      process(CADRC_v2) %>% .$result %>% ungroup()
      
    
    if(desagregacion_mensual) {
    
        resultado <- resultado %>% 
          mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%  
          select(organismo = iep, mes, 
                 causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto, 
                 otras = res_xotra, resoluciones_a_termio = res_atermino, resoluciones_vencidas = res_luegovenc, 
                 sent_Unicidad_VictImp_NO_PrivLib, sent_Unicidad_VictImp_PrivLib, sent_Pluralidad_VictImp, fecha) %>%
          mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>% 
          left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
          select(-organismo) %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
          mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                              "junio", "julio", "agosto", "septiembre", 
                                              "octubre", "noviembre", "diciembre", "Total"),
                              ordered = TRUE)) %>% 
          arrange(circunscripcion, organismo) %>% 
          group_by(circunscripcion, organismo) %>% 
          do(janitor::adorn_totals(.)) 
        
        resultado
        
      } else {
        
        resultado <- resultado %>% 
          select(organismo = iep,  
                 causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto, 
                 otras = res_xotra, resoluciones_a_termio = res_atermino, resoluciones_vencidas = res_luegovenc, 
                 sent_Unicidad_VictImp_NO_PrivLib, sent_Unicidad_VictImp_PrivLib, sent_Pluralidad_VictImp) %>%
          mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>% 
          left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
          select(-organismo) %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
          arrange(circunscripcion, organismo) %>% 
          group_by(circunscripcion, organismo) %>% 
          do(janitor::adorn_totals(.)) 
        
        resultado
      }
    
      if(desagregacion_mensual) { if(!cfecha) {resultado <- resultado %>% select(-fecha)} }
    
      resultado
  }
  
  else if(any(str_detect(poblacion, "sal"))) {
    
    resultado <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADR3P') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep %in% !!poblacion$organismo) 
    
    if(desagregacion_mensual) {
      resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      resultado <- resultado %>% group_by(iep) 
    }
    
    resultado <- resultado %>%
      process(CADR3P) %>% .$result %>% ungroup()
    
    if(desagregacion_mensual) {
      
      resultado <- resultado %>% 
         mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%   
        select(organismo = iep, mes, 
               causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto, 
               resoluciones_a_termino = res_atermino, resoluciones_vencidas = res_luego1venc, 
               resoluciones_sin_vencimiento_registrado = res_sinvenc, fecha) %>%
        mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>% 
        left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
        select(-organismo) %>% 
        select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
        mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                            "junio", "julio", "agosto", "septiembre", 
                                            "octubre", "noviembre", "diciembre", "Total"),
                            ordered = TRUE)) %>% 
        arrange(circunscripcion, organismo) %>% 
        janitor::adorn_totals("row") 
      
      if(!cfecha) {resultado <- resultado %>% select(-fecha)} 
      
      
      resultado
      
    } else {
      
      resultado <- resultado %>% 
        select(organismo = iep, 
               causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto, 
               resoluciones_a_termino = res_atermino, resoluciones_vencidas = res_luego1venc, 
               resoluciones_sin_vencimiento_registrado = res_sinvenc) %>%
        mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>% 
        left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
        select(-organismo) %>% 
        select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
        arrange(circunscripcion, organismo) %>% 
        janitor::adorn_totals("row") 
      
      resultado
      
    }
    
    # Desde Agosto 2019 no informa CADR3PC la Sala Penal.
    if (start_date < "2019-08-01"){
        
      resultadopc <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADR3PC') %>% 
        apgyeDSL::interval(start_date, end_date) %>% 
        filter(iep %in% !!poblacion$organismo)  
      
      if(desagregacion_mensual) {
        resultadopc <- resultadopc %>%  group_by(iep, data_interval_start, data_interval_end) 
      } else {
        resultadopc <- resultadopc %>% group_by(iep) 
      }
      
      resultadopc <- resultadopc %>%
        process(CADR3PC) %>% .$result %>% ungroup()
      
      if(desagregacion_mensual) {
        
        resultadopc <- resultadopc %>% 
           mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%   
          select(organismo = iep, mes, 
                 causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto, 
                 resoluciones_a_termio = res_atermino, resoluciones_vencidas = res_luego1venc, fecha) %>%
          mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>% 
          left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
          select(-organismo) %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
          mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                              "junio", "julio", "agosto", "septiembre", 
                                              "octubre", "noviembre", "diciembre", "Total"),
                              ordered = TRUE)) %>% 
          arrange(circunscripcion, organismo) %>% 
          janitor::adorn_totals("row") 
        
        if(!cfecha) {resultadopc <- resultadopc %>% select(-fecha)} 
        
        
        resultadopc
      
    } else {
      
      resultadopc <- resultadopc %>% 
        select(organismo = iep, 
               causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto, 
               resoluciones_a_termio = res_atermino, resoluciones_vencidas = res_luego1venc) %>%
        mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>% 
        left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
        select(-organismo) %>% 
        select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
        arrange(circunscripcion, organismo) %>% 
        janitor::adorn_totals("row") 
      
      resultadopc
      
    }
    
    resultadopc <<- resultadopc
    
    
    resultado
    }
    
    
  }
  
  else if(any(str_detect(poblacion, "jdopna"))) {

    resultado <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADRPM') %>%
      apgyeDSL::interval(start_date, end_date) %>%
      filter(iep %in% !!poblacion$organismo)

    if(desagregacion_mensual) {
      resultado <- resultado %>% group_by(iep, data_interval_start, data_interval_end)
    } else {
      resultado <- resultado %>% group_by(iep)
    }

    resultado <- resultado %>%
      process(CADRPM) %>% .$result %>% ungroup()
    
    
    if(desagregacion_mensual) {

      resultado <- resultado %>%
         mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F), fecha =  as.Date(data_interval_end) - 1) %>%  
        select(organismo = iep, mes,
               causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto,
               resoluciones_a_termino = res_atermino, resoluciones_vencidas = res_luego1venc, fecha) %>%
        mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>%
        left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>%
        select(-organismo) %>%
        select(circunscripcion, organismo = organismo_descripcion, everything()) %>%
        mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo",
                                            "junio", "julio", "agosto", "septiembre",
                                            "octubre", "noviembre", "diciembre", "Total"),
                            ordered = TRUE)) %>%
        arrange(circunscripcion, organismo) %>%
        janitor::adorn_totals("row")

      resultado

    } else {

      resultado <- resultado %>%
        select(organismo = iep,
               causas_adespacho, causas_resueltas, sentencias = res_xsentencia, autos = res_xauto,
               resoluciones_a_termino = res_atermino, resoluciones_vencidas = res_luego1venc) %>%
        mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>%
        left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>%
        select(-organismo) %>%
        select(circunscripcion, organismo = organismo_descripcion, everything()) %>%
        arrange(circunscripcion, organismo) %>%
        janitor::adorn_totals("row")

      resultado

    }
    
    
    if(desagregacion_mensual) { if(!cfecha) {resultado <- resultado %>% select(-fecha)} }

    resultado

  }

  else if(any(str_detect(poblacion, "jdopep"))) {
    
    resultado <- DB_PROD() %>% apgyeDSL::apgyeTableData('IGEP') %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep %in% !!poblacion$organismo) %>% 
      mutate(finicio = dmy(finicio), ffin = dmy(ffin), fhecho = dmy(fhecho), fmov = dmy(fmov)) %>% 
      group_by(iep, data_interval_start, data_interval_end) %>% 
      collect() 
    
    
    resultado <- resultado %>% 
      tidyr::separate_rows(fmov_treg_fres, sep = "\\$%") %>% # resolver casos de multiples registros
      filter(str_detect(fmov_treg_fres, "APGE")) %>%        # eliminar registros no apge
      mutate(index = row_number()) %>%
      select(index, everything()) %>% 
      tidyr::separate_rows(fmov_treg_fres, sep = "\\$") %>% 
      tidyr::separate(fmov_treg_fres, c("campo", "dato"), sep = ":") %>% 
      mutate(dato = ifelse(dato == "", NA, dato)) %>% 
      tidyr::spread(campo, dato) %>% 
      filter(str_detect(tr, "Resol")) %>% 
      mutate(f1 = lubridate::dmy(f1), f2 = lubridate::dmy(f2),
             f3 = lubridate::dmy(f3), f4 = lubridate::dmy(f4),
             f5 = lubridate::dmy(f5), f6 = lubridate::dmy(f6),
             fmr = lubridate::dmy(fmr), n1 = as.integer(n1), n2 = as.integer(n2)) %>% 
      rename(fdesp = f1, fvenc = f2, fres = f3, fcump = f4, 
             tres = n1, resul = n2, tsp = t1, tterm = t2) %>% 
      codconver("IGEP", "tres") %>% 
      codconver("IGEP", "resul") %>% 
      codconver("IGEP", "tsp") %>% 
      codconver("IGEP", "tterm") 
   
    resultado <- resultado %>% 
      filter(fres >= data_interval_start, fres < data_interval_end ) %>% 
      #mutate(año_res = as.character(as.integer(lubridate::year(fres)))) %>%
      mutate(mes_res = as.character(lubridate::month(fres, label=T, abbr = F)), fecha =  as.Date(data_interval_end) - 1) %>%
      select(iep, mes_res, nro, everything(), -fmov, -desc, -movt, -id_submission,
             -t3, -t4, -t5, -t6, -t7, -t8, -tr)
    
    jdo_pep_res_prim <- resultado
    
    # errores <- resultado %>% 
    #   mutate(ERROR_sin_fvenc = ifelse(is.na(fvenc), "si", "no"), 
    #          ERROR_fvenc_menora_fdesp = ifelse(fdesp > fvenc, "si", "no"), 
    #          ERROR_sin_tres_valida = ifelse(tres == "sin_dato", "si", "no"), 
    #          ERROR_sin_resul_valido = ifelse(resul == "sin_dato", "si", "no")) %>% 
    #   filter_all(any_vars(str_detect(., pattern = "si"))) 
    
    resultado <- resultado %>% 
      group_by(iep, mes_res,fecha, tres, resul) %>% 
      summarise(cantidad_resueltos = n()) %>% 
      arrange(iep, mes_res, tres) %>% 
      ungroup() %>%  
      left_join(poblacion %>% 
                  filter(str_detect(organismo, "jdopep")) %>% 
                  select(iep=organismo, organismo_descripcion, circunscripcion),
                by="iep") %>% 
      select(circunscripcion, organismo_descripcion, mes_res, tipo_resolucion = tres, resultado = resul, everything(), -iep) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) 
    
    if(!cfecha) {resultado <- resultado %>% select(-fecha)} 
    
    resultado
    
  }
  
  if(exists("jdo_pep_res_prim")){
    jdo_pep_res_prim <<- jdo_pep_res_prim
  }
 
  if(exists("actos_conclusivos_gtia_xtproc")){
    actos_conclusivos_gtia_xtproc <<- actos_conclusivos_gtia_xtproc
  }
  
  if(exists("actos_conclusivos_tjui_xtproc")){
    actos_conclusivos_tjui_xtproc <<- actos_conclusivos_tjui_xtproc
  
  }

  if(exists("resultado_tapel")){
    resultado_tapel <<- resultado_tapel
    
  }
  
  if(exists("resultadopc")){
    resultadopc <<- resultadopc
    
  }
 
  resultado

}


resoluciones_3pc_xtipo <- function(poblacion, start_date="2019-02-01", end_date = "2019-03-01") {
  
  resultadopc <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADR3PC') %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo)  
  
  resultadopc <- resultadopc %>%
    group_by(iep) %>% 
    process(CADR3PC) %>% .$resoluciones_xtipo %>% ungroup()
  
  resultadopc <- resultadopc %>% 
    rename(organismo = iep) %>% 
    left_join(poblacion %>% select(organismo, organismo_descripcion, circunscripcion), by = "organismo") %>% 
    select(-organismo) %>% 
    select(circunscripcion, organismo = organismo_descripcion, everything()) %>% 
    arrange(circunscripcion, organismo) %>% 
    janitor::adorn_totals("row") 
    
  resultadopc
  
  
}
  
  
  
  
  
  
