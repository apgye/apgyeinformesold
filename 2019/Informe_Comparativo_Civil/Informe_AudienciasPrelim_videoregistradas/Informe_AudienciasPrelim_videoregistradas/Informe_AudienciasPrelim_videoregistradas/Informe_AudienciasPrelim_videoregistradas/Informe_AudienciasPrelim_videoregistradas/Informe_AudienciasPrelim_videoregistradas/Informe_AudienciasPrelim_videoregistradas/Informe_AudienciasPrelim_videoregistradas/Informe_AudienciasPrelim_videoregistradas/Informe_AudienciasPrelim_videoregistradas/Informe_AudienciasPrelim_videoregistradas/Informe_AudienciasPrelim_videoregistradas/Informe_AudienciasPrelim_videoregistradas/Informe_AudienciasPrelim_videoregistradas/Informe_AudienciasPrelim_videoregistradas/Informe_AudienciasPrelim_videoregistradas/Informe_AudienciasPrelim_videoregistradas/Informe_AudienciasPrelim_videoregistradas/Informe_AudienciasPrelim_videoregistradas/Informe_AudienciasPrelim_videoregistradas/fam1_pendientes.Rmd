---
title: Causas Pendientes de Resolución
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## Listado de Causas Pendientes de Resolución `r getDataIntervalStr(start_date, end_date)`

En este listado usted puede encontrar todas las causas pendientes de resolución según los informes producidos por el organismo en los últimos seis meses contados desde la fecha de inicio del informe. Este dato surge a partir de la revisión de todas las causas puestas a despacho que no han registrado una resolución o salida de despacho según las previsiones fijadas por el STJER. 

La columna "fecha_vencimiento" representa la fecha registrada por el organismo como fecha de primer vencimiento.

Esta información representa el estado del despacho del organismo al último día hábil del mes anterior a la fecha del informe. Las resoluciones dictadas en el mes en curso recién son informadas al Área de Estadística cumplido el mes. 

```{r}

pendientes(poblacion = jdos_fam,
                      operacion = "CADR1C",
                      start_date = start_date,
                      end_date = end_date) %>% 
  kable(caption = "Causas Pendientes", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  #column_spec(7, "4cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```
