---
title: Duracion de Procesos Inicio - Apertura Pruba
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')

```

## Duracion de Procesos desde Inicio hasta Apertura a Prueba

Para esta exploración se compararon las causas con apertura a prueba 


```{r}
durac_inic_apprueba_cco1()

tabla_durac__intera_inic_aperprueb %>% 
  outputTable(caption = "Duración desde Inicio Causa - Apertura a Prueba") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```


```{r}
comparativo %>% 
  outputTable(caption = "Duración de Procesos") %>% 
  row_spec(0, angle = 90) 
```


