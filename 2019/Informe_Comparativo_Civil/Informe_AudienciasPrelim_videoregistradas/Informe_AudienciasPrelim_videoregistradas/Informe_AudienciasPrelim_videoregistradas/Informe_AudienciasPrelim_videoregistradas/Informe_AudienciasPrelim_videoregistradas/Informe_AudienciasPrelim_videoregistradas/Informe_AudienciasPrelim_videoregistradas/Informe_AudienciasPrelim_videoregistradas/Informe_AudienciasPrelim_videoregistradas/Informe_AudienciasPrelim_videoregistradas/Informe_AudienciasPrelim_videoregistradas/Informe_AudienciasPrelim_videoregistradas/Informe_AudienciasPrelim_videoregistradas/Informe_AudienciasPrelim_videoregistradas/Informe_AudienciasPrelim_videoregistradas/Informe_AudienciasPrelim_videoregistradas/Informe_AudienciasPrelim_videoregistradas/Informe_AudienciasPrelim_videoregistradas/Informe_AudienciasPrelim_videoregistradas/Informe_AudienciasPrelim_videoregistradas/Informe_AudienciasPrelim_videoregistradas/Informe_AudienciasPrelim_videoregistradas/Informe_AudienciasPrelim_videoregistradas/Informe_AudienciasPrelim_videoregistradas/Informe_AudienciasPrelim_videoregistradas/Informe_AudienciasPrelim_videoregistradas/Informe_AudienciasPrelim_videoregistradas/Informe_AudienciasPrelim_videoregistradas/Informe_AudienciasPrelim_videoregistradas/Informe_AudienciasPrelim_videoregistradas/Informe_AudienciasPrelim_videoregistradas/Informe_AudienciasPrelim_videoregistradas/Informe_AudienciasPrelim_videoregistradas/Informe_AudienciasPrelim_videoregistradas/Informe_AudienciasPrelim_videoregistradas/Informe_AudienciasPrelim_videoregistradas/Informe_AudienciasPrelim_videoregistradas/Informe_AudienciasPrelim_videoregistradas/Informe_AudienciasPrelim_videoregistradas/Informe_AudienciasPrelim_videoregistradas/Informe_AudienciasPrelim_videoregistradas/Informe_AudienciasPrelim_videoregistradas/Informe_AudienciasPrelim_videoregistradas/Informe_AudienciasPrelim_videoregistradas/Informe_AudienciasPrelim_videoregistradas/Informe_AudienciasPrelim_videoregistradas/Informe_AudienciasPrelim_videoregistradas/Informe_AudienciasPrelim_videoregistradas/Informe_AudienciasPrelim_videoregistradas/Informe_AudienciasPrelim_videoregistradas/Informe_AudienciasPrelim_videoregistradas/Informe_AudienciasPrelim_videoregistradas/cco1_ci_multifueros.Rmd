---
title: Causas Iniciadas ordenadas por Grupos en Órganos Multifueros
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
multifueros_fam <- any(jdos_cco$organismo %in% c("jdocco0000fed", "jdocco0000ssa", "jdocco0000vpa"))

resultado <- iniciados_multifuero_labfam(poblacion = jdos_cco,
              start_date = start_date,
              end_date = end_date, estadistico = "conteo")
```



## Causas Iniciadas en Órganos Multifuero - `r getDataIntervalStr(start_date, end_date)`

```{r, cco1_ci_mult_lab_t, eval = (ci & t)}
resultado$inic_multifuero %>% 
  iniciados_multifuero_comparativo("laboral", cfecha = desagregacion_mensual) %>%
  kable(caption = str_c("Causas Iniciadas en materia Laboral"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```

\pagebreak

```{r, cco1_ci_mult_fam_t, eval = (ci & t & multifueros_fam), echo=FALSE }
resultado$inic_multifuero %>% 
  iniciados_multifuero_comparativo("familia", cfecha = desagregacion_mensual) %>%
  kable(caption = str_c("Causas Iniciadas en materia Familia"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90) %>% 
  landscape()


```

\pagebreak

`r if (ci & st) 'Materia Laboral'`

```{r, cco1_ci_mult_lab_st, eval = (ci & st)}
resultado$inic_ccolab_pc %>% 
  tsci_g(finicio, org_facet = org_facet)

```

\pagebreak

`r if (ci & st & multifueros_fam) 'Materia Familia'`

```{r, cco1_ci_mult_fam_st, eval = (ci & st & multifueros_fam), echo=FALSE }
resultado$inic_multi_famlab_pc  %>% 
  tsci_g(finicio, org_facet = org_facet)

```


```{r, cco1_mult_lab_pc, eval = (ci & pc)}
resultado$inic_ccolab_pc %>% 
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(4, "6cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```


```{r, cco1_mult_famlab_pc, eval = (ci & pc & multifueros_fam)}
resultado$inic_multi_famlab_pc %>% 
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(4, "6cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```
