# FUNCIONES SEGUNDO NIVEL-----------------------------------------------------

CCO1 <- function(target_mes ="2019-09-01")  {
  
  # Params: poblacion y fechas de intervalos---------------------------------
  poblacion <- jdos_cco
  start_date <- target_mes
  end_date <- ymd(start_date) %m+% months(1)
  trim_2_start <- ymd(start_date) %m-% months(1)
  trim_2_end <- start_date
  trim_1_start <- ymd(start_date) %m-% months(2)
  trim_1_end <- trim_2_start
  desagregacion_mensual = T
  
  # Iniciados -------------------------------------------------------------------
  # 3  
  resultado_cco <- iniciados_cco(poblacion = jdos_cco, start_date = start_date,
                                 end_date = end_date, estadistico = "conteo") %>% 
    .$inic %>% 
    iniciados_cco_comparativo(cfecha = desagregacion_mensual) %>% 
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Civil_Comercial = Total) 
  
  resultado_lab <- iniciados_multifuero_labfam(poblacion = jdos_cco, start_date = start_date,
                                               end_date = end_date, estadistico = "conteo") %>% 
    .$inic_multifuero %>%  
    iniciados_multifuero_comparativo("laboral", cfecha = desagregacion_mensual) %>%
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Laboral = Total) 
  
  resultado_fam <- iniciados_multifuero_labfam(poblacion = jdos_cco, start_date = start_date,
                                               end_date = end_date, estadistico = "conteo") %>% 
    .$inic_multifuero %>%  
    iniciados_multifuero_comparativo("familia", cfecha = desagregacion_mensual) %>%
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Familia = Total) 
  
  resultado_3 <- resultado_cco %>% 
    left_join(resultado_lab) %>% 
    left_join(resultado_fam) %>% rowwise() %>% 
    mutate(iniciadas = sum(Causas_Iniciadas_Civil_Comercial,
                           Causas_Iniciadas_Laboral,
                           Causas_Iniciadas_Familia, na.rm = T)) %>% 
    select(circunscripcion, organismo, iniciadas) %>% 
    bind_rows(tribble(
      ~circunscripcion, ~organismo, ~iniciadas, 
      "Promedio_Provincial", "", round(mean(.[[3]], na.rm = T))))
  
  var_name <- paste0("iniciadas ", format(as.Date(start_date), "%Y-%m"), " (ordena)")
  names(resultado_3) <- c("circunscripcion", "organismo", var_name)
  rm(resultado_cco, resultado_fam, resultado_lab)  
  
  # 2
  resultado_cco <- iniciados_cco(poblacion = jdos_cco, start_date = trim_2_start,
                                 end_date = trim_2_end, estadistico = "conteo") %>% 
    .$inic %>% 
    iniciados_cco_comparativo(cfecha = desagregacion_mensual) %>% 
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Civil_Comercial = Total) 
  
  resultado_lab <- iniciados_multifuero_labfam(poblacion = jdos_cco, start_date = trim_2_start,
                                               end_date = trim_2_end, estadistico = "conteo") %>% 
    .$inic_multifuero %>%  
    iniciados_multifuero_comparativo("laboral", cfecha = desagregacion_mensual) %>%
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Laboral = Total) 
  
  resultado_fam <- iniciados_multifuero_labfam(poblacion = jdos_cco, start_date = trim_2_start,
                                               end_date = trim_2_end, estadistico = "conteo") %>% 
    .$inic_multifuero %>%  
    iniciados_multifuero_comparativo("familia", cfecha = desagregacion_mensual) %>%
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Familia = Total) 
  
  resultado_2 <- resultado_cco %>% 
    left_join(resultado_lab) %>% 
    left_join(resultado_fam) %>% rowwise() %>% 
    mutate(iniciadas = sum(Causas_Iniciadas_Civil_Comercial,
                           Causas_Iniciadas_Laboral,
                           Causas_Iniciadas_Familia, na.rm = T)) %>% 
    select(circunscripcion, organismo, iniciadas) %>% 
    bind_rows(tribble(
      ~circunscripcion, ~organismo, ~iniciadas, 
      "Promedio_Provincial", "", round(mean(.[[3]], na.rm = T))))
  
  var_name <- paste0("iniciadas ", format(as.Date(trim_2_start), "%Y-%m"))
  names(resultado_2) <- c("circunscripcion", "organismo", var_name)
  rm(resultado_cco, resultado_fam, resultado_lab)  
  
  
  # 1 
  resultado_cco <- iniciados_cco(poblacion = jdos_cco, start_date = trim_1_start,
                                 end_date = trim_1_end, estadistico = "conteo") %>% 
    .$inic %>% 
    iniciados_cco_comparativo(cfecha = desagregacion_mensual) %>% 
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Civil_Comercial = Total) 
  
  resultado_lab <- iniciados_multifuero_labfam(poblacion = jdos_cco, start_date = trim_1_start,
                                               end_date = trim_1_end, estadistico = "conteo") %>% 
    .$inic_multifuero %>%  
    iniciados_multifuero_comparativo("laboral", cfecha = desagregacion_mensual) %>%
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Laboral = Total) 
  
  resultado_fam <- iniciados_multifuero_labfam(poblacion = jdos_cco, start_date = trim_1_start,
                                               end_date = trim_1_end, estadistico = "conteo") %>% 
    .$inic_multifuero %>%  
    iniciados_multifuero_comparativo("familia", cfecha = desagregacion_mensual) %>%
    filter(año_mes != "-") %>% select(circunscripcion, organismo, año_mes, Total) %>% 
    rename(Causas_Iniciadas_Familia = Total) 
  
  resultado_1 <- resultado_cco %>% 
    left_join(resultado_lab) %>% 
    left_join(resultado_fam) %>% rowwise() %>% 
    mutate(iniciadas = sum(Causas_Iniciadas_Civil_Comercial,
                           Causas_Iniciadas_Laboral,
                           Causas_Iniciadas_Familia, na.rm = T)) %>% 
    select(circunscripcion, organismo, iniciadas) %>% 
    bind_rows(tribble(
      ~circunscripcion, ~organismo, ~iniciadas, 
      "Promedio_Provincial", "", round(mean(.[[3]], na.rm = T))))
  
  var_name <- paste0("iniciadas ", format(as.Date(trim_1_start), "%Y-%m"))
  names(resultado_1) <- c("circunscripcion", "organismo", var_name)
  rm(resultado_cco, resultado_fam, resultado_lab)  
  
  # Inic_combinado ----------------------------------------------------------
  resultado_inic <- resultado_3 %>% 
    full_join(resultado_2) %>% 
    full_join(resultado_1) %>% 
    arrange(desc(.[[3]]))
  
  rm(resultado_1, resultado_2, resultado_3)
  
  # Resueltos ---------------------------------------------------------------
  # 3
  resultado_3 <- resoluciones_cco(poblacion = jdos_cco, start_date = start_date, end_date = end_date,
    desagregacion_mensual = desagregacion_mensual) %>% 
    filter(circunscripción != "-") %>% 
    select(circunscripcion = circunscripción, organismo, sentencias, autos) %>%
    bind_rows(tribble(
      ~circunscripcion, ~organismo, ~sentencias, ~autos, 
      "Promedio_Provincial", "", round(mean(.[[3]], na.rm = T)),  
      round(mean(as.integer(.[[4]]), na.rm = T)))) %>% 
    arrange(desc(.[[3]])) 
    
  name_sent <- paste0("sentencias ", format(as.Date(start_date), "%Y-%m"), " (ordena)")
  name_aut <- paste0("autos ", format(as.Date(start_date), "%Y-%m"))
  names(resultado_3) <- c("circunscripcion", "organismo", name_sent, name_aut)
  
  # 2
  resultado_2 <- resoluciones_cco(poblacion = jdos_cco, start_date = trim_2_start, end_date = trim_2_end,
                                  desagregacion_mensual = desagregacion_mensual) %>% 
    filter(circunscripción != "-") %>% 
    select(circunscripcion = circunscripción, organismo, sentencias, autos) %>%
    bind_rows(tribble(
      ~circunscripcion, ~organismo, ~sentencias, ~autos, 
      "Promedio_Provincial", "", round(mean(.[[3]], na.rm = T)),  
      round(mean(as.integer(.[[4]]), na.rm = T)))) 
  
  name_sent <- paste0("sentencias ", format(as.Date(trim_2_start), "%Y-%m"))
  name_aut <- paste0("autos ", format(as.Date(trim_2_start), "%Y-%m"))
  names(resultado_2) <- c("circunscripcion", "organismo", name_sent, name_aut)
  
  # 1
  resultado_1 <- resoluciones_cco(poblacion = jdos_cco, start_date = trim_1_start, end_date = trim_1_end,
                                  desagregacion_mensual = desagregacion_mensual) %>% 
    filter(circunscripción != "-") %>% 
    select(circunscripcion = circunscripción, organismo, sentencias, autos) %>%
    bind_rows(tribble(
      ~circunscripcion, ~organismo, ~sentencias, ~autos, 
      "Promedio_Provincial", "", round(mean(.[[3]], na.rm = T)),  
      round(mean(as.integer(.[[4]]), na.rm = T)))) 
  
  name_sent <- paste0("sentencias ", format(as.Date(trim_1_start), "%Y-%m"))
  name_aut <- paste0("autos ", format(as.Date(trim_1_start), "%Y-%m"))
  names(resultado_1) <- c("circunscripcion", "organismo", name_sent, name_aut)
  
  
  # Res_combinado ----------------------------------------------------------
  resultado_res <- resultado_3 %>% 
    full_join(resultado_2) %>% 
    full_join(resultado_1) %>% 
    arrange(desc(.[[3]])) %>% 
    select(circunscripcion, organismo, starts_with("sent"), starts_with("aut"))
    
  rm(resultado_1, resultado_2, resultado_3)
  
    
  # Audiencias ----------------------------------------------------------------  
  
  resultado_3 <- audicco_prim(jdos_cco,  start_date,  end_date) %>% 
    audicco_realizadas_xtipo_cantidad_minutos(desagregacion_mensual = desagregacion_mensual) 
  
  
  
  
  resultado_3 <- audicco_prim(jdos_cco,  start_date,  end_date) %>% 
    audicco_xestado(desagregacion_mensual = desagregacion_mensual) %>% 
    
    
    
    
    
    
  # ----------------  
    
  
  resultado_unif <- resultado_inic %>% 
    left_join(resultado_res)
  
  
  
  
  
}