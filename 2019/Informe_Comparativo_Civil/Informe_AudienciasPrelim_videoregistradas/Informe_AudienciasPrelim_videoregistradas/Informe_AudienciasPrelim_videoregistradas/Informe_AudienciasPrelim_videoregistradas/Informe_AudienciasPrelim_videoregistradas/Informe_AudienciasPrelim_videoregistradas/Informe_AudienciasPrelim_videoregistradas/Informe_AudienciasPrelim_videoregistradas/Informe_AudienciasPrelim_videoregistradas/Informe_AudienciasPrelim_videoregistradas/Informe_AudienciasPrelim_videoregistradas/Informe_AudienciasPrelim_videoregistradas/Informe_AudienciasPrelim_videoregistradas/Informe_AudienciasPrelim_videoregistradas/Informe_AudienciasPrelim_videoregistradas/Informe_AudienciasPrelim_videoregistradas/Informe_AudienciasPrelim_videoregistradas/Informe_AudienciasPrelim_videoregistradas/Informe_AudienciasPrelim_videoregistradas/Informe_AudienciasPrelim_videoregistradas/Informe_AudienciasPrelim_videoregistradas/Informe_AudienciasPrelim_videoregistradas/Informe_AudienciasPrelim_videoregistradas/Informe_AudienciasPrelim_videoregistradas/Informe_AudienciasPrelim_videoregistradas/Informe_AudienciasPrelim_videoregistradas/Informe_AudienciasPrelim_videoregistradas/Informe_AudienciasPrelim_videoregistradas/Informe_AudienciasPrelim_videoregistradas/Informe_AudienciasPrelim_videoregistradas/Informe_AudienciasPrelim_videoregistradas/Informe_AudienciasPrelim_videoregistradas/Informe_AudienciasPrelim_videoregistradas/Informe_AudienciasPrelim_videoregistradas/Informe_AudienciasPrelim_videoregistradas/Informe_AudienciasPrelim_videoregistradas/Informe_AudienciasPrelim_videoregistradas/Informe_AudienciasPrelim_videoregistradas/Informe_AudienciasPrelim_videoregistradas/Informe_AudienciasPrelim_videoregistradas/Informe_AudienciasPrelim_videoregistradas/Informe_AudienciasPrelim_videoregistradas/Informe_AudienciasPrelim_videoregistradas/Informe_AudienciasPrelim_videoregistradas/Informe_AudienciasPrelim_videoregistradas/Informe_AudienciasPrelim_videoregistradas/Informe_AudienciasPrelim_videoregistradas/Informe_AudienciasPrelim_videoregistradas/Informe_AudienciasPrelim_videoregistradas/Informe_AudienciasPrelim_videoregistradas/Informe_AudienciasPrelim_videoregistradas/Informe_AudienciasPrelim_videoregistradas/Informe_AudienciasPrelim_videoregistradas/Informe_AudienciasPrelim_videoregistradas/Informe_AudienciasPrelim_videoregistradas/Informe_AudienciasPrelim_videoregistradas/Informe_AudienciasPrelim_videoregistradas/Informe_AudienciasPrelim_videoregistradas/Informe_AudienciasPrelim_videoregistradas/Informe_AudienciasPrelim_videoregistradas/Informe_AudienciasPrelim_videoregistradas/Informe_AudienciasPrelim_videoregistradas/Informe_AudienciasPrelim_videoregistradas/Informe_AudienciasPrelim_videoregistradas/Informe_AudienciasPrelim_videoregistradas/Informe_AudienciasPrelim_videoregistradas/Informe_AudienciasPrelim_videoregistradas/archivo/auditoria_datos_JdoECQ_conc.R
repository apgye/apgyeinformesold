# Primera Pregunta
# ¿Las causas iniciadas durante 2018 en ambas secretarías son las mismas?

start_date = "2018-02-01"
end_date = "2019-01-01"
poblacion <- jdos_ecq %>% filter(str_detect(organismo, "1con"))
operacion = "CINC1C"


con1 <- inic_xcetal1sem18(poblacion, operacion = operacion) %>% 
  distinct(iep, nro, .keep_all = TRUE)



poblacion <- jdos_ecq %>% filter(str_detect(organismo, "2con"))

con2 <- inic_xcetal1sem18(poblacion, operacion = operacion) %>% 
  distinct(iep, nro, .keep_all = TRUE)


intersecto <- con1 %>% select(nro, finicio) %>% 
  dplyr::intersect(con2 %>% select(nro, finicio))


casos_repetidos_ambos_juzgados <- nrow(intersecto)
casos_repetidos_ambos_juzgados


# por eso, dado que las bases son las mismas (i.e. es indicernible lo que corresponde
# a una secretaría y lo que corresponde a la otra) corresponde imputar como único organismo


# Segunda Pregunta
# ¿Las causas iniciadas a partir de abril 2019 en ambas secretarías son distintas
# según lo solicitado en la comunicación?

start_date = "2019-04-01"
end_date = "2019-06-01"
poblacion <- jdos_ecq %>% filter(str_detect(organismo, "1con"))
operacion = "CINC1C"

con1 <- inic_xcetal1sem18(poblacion, operacion = operacion) %>% 
  distinct(iep, nro, .keep_all = TRUE)


poblacion <- jdos_ecq %>% filter(str_detect(organismo, "2con"))

con2 <- inic_xcetal1sem18(poblacion, operacion = operacion) %>% 
  distinct(iep, nro, .keep_all = TRUE)


intersecto <- con1 %>% select(nro, finicio) %>% 
  dplyr::intersect(con2 %>% select(nro, finicio))

casos_repetidos_ambos_juzgados <- nrow(intersecto)
casos_repetidos_ambos_juzgados

# No hay repetidos

# ---------------------------------------------------------------------------


inic <- inic %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  filter(!(iep == "jdocco0502con" & finicio < make_date(2019,04,01)))












start_date = "2018-02-01"
end_date = "2019-08-01"
poblacion <- jdos_ecq
operacion = "CINC1C"


df <- inic_xcetal1sem18(poblacion, operacion = operacion)

# Caso duplicado: 00055
inic <- df %>% 
  distinct(iep, nro, .keep_all = TRUE) #%>% 
#filter(iep != "jdocco0502con" & finicio < "2019-03-01" ) # Anulo presentaciones duplicadas hasta el mes de febrero
# en virtud de la anómala situación de las secretarías con BD unificadas

if (any(str_detect(poblacion$organismo, "con")))  {
  inic_con <- inic %>% 
    filter(str_detect(iep, "con")) %>% 
    distinct(nro, .keep_all = T)
  
  inic <- inic %>% 
    filter(!str_detect(iep, "con"))
  
  inic <- bind_rows(inic, inic_con)
  
  inic
  
} else {
  inic 
}




start_date = "2018-12-01"
end_date = "2019-01-01"
poblacion <- jdos_ecq %>% filter(str_detect(organismo, "con"))
operacion = "CINC1C"


df2 <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CINC1C") %>% 
  filter(iep %in% !!poblacion$organismo) %>% 
  filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  filter(!is.na(finicio)) %>% 
  filter(finicio >= start_date, finicio < end_date ) %>% 
  select(iep, nro, caratula, tproc, finicio) %>% 
  collect() 

inic2 <- df2 %>% 
  distinct(iep, nro, .keep_all = TRUE) #%>% 


if (any(str_detect(poblacion$organismo, "con")))  {
  inic_con <- inic2 %>% 
    filter(str_detect(iep, "con")) %>% 
    distinct(nro, .keep_all = T)
  
  inic2 <- inic2 %>% 
    filter(!str_detect(iep, "con"))
  
  inic2 <- bind_rows(inic2, inic_con)
  
  inic2
  
} else {
  inic2 
}