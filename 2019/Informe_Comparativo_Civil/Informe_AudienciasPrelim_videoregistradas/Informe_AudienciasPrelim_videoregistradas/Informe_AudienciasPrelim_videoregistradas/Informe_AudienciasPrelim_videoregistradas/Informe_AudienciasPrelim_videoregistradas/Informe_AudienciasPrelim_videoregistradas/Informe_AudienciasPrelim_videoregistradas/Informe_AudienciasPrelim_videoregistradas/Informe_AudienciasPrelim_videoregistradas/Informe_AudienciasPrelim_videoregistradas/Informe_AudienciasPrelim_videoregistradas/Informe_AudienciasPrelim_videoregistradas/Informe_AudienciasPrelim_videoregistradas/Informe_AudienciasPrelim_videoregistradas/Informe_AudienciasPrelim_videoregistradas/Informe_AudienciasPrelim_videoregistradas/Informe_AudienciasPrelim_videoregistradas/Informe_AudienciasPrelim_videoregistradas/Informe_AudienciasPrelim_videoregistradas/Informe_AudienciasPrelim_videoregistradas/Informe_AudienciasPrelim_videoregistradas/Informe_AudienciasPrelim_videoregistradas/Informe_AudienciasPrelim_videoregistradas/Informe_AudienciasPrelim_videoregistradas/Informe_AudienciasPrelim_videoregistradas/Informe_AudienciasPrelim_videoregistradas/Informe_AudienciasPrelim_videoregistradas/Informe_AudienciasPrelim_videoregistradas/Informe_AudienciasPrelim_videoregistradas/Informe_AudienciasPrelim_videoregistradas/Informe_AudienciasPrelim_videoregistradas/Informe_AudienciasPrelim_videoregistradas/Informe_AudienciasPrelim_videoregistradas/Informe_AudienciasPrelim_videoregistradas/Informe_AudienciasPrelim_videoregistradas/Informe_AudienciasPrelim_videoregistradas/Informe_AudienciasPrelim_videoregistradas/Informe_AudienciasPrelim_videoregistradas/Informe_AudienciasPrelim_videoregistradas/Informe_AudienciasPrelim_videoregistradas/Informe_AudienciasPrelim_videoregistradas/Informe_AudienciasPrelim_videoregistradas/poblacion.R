# integracion indicador estadístico y poblacion por departamento

listar_poblacion <- function(año) {
  
  library(readxl)
  
  download.file("https://www.indec.gob.ar/ftp/cuadros/poblacion/proy_1025_depto_entre_rios.xls",
                 destfile="~/apgyeinformes/2019/Modelo_Parametrizado/data/poblacion.xls")
  
  # download.file("https://www.entrerios.gov.ar/dgec/wp-content/uploads/2017/04/Municipios-y-Juntas-de-Gobierno.xlsx",
  #               destfile="~/apgyeinformes/2019/Modelo_Parametrizado/data/poblacion_muni.xls")
  
 
  pob_list <- list()
  
  pob <- read_excel("~/apgyeinformes/2019/Modelo_Parametrizado/data/poblacion.xls", sheet = 1, skip = 4, range = "A5:Q26")
  pob_muni <- readr::read_csv("~/apgyeinformes/2019/Modelo_Parametrizado/data/poblacion_muni.csv")
  
  
  pob_list$xdpto <- pob %>% 
    na.omit() %>% 
    select(circunscripcion = Departamento, año)
  
  pob_list$xmuni <- pob_muni %>% 
    na.omit() %>% 
    filter(Gobierno_Local != "junta_gobierno") %>% 
    select(circunscripcion = circunscripción, poblacion)
  
  pob_list
}


