---
title: Resoluciones 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
existepresP <- existepresentacion(sal_pen, start_date, end_date, "CADR3P")
existepresPC <- existepresentacion(sal_pen, start_date, end_date, "CADR3PC")

```

## Resoluciones en Procesos Penales - `r getDataIntervalStr(start_date, end_date)`

```{r, eval = existepresP}
if (desagregacion_mensual){
  
  resoluciones_pen(poblacion = sal_pen,
              start_date = start_date,
              end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Resoluciones en Sala") %>%
  row_spec(0, angle = 90) 
  
} else {
  
   resoluciones_pen(poblacion = sal_pen,
              start_date = start_date,
              end_date = end_date,
              desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Resoluciones en Sala") %>%
  row_spec(0, angle = 90)
  
}

```


```{r, eval= !existepresP}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "No registra información presentada en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, color = "red") %>%
  column_spec(2, width = "30em")
    
```

## Resoluciones en Procesos Constitucionales - `r getDataIntervalStr(start_date, end_date)`

```{r, eval = (start_date < "2019-08-01" | end_date <= "2019-08-01") & existepresPC}

resultadopc %>% 
  outputTable(caption = "Resoluciones en Sala") %>%
  row_spec(0, angle = 90) 
  
```


```{r, eval = (start_date < "2019-08-01" | end_date <= "2019-08-01") & !existepresPC}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "No registra información presentada en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, color = "red") %>%
  column_spec(2, width = "30em")
    
```

\pagebreak

## Resoluciones por Tipo en Procesos Constitucionales - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se agrupan las resoluciones dictada por tipo según clasificación en uso por el organismo.

```{r, eval = start_date < "2019-08-01"}

resoluciones_3pc_xtipo(poblacion = sal_pen,
              start_date = start_date,
              end_date = end_date) %>%
  outputTable(caption = "Resoluciones por tipo") %>%
  row_spec(0, angle = 90)

```

