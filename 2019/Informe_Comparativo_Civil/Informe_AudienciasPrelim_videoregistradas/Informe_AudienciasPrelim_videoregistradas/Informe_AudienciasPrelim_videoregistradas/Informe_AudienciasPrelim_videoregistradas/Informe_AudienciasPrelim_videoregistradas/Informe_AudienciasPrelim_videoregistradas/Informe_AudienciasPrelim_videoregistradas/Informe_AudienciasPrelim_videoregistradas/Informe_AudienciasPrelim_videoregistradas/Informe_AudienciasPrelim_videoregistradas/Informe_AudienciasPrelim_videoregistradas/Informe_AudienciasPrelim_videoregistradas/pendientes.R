
pendientes <- function(poblacion, operacion = "CADR1C", start_date = "2019-03-01", end_date = "2018-09-02") {

  
  operacion = rlang::enexpr(operacion)
  
  #inicio_sem <- as.Date(start_date) %m-% months(6)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!!operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    group_by(iep) %>% 
    process() %>%  .$listado_pendientes %>%
    ungroup()
 
  
  if(str_detect(poblacion$organismo[[1]], "lab")) {
    resultado <- resultado %>% 
      select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
             fecha_adespacho = fdesp, fecha_vencimiento = fvenc, vencido)
  } else {
    resultado <- resultado %>% 
      select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
             fecha_adespacho = fdesp, fecha_vencimiento = fvenc1, vencido)
    
  }
  
  resultado <- resultado  %>% 
    mutate(caratula = word(caratula, 1,1)) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo") %>% 
    select(-organismo) %>% 
    rename(organismo = organismo_descripcion) %>% 
    mutate(tproc = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tproc))) %>% 
    mutate(vencido = ifelse(vencido, "si", "no"),
           tipo_proc = str_sub(tproc, 1,20), 
           organismo = abbreviate(organismo, minlength = 13)) %>% 
    select(-tproc) %>% 
    select(circunscripcion, organismo, fecha_adespacho, fecha_vencimiento, nro, caratula, tipo_proc, everything()) %>% 
    arrange(circunscripcion, organismo, desc(vencido))
   
    resultado
}
  

