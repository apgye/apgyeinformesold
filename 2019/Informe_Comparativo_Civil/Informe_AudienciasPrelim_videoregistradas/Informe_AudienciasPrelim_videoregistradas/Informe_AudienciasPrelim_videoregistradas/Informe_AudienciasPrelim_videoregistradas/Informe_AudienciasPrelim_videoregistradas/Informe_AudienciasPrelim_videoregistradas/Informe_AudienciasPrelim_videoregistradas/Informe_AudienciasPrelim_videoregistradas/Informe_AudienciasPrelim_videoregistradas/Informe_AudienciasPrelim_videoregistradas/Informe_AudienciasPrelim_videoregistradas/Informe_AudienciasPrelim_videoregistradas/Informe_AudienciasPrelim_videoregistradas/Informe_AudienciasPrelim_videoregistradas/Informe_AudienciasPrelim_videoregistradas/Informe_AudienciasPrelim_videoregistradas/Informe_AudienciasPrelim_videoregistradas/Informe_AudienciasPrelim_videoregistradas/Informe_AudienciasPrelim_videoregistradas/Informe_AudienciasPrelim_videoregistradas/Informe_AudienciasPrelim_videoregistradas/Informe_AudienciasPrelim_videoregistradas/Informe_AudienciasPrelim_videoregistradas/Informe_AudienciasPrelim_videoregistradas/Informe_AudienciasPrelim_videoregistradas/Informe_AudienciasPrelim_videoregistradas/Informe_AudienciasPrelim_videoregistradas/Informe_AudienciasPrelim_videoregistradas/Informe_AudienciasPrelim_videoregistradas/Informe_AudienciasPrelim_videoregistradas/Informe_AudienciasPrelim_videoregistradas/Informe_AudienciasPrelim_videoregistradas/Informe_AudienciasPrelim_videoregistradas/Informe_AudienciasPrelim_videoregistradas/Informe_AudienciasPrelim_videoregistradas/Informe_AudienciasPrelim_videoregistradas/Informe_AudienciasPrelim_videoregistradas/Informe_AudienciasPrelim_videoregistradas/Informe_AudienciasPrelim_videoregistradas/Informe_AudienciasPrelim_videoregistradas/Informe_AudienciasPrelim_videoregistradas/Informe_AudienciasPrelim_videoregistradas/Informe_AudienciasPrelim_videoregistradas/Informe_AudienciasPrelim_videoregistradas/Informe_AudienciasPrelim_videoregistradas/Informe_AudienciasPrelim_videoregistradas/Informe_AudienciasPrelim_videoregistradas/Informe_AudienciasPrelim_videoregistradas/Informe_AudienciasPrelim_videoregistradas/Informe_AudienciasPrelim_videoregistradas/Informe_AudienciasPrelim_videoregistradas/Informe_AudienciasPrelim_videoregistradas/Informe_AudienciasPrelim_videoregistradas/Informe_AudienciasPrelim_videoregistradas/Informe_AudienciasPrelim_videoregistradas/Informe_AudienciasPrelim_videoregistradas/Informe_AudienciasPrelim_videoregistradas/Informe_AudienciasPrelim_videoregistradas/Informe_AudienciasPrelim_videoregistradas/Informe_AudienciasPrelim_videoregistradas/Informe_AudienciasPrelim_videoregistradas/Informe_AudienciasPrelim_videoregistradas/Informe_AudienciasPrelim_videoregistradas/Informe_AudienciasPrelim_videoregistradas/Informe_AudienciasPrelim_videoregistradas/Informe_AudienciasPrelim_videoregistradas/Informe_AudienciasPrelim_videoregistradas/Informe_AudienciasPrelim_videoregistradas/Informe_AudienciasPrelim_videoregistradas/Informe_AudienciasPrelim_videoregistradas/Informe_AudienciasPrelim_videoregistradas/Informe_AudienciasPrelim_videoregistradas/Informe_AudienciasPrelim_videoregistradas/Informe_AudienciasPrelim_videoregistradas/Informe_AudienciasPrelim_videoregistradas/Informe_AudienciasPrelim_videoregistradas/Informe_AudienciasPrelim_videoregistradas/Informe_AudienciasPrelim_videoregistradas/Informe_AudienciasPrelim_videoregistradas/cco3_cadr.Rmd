---
title: Causas Resueltas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


## Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r, cco3_cadr_t, eval= (cr & t)}
resoluciones_cco(
  poblacion = sal_civil,
  start_date = start_date,
  end_date = end_date,
  operacion = "CADR3C",
  desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Resoluciones y Vencimientos") %>%
  column_spec(c(4,8,9,10), background = "#BBBBBB") %>%
  row_spec(0, angle = 90)

```

\pagebreak

`r if (cr & pc) '## Listado Primario de Datos'`

```{r, cco3_cadr_pc_intro, eval= (cr & pc),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("En este listado usted puede encontrar todas las causas resueltas informadas al APGE en el período considerado. Esta consulta se realiza a la base de datos de información primaria del organismo, datos a los que usted puede acceder a través del módulo de presentación del sistema JUSTAT.Para una fácil revisión de las cantidades informadas agregamos columnas donde se identifica individualmente el proceso computado en cada uno de los indicadores asociados a causas resueltas: resueltas a termino y resueltas luego del 1er vencimiento.")
```


```{r, cco3_cadr_pc, eval= (cr & pc)}

resoluciones_cco_prim(poblacion = sal_civil,
                      operacion = "CADR3C",
                      start_date = start_date,
                      end_date = end_date) %>% 
  kable(caption = "Causas Resueltas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```

