
duracion <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE, 
                     desagregacion_xorg = T) {

    resultado <- DB_PROD() %>% 
      apgyeTableData(!! operacion) %>% 
      apgyeDSL::interval(start_date, end_date) %>% 
      filter(iep %in% poblacion$organismo)
    
    
    # Separa Primera y Segunda Instancia
    
    if (str_detect(poblacion$organismo[[1]], "jdo")) {
      
      # Primera Instancia
      
      if(desagregacion_mensual) {
        resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
      } else {
        resultado <- resultado %>% group_by(iep) 
      }
      
      resultado <- resultado %>% 
        process() %>%
        .$result %>%
        ungroup() 
      
     
      if(desagregacion_mensual) {
        
        resultado <- resultado %>% 
          mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
          select(organismo = iep, mes, durac_inic_sent_xproc) %>% 
          tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
          tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion"), sep="&") 
        
        if(desagregacion_xorg){
          resultado <- resultado %>% 
            group_by(organismo, mes, tipo_proceso) %>% 
            summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                      cantidad_casos = n()) %>% 
            arrange(organismo, mes, desc(mediana_duracion)) %>% 
            ungroup() %>% 
            left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                      "circunscripcion")], by = "organismo") %>% 
            select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
          resultado
        } else {
          resultado <- resultado %>% 
            group_by(mes, tipo_proceso) %>% 
            summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                      cantidad_casos = n()) %>% 
            arrange(mes, desc(mediana_duracion)) %>% 
            ungroup() 
          resultado
        }
        resultado
        
      } else {
        
        resultado <- resultado %>% 
          select(organismo = iep, durac_inic_sent_xproc) %>% 
          tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
          tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion"), sep="&") 
        
        if(desagregacion_xorg) {
          resultado <- resultado %>% 
            group_by(organismo, tipo_proceso) %>% 
            summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                      cantidad_casos = n()) %>% 
            arrange(organismo, desc(mediana_duracion)) %>% 
            ungroup() %>% 
            left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                      "circunscripcion")], by = "organismo") %>% 
            select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
          resultado
          
        } else {
          resultado <- resultado %>% 
            group_by(tipo_proceso) %>% 
            summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                      cantidad_casos = n()) %>% 
            arrange(desc(mediana_duracion)) 
          resultado
        }
        resultado
      }
      
    } else {
      
      # Segunda Instancia
      
      if(desagregacion_mensual) {
        resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
      } else {
        resultado <- resultado %>% group_by(iep) 
      }
      
      resultado <- resultado %>% 
        filter(as %in% c("S", "s")) %>% 
        filter(!is.na(fres)) %>% filter(tres != "0") %>%
        mutate(fres = dmy(fres)) %>% 
        mutate(finicio = dmy(finicio)) %>%
        collect() %>% 
        ungroup()
        
      if(desagregacion_mensual) {
        resultado <- resultado %>% 
          mutate(mes = lubridate::month(fres, label=T, abbr = F)) %>% 
          filter(!str_detect(tproc, "VIOLENCIA|AMPARO"), !is.na(tproc)) %>% 
          filter(fres <= Sys.Date()) %>%
          select(organismo = iep, mes, nro, caratula, tproc, 
                 fecha_inicio_camara = finicio,
                 fecha_resolucion = fres) %>%
          mutate(duracion = fecha_resolucion - fecha_inicio_camara)  
          # mutate(tproc = ifelse(tproc == "COBRO DE PESOS", 
          #                       "ORDINARIO COBRO DE PESOS", tproc), 
          #        tproc = ifelse(tproc == "ORDINARIO (CIVIL)", 
          #                       "ORDINARIO", tproc)) %>% 
        if(desagregacion_xorg){
          resultado <- resultado %>% 
            group_by(organismo, mes, tproc) %>% 
              summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                        cantidad_casos = n()) %>% 
              ungroup() %>% 
              arrange(desc(mediana_duracion)) %>% 
              rename(tipo_proceso = tproc) %>% 
              left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                        "circunscripcion")], by = "organismo") %>% 
              select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
          
          resultado
        } else {
          resultado <- resultado %>%
            group_by(mes, tproc) %>% 
              summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                        cantidad_casos = n()) %>% 
              ungroup() %>% 
              arrange(desc(mediana_duracion)) %>% 
              rename(tipo_proceso = tproc) 
          resultado
        }
        
      } else {
        
        resultado <- resultado %>% 
          filter(!str_detect(tproc, "VIOLENCIA|AMPARO"), !is.na(tproc)) %>% 
          filter(fres <= Sys.Date()) %>%
          select(organismo = iep, nro, caratula, tproc, 
                 fecha_inicio_camara = finicio,
                 fecha_resolucion = fres) %>%
          mutate(duracion = fecha_resolucion - fecha_inicio_camara) 
        
        if(desagregacion_xorg){
          resultado <- resultado %>%
            group_by(organismo, tproc) %>% 
              summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                        cantidad_casos = n()) %>% 
              ungroup() %>% 
              arrange(desc(mediana_duracion)) %>% 
              rename(tipo_proceso = tproc) %>% 
              left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                        "circunscripcion")], by = "organismo") %>% 
              select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
          
          resultado
          
        } else {
          resultado <- resultado %>%
            group_by(tproc) %>% 
              summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                        cantidad_casos = n()) %>% 
              ungroup() %>% 
              arrange(desc(mediana_duracion)) %>% 
              rename(tipo_proceso = tproc) 
          resultado
        }
          
        resultado
      }
      resultado
    }
    resultado
}




durac_subselec <- function(df, materia) {
  
  df <- df %>% 
    #filter(cantidad_casos > 3) %>% 
    rename(tproc = tipo_proceso)
  
  if (materia ==  "civil") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(!str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR "),
        !str_detect(tproc, "MONITORIO"),
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INTERDICT"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "^HOMOLOGACION"), 
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL"),
        # Otros Fueros Laboral y Familia
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"), 
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO")) 
    df
  } else if (materia == "familia") {
    df <- df %>%
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "VIOLENCIA FAMILIAR"), #
        !str_detect(tproc, "VIOLENCIA DE GENERO"), #
        !str_detect(tproc, "HOMOLOGACION"), 
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU"),
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA"),
        !str_detect(tproc, "^RESTRICCIONES"),
        !str_detect(tproc, "^INTERNACION"), #
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "^REGIMEN"), #
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "^SU |^EXHORTO|^OFICIO|DESARCHIVO|^INFORME|^DENUNCIA|^TESTIMONIO"),
        !str_detect(tproc, "CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL"),
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"))
    df
  } else if (materia == "laboral") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "^HOMOLOGACION"), #
        !str_detect(tproc, "^EJEC|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR "),
        !str_detect(tproc, "^MEDIDAS PREP"),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|MONITORI|PERSONAL"),
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO|^DESALOJO|^APELACION JUZGADO|^DIVISION DE|^SEGUNDO TEST"))
    df
  } else if(materia == "pazproc") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "), #
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR "), #
        !str_detect(tproc, "MONITORIO"), #
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INTERDICT"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        !str_detect(tproc, "^PREPARACION "),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "^HOMOLOGACION"), 
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA|^SUPERVIVENCIA|^AUTORIZACION"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|PAZ"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL|^LICENCIAS|^ARANCELES"),
        # Otros Fueros Laboral y Familia
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"), 
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^LESIONES|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO"))
    df
  }
  df
}









muestras11_12 <- muestra11 %>% 
  full_join(muestra12, by = "tipo_proceso") %>% 
  rename(mediana_duracion_1aInst = 2, cantidad_casos_1aInst = 3,
         mediana_duracion_2aInst = 4, cantidad_casos_2aInst = 5)

muestras11_12_sinNa <- muestras11_12 %>% 
  na.omit(.) %>% rowwise() %>% 
  mutate(duracion_total = sum(mediana_duracion_1aInst,
                              mediana_duracion_2aInst), 
         cantidad_total_casos = sum(cantidad_casos_1aInst,
                              cantidad_casos_2aInst), 
         año = 2017) %>% 
  ungroup()


# 2018

# Primera Instancia

# Muestra 21



muestras21_22 <- muestra21 %>% 
  full_join(muestra22, by = "tipo_proceso") %>% 
  rename(mediana_duracion_1aInst = 2, cantidad_casos_1aInst = 3,
         mediana_duracion_2aInst = 4, cantidad_casos_2aInst = 5)

muestras21_22_sinNa <- muestras21_22 %>% 
  na.omit(.) %>% rowwise() %>% 
  mutate(duracion_total = sum(mediana_duracion_1aInst,
                              mediana_duracion_2aInst), 
         cantidad_total_casos = sum(cantidad_casos_1aInst,
                                    cantidad_casos_2aInst),
         año = 2018) %>% 
  ungroup()


# Comparacion entre muestras
comp_muestras <- muestras11_12_sinNa %>% 
  select(tipo_proceso, duracion_total, cantidad_total_casos) %>% 
  left_join(muestras21_22_sinNa[, c("tipo_proceso", "duracion_total", 
                                    "cantidad_total_casos")], by = "tipo_proceso") %>% 
  na.omit() %>% 
  rename(duraciones_2017 = 2, cantidad_casos2017 = 3,
         duraciones_2018 = 4, cantidad_casos2018 = 5) %>% 
  rowwise() %>% 
  mutate(comparacion_interanual = ifelse(duraciones_2018 < duraciones_2017,
                                         "disminución", "incremento"), 
         variacion_interanual = stringr::str_c(round((duraciones_2018 - duraciones_2017) / duraciones_2017 * 100), ' %')) %>% 
  arrange(variacion_interanual)


# Data Frame de Duracones 2017 y 2018 casos con valores en 1a y 2a instancia
consolidado_muestras1y2_sinNa <- bind_rows(muestras11_12_sinNa, muestras21_22_sinNa) %>% 
  filter(!str_detect(tipo_proceso, "DECLARACION DE INCAPACIDAD")) %>% 
  select(año, everything())









# Analisis Particular de Duraciones 

# Primera Instancia
cadr1c_sent <- apgyeRStudio::primariasOrganismo("jdocco*", "2018-02-01", "2018-11-01")$CADR1C$tabla() %>%
  # Filtros de base de datos
  filter(iep %in% poblacion1a$organismo) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% 
  # Procesamiento con Proc
  process() %>% .$result %>% 
  ungroup() %>% 
  mutate(mes = months(data_interval_start)) %>%
  mutate(mes = factor(mes, levels = c("febrero", "marzo", "abril", "mayo",
                                      "junio", "julio", "agosto", "septiembre",
                                      "octubre", "noviembre", "diciembre"), ordered = TRUE)) %>%
  select(iep, mes, durac_inic_sent_xproc) %>% 
  tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
  tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion"), sep="&") %>% 
  group_by(iep, tipo_proceso) %>% 
  summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
            cantidad_casos = n()) %>% 
  ungroup() %>% 
  # Características de los casos seleccionados de la muestra
  filter(!str_detect(tipo_proceso, 
                     "EJECUCION DE SENTENCIA|DECLARACION DE INCAPACIDAD|LABORAL|VIOLENCIA"), !is.na(tipo_proceso), 
         cantidad_casos > 5) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], by = c("iep" = "organismo")) %>% 
  mutate(organismo_descripcion = str_c(organismo_descripcion, circunscripcion, sep = " "), 
         tipo_proceso = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tipo_proceso))) %>% 
  select(tipo_proceso, organismo_descripcion, mediana_duracion, cantidad_casos) %>% 
  arrange(tipo_proceso, desc(mediana_duracion))

  

# Resumen por tipo de proceso

resumen_durac1c_xtproc <- apgyeRStudio::primariasOrganismo("jdocco*", "2018-02-01", "2018-11-01")$CADR1C$tabla() %>%
  # Filtros de base de datos
  filter(iep %in% poblacion1a$organismo) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% 
  # Procesamiento con Proc
  process() %>% .$result %>% 
  ungroup() %>% 
  mutate(mes = months(data_interval_start)) %>%
  mutate(mes = factor(mes, levels = c("febrero", "marzo", "abril", "mayo",
                                      "junio", "julio", "agosto", "septiembre",
                                      "octubre", "noviembre", "diciembre"), ordered = TRUE)) %>%
  select(iep, mes, durac_inic_sent_xproc) %>% 
  tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
  tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion"), sep="&") %>% 
  group_by(tipo_proceso) %>% 
  summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
            cantidad_casos = n()) %>% 
  ungroup() %>% 
  # Características de los casos seleccionados de la muestra
  filter(!str_detect(tipo_proceso, 
                     "EJECUCION DE SENTENCIA|DECLARACION DE INCAPACIDAD|LABORAL|VIOLENCIA"), !is.na(tipo_proceso), 
         cantidad_casos > 5) %>% 
  mutate(tipo_proceso = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tipo_proceso))) %>% 
  arrange(tipo_proceso, desc(mediana_duracion))


# Segunda Instancia

# Duracion de Procesos Segunda Instancia
durac_proc <- apgyeRStudio::primariasOrganismo("camcco*","2017-01-01", "2018-01-01")$CADUR2C$tabla() %>%
  filter(iep %in% poblacion$organismo) %>% 
  mutate(fress = dmy(fress), fing = dmy(fing)) %>% filter(!is.na(fress)) %>% 
  collect() %>% 
  mutate(año = lubridate::year(fress)) %>% 
  mutate(mes = lubridate::month(fress)) %>% 
  filter(fress <= Sys.Date()) %>% 
  select(iep, año, mes, nro, caratula, tproc, fecha_inicio_camara = fing, 
         fecha_resolucion = fress, devoluciones = devol) %>% 
  mutate(duracion = fecha_resolucion - fecha_inicio_camara) %>% 
  ungroup() %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")],
            by = c("iep" = "organismo")) %>%
  select(circunscripcion, organismo_descripcion, iep, everything()) %>% 
  mutate(caratula = str_sub(caratula, 1, 15), 
         tproc = str_sub(tproc, 1, 20)) %>% 
  filter(duracion > 0) %>% 
  arrange(circunscripcion, organismo_descripcion, iep, mes, desc(duracion))


# Casos Informados
casos_informados_conRSRR <- durac_proc %>% 
  group_by(circunscripcion, organismo_descripcion) %>% 
  summarise(cantidad = n())

# Muestra insuficiente para Concordia II y Uruguay

# Ampliación de muestra extrayendo casos e informción de Causas Resueltas
cadr2c_sent <- apgyeRStudio::primariasOrganismo("camcco*", "2018-02-01", "2018-11-01")$CADR2C$tabla() %>%
  # Filtros de base de datos
  filter(iep %in% c("camcco0000uru", "camcco0200con")) %>%
  mutate(fres = dmy(fres)) %>% mutate(fdesp1 = dmy(fdesp1)) %>%
  mutate(finicio = dmy(finicio)) %>% mutate(fvenc1 = dmy(fvenc1)) %>% mutate(fvenc2 = dmy(fvenc2)) %>%
  filter(!is.na(fres)) %>% filter(tres != "0") %>%
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect() %>% ungroup() %>%
  mutate(año = lubridate::year(fres)) %>% 
  mutate(mes = lubridate::month(fres)) %>% 
  filter(toupper(as) == "S") %>% 
  filter(!str_detect(tproc, "AMPARO")) %>% 
  filter(fres <= Sys.Date()) %>%
  filter(finicio > lubridate::make_date(2017,01,01)) %>% 
  # filter(mes >= lubridate::month(lubridate::ymd(start_date)),
  #        mes < lubridate::month(lubridate::ymd("2018-01-01"))) %>%
  # Data wrangling
  select(iep, año, mes, nro, caratula, tproc, 
         fecha_inicio_camara = finicio,
         fecha_resolucion = fres) %>%
  ungroup() %>%
  mutate(devoluciones = NA, 
         duracion = fecha_resolucion - fecha_inicio_camara) %>% 
  mutate(caratula = str_sub(caratula, 1, 20)) %>%
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")],
            by = c("iep" = "organismo")) %>%
  select(circunscripcion, organismo_descripcion, iep, everything()) %>% 
  mutate(caratula = str_sub(caratula, 1, 15), 
         tproc = str_sub(tproc, 1, 20)) %>% 
  filter(duracion > 0) %>% 
  arrange(circunscripcion, organismo_descripcion, iep, mes, desc(duracion))


casos_complementarios <- cadr2c_sent %>% 
  group_by(circunscripcion, organismo_descripcion) %>% 
  summarise(cantidad = n())


# Consolidación de Información
durac_proc <- bind_rows(durac_proc, cadr2c_sent)


# Procesamiento

resumen_durac_sin_devol <- durac_proc %>% 
  filter(devoluciones %in% c("0", NA)) %>% 
  mutate(tipo_proceso = tproc) %>% 
  group_by(circunscripcion, organismo_descripcion, tproc) %>% 
  summarise(casos_considerados = n(), 
            promedio_duracion = round(as.integer(mean(duracion, na.rm = T), digits = 1)), 
            #mediana = median(duracion, na.rm = T), 
            minima_duracion = as.integer(min(duracion, na.rm = T)), 
            minima_duracion_expte = nro[which.min(duracion)], 
            maxima_duracion = as.integer(max(duracion, na.rm = T)), 
            maxima_duracion_expte = nro[which.max(duracion)],
            'nros expedientes 1a.Instancia' = str_c(nro, collapse = "; "))  

# Procesos registrados con duraciones mayores a 180 días

resumen_durac_intervalos <- durac_proc %>% 
  mutate(duracion = as.integer(duracion)) %>% 
  filter(devoluciones %in% c("0", NA), duracion > 180) %>%
  mutate(tipo_proceso = tproc) %>% 
  mutate(duracion_intervalos = case_when(
    duracion > 360 & duracion <= 400 ~ "entre 361 y 400 días", 
    duracion > 400 ~ "mas de 400 días", 
    TRUE ~ "entre 180 y 360 días")) %>% 
  group_by(circunscripcion, organismo_descripcion, duracion_intervalos) %>% 
  summarise('cantidad de procesos' = n(), 
            'nros expedientes 1a.Instancia' = str_c(nro, collapse = "; ")) %>% 
  arrange(duracion_intervalos)


# no funciona el agrupamiento
# resumen_tproc <- resumen_durac_sin_devol %>% 
#   filter(casos_considerados >= 10) %>% 
#   ungroup() %>% 
#   mutate(organismo_descripcion = str_c(organismo_descripcion, circunscripcion)) %>% 
#   spread(organismo_descripcion, promedio_duracion) %>% 
#   select(tproc, 9:12) %>% ungroup() %>% 
#   arrange(tproc) 

resumen_devoluciones <- durac_proc %>% 
  filter(devoluciones != "0") %>% 
  mutate(devoluciones = as.integer(devoluciones)) %>% 
  group_by(circunscripcion, organismo_descripcion) %>% 
  summarise(cantidad_devoluciones = sum(devoluciones, na.rm = T), 
            'nros expedientes 1a.Instancia' = str_c(nro, collapse = "; ")) %>%  
  filter(!circunscripcion %in% c("Concordia", "Uruguay")) 











# Casos Concordia y Uruguay: muestras pequeñas
# Caso Uruguay calcular duración finicio - fres de CADR2C el mejor ejemplo!
# Casos Concordia completar cadur2c con resueltos según cadr2c


# # Causas y Movimientos
# camov2c <- apgyeRStudio::primariasOrganismo("camcco*", "2018-02-01", "2018-11-01")$CAMOV$tabla() %>%
#   # Filtros de base de datos
#   filter(iep %in% poblacion$organismo) %>% collect() %>% 
#   group_by(iep, nro) %>% 
#   slice(n()) %>% ungroup %>% 
#   separate_rows(movt, sep = "%") %>%
#   separate(movt, into = c("fecha", "desc"), sep = "\\$") %>% 
#   group_by(iep, nro) %>% 
#   mutate(fecha = as.Date(fecha,  format="%d/%m/%Y")) %>% 
#   mutate(finicio = as.Date(finicio,  format="%d/%m/%Y")) %>%
#   summarise(cantidad_movimientos = n(),
#             cuenta_sentencias = sum(str_count(desc, "^SENTENCIA"), na.rm = T),
#             fecha_primer_mov_causa = fecha[which.min(fecha)],
#             fecha_inicio_camara = unique(finicio)) %>% 
#   ungroup() 
        

# Casos de Control de consolidación datos de resoluciones en Camara y movimientos en Primera Instancia
# 8942:2007-10-17
# 9323:2014-04-09


# Duración desde el primer ingreso hasta la sentencia
# resumen_durac_1eringreso_sentencia <- muestra1 %>% 
#   group_by(circunscripcion, organismo_descripcion, iep, tproc) %>% 
#   summarise(casos_considerados = n(), 
#             mediana = median(duracion, na.rm = T), 
#             'nros expedientes considerados' = str_c(nro, collapse = "; "))
# 
# plot1 <- ggplot(resumen_durac_1eringreso_sentencia, 
#               aes(x = iep, y = as.integer(mediana))) +
#   geom_boxplot()+
#   geom_jitter()
# plot1







  