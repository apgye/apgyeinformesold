personal <- function(poblacion, mes_menos) {
  
  library(lubridate)
  
  end_date <- as.Date(end_date) %m-% months(1)
  
  mes_menos_start <- as.Date(end_date) %m-% months(mes_menos)
  mes_menos_end <- as.Date(end_date) %m-% months(mes_menos-1)
  
  if (mes_menos_end < end_date){
    end_date <- mes_menos_end
  }
  
  DB_PROD() %>%
    apgyeData(GHSTRD) %>%
    apgyeDSL::interval(mes_menos_start, end_date) %>%
    activate(tbs_prim_GHSTRD_planta) %>%
    left_join(tbl(DB_PROD(), "tabla_personal")) %>%
    filter(organismoAPGyE %in% poblacion$organismo) %>%
    deactivate() %>%
    activate(tbs_prim_GHSTRD_planta_ocupada) %>%
    left_join(tbl(DB_PROD(), "tabla_personal")) %>%
    filter(organismoAPGyE %in% poblacion$organismo) %>%
    deactivate() %>%
    group_by(organismoAPGyE) %>% 
    process() %>% .$planta_ocupada_revista %>% 
    left_join(poblacion %>% select(-categoria), by=c("organismoAPGyE"="organismo")) %>%
    ungroup() %>% select(circunscripcion, organismo=organismo_descripcion, categoria, revista, cantidad) %>%
    select(-revista) %>% filter(!str_detect(categoria, "JUEZ|VOCAL|SECRETARIO|AUXILIAR|AYUDANTE")) %>%
    group_by(circunscripcion, organismo, categoria) %>%
    summarise(cantidad = n()) %>%
    ungroup() %>%
    mutate(mes = as.character(month(end_date))) %>% 
    tidyr::spread(key = categoria, value = cantidad) %>% 
    janitor::adorn_totals("col")
}

personal_trimestre <- function(poblacion) {
  
  personal_ultmes <- personal(poblacion, mes_menos =  1) 
  personal_ultmenos1 <- personal(poblacion, mes_menos =  2) 
  personal_ultmenos2 <- personal(poblacion, mes_menos =  3) 
  
  personal_trimestre <- personal_ultmes %>% 
    full_join(personal_ultmenos1) %>% 
    full_join(personal_ultmenos2) %>% 
    mutate(mes = as.integer(mes)) %>% 
    arrange(circunscripcion, organismo, mes) 
  
  personal_trimestre
}

disponibilidad <- function(poblacion, mes_menos) {
  
  library(lubridate)
  
  end_date <- as.Date(end_date) %m-% months(1)
  
  mes_menos_start <- as.Date(end_date) %m-% months(mes_menos)
  mes_menos_end <- as.Date(end_date) %m-% months(mes_menos-1)
  
  if (mes_menos_end < end_date){
    end_date <- mes_menos_end
  }
  
  resultado <- DB_PROD() %>%
    apgyeData(GHSTRD) %>%
    apgyeDSL::interval(mes_menos_start, end_date) %>%
    activate(tbs_prim_GHSTRD_planta) %>%
    left_join(tbl(DB_PROD(), "tabla_personal")) %>%
    filter(organismoAPGyE %in% poblacion$organismo) %>%
    deactivate() %>%
    activate(tbs_prim_GHSTRD_planta_ocupada) %>%
    left_join(tbl(DB_PROD(), "tabla_personal")) %>%
    filter(organismoAPGyE %in% poblacion$organismo) %>%
    deactivate() %>%
    group_by(organismoAPGyE) %>% 
    process() %>% .$licencias_por_categoria %>% 
    left_join(poblacion %>% select(-categoria), by=c("organismoAPGyE"="organismo")) %>%
    ungroup() %>% select(circunscripcion, organismo=organismo_descripcion, categoria, cantidad_personas = cantidad,
                         diasDisponibles, dias_licencias) %>%
    filter(!str_detect(categoria, "AUXILIAR|AYUDANTE")) %>%
    mutate(mes = lubridate::month(end_date)) %>%
    rowwise() %>% 
    mutate(disponibilidad = diasDisponibles - dias_licencias) %>% 
    mutate(disponibilidad_ptje = stringr::str_c(round(disponibilidad/diasDisponibles*100), ' %')) %>%  
    select(-disponibilidad) %>% 
    group_by(circunscripcion, organismo, mes) %>%
    do(janitor::adorn_totals(.))
  
  resultado
    
}

disponibilidad_trimestre <- function(poblacion) {
  
  disponibilidad_ultmes <- disponibilidad(poblacion, mes_menos =  1) 
  disponibilidad_ultmenos1 <- disponibilidad(poblacion, mes_menos =  2) 
  disponibilidad_ultmenos2 <- disponibilidad(poblacion, mes_menos =  3) 
  
  disponibilidad_trimestre <- disponibilidad_ultmes %>% 
    bind_rows(disponibilidad_ultmenos1) %>% 
    bind_rows(disponibilidad_ultmenos2) %>% 
    filter(circunscripcion != "Total") %>% 
    select(circunscripcion, organismo, mes, everything()) %>% 
    group_by(circunscripcion, organismo, mes) %>% 
    arrange(circunscripcion, organismo, as.integer(mes)) %>% 
    do(janitor::adorn_totals(.)) %>% ungroup() %>% 
    mutate(mes = ifelse(circunscripcion == "Total", "-", mes))
  
  disponibilidad_trimestre
  
}


# personalPorCategoria <- function(poblacion, personal) {
#   personal %>%
#     activate(planta_ocupada_revista) %>% 
#     rename(iep = organismoAPGyE) %>% 
#     left_join(poblacion %>% select(-categoria), by=c("iep"="organismo")) %>% 
#     select(circunscripcion, organismo=organismo_descripcion, everything()) %>% 
#     filter(!is.na(categoria)) %>% 
#     group_by(circunscripcion, organismo, categoria) %>% 
#       summarise(cantidad = sum(cantidad, na.rm = TRUE))
# }
