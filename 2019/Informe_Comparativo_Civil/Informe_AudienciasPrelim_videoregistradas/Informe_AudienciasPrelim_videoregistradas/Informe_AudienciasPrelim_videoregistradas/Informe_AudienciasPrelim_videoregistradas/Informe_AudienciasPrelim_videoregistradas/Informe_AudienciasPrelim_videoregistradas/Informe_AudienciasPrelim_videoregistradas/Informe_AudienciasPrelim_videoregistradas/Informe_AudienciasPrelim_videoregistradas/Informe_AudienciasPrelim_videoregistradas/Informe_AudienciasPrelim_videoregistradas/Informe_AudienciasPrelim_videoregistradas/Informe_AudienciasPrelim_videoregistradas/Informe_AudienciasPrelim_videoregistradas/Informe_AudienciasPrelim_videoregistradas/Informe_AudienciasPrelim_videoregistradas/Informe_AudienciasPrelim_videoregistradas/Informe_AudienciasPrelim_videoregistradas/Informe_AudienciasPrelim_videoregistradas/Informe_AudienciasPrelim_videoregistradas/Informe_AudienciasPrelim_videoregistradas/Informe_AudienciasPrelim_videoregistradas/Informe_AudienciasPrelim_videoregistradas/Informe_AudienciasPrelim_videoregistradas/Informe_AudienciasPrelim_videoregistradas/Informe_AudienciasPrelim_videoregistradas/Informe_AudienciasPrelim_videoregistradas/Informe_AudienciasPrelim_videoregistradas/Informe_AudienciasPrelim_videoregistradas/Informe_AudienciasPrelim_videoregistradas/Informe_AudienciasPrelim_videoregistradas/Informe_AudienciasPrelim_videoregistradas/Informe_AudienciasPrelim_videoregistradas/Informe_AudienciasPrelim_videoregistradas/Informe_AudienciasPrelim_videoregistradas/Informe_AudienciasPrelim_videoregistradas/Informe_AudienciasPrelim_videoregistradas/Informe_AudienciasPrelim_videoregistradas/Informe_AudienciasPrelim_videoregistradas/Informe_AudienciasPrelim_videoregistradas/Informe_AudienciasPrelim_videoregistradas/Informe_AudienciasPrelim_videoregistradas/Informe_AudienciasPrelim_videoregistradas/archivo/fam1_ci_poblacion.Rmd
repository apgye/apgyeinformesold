---
title: Causas Iniciadas ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


## Causas Iniciadas y Poblacion - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestran las causas iniciados por organismo y la población por Municipio según último censo del INDEC 2010.     

Agregamos una columna que vincula las causas iniciadas cada mil habitantes con el fin de represenar la litigiosidad de cada unidad territorial. Sin perjuicio de la desactualización que tiene los datos poblacionales, esta litigiosidad por habitantes permiten advertir la distribución heterogénea que tiene el conflicto en esta materia.     

Las fuentes consultadas para los datos censales son: INDEC y la Dirección Provincial de Estadísticas y Censos, MEHF, Gobierno de Entre Ríos (<https://www.indec.gob.ar/indec/web/Nivel4-Tema-2-24-119> y <https://www.entrerios.gov.ar/dgec/censo2010/>).


```{r }

inic_fam <- iniciados_fam(poblacion = jdos_fam,
  start_date = start_date,
  end_date = end_date) %>% 
  filter(tipo_proceso != "subtotal") %>% 
  group_by(circunscripcion, organismo) %>% 
  summarise(cantidad_iniciados = sum(cantidad, na.rm = T)) %>% 
  rename(organismo_descripcion = organismo) %>% 
  left_join(jdos_fam %>% select(organismo_descripcion, circunscripcion, localidad)) %>% 
  ungroup() %>% 
  select(circunscripcion = localidad, organismo_descripcion, cantidad_iniciados)

temp <-inic_fam %>% 
  group_by(circunscripcion) %>% 
  summarise(cantidad_jdos_xcirc = n()) %>% 
  ungroup()
  
inic_fam <- inic_fam %>% 
  left_join(temp)


pob_list <- listar_poblacion("2019") 
 
inic_xhab <- inic_fam %>% 
  mutate(circunscripcion = str_replace(circunscripcion, "Feliciano", "San José De Feliciano"),
         circunscripcion = str_replace(circunscripcion, "Colón", "Colon"),
         circunscripcion = str_replace(circunscripcion, "Rosario del Tala", "Rosario Del Tala"), 
         circunscripcion = str_replace(circunscripcion, "Concepción del Uruguay", "Concepción Del Uruguay")) %>% 
  left_join(pob_list$xmuni) %>% 
  rowwise() %>% 
  mutate(prop_poblacion = round(poblacion/cantidad_jdos_xcirc)) %>% 
  mutate(iniciadas_c_milhab = round(cantidad_iniciados/(prop_poblacion/1000), digits = 2)) %>% 
  arrange(desc(iniciadas_c_milhab)) 

inic_xhab %>% 
  rename('causas iniciadas cada mil habitantes' = iniciadas_c_milhab, municipalidad = circunscripcion) %>% 
  kable(caption = "Causas Iniciadas y Poblacion", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  #column_spec(5, "3cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```



