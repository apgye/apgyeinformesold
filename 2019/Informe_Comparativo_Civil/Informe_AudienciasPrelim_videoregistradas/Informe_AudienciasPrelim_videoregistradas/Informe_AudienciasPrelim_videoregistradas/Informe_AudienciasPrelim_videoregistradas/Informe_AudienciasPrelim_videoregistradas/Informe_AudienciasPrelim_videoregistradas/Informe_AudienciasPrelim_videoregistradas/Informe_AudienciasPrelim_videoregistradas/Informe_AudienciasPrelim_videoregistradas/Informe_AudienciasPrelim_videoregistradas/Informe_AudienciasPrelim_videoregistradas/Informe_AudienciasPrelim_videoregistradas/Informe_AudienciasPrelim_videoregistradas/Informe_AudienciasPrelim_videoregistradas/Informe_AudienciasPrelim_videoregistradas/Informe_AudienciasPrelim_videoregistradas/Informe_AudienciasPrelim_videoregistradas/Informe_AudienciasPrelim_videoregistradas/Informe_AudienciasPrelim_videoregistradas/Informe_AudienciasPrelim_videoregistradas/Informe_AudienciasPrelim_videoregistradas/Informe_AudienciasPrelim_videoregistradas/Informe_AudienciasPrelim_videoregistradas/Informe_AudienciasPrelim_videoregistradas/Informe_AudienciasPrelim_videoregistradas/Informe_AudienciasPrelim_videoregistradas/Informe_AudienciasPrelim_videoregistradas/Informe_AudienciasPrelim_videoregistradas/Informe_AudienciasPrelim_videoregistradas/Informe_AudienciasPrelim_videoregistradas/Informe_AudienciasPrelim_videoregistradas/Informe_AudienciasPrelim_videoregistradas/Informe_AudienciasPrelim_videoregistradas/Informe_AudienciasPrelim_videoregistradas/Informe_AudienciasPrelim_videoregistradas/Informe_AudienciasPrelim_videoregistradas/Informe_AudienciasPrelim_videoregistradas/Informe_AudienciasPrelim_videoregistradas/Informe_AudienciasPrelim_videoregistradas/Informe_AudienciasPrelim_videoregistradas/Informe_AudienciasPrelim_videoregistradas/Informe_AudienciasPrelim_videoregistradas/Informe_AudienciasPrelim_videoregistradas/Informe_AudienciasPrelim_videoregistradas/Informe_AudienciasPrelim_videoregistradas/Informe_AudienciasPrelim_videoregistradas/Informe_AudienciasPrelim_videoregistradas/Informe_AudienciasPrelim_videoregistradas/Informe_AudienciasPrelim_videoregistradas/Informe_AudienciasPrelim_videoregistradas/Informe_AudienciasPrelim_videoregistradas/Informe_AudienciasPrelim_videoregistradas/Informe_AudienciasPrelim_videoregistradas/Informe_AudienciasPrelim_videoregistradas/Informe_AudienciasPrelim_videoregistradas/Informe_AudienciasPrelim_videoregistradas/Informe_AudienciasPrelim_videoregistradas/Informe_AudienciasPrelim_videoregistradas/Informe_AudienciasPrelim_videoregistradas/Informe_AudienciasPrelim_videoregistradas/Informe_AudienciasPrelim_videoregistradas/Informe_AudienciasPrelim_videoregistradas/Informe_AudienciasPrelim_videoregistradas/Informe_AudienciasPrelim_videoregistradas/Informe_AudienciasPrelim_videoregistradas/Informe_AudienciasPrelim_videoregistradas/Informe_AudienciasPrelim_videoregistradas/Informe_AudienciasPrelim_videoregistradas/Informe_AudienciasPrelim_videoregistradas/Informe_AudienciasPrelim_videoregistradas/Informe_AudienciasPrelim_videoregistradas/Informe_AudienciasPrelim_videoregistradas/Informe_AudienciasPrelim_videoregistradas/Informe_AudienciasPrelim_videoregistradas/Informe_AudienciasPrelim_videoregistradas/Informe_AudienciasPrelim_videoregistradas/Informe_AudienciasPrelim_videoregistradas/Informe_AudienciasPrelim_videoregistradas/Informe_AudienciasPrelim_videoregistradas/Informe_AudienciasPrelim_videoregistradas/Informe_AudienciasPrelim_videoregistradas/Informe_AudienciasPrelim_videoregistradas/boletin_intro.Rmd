---
title: Boletín Mensual de Estadística
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

\pagebreak

## Introducción 

A través de este Boletín Mensual el Área de Planificación Gestión y Estadística acerca a sus órganos informantes los resultados obtenidos en materia de indicadores estadístico-judiciales, reafirmando el compromiso con la calidad de la información abrazado por nuestra comunidad, en el marco de transparencia e imparcialidad propugnado por nuestro Superior Tribunal de Justicia.    

Estos indicadores parten de los datos brindados por cada órgano de conformidad al Reglamento de Estadística (Res. T.S. Nº234/17) y las previsiones del art.20 de la LOPJ, como así también en el marco del principio de acceso a información pública (Art. 13 de la Constitución Provincial y 1º, 33, 41, 42 y 72 inc. 22 de la Constitución Nacional), cumpliendo las exigencias fijadas por la Ley Nacional 25326 de Protección de Datos Personales.   

Considerando el esfuerzo diario que realizan los órganos informantes en el mantenimiento y mejora de nuestro sistema estadístico, advirtiendo los importantes límites en materia de recursos y tecnologías que afectan el trabajo de registración, este boletín persigue una doble finalidad: por un lado, dar a conocer los indicadores estadísticos de cada organismo y, por el otro, mejorar la información judicial registrada subsanando errores e inconsistencias en la carga de datos.    

Por estas razones el Boletín Mensual de Estadística tendrá una instancia de revisión previa a la consolidación definitiva de datos que se cumplirá conforme al siguiente cronograma:   

**Instancia de revisión: Informe Mensual para Revisión**       

  + Día 13 de cada mes (o día hábil siguiente): remisión del Informe Mensual para Revisión – Fueros Civil-Comercial y Laboral.     
  + Día 18 de cada mes (o día hábil siguiente): remisión del Informe Mensual para Revisión – Fuero Penal.   

**Consolidado definitivo: Boletín Mensual**

  + Día 16 de cada mes (o día hábil siguiente): remisión del Boletín Mensual de Estadística – Fueros Civil-Comercial y Laboral-   
  + Día 21 de cada mes (o día hábil siguiente): remisión del Boletín Mensual de Estadística – Fuero Penal.   

Estimamos que esta instancia previa de revisión permitirá mejorar la calidad de la información generando una plataforma fáctica a partir de la cual asentar –al menos en parte- los diversos diálogos institucionales que demanda la marcha de nuestra institución.     

Al mismo tiempo, esperamos que la disponibilidad y periodicidad del boletín lo convierta en una herramienta útil para la gestión y planificación de cada unidad de trabajo, aspectos insoslayables de la actividad judicial en contextos de escasez de recursos.    

Para la lectura de estos documentos y dada su extensión proponemos las siguientes ayudas: el documento en formato pdf dispone de un índice de contenidos con hipervínculos de tal forma de navegar el informe hacia la parte de interés de cada lector (clic en el índice).  Así, cualquier revisión local no implica más que la lectura de 1 o 2 páginas. Eventualmente, para el caso dudas sobre el indicador de causas resueltas, agregamos el listado primario de datos subido por cada organismo para su consulta, lo que podría agregar a la lectura 1 o 2 páginas más. Para dudas sobre metodología remitimos a la lectura de la sección correspondiente donde están explicitados los procedimientos y pautas generales observadas en el cálculo de indicadores. Para dudas específicas, puede comunicarse con el área al teléfono 0343-4209409 int.396.       

Destacamos que los órganos cuyos datos no aparecen informados no tienen presentación a la fecha.    

Para la corrección de errores e inconsistencias detectadas cada magistrado y funcionario tiene a disposición diversos procedimientos de modificación de datos; desde la representación del listado de Lex-Doctor a través de sistema hasta la modificación manual de errores menores en los listados mediante el módulo de Presentaciones en JUSTAT (consultar en este enlace https://docs.google.com/document/d/18wAIEia5DF3rMnBHJSgnqPcyzSZXX-y69nxzEphBtxc/edit#heading=h.buodwbbrck7).  Las correcciones que se estimen necesarias para conformar los indicadores a los aspectos fácticas que éstos miden SOLO PUEDE REALIZARSE actualizando el listado primario de datos de Lex-Doctor presentado hasta las 23 hs. del día previo a la liberación del Boletín Mensual. Recordamos que La información primaria presentada por cada órgano judicial constituye un conjunto de datos único cuya integridad permanece inalterada desde su presentación, y se almacenan en un servidor seguro del área y no son modificados ni editados de ninguna forma durante el procesamiento estadístico, manteniendo su identidad desde la presentación. Están disponibles para consultas en todo momento.   

El Boletín Mensual de Estadística constituye –junto con los Estudios específicos- uno de los canales centrales de publicación de datos por parte del área, por lo que esperamos evolucione incorporando más y mejor información a partir de sus consultas e inquietudes.    

Finalmente, deseo agradecer todo el esfuerzo realizado para concretar este producto. Asumimos que, si la credibilidad de un órgano de estadística se cifra en su profesionalismo y transparencia, la de sus productos está en la fidelidad con la que representa aspectos de la realidad; fidelidad que no es posible alcanzar sino a través de un compromiso colectivo.    

Queda el área a su disposición,   

Lic. Sebastián Castillo    
Director   