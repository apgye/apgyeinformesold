# Cargamos los csv obtenidos de Google Forms y ajustados en cuanto a estructura

# Cerramos los Formularios hoy 12/08.

library(dplyr)

feria_cco_jul2019_txt <-
  read.delim("2019/Modelo_Parametrizado/data/Feria Civil-Laboral JULIO 2019_reparado_v2.txt")

feria_paz_jul2019_txt <-
  read.delim("2019/Modelo_Parametrizado/data/Feria Paz JULIO 2019_reparado_v2.txt")

feria_pen_jul2019_txt <-
  read.delim("2019/Modelo_Parametrizado/data/Feria Penal JULIO 2019_reparado_v2.txt")

# --- CIVIL
View(feria_cco_jul2019_txt)
names(feria_cco_jul2019_txt) <- c("fecha", "mail1", "mail2",
                                  "usuario", "usuario_dni", "Circunscripción",
                                  "lex", "Materia", "Magistrado", "causas_con_movim",
                                  "causas_con_habilit", "audiencias", "resoluciones",
                                  "observaciones")

View(feria_paz_jul2019_txt)
names(feria_paz_jul2019_txt) <- c("fecha", "mail1", "mail2",
                                  "usuario", "usuario_dni", "Circunscripción",
                                  "lex", "Materia", "Magistrado", "causas_con_movim",
                                  "causas_con_habilit", "tramites", "resoluciones",
                                  "observaciones")


# index by the original count
feria_cco_jul2019_txt$index <- row.names(feria_cco_jul2019_txt)
# Eliminado por re-presentaciones (manual)
# index 41 (2019/07/22 12:14:27 p.m.) reemplaza al 39 (2019/07/22 12:07:27 p.m.)
# index 8 (2019/07/13 8:05:13 p.m.) reemplaza al 7 (2019/07/13 7:55:33 p.m.)

feria_cco_jul2019_txt <- feria_cco_jul2019_txt %>% 
  filter(!index %in% c(39,7))

#names(feria_cco_jul2019_txt)
cco <- feria_cco_jul2019_txt %>% 
  #mutate(Magistrado = toupper(Magistrado)) %>% 
  group_by(Circunscripción) %>% 
  summarise(
    causas_con_movimiento = sum(causas_con_movim),
    causas_habilitacion = sum(causas_con_habilit),
    audiencias = sum(audiencias),
    resoluciones = sum(resoluciones)
  )
View(cco)

# Reimputación de Resoluciones de Colón según T889:
cco$resoluciones[cco$Circunscripción == "Colon"] <- 5

# Circunscripciones que no presentaron
jurisdicciones[!jurisdicciones %in% cco$Circunscripción]


# 
# cco_mostrar_completo <- feria_cco_jul2019_txt %>% 
#   select(-c("fecha", "mail1", "mail2",
#             "usuario", "usuario_dni",
#             "observaciones", "index"))
# 
# View(cco_mostrar_completo)

cco_observaciones <- feria_cco_jul2019_txt %>% 
  mutate(usuario = toupper(usuario)) %>% 
  select(c(usuario, Circunscripción, observaciones)) %>% 
  filter(!is.na(observaciones)) %>% 
  filter(!observaciones == "") %>% 
  filter(!observaciones == " ") %>% 
  filter(!observaciones == "  ") %>% 
  filter(!observaciones == "   ") %>% 
  filter(!observaciones == "    ") %>% 
  filter(!observaciones == "     ") %>% 
  filter(!grepl("NINGUNA|----", observaciones)) %>% 
  filter(!is.null(observaciones))

View(cco_observaciones)

jurisdicciones <- as.character(unique(cco$Circunscripción))
# --- PAZ

View(feria_paz_jul2019_txt)
names(feria_paz_jul2019_txt) <- c("fecha", "mail1", "mail2",
                                  "usuario", "usuario_dni", "Circunscripción",
                                  "lex", "Materia", "Magistrado", "causas_con_movim",
                                  "causas_con_habilit", "tramites", "resoluciones",
                                  "observaciones")

# index by the original count
feria_paz_jul2019_txt$index <- row.names(feria_paz_jul2019_txt)

# Eliminado por re-presentaciones (manual)
# index 8 reemplaza a la 6 y 7
# index 4 reemplaza a la 3
feria_paz_jul2019_txt <- feria_paz_jul2019_txt %>%
  filter(!index %in% c(3,6,7))

names(feria_paz_jul2019_txt)
paz <- feria_paz_jul2019_txt %>% 
  group_by(Circunscripción) %>% 
  summarise(
    causas_con_movimiento = sum(causas_con_movim),
    causas_habilitacion = sum(causas_con_habilit),
    tramites = sum(tramites),
    resoluciones = sum(resoluciones)
  )
View(paz)
# Circunscripciones que no presentaron
jurisdicciones[!jurisdicciones %in% paz$Circunscripción]

# paz_mostrar_completo <- feria_paz_jul2019_txt %>% 
#   select(-c("fecha", "mail1", "mail2",
#             "usuario", "usuario_dni", "index"))
# 
# View(paz_mostrar_completo)
paz_observaciones <- feria_paz_jul2019_txt %>% 
  mutate(usuario = toupper(usuario)) %>% 
  select(c(usuario, Circunscripción, observaciones)) %>% 
  filter(!is.na(observaciones)) %>% 
  filter(!observaciones == "") %>% 
  filter(!observaciones == " ") %>% 
  filter(!grepl("0|----", observaciones)) %>% 
  filter(!is.null(observaciones))

View(paz_observaciones)


# --- PENAL

View(feria_pen_jul2019_txt)
names(feria_pen_jul2019_txt)
names(feria_pen_jul2019_txt) <- c("fecha", "mail1", "mail2",
                                  "usuario", "usuario_dni", "Circunscripción",
                                  "lex", "Materia", "Magistrado", "causas_con_movim",
                                  "causas_con_habilit", "resoluciones",
                                  "medidas", "prision_prevent", "detenciones",
                                  "allanamientos", "formulac_cargos", "otras", 
                                  "audiencias", "observaciones")

# index by the original count
feria_pen_jul2019_txt$index <- row.names(feria_pen_jul2019_txt)

# Eliminado por re-presentaciones (manual)
# index 4 reemplaza al 3
feria_pen_jul2019_txt <- feria_pen_jul2019_txt %>%
  filter(!index == 3)

names(feria_pen_jul2019_txt)
pen <- feria_pen_jul2019_txt %>% 
  group_by(Circunscripción) %>% 
  summarise(
    causas_con_movimiento = sum(causas_con_movim),
    causas_habilitacion = sum(causas_con_habilit),
    resoluciones = sum(resoluciones),
    medidas = sum(medidas),
    prision_prevent = sum(prision_prevent),
    detenciones = sum(detenciones),
    allanamientos = sum(allanamientos),
    formulac_cargos = sum(formulac_cargos),
    otras = sum(otras),
    audiencias = sum(audiencias)
  )
View(pen)
# Circunscripciones que no presentaron
jurisdicciones[!jurisdicciones %in% pen$Circunscripción]
# pen_mostrar_completo <- feria_pen_jul2019_txt %>% 
#   select(-c("fecha", "mail1", "mail2",
#             "usuario", "usuario_dni", "index"))
# 
# View(pen_mostrar_completo)
pen_observaciones <- feria_pen_jul2019_txt %>% 
  mutate(usuario = toupper(usuario)) %>% 
  select(c(usuario, Circunscripción, observaciones)) %>% 
  filter(!is.na(observaciones)) %>% 
  filter(!observaciones == "") %>% 
  filter(!observaciones == " ") %>% 
  filter(!grepl("0|----", observaciones)) %>% 
  filter(!is.null(observaciones))

View(pen_observaciones)
