---
title: Serie Temporal de Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

<!-- \blandscape -->

## Casos Iniciados `r paste0(month(start_date,abbr=T,label =T),"-",year(start_date),"/",  month(ymd(end_date)-1,abbr =T,label =T),"-",year(end_date))`

```{r}
iniciados_paz_23c(poblacion = jdos_paz_23,
              start_date = start_date,
              end_date = end_date, 
              desagregacion_mensual = T) %>%
  ts_gpo_iniciados_paz_23() %>%  ungroup() %>% 
  mutate(grupo_proceso = str_replace_all(grupo_proceso, "proceso", "Procesos")) %>%
  mutate(grupo_proceso = str_replace_all(grupo_proceso, "tramite", "Tramites_Voluntarios")) %>% 
  group_by(grupo_proceso, fecha) %>% 
  summarise(iniciados = sum(iniciados, na.rm = T)) %>% 
  mutate(fecha = ymd(fecha)) %>% 
  filter(grupo_proceso != "otro") %>% 
  mutate(mes = floor_date(fecha, "month")) %>% 
  ggplot(aes(x = mes, y = iniciados)) + 
  geom_point() +
  geom_text(aes(label=iniciados),hjust= 0.5, vjust= 1.5) +
  geom_line(stat = "identity") +
  geom_smooth(se = F) +
  theme_minimal() +
  labs(x = " ", y = "Iniciados") +
  scale_x_date(date_breaks = "1 month", date_labels = "%b-%y" ) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1, size = 12), 
        strip.text = element_text(size = 18)) +
  facet_wrap(~ grupo_proceso, scales = "free_y") +
  labs(title = "Total Provinvial del Fuero de Paz", x = "",
       caption = "APGE-STJER")
```


## Procesos Iniciados por Juzgado `r paste0(month(start_date,abbr=T,label =T),"-",year(start_date),"/", month(ymd(end_date)-1,abbr =T,label =T),"-",year(end_date))`

```{r}
iniciados_paz_23c(poblacion = jdos_paz_23,
              start_date = start_date,
              end_date = end_date, 
              desagregacion_mensual = T) %>%
  ts_gpo_iniciados_paz_23() %>% 
  tsg_p23("proceso")
```

## Trámites Voluntarios Iniciados por Juzgado `r paste0(month(start_date,abbr=T,label =T),"-",year(start_date),"/",  month(ymd(end_date)-1,abbr =T,label =T),"-",year(end_date))`

```{r}
iniciados_paz_23c(poblacion = jdos_paz_23,
              start_date = start_date,
              end_date = end_date, 
              desagregacion_mensual = T) %>%
  ts_gpo_iniciados_paz_23() %>% 
  tsg_p23("tramite")
```


<!-- \elandscape -->

