library(dplyr)

poblacion <- jdos_cco %>% 
  filter(organismo == "jdocco0600pna")
start_date <- "2018-07-01"
end_date <- "2019-07-01"



resultado <- DB_PROD() %>% 
  apgyeTableData("AUDIC") %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !! poblacion$organismo) %>%
  filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
  mutate(fea = dmy(fea), fprog = dmy(fprog), ffa = dmy(ffa)) %>% 
  filter(fea >= data_interval_start, fea < data_interval_end) %>%
  select(-data_interval_start, -data_interval_end, -justiables) %>%
  collect() %>% 
  distinct(iep, nro, caratula, tproc, fea, ta, .keep_all = T) # Filtro Casos Duplicados

resultado <- resultado %>% 
  filter(id_submission == 11079) %>% #Presentacion Aud Civ Nov 2018 Civ 6
  collect()

View(resultado)


# ------------------

# Atentos a resultado Taiga, efectuar los cambios del punto 1:
# 1) Integrar la información declarada por tu organismo en el 2019 a la Base de
#   Datos 2018 del Proyecto de Oralidad
# MOTIVO: Se representó en Justat el 07/05/2019, id_submission: 15791

# Cambio a realizar: Agregar la 'S' al campo resultado_preliminar, del jdocco0600pna, Nro: 16035

oralidad2018 <- readRDS("~/apgyeinformes/2019/Grupo_Informes_Oralidad_v2/data/oralidad2018.rds")
View(oralidad2018)

# cambiamos caso Julio
oralidad2018$resultado_preliminar[oralidad2018$iep == "jdocco0600pna" &
                  oralidad2018$nro == "16035"] <- "S"


# cambiamos caso Diciembre
# leemos presentacion diciembre

id13720 <- apgyeDataAccess::viewFileId(13720)
id13720 <- id13720 %>% 
  filter(nro == 15879)
View(id13720)

names(oralidad2018)

dic_entry <- data.frame(
  iep = "jdocco0600pna"
  ,nro = id13720$nro
  ,caratula = abbreviate(id13720$caratula, minlength = 1, named = FALSE)
  ,tproc = id13720$tproc
  ,fecha_ingreso = as.Date(id13720$finicio, format="%d/%m/%Y")
  ,fecha_efect_preliminar = as.Date(id13720$fea, format="%d/%m/%Y")
  ,estado_preliminar = "realizada"
  ,resultado_preliminar = id13720$ra
  ,apertura_prueba = as.Date(id13720$faprueb, format="%d/%m/%Y")
  ,fprogramada_vista_causa = as.Date(id13720$fprog, format="%d/%m/%Y")
  ,fecha_efect_vcausa = NA
  ,estado_vcausa = NA
  ,resultado_vcausa = NA
  ,fecha_despacho = NA
  ,fecha_resolucion = NA
  ,as = NA
  ,forma_deresolucion = NA
  ,sinVC_PrInfDoc = NA
)
str(oralidad2018)
new_df <- bind_rows(oralidad2018, dic_entry)

View(new_df)

saveRDS(new_df, "2019/Grupo_Informes_Oralidad_v2/data/oralidad2018.rds")

oralidad2018_nuevo <- readRDS("~/apgyeinformes/2019/Grupo_Informes_Oralidad_v2/data/oralidad2018.rds")
View(oralidad2018_nuevo)


# --- BUSCAR ALGUN fea con día 01

dia1 <- oralidad2018_nuevo %>% 
  filter(grepl("-01", fecha_efect_preliminar) | grepl("-01", fecha_efect_vcausa))
