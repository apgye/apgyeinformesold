---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
resultado <- iniciados_paz_23c(poblacion = jdos_paz_23,
                                       start_date = start_date,
                                       end_date = end_date, estadistico = "conteo")
```

## Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

Observación: los órganos que migraron sus sistemas a la versión Lex-Doctor 9 tienen su cómputo de causas iniciadas en la presente tabla y en la tabla correspondiente a los Juzgados de Paz de primera categoría. El total de iniciados aparece sumarizado en los gráficos de las próximas secciones.

Adicionalmente se detallan los Órganos que migraron de versión de Lex: el Juzgado de Paz de **Crespo** informó en ésta sección hasta Mayo 2019 y los Juzgados de Paz de **Oro Verde** y **San Benito** hasta Junio 2019. Los períodos posteriores se representan en la Sección correspondiente a los Juzgados de Paz de 1er Cat.

```{r, paz23_ci, eval = (ci & t)}
resultado$inic %>%
  iniciados_paz_comparativo_23(cfecha = desagregacion_mensual) %>%
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90)
```

`r if (ci & st) '## Total de Casos Iniciados del Fuero'`
 
```{r, paz23_ci_st_total, eval= (ci & st)}
resultado$inic %>%
  ts_gpo_iniciados_paz_23() %>%  ungroup() %>% 
  mutate(grupo_proceso = str_replace_all(grupo_proceso, "proceso", "Procesos")) %>%
  mutate(grupo_proceso = str_replace_all(grupo_proceso, "tramite", "Tramites_Voluntarios")) %>% 
  group_by(grupo_proceso, fecha) %>% 
  summarise(iniciados = sum(iniciados, na.rm = T)) %>% 
  mutate(fecha = ymd(fecha)) %>% 
  filter(grupo_proceso != "otro") %>% 
  mutate(mes = floor_date(fecha, "month")) %>% 
  ggplot(aes(x = mes, y = iniciados)) + 
  geom_point() +
  geom_text(aes(label=iniciados),hjust= 0.5, vjust= 1.5) +
  geom_line(stat = "identity") +
  geom_smooth(se = F) +
  theme_minimal() +
  labs(x = " ", y = "Iniciados") +
  scale_x_date(date_breaks = "1 month", date_labels = "%b-%y" ) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1, size = 12), 
        strip.text = element_text(size = 18)) +
  facet_wrap(~ grupo_proceso, scales = "free_y") +
  labs(title = "Total Provinvial del Fuero de Paz", x = "",
       caption = "APGE-STJER")
```

`r if (ci & st) '## Procesos Iniciados por Juzgado'`

```{r, paz23_ci_st_proc, eval= (ci & st)}
resultado$inic %>%
  ts_gpo_iniciados_paz_23() %>% 
  tsg_p23("proceso")
```

`r if (ci & st) '## Trámites Voluntarios Iniciados por Juzgado'`

```{r, paz23_ci_st_tram, eval= (ci & st)}
resultado$inic %>%
  ts_gpo_iniciados_paz_23() %>% 
  tsg_p23("tramite")
```

