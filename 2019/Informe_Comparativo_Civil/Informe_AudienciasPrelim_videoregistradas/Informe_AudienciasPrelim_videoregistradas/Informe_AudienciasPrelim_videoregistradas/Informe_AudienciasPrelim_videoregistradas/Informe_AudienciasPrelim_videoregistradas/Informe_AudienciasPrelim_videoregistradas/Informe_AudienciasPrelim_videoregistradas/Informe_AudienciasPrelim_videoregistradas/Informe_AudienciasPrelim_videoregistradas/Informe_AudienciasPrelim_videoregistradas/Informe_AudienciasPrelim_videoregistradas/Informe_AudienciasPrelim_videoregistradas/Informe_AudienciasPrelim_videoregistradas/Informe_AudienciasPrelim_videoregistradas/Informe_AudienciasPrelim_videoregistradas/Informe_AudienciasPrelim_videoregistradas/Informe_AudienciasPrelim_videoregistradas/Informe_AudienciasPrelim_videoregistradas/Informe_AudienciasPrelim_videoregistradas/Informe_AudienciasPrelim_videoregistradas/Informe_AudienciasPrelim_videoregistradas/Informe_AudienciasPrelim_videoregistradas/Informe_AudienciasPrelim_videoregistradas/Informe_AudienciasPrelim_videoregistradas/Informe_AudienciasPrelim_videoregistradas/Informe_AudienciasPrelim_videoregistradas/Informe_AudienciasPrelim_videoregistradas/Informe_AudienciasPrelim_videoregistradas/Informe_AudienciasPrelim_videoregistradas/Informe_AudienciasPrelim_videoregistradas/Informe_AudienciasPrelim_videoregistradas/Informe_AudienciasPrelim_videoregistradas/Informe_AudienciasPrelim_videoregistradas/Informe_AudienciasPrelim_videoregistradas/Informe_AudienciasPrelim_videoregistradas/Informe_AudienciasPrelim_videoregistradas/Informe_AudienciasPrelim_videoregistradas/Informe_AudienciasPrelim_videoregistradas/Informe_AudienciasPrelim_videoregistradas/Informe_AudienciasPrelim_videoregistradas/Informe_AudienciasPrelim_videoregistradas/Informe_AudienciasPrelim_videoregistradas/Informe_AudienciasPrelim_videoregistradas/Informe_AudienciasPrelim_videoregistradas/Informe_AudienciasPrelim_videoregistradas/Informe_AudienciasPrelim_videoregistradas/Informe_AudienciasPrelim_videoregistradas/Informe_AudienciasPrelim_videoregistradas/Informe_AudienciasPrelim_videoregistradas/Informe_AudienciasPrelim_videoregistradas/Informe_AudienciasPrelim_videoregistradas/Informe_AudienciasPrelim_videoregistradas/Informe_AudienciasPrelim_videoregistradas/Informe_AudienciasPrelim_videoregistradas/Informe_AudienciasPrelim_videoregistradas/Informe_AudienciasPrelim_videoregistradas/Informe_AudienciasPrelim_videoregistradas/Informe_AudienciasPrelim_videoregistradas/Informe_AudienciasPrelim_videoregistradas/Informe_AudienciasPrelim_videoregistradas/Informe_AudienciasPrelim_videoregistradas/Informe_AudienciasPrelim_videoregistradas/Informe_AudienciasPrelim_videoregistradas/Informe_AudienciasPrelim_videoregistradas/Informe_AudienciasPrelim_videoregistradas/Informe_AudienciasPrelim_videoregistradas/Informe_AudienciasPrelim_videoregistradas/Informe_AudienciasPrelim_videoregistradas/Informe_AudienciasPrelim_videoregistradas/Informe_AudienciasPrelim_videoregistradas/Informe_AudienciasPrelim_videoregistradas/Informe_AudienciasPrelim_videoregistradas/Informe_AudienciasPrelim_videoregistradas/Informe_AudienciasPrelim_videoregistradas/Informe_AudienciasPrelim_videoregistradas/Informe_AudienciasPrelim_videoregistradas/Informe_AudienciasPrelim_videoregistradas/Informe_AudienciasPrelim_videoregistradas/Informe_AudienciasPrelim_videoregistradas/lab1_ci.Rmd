---
title: Causas Iniciadas ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
resultado <- iniciados_lab(poblacion = jdos_lab, start_date = start_date,
    end_date = end_date, estadistico = "conteo")

```


## Causas Iniciadas por Grupo de Procesos - `r getDataIntervalStr(start_date, end_date)`

```{r, lab1_ci, eval = (ci & t), echo=FALSE }
resultado$inic %>% 
  iniciados_lab_comparativo(cfecha = desagregacion_mensual) %>%
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```


```{r, lab1_ci_g, eval= (ci & g),  echo=FALSE}
resultado$inic %>% 
  iniciados_lab_xg() %>%
  inic_lab_graf_xg()
```




