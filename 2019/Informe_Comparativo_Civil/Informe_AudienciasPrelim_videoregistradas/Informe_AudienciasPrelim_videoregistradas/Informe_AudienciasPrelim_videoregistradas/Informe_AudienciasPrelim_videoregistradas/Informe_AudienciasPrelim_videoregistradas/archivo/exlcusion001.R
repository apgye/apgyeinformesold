# Se practica exclusión de casos informados por el juzgado de familia de colon
# por ser casos iniciados con fechas anteriores al 2019 trasnferidos por los juzgados civiles 
# por razones de competencia. A estos casos erróneamente se le cargó fecha de inicio.

jdos_fam
start_date = "2019-02-01"
end_date = "2019-08-01"

# Caso testigo 13366 y 11462


resultado <- DB_PROD() %>% 
  apgyeTableData("CAMOV") %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% c("jdofam0001col", "jdofam0002col")) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
  mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
  select(iep, nro, caratula, tproc, finicio, fmov, movt, data_interval_start, data_interval_end) %>% 
  collect() %>% 
  resolverconvertidos() %>% 
  tidyr::separate_rows(movt, sep="%")  %>%
  tidyr::separate(movt, into = c("fmovinterno", "descripcion"), sep="\\$") %>% 
  filter(!is.na(fmovinterno)) %>%
  mutate(fmovinterno = as.Date(fmovinterno, format = "%d/%m/%Y")) %>% 
  filter(finicio >= make_date(2019,01,01)) %>% 
  group_by(iep, nro, caratula, tproc, finicio) %>% 
  slice(which.min(fmovinterno)) %>% 
  mutate(transferido = finicio > fmovinterno) %>% 
  filter(transferido) %>% 
  ungroup() %>% 
  select(iep, nro, finicio)

saveRDS(resultado, file = "transferidos.rds")

# Restore the object
transferidos <- readRDS(file = "~/apgyeinformes/2019/Modelo_Parametrizado/data/exclusion001.rds")

