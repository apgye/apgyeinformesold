---
title: Causas Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
resultado <- iniciadas_pen(poblacion = sal_pen,
              start_date = start_date,
              end_date = end_date) 
```


## Causas Iniciadas en Sala- `r getDataIntervalStr(start_date, end_date)`

```{r, pen_sal_ci_t, eval=(ci & t)}
resultado$inic_xorg %>% 
  iniciadas_pen_complemento(desagregacion_mensual = desagregacion_mensual) %>% 
  kable(caption = str_c("Causas Iniciados"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) 
```

\pagebreak

`r if (ci & td) '### Agrupados por Tipo de Proceso'`

```{r, pen_sal_ci_pc, eval= (ci & td)}
resultado$inic_xmat_proc %>% 
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  %>% 
  landscape()
```



