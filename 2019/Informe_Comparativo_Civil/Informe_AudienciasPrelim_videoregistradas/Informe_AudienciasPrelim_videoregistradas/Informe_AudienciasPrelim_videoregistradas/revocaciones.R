# Procesa Resoluciones en órganos civiles - todas las materias -

revocaciones <- function(poblacion, operacion = "CADR2C", start_date = "2018-07-01", end_date = "2018-09-02") {
  
  
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(!grepl("AMPARO|HABEAS|ACCION DE PROHIBICION|ACCION DE EJECUCION", tproc)) %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    group_by(iep, data_interval_start, data_interval_end) %>% 
    process() %>%
    .$result %>%
    ungroup()
 
  
  confirma <- resultado %>% 
    mutate(año = lubridate::year(data_interval_start)) %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
    select(organismo = iep, año, mes, tres_confirmacion_juez) %>% 
    tidyr::separate_rows(tres_confirmacion_juez, sep="&")  %>%
    tidyr::separate(tres_confirmacion_juez, into = c("jact", "nro"), sep="%") %>% 
    mutate(tres = "confirmación")
  
  revoca <- resultado %>% 
    mutate(año = lubridate::year(data_interval_start)) %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
    select(organismo = iep, año, mes, tres_revocacion_juez ) %>% 
    tidyr::separate_rows(tres_revocacion_juez, sep="&")  %>%
    tidyr::separate(tres_revocacion_juez, into = c("jact", "nro"), sep="%") %>% 
    mutate(tres = "revocación")
  
  revoca_parcial <- resultado %>% 
    mutate(año = lubridate::year(data_interval_start)) %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
    select(organismo = iep, año, mes, tres_revoc_parcial_juez) %>% 
    tidyr::separate_rows(tres_revoc_parcial_juez, sep="&")  %>%
    tidyr::separate(tres_revoc_parcial_juez, into = c("jact", "nro"), sep="%") %>% 
    mutate(tres = "revocación_parcial")
  
  anula <- resultado %>% 
    mutate(año = lubridate::year(data_interval_start)) %>% 
    mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
    select(organismo = iep, año, mes, tres_nulidad_juez) %>% 
    tidyr::separate_rows(tres_nulidad_juez, sep="&")  %>%
    tidyr::separate(tres_nulidad_juez, into = c("jact", "nro"), sep="%") %>% 
    mutate(tres = "nulidad") %>% 
    filter(!is.na(jact))
  
  
  resultado <- bind_rows(confirma, revoca, revoca_parcial, anula) 
  
  
  juez <- resultado %>% as.data.frame() %>%
    select(jact) %>%
    dplyr::mutate(rn = row_number()) %>% separate_rows(jact) %>%
    left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                mutate(
                  magistrado = apellido,
                  iep_actuante = iep
                ) %>%
                select(idagente, iep_actuante, magistrado), by=c("jact"="idagente"), copy = T) %>%
    .$magistrado
  
  resultado <- resultado %>% 
    mutate(jact = juez) 
  
  resultado_base <- resultado %>% 
    mutate(tipo_nro = str_c(tres, nro, sep = ":")) %>% 
    group_by(jact, año, mes) %>% 
    summarise(cantidad = n(), 
              'tipo de resolución y nro. expte.' = str_c(tipo_nro, collapse = "; ")) %>% 
    filter(!is.na(jact))
     
  
  # FALLA EN NULIDAD; por no venir en el df la columna 'nulidad'
  c_names <- names(resultado)
  resultado <- resultado %>% 
    mutate(cantidad = 1) %>% 
    tidyr::spread(tres, cantidad) %>% 
    group_by(organismo, año, mes, jact) %>% 
    summarise(confirmación = sum(confirmación, na.rm = T), 
              revocación = sum(revocación, na.rm = T), 
              revocación_parcial = sum(revocación_parcial, na.rm = T), 
              nulidad = if ("nulidad" %in% c_names) sum(nulidad, na.rm = T) else 0, 
              causas_nros = str_c(nro, collapse = ";")) 
  
  
  porcentajes <- resultado %>% 
    ungroup() %>% 
    select(jact, confirmación, revocación, revocación_parcial, nulidad) %>% 
    janitor::adorn_percentages() %>% 
    mutate_if(is.numeric, list(~ . * 100)) %>% 
    mutate_if(is.numeric, round, digit = 1) %>% 
    select(-jact) %>% 
    rename_all(~paste0(., "%"))
    #rename_all(list(~paste0), "%")
  
  
  resultado <- resultado %>% 
    bind_cols(porcentajes ) %>% 
    select(organismo, año, mes, juez = jact, everything()) %>%
    mutate_at(vars(matches("%")), str_c, "%") %>% 
    ungroup() 
  
  lista_resultados <- list()
  
  lista_resultados$resultado_base <- resultado_base
  
  lista_resultados$resultado <- resultado
  
  lista_resultados$resultado_gral <- resultado %>% 
    select(confirmación, revocación, revocación_parcial, nulidad) %>% 
    summarise_all(sum)
  
  lista_resultados$resultado_xadquem <- resultado %>% 
    group_by(organismo) %>% 
    summarise(confirmación = sum(confirmación, na.rm = T), 
              revocación = sum(revocación, na.rm = T), 
              revocación_parcial = sum(revocación_parcial, na.rm = T), 
              nulidad = sum(nulidad, na.rm = T)) %>% 
    bind_cols(janitor::adorn_percentages(.) %>% 
                mutate_if(is.numeric, list(~ . * 100)) %>% 
                mutate_if(is.numeric, round, digit = 1) %>% 
                mutate_if(is.numeric, as.character)) %>% 
    select(-organismo1) %>% 
    #rename_at(vars(matches("1")), funs(str_replace(., "1", "%"))) %>% 
    rename_at(vars(matches("1")), str_replace, "1", "%") %>% 
    ungroup() %>% 
    left_join(poblacion[, c("organismo", "organismo_descripcion","circunscripcion")], by = "organismo") %>% 
    select(circunscripcion, organismo_descripcion, everything(), -organismo) %>% 
    rename(organismo = organismo_descripcion) %>% 
    janitor::adorn_totals("row")
  
  
  lista_resultados$resultado_xaquo <- resultado %>% 
    group_by(organismo, juez) %>% 
    summarise(confirmación = sum(confirmación, na.rm = T), 
              revocación = sum(revocación, na.rm = T), 
              revocación_parcial = sum(revocación_parcial, na.rm = T), 
              nulidad = sum(nulidad, na.rm = T)) %>% 
    bind_cols(janitor::adorn_percentages(.) %>% 
                mutate_if(is.numeric, list(~. * 100)) %>% 
                mutate_if(is.numeric, round, digit = 1) %>% 
                mutate_if(is.numeric, as.character)) %>% 
    select(-organismo1, -juez1) %>%
    #rename_at(vars(matches("1")), funs(str_replace(., "1", "%"))) %>%
    rename_at(vars(matches("1")), str_replace, "1", "%") %>% 
    left_join(poblacion[, c("organismo", "organismo_descripcion","circunscripcion")], by = "organismo") %>% 
    select(circunscripcion, organismo_descripcion, everything()) %>% 
    ungroup() %>% select(-organismo) %>% 
    rename(organismo = organismo_descripcion, 'magistrado/a' = juez) %>% 
    group_by(circunscripcion, organismo) %>% 
    do(janitor::adorn_totals(.))
  
  lista_resultados <<- lista_resultados
  
} 



