---
title: Oralidad
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
source("~/apgyeinformes/2019/Grupo_Informes_Oralidad/utilsO.R")
source("oralidad.R")
```

```{r}
## Casos Oralizados
#Casos2018
df_casos_base <- readRDS("~/apgyeinformes/2019/Grupo_Informes_Oralidad/data/oralidad2018.rds")
#Preliminares
audic_prelim_raw <- preliminaresO(jdos_civiles, "2019-02-01", end_date)
# Audiencia Vista Causa
audic_vcausa_raw <- vcausaO(jdos_civiles, "2019-02-01", end_date)
# Resoluciones
resoluciones <- resolucionesO(jdos_civiles, "2019-02-01", end_date)
# Consolidado
# imputs df_1ros_casos, audic_prelim_raw, audic_vcausa_raw
consolidado <- consolidar(df_casos_base, audic_prelim_raw, audic_vcausa_raw, resoluciones)


consolidado_cfmeMINJUS <- consolidado %>% 
  left_join(jdos_civiles[, c("organismo", "organismo_descripcion", 
                           "circunscripcion")], by = c("iep" = "organismo")) %>% 
  select(circunscripcion, organismo_descripcion, everything()) %>% 
  mutate(organismo = str_remove_all(iep, "[a-z,0]")) %>%  
  mutate(fecha_resolucion = as.character(fecha_resolucion)) %>%
  #reimputación solicitada por el ministerio siempre que resultado preliminar T, 
  #forma de resolucion = Conciliacion y fecha de efectivizacion
  mutate(forma_deresolucion =  ifelse(resultado_preliminar == "T" & 
                                        !is.na(resultado_preliminar), 
                                      "Conciliación", forma_deresolucion), 
         fecha_resolucion =  ifelse(resultado_preliminar == "T" & 
                                      is.na(fecha_resolucion), 
                                    as.character(fecha_efect_preliminar), 
                                    fecha_resolucion)) %>% 
  mutate(forma_deresolucion =  ifelse(resultado_vcausa == "T" & 
                                        !is.na(resultado_vcausa), 
                                      "Conciliación", forma_deresolucion),
         fecha_resolucion =  ifelse(resultado_vcausa == "T" & 
                                      is.na(fecha_resolucion), 
                                    as.character(fecha_efect_vcausa), 
                                    fecha_resolucion)) %>% 
   select(circunscripcion, organismo_descripcion, iep, organismo, everything()) %>% 
   arrange(circunscripcion, organismo_descripcion)  

resumen <- consolidado_cfmeMINJUS %>% 
  group_by(circunscripcion, organismo_descripcion) %>% 
  summarise(cantidad_procesos_en_oralidad_informados = n(),
    cantidad_preliminares_realizadas = sum(!is.na(fecha_efect_preliminar) &
                                             estado_preliminar == "realizada"),
    cantidad_preliminares_no_realizadas = sum(!is.na(fecha_efect_preliminar) &
                                             estado_preliminar == "no_realizada"),
    cantidad_vista_causa_programadas = sum(!is.na(fprogramada_vista_causa)),
    cantidad_vista_causa_realizadas = sum(!is.na(fecha_efect_vcausa) &
                                            estado_vcausa == "realizada"),
    cantidad_vista_causa_no_realizadas = sum(!is.na(fecha_efect_vcausa) &
                                            estado_vcausa == "no_realizada"), 
    cantidad_conciliaciones_enAPoAVC = sum(forma_deresolucion == "Conciliación", na.rm = T),
    cantidad_sentencias_procesos_en_oralidad = sum(!is.na(fecha_resolucion) &
                                                     forma_deresolucion != "Conciliación")) %>% 
  select(circunscripcion, organismo_descripcion, everything()) %>% 
  arrange(circunscripcion, organismo_descripcion)  %>% 
  janitor::adorn_totals("row")

consolidado_informesSTJ <- consolidado_cfmeMINJUS %>%
  filter(estado_preliminar == "realizada" | estado_vcausa == "realizada") %>%
  select(circunscripcion, organismo_abbrev = organismo_descripcion, nro_expte = nro,
          fecha_ingreso, fecha_efect_preliminar, resultado_preliminar, apertura_prueba,
          fprogramada_vista_causa, sinVC_PrInfDoc, fecha_efect_vcausa, resultado_vcausa,
          fecha_resolucion, as, forma_deresolucion) %>% 
  mutate(circunscripcion = abbreviate(circunscripcion, minlength = 6), 
         organismo_abbrev = abbreviate(organismo_abbrev, minlength = 13)) 


consolidado_comparativo <- consolidado %>% 
  agrupartproc(materia = "civil") %>% 
  group_by(iep, gtproc_forma, tproc) %>% 
  summarise(cantidad = n()) %>% 
  arrange(gtproc_forma, desc(cantidad)) %>% 
  do(janitor::adorn_totals(.)) %>% 
  ungroup() %>% 
  mutate(tproc = ifelse(gtproc_forma == "-", "subtotal", tproc)) %>% 
  left_join(jdos_civiles %>% select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  select(circunscripcion, organismo, grupo_proceso = gtproc_forma, tipo_proceso = tproc, everything(), -iep) %>% 
  iniciados_cco_comparativo() 


```


## Causas Iniciadas según clasificación en Procesos de Conocimiento y Otros Tipos de Proceso 

En las próximas tablas se presentan totales de causas iniciadas en los órganos civiles y comerciales (excluyendo a juzgados de Ejecución, Concursos y Quiebras) clasificadas en procesos de conocimiento y otros tipos de proceso. 

### Período `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados_conocimiento_resumen %>% 
  kable(caption = "Procesos de Conocimiento Iniciados en el Período", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

*** 

```{r}
iniciados_noconocimiento_xgpo %>% 
  kable(caption = "Detallle por grupos de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

***

```{r}
iniciados_noconocimiento %>% 
  kable(caption = "Detallle desagrupado de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)%>% 
  landscape()
```

\pagebreak

### Período julio - diciembre 2018

```{r}
iniciados_conocimiento_resumen_2018 %>% 
  kable(caption = "Procesos de Conocimiento Iniciados en el Período", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

*** 

```{r}
iniciados_noconocimiento_xgpo_2018 %>% 
  kable(caption = "Detallle por grupos de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0)

```

***

```{r}
iniciados_noconocimiento_2018 %>% 
  kable(caption = "Detallle desagrupado de procesos que no son de conocimiento", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0) %>% 
  landscape()
```

\pagebreak

## Datos de Avance del Proyecto de Oralidad a la fecha: `r end_date`

```{r}
kable(resumen, caption = "Resumen", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90)
```

\pagebreak

## Datos particulares del conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

```{r, size = 'tiny'}
kable(consolidado_informesSTJ, caption = "Tabla General", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 8, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>%
  landscape() 
```


