# https://www.r-graph-gallery.com/297-circular-barplot-with-groups/

poblacion <- jdos_cco

df <- duracion(jdos_cco, desagregacion_mensual = F, desagregacion_xorg = F)

df <- df %>% 
  filter(cantidad_casos > 20) %>% 
  mutate(group = "civil") %>% 
  rename(value = mediana_duracion)


df_fam <- duracion(jdos_fam, desagregacion_mensual = F, desagregacion_xorg = F)

df_fam <- df_fam %>% 
  filter(cantidad_casos > 20) %>% 
  mutate(group = "familia") %>% 
  rename(value = mediana_duracion)

data <- bind_rows(df, df_fam)

data <- data %>% 
  select(tipo_proceso, value, group) %>% 
  filter(between(value, 100, 2000))

# Set a number of 'empty bar' to add at the end of each group
empty_bar = 10
to_add = data.frame(matrix(NA, empty_bar * nlevels(data$group), ncol(data)))
colnames(to_add) = colnames(data)
to_add$group=rep(levels(data$group), each=empty_bar)
data=rbind(data, to_add)
data=data %>% arrange(group)
data$id=seq(1, nrow(data))

# Get the name and the y position of each label
label_data=data
number_of_bar=nrow(label_data)
angle= 90 - 360 * (label_data$id-0.5) /number_of_bar     # I substract 0.5 because the letter must have the angle of the center of the bars. Not extreme right(1) or extreme left (0)
label_data$hjust<-ifelse( angle < -90, 1, 0)
label_data$angle<-ifelse(angle < -90, angle+180, angle)

# Make the plot
p = ggplot(data, aes(x=as.factor(id), y=value, fill=group)) +       # Note that id is a factor. If x is numeric, there is some space between the first bar
  geom_bar(stat="identity", alpha=0.5) +
  #ylim(-100,120) +
  theme_minimal() +
  theme(
    legend.position = "none",
    axis.text = element_blank(),
    axis.title = element_blank(),
    panel.grid = element_blank(),
    plot.margin = unit(rep(-1,4), "cm") 
  ) +
  coord_polar() + 
  geom_text(data=label_data, aes(x=id, y=value+10, label=tipo_proceso, hjust=hjust), color="black", fontface="bold",alpha=0.6, size=2.5, angle= label_data$angle, inherit.aes = FALSE ) 

p
