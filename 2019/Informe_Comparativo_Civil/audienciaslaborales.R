
audilab_prim <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02"){
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIL") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!poblacion$organismo) %>%
    filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
    mutate(fea = dmy(fea)) %>% 
    # mutate(audvid = as.integer(audvid)) %>% no tiene campo de videofilmacion
    filter(fea >= data_interval_start, fea < data_interval_end) %>%
    select(-data_interval_start, -data_interval_end, -justiables) %>%
    collect() 
  
  #resultado <- resolverconvertidos(resultado)
  
  resultado <- resultado %>%
    mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
    mutate(duracm = str_trim(duracm)) %>%
    mutate(ra = str_sub(toupper(ra), 1,1)) %>%
    mutate(ta = str_sub(toupper(ta), 1,1)) %>%
    mutate(ta = str_trim(ta)) %>% 
    mutate(esta = str_remove_all(esta, "[:alpha:]|[:punct:]")) %>%
    mutate(esta = str_trim(esta)) %>% 
    codconver("AUDIL", "esta") %>% 
    codconver("AUDIL", "ra") %>% 
    codconver("AUDIL", "ta") 
  
  resultado
}

audilab_xestado <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      select(-fijada) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      resultado
      
    } else {
      resultado <- resultado %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      resultado
    }
    
   
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, esta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(esta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audilab_realizadas_xtipoyresultado <- function(df, desagregacion_mensual = TRUE) {
  
  df <- df %>% 
    filter(esta == "realizada") %>% 
    filter(ta != "otras")
  
  if(desagregacion_mensual) {
    resultado <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col") 
    
    resultado
    
  } else {
    resultado <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo,ta, ra) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ra, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion,  everything(), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    resultado
  }
  resultado
}

audilab_realizadas_xtipo_cantidad_minutos <- function(df, desagregacion_mensual = TRUE) {
  
  un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
  
  df <- df %>% 
    filter(esta == "realizada") 
  
  if(desagregacion_mensual) {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
    
    if(un_mes) {
      cantidad_audiencias <- cantidad_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
      
    } else {
      cantidad_audiencias <- cantidad_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      cantidad_audiencias
    }
    
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep, año_mes = format(as.Date(fea), "%Y-%m"))  %>%
      group_by(organismo, año_mes, ta) %>%
      summarise(minutos = sum(as.integer(duracm), na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>%
      ungroup() %>%
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, año_mes, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion, año_mes)
      
    
    if(un_mes) {
      minutos_audiencias <- minutos_audiencias %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    } else {
      minutos_audiencias <- minutos_audiencias %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias
      
    }
    
  } else {
    
    cantidad_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(ta, cantidad, drop = F, fill = 0) %>% 
      ungroup() %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col") 
    
    minutos_audiencias <- df %>%
      mutate(organismo = iep)  %>%
      group_by(organismo, ta) %>%
      summarise(minutos = sum(as.integer(duracm), na.rm = T)) %>%
      tidyr::spread(ta, minutos, drop = F, fill = 0) %>%
      ungroup() %>%
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>%
      arrange(circunscripcion, organismo_descripcion) %>%
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col")

    
    
  }
  
  resultado <- list()
  
  resultado$minutos_audiencias <- minutos_audiencias
  resultado$cantidad_audiencias <- cantidad_audiencias
  
  resultado
  
}

# Segundo Nivel ---------------------------------------------------------------

resumir_audil <- function(poblacion, start_date, end_date, desagregacion_mensual = desagregacion_mensual) {
  
  #periodo <- as.period(interval(ymd(start_date),ymd(end_date))) %>% month()
  
  df <- audilab_prim(poblacion, start_date, end_date) 
  
  cantidad_minutos <- audilab_realizadas_xtipo_cantidad_minutos(df, desagregacion_mensual = T) 
  
  xresultado <- audilab_realizadas_xtipoyresultado(df, desagregacion_mensual = T) 
  
  audil_tb <- cantidad_minutos$cantidad_audiencias %>% 
    filter(circunscripcion != "Total") %>% 
    select(circunscripcion, organismo_descripcion, año_mes, audiencias_realizadas = Total, everything()) %>% 
    left_join(cantidad_minutos$minutos_audiencias %>%  
                filter(circunscripcion != "Total") %>% 
                select(circunscripcion, organismo_descripcion, año_mes, Total) %>% 
                rename(minutos_en_audiencia = Total), 
              by = c("circunscripcion", "organismo_descripcion", "año_mes")) %>% 
    left_join(xresultado %>% 
                filter(circunscripcion != "Total") %>%
                rename(conciliacion_total = Conciliacion_Total, conciliacion_parcial = Conciliacion_Parcial) %>% 
                group_by(circunscripcion, organismo_descripcion, año_mes) %>% 
                summarise(conciliaciones_totales = sum(conciliacion_total, na.rm = T), 
                          conciliaciones_parciales = sum(conciliacion_parcial, na.rm = T)), 
              by = c("circunscripcion", "organismo_descripcion", "año_mes")) %>% 
    # mutate(horas_en_audiencia = round(minutos_en_audiencia/60)) %>% 
    select(circunscripcion, organismo_descripcion, año_mes,
           audiencias_realizadas, 
           tiempo_en_audiencia = minutos_en_audiencia, 
           conciliaciones_totales,
           conciliaciones_parciales, 
           everything())
  
  
  if(desagregacion_mensual) {
    
    audil_tb <- audil_tb %>% 
      #mutate(tiempo_en_audiencia = substr(chron::times((tiempo_en_audiencia%/%60 +  tiempo_en_audiencia%%60 /60)/24), 1, 5)) 
      mutate(tiempo_en_audiencia = round(tiempo_en_audiencia/60))
  } else {
    
    audil_tb <- audil_tb %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      summarise('total audiencias realizadas' = sum(audiencias_realizadas, na.rm = T), 
                'promedio mensual realizadas' = round(mean(audiencias_realizadas, na.rm = T), digits = 1), 
                p = as.integer(round(mean(tiempo_en_audiencia, na.rm = T))),
                'suma conciliaciones totales' = sum(conciliaciones_totales, na.rm = T), 
                'promedio mensual conc.tot.' = round(mean(conciliaciones_totales, na.rm = T), digits = 1),
                'suma conciliaciones parciales' = sum(conciliaciones_parciales, na.rm = T),
                'promedio mensual conc.parc.' = round(mean(conciliaciones_parciales, na.rm = T), digits = 1),
                'conciliaciones' = sum(conciliacion, na.rm = T),
                'vista de causas' = sum(vista_causa, na.rm = T),
                'otras' = sum(otras, na.rm = T),
                'sd' = sum(sd, na.rm = T)) %>% 
      #mutate(p = substr(chron::times((p%/%60 + p%%60 /60)/24), 1, 5)) %>%
      mutate(p = round(p/60)) %>% 
      janitor::adorn_totals("row") %>% 
      rename('promadio mensual de horas en audiencia' = p)
    
    
  }
  
  audil_tb
  
}
