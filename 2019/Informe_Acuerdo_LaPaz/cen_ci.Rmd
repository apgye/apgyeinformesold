---
title: Mediaciones Iniciadas 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Mediaciones Iniciadas por Circunscripción - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestran grupos de tipos de Mediación.


```{r, cen_ci, eval= (ci & t) }
  inic <- iniciados_cen(poblacion = cen,
              start_date = start_date,
              end_date = end_date)

  inic[[1]] %>%
    kable(caption = "Causas Iniciadas Agrupadas por Tipo de Mediación",
          align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                  full_width = F, font_size = 10)  %>%
    row_spec(0, angle = 90)
```

<!-- ## Causas Iniciadas por Grupo de Procesos en materia Civil y Comercial - `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se muestran grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura. -->


<!-- ```{r } -->
<!--   # inic <- iniciados_cen(poblacion = cen, -->
<!--   #             start_date = start_date, -->
<!--   #             end_date = end_date) -->

<!--   inic[[2]] %>%  -->
<!--   kable(caption = "Causas Iniciadas Agrupadas por Procesos", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(1, "15cm") %>% -->
<!--   row_spec(0, angle = 90) %>%  -->
<!--   landscape() -->

<!-- ``` -->




