---
title: Sentencias por Tipo 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## Sentencias por Tipo `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_cco3_xtres(
  poblacion = sal_civil,
  start_date = start_date,
  end_date = end_date) %>%
  outputTable(caption = "Sentencias por Tipo") %>%
  row_spec(0, angle = 90)
```



