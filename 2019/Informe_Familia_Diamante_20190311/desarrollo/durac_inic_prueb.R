poblacion <- jdos_lab
start_date <- "2018-01-01"
end_date <- "2019-01-01"

library(dplyr)
resultado <- DB_PROD() %>% 
  apgyeTableData("AUDIL") %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep == "jdolab0100pna") %>%
  filter(!(is.na(nro) & is.na(caratula) & is.na(tproc))) %>%
  mutate(fea = dmy(fea)) %>% 
  # mutate(audvid = as.integer(audvid)) %>% no tiene campo de videofilmacion
  filter(fea > data_interval_start, fea < data_interval_end) %>%
  select(-data_interval_start, -data_interval_end, -justiables) %>%
  collect() 

#resultado <- resolverconvertidos(resultado)
resultado <- resultado %>%
  mutate(duracm = str_remove_all(duracm, "[:alpha:]|[:punct:]")) %>%
  mutate(duracm = str_trim(duracm)) %>%
  mutate(ra = str_sub(toupper(ra), 1,1)) %>%
  mutate(ta = str_sub(toupper(ta), 1,1)) %>%
  mutate(ta = str_trim(ta)) %>% 
  mutate(esta = str_remove_all(esta, "[:alpha:]|[:punct:]")) %>%
  mutate(esta = str_trim(esta)) %>% 
  codconver("AUDIL", "esta") %>% 
  codconver("AUDIL", "ra") %>% 
  codconver("AUDIL", "ta") 

df <- resultado
audilab_realizadas_xresultado(df, desagregacion_mensual = T) %>% View()
audilab_realizadas_xtipo(df, T) %>% View()

audilab_prim(poblacion, "2018-01-01", "2019-01-01") %>%
  audilab_realizadas_xtipo(desagregacion_mensual = T) %>% View()


audilab_realizadas_xtipo <- function(df, desagregacion_mensual = TRUE) {
  
  df <- df %>% 
    filter(esta == "realizada")
    
    if (desagregacion_mensual) {
      
      cantidad_audiencias <<- df %>%
        mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>%
        group_by(organismo, mes, ta) %>% 
        summarise(cantidad = n()) %>%
        tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
        ungroup() %>% 
        left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                  "circunscripcion")],
                  by = "organismo") %>%
        select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) %>% 
        group_by(circunscripcion, organismo_descripcion) %>% 
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>% 
        janitor::adorn_totals("col") 
      
      minutos_audiencias <<- df %>%
        mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>%
        group_by(organismo, mes, ta) %>%
        summarise(minutos = sum(as.integer(duracm), na.rm = T)) %>%
        tidyr::spread(ta, minutos, drop = F, fill = NA) %>%
        ungroup() %>%
        left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                  "circunscripcion")],
                  by = "organismo") %>%
        select(circunscripcion, organismo_descripcion, mes, everything(), matches("sd"), -organismo) %>%
        group_by(circunscripcion, organismo_descripcion) %>%
        do(janitor::adorn_totals(.)) %>%
        ungroup() %>%
        janitor::adorn_totals("col")
      
    } else {
      
      cantidad_audiencias <<- df %>%
        mutate(organismo = iep)  %>%
        group_by(organismo, ta) %>% 
        summarise(cantidad = n()) %>%
        tidyr::spread(ta, cantidad, drop = F, fill = NA) %>% 
        ungroup() %>% 
        left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                  "circunscripcion")],
                  by = "organismo") %>%
        select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        janitor::adorn_totals("row") %>%
        janitor::adorn_totals("col") 
      
      minutos_audiencias <<- df %>%
        mutate(organismo = iep)  %>%
        group_by(organismo, ta) %>%
        summarise(minutos = sum(as.integer(duracm), na.rm = T)) %>%
        tidyr::spread(ta, minutos, drop = F, fill = NA) %>%
        ungroup() %>%
        left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                  "circunscripcion")],
                  by = "organismo") %>%
        select(circunscripcion, organismo_descripcion, everything(), matches("sd"), -organismo) %>%
        arrange(circunscripcion, organismo_descripcion) %>%
      janitor::adorn_totals("row") %>%
      janitor::adorn_totals("col")


      
    }
  
  minutos_audiencias <<- minutos_audiencias
  cantidad_audiencias <<- cantidad_audiencias
  
}


start_date <- "2018-10-01"
end_date <- "2019-01-01"

ultimo_ano <- apgyeRStudio::primariasOrganismo("jdocco*", start_date, end_date)$AUDIC$tabla() %>%
  mutate(fea = dmy(fea), finicio = dmy(finicio), faprueb = dmy(faprueb)) %>% 
  filter(fea >= data_interval_start, fea < data_interval_end) %>% 
  filter(esta == "2", ta == "1") %>%
  collect() %>% ungroup() %>% 
  filter(faprueb > finicio) %>% 
  distinct(iep, nro, caratula, tproc, fea, .keep_all = T) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>%
  mutate(tproc = ifelse(str_detect(tproc, "ORDINARIO CIVIL"), "ORDINARIO", tproc)) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "SUMARISIMO CIVIL"), "SUMARISIMO", tproc)) %>% 
  select(iep, nro, caratula, tproc, fecha_inicio_causa = finicio, fecha_apertura_prueba = faprueb) %>% 
  mutate(dias_inicio_aperturaprueba = fecha_apertura_prueba - fecha_inicio_causa) %>% 
  group_by(tproc) %>% 
  summarise(cantidad_casos = n(), 
            promedio_inicio_aperprueba = round(mean(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
            mediana_inicio_aperprueba = round(median(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
            casos_durac_minima = min(dias_inicio_aperturaprueba),
            casos_durac_maxima = max(dias_inicio_aperturaprueba)) %>% 
  arrange(desc(promedio_inicio_aperprueba)) %>% 
  filter(cantidad_casos > 10) %>% 
  mutate(ano_ref = "año_2018")

historico <- apgyeRStudio::primariasOrganismo("jdocco*", "2017-01-01", "2018-01-01")$CADUR1C$tabla() %>%
  filter(!is.na(fapr)) %>% 
  mutate(finicio = dmy(finicio), fapr = dmy(fapr)) %>% 
  collect() %>% ungroup() %>% 
  filter(fapr > finicio) %>% 
  filter(fapr > as.Date("2017-01-01") & fapr < as.Date("2017-12-31")) %>% 
  mutate(faprueb = fapr) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>%
  mutate(tproc = ifelse(str_detect(tproc, "ORDINARIO CIVIL"), "ORDINARIO", tproc)) %>% 
  mutate(tproc = ifelse(str_detect(tproc, "SUMARISIMO CIVIL"), "SUMARISIMO", tproc)) %>% 
  select(iep, nro, caratula, tproc, fecha_inicio_causa = finicio, fecha_apertura_prueba = faprueb) %>% 
  mutate(dias_inicio_aperturaprueba = fecha_apertura_prueba - fecha_inicio_causa) %>% 
  group_by(tproc) %>% 
  summarise(cantidad_casos = n(), 
            promedio_inicio_aperprueba = round(mean(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
            mediana_inicio_aperprueba = round(median(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
            casos_durac_minima = min(dias_inicio_aperturaprueba),
            casos_durac_maxima = max(dias_inicio_aperturaprueba)) %>% 
  arrange(desc(promedio_inicio_aperprueba)) %>% 
  filter(cantidad_casos > 10) %>% 
  mutate(ano_ref = "año_2017")

tabla_durac__intera_inic_aperprueb <- ultimo_ano %>% 
  bind_rows(historico)
  
comparativo <- ultimo_ano %>% 
  bind_rows(historico) %>% 
  select(tproc, ano_ref, mediana_inicio_aperprueba) %>% 
  tidyr::spread(ano_ref, mediana_inicio_aperprueba) %>% 
  na.omit()

    
    
