---
title: Causas Iniciadas ordenadas por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")

if (nrow(jdos_lab) != 0) { 
  laboral <- TRUE 
  } else {
  laboral <- FALSE
  }

```


## Causas Iniciadas por Grupo de Procesos - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura.


```{r conditional_block_lab1_cigpo, eval = laboral, echo=FALSE }
iniciados_lab(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date) %>%
  iniciados_lab_comparativo() %>%
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "5cm") %>%
  row_spec(0, angle = 90) %>%
  landscape()

```



