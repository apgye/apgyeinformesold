---
title: Legajos y Movimientos  
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## Movimientos en Legajos de OMA  `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se muestra la cantidad de movimientos en cada Circunscripción y órgano (función).    

Para un cálculo correcto de este indicador los operadores deben trabajar con los modelos de escrito definidos por la Dirección de OMA que se explicitan en la Guía de Estadística (pto. *3) Denominación de los Modelos Genéricos de Escritos de OMA*). Dicha clasificación está encabezada por un código numérico que sirve de referencia para el cómputo, sin el cual no se cuenta ningún movimiento.   

```{r, size= 'tiny'}
# LA TABLA DE MOVIMIENTOS POR TIPO SÓLO DEBE MOSTRAR LOS EQUIPOS DE PROBATION 
# (dato suministrado personalmente por Amateis el 08/10/2019 12 hs.)
movimientos_oma(oma, 
         start_date = start_date,
         end_date = end_date) %>% 
  kable(caption = "Movimientos por Tipo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>%
  landscape()

```



