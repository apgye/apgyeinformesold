entramite <- function(poblacion, operacion_inic) {
  
  primer_dia_ano_antes <- paste0(lubridate::year(end_date) - 1, '-01-01')
  primer_dia_ano_actual <- paste0(lubridate::year(end_date), '-01-01')
  
  hace_10_años = as.Date(end_date) - lubridate::years(10)
  hace_5_años = as.Date(end_date) - lubridate::years(5)
  hace_2_años = as.Date(end_date) - lubridate::years(2)
  
  
  resultado <- DB_PROD() %>% 
    # en trámite al 31/12 año antes a end_date
    apgyeDSL::apgyeTableData("CETAL_XL") %>% 
    apgyeDSL::interval(primer_dia_ano_antes, primer_dia_ano_actual) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    filter(!is.na(finicio), !is.na(fmov)) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
    #filter(fmov < !!Sys.Date()) %>% excluido el 27-02-19: no podría excluirse un registro con error informado en una variable.
    group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
    filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
    ungroup()  %>%
    select(iep, nro, caratula, tproc, finicio, fmov)  %>% 
    # sumo lo iniciado desde 1/1 año actual hasta end_date
    union(
      DB_PROD() %>% 
        apgyeDSL::apgyeTableData(!! operacion_inic) %>% 
        filter(iep %in% poblacion$organismo) %>% 
        apgyeDSL::interval(primer_dia_ano_actual, end_date ) %>%
        filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
        mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
        filter(!is.na(finicio)) %>% 
        filter(finicio >= primer_dia_ano_actual, finicio < end_date ) %>% 
        select(iep, nro, caratula, tproc, finicio, fmov) 
    ) %>% 
    collect() %>% 
    group_by(iep, nro) %>% filter(row_number() == n()) %>%  ungroup() %>%   # quitando repetidos
    mutate(año_ult_mov_procesal = lubridate::year(fmov)) %>% 
    mutate(antiguedad_ult_mov_procesal = case_when(
      fmov >= hace_2_años ~ "menor_2_años",  
      fmov < hace_2_años & fmov > hace_10_años ~ "entre_2y10_años",
      fmov <= hace_10_años ~ "mayor_10_años", 
      TRUE ~ "sin_dato_sobre_movimiento"))
  
  resultado <- resolverconvertidos(resultado)
  
  
  # resto los archivados desde 1/1 año actual hasta end_date
  
  archivados <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData("CARCH") %>%
    filter(iep %in% poblacion$organismo) %>% 
    apgyeDSL::interval(primer_dia_ano_actual, end_date ) %>%
    select(iep, nro, caratula, tproc, finicio, fmov) %>% 
    collect()
  
  if (nrow(archivados) != 0) {
    resultado  <- resultado %>% 
      anti_join(archivados, by = c( "iep", "nro"))
  } else {
    resultado <- resultado
    
  }
  
  resultado <- resultado %>% 
    rename(organismo = iep) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], by = "organismo") %>% 
    select(-organismo) %>% 
    mutate(caratula = str_sub(caratula, 1, 20), 
           tproc = str_sub(tproc, 1, 20)) %>% 
    select(circunscripcion, organismo = organismo_descripcion, antiguedad_ult_mov_procesal, 
           año_ult_mov_procesal, fecha_ult_mov_proc = fmov,
           nro, caratula, tproc, fecha_inicio_causa = finicio) %>% 
    mutate(antiguedad_ult_mov_procesal = factor(antiguedad_ult_mov_procesal, 
                                                levels = c("mayor_10_años",
                                                  "entre_2y10_años",
                                                  "menor_2_años",
                                                  "sin_dato_sobre_movimiento"),
                                  ordered = TRUE)) %>% 
    arrange(circunscripcion, organismo, antiguedad_ult_mov_procesal, año_ult_mov_procesal)
  
  resultado
    
}

enTramite_gral <- function(causasEnTramite) {
  causasEnTramite <- causasEnTramite %>%
    group_by(circunscripcion, organismo) %>% 
    summarise(causas_en_tramite_totales = n(), 
              causas_en_tramite_ultimos_2años = sum(antiguedad_ult_mov_procesal ==
                                                                "menor_2_años", na.rm = T)) %>% 
        select(circunscripcion, organismo, everything()) %>% 
    arrange(circunscripcion, organismo)
  
  causasEnTramite
  
}

enTramitePorGrupoyAño <- function(causasEnTramite) {
  causasEnTramite %>%
    select(circunscripcion, organismo, antiguedad_ult_mov_procesal, nro) %>% 
    group_by(circunscripcion, organismo, antiguedad_ult_mov_procesal) %>% 
    summarise(cantidad = n()) %>% 
    tidyr::spread(key=antiguedad_ult_mov_procesal, value=cantidad) %>% 
    janitor::adorn_totals("col") %>%
    janitor::adorn_totals("row") %>% 
    ungroup() 
}


# en trámite por procesos

entramite_xproc <- function(poblacion, operacion_inic, tipos_proc) {
  
  primer_dia_ano_antes <- paste0(lubridate::year(end_date) - 1, '-01-01')
  primer_dia_ano_actual <- paste0(lubridate::year(end_date), '-01-01')
  
  hace_10_años = as.Date(end_date) - lubridate::years(10)
  hace_5_años = as.Date(end_date) - lubridate::years(5)
  hace_2_años = as.Date(end_date) - lubridate::years(2)
  
  
  resultado <- DB_PROD() %>% 
    # en trámite al 31/12 año antes a end_date
    apgyeDSL::apgyeTableData("CETAL_XL") %>% 
    apgyeDSL::interval(primer_dia_ano_antes, primer_dia_ano_actual) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    filter(!is.na(finicio), !is.na(fmov)) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
    #filter(fmov < !!Sys.Date()) %>% excluido el 27-02-19: no podría excluirse un registro con error informado en una variable.
    group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
    filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
    ungroup()  %>%
    select(iep, nro, caratula, tproc, finicio, fmov)  %>% 
    # sumo lo iniciado desde 1/1 año actual hasta end_date
    union(
      DB_PROD() %>% 
        apgyeDSL::apgyeTableData(!! operacion_inic) %>% 
        filter(iep %in% poblacion$organismo) %>% 
        apgyeDSL::interval(primer_dia_ano_actual, end_date ) %>%
        filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
        mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
        filter(!is.na(finicio)) %>% 
        filter(finicio >= primer_dia_ano_actual, finicio < end_date ) %>% 
        select(iep, nro, caratula, tproc, finicio, fmov) 
    ) %>% 
    collect() %>% 
    #filter(iep != "jdocco0502con") %>% # Anulo presentación duplicada
    group_by(iep, nro) %>% filter(row_number() == n()) %>%  ungroup() %>%   # quitando repetidos
    filter(str_detect(tproc, !!tipos_proc)) %>% 
    mutate(año_inicio = lubridate::year(finicio)) %>% 
    mutate(año_inicio = case_when(
      año_inicio == 2017 ~ "iniciado_2017",  
      año_inicio == 2018 ~ "iniciado_2018",
      año_inicio == 2016 ~ "iniciado_2016", 
      TRUE ~ "iniciado_2015_o_antes"))
  
  resultado <- resolverconvertidos(resultado)
  
  
  # resto los archivados desde 1/1 año actual hasta end_date
  
  archivados <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData("CARCH") %>%
    filter(iep %in% poblacion$organismo) %>% 
    apgyeDSL::interval(primer_dia_ano_actual, end_date ) %>%
    select(iep, nro, caratula, tproc, finicio, fmov) %>% 
    collect()
  
  if (nrow(archivados) != 0) {
    resultado  <- resultado %>% 
      anti_join(archivados, by = c( "iep", "nro"))
  } else {
    resultado <- resultado
    
  }
  
  resultado <- resultado %>% 
    rename(organismo = iep) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], by = "organismo") %>% 
    select(-organismo) %>% 
    mutate(caratula = str_sub(caratula, 1, 20), 
           tproc = str_sub(tproc, 1, 20)) %>% 
    select(circunscripcion, organismo = organismo_descripcion, año_inicio, nro, tproc) %>% 
    mutate(año_inicio = factor(año_inicio, levels = c("iniciado_2015_o_antes", 
                                                      "iniciado_2016",
                                                      "iniciado_2017",
                                                      "iniciado_2018"),
                               ordered = TRUE)) %>% 
    group_by(circunscripcion, organismo, año_inicio) %>% 
    summarise(cantidad_causas_en_tramite = n()) 
  
  
  resultado
  
}




