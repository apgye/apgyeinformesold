
duracion <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE, 
                     desagregacion_xorg = T) {
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    resolverconvertidos()
  
  
  # Separa Primera y Segunda Instancia
  
  if (str_detect(poblacion$organismo[[1]], "jdo")) {
    
    # Primera Instancia
    
    if(desagregacion_mensual) {
      resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      resultado <- resultado %>% group_by(iep) 
    }
    
    resultado <- resultado %>% 
      process() %>%
      .$result %>%
      ungroup() 
    
    
    if(desagregacion_mensual) {
      
      resultado <- resultado %>% 
        mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
        select(organismo = iep, mes, durac_inic_sent_xproc) %>% 
        tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
        tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion"), sep="&") %>%
        mutate(tipo_proceso = str_replace(tipo_proceso, "[[:punct:]]", "")) %>% 
        mutate(tipo_proceso = str_replace(tipo_proceso, '"', "")) %>%
        mutate(tipo_proceso = str_trim(tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "ORDINARIO CIVIL"), "ORDINARIO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "SUMARISIMO CIVIL"), "SUMARISIMO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DESALOJO Y COBRO DE ALQUIL"), "DESALOJO Y COBRO ALQUILERES", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DIVORCIO"), "DIVORCIO", tipo_proceso)) 
      
      
      if(desagregacion_xorg){
        resultado <- resultado %>% 
          group_by(organismo, mes, tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(organismo, mes, desc(mediana_duracion)) %>% 
          ungroup() %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        resultado
      } else {
        resultado <- resultado %>% 
          group_by(mes, tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(mes, desc(mediana_duracion)) %>% 
          ungroup() 
        resultado
      }
      resultado
      
    } else {
      
      resultado <- resultado %>% 
        select(organismo = iep, durac_inic_sent_xproc) %>% 
        tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
        tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion"), sep="&") %>% 
        mutate(tipo_proceso = str_replace(tipo_proceso, "[[:punct:]]", "")) %>% 
        mutate(tipo_proceso = str_replace(tipo_proceso, '"', "")) %>%
        mutate(tipo_proceso = str_trim(tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "ORDINARIO CIVIL"), "ORDINARIO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "SUMARISIMO CIVIL"), "SUMARISIMO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DESALOJO Y COBRO DE ALQUIL"), "DESALOJO Y COBRO ALQUILERES", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DIVORCIO"), "DIVORCIO", tipo_proceso)) 
      
        
      if(desagregacion_xorg) {
        resultado <- resultado %>% 
          group_by(organismo, tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(organismo, desc(mediana_duracion)) %>% 
          ungroup() %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        resultado
        
      } else {
        resultado <- resultado %>% 
          group_by(tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(desc(mediana_duracion)) 
        resultado
      }
      resultado
    }
    
  } else {
    
    # Segunda Instancia
    
    if(desagregacion_mensual) {
      resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      resultado <- resultado %>% group_by(iep) 
    }
    
    resultado <- resultado %>% 
      filter(as %in% c("S", "s")) %>% 
      filter(!is.na(fres)) %>% filter(tres != "0") %>%
      mutate(fres = dmy(fres)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      ungroup()  %>% 
      mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
      mutate(tproc = str_replace(tproc, "[[:punct:]]", "")) %>% 
      mutate(tproc = str_trim(tproc))  
      
    
    if(desagregacion_mensual) {
      resultado <- resultado %>% 
        mutate(mes = lubridate::month(fres, label=T, abbr = F)) %>% 
        filter(!str_detect(tproc, "VIOLENCIA|AMPARO"), !is.na(tproc)) %>% 
        filter(fres <= Sys.Date()) %>%
        select(organismo = iep, mes, nro, caratula, tproc, 
               fecha_inicio_camara = finicio,
               fecha_resolucion = fres) %>%
        mutate(duracion = fecha_resolucion - fecha_inicio_camara)  
      # mutate(tproc = ifelse(tproc == "COBRO DE PESOS", 
      #                       "ORDINARIO COBRO DE PESOS", tproc), 
      #        tproc = ifelse(tproc == "ORDINARIO (CIVIL)", 
      #                       "ORDINARIO", tproc)) %>% 
      if(desagregacion_xorg){
        resultado <- resultado %>% 
          group_by(organismo, mes, tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        
        resultado
      } else {
        resultado <- resultado %>%
          group_by(mes, tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) 
        resultado
      }
      
    } else {
      
      resultado <- resultado %>% 
        filter(!str_detect(tproc, "VIOLENCIA|AMPARO"), !is.na(tproc)) %>% 
        filter(fres <= Sys.Date()) %>%
        select(organismo = iep, nro, caratula, tproc, 
               fecha_inicio_camara = finicio,
               fecha_resolucion = fres) %>%
        mutate(duracion = fecha_resolucion - fecha_inicio_camara) 
      
      if(desagregacion_xorg){
        resultado <- resultado %>%
          group_by(organismo, tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        
        resultado
        
      } else {
        resultado <- resultado %>%
          group_by(tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) 
        resultado
      }
      
      resultado
    }
    resultado
  }
  
  resultado <- resultado %>% 
    mutate(tipo_proceso = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tipo_proceso)))
   
  
  resultado
           
}


durac_subselec <- function(df, materia) {
  
  df <- df %>% 
    #filter(cantidad_casos > 3) %>% 
    rename(tproc = tipo_proceso)
  
  if (materia ==  "civil") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(!str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR |^MEDIDA"),
        !str_detect(tproc, "MONITORIO|^PREPARACION DE LA VIA MONIT"),
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INTERDICT"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "^HOMOLOGACION"), 
        !str_detect(tproc, "^SEGUNDO TESTIMONIO"),
        !str_detect(tproc, "^SUCESORIO"), # exlucido por variación en la codificación de resoluciones
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL"),
        # Otros Fueros Laboral y Familia
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"), 
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO")) 
    df
  } else if (materia == "familia") {
    df <- df %>%
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "VIOLENCIA FAMILIAR"), #
        !str_detect(tproc, "VIOLENCIA DE GENERO"), #
        !str_detect(tproc, "HOMOLOGACION"), 
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU"),
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA"),
        #!str_detect(tproc, "^RESTRICCIONES"),
        #!str_detect(tproc, "^INTERNACION"), #
        !str_detect(tproc, "^BENEFICIO"), #
        #!str_detect(tproc, "^REGIMEN"), #
        #!str_detect(tproc, "^AUTORIZACION JUDICIAL"),
        #!str_detect(tproc, "^DECLARACION DE INCAPACIDAD"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "^SU |^EXHORTO|^OFICIO|DESARCHIVO|^INFORME|^DENUNCIA|^TESTIMONIO"),
        !str_detect(tproc, "CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION|^AMENAZA"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL"),
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"))
    df
  } else if (materia == "laboral") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "^HOMOLOGACION"), #
        !str_detect(tproc, "^EJEC|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR "),
        !str_detect(tproc, "^MEDIDAS PREP"),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|MONITORI|PERSONAL"),
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO|^DESALOJO|^APELACION JUZGADO|^DIVISION DE|^SEGUNDO TEST"))
    df
  } else if(materia == "pazproc") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "), #
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR "), #
        !str_detect(tproc, "MONITORIO"), #
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INTERDICT"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        !str_detect(tproc, "^PREPARACION "),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "^HOMOLOGACION"), 
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA|^SUPERVIVENCIA|^AUTORIZACION"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|PAZ"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL|^LICENCIAS|^ARANCELES"),
        # Otros Fueros Laboral y Familia
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"), 
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^LESIONES|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO"))
    df
  }
  df
}


durac_interanual <- function(poblacion, operacion = "CADR1C", materia = "civil", start_date = "2017-01-01", end_date = "2018-01-01") {
  
  library(ggplot2)  
  library(reshape2)
  
  inicio_ano_base <- paste0(lubridate::year(end_date) - 2, '-01-01')
  fin_ano_base <- paste0(lubridate::year(end_date) - 1, '-01-01')
  
  inicio_ano_analizado <- fin_ano_base
  fin_ano_analizado <- end_date
  
  
  durac_ano_base <- duracion(poblacion, 
                        operacion = operacion, 
                        start_date = inicio_ano_base,
                        end_date = fin_ano_base,  
                        desagregacion_mensual = F, 
                        desagregacion_xorg = F) %>% 
    durac_subselec(materia) %>% 
    mutate(año = lubridate::year(inicio_ano_base))
  
  
  durac_ano_analizado <- duracion(poblacion, 
                             operacion = operacion, 
                             start_date = inicio_ano_analizado,
                             end_date = fin_ano_analizado,  
                             desagregacion_mensual = F, 
                             desagregacion_xorg = F) %>% 
    durac_subselec(materia) %>% 
    mutate(año = lubridate::year(inicio_ano_analizado))
  
  
  durac_ano_base <- durac_ano_base %>% 
    semi_join(durac_ano_analizado, by = "tproc")
  durac_ano_analizado <- durac_ano_analizado %>% 
    semi_join(durac_ano_base, by = "tproc")
  
  durac_interanual <- durac_ano_analizado %>% 
    bind_rows(durac_ano_base) %>% 
    select(tipo_proceso = tproc, año, everything())
  
  casos_insuficienstes <- durac_interanual$tipo_proceso[durac_interanual$cantidad_casos < 10]  
  
  durac_interanual <- durac_interanual %>% 
    filter(!tipo_proceso %in% casos_insuficienstes) 
  
  durac_interanual <<- durac_interanual
  
  durac_interanual_tabla <- durac_interanual %>% 
    select(-cantidad_casos) %>% 
    tidyr::spread(año, mediana_duracion) %>% 
    rowwise() %>% 
    rename(año_2017 = "2017", año_2018 = "2018") %>% 
    mutate(diferencia_interanual_absoluta =  año_2018 - año_2017) %>% 
    mutate(diferencia_interanual_relativa =  round(diferencia_interanual_absoluta/año_2017, digits = 2) * 100) %>% 
    arrange(diferencia_interanual_relativa) %>% 
    mutate(diferencia_interanual_relativa = paste0(diferencia_interanual_relativa, " %")) %>% 
    rename('variacion interanual absoluta' = diferencia_interanual_absoluta,
           'variacion interanual relativa' = diferencia_interanual_relativa)  
    
  
  
  durac_interanual_tabla <<- durac_interanual_tabla
  
  durac_interanual <- durac_interanual %>% 
    mutate(casos_disminucion = tipo_proceso %in% 
             durac_interanual_tabla$tipo_proceso[durac_interanual_tabla$'variacion interanual absoluta' < 0]) %>% 
    mutate(casos_disminucion = ifelse(casos_disminucion, "DISMINUYERON DURACION",
                                      "AUMENTARON DURACION"))
  
  grafico <- durac_interanual %>% 
    ggplot(aes(x = tipo_proceso, fill = as.factor(año), 
               y = ifelse(test = año == 2017, yes = -mediana_duracion, no = mediana_duracion))) +
    geom_bar(stat = "identity") +
    #geom_text(aes(label=mediana_duracion), vjust=0.5, color="#666666", size=3.5) +
    scale_y_continuous(limits = max(durac_interanual$mediana_duracion) * c(-1,1)) +
    scale_y_continuous(labels = abs)  +
    coord_flip() +
    scale_fill_manual(values = c("grey", "#66CC99"), name = "") +
    labs(title= expression(atop("Comparación Interanual de la Duración de los Procesos")),
         y = "Duración de procesos en dias corridos de inicio a sentencia (Mediana)", x = "") +
    theme(axis.text.x = element_text(size=10, color="black"),
          axis.text.y = element_text(size=10, color="black"),
          legend.text = element_text(size=14),
          axis.title = element_text(size=14),
          plot.title = element_text(size = 18, face = "bold", colour = "black", vjust = -1),
          legend.position ="top", 
          strip.text = element_text(size=14)) +
    coord_flip() +
    facet_grid(. ~ casos_disminucion) 
    
  
  grafico <<- grafico
  
}


durac_inic_apprueba_cco1 <- function() {
  
  ultimo_ano <- apgyeRStudio::primariasOrganismo("jdocco*", "2018-10-01", "2019-01-01")$AUDIC$tabla() %>%
    mutate(fea = dmy(fea), finicio = dmy(finicio), faprueb = dmy(faprueb)) %>% 
    filter(fea >= data_interval_start, fea < data_interval_end) %>% 
    filter(esta == "2", ta == "1") %>%
    collect() %>% ungroup() %>% 
    filter(faprueb > finicio) %>% 
    distinct(iep, nro, caratula, tproc, fea, .keep_all = T) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>%
    mutate(tproc = ifelse(str_detect(tproc, "ORDINARIO CIVIL"), "ORDINARIO", tproc)) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "SUMARISIMO CIVIL"), "SUMARISIMO", tproc)) %>% 
    select(iep, nro, caratula, tproc, fecha_inicio_causa = finicio, fecha_apertura_prueba = faprueb) %>% 
    mutate(dias_inicio_aperturaprueba = fecha_apertura_prueba - fecha_inicio_causa) %>% 
    group_by(tproc) %>% 
    summarise(cantidad_casos = n(), 
              promedio_inicio_aperprueba = round(mean(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              mediana_inicio_aperprueba = round(median(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              casos_durac_minima = min(dias_inicio_aperturaprueba),
              casos_durac_maxima = max(dias_inicio_aperturaprueba)) %>% 
    arrange(desc(promedio_inicio_aperprueba)) %>% 
    filter(cantidad_casos > 10) %>% 
    mutate(ano_ref = "año_2018")
  
  historico <- apgyeRStudio::primariasOrganismo("jdocco*", "2017-01-01", "2018-01-01")$CADUR1C$tabla() %>%
    filter(!is.na(fapr)) %>% 
    mutate(finicio = dmy(finicio), fapr = dmy(fapr)) %>% 
    collect() %>% ungroup() %>% 
    filter(fapr > finicio) %>% 
    filter(fapr > as.Date("2017-01-01") & fapr < as.Date("2017-12-31")) %>% 
    mutate(faprueb = fapr) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>%
    mutate(tproc = ifelse(str_detect(tproc, "ORDINARIO CIVIL"), "ORDINARIO", tproc)) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "SUMARISIMO CIVIL"), "SUMARISIMO", tproc)) %>% 
    select(iep, nro, caratula, tproc, fecha_inicio_causa = finicio, fecha_apertura_prueba = faprueb) %>% 
    mutate(dias_inicio_aperturaprueba = fecha_apertura_prueba - fecha_inicio_causa) %>% 
    group_by(tproc) %>% 
    summarise(cantidad_casos = n(), 
              promedio_inicio_aperprueba = round(mean(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              mediana_inicio_aperprueba = round(median(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              casos_durac_minima = min(dias_inicio_aperturaprueba),
              casos_durac_maxima = max(dias_inicio_aperturaprueba)) %>% 
    arrange(desc(promedio_inicio_aperprueba)) %>% 
    filter(cantidad_casos > 10) %>% 
    mutate(ano_ref = "año_2017")
  
  tabla_durac__intera_inic_aperprueb <<- ultimo_ano %>% 
    bind_rows(historico)
  
  comparativo <<- ultimo_ano %>% 
    bind_rows(historico) %>% 
    select(tproc, ano_ref, mediana_inicio_aperprueba) %>% 
    tidyr::spread(ano_ref, mediana_inicio_aperprueba) %>% 
    na.omit()
  
}