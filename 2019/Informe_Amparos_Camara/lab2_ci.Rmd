---
title: Causas Iniciadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


<!-- ## Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r } -->
<!-- iniciados_cam(poblacion = cam_lab,  -->
<!--               start_date = start_date, -->
<!--               end_date = end_date) %>% -->
<!--   outputTable(caption = "Causas Iniciadas") %>% -->
<!--   column_spec(3, width = "8cm") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->


<!-- ``` -->


## Procesos Constitucionales Ingresados a Despacho en Segunda Instancia - `r getDataIntervalStr(start_date, end_date)`

En la siguiente tabla se presentan los Procesos Constitucionales ingresados a despacho para resolución en órganos de Segunda Instancia de la Provincia. Esta información surge de los listados de causas a despacho mensuales presentadas por cada organismo y está a disposición para consulta. 


```{r }
amparos_2(poblacion = cam_lab, 
          operacion = "CADR2L",
          start_date = start_date,
          end_date = end_date) %>%
  outputTable(caption = "Procesos Constitucionales Iniciados")
 

```

