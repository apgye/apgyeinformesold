---
title: Audiencias Realizadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

## Audiencias Realizadas y sus Duraciones - `r getDataIntervalStr(start_date, end_date)`

```{r}
audienciasPenales(poblacion = jdo_pna, 
             start_date = start_date, 
             end_date = end_date, 
             desagregacion_mensual = desagregacion_mensual) %>% 
  outputTable(caption = "Audiencias Realizadas y Duración") %>%
  landscape()
```

## Audiencias realizadas por Tipo según artículo CPP - `r getDataIntervalStr(start_date, end_date)`

```{r}
realizadas_xtipo %>% 
  outputTable(caption = "Audiencias Realizadas por Tipo según artículo CPP") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```
=======
