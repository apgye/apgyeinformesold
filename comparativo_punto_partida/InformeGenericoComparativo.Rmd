---
title: "Informe <Generico> Comparativo"
subtitle: informe preliminar
author: "Área Planificación, Gestión y Estadística"
date: "format(Sys.Date(), '%d de %B de %Y')"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```


```{r echo=FALSE, include=FALSE}
# Parámetros del informe
desagregacion_mensual = F
start_date <- "2018-01-01" 
end_date <- "2018-07-01" 
```

```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam"))

jdos_cco <- poblacion_total %>% 
  filter(grepl("cco", organismo)) %>% 
  filter(str_detect(materia, "cco"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_paz <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo != "cam")

cam_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "cam")

cam_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "cam")

```




## Juzgados Civil y Comercial

La información procesada para este informe surge de listados primarios de datos presentados por cada organismo de manera digital. Dichos listados primarios permanecen inalterados para consulta a tavés del módulo *Presentaciones*. 

[//]: # (Causas iniciadas, para quitar knit_child -> #knit_child)
`r knit_child('causas_iniciadas.Rmd', envir=newenv(poblacion=jdos_cco, separar_pconocimiento=TRUE))`


[//]: # (Causas en trámite, para quitar knit_child -> #knit_child)
`r knit_child('causas_entramite.Rmd', envir=newenv(poblacion=jdos_cco))`


[//]: # (Causas Resueltas, para quitar knit_child -> #knit_child)
`r knit_child('causas_resueltas.Rmd', envir=newenv(poblacion=jdos_cco))`




\blandscape
### Audiencias Realizadas y Conciliadas

En la siguiente tabla se muestran las Audiencias Preliminares informadas por cada organismo en el período considerado.

Se calcula la *porcentaje de realización* como el cociente entre las audiencias realizadas y el total de realizadas y no realizadas expresado en valores porcentuales, y la *porcentaje de conciliación* como cociente entre audiencias conciliadas y el total de conciliadas y sin conciliación, expresado en valores porcentuales.

```{r}
audienciasCiviles(
  poblacion = jdos_cco,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Audiencias y Porcentajes")
```

\elandscape

## Juzgados Familia

\blandscape
### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados(
  poblacion = jdos_fam,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    top_n_iniciados(desagregacion_mensual = desagregacion_mensual) %>%
      outputTable(caption = "Causas Iniciadas") %>%
      column_spec(2, width = "5cm") %>%
      row_spec(0, angle = 90)
```
\elandscape

### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones(
  poblacion = jdos_fam,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Resoluciones y Vencimientos") %>%
    row_spec(0, angle = 90)
```

### Audiencias resumen provincial - `r getDataIntervalStr(start_date, end_date)`
```{r}
audifamDF <- audifam(
  poblacion = jdos_fam,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual)

audifamDF %>%
  audifam_resumen() %>%
  outputTable(caption = "Audiencias resumen provincial")

```

### Audiencias por tipo - `r getDataIntervalStr(start_date, end_date)`
```{r}
audifamDF %>%
    outputTable(caption = "Audiencias por tipo") %>%
    column_spec(column = 2, width = "3cm") %>%
    column_spec(column = 4, width = "7cm")
```

\newpage

## Juzgados Paz 1 Categoría

\blandscape
### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados(
  poblacion = jdos_paz,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    top_n_iniciados(desagregacion_mensual = desagregacion_mensual) %>%
      outputTable(caption = "Causas Iniciadas") %>%
      column_spec(2, width = "5cm") %>%
      row_spec(0, angle = 90)
```
\elandscape

### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones(
  poblacion = jdos_paz,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Resoluciones y Vencimientos") %>%
    row_spec(0, angle = 90)
```

\newpage

## Juzgados Laborales 

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
    top_n_iniciados(desagregacion_mensual = desagregacion_mensual) %>%
      outputTable(caption = "Causas Iniciadas") %>%
      column_spec(2, width = "5cm") %>%
      row_spec(0, angle = 90)
```


### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date,
  operacion = "CADR1L",
  filtroMateria = "L",
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Resoluciones y Vencimientos") %>%
    row_spec(0, angle = 90)
```

\newpage

## Segunda Instancia Civil y Comercial

### Causas Iniciadas - 1er semestre 2018

```{r}
iniciadosCITIPJ_1er_semestre(
  poblacion = cam_civil) %>%
      outputTable(caption = "Causas Iniciadas 1er semestre 2018") %>%
      column_spec(2, width = "2cm") %>%
      column_spec(4, width = "5cm") %>%
      row_spec(0, angle = 90)
```


### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones(
  poblacion = cam_civil,
  start_date = start_date,
  end_date = end_date,
  operacion = "CADR2C",
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Resoluciones y Vencimientos") %>%
    row_spec(0, angle = 90)
```

\newpage

## Segunda Instancia Laboral

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciadosCITIPJ_1er_semestre(
  poblacion = cam_lab) %>%
      outputTable(caption = "Causas Iniciadas 1er semestre 2018") %>%
      column_spec(2, width = "2cm") %>%
      column_spec(4, width = "5cm") %>%
      row_spec(0, angle = 90)
```


### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones(
  poblacion = cam_lab,
  start_date = start_date,
  end_date = end_date,
  operacion = "CADR2L",
  desagregacion_mensual = desagregacion_mensual) %>%
    outputTable(caption = "Resoluciones y Vencimientos") %>%
    row_spec(0, angle = 90)
```


