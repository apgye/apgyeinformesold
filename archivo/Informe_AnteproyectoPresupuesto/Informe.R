# Presupuesto 2019
library(dplyr)
library(stringr)

entidades <- apgyeJusEROrganization::listar_organismos()

seg_inst <- entidades %>% 
  filter(str_detect(tipo, "cam|tja")) %>% 
  filter(!str_detect(circunscripcion, "Paz|Nogoy")) %>% 
  group_by(circunscripcion, fuero, organismo_descripcion) %>% 
  summarise(cantidad = n())

primera_instancia <- entidades %>% 
  filter(str_detect(tipo, "jdo|oga")) %>% 
  group_by(circunscripcion, fuero, materia) %>% 
  summarise(cantidad = n())

mediacion <- entidades %>% 
  filter(str_detect(tipo, "cen|oma")) %>% 
  filter(circunscripcion == "Paraná") %>% 
  group_by(circunscripcion, organismo_descripcion, materia) %>% 
  summarise(cantidad = n())
  
  