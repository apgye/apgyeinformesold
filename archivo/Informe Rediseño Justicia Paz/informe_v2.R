library(dplyr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)


entidades <- apgyeJusEROrganization::listar_organismos()

start_date <- "2018-02-01"
end_date <- "2018-07-01"


# Listado de Magistrados y Funcionarios
magistrados_y_secretarios <- DB_PROD() %>% apgyeDSL::apgyeTableData('personal_planta_ocupada') %>%
  left_join(DB_PROD() %>% apgyeDSL::apgyeTableData('personal_personas') %>%
              select(idagente, categoria, apellido, nombres), by=c("idagente")
  ) %>%
  filter(grepl("JUEZ|SECRETARIO|VOCAL|FISCAL|DEFENSOR", categoria)) %>%
  collect()
magist_func_id_agentes <- magistrados_y_secretarios$idagente
# add "0" ti IDs for External Agent
magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"



jdos_paz1 <- entidades %>% 
  filter(tipo == "jdo", materia== "paz", categoria == "1")

jdos_paz23 <- entidades %>% 
  filter(tipo == "jdo", materia== "paz", categoria %in% c("2", "3"))



############################## Primera Categoría###############################

citipj <- apgyeRStudio::primariasOrganismo("jdopaz*",
                                           "2018-01-01", "2018-07-02")$CITIPJ$tabla() %>% collect()

inic_proc_1sem <- citipj %>% 
  mutate(nivel = stringr::str_count(`Tipo de proceso`, "     ")) %>%
  tidyr::separate( `Tipo de proceso`, into = c("mat", "proc"),
                   sep = "     "  )  %>%
  mutate(mat = if_else(mat=="", NA_character_ ,mat)) %>%
  tidyr::fill(mat) %>% filter(nivel == 1) %>%
  mutate(cantidad = as.integer(proc)) %>% 
  group_by(iep, mat, proc) %>% 
  summarise(cant_procesos = sum(as.integer(Procesos), na.rm = T)) %>%
  ungroup() %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                        "circunscripcion")], by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, organismo = iep, everything()) 


# Discriminación Procesos y Trámites
tproc <- apgyeOperationsJusER::Tipo_procesos
tramites <- tproc$tipo_de_procesos[tproc$Tipo_tramites_turnovoluntario == 1]
inic_proc_1sem <- inic_proc_1sem %>% 
  mutate(procesos = ! proc %in% tramites)


# Procesos Iniciados por Organismo
inic_xorg <- inic_proc_1sem %>% 
  mutate(desclasific = str_detect(mat, "QUIEBRAS|INSTRUCCION|LABORAL|ADMINISTRATIVO")) %>% 
  group_by(organismo) %>% 
  summarise(Procesos = sum(cant_procesos[procesos == TRUE & desclasific == FALSE]), 
            Tramites = sum(cant_procesos[procesos == FALSE]), 
            Desclasificados = sum(cant_procesos[desclasific == TRUE])) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], by = "organismo") %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, organismo, everything(), 
         -Tramites) %>% 
  arrange(desc(Procesos)) %>% 
  janitor::adorn_totals("col")%>% 
  janitor::adorn_totals("row")


# Tramites
## Tramites Parana en el Turno Voluntario
titTV <- apgyeRStudio::primariasOrganismo("turvol*",
                                          "2018-02-01", "2018-07-02")$TIT$tabla() %>% collect()
turvol_pna <- titTV %>% 
  summarise(cantidad = n()) %>% 
  mutate(circunscripcion = "Paraná", 
         organismo_descripcion = "Turno Voluntario Parana",
         organismo = "turvol0000pna") %>% 
  select(circunscripcion, organismo_descripcion, organismo, Tramites = cantidad)

tramites_xorg <- inic_proc_1sem %>% 
  mutate(desclasific = str_detect(mat, "QUIEBRAS|INSTRUCCION|LABORAL|ADMINISTRATIVO")) %>% 
  group_by(organismo) %>% 
  summarise(Procesos = sum(cant_procesos[procesos == TRUE & desclasific == FALSE]), 
            Tramites = sum(cant_procesos[procesos == FALSE]), 
            Desclasificados = sum(cant_procesos[desclasific == TRUE])) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], by = "organismo") %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, organismo, everything(), 
         -Procesos, -Desclasificados) %>% 
  bind_rows(turvol_pna) %>% 
  arrange(desc(Tramites)) %>% 
  janitor::adorn_totals("row")


cadr1c <- apgyeRStudio::primariasOrganismo("jdopaz*", "2018-02-01","2018-09-01")$CADR1C$tabla() %>%
  group_by(iep, data_interval_start, data_interval_end) %>%
  process() %>% .$result %>%
  ungroup() %>% 
  mutate(mes = months(data_interval_start)) %>% 
  mutate(mes = factor(mes, levels = c("febrero", "marzo", "abril", "mayo", 
                                      "junio", "julio", "agosto", "septiembre", 
                                      "octubre", "noviembre", "diciembre"),
                      ordered = TRUE)) %>% 
  select(organismo = iep, mes, causas_adespacho, causas_resueltas, 
         a_termino = res_atermino, luego_1erVencim = res_luego1venc, 
         luego_2doVencim = res_luego2venc, sentencias = res_xsentencia, 
         autos = res_xauto, sin_clasif =  res_xindeter) %>% 
  rowwise() %>% 
  mutate(resol_vencidas = str_c(round(sum(luego_1erVencim, luego_2doVencim, 
                                          na.rm = T)/causas_resueltas*100, 
                                      digits = 1), "%")) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = "organismo") %>% 
  select(circunscripcion, organismo_descripcion, organismo, everything()) %>% 
  arrange(organismo, mes) %>% 
  janitor::adorn_totals("row")



################################ Segunda Categoría ############################

presentaciones_justat <- DB_PROD() %>% tbl("submission") %>% select(-input) %>% collect()

citipp23_base_computo <- presentaciones_justat %>% 
  filter(data_interval_start >= as.Date(start_date),
         data_interval_end <= as.Date(end_date),
         iep %in% jdos_paz23$organismo,
         str_detect(operation, "CITIPP23"), enabled == T) %>% 
  group_by(iep, operation) %>% 
  summarise(presentaciones = n(),
            sin_dato = sum(str_detect(json_input, "true"))) %>% 
  left_join(apgyeJusEROrganization::OPERATION_DESCRIPTION, 
            by = c("operation" = "operacion")) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, iep, organismo_descripcion, operacion_descripcion, presentaciones, sin_dato) %>% 
  arrange(desc(circunscripcion)) %>% 
  janitor::adorn_totals("row")

citipp23_resumen <- 
  apgyeRStudio::primariasOrganismo("jdopaz*","2018-02-01", "2018-07-02")$CITIPP23$tabla() %>% 
  # filter(!(iep == "jdopaz0000bov" & DESCRIPCIN == "COBRO DE PESOS")) %>% 
  group_by(iep) %>%
  process(CITIPP23) %>% .$result  %>% 
  select(-calidad_primaria, -calidad) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = c("iep" = "organismo")) %>%  
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, organismo =iep, everything())
  

citipp23_primarias <- 
  apgyeRStudio::primariasOrganismo("jdopaz*","2018-02-01", "2018-07-02")$CITIPP23$tabla() %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  process(CITIPP23) %>% .$primarias

## Ojo excel W:\PRODUCTOS\INFORMES\2018\Informe Jdos Paz - Causas Iniciadas Primer Semestre
# jdopaz0000cre	170	230	1663	hecho
# jdopaz0000her	26	655	1365	hecho
# jdopaz0000mgr	99	811	930	hecho
# jdopaz0000vur	16	168	920	hecho
# jdopaz0000bas	14	161	107	hecho
# jdopaz0000slu	10	371	24	hecho
# jdopaz0000bov	909	673	0	hecho


cadr23 <- apgyeRStudio::primariasOrganismo("jdopaz*", "2018-02-01","2018-09-01")$CADRA23$tabla() %>%
  group_by(iep, data_interval_start, data_interval_end) %>%
  process(CADRA23) %>% .$result %>%
  ungroup() %>% 
  mutate(mes = months(data_interval_start)) %>% 
  mutate(mes = factor(mes, levels = c("febrero", "marzo", "abril", "mayo", 
                                      "junio", "julio", "agosto", "septiembre", 
                                      "octubre", "noviembre", "diciembre"),
                      ordered = TRUE)) %>% 
  select(organismo = iep, mes, causas_adespacho, causas_resueltas, 
         a_termino = res_atermino, luego_1erVencim = res_luego1venc, 
         luego_2doVencim = res_luego2venc, sentencias = res_xsentencia, 
         autos = res_xauto, sin_clasif =  res_xindeter) %>% 
  rowwise() %>% 
  mutate(resol_vencidas = str_c(round(sum(luego_1erVencim, luego_2doVencim, 
                                          na.rm = T)/causas_resueltas*100, 
                                      digits = 1), "%")) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = "organismo") %>% 
  select(circunscripcion, organismo_descripcion, organismo, everything()) %>% 
  arrange(organismo, mes) %>% 
  janitor::adorn_totals("row")



