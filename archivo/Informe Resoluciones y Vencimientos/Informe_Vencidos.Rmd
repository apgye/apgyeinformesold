---
title: "Resoluciones y Vencimientos"
author: "Área de Planificación Gestión y Estadística"
date: "2018-9-21"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
```

# Informe de Resoluciones y Vencimientos - AGOSTO 2018

```{r, echo=FALSE, warning=FALSE, message=FALSE}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)

```


```{r}
source("Informe_Vencidos.R")
```


## Aclaraciones Metodológicas


La información procesada para este informe surge de listados primarios de datos presentados por cada organismo de manera digital.   

Dichos listados primarios permanecen inalterados para consulta a tavés del módulo *Presentaciones*. 

Todo magistrado o funcionario puede, ante un error de registración, acceder al sistema JUSTAT y corregir el dato erróneo mediante un formulario de rectificación. Este formulario se encuentra también en el módulo *Presentaciones*, junto a los listados suministrados por el organismo.


## Civil y Comercial

### Primera Instancia

```{r}
kable(primera_inst_cco, caption = "Juzgados Civil y Comercial", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```


```{r}
kable(primera_inst_cco_matL, caption = "Juzgados Civil y Comercial -Materia Laboral", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```



```{r}
kable(primera_inst_cco_matL, caption = "Juzgados Civil y Comercial -Materia Familia", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```



```{r}
kable(primera_inst_fam, caption = "Juzgados Familia", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```



```{r}
kable(primera_inst_paz1, caption = "Juzgados de Paz 1a Categoría", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```

### Segunda Instancia

```{r}
kable(segunda_inst_cco, caption = "Segunda Instancia Civil y Comercial", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```


## Laboral

### Primera Instancia

```{r}
kable(primera_inst_lab, caption = "Juzgados Laborales", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```


### Segunda Instancia

```{r}
kable(segunda_inst_lab, caption = "Segunda Instancia Laboral", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10)  %>%
  row_spec(0, angle = 90) %>% 
  landscape() 
```

*Observación*: La Cámara de Apelaciones de Concordia - Sala Laboral- no presentó información de agosto con fecha de vencimiento para presentacion el 10/09/18. 

```{r}
# kable(organos_sin_informar, caption = 
#         "Órganos sin Audiencia Preliminar en el período", longtable = TRUE) %>% 
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
#                 full_width = F) 
```


Lic. Sebastián Castillo
Área de Planificación Gestión y Estadística    
`r Sys.Date()`








