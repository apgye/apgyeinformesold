resoluciones <- function(poblacion, operacion = "CADR1C", filtroMateria = "C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo)
  
  if("mat" %in% colnames(resultado)) {
    resultado <- resultado %>% 
      filter( is.na(mat) | toupper(mat) == !!filtroMateria )
  }
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }
  resultado <- resultado %>% 
    process() %>%
    .$result %>%
    ungroup()
  tiene_2do_venc <- "res_luego2venc" %in% colnames(resultado)
  if(!tiene_2do_venc) {
    resultado <- resultado %>% 
      mutate(res_luego2venc = 0)
  }
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, 
           matches("res_luego2venc"), sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego2venc", replacement="luego_2do_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    janitor::adorn_totals("row") %>% 
    mutate(resol_vencidas = stringr::str_c(round((luego_1er_venc + luego_2do_venc)/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           "luego 2do Vencim" = luego_2do_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas"
    )
  
  if(!tiene_2do_venc) {
    resultado <- resultado %>% 
      select(-`luego 2do Vencim`)
  }
  
  resultado
}
