---
title: Resoluciones en Tibunal de Juicio por Acto Conclusivo 
author: "Área de Planificación Gestión y Estadística"
date: '2018-01-02'
output:
  word_document:
    reference_docx: modelo_informe_gral.docx
  pdf_document: default
  html_document: default
---
Tabla que presenta la cantidad de resoluciones dictadas en Tribunal de Juicio (con integracion Unipersonal o Colegiada) clasificadas según tipo de acto conclusivo. Para las integraciones colegiadas se discriminan las resoluciones según el orden de votacion de los magistrados del Tribunal.

La metodología y procedimientos empleados en el relevamiento de datos usted la puede consultar, por órgano, instancia y fuero, en los siguientes links:

+ [OGA: Garantía y Tribunal de Juicio](https://drive.google.com/open?id=1mRQbflTQeORMGGUWOhz8Q6i0zCnJox5HL67PVlTPGu4)  
+ [Jdo.Penal de Niñ.Niña.Ado.](https://drive.google.com/open?id=1kuV1ioDmHqidw3W1NKYC3pABsAKNqK4sAWr9FBxf2YU) 
+ [Casación](https://drive.google.com/open?id=1V6pz_sW2v-vbyJxufaoMtgqUiiJ-6REoB97BNnlMYFA)   
 
Las rutinas de procesamiento estadístico empleadas en el cálculo de indicadores usted la puede consultar seleccionando la operación correspondiente del repositorio accesible en el siguiente link:

+ [Operación](https://bitbucket.org/apgye/apgyeoperationsjuser/src/master/)

***
