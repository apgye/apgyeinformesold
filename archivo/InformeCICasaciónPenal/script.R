# Informe sobre Causas Iniciadas en Casación Penal STJER 2017

library(data.table)
library(lubridate)
library(stringr)
library(purrr)
library(stringdist)
library(dplyr)
library(reshape2)
library(tidyr)
library(ggplot2)


# Datos primarios por listado
source('~/justat/R/leerArchivoPresentado.R', echo=TRUE)
dir <- "/home/scastillo/justat/PRODUCTOS/2018/InformeCICasaciónPenal/CI_segundo_semestre2017.TXT"
cica <- leerArchivoPresentado(dir)

# explorar
str(cica)
cica$caratula
cica$DEPENDENCIA


# limpieza de datos
cica$finicio <-  as.Date(cica$finicio, format="%d/%m/%Y")
cica$m_inic <- month(cica$finicio)

# limpieza dependencias
sum(is.na(cica$DEPENDENCIA))
cica$circ  <- str_split_fixed(gsub("[[:space:]]", "", cica$DEPENDENCIA), "-", 2)[,1]
cica$org <- str_split_fixed(gsub("[[:space:]]", "", cica$DEPENDENCIA), "-", 2)[,2]


# limpieza tipo proceso
table(cica$tproc)
table(cica$tproc[cica$circ == "PARANA"])

# limpieza tipo delitos
# tipos <- (str_split_fixed(gsub("[[:space:]]", "", cica$caratula), "-", 2))[, 2]
# table(tolower(substr(tipos, 1,6)))
tipos <- str_split_fixed(cica$caratula, "-", 2)[, 2]
tipos

# Mejor opción
tipos_substr <- word(tipos, 1,3)
tipos_substr <- gsub("[[:punct:]]", "", gsub("[[:space:]]", "", tipos_substr))
tipos_substr <- tolower(tipos_substr)
tipos_substr
personas <- c("villalbacésar", "romerodavid", "sanabrialuisa", "sueldomaría", 
              "neumansantiago", "pacayutabelardo", "lópezbrian", "lopezoscar", 
              "maldonadomartín", "moranraul", "moreyrabrenda", "godoymatias", 
              "berardomaría", "cs")    

tipos_substr[(tipos_substr %in% personas)] <- NA
table(tipos_substr)
cica$cod_delito <- tipos_substr

# Filtrar Procesos Sin Radicacion
cica <- cica %>% 
  filter(!tproc %in% c("ACCION DE AMPARO", "ACCION DE AMPARO POR MORA",
                    "HABEAS CORPUS", "ACCION DE EJECUCION",
                    "ACCION DE CUMPLIMIENTO DE SENTENCIA",
                    "RECURSO DE CASACION FALSEDAD IDEOLOGICA EN CONCURSO REAL",
                    "ACCION DE CUMPLIMIENTO DE SENTENCIA",
                     "RECURSO DE QUEJA"))

cica$circ[cica$circ ==""] <- NA 

cica <- cica %>% 
  filter(!is.na(circ))

# Analisis
costa_uruguay <- c("GUALEGUAY", "COLON", "CONCEPCIONDELURUGUAY", "CONCORDIA",
                   "FEDERACION", "GUALEGUAYCHU")
cica <- cica %>% 
  mutate(costa = circ %in% costa_uruguay) 

cica$costa[cica$costa == TRUE] <- "uruguay"
cica$costa[cica$costa == "FALSE"] <- "parana"


# Recodificar organismos
str_detect(cica$org, "EJEC")
jdosejec <- str_detect(cica$org, "EJEC")
jdogtia <- str_detect(cica$org, "GARANTIA")
tribunal <- str_detect(cica$org, "TRIBUNAL")
jodpenalmenores <- str_detect(cica$org, "NIÑOS")

cica$org[jdosejec] <- "JUZGADODEEJECUCION_PMS"
cica$org[jdogtia] <- "JUZGADOGARANTIA"
cica$org[tribunal] <- "TRIBUNALJUICIO"
cica$org[jodpenalmenores] <- "JUZGADOPENALMENORES"


# Analisis
table(cica$costa)

tablaxcosta <- cica %>%
  group_by(costa) %>%
  summarise(causas_iniciadas = n())

tablaxcosta_tproc_org <- cica %>%
  group_by(costa, tproc, org) %>%
  summarise(causas_iniciadas = n())

tablaxcosta_tproc_org_delito <- cica %>%
  group_by(costa, tproc, org, cod_delito) %>%
  summarise(causas_iniciadas = n())




