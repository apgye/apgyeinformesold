---
title: Causas en Trámite
subtitle: informe preliminar
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1.5cm
mainfont: Liberation Sans
sansfont: Liberation Sans
df_print: kable   
params:
   poblacion: "jdocco09*pna"
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r}
  if(!exists("poblacion")) {
    organismoEXPR <- utils::glob2rx(params$poblacion)
    poblacion <- apgyeJusEROrganization::listar_organismos() %>% filter(grepl(organismoEXPR, organismo))
  }

```

\blandscape
### Causas en Trámite

### Según último movimiento procesal
```{r}
entramite(poblacion = poblacion) %>%
  enTramitePorAño() %>%
    outputTable(caption = "Causas en Trámite, según último movimiento procesal (a agosto 2018)")  %>% 
    row_spec(0, angle = 45)
```
\elandscape

### Según tipo de proceso en los últimos 2 años
```{r}
entramite(poblacion = poblacion) %>%
  enTramitePorTipoProcesoUltimos2Años() %>%
    outputTable(caption = "Causas en Trámite, con mov procesal en los últimos 2 años por tipo de proceso (a agosto 2018)",
                row_group_label_position = "stack") %>% 
    column_spec(2, "3cm")
  
```
