# Probar método para ver

library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)


cams <- apgyeRStudio::primariasOrganismo("camcco*", "2018-08-01", "2018-09-01")$CADR2C$obtener()
str(cams)
colnames(cams)

cams_fdesp_fvenc1 <- cams %>% 
  filter(toupper(as) == "S", !is.na(fres)) %>% 
  mutate(fdesp1 = as.Date(fdesp1,  format="%d/%m/%Y"), 
         fvenc1 = as.Date(fvenc1, format="%d/%m/%Y"), 
         fres = as.Date(fvenc1, format="%d/%m/%Y")) %>% 
  mutate(dias_fdesp_fvenc1 = fvenc1 - fdesp1) %>% 
  select(iep, nro, caratula, tproc, as, fdesp1, fvenc1, fres, dias_fdesp_fvenc1)

# Agrupo por días corridos entre fdespyfvenc y tipo de proceso para ver discrepancias
cams_fdesp_fvenc_resumen <- cams_fdesp_fvenc1 %>% 
  group_by(fdesp1, tproc)  %>% 
  summarise(cantidad = n(), 
            dias_corridos_1venc = str_c(dias_fdesp_fvenc1, collapse = "-"), 
            camaras = str_c(iep, collapse = "-"), 
            nro_expte = str_c(nro, collapse = "-"))
