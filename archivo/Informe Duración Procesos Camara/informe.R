library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)

# Activas métodos de acceso a primarias
entidades <- listar_organismos()
start_date <- "2017-01-01"
end_date <- "2018-01-01"

magistrados_y_secretarios <- DB_PROD() %>% apgyeDSL::apgyeTableData('personal_planta_ocupada') %>%
  left_join(DB_PROD() %>% apgyeDSL::apgyeTableData('personal_personas') %>%
              select(idagente, categoria, apellido, nombres), by=c("idagente")
  ) %>%
  filter(grepl("JUEZ|SECRETARIO|VOCAL|FISCAL|DEFENSOR", categoria)) %>%
  collect()
magist_func_id_agentes <- magistrados_y_secretarios$idagente
# add "0" ti IDs for External Agent
magist_func_id_agentes[length(magist_func_id_agentes)+1] <- "0"



segunda_inst_cco <- apgyeRStudio::primariasOrganismo("camcco*", "2018-08-01","2018-09-01")$CADR2C$tabla() %>%
  group_by(iep, data_interval_start, data_interval_end) %>%
  process() %>% .$result %>% ungroup() %>% 
  mutate(mes = months(data_interval_start)) %>% 
  select(organismo = iep, 
         mes,
         causas_adespacho, 
         causas_resueltas, 
         a_termino = res_atermino, 
         luego_1erVencim = res_luego1venc, 
         luego_2doVencim = res_luego2venc, 
         sentencias = res_xsentencia, 
         autos = res_xauto, 
         sin_clasif =  res_xindeter) %>% 
    rowwise() %>% 
    mutate(resol_vencidas = str_c(round(sum(luego_1erVencim, luego_2doVencim, 
                                            na.rm = T)/causas_resueltas*100, 
                                        digits = 1), "%")) %>% 
    left_join(entidades[, c("organismo", "organismo_descripcion", 
                            "circunscripcion")], 
              by = "organismo") %>% 
    select(circunscripcion, organismo_descripcion, organismo, everything()) %>% 
    arrange(desc(circunscripcion)) %>% 
    janitor::adorn_totals("row") 


segunda_inst_lab <- apgyeRStudio::primariasOrganismo("camlab*", "2018-08-01","2018-09-01")$CADR2L$tabla() %>%
  group_by(iep, data_interval_start, data_interval_end) %>%
  process() %>% .$result %>% ungroup() %>% 
  mutate(mes = months(data_interval_start)) %>% 
  select(organismo = iep, 
         mes,
         causas_adespacho, 
         causas_resueltas, 
         a_termino = res_atermino, 
         luego_1erVencim = res_luego1venc, 
         sentencias = res_xsentencia, 
         autos = res_xauto, 
         sin_clasif =  res_xindeter) %>% 
  rowwise() %>% 
  mutate(resol_vencidas = str_c(round(sum(luego_1erVencim,  
                                          na.rm = T)/causas_resueltas*100, 
                                      digits = 1), "%")) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = "organismo") %>% 
  select(circunscripcion, organismo_descripcion, organismo, everything()) %>% 
  arrange(desc(circunscripcion)) %>% 
  janitor::adorn_totals("row") 


# Trabajo desde primarias


segunda_inst_cco <- apgyeRStudio::primariasOrganismo("camcco*", "2018-08-01","2018-09-01")$CADR2C$tabla() %>%
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect()
 

# duraciones
segunda_inst_cco_cadur <- apgyeRStudio::primariasOrganismo("camcco*", start_date, end_date)$CADUR2C$tabla() %>%
  #group_by(iep, data_interval_start, data_interval_end) %>%
  collect()

segunda_inst_lab_cadur <- apgyeRStudio::primariasOrganismo("camlab*", "2017-01-01", "2018-01-01")$CADUR2L$tabla() %>%
  #group_by(iep, data_interval_start, data_interval_end) %>%
  collect()


segunda_cco_cadur_resumen <- segunda_inst_cco_cadur %>% 
  group_by(iep) %>% 
  summarise(cantidad = n())


#trabajar con iniciado en mui y resuelto en camara
#No se puede trabajar por MUI
cinc_segunda_inst_xmui <- apgyeRStudio::primariasOrganismo("mui*", "2018-02-01", "2018-10-01")$CITIPM2$tabla() %>%
  #process() %>% .$result 
  collect() %>% 
  left_join(DB_PROD() %>% tbl("mui_radicacion") %>% as.data.frame())
  


