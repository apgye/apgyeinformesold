# Presentacion

# Iniciados
iniciados_tipo <- ci2017_xproc %>% 
  group_by(proc) %>% 
  summarise(Iniciados = sum(proc_iniciados, na.rm = T)) %>% 
  mutate(Proporcion = round((Iniciados/sum(Iniciados, na.rm = T)) * 100,
                            digit= 2)) %>% 
  filter(Proporcion >= 2) 

iniciados_procmenos2 <- ci2017_xproc %>% 
  group_by(proc) %>% 
  summarise(Iniciados = sum(proc_iniciados, na.rm = T)) %>% 
  mutate(Proporcion = round((Iniciados/sum(Iniciados, na.rm = T)) * 100,
                            digit= 2)) %>% 
  filter(Proporcion < 2) 

inic <- data_frame(proc = "OTROS (129 tproc)",
                   Iniciados = sum(iniciados_procmenos2$Iniciados), 
                   Proporcion = sum(iniciados_procmenos2$Proporcion))
  
iniciados_tipo <- bind_rows(iniciados_tipo, inic)  
write.table(iniciados_tipo, "iniciados_xtipo.csv", col.names = T, row.names = F, sep = ",")


inic_conciliables <- ci2017_xproc %>% 
  filter(proc %in% c("ALIMENTOS",
                                 "ALIMENTOS Y LITIS EXPENSAS", 
                                 "CUIDADO PERSONAL DEL HIJO O DE LOS HIJOS",
                                 "DILIGENCIAS PRELIMINARES", 
                                 "EJECUCION DE ALIMENTOS", 
                                 "EJECUCION DE CONVENIO",
                                 "EJECUCION DE SENTENCIA", 
                                 "MEDIDA PROVISIONAL",
                                 "REGIMEN DE COMUNICACION",
                                 "RESPONSABILIDAD PARENTAL",
                                 "SUMARISIMO") | 
           str_detect(proc, "MEDIDA CAUTELAR") |
           str_detect(proc, "ORDINARIO"),
         !str_detect(proc, "INCIDENTE")) %>% 
  group_by(proc) %>% 
  summarise(Cantidad = sum(proc_iniciados, na.rm = T))

sum(inic_conciliables$Cantidad)


# Audiencias por Tipo
audif_tipo <- df_audif_tipo_durac_mes %>% 
  group_by(tipo) %>% 
  summarise(`Total Realizadas` = n(), 
            promedio_duracion = round(mean(durac), digits =  2)) %>% 
  mutate(Proporcion = round((`Total Realizadas`/sum(`Total Realizadas`, 
                                                    na.rm = T)) * 100, digit= 2))

write.table(audif_tipo, "audif_xtipo.csv", col.names = T, row.names = F, sep = ",")


# Eficacia de la Conciliación en Audiencias Conciliables -Conciliacion y Preliminar
# Códigos 11 y 12
# esta 2
# fea !na

sum(causas_resueltas$conciliacion_enaudiencia, na.rm = T)

conciliables <- sum(tasas_realiz_conciliacion$conciliacion_total, 
    tasas_realiz_conciliacion$sinconciliacion, na.rm = T)

conciliadas <- sum(tasas_realiz_conciliacion$conciliacion_total, 
                   na.rm = T)
conciliables
conciliadas
conciliadas/conciliables * 100

# Conciliaciones por Circunscripción

org_circ <- fuero_familia %>% select(organismo, circunscripcion) %>% 
  rename(iep = organismo)
poblacionIndec <- read_csv("data/INDEC_pob_proyectal01-07-Año.csv")

poblacionIndec2018 <- poblacionIndec %>% 
  select(Departamento, A2018) %>% 
  rename(circunscripcion = Departamento )
poblacionIndec2018

cnc_circ <- left_join(ci_aud_conc, org_circ, by = ("iep")) 

cnc_circ <- cnc_circ %>% 
  group_by(circunscripcion) %>% 
  summarise(conciliaciones = sum(Conciliaciones, na.rm = T)) 
cnc_circ <- cnc_circ %>%  filter(circunscripcion != "Chajarí")
cnc_circ$conciliaciones[cnc_circ$circunscripcion == "Federación"] <- 12

cnc_circ <- left_join(cnc_circ, poblacionIndec2018, by = "circunscripcion")
cnc_circ <- cnc_circ %>% 
  mutate(ConcX1000h = round(conciliaciones/(A2018/1000), digits = 2)) %>% 
  mutate(habX1000 = A2018/1000) %>% 
  arrange(desc(A2018))

ggplot(cnc_circ, aes(x=habX1000, y=conciliaciones)) +
  geom_point(shape=1) +  geom_smooth(method=lm , color="red", se=TRUE)  +
  geom_text_repel(label= cnc_circ$circunscripcion, 
                nudge_x = 0.25, nudge_y = 0.25) +
  #theme(panel.background = element_blank()) +
  labs(title = "Correlación entre Habitantes (cada 1000) y Conciliaciones",
       y = "Conciliaciones", x = "Habitantes") 

write.table(cnc_circ, "conciliacionesyhabitantes.csv", 
            col.names = T, row.names = F, sep = ";")


# Duración de Procesos y Conciliacion

# consultas AUDIF
df_AUDIF <- DB_CONNECTION %>% apgyeData(AUDIF) %>%
  interval(2018-02-01, 2018-06-01) %>%
  group_by(iep, data_interval_start, data_interval_end) %>% 
  collect() 

df_audi_conct <- df_audi %>% 
  mutate(ra = as.character(ra), esta = as.integer(esta)) %>% 
  filter(!is.na(fea), esta == 2, ra == "T") 
  
# consultas CAMOV
df_CAMOV <- DB_CONNECTION %>% apgyeData(CAMOV) %>%
  interval(2018-02-01, 2018-06-01) %>%
  filter(iep %in% !!fuero_familia$organismo) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% 
  collect() %>% .$tbs_prim_CAMOV %>% 
  mutate(finicio = as.Date(finicio, format="%d/%m/%Y")) %>% 
  mutate(mes = month(as.Date(finicio)),
         ano = year(as.Date(finicio))) %>% 
  select(1,2,3,4,9,10,11)


df_audi_conct <- df_audi_conct %>% 
  mutate(finicio = as.Date(finicio, format="%d/%m/%Y"), 
         fea = as.Date(fea, format="%d/%m/%Y"), 
         durac_ini_conc = fea - finicio) %>% 
  select(1,2,3,4,5, 9,18,21)


durac_tproc_conc <- df_audi_conct %>% 
  group_by(tproc) %>% 
  summarise(cantidad_casos = n(),
            duracion_mediana = median(durac_ini_conc)) %>% 
  filter(cantidad_casos > 10)

ggplot(df_audi_conct, aes(durac_ini_conc)) +
  geom_histogram() +
  scale_x_continuous(breaks = seq(0, 100, 5), lim = c(0, 500))
  labs(x = "Duracion en Dias Corridos", y = "Cantidad de Procesos") +
  ggtitle("Cantidad de Procesos Conciliados y Duración feb/may 2018 -Histograma-") 
  
df_audi_conct_categorias <- df_audi_conct %>% 
  mutate(categorias = cut(as.integer(durac_ini_conc),
                          breaks = c(0, 365, 730, 1095, Inf),
                      labels=c("<laño","<1año","<2años", "<3años"))) %>% 
  group_by(categorias) %>% 
  summarise(cantidad = n())




