library(apgyeDSL)
library(dplyr)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)


if(!exists("DB")) {
  DB <- DBI::dbConnect(RPostgreSQL::PostgreSQL(),host = "192.168.254.221", dbname = "justat2.2", user = "postgres",password = "Toba201*")
}

personas <- DB %>% apgyeData( GHSTRD ) %>% 
    interval(2018-02-01, 2018-03-01) %>% 
    activate(tbs_prim_GHSTRD_personas) %>% 
    collect() 

table(personas$organismo)

personas_temporarios <- personas %>% 
  filter(revista == "Temporario") %>% 
  group_by(sexo) %>% 
  summarise(cantidad = n())
  

personas_permanentes <- personas %>% 
  filter(revista != "Temporario")

df_permanentes <- personas_permanentes %>% 
  filter(!str_detect(organismo, "Electoral")) %>% 
  group_by(categoria) %>% 
  summarise(masculino = sum(sexo =="M", na.rm = T), 
         femenino = sum(sexo == "F", na.rm = T), 
         total = masculino + femenino) %>% 
  select(categoria, femenino, masculino, total)

sum(df_permanentes$total, na.rm = T)
sum(df_permanentes$masculino, na.rm = T) + sum(df_permanentes$femenino, na.rm = T)

# jueces extraido de df_permanentes

# fiscal
fiscales <- df_permanentes %>% 
  filter(str_detect(categoria, "FISCAL")) %>% 
  summarise(masculino = sum(masculino), 
            femenino = sum(femenino)) # Restar Fiscal General 1


# defensor
defensor <- df_permanentes %>% 
  filter(str_detect(categoria, "DEFENSOR")) %>% 
  summarise(masculino = sum(masculino), 
            femenino = sum(femenino)) # Restar Fiscal General 1

# secretarios
secretarios <- df_permanentes %>% 
  filter(str_detect(categoria, "SECRET")) 
  
# EMPLEADOS
auxiliares <- c("AUXILIAR AYUDANTE", "AUXILIAR DE PRIMERA", "AUXILIAR DE SEGUNDA",
                "AUXILIAR MAYOR", "AUXILIAR PRINCIPAL TECNICO", "AUXILIAR TECNICO",
                "AYUDANTE")

empleados <- df_permanentes %>% 
  filter(!str_detect(categoria, "FISCAL|DEFENSOR|SECRET|JUEZ|VOCAL|PROCURADOR|No usar|ESCALAFON")) %>% 
  filter(!categoria %in% auxiliares) 
sum(empleados$total)

empleados_funcionjcial <- personas_permanentes %>% 
  filter(!str_detect(categoria, "FISCAL|DEFENSOR|SECRET|JUEZ|VOCAL|PROCURADOR|No usar|ESCALAFON")) %>% 
  filter(!categoria %in% auxiliares) %>% 
  filter(!str_detect(organismo, "Electoral")) %>% 
  filter(str_detect(organismo, "Juz|Cam|Cám|Sal|OGA|OMA|Jdo|Equipo|Contencioso|Delegado Jud|Penal")) %>% 
  summarise(masculino = sum(sexo == "M", na.rm = T), 
            femenino = sum(sexo == "F", na.rm = T)) 

empleados_funcionadm <- personas_permanentes %>% 
  filter(!str_detect(categoria, "FISCAL|DEFENSOR|SECRET|JUEZ|VOCAL|PROCURADOR|No usar|ESCALAFON")) %>% 
  filter(!categoria %in% auxiliares) %>% 
  filter(!str_detect(organismo, "Electoral")) %>% 
  filter(!str_detect(organismo, "Juz|Cam|Cám|Sal|OGA|OMA|Jdo|Equipo|Contencioso|Delegado Jud|Penal")) %>% 
  filter(!str_detect(organismo, "Fiscal|Defens|Electoral|Proc|Minister|Adopcion")) %>% 
  filter(!str_detect(revista, "Temporario")) %>% 
  summarise(masculino = sum(sexo == "M", na.rm = T), 
            femenino = sum(sexo == "F", na.rm = T)) 

empleados_fiscal <- personas_permanentes %>% 
  filter(!str_detect(categoria, "FISCAL|DEFENSOR|SECRET|JUEZ|VOCAL|PROCURADOR|No usar|ESCALAFON")) %>% 
  filter(!categoria %in% auxiliares) %>% 
  filter(str_detect(organismo, "Fiscal|Proc|Minister")) %>% 
  summarise(masculino = sum(sexo == "M", na.rm = T), 
            femenino = sum(sexo == "F", na.rm = T)) 


empleados_defensa <- personas_permanentes %>% 
  filter(!str_detect(categoria, "FISCAL|DEFENSOR|SECRET|JUEZ|VOCAL|PROCURADOR|No usar|ESCALAFON")) %>% 
  filter(!categoria %in% auxiliares) %>% 
  filter(str_detect(organismo, "Defens|Adopcion")) %>% 
  summarise(masculino = sum(sexo == "M", na.rm = T), 
            femenino = sum(sexo == "F", na.rm = T)) 

empleados_funcionjcial
empleados_funcionadm
empleados_fiscal
empleados_defensa
rowSums(empleados_defensa)
rowSums(empleados_fiscal)
rowSums(empleados_funcionadm)
rowSums(empleados_funcionjcial)
nrow(personas)


# Maestranzas
maestranza <- df_permanentes %>% 
    filter(categoria %in% auxiliares)
sum(maestranza$femenino)
sum(maestranza$masculino)
sum(maestranza$total)

#### MEJORAR ESTE RELEVAMIENTO DANDO DEFINICIONES EXTENSIVAS DE LAS CATEGORIAS
#### CREANDO LAS VARIABLES NECESARIAS, Y LUEGO PROCESANDO







