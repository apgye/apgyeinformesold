library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)


jdosquieb <- apgyeRStudio::primariasOrganismo("jdocco*", "2018-06-01", "2018-09-01")$CADR1C$obtener()


# junio, julio, agosto

jdos_fdesp_fvenc1 <- jdosquieb %>% 
  filter(iep %in% c("jdocco0901pna", "jdocco0902pna", "jdocco0501con", "jdocco0502con", 
                  "jdocco0302uru")) %>% 
  filter(!is.na(fres)) %>% 
  mutate(fdesp = as.Date(fdesp,  format="%d/%m/%Y"), 
         fvenc1 = as.Date(fvenc1, format="%d/%m/%Y"), 
         fres = as.Date(fvenc1, format="%d/%m/%Y")) %>% 
  mutate(dias_fdesp_fvenc1 = fvenc1 - fdesp) %>% 
  select(iep, nro, caratula, tproc, as, fdesp, fvenc1, fres, dias_fdesp_fvenc1)

# Agrupo por días corridos entre fdespyfvenc y tipo de proceso para ver discrepancias
resumen <- jdos_fdesp_fvenc1 %>% 
  filter(!stringr::str_detect(tproc, "AMPARO")) %>% 
  mutate(as = toupper(as)) %>% 
  group_by(tproc, iep, as)  %>% 
  summarise('cantidad de resoluciones' = n(), 
            'listado dias corridos desde despacho a 1venc' = str_c(dias_fdesp_fvenc1, collapse = "-"), 
            nro_expte = str_c(nro, collapse = "-")) %>% 
  arrange(tproc)
