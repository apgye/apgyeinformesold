---
title: "Informe Causas Iniciadas por Ciudad"
author: "Área de Planificación Gestión y Estadística"
date: "2018-8-7"
output: 
 word_document:
    reference_docx: modelo.docx
---

```{r setup, include=FALSE, warning=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(data.table)
library(lubridate)
library(stringr)
library(purrr)
library(stringdist)
library(dplyr)
library(tidyr)
library(pander)
library(readr)
library(stringr)
library(lubridate)
library(janitor)
```

```{r, echo=FALSE, warning=FALSE, message=FALSE}
setwd("~/justat/PRODUCTOS/2018/Cusas Iniciadas por Ciudad en Jdo Familia Chajari")
tproc <- apgyeOperationsJusER::Tipo_procesos
municip <- read_csv("municip&localidades.csv")
```


## Informe Sobre Causas Iniciadas por Ciudad 

En fecha miércoles 04/07/2018 13:58, se solicitó a la Dra. Griselda Mabel Moscatelli efectúa informe de causas iniciadas en el 2017 empleando listado primario de datos recuperando el domicilio registrado de cada justiciable actor. 

```{r, echo=FALSE, message=FALSE, warning=FALSE}
df <- apgyeOperationsJusER::leerArchivoPresentado("ESTADISTICA POR CIUDAD.TXT", isDBF = F)
casos_informados <- nrow(df)
```

El total de casos informados es: `r casos_informados`

Del total de iniciados surgen las siguientes cantidades agrupadas por tipo de proceso:

```{r, echo=FALSE}
casos_xtproc <- as.data.frame(table(df$trpoc)) %>% 
  rename(tipo_proceso = 1, iniciados2017 =2) %>% 
  filter(tipo_proceso %in% tproc$tipo_de_procesos) %>% 
  filter(!str_detect(tipo_proceso, "OFICIO|SITUACION")) %>% 
  arrange(desc(iniciados2017))
```

```{r, echo=FALSE}
knitr::kable(casos_xtproc, caption = "Causas Iniciadas 2017")
```

Se advierten casos registrados en el sistema que no están conteplados como tipos procesales válidos según listado normalizado de Lex-Doctor.

```{r, echo=FALSE}
procesos_desclasificados <-
  df$trpoc[!df$trpoc %in% tproc$tipo_de_procesos] %>% 
  table() 

procesos_desclasificados <- as.data.frame(procesos_desclasificados) %>% 
  arrange(desc(Freq))

knitr::kable(procesos_desclasificados, caption = "Procesos No Reconocidos")
```

## Analisis

```{r, echo=FALSE}
# Asignación de tipos de variables y creación de variables de analisis
for (i in seq_along(df)) {
  df$finicio <-
    as.Date(df$finicio, format="%d/%m/%Y")
  df$a_inicio <-
    year(df$finicio)
  df$justiciable <- 
    toupper(df$justiciable)
  df$c_actor <- 
    str_detect(df$justiciable, "ACTOR")
}
df_a <- df[df$c_actor == T & df$a_inicio == 2017, ]

# Muestra
casos_c_actores <- nrow(df_a)

```


Los causas con actores son `r casos_c_actores`, representan el `r round(casos_c_actores/casos_informados, digit = 2)`% del total de casos informados.


```{r, echo=FALSE}
# Limpieza de Datos para el df_actores.
localidades <- df_a %>% 
  separate_rows(justiciable, convert = TRUE, sep = "%") %>% 
  filter(str_detect(justiciable, " ")) %>% 
  separate(justiciable, into = c("nombre_actor", "caracter", "localidad"),
           sep = "\\$") %>% 
  group_by(nro) %>% 
  summarise(localidad_actores = str_c(localidad, collapse = "-")) %>% 
  left_join(df_a, by = "nro") %>% 
  select(nro, caratula, trpoc, justiciable, localidad_actores)
  
  
sumario <- localidades %>% 
  summarise(federeacion = sum(str_detect(localidad_actores, "FEDERAC") & !
              str_detect(localidad_actores, "CHAJAR")), 
            chajari = sum(str_detect(localidad_actores, "CHAJAR") & !
               str_detect(localidad_actores, "FEDERAC")), 
            ambas = sum(str_detect(localidad_actores, "FEDERAC") & 
              str_detect(localidad_actores, "CHAJAR")),
            otras = sum(!str_detect(localidad_actores, "CHAJAR|FEDERAC"))) %>% 
  as.data.frame()  %>% 
  gather() %>% 
  rename(localidades = key, causas_iniciadas = value) 

```


De las causas informadas con actor se desprende la siguiente distribución según la localidad de domicilio:

```{r, echo=FALSE}
knitr::kable(sumario, caption = "Tabla 1. Cantidad de Causas Iniciadas según domicilio registrado del Actor")

```

## Consideraciones Finales

Aunque la muestra analizada de 262 causas con actores registrados permite alcanzar niveles aceptables de confianza en los resultados logrados, anomalías en la registración obligan a considerar con prudencia la distribución informada en la Tabla.1.


## Anexo


```{r}
knitr::kable(localidades, caption = "Causas Iniciadas con domicilio registrado del Actor")
```

