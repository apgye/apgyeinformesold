library(apgyeDSL)
library(dplyr)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)

if(!exists("DB")) {
  DB <- DBI::dbConnect(RPostgreSQL::PostgreSQL(),host = "192.168.254.221", dbname = "justat2.2", user = "postgres",password = "Toba201*")
}

resolpg <- DB %>% apgyeData( RESOLPG ) %>% 
    activate(tbs_prim_RESOLPG) %>% 
    filter(tsent %in% c("1", "2", "3")) %>% 
    collect()
    
condenas_gtia <- resolpg %>% 
  select(nro:tproc, fres, tsent)

resolpt <- DB %>% apgyeData( RESOLPT ) %>% 
  activate(tbs_prim_RESOLPT) %>% 
  filter(tres %in% c("1", "2", "3")) %>% 
  collect()

condenas_tjui <- resolpt %>% 
  select(nro:tproc, fres, tres)

condenas_gtia$instancia <- "garantia"
condenas_tjui$instancia <- "t_juicio"
colnames(condenas_gtia)
colnames(condenas_tjui) <- colnames(condenas_gtia)
condenas <- bind_rows(condenas_gtia, condenas_tjui)

condenas_parana <- condenas %>% 
  mutate(tsent = recode(tsent, `1` = "CONDENA_EFECTIVA", 
       `2` = "CONDENA_EFECT_SUSTITUTIVA", 
       `3` = "CONDENA_CONDICIONAL"))
write.table(condenas_parana, "condenas_parana.csv", row.names = F, 
            col.names = T, sep = ",")

months()

library(lubridate)
condenas_parana %>% 
  mutate(mes = months(as.Date(fres, format="%d/%m/%Y")), 
         año = year(as.Date(fres, format="%d/%m/%Y"))) %>% 
  group_by(tsent, año, mes) %>% 
  summarise(cantidad = n()) %>% 
  arrange(año, mes) %>%  View


