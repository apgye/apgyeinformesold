---
title: "Informe La Paz - Penal"
author: "Área Planificación, Gestión y Estadística"
date: "23 de agosto de 2018"
output:word
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE)
library(knitr)
library(kableExtra)
```

```{r echo = FALSE, warning = FALSE, include=FALSE} 
source('./InformePenal.R')
```


## Resoluciones en Garantía

Período: 2017-6-01 - 2018-08-01

```{r}
gtia_resol_1 <- gtia_resol %>% 
  select(-c(iep, data_interval_end, data_interval_start, calidad_primaria, calidad,
            sobreseidos, condenados)) %>% 
  select_if(~is.numeric(.) & mean(., na.rm=TRUE) > 0) 
kable(gtia_resol_1)
```

## Resoluciones Tribunal de juicio

Período: 2017-6-01 - 2018-08-01

```{r}
kable(tjui_resol %>% 
        select(6:ncol(.)), "latex", booktabs = T) %>% 
  kable_styling(latex_options = c("striped", "scale_down"))
```


## Audiencias Penal

Período: 2017-6-01 - 2018-08-01

```{r}

kable(audip %>% 
        select(6:9, 17:19), "latex", booktabs = T) %>% 
  kable_styling(latex_options = c("striped", "scale_down"))
```


## Primarias Audiencias

```{r}

kable(audip_tprim2 %>% collect() %>% ungroup() %>% 
        select(1:17) %>% 
        mutate(caratula = stringr::str_trunc(caratula, 35), 
               tproc = stringr::str_trunc(tproc, 35) ), "latex", booktabs = T, longtable = T) %>% 
  kable_styling(latex_options = c("striped"), font_size = 7) %>% 
  landscape()
```
