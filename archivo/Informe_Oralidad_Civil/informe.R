library(dplyr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)

# Activas métodos de acceso a primarias
entidades <- listar_organismos()

jdos_civiles <- entidades %>% 
  filter(str_detect(fuero, "Civil")) %>% 
  filter(tipo == "jdo", str_detect(materia, "cco" ))

audic <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                          "2018-07-01", "2018-08-02")$AUDIC$obtener()

organos_sin_informar <- jdos_civiles$organismo[!jdos_civiles$organismo %in% names(table(audic$iep))] 






