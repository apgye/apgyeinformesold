---
title: "Informe <Generico> Comparativo - Juzgados de Familia"
author: "Área Planificación, Gestión y Estadística"
date: "3 de octubre de 2018"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
subtitle: informe preliminar
df_print: kable  
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
```

```{r, echo=FALSE, warning=FALSE, message=FALSE}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)

```


```{r}
source("informe.R")
```


```{r}
outputTable <- function (table, caption) {
  table %>% rename_all(.funs = stringr::str_replace_all, pattern = "_", replacement=" ") %>% 
    kable(caption = caption,
        align = 'c', longtable = TRUE, booktabs = T ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), latex_options = c("repeat_header"),
                  full_width = F, font_size = 8)
}
```


# Juzgados Familia

## Causas Iniciadas - Primer Semestre 2018

Esta información se extrajo de la Operación sobre **Causas Iniciadas por Tipo de Proceso**, primer semestre 2018, cuyos listados primarios son presentados por organismo en julio.  

```{r}
iniciadosCITIPJ(poblacion, "jdofam*") %>%
  outputTable(caption = "Iniciadas Primer Semestre 2018") %>%
  column_spec(3, width = "5cm") %>% 
  collapse_rows(columns = 1:2, row_group_label_position = 'stack')
```

Lic. Sebastián Castillo

\pagebreak

# Anexo - Metodos de Consulta Bases de Datos y Procesamiento Estadístico

```{r, echo=TRUE}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(lubridate)

entidades <- apgyeJusEROrganization::listar_organismos()

# Poblacion
poblacion <- entidades %>% 
  filter(tipo %in% c("jdo"), (str_detect(materia, "fam")))  


iniciadosCITIPJ <- function(poblacion, patronOrg, start_date="2018-01-01", end_date = "2018-07-02") {
  
  apgyeRStudio::primariasOrganismo(patronOrg,  start_date, end_date)$CITIPJ$obtener() %>%
    filter(iep %in% poblacion$organismo) %>%
    mutate(nivel = stringr::str_count(`Tipo de proceso`, "     ")) %>%
    tidyr::separate( `Tipo de proceso`, into = c("mat", "proc"),
                     sep = "     "  )  %>%
    mutate(mat = if_else(mat=="", NA_character_ ,mat)) %>%
    tidyr::fill(mat) %>% filter(nivel == 1) %>%
    mutate(cantidad = as.integer(proc)) %>%
    group_by(iep, mat, proc) %>%
    filter(!str_detect(mat, "DEFENSORIA|ADMINISTRATIVO|SALA|INSTRUCCION|PAZ")) %>%
    summarise(Cantidad_Iniciados = sum(as.integer(Procesos), na.rm = T)) %>%
    ungroup() %>%
    left_join(entidades[, c("organismo", "organismo_descripcion",
                            "circunscripcion")], by = c("iep" = "organismo")) %>%
    ungroup() %>%
    select("circunscripción" = circunscripcion,  organismo = organismo_descripcion, proc, iniciados = Cantidad_Iniciados) %>%
    arrange(`circunscripción`, organismo, desc(iniciados)) %>%
    janitor::adorn_totals("row")
  
} 



```



