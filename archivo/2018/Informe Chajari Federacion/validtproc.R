library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tidyr)
library(tibble)

poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam"))

jdos_cco <- poblacion_total %>% 
  filter(grepl("cco", organismo)) %>% 
  filter(str_detect(materia, "cco"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_cco_ecq <- poblacion_total %>% 
  filter(grepl("cco", organismo)) %>% 
  filter(str_detect(materia, "eje|cqb"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_lab <- poblacion_total %>% 
  filter(str_detect(organismo, "lab"), tipo != "cam")

desagregacion_mensual = T
start_date <- "2018-02-01" 
end_date <- "2018-11-01"


inic <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CETAL_XL") %>% 
  filter(iep %in% jdos_cco$organismo) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO|LABORAL", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(fmov = dmy(fmov)) %>%
  filter(finicio >= start_date, finicio < end_date ) %>% 
  group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
  filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
  ungroup()  %>%                                # para los casos en que se hacen pases por competencia
  select(iep, nro, caratula, tproc, finicio)  %>% 
  union(
    DB_PROD() %>% 
      apgyeDSL::apgyeTableData("CINC1C") %>% 
      filter(iep %in% jdos_cco$organismo) %>% 
      filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(!is.na(finicio)) %>% 
      filter(finicio >= start_date, finicio < end_date ) %>% 
      select(iep, nro, caratula, tproc, finicio)  
  ) %>% 
  collect() %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  mutate(tproc = toupper(tproc)) %>%
  mutate(gtproc_forma = case_when(
    str_detect(tproc, "^ORDINARIO|^ORDINARIO |USUCAP") ~ "ORDINARIO", #
    str_detect(tproc, "^SUMARISIMO|^SUMARISIMO ") ~ "SUMARISIMO",  #
    str_detect(tproc, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES",
    str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR ") ~ "CAUTELARES",
    str_detect(tproc, "MONITORIO") ~ "MONITORIOS",
    str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO") ~ "EJECUCIONES", #
    str_detect(tproc, "^INTERDICT") ~ "INTERDICTOS",
    str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTITUCIONALES",
    str_detect(tproc, "^PREPARACION DE LA VIA") ~ "PREPARATORIOS",
    str_detect(tproc, "^SUCESORIO") ~ "SUCESORIOS", #
    # Anomalías: desclasificados, no procesos y errores
    str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE") ~ "noprocesos",
    str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA") ~ "desclasificados",
    str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO") ~ "error",
    # match estricto
    tproc %in% c("PERSONAL") ~ "error",
    tproc %in% c("LABORAL") ~ "desclasificados", 
    TRUE ~ "OTROS"
  )) %>% 
  group_by(iep, gtproc_forma, tproc) %>% 
  summarise(cantidad = n()) %>% 
  arrange(gtproc_forma, desc(cantidad)) %>% 
  ungroup() %>% 
  left_join(jdos_cco %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  # left_join(tipos_proceso, by=c("tproc"="tipo_de_procesos")) %>% 
  mutate(tproc = ifelse(tproc =="-", "Total Organismo", tproc)) %>% 
  select(circunscripcion, organismo, grupo_proceso = gtproc_forma,
         tipo_proceso = tproc, everything(), -iep) 


inic_ecq <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CETAL_XL") %>% 
  filter(iep %in% jdos_cco_ecq$organismo) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO|LABORAL", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(fmov = dmy(fmov)) %>%
  filter(finicio >= start_date, finicio < end_date ) %>% 
  group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
  filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
  ungroup()  %>%                                # para los casos en que se hacen pases por competencia
  select(iep, nro, caratula, tproc, finicio)  %>% 
  union(
    DB_PROD() %>% 
      apgyeDSL::apgyeTableData("CINC1C") %>% 
      filter(iep %in% jdos_cco_ecq$organismo) %>% 
      filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(!is.na(finicio)) %>% 
      filter(finicio >= start_date, finicio < end_date ) %>% 
      select(iep, nro, caratula, tproc, finicio)  
  ) %>% 
  collect() %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  group_by(iep, tproc) %>% 
  summarise(cantidad = n()) %>% 
  arrange(desc(cantidad)) %>% 
  ungroup() %>% 
  left_join(jdos_cco_ecq %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  # left_join(tipos_proceso, by=c("tproc"="tipo_de_procesos")) %>% 
  mutate(tproc = ifelse(tproc =="-", "Total Organismo", tproc)) %>% 
  select(circunscripcion, organismo, tipo_proceso = tproc, everything(), -iep) 

# Iniciados por Tipos de Procesos
ini_tproc_e <- inic_ecq %>% 
  filter(str_detect(organismo, "10")) %>% 
  group_by(tipo_proceso) %>% 
  filter(!str_detect(tipo_proceso, "ACCION|APELACION|SEGUNDA INSTANCIA")) %>% 
  summarise(cantidad = sum(cantidad))


# Iniciados por Tipos de Procesos
ini_tproc <- inic %>% 
  group_by(tipo_proceso) %>% 
  summarise(cantidad = sum(cantidad))



# Procesado de tabla maestra 

tproc <- apgyeOperationsJusER::tipo_de_procesos 

tproc_editada <- ini_tproc %>% 
  rename(tipo_de_procesos = tipo_proceso) %>%
  # toupper !!!!!!
  mutate(gtproc_forma = case_when(
    str_detect(tipo_de_procesos, "^ORDINARIO|^ORDINARIO |USUCAP") ~ "ORDINARIO", #
    str_detect(tipo_de_procesos, "^SUMARISIMO|^SUMARISIMO ") ~ "SUMARISIMO",  #
    str_detect(tipo_de_procesos, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES",
    str_detect(tipo_de_procesos, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR ") ~ "CAUTELARES",
    str_detect(tipo_de_procesos, "MONITORIO") ~ "MONITORIOS",
    str_detect(tipo_de_procesos, "^EJECU|^PROCESO DE EJECU|^APREMIO") ~ "EJECUCIONES", #
    str_detect(tipo_de_procesos, "^INTERDICT") ~ "INTERDICTOS",
    str_detect(tipo_de_procesos, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTITUCIONALES",
    str_detect(tipo_de_procesos, "^PREPARACION DE LA VIA") ~ "PREPARATORIOS",
    str_detect(tipo_de_procesos, "^SUCESORIO") ~ "SUCESORIOS", #
    # Anomalías: desclasificados, no procesos y errores
    str_detect(tipo_de_procesos, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE") ~ "noprocesos",
    str_detect(tipo_de_procesos, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA") ~ "desclasificados",
    str_detect(tipo_de_procesos, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO") ~ "error",
    # match estricto
    tipo_de_procesos %in% c("PERSONAL") ~ "error",
    tipo_de_procesos %in% c("LABORAL") ~ "desclasificados", 
    TRUE ~ "OTROS"
  )) %>% 
  
  
  
  do(bind_rows(., data.frame(gtproc_forma = "Total Organismo", 
                             cantidad =  sum(cantidad[tipo_proceso == "subtotal"], na.rm = T)))) 



  
# Guardado de data frames
# setwd("~/apgyeInformes/archivo/Tipo_procesos")
# write.table(tproc_editada, "tproc.csv", col.names = T, row.names = F, sep = ";")
# write.table(inic_tproc, "inictproc.csv", col.names = T, row.names = F, sep = ";")




# ########################## otros intentos
# # call the stringdistmatrix function and request 20 groups
# library(stringdist)
# uniquemodels <- inic_tproc$tipo_proceso
# distancemodels <- stringdistmatrix(uniquemodels,uniquemodels,method = "jw")
# rownames(distancemodels) <- uniquemodels
# colnames(distancemodels) <- uniquemodels
# hc <- hclust(as.dist(distancemodels))
# dfClust <- data.frame(uniquemodels, cutree(hc, k=30))
# names(dfClust) <- c('modelname','cluster')
# # visualize the groupings
# plot(table(dfClust$cluster))
# print(paste('Average number of models per cluster:', mean(table(dfClust$cluster))))
#


inic <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CETAL_XL") %>% 
  filter(iep %in% jdos_lab$organismo) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO|LABORAL", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(fmov = dmy(fmov)) %>%
  filter(finicio >= start_date, finicio < end_date ) %>% 
  group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
  filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
  ungroup()  %>%                                # para los casos en que se hacen pases por competencia
  select(iep, nro, caratula, tproc, finicio)  %>% 
  union(
    DB_PROD() %>% 
      apgyeDSL::apgyeTableData("CINC1L") %>% 
      filter(iep %in% jdos_cco$organismo) %>% 
      filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(!is.na(finicio)) %>% 
      filter(finicio >= start_date, finicio < end_date ) %>% 
      select(iep, nro, caratula, tproc, finicio)  
  ) %>% 
  collect() %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  mutate(tproc = toupper(tproc)) %>%
  mutate(gtproc_forma = case_when(
    str_detect(tproc, "^COBRO") ~ "COBRO DE PESOS", #
    str_detect(tproc, "^ACCIDENTE") ~ "ACCIDENTE DE TRABAJO", #
    str_detect(tproc, "^APELACION DIC") ~ "APELACION COMISION MEDICA", #
    str_detect(tproc, "^EJEC|^PROCESO DE EJECU|^APREMIO") ~ "EJECUCIONES", #
    str_detect(tproc, "^ORDINARIO|^ORDINARIO ") ~ "ORDINARIO", #
    str_detect(tproc, "^SUMARISIMO|^SUMARISIMO ") ~ "SUMARISIMO",  #
    str_detect(tproc, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES",
    str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR ") ~ "CAUTELARES",
    str_detect(tproc, "MONITORIO") ~ "MONITORIOS",
    str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTITUCIONALES",
    # Anomalías: desclasificados, no procesos y errores
    str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE") ~ "noprocesos",
    str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA") ~ "desclasificados",
    str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO") ~ "error",
    # match estricto
    tproc %in% c("PERSONAL") ~ "error",
    tproc %in% c("LABORAL") ~ "desclasificados", 
    TRUE ~ "OTROS"
  )) %>% 
  group_by(iep, gtproc_forma, tproc) %>% 
  summarise(cantidad = n()) %>% 
  arrange(desc(cantidad)) %>% 
  ungroup() %>% 
  left_join(jdos_lab %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  # left_join(tipos_proceso, by=c("tproc"="tipo_de_procesos")) %>% 
  mutate(tproc = ifelse(tproc =="-", "Total Organismo", tproc)) %>% 
  select(circunscripcion, organismo, grupo_proceso = gtproc_forma,
         tipo_proceso = tproc, everything(), -iep) 



ini_tproc <- inic %>% 
  group_by(tproc) %>% 
  summarise(cantidad = n())
