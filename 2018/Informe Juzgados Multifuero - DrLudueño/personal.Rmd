---
title: "Personal: Planta Ocupada"
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1.5cm
mainfont: Liberation Sans
sansfont: Liberation Sans
df_print: kable   
params:
   poblacion: "jdocco*"
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r}
  if(!exists("poblacion")) {
    organismoEXPR <- utils::glob2rx(params$poblacion)
    poblacion <- apgyeJusEROrganization::listar_organismos() %>% filter(grepl(organismoEXPR, organismo))
  }

```

\blandscape
### Personal

```{r}
personal(poblacion = poblacion) %>%
  personalPorCategoria(poblacion = poblacion) %>% 
    filter(!grepl("JUEZ", categoria)) %>% 
    tidyr::spread(key = categoria, value = cantidad ) %>% 
    outputTable(caption = "Planta Ocupada")  %>% 
    row_spec(0, angle = 90)
```
\elandscape

