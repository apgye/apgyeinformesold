# modelo 
audifam <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02", desagregacion_mensual = TRUE) {
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIF") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(fea = dmy(fea)) %>% 
    filter(fea > data_interval_start, fea < data_interval_end) %>% 
    filter(esta == "2") %>%
    collect() %>% 
    mutate(tipo_audiencia = case_when(
      ta == "1" ~ "Control de Legalidad Internaciones/Externaciones",
      ta == "2" ~  "Escucha Niño/Niñas/Adolescentes",
      ta == "3" ~  "Escucha Personas c/Capacidad Restringida",
      ta == "4" ~  "Escucha al Denunciante Violencia Familiar o de Género",
      ta == "5" ~  "Escucha al Denunciado",
      ta == "6" ~  "Control de Legalidad Medidas Excepcionales",
      ta == "7" ~  "Adoptabilidad",
      ta == "8" ~  "Proceso de Vinculación",
      ta == "9" ~  "Guarda con Fines de Adopción",
      ta == "10" ~  "Adopción",
      ta == "11" ~  "Audiencia de Conciliación",
      ta == "12" ~  "Audiencia Art.70 Preliminar(ej. Filiación)",
      ta == "13" ~  "Audiencias en otros procesos voluntarios",
      ta == "14" ~  "Audiencia de Divorcio",
      ta == "15" ~  "Juicio Oral (proyectada)",
      ta == "16" ~  "Audiencia de Alimentos",
      # ta == "17" ~  "Otras",
      ta == "20" ~ "Formulación/Imputación", 
      ta == "21" ~ "Audiencia Conclusiva",
      ta == "22" ~ "Suspensión de Juicio a Prueba",
      ta == "23" ~ "Medidas",
      ta == "24" ~ "Remisión a Juicio",
      ta == "25" ~ "Juicio Abreviado",
      ta == "26" ~ "Remisión Judicial",
      ta == "27" ~ "Juicio Oral",
      ta == "28" ~ "Integración de Sentencia",
      # ta == "29" ~ "Otra",
      TRUE ~ "Otras"
    ))
 
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo") %>%
      group_by(organismo, mes, tipo_audiencia)
  } else {
    resultado <- resultado %>% 
      mutate(organismo = iep) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo") %>%
      group_by(organismo, tipo_audiencia)
  } 
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo_descripcion, mes) %>% 
      group_by(circunscripcion, organismo_descripcion, mes, tipo_audiencia) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(tipo_audiencia, cantidad) %>% 
      select(circunscripcion, organismo_descripcion, mes, everything()) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col")
  } else {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo_descripcion) %>% 
      group_by(circunscripcion, organismo_descripcion, tipo_audiencia) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(tipo_audiencia, cantidad) %>% 
      select(circunscripcion, organismo_descripcion, everything()) %>% 
      janitor::adorn_totals("col")
  }
  
  resultado
  
}


audifam_resumen <- function(audifamDF) {
  audifamDF %>%
    filter(!str_detect(circunscripcion, "Total")) %>%
    group_by(tipo_audiencia) %>%
    summarise(audiencias_realizadas = sum(cantidad, na.rm = T)) %>%
    ungroup() %>% 
    mutate(porcentaje = stringr::str_c(round( audiencias_realizadas / sum(audiencias_realizadas, na.rm = T) * 100), ' %')) %>% 
    arrange(desc(audiencias_realizadas))
}

audifam_resumen_xjdo <- function(audifamDF) {
  audifamDF %>%
    filter(!str_detect(circunscripcion, "Total")) %>%
    arrange(circunscripcion, organismo) %>% 
    group_by(circunscripcion, organismo, tipo_audiencia) %>%
    summarise(audiencias_realizadas = sum(cantidad, na.rm = T)) %>%
    do(janitor::adorn_totals(.)) %>%
    ungroup() %>% 
    mutate(circunscripcion = ifelse(circunscripcion == "-", "Total", circunscripcion)) %>% 
    ungroup() 
    
}
