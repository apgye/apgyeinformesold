---
title: "Análisis sobre Redistribución de Competencia"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r echo=FALSE, include=FALSE}
# Parámetros del informe
desagregacion_mensual = T
start_date <- "2018-02-01" 
end_date <- "2018-11-01" 
circ <- c("Concordia")
#circ <- c("Paraná", "Concordia", "Gualeguaychú", "Uruguay", "Gualeguay")

```


```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam")) 



if(is.na(circ)) {
  poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>% 
      filter(circunscripcion %in% circ)
  }


jdos_cco <- poblacion_total %>% 
  filter(grepl("cco", organismo)) %>% 
  filter(str_detect(materia, "cco"), tipo != "cam", categoria == "1" | is.na(categoria)) 


jdos_lab <- poblacion_total %>% 
  filter(str_detect(organismo, "lab"), tipo != "cam") 


```

## Juzgados Civiles-Comerciales y Laborales

Presentamos a continuación datos de casusas iniciadas en los Juzgados Civiles y Comerciales y Laborales de las Jurisdicciones de Paraná, Concordia, Gualeguaychú, Uruguay y Gualeguay. Jurisdicciones que tienen más de un organismos civil y al menos un juzgado laboral. De este grupo se excluyó el caso de Colón pues aunque comparte dichas características, el juzgado laboral fue creado este año y comenzó a informar datos estadísticas a partir de abril 2018.


<!-- ## Juzgados Civil y Comercial -->

<!-- #### Causas en trámite  -->

<!-- En esta tabla se muestran las causas en trámite totales y las causas en trámite activas por organismo. Las causas en trámite activas son aquellas que presetaron al menos un movimiento procesal en los últimos dos años.    -->

<!-- ```{r} -->
<!-- entramite(poblacion = jdos_cco) %>%  -->
<!--   enTramite_gral() %>% -->
<!--   outputTable(caption = "Causas en Trámite Totales y Causas en Trámite Activas)", -->
<!--                 row_group_label_position = "identity")   -->

<!-- ``` -->

\pagebreak

### Causas Iniciadas Agrupadas - `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura.

Para poder consultar los tipos de proceso que conforman cada grupo usted puede ver la próxima tabla.  Recomendamos esto particularmente para el conjunto de casos incluidos bajo la categoría **sin agrupar** que está conformada por tipos de proceso sin mas descripción que la que surge del sistema.

Finalmente, dado el estado crítico de las tablas de tipos de proceso del sistema LD y luego de la revisión efectuada con magistrados, se excluyeron (mediante la aplicación de patrones regulares de lenguaje) los siguientes casos: *OFICIO, EXHORTO, SUMARIO, COMPENSACION, MATRICULA DE COMERCIANTE, CIVIL Y COMERCIAL, PROCESO LABORAL, PROCESO DE FAMILIA, CONCURSOS Y QUIEBRAS, SENTENCIA, SOLICITA, CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO*.

```{r}
iniciados_cco(poblacion = jdos_cco,
              start_date = start_date,
              end_date = end_date) %>%
  iniciados_cco_comparativo() %>%
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) %>%
  landscape()

```


### Causas Iniciadas: Grafico `r getDataIntervalStr(start_date, end_date)`

```{r, fig.width=12, fig.height=4, echo=FALSE}

iniciados_cco(poblacion = jdos_cco,
  start_date = start_date,
  end_date = end_date) %>% 
  iniciados_cco_xg() %>% 
  inic_cco_graf_xg() 

```

\pagebreak

<!-- ### Causas Resueltas -todas las materias - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_cco( -->
<!--   poblacion = jdos_cco, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Resoluciones y Vencimientos") %>% -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape() -->

<!-- ``` -->

<!-- ### Sentencias por Tipo `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_cco_xtres( -->
<!--   poblacion = jdos_cco, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Sentencias por Tipo") %>% -->
<!--   row_spec(0, angle = 90)  -->
<!-- ``` -->

<!-- ### Audiencias Realizadas -->

<!-- En la siguiente tabla se muestran las audiencias realizadas e informadas por cada organismo en el período considerado. -->

<!-- ```{r} -->
<!-- audienciasCiviles( -->
<!--   poblacion = jdos_cco, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Audiencias Realizadas") %>% -->
<!--   column_spec(5, "3cm") %>%  -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->
<!-- ``` -->

<!-- ## Juzgados de Familia -->

<!-- #### Causas en trámite  -->

<!-- En esta tabla se muestran las causas en trámite totales y las causas en trámite activas que son aquellas que presetaron al menos un movimiento procesal en los últimos dos años.    -->

<!-- ```{r} -->
<!-- entramite(poblacion = jdos_fam) %>% resolverconvertidos() %>%  -->
<!--   enTramite_gral() %>%  -->
<!--   outputTable(caption = "Causas en Trámite Totales y Causas en Trámite Activas)", -->
<!--               row_group_label_position = "identity")  -->

<!-- ``` -->

<!-- ### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_todos( -->
<!--   poblacion = jdos_fam, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   kable(caption = "Causas Iniciadas", -->
<!--       align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(3, "12cm") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->
<!-- ``` -->

<!-- ### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_fam( -->
<!--   poblacion = jdos_fam, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Resoluciones y Vencimientos") %>% -->
<!--   row_spec(0, angle = 90)%>% -->
<!--   landscape() -->

<!-- ``` -->

<!-- \pagebreak -->

<!-- ### Audiencias por tipo - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->

<!-- audifam(jdos_fam,  -->
<!--         start_date = start_date, -->
<!--         end_date = end_date, -->
<!--         desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Audiencias Realizadas") %>% -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape() -->

<!-- ``` -->


<!-- ## Juzgados Paz 1 Categoría -->

<!-- #### Causas en trámite -->

<!-- En esta tabla se muestran las causas en trámite totales y las causas en trámite activas por organismo. Las causas en trámite activas son aquellas que presetaron al menos un movimiento procesal en los últimos dos años. -->

<!-- ```{r} -->
<!-- entramite(poblacion = jdos_paz) %>% enTramite_gral() %>% -->
<!--     outputTable(caption = "Causas en Trámite Totales y Causas en Trámite Activas)", -->
<!--                 row_group_label_position = "identity") -->

<!-- ``` -->


<!-- ### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_todos( -->
<!--   poblacion = jdos_paz, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   kable(caption = "Causas Iniciadas", -->
<!--       align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(3, "12cm") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->

<!-- ``` -->

<!-- ### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_paz( -->
<!--   poblacion = jdos_paz, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date,  -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--     outputTable(caption = "Resoluciones y Vencimientos") %>% -->
<!--     row_spec(0, angle = 90)  %>% -->
<!--   landscape() -->

<!-- ``` -->

<!-- \newpage -->

<!-- ## Juzgados Paz 2a. y 3a. Categoría -->

<!-- ### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_paz_23c( -->
<!--   poblacion = jdos_paz_23, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   kable(caption = "Causas Iniciadas", -->
<!--       align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(3, "12cm") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->

<!-- ``` -->


## Juzgados Laborales

En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura. Remitimos a las observaciones formuladas respecto de los órganos civiles. 


<!-- #### Causas en trámite -->

<!-- En esta tabla se muestran las causas en trámite totales y las causas en trámite activas por organismo. Las causas en trámite activas son aquellas que presetaron al menos un movimiento procesal en los últimos dos años. -->

<!-- ```{r} -->
<!-- entramite(poblacion = jdos_lab) %>% enTramite_gral() %>% -->
<!--     outputTable(caption = "Causas en Trámite Totales y Causas en Trámite Activas)", -->
<!--                 row_group_label_position = "identity") -->

<!-- ``` -->

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}

iniciados_lab(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date) %>%
  iniciados_lab_comparativo() %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) %>%
  landscape()
```


### Causas Iniciadas: Grafico `r getDataIntervalStr(start_date, end_date)`

```{r, fig.width=12, fig.height=4, echo=FALSE}

iniciados_lab(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date) %>% 
  iniciados_lab_xg() %>% 
  inic_lab_graf_xg() 

```

\pagebreak


## Tablas de Datos Primarios de Cada Organismos

### Civil


[//]: # (Causas iniciadas, para quitar knit_child -> #knit_child)
`r knit_child('causas_iniciadas.Rmd', envir=newenv(poblacion=jdos_cco, separar_pconocimiento=TRUE))`

### Laboral

[//]: # (Causas iniciadas, para quitar knit_child -> #knit_child)
`r knit_child('causas_iniciadas_lab.Rmd', envir=newenv(poblacion=jdos_lab, separar_pconocimiento=TRUE))`




<!-- ### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_lab( -->
<!--   poblacion = jdos_lab, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date,  -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Resoluciones y Vencimientos") %>% -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape()  -->

<!-- ``` -->

<!-- ### Sentencias por Tipo `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_lab_xtres( -->
<!--   poblacion = jdos_lab,  -->
<!--   start_date = start_date,  -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Sentencias por Tipo") %>% -->
<!--   row_spec(0, angle = 90)  -->
<!-- ``` -->


<!-- ### Audiencias Realizadas -->

<!-- En la siguiente tabla se muestran las audiencias realizadas e informadas por cada organismo en el período considerado. -->

<!-- ```{r} -->
<!-- audienciasLaborales( -->
<!--   poblacion = jdos_lab, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Audiencias Realizadas") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->
<!-- ``` -->


<!-- \newpage -->

<!-- ## Segunda Instancia Civil y Comercial -->

<!-- ### Causas Iniciadas - 1er semestre 2018 -->

<!-- ```{r} -->
<!-- iniciados_cam(poblacion = cam_civil) %>%  -->
<!--   outputTable(caption = "Causas Iniciadas") %>% -->
<!--   column_spec(3, width = "5cm") %>%  -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->

<!-- ``` -->


<!-- ### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_cco( -->
<!--   poblacion = cam_civil, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   operacion = "CADR2C", -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--     outputTable(caption = "Resoluciones y Vencimientos") %>% -->
<!--     row_spec(0, angle = 90) -->
<!-- ``` -->

<!-- \newpage -->

<!-- ## Segunda Instancia Laboral -->

<!-- ### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_cam(poblacion = cam_lab) %>%  -->
<!--   outputTable(caption = "Causas Iniciadas") %>% -->
<!--   column_spec(3, width = "5cm") %>%  -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->


<!-- ``` -->


<!-- ### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- resoluciones_lab( -->
<!--   poblacion = cam_lab, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   operacion = "CADR2L", -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--     outputTable(caption = "Resoluciones y Vencimientos") %>% -->
<!--     row_spec(0, angle = 90) -->
<!-- ``` -->


<!-- \pagebreak -->

<!-- # Anexo Metodológico -->

<!-- La información procesada para este informe surge de listados primarios de datos presentados por cada organismo de manera digital. Dichos listados primarios permanecen inalterados para consulta a tavés del módulo *Presentaciones*. -->



