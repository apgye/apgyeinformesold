---
title: "Primer Informe - Juzgados de Ejecuciones de Penas y Med.Seg."
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("exploracion.R")
source("utils.R")
```

```{r echo=FALSE, include=FALSE}
# Parámetros del informe

start_date = "2018-10-01"
end_date = "2018-12-01"

```


```{r echo=FALSE, include=FALSE}
# Poblacion
jdos_ejec <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(str_detect(organismo, "jdopep"))

```

# Introducción

Presento en este documento los primeros resultados del trabajo de registración y procesamiento estadístico de indicadores de los juzgados, con el fin de consolidar este nuevo sistema de mediciones aprobado por el STJER y subsanar los errores de registración hacia las próximas presentaciones de datos. 

En tal sentido, presentaré a continuación:

+ indicadores de causas iniciadas, causas archivadas, resoluciones dictadas y audiencias celebradas, y 
+ errores de registración detectados con el señalamiento específico del número de expediente donde se encuentra el error.

Quedo a su disposición, 

Lic. Sebastián Castillo

\pagebreak

### Causas Iniciadas por Tipo de Proceso- `r getDataIntervalStr(start_date, end_date)`


```{r}

inic_tb_prim(jdos_ejec, start_date = start_date, end_date = end_date) %>% 
  inic_xproc() %>% select(-organismo_descripcion) %>% 
  kable(caption = "Causas Iniciadas por tipo de Proceso", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(5, "8cm") %>%
  row_spec(0, angle = 90) 
```

\pagebreak

### Causas Iniciadas por Tipo de Pena - `r getDataIntervalStr(start_date, end_date)`

En este cuadro se pueden observar las causas iniciadas en el período informado agrupadas por organismo, mes y tipo de pena registrada en el sistema Lex-Doctor. 

Como puede advertirse por la presencia de celdas **"sin_dato"** en ambos organismos existen causas con errores de registración según las pautas fijadas en la Guía de Estadística para clasificar los tipos de pena (ver Guía pto.1 *¿Cómo se deben registrar las causas iniciadas?*, campo sobre "N.2ºIns" sobre Tipo/Modalidad de Pena).

En las próximas presentaciones se evaluará la corrección de este datos para lo cual le solicitamos corrija o efectúe una registración adecuada.


```{r}

inic_tb_prim(jdos_ejec,
             start_date = start_date,
             end_date = end_date) %>% 
  inic_xpena() %>% 
  kable(caption = "Causas Iniciadas por tipo de Pena", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
#  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```


### Causas Iniciadas: cambio en la codificación del campo "reincidencia"

En el procesamiento de la información se constata distintas pautas para registrar el dato de reincidencia, atento a que en la Guía de Estadística no contenía especificaciones en este punto. Por tal motivo, surge la oportunidad de mejorar esta información adoptando la siguiente clasificación (que empieza a regir para las próximas presentaciones):

Reincidente: Se registrará el caso de reincidencia según la siguiente codificación:   

+ 1 = "si es reincidente"    

+ 0 = "no es reincidente"  


### Causas Archivadas - `r getDataIntervalStr(start_date, end_date)`

Para este indicador no hay casos informados. 

Recordamos que es muy importante que el archivo de la causa por disposición del juez se traduzca en una registración del estado del legajo en el Lex-Doctor consignando en la carátula del expediente los siguientes datos:  

+ Fec. Finaliz.: Fecha de Archivo Judicial del Legajo, y 
+ Grupo: Cambiar el estado de "En Trámite" a "Archivado".


<!-- ```{r} -->
<!-- arch_tb_prim(jdos_ejec, -->
<!--              start_date = start_date, -->
<!--              end_date = end_date) %>%  -->
<!--   kable(caption = "Causas Iniciadas por tipo de Pena", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!-- #  column_spec(2, "4cm") %>% -->
<!--   row_spec(0, angle = 90)  -->

<!-- ``` -->

\pagebreak


### Causas Resueltas  `r getDataIntervalStr(start_date, end_date)` 

```{r}

resoluciones_tb_prim(jdos_ejec, start_date, end_date) %>% 
  resol_xtresresul() %>% select(-organismo_descripcion, -iep) %>% 
  outputTable(caption = "Resoluciones Dictadas") %>%
  row_spec(0, angle = 90) 

```

\pagebreak

### Audiencias Realizadas  `r getDataIntervalStr(start_date, end_date)` 

```{r}

audiencias_tb_prim(jdos_ejec, start_date, end_date) %>% 
  audic_xtaud() %>% select(-organismo_descripcion, -iep) %>% 
  outputTable(caption = "Audiencias Realizadas") %>%
  row_spec(0, angle = 90) 
```

Como puede advertirse por la presencia de celdas **"sin_dato"** en el juzgado de Parana existen causas con errores de registración según los tipos de audiencias fijados en la Guía de Estadística. Los casos específicos identificados por su número de expediente son:

```{r}
ERROR_sin_tipoaudiencia
```

En las próximas presentaciones se evaluará la corrección de este datos para lo cual le solicitamos efectúe una registración adecuada.


Lic. Sebastián Castillo
