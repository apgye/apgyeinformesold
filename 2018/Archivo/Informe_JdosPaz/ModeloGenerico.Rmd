---
title: "Informe"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r echo=FALSE, include=FALSE}
# Parámetros del informe
desagregacion_mensual = T
start_date <- "2018-02-01" 
end_date <- "2018-11-01" 
circ <- NA
```


```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam")) 

if(is.na(circ)) {
  poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>% 
      filter(circunscripcion %in% circ)
  }

jdos_cco <- poblacion_total %>% 
  filter(grepl("cco", organismo)) %>% 
  filter(str_detect(materia, "cco|eje|cqb"), tipo != "cam", categoria == "1" | is.na(categoria)) %>% 
  filter(materia == "cco")

jdos_paz <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_paz_23 <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria != "1" | is.na(categoria))

```


<!-- ### Causas Iniciadas Agrupadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura. -->

<!-- Para poder consultar los tipos de proceso que conforman cada grupo usted puede ver la próxima tabla.  Recomendamos esto particularmente para el conjunto de casos incluidos bajo la categoría **sin agrupar** que está conformada por tipos de proceso sin mas descripción que la que surge del sistema. -->

<!-- Finalmente, dado el estado crítico de las tablas de tipos de proceso del sistema LD y luego de la revisión efectuada con magistrados, se excluyeron (mediante la aplicación de patrones regulares de lenguaje) los siguientes casos: *OFICIO, EXHORTO, SUMARIO, COMPENSACION, MATRICULA DE COMERCIANTE, CIVIL Y COMERCIAL, PROCESO LABORAL, PROCESO DE FAMILIA, CONCURSOS Y QUIEBRAS, SENTENCIA, SOLICITA, CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO*. -->

<!-- ```{r} -->
<!-- iniciados_cco(poblacion = jdos_cco, -->
<!--               start_date = start_date, -->
<!--               end_date = end_date) %>% -->
<!--   iniciados_cco_comparativo() %>% -->
<!--   kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(2, "4cm") %>% -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape() -->

<!-- ``` -->


<!-- ### Causas Iniciadas: Grafico `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r, fig.width=12, fig.height=4, echo=FALSE} -->

<!-- iniciados_cco(poblacion = jdos_cco, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   iniciados_cco_xg() %>% -->
<!--   inic_cco_graf_xg() -->

<!-- ``` -->

<!-- \pagebreak -->

## Juzgados Paz 1 Categoría

```{r}
iniciados_paz_1c(poblacion = jdos_paz,
              start_date = start_date,
              end_date = end_date) %>% 
  iniciados_paz_comparativo() %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```


## Juzgados Paz 2a. y 3a. Categoría


```{r}
iniciados_paz_23c(poblacion = jdos_paz_23,
              start_date = start_date,
              end_date = end_date) %>% 
  iniciados_paz_comparativo() %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```


Lic. Sebastián Castillo

\pagebreak

## Juzgados Paz 2a. y 3a. Categoría - Detalla sobre Trámites Turno Voluntario


```{r}
iniciados_paz_23c(poblacion = jdos_paz_23,
              start_date = start_date,
              end_date = end_date) %>% 
  filter(grupo_proceso == "tramite") %>% 
  select(-grupo_proceso) %>% 
  group_by(circunscripcion, organismo) %>% 
  do(janitor::adorn_totals(.)) %>% 
  kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) 

```



<!-- ## Listados Primarios de Procesos y Trámites -->

<!-- ### Juzgados 1 Categoría - Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_paz_1c( -->
<!--   poblacion = jdos_paz, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   kable(caption = "Causas Iniciadas", -->
<!--       align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(3, "12cm") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->

<!-- ``` -->

<!-- \pagebreak -->

<!-- ## Juzgados Paz 2a. y 3a. Categoría - Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_paz_23c( -->
<!--   poblacion = jdos_paz_23, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   kable(caption = "Causas Iniciadas", -->
<!--       align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(3, "12cm") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->

<!-- ``` -->





