---
title: "Informe General sobre Oralidad: Segunda Presentación"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
options(knitr.kable.NA = '')
```

```{r}
source("informe.R")
source("utils.R")
```


```{r}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tibble)
library(lubridate)
```


# Meses Informados: `r getDataIntervalStr(start_date, end_date)`

# Nueva Presentación del 07/12/18

En este informe se subsanaron errores de registración por parte de organismos informantes y se incorpora un detalle de 'motivos' para la no registración de fecha de programación de audienicas de Vista de Causa. 

# Aclaraciones Metodológicas

Remito a informes precedentes. 

\pagebreak

## Datos Generales del conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

En esta tabla se presenta información sobre el conjunto de procesos sometidos al Protocolo de Oralidad e informados al área en los datos presentados de julio a la fecha.

Para la determinación de esos casos, en esta nueva presentación, se excluyeron procesos a los que no se aplica el Protocolo de Procesos por Audiencia (AC.Gral.18/18) según definición de cada magistrado. A partir del mes de noviembre se computarán los procesos incluidos en el protocolo tomando como referencia el caso con Audiencia Preliminar fijada. 

En el computo de cada indicador se empleó el siguiente procedimiento:

+ **cantidad procesos en oralidad informados**: casos con audiencia preliminar fijada. Entre julio y octubre 2018 los casos sometidos a oralidad se consolidaron a través de declaración de los organismos.
+ **cantidad_preliminares_realizadas**: conjunto de casos con audiencia preliminar realizada en el período considerado. Se excluyen del conteo los casos duplicados.   
+ **cantidad_vista_causa_programadas**: conjunto de casos con audiencia de vista de causa programada e informada al momento de celebración de la preliminar. Se excluyen del conteo los casos duplicados.   
+ **cantidad_vista_causa_realizadas**: conjunto de casos con audiencia de vista de causa celebrada a partir de *septiembre 2018*. Se excluyen del conteo los casos duplicados y los casos con multiples registros de audiencia para un mismo mes.   
+ **cantidad_conciliaciones_enAPoAVC**: conjunto de casos con conciliación total registrada en audiencia preliminar o en audiencia de vista de causa.        
+ **cantidad_sentencias**: conjunto de casos con sentencia dictada en procesos sometidos al Protocolo de Oralidad que se informaron al área.   


```{r}
kable(oralidad_resumen, caption = "Resumen", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90)
```

**ATENCION**: En ANEXO 1 se agrega la tabla completa empleada para el resumen precedente. 

\pagebreak

# Audiencias de Vista de Causa Programadas a partir de `r end_date`

```{r}

oralidad_vista_causa <- oralidad_consolidado_parcial %>% 
  select(circunscripcion, organismo_descripcion, nro_expte, fprogramada_vista_causa) %>% 
  filter(!is.na(fprogramada_vista_causa) & fprogramada_vista_causa >= end_date) %>% 
  mutate(mes = as.character(lubridate::month(fprogramada_vista_causa)), 
         año = as.character(lubridate::year(fprogramada_vista_causa))) %>%
  group_by(circunscripcion, organismo_descripcion, año, mes) %>% 
  summarise(cantidad_audiencias = n(), 
            nros_expte_y_fechaAudiencia = paste(nro_expte, fprogramada_vista_causa,
                                sep = ": ", collapse = "; ")) %>% 
  ungroup() %>% 
  arrange(circunscripcion, organismo_descripcion)  %>% 
  janitor::adorn_totals("row")

  
outputTable(oralidad_vista_causa, 
            caption = "Vista de Causa Programadas por organismo") %>%
  column_spec(6, "5cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

# kable(oralidad_vista_causa, caption = "Vista de Causa Programadas por organismo",
#       align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                 full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
#   column_spec(5, "8cm") %>%
#   row_spec(0, angle = 90) %>% 
#   landscape() 

```


# Procesos sin programación de Vista de Causa: sus motivos

En esta tabla se pueden consultar las respuestas recibidas por cada organismo a la fecha respecto de la solicitud de aclaración enviada por el grupo de coordinadoras de oralidad sobre la falta de declaración de fecha programada para la Audiencia de Vista de Causa. 

```{r}

outputTable(respuestasobs_motivosnoVC, 
            caption = "Vista de Causa Programadas por organismo") %>%
  column_spec(2, "3cm") %>%  column_spec(8, "4cm") %>% column_spec(6, "2cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()

# kable(oralidad_vista_causa, caption = "Vista de Causa Programadas por organismo",
#       align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                 full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
#   column_spec(5, "8cm") %>%
#   row_spec(0, angle = 90) %>% 
#   landscape() 

```


# Procesos sin programación de Vista de Causa: falta de datos registrados

En esta tabla se observa por organismo procesos (indentificados por el número de ingreso) que tuvieron audiencia preliminar y no registraron programación de audiencia de vista de causa.

```{r}

outputTable(sinprogVC, 
            caption = "Sin datos registrados sobre programación de Vista de Causa") %>%
  #column_spec(8, "4cm") %>% column_spec(6, "2cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()

# kable(oralidad_vista_causa, caption = "Vista de Causa Programadas por organismo",
#       align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                 full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
#   column_spec(5, "8cm") %>%
#   row_spec(0, angle = 90) %>% 
#   landscape() 

```

\pagebreak

# Encuestas de Satisfacción (implementadas a partir del 01/08/18)

La escala de medición de "Satisfacción" para todas las variables consideradas (tanto en la Audiencia Preliminar como la de Vista de Causa) es ordinal y se conforma de la siguiente manera:

4 = Muy Bien,  
3 = Bien,   
2 = Regular, y   
1 = Mal

## Encuestas de Satisfacción sobre Audiencia Preliminar

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*depurac_prueba* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en la depuración de la prueba en este proceso?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
kable(ap_sat_ab_resumen, caption = "Satisfaccion de Abogados según Dimensión Evaluada",
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()


```

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia?  

```{r}
kable(ap_sat_us_resumen, caption = "Satisfaccion de Usuarios según Dimensión Evaluada",
       align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()

```


## Encuestas de Satisfacción sobre Audiencia de Vista de Causa

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*plazo_resol* = ¿Cuál es su grado de satisfacción respecto del plazo de resolución de este proceso?
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
kable(avc_sat_ab_resumen, caption = "Satisfaccion de Abogados según Dimensión Evaluada",
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()


```

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*duracion* = ¿Cuál es su grado de satisfacción respecto de la duración de este proceso?
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia?  

```{r}
kable(avc_sat_us_resumen, caption = "Satisfaccion de Usuarios según Dimensión Evaluada",
       align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()

```

Lic. Sebastián Castillo
Área de Planificación Gestión y Estadística    
`r Sys.Date()`


\pagebreak

# ANEXO 1

## Datos particulares del conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

```{r}
kable(oralidad_consolidado_parcial, caption = "Tabla General", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  column_spec(2, "3cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape() 
```



