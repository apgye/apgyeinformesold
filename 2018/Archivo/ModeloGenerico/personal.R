personal <- function(poblacion, start="2018-09-01", end="2018-10-01") {
  
  DB_PROD() %>%
    apgyeData(GHSTRD) %>%
    apgyeDSL::interval(!! start, !! end) %>%
    activate(tbs_prim_GHSTRD_planta) %>%
      left_join(tbl(DB_PROD(), "tabla_personal")) %>%
      filter(organismoAPGyE %in% !! poblacion$organismo) %>%
    deactivate() %>%
    activate(tbs_prim_GHSTRD_planta_ocupada) %>%
      left_join(tbl(DB_PROD(), "tabla_personal")) %>%
      filter(organismoAPGyE %in% !! poblacion$organismo) %>%
    deactivate() %>%
    group_by(organismoAPGyE) %>% 
    process()
}

personalPorCategoria <- function(poblacion, personal) {
  personal %>%
    activate(planta_ocupada_revista) %>% 
    rename(iep = organismoAPGyE) %>% 
    left_join(poblacion %>% select(-categoria), by=c("iep"="organismo")) %>% 
    select(circunscripcion, organismo=organismo_descripcion, everything()) %>% 
    filter(!is.na(categoria)) %>% 
    group_by(circunscripcion, organismo, categoria) %>% 
      summarise(cantidad = sum(cantidad, na.rm = TRUE))
      
}