entramite <- function(poblacion) {
  hace_10_años = Sys.Date() - lubridate::years(10)
  hace_5_años = Sys.Date() - lubridate::years(5)
  hace_2_años = Sys.Date() - lubridate::years(2)
  # hace_2_años = lubridate::make_date(lubridate::year(Sys.Date()) - 1, 1, 1)
  
  entramite <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData("CETAL_XL") %>% 
    filter(iep %in% poblacion$organismo) %>% 
    filter(!is.na(finicio), !is.na(fmov)) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(fmov = dmy(fmov)) %>%
    filter(fmov < !!Sys.Date()) %>% 
    group_by(nro, caratula) %>%   
    collect() %>%                                 # se quitan causas que se han movido entre organismos
    filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
    ungroup()  %>%                                # para los casos en que se hacen pases por competencia
    select(-movt)  %>%
    group_by(iep, nro) %>% filter(row_number() == n()) %>%  ungroup() %>%   # quitando repetidos
    mutate(con_resolucion = grepl("ONline Publico|ONLINE Público", fmov_treg_fres)) %>% 
    mutate(año_ult_mov = lubridate::year(fmov)) %>% 
    filter(!is.na(año_ult_mov)) %>% 
    mutate(año_ult_mov = ifelse(año_ult_mov < 2008, "mas 10 años", as.character(año_ult_mov) )) %>% 
    mutate(ultimo_movimiento = ifelse(fmov < !!hace_2_años, "mayor 2 años", "menor 2 años")) %>% 
    left_join(poblacion %>% 
                select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep", copy=TRUE) %>% 
    select(circunscripcion, organismo, everything())
  entramite
}
enTramitePorAño <- function(causasEnTramite) {
  causasEnTramite %>%
    group_by(circunscripcion, organismo, año_ult_mov, con_resolucion) %>% 
    summarise(cantidad = n()) %>% 
    collect() %>% 
    ungroup() %>% 
    mutate(con_resolucion = ifelse(con_resolucion, "con registro", "sin registro")) %>% 
    tidyr::spread(key=año_ult_mov, value=cantidad) %>% 
    janitor::adorn_totals(where = "col") %>% 
    rename("res. mesa virtual" = con_resolucion)
}
enTramitePorTipoProcesoUltimos2Años <- function(causasEnTramite) {
  causasEnTramite %>%
    filter(ultimo_movimiento == "menor 2 años") %>% 
    group_by(circunscripcion, organismo, tproc) %>% 
    summarise(cantidad = n()) %>% 
    collect() %>% 
    arrange(circunscripcion, organismo, desc(cantidad)) %>% 
    ungroup() %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(tproc=ifelse(tproc=="-", "Total", tproc), circunscripcion=.$circunscripcion[1],  organismo=.$organismo[1])) %>% 
      ungroup() %>% 
    rename("tipo proceso" = tproc) 
}