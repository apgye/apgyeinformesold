
audienciasLaborales <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02", desagregacion_mensual = TRUE) {
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIL") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(fea = dmy(fea)) %>% 
    filter(fea > !!start_date, fea < !!end_date) %>% 
    filter(esta == "2") %>% # no canceladas
    collect() %>% ungroup() %>% 
    mutate(ra = toupper(ra), ta = toupper(ta)) %>%
    distinct(iep, nro, caratula, tproc, fea, .keep_all = T) %>% # Filtro Casos Duplicados
    mutate(ta = case_when(ta == "P" ~ "conciliacion", 
                          ta == "V" ~ "vista_causa", 
                          ta == "O" ~ "otras", 
                          TRUE ~ "sd")) %>% 
    mutate(ra = case_when(
      ra == "T" ~ "Conciliacion_Total",
      ra == "P" ~ "Conciliacion_Parcial",
      ra == "S" ~ "Sin_Conciliacion", 
      TRUE ~ "sin_dato")) %>% 
    mutate(tipo = ta) 
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(organismo = iep, mes = lubridate::month(fea, label=T, abbr = F))  %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo")
  } else {
    resultado <- resultado %>% 
      mutate(organismo = iep) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo")
  } 
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo_descripcion, mes) %>% 
      group_by(circunscripcion, organismo_descripcion, mes, tipo) %>% 
      summarise(cantidad = n()) %>%
      tidyr::spread(tipo, cantidad) %>% 
      select(circunscripcion, organismo_descripcion, mes, conciliacion, vista_causa,
             otras, everything()) %>% 
      group_by(circunscripcion, organismo_descripcion) %>% 
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>% 
      janitor::adorn_totals("col")
    } else {
      resultado <- resultado %>% 
        arrange(circunscripcion, organismo_descripcion) %>% 
        group_by(circunscripcion, organismo_descripcion, tipo) %>% 
        summarise(cantidad = n()) %>%
        tidyr::spread(tipo, cantidad) %>% 
        select(circunscripcion, organismo_descripcion, conciliacion, vista_causa, 
               otras, everything()) %>% 
        janitor::adorn_totals("col")
    }
  
  resultado
}