---
title: "Informe"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r echo=FALSE, include=FALSE}
# Parámetros del informe
desagregacion_mensual = T
start_date <- "2018-02-01" 
end_date <- "2018-12-01" 
circ <- c("Chajarí")
```


```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam")) 

if(is.na(circ)) {
  poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>% 
      filter(circunscripcion %in% circ)
  }

jdos_cco <- poblacion_total %>% 
  filter(grepl("cco", organismo)) %>% 
  filter(str_detect(materia, "cco|eje|cqb"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_lab <- jdos_cco


```

\pagebreak


## Juzgados Civil y Comercial con comptencia Laboral


<!-- ### Causas Iniciadas  -->

<!-- En esta tabla se muestras grupos de tipos de proceso que surgen de las tablas de Lex-Doctor y que procuran ordenar la información para su fácil lectura. -->


<!-- ```{r} -->
<!-- iniciados_cco(poblacion = jdos_cco, -->
<!--               start_date = start_date, -->
<!--               end_date = end_date) %>% -->
<!--   iniciados_cco_comparativo() %>% -->
<!--   kable(caption = "Causas Iniciadas Civil: Febrero-Noviembre", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(2, "4cm") %>% -->
<!--   row_spec(0, angle = 90)  -->

<!-- ``` -->



### Causas Iniciadas 2018 

```{r}
inic <- inic_todos <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CETAL_XL") %>% 
  filter(iep %in% jdos_cco$organismo) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(fmov = dmy(fmov)) %>%
  filter(finicio >= start_date, finicio < end_date ) %>% 
  group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
  filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
  ungroup()  %>%                                # para los casos en que se hacen pases por competencia
  select(iep, nro, caratula, tproc, finicio)  %>% 
  union(
    DB_PROD() %>% 
      apgyeDSL::apgyeTableData("CINC1L") %>% 
      filter(iep %in% jdos_cco$organismo) %>% 
      filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(!is.na(finicio)) %>% 
      filter(finicio >= start_date, finicio < end_date ) %>% 
      select(iep, nro, caratula, tproc, finicio)  
  ) %>% 
  collect() %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  left_join(jdos_cco %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  select(circunscripcion, organismo, tipo_proceso = tproc, everything(), -iep) %>% 
  mutate(fuero = ifelse(str_detect(nro, '"L"'), "Laboral", "Civil")) %>% 
  group_by(organismo, fuero, tipo_proceso) %>% 
  summarise(cantidad = n()) 

inic %>% 
  group_by(organismo, fuero) %>% 
  summarise(cantidad_iniciados = sum(cantidad)) %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90)
```

\pagebreak

### Causas Iniciadas por Tipo de Proceso

```{r}
inic <- inic_todos <- DB_PROD() %>% 
  apgyeDSL::apgyeTableData("CETAL_XL") %>% 
  filter(iep %in% jdos_cco$organismo) %>% 
  filter(!is.na(finicio), !is.na(fmov)) %>% 
  filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(fmov = dmy(fmov)) %>%
  filter(finicio >= start_date, finicio < end_date ) %>% 
  group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
  filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
  ungroup()  %>%                                # para los casos en que se hacen pases por competencia
  select(iep, nro, caratula, tproc, finicio)  %>% 
  union(
    DB_PROD() %>% 
      apgyeDSL::apgyeTableData("CINC1L") %>% 
      filter(iep %in% jdos_cco$organismo) %>% 
      filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      filter(!is.na(finicio)) %>% 
      filter(finicio >= start_date, finicio < end_date ) %>% 
      select(iep, nro, caratula, tproc, finicio)  
  ) %>% 
  collect() %>% 
  distinct(iep, nro, .keep_all = TRUE) %>% 
  left_join(jdos_cco %>% 
              select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
  select(circunscripcion, organismo, tipo_proceso = tproc, everything(), -iep) %>% 
  mutate(fuero = ifelse(str_detect(nro, '"L"'), "Laboral", "Civil")) %>% 
  group_by(organismo, fuero, tipo_proceso) %>% 
  summarise(cantidad = n()) %>% 
  do(janitor::adorn_totals(.))

inic %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(2, "4cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```


