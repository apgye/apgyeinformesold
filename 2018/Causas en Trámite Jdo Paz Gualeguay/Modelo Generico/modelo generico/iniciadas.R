## Iniciados

iniciados_todos <- function(poblacion, start_date="2018-02-01", end_date = "2018-07-01") {
  
  #tipos_proceso <- apgyeOperationsJusER::Tipo_procesos %>% filter(materia == !!materia) %>% select(tipo_de_procesos,proceso_conocimiento)
  
  inic <- DB_PROD() %>% 
    apgyeDSL::apgyeTableData("CETAL_XL") %>% 
    filter(iep %in% poblacion$organismo) %>% 
    filter(!is.na(finicio), !is.na(fmov)) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(finicio = dmy(finicio)) %>%
    mutate(fmov = dmy(fmov)) %>%
    filter(finicio >= start_date, finicio < end_date ) %>% 
    group_by(nro, caratula) %>%                   # se quitan causas que se han movido entre organismos
    filter(fmov == max(fmov, na.rm = TRUE)) %>%   # esto se hace con el fin de remover duplicados
    ungroup()  %>%                                # para los casos en que se hacen pases por competencia
    select(iep, nro, caratula, tproc, finicio)  %>% 
    union(
      DB_PROD() %>% 
        apgyeDSL::apgyeTableData("CINC1C") %>% 
        filter(iep %in% poblacion$organismo) %>% 
        filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
        mutate(finicio = dmy(finicio)) %>%
        filter(!is.na(finicio)) %>% 
        filter(finicio >= start_date, finicio < end_date ) %>% 
        select(iep, nro, caratula, tproc, finicio)  
    ) %>% 
    collect() %>% 
    distinct(iep, nro, .keep_all = TRUE) %>% 
    filter(!str_detect(tproc, "RUBRICA|FORMULARIO")) %>% 
    group_by(iep, tproc) %>% 
    summarise(cantidad = n()) %>% 
    arrange(desc(cantidad)) %>% 
    do(janitor::adorn_totals(.)) %>% 
    ungroup() %>% 
    left_join(poblacion %>% 
                select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>% 
    mutate(tproc = ifelse(tproc =="-", "Total Organismo", tproc)) %>% 
    select(circunscripcion, organismo, tipo_proceso = tproc, everything(), -iep)
} 




iniciadosCITIPJ_1er_semestre <- function(poblacion) {
  DB_PROD() %>% 
    apgyeTableData("CITIPJ") %>% 
    apgyeDSL::interval(2018-01-01, 2018-07-01) %>% 
    filter(iep %in% poblacion$organismo) %>%
    collect() %>% 
    mutate(nivel = stringr::str_count(`Tipo de proceso`, "     ")) %>%
    tidyr::separate( `Tipo de proceso`, into = c("mat", "proc"),
                     sep = "     "  )  %>%
    mutate(mat = if_else(mat=="", NA_character_ ,mat)) %>%
    tidyr::fill(mat) %>% filter(nivel == 1) %>%
    mutate(cantidad = as.integer(proc)) %>%
    group_by(iep, mat, proc) %>%
    summarise(cant_procesos = sum(as.integer(Procesos), na.rm = T)) %>%
    ungroup() %>%
    left_join(poblacion[, c("organismo", "organismo_descripcion",
                            "circunscripcion")], by = c("iep" = "organismo")) %>%
    ungroup() %>%
    select(circunscripcion, organismo_descripcion, organismo = iep, everything()) %>%
    mutate(organismo_descripcion = str_sub(organismo_descripcion, 1, 16),
           mat = str_sub(mat, 1,3)) %>%
    select(circunscripcion, organismo = organismo_descripcion, materia = mat, "tipo proceso" = proc, cant_procesos) %>%
    arrange(circunscripcion, organismo, materia, desc(cant_procesos)) %>%
    janitor::adorn_totals("row")
}





