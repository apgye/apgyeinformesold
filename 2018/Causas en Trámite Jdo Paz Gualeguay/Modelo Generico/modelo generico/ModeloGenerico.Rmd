---
title: "Informe"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r echo=FALSE, include=FALSE}
# Parámetros del informe
desagregacion_mensual = T
start_date <- "2018-02-01" 
end_date <- "2018-11-01" 
```

```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam"))

jdos_paz <- poblacion_total %>% 
  filter(organismo == "jdopaz0000gay")

```

## Juzgados Paz 1 Categoría

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados_todos(
  poblacion = jdos_paz,
  start_date = start_date,
  end_date = end_date) %>%
  kable(caption = "Causas Iniciadas",
      align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(3, "10cm") %>%
  row_spec(0) 
```

