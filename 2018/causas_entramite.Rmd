---
title: Causas en Trámite
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1.5cm
mainfont: Liberation Sans
sansfont: Liberation Sans
df_print: kable   
params:
   poblacion: "jdopaz0000gay"
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r}
  if(!exists("poblacion")) {
    organismoEXPR <- utils::glob2rx(params$poblacion)
    poblacion <- apgyeJusEROrganization::listar_organismos() %>% filter(grepl(organismoEXPR, organismo))
  }

```


#### Causas en trámite en los últimos 2 años

En esta tabla se muestran las causas por tipo de proceso que registraron al menos un movimiento procesal en los últimos dos años. Se excluyeron de los tipos de proceso informados los Oficios y Exhortos, y los trámites del Turno Voluntario.

```{r}
entramite(poblacion = poblacion) %>%
  enTramitePorTipoProcesoUltimos2Años() %>%
    outputTable(caption = "Causas en Trámite, con mov procesal en los últimos 2 años por tipo de proceso (a agosto 2018)",
                row_group_label_position = "identity") %>% 
    column_spec(2, "3cm") %>% 
    column_spec(3, "8cm")
  
```

\blandscape

### Causas en Trámite: Total según Base de Datos del Organismo

En esta tabla se muestran las causas en trámite en el juzgado considerando el año de su último movimiento procesal registrado. Además de eso, desagregamos las causas según se detecte si la causa presenta resolución publicada en mesa virtual o no, ello como *señalización* del estado aproximado de las causas.

```{r}
entramite(poblacion = poblacion) %>%
  enTramitePorAño() %>%
    outputTable(caption = "Causas en Trámite, según último movimiento procesal (a agosto 2018)")  %>% 
    row_spec(0, angle = 45)
```


Lic. Sebastián Castillo

\elandscape

