newenv <- function(...) {
  envinitialization <- rlang::enexprs(...)
  env = new.env()
  for(var in names(envinitialization)) {
    env[[var]] = eval(envinitialization[[var]])
  }
  env
}

outputTable <- function (table, caption, row_group_label_position = "identity") {
  if(row_group_label_position == "identity") {
    total_rows <-  c(which(table[, 1] == "Total"), which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  } else {
    total_rows <-  c(which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  }
  table %>% rename_all(.funs = stringr::str_replace_all, pattern = "_", replacement=" ") %>% 
    kable("latex", caption = caption,
          align = 'c', longtable = TRUE, booktabs = T ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), latex_options = c("repeat_header"),
                  full_width = F, font_size = 10) %>% 
    row_spec(total_rows, bold = T) %>% 
    collapse_rows(columns = 1:2, row_group_label_position = row_group_label_position)
  
}

getDataIntervalStr <- function(data_interval_start, data_interval_end){
  year <- lubridate::year(data_interval_start);
  year_end <- lubridate::year(data_interval_end);
  month <- lubridate::month(data_interval_start);
  month_end <- lubridate::month(lubridate::as_date(data_interval_end)  - lubridate::days(1));
  monthDiff <- lubridate::interval(data_interval_start %>% as.Date(), data_interval_end %>% as.Date()) %/% months(1)
  period <- format(lubridate::make_date(year, month, 1), '%B %Y')
  
  period[monthDiff == 12] <- paste("anual", year[monthDiff == 12])
  period[monthDiff < 12] <- paste(format(lubridate::make_date(year, month, 1), '%B'), '-', format(lubridate::make_date(year, month_end, 1), '%B'), year[monthDiff < 12])
  period[monthDiff > 12] <- paste(format(lubridate::make_date(year, month, 1), '%B %Y'), '-', format(lubridate::make_date(year_end, month_end, 1), '%B %Y'))
  
  period[monthDiff == 6 & month == 1] <- paste("primer semestre", year[monthDiff == 6 & month == 1])
  period[monthDiff == 6 & month == 7] <- paste("segundo semestre", year[monthDiff == 6 & month == 7])
  period[monthDiff == 1] <- paste(format(lubridate::make_date(year[monthDiff == 1], month[monthDiff == 1], 1), '%B %Y'))
  
  
  period
}