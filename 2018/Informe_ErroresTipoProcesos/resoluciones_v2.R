
# Procesa Resoluciones en órganos civiles - todas las materias -

resoluciones_cco <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo)
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }

  resultado <- resultado %>% 
    process() %>%
    .$result %>%
    ungroup()
 
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, res_luego2venc, sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego2venc", replacement="luego_2do_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    mutate(resol_vencidas = stringr::str_c(round((luego_1er_venc + luego_2do_venc)/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           "luego 2do Vencim" = luego_2do_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas")
  
  resultado
}

resoluciones_cco_xtres <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("CADR1C") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(fres = dmy(fres)) %>%  mutate(fdesp = dmy(fdesp)) %>% 
    mutate(finicio = dmy(finicio)) %>% mutate(fvenc1 = dmy(fvenc1)) %>% 
    filter(!is.na(fres)) %>% 
    filter(toupper(as) == "S") %>% 
    collect() %>% 
    filter(fres >= data_interval_start & fres < data_interval_end) %>% 
    filter(!tres %in% c("0")) %>% 
    ungroup() %>% 
    mutate(tipo_resolucion = case_when(
      tres == "1" ~ "Conciliación",
      tres == "2" ~ "Transacción", 
      tres == "3" ~ "Caducidad",
      tres == "4" ~ "Desistimiento",
      tres == "5" ~ "Allanamiento",
      tres == "6" ~ "Incompetencia",
      tres == "7" ~ "Excepción",
      tres == "8" ~ "Sentencia monitoria sin oposición",
      tres == "9" ~ "Sentencia monitoria con oposición",
      tres == "10" ~ "Sent.Definitiva", # en código otra se registraba sent.def
      tres == "11" ~ "Rechazo “In Limine”",
      #Códigos Específicos para resoluciones en Procesos Sucesorios (a partir del 01/11/18)
      tres == "31" ~ "Sucesorio:Declarat.Herederos",
      tres == "32" ~ "Sucesorio:Ampliac./Rect.Declarat.Her.",
      tres == "33" ~ "Sucesorio:Aprobac.Testamento",
      tres == "34" ~ "Sucesorio:Inventario Avalúo",
      tres == "35" ~ "Sucesorio:Partic.yAdjudic.",
      tres == "36" ~ "Sucesorio:Herencia Vacante",
      tres == "37" ~ "Sucesorio:otras resoluciones",
      TRUE ~ "Sent.Definitiva")) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = c("iep" = "organismo")) %>% 
    group_by(circunscripcion, organismo_descripcion, tipo_resolucion) %>% 
    summarise(cantidad = n()) %>% 
    #filter(!tipo_resolucion %in% c( "Incompetencia", "Excepción")) %>%
    do(janitor::adorn_totals(.)) %>% 
    ungroup() 
  
  resultado
  
}


# Proceso Resoluciones Laborales 

resoluciones_lab <- function(poblacion, operacion = "CADR1L", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo)
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }
  
  resultado <- resultado %>% 
    process() %>%
    .$result %>%
    ungroup()
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    mutate(resol_vencidas = stringr::str_c(round(luego_1er_venc/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas")
  
  resultado
}


resoluciones_lab_xtres <- function(poblacion, operacion = "CADR1L", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData("CADR1L") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(fres = dmy(fres)) %>%  mutate(fdesp = dmy(fdesp)) %>% 
    mutate(finicio = dmy(finicio)) %>% mutate(fvenc = dmy(fvenc)) %>% 
    filter(!is.na(fres)) %>% 
    filter(toupper(as) == "S") %>% 
    collect() %>% 
    filter(fres >= data_interval_start & fres < data_interval_end) %>% 
    filter(!tres %in% c("0")) %>% 
    ungroup() %>% 
    mutate(tipo_resolucion = case_when(
      tres == "1" ~ "Conciliación",
      tres == "2" ~ "Transacción", 
      tres == "3" ~ "Caducidad",
      tres == "4" ~ "Desistimiento",
      tres == "5" ~ "Allanamiento",
      tres == "6" ~ "Incompetencia",
      tres == "7" ~ "Excepciones Previas",
      tres == "8" ~ "Otra",
      tres == "9" ~ "Rechazo “In Limine”",
      TRUE ~ "Sent.Definitiva")) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = c("iep" = "organismo")) %>% 
    group_by(circunscripcion, organismo_descripcion, tipo_resolucion) %>% 
    summarise(cantidad = n()) %>% 
    #filter(!tipo_resolucion %in% c( "Incompetencia", "Excepción")) %>%
    do(janitor::adorn_totals(.)) %>% 
    ungroup() 
  
  resultado
  
}


# Resoluciones Familia

resoluciones_fam <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) 
  
  resultado <- resolverconvertidos(resultado)
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }
  
  resultado <- resultado %>% 
    process() %>%
    .$result %>%
    ungroup()
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, res_luego2venc, sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego2venc", replacement="luego_2do_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    mutate(resol_vencidas = stringr::str_c(round((luego_1er_venc + luego_2do_venc)/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           "luego 2do Vencim" = luego_2do_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas")
  
  resultado
}


resoluciones_paz <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo)
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }
  
  resultado <- resultado %>% 
    process() %>%
    .$result %>%
    ungroup()
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, res_luego2venc, sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego2venc", replacement="luego_2do_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    mutate(resol_vencidas = stringr::str_c(round((luego_1er_venc + luego_2do_venc)/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           "luego 2do Vencim" = luego_2do_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas")
  
  resultado
}


resoluciones_paz_23 <- function(poblacion, start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(CADRA23) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo)
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }
  
  resultado <- resultado %>% 
    process(CADRA23) %>%
    .$result %>%
    ungroup()
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, res_luego2venc, sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego2venc", replacement="luego_2do_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    mutate(resol_vencidas = stringr::str_c(round((luego_1er_venc + luego_2do_venc)/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           "luego 2do Vencim" = luego_2do_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas")
  
  resultado
}


# Segunda Instancia

resoluciones_cco_2 <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo)
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }
  
  resultado <- resultado %>% 
    process() %>%
    .$result %>%
    ungroup()
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, res_luego2venc, sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego2venc", replacement="luego_2do_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    mutate(resol_vencidas = stringr::str_c(round((luego_1er_venc + luego_2do_venc)/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           "luego 2do Vencim" = luego_2do_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas")
  
  resultado
}



resoluciones_lab_2 <- function(poblacion, operacion = "CADR1L", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE) {
  operacion = rlang::enexpr(operacion)
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo)
  
  
  if(desagregacion_mensual) {
    resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
  } else {
    resultado <- resultado %>% group_by(iep) 
  }
  
  resultado <- resultado %>% 
    process() %>%
    .$result %>%
    ungroup()
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
  }
  
  resultado <- resultado %>% 
    select(organismo = iep, matches("mes"), causas_adespacho, causas_resueltas, 
           a_termino = res_atermino, res_luego1venc, sentencias = res_xsentencia, 
           autos = res_xauto, sin_clasif =  res_xindeter) %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "res_luego1venc", replacement="luego_1er_venc") %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = "organismo")
  
  if(desagregacion_mensual) {
    resultado <- resultado %>% 
      arrange(circunscripcion, organismo, mes) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.) %>%
           mutate(mes=ifelse(organismo_descripcion=="-", "Total", mes), circunscripcion=.$circunscripcion[1], organismo_descripcion=.$organismo_descripcion[1]) ) %>% 
      ungroup() %>% 
      select(circunscripcion, organismo, mes, everything()) %>% 
      mutate(mes = factor(mes, levels = c("enero", "febrero", "marzo", "abril", "mayo", 
                                          "junio", "julio", "agosto", "septiembre", 
                                          "octubre", "noviembre", "diciembre", "Total"),
                          ordered = TRUE))
  } else {
    resultado <- resultado %>% arrange(circunscripcion, organismo) 
  }
  resultado <- resultado %>% 
    select(circunscripcion, organismo, organismo_descripcion, everything()) %>% 
    filter(causas_resueltas > 0) %>% 
    mutate(circunscripcion = as.character(circunscripcion)) %>% 
    mutate(resol_vencidas = stringr::str_c(round(luego_1er_venc/causas_resueltas*100), ' %')) %>% 
    select("circunscripción" = circunscripcion,
           organismo=organismo_descripcion,
           matches("mes"),
           "causas a despacho" = causas_adespacho,
           "causas resueltas" = causas_resueltas,
           "a termino" = a_termino,
           "luego 1er Vencim" = luego_1er_venc,
           sentencias,
           autos,
           "sin clasif" = sin_clasif,
           "resol vencidas" = "resol_vencidas")
  
  resultado
}


