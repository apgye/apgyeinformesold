#consolidado_octubre
oralidad_consolidado <- read_csv("oralidad_consolidado.csv")
# Exclusiones
exclusiones2 <- tribble(  ~organismo, ~nro,
                         "jdocco0400pna", 23083, #correo7/12/18
                         "jdocco0400pna", 22849, 
                         "jdocco0300pna", 33627,# mal registrada 33707, 
                         # tiene fecha de realización 
                         #y está en cuarto intermedio
                         "jdocco0300pna", 34089,
                         "jdocco0300pna", 34177,
                         "jdocco0300pna", 33329, 
                         "jdocco0300pna", 34087, 
                         "jdocco0300pna", 34109,
                         "jdocco0300pna", 33043,
                         
                         "jdocco0100con", 12262, 
                         "jdocco0100con", 11780,
                         "jdocco0100con", 12204, 
                         "jdocco0100con", 12127,
                         "jdocco0100con", 11744) %>% 
  mutate(nro = as.character(nro))

oralidad_consolidado <- oralidad_consolidado  %>%
  select(-organismo) %>% 
  left_join(jdos_civiles[, c("circunscripcion", "organismo", "organismo_descripcion")], 
            by = c("circunscripcion", "organismo_descripcion")) %>% 
  anti_join(exclusiones, by = c("organismo", "nro")) %>% 
  select(-organismo_descripcion, -circunscripcion) %>% 
  select(iep = organismo, everything())


## Corregir errores de registracion

oralidad_consolidado <- oralidad_consolidado %>% 
  filter(estado_preliminar != "no_realizada" | estado_vcausa != "no_realizada") %>% 
  mutate(duplicados = duplicated(nro)) %>% 
  select(iep, nro, duplicados, everything()) %>% 
  arrange(iep, nro)

duplicados <- oralidad_consolidado$nro[oralidad_consolidado$duplicados]


# Filtrar las audiencias no realizadas
# Corregir duplicados 


# Trabajo sobre repetidos
# 12972
oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0000dia" &
                                                            nro == 12972 & 
                                                            duplicados == F))


# 8189 fecha
oralidad_consolidado$fecha_efect_vcausa[oralidad_consolidado$iep == "jdocco0200gay" &
                                          oralidad_consolidado$nro == 8189 &
                                          oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$fecha_efect_vcausa[oralidad_consolidado$iep == "jdocco0200gay" &
                                            oralidad_consolidado$nro == 8189 &
                                            oralidad_consolidado$duplicados == T] 

  
oralidad_consolidado$estado_vcausa[oralidad_consolidado$iep == "jdocco0200gay" &
                                          oralidad_consolidado$nro == 8189 &
                                          oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$estado_vcausa[oralidad_consolidado$iep == "jdocco0200gay" &
                                            oralidad_consolidado$nro == 8189 &
                                            oralidad_consolidado$duplicados == T] 

oralidad_consolidado$resultado_vcausa[oralidad_consolidado$iep == "jdocco0200gay" &
                                          oralidad_consolidado$nro == 8189 &
                                          oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$resultado_vcausa[oralidad_consolidado$iep == "jdocco0200gay" &
                                            oralidad_consolidado$nro == 8189 &
                                            oralidad_consolidado$duplicados == T] 


oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0200gay" &
                                                            nro == 8189 & 
                                                            duplicados == T))


# "8237" No duplicado pues están en distintos org

#"5746"  
oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0200lpa" &
                                                            nro == 5746 & 
                                                            duplicados == F))

#"2136" 
oralidad_consolidado$fecha_efect_vcausa[oralidad_consolidado$iep == "jdocco0200nog" &
                                          oralidad_consolidado$nro == 2136 &
                                          oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$fecha_efect_vcausa[oralidad_consolidado$iep == "jdocco0200nog" &
                                            oralidad_consolidado$nro == 2136 &
                                            oralidad_consolidado$duplicados == T] 


oralidad_consolidado$estado_vcausa[oralidad_consolidado$iep == "jdocco0200nog" &
                                     oralidad_consolidado$nro == 2136 &
                                     oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$estado_vcausa[oralidad_consolidado$iep == "jdocco0200nog" &
                                       oralidad_consolidado$nro == 2136 &
                                       oralidad_consolidado$duplicados == T] 

oralidad_consolidado$resultado_vcausa[oralidad_consolidado$iep == "jdocco0200nog" &
                                        oralidad_consolidado$nro == 2136 &
                                        oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$resultado_vcausa[oralidad_consolidado$iep == "jdocco0200nog" &
                                          oralidad_consolidado$nro == 2136 &
                                          oralidad_consolidado$duplicados == T] 


oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0200nog" &
                                                            nro == 2136 & 
                                                            duplicados == T))


#"7765"  
oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0200uru" &
                                                            nro == 7765 & 
                                                            duplicados == F))

oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0200uru" &
                                                            nro == 7765 & 
                                                            fecha_efect_preliminar == "2018-10-10"))


#"8614"  
oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0200uru" &
                                                            nro == 8614 & 
                                                            duplicados == F))

#"8724"  
oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0200uru" &
                                                            nro == 8724 & 
                                                            duplicados == F))

#"33231" 
oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0300pna" &
                                                            nro == 33231 & 
                                                            duplicados == F))

#"8280"
oralidad_consolidado$fecha_efect_vcausa[oralidad_consolidado$iep == "jdocco0600con" &
                                          oralidad_consolidado$nro == 8280 &
                                          oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$fecha_efect_vcausa[oralidad_consolidado$iep == "jdocco0600con" &
                                            oralidad_consolidado$nro == 8280 &
                                            oralidad_consolidado$duplicados == T] 


oralidad_consolidado$estado_vcausa[oralidad_consolidado$iep == "jdocco0600con" &
                                     oralidad_consolidado$nro == 8280 &
                                     oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$estado_vcausa[oralidad_consolidado$iep == "jdocco0600con" &
                                       oralidad_consolidado$nro == 8280 &
                                       oralidad_consolidado$duplicados == T] 

oralidad_consolidado$resultado_vcausa[oralidad_consolidado$iep == "jdocco0600con" &
                                        oralidad_consolidado$nro == 8280 &
                                        oralidad_consolidado$duplicados == F] <- 
  
oralidad_consolidado$resultado_vcausa[oralidad_consolidado$iep == "jdocco0600con" &
                                          oralidad_consolidado$nro == 8280 &
                                          oralidad_consolidado$duplicados == T] 


oralidad_consolidado <- oralidad_consolidado %>% filter(!(iep == "jdocco0600con" &
                                                            nro == 8280 & 
                                                            duplicados == T))


oralidad_consolidado <- oralidad_consolidado %>% 
  select(-duplicados)

write.table(oralidad_consolidado, 
                     "~/apgyeInformes/2018/Grupo Informes Oralidad Civil/diciembre/oralidad_consolidado.csv", 
                       row.names = F, col.names = T, sep = ",")
            
            