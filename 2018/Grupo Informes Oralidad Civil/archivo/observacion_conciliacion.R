
12/158*100

satisf_concil_ab <- ap_sat_ab_resumen %>% 
  filter(circunscripcion != "Total") %>% 
  group_by(`dimension evaluada`) %>% 
  summarise(`Muy Bien` = sum(`Muy Bien`, na.rm = T), 
            Bien = sum(Bien, na.rm = T), 
            Regular = sum(Regular, na.rm = T),
            Mal = sum(Mal, na.rm = T),
            `NS NC` = sum(`NS NC`, na.rm = T))

satisf_concil_us <- ap_sat_us_resumen %>% 
  filter(circunscripcion != "Total") %>% 
  group_by(`dimension evaluada`) %>% 
  summarise(`Muy Bien` = sum(`Muy Bien`, na.rm = T), 
            Bien = sum(Bien, na.rm = T), 
            Regular = sum(Regular, na.rm = T),
            Mal = sum(Mal, na.rm = T),
            `NS NC` = sum(`NS NC`, na.rm = T))
