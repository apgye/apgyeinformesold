---
title: "Oralidad Civil y Comercial - Entre Ríos"
author: "Área de Planificación Gestión y Estadística"
date: "2018-9-13"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Generalización de la Oralidad en Procesos de Conocimiento Civiles y Comerciales de la Provincia de Entre Ríos - Resumen Ejecutivo


```{r, echo=FALSE, warning=FALSE, message=FALSE}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)

# Activas métodos de acceso a primarias
entidades <- listar_organismos()
start_date <- lubridate::make_date(2018,7,1) 
end_date <- lubridate::make_date(2018,9,1) 

jdos_civiles <- entidades %>% 
  filter(str_detect(fuero, "Civil")) %>% 
  filter(tipo == "jdo", str_detect(materia, "cco" ))

audic <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                          "2018-07-01", "2018-09-02")$AUDIC$obtener()

organos_sin_informar <- jdos_civiles %>% 
  mutate(faltantes = organismo %in% names(table(audic$iep))) %>% 
  filter(faltantes == FALSE) %>% 
  select(circunscripcion, organismo_descripcion) %>% 
  arrange(circunscripcion)

# Audiencia Preliminar
preliminares_realiz_nor_cancel <- audic %>% 
  mutate(fea = as.Date(fea, format="%d/%m/%Y")) %>% 
  filter(fea > start_date, fea < end_date) %>% 
  mutate(ra = toupper(ra)) %>% 
  filter(esta %in% c("2", "3"), ta == "1")

preliminares_resumen <- preliminares_realiz_nor_cancel %>% 
  group_by(iep) %>% 
  summarise(realizadas = sum(esta == "2", na.rm = T),
            no_realizada = sum(esta %in% c("3"), na.rm = T),
            conciliacion = sum(ra == "T", na.rm = T), 
            sin_conciliacion = sum(ra %in% c("P", "S"), na.rm = T)) %>% 
  rowwise() %>% 
  mutate(tasa_realizacion = 
          str_c(round(realizadas/(no_realizada+realizadas)*100, 
                      digits= 1), "%"), 
         tasa_conciliacion =
          str_c(round(conciliacion/(conciliacion+sin_conciliacion)*100,
                      digits= 1), "%")) %>%
  mutate(tasa_realizacion = ifelse(tasa_realizacion == "NaN%",
                                   "0%", tasa_realizacion),
         tasa_conciliacion = ifelse(tasa_conciliacion == "NaN%", 
                                    "0%", tasa_conciliacion)) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                        "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  select(circunscripcion, organismo_descripcion, everything(), -iep) %>% 
  arrange(desc(circunscripcion)) %>% 
  janitor::adorn_totals("row")

colnames(preliminares_resumen) <- chartr("_", " ",
                                         colnames(preliminares_resumen))

# Encuestas de Satisfacción
ap_sat_ab <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                          "2018-08-01",
                                          "2018-09-02")$AP_SAT_AB$obtener()

ap_sat_ab_resumen <- ap_sat_ab %>% 
  select(iep, trato, depurac_prueba, intent_concil, instalaciones) %>% 
  tidyr::gather(key = "dimension_evaluada", value = "calificacion",
                -iep) %>% 
  mutate(calificacion = case_when(
    calificacion == "4" ~ "Muy_Bien", 
    calificacion == "3" ~ "Bien", 
    calificacion == "2" ~ "Regular", 
    calificacion == "1" ~ "Mal",
    TRUE ~ "NS_NC"
    )) %>% 
  mutate(nro = 1:length(iep), cantidad = 1, 
         calificacion = factor(calificacion, 
                               levels= c("Muy_Bien", "Bien",
                                         "Regular", "Mal", "NS_NC"))) %>% 
  tidyr::spread(calificacion, cantidad, fill = 0, drop = F) %>% 
  group_by(iep, dimension_evaluada) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE ) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                        "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, iep, dimension_evaluada, 
         Muy_Bien, Bien, Regular, Mal, NS_NC, -iep) %>% 
  arrange(circunscripcion)%>% 
  janitor::adorn_totals("row")

colnames(ap_sat_ab_resumen) <- chartr("_", " ",
                                         colnames(ap_sat_ab_resumen))

ap_sat_us <- apgyeRStudio::primariasOrganismo("jdocco*", 
                                          "2018-08-01",
                                          "2018-09-02")$AP_SAT_US$obtener() 


ap_sat_us_resumen <- ap_sat_us %>% 
  select(iep, trato, comprension = compresion,
         escucha, instalaciones) %>% 
  tidyr::gather(key = "dimension_evaluada", value = "calificacion",
                -iep) %>% 
  mutate(calificacion = case_when(
    calificacion == "4" ~ "Muy_Bien", 
    calificacion == "3" ~ "Bien", 
    calificacion == "2" ~ "Regular", 
    calificacion == "1" ~ "Mal",
    TRUE ~ "NS_NC"
    )) %>% 
  mutate(nro = 1:length(iep), cantidad = 1, 
         calificacion = factor(calificacion, 
                               levels= c("Muy_Bien", "Bien",
                                         "Regular", "Mal", "NS_NC"))) %>% 
  tidyr::spread(calificacion, cantidad, fill = 0, drop = F) %>% 
  group_by(iep, dimension_evaluada) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE ) %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                        "circunscripcion")], 
            by = c("iep" = "organismo")) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo_descripcion, dimension_evaluada, 
         Muy_Bien, Bien, Regular, Mal, NS_NC, -iep) %>% 
  arrange(circunscripcion)%>% 
  janitor::adorn_totals("row")

colnames(ap_sat_us_resumen) <- chartr("_", " ",
                                         colnames(ap_sat_us_resumen))





```


# Meses Informados: desde `r months(start_date)` hasta agosto (inclusive)

# Aclaraciones Metodológicas


La información procesada para este informe surge de listados primarios de datos presentados por cada organismo de manera digital.

Las categorías de audiencia contempladas en el sistema estadístico del STJER son:

+ Audiencia Realizada: audiencia efectivamente celebrada;  
+ Audiencia No Realizada: audiencias que no se realiza por incomparencia de parte;  
+ Audiencia Cancelada: audiencia que estaba fijada y se dejó sin efecto a pedido de parte; y    
+ Audiencia Reprogramada: audiencia fijada que se suspende, fijándose nuevo día y hora para su realización.  

Para este informe se cuentan las audiencias clasificadas como *Audiencias Realizadas* y *Audiencias No Realizadas*.

Para los casos de Audiencia en procesos con contradicción se clasifican sus resultados de la siguiente manera:

+ Conciliación Total     
+ Conciliación Parcial    
+ Sin Conciliación    

A los fines del procesamiento de datos, y según la definición vigente para el Ministerio de Justicia y DDHH de la Nación, la categoría de *Conciliación Parcial* queda subsumida bajo la categoría *Sin Conciliación* atendiendo a la contiuación del proceso.   

Finalmente, se presentan indicadores de Satisfaccion de Usuarios y Abogados en Audiencia, según las variables y procedimientos fijados por el documento remitido por el Ministerio en fecha 16/07/18 denominado **Encuestas de satisfacción – Instructivo y formularios – Marzo 2018**.  

# Resultados

A continuación se presentan los resultados de la experiencia de Oralidad (iniciada en `r months(start_date)`, a través de indicadores que dan cuenta de las audiencias realizadas, la tasa de conciliación y la satisfacción de usuarios y abogados.

# Audiencias Realizadas y Conciliadas

En la siguiente tabla se muestran las Audiencias Preliminares informadas por cada organismo en el período considerado. Se calcula la *tasa de realización* como el cociente entre las audiencias realizadas y el total de realizadas y no realizadas, y la *tasa de conciliación* como cociente entre audiencias conciliadas y el total de conciliadas y sin conciliación.

```{r}
kable(preliminares_resumen, caption = "Audiencias Preliminares y Tasas", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape() 
```

# Organos sin Audiencias Preliminares Informadas en el Período


```{r}
kable(organos_sin_informar, caption = 
        "Órganos sin Audiencia Preliminar en el período", longtable = TRUE) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) 
```


# Encuestas de Satisfacción (implementadas a partir del 01/08/18)

La escala de medición de "Satisfacción" para todas las variables consideradas (tanto en la Audiencia Preliminar como la de Vista de Causa) es ordinal y se conforma de la siguiente manera:

4 = Muy Bien,  
3 = Bien,   
2 = Regular, y   
1 = Mal

## Encuestas de Satisfacción sobre Audiencia Preliminar

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*depurac_prueba* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en la depuración de la prueba en este proceso?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
kable(ap_sat_ab_resumen, caption = "Satisfaccion de Abogados según Dimensión Evaluada",
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()


```

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia?  

```{r}
kable(ap_sat_us_resumen, caption = "Satisfaccion de Usuarios según Dimensión Evaluada",
       align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F) %>% 
  landscape()

```


Lic. Sebastián Castillo
Área de Planificación Gestión y Estadística    
`r Sys.Date()`








