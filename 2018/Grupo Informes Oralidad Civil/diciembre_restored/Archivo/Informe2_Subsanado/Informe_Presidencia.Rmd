---
title: "Informe General sobre Oralidad"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE)
options(knitr.kable.NA = '')
```

```{r}
source("informe.R")
source("utils.R")
outputTable <- function (table, caption, row_group_label_position = "identity") {
  if(row_group_label_position == "identity") {
    total_rows <-  c(which(table[, 1] == "Total"), which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  } else {
    total_rows <-  c(which(table[, 2] == "Total"), which(table[, 3] == "Total"))
  }
  table %>% rename_all(.funs = stringr::str_replace_all, pattern = "_", replacement=" ") %>% 
    kable("latex", caption = caption,
          align = 'c', longtable = TRUE, booktabs = T ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), latex_options = c("repeat_header"),
                  full_width = F, font_size = 10) %>% 
    row_spec(total_rows, bold = T) %>% 
    collapse_rows(columns = 1:2, row_group_label_position = row_group_label_position)
  
}

```

```{r}
library(dplyr)
library(knitr)
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)
library(stringr)
library(janitor)
library(stringr)
library(kableExtra)
library(tibble)
```

# Meses Informados: `r getDataIntervalStr(start_date, end_date)`

```{r}

oralidad_resumen_xcirc <- oralidad_resumen %>% 
  group_by(circunscripcion) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE) %>% 
  arrange(desc(cantidad_procesos_en_oralidad_informados)) %>% 
  janitor::adorn_totals("row")

kable(oralidad_resumen_xcirc, caption = "Resumen", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90)
```

**ATENCION**: En ANEXO 1 se agrega la tabla completa empleada para el resumen precedente. 
\pagebreak

# Detalle específico sobre Audiencias de Vista de Causa Programadas

```{r}

oralidad_vista_causa <- oralidad_consolidado_parcial %>% 
  select(organismo, nro_expte, fprogramada_vista_causa) %>% 
  filter(!is.na(fprogramada_vista_causa) & fprogramada_vista_causa >= end_date) %>% 
  mutate(mes = months(fprogramada_vista_causa), 
         año = year(fprogramada_vista_causa)) %>%
  mutate(mes = factor(mes, levels = c("febrero", "marzo", "abril", "mayo",
                                      "junio", "julio", "agosto", "septiembre",
                                      "octubre", "noviembre", "diciembre"), 
                      ordered = TRUE)) %>%
  group_by(organismo, año, mes) %>% 
  summarise(cantidad_audiencias = n(), 
            nros_expte_y_fechaAudiencia = paste(nro_expte, fprogramada_vista_causa,
                                sep = ": ", collapse = "; ")) %>% 
  ungroup() %>% 
  left_join(entidades[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], by = c("organismo")) %>% 
  select(circunscripcion, organismo_descripcion, organismo, everything()) %>%
  select(-organismo) %>% 
  arrange(circunscripcion, organismo_descripcion)  
  
outputTable(oralidad_vista_causa, 
            caption = "Vista de Causa Programadas por organismo") %>%
  column_spec(6, "5cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

# kable(oralidad_vista_causa, caption = "Vista de Causa Programadas por organismo",
#       align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                 full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
#   column_spec(5, "8cm") %>%
#   row_spec(0, angle = 90) %>% 
#   landscape() 

```

\pagebreak


# Encuestas de Satisfacción (implementadas a partir del 01/08/18)

La escala de medición de "Satisfacción" para todas las variables consideradas (tanto en la Audiencia Preliminar como la de Vista de Causa) es ordinal y se conforma de la siguiente manera:

4 = Muy Bien,  
3 = Bien,   
2 = Regular, y   
1 = Mal

## Encuestas de Satisfacción sobre Audiencia Preliminar

### Opinión de Abogados

Las dimensiones evaluadas, que se identifican con un código y se formularon mediante preguntas de opinión, son:

*trato* = ¿Cómo lo trataron durante la audiencia?  
*depurac_prueba* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en la depuración de la prueba en este proceso?  
*intent_concil* = ¿Cuál es su grado de satisfacción respecto de la actividad del Magistrado en los intentos conciliatorios en este proceso?  
*instalaciones* = ¿Considera convenientes las instalaciones para la realización de la audiencia?


```{r}
ap_sat_ab_resumen_xcirc_total <- ap_sat_ab_resumen %>% 
  group_by(circunscripcion) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE) %>% 
  arrange(desc(`Muy Bien`))

kable(ap_sat_ab_resumen_xcirc_total, caption = "Encuesta de Satisfaccion de Abogados", 
      align = 'c', longtable = TRUE ) %>% 
  row_spec(1, bold = T, background = "gray") %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  

```

\pagebreak

```{r}
ap_sat_ab_resumen_xcirc <- ap_sat_ab_resumen %>% 
  filter(circunscripcion != "Total") %>% 
  group_by(circunscripcion, `dimension evaluada`) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE) %>% 
  do(janitor::adorn_totals(.)) %>%
  ungroup() 

outputTable(ap_sat_ab_resumen_xcirc, 
            caption = "Resultados de las Encuestas de Satisfaccion de Abogados por Circunscripcion") 

```

\pagebreak

### Opinión de Usuarios

*trato* = ¿Cómo lo trataron durante la audiencia?  
*comprension* = ¿Comprendió lo que le explicaron durante la audiencia?  
*escucha* = ¿Cuál es su grado de satisfacción respecto de haber sido escuchado por el Magistrado?  
*instalaciones* ¿Considera convenientes las instalaciones para la realización de la audiencia? 

```{r}
ap_sat_us_resumen_xcirc_total <- ap_sat_us_resumen %>% 
  group_by(circunscripcion) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE) %>% 
  arrange(desc(`Muy Bien`))

kable(ap_sat_us_resumen_xcirc_total, caption = "Encuesta de Satisfaccion de Usuarios", 
      align = 'c', longtable = TRUE ) %>% 
  row_spec(1, bold = T, background = "gray") %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  

```

\pagebreak

```{r}
ap_sat_us_resumen_xcirc <- ap_sat_us_resumen %>% 
  filter(circunscripcion != "Total") %>% 
  group_by(circunscripcion, `dimension evaluada`) %>% 
  summarise_if(is.numeric, sum, na.rm=TRUE) %>% 
  do(janitor::adorn_totals(.)) %>%
  ungroup() 

outputTable(ap_sat_us_resumen_xcirc, 
            caption = "Resultados de las Encuestas de Satisfaccion de Usuarios por Circunscripcion ") 

```


Lic. Sebastián Castillo
Área de Planificación Gestión y Estadística    
`r Sys.Date()`


\pagebreak

# ANEXO 1

## Datos particulares del conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

```{r}
kable(oralidad_consolidado_parcial, caption = "Tabla General", 
      align = 'c', longtable = TRUE ) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), 
                full_width = F, font_size = 10, latex_options = "repeat_header")  %>%
  row_spec(0, angle = 90) %>%
  landscape() 
```

\pagebreak


# Anexo 3: Aclaraciones Metodológicas

La información procesada para este informe surge de listados primarios de datos presentados por cada organismo de manera digital.

Las categorías de audiencia contempladas en el sistema estadístico del STJER son:

+ Audiencia Realizada: audiencia efectivamente celebrada;  
+ Audiencia No Realizada: audiencias que no se realiza por incomparencia de parte;  

Para los casos de Audiencia en procesos con contradicción se clasifican sus resultados de la siguiente manera:

+ Conciliación Total     
+ Conciliación Parcial    
+ Sin Conciliación    

A los fines del procesamiento de datos, y según la definición vigente para el Ministerio de Justicia y DDHH de la Nación, la categoría de *Conciliación Parcial* queda subsumida bajo la categoría *Sin Conciliación* atendiendo a la contiuación del proceso.   

Finalmente, se presentan indicadores de Satisfaccion de Usuarios y Abogados en Audiencia, según las variables y procedimientos fijados por el documento remitido por el Ministerio en fecha 16/07/18 denominado **Encuestas de satisfacción – Instructivo y formularios – Marzo 2018**. 


## Datos globales del conjunto de procesos sometidos al Protocolo de Oralidad e informados al Área.

En esta tabla se presenta información sobre el conjunto de procesos sometidos al Protocolo de Oralidad e informados al área en los datos presentados de julio a la fecha.

En tal sentido el informe *NO INCLUYE*:

+ causas iniciadas no identificadas como sometidas al Protocolo de Procesos por Audiencia, y 
+ causas donde no se haya realizada audiencia preliminar.


En el computo de cada indicador se empleó el siguiente procedimiento:

+ **cantidad procesos en oralidad informados**: conjunto de casos comprendidos en el Protocolo de Procesos por Audiencias informados, que se obtiene mediante la suma de: 1) los procesos con audiencia preliminar celebrada a partir de julio, y 2) los casos iniciados a partir de agosto indentificados como proceso por audiencia según registro específico.   
+ **cantidad_preliminares_realizadas**: conjunto de casos con audiencia preliminar realizada en el período considerado. Se excluyen del conteo los casos duplicados.   
+ **cantidad_vista_causa_programadas**: conjunto de casos con audiencia de vista de causa programada e informada al momento de celebración de la preliminar. Se excluyen del conteo los casos duplicados.   
+ **cantidad_vista_causa_realizadas**: conjunto de casos con audiencia de vista de causa celebrada. Se excluyen del conteo los casos duplicados y los casos con multiples registros de audiencia para un mismo mes.   
+ **cantidad_sentencias**: conjunto de casos con sentencia dictada en procesos sometidos al Protocolo de Oralidad que se informaron al área.   




