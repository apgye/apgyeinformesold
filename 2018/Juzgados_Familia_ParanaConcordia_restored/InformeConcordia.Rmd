---
title: "Documento en Proceso"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r echo=FALSE, include=FALSE}
# Parámetros del informe
desagregacion_mensual = T
start_date <- "2018-02-01" 
end_date <- "2018-12-01" 
circ <- c("Concordia")
#circ <- c("Chajarí", "Federación", "Islas del Ibicuy", "San Salvador")
```


```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam")) 

if(is.na(circ)) {
  poblacion_total <- poblacion_total
  } else {
    poblacion_total <- poblacion_total %>% 
      filter(circunscripcion %in% circ)
  }

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria))


```


## Juzgados de Familia


En este documento *en proceso* se explora la distribución de causas en trámmite por organismo con competencia en Familia en Concordia considerando en particular los procesos de Violencia Familiar y Violencia de Género. 

Se observan valores muy heterogéneos en las cantidades de causas en trámite emergentes de los listados presentados de movimientos mensuales por organismo. En la consideración de dichos listados se tuvo en cuenta la conversión de la *antigua secretaría 2* en juzgado 3. 

A pedido del Dr. Darío Andrioli y a los fines de dialogar sobre estos resultado se emite el siguiente pre-informe. 

\pagebreak

#### Causas en trámite

En esta tabla se muestran las causas en trámite totales y las causas en trámite activas que son aquellas que presentaron al menos un movimiento procesal en los últimos dos años (contados desde agosto 2018, mes de la presentación del informe de Causas en Trámite).


```{r}
entramite(poblacion = jdos_fam) %>% resolverconvertidos() %>% enTramite_gral() %>% 
  arrange(circunscripcion, organismo) %>% 
  outputTable(caption = "Causas en Trámite Totales y Causas en Trámite últimos 2 años (Activas)",
              row_group_label_position = "identity")

```

#### Causas en trámite en los últimos dos años: Violencias vs. demás Procesos

De las causas en trámite en los últimos dos años, se pueden analizar el total de causas que corresponden a los procesos de Violencia Familiar o de Género y el total de los demás tipos de proceso que tiene el organismo. 


```{r}
entramite(poblacion = jdos_fam) %>% 
  resolverconvertidos() %>% 
  filter(str_detect(ultimo_movimiento, "menor")) %>% 
  mutate(grupos = ifelse(str_detect(tproc, "VIOLENCIA"), "violencias", "otros_procesos")) %>% 
  group_by(iep, grupos) %>% 
  summarise(cantidad_en_tramite = n()) %>% 
  tidyr::spread(grupos, cantidad_en_tramite) %>% 
  left_join(jdos_fam %>% 
                select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep", copy=TRUE) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo, everything(), -iep) %>% 
  arrange(circunscripcion, organismo) %>% 
  outputTable(caption = "Causas en Trámite en los últimos dos años: Violencias vs. demás Procesos",
               row_group_label_position = "identity")

```


<!-- ### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_fam( -->
<!--   poblacion = jdos_fam, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   iniciados_fam_comparativo() %>%  -->
<!--   kable(caption = "Causas Iniciadas Agrupadas", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(2, "4cm") %>% -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape() -->
<!-- ``` -->


<!-- ### Causas Iniciadas: Grafico `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r, fig.width=12, fig.height=4, echo=FALSE} -->

<!-- iniciados_fam(poblacion = jdos_fam, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   iniciados_fam_xg() %>% -->
<!--   inic_fam_graf_xg() -->

<!-- ``` -->

\pagebreak


### Movimientos en procesos de Violencia `r getDataIntervalStr(start_date, end_date)`

En esta tabla se muestran las Causas de Violencia Familiar o de Género con Movimiento Mensual y el Promedio de Movimiento por Causa.    

Para este indicador se extrajo información del listado de movimientos mensuales que presenta cada organismo, procesándose la columna de movimientos totales de cada registro del sistema considerando la fecha del movimiento.

El Juzgado 3 no presenta causas de Violencia con movimiento en los meses febrero a agosto, razón por la cual no aparecen informados esos meses. 

\pagebreak

```{r}
camov <- apgyeRStudio::primariasOrganismo("jdofam*", start_date, end_date)$CAMOV$tabla() %>% 
  filter(iep %in% jdos_fam$organismo) %>%
  filter(grepl("VIOLENCIA", tproc)) %>% 
  mutate(fmov = dmy(fmov)) %>%
  filter(fmov >= data_interval_start, fmov < data_interval_end) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% 
  collect() %>% 
  ungroup() %>% 
  tidyr::separate_rows(movt, sep = "%") %>% 
  tidyr::separate(movt, c("fecha_mov", "desc"), sep = "\\$") %>% 
  mutate(fecha_mov = lubridate::dmy(fecha_mov)) %>% 
  filter(fecha_mov >= data_interval_start, fecha_mov < data_interval_end) %>% 
  mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
  resolverconvertidos() %>%
  group_by(iep, mes, nro) %>% 
  summarise(cantidad = n()) %>% 
  ungroup() %>% 
  group_by(iep, mes) %>% 
  summarise(cantidad_causas_conmov = n(), 
            promedio_mov_xcausa = round(mean(cantidad, na.rm = T), digits = 1)) %>% 
  left_join(jdos_fam %>% 
                select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep", copy=TRUE) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo, everything(), -iep, -circunscripcion) %>% 
  arrange(organismo, mes) 

camov %>% 
  outputTable(caption = "Causas de Violencia con Movimiento: detalle mensual por organismo",
               row_group_label_position = "identity")

```

Lic. Sebastián Castillo


