
audienciasCiviles <- function(poblacion, start_date = "2018-07-01", end_date="2018-09-02", desagregacion_mensual = TRUE) {
  resultado <- DB_PROD() %>% 
    apgyeTableData("AUDIC") %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% poblacion$organismo) %>% 
    mutate(fea = dmy(fea)) %>% 
    filter(fea > !!start_date, fea < !!end_date) %>% 
    mutate(ra = toupper(ra)) %>% 
    filter(esta %in% c("2", "3")) %>% # no canceladas
    collect() %>% 
    mutate(ta = case_when(ta == 1 ~ "preliminar", ta == 2 ~ "prueba", ta == 3 ~ "conciliación", ta == 4 ~ "otras")) %>% 
    mutate(tipo = factor(ta, levels = c("preliminar", "prueba", "conciliación", "otras"), ordered = TRUE)) %>%
    mutate(mes = lubridate::month(fea, label=T, abbr = F))
  if(desagregacion_mensual) {
    resultado <- resultado %>% group_by(iep, mes, tipo)
  } else {
    resultado <- resultado %>% group_by(iep, tipo)
  }
  resultado <- resultado %>% summarise(realizadas = sum(esta == "2", na.rm = T),
                                       no_realizada = sum(esta %in% c("3"), na.rm = T),
                                       conciliacion = sum(ra == "T", na.rm = T), 
                                       sin_conciliacion = sum(esta == "2" & ra %in% c("P", "S", NA), na.rm = T)) %>% 
    ungroup() %>% 
    mutate(porc_realizacion = 
             stringr::str_c(round(realizadas/(no_realizada+realizadas)*100, 
                                  digits= 1), "%"), 
           porc_conciliacion =
             stringr::str_c(round(conciliacion/(conciliacion+sin_conciliacion)*100,
                                  digits= 1), "%")) %>%
    mutate(porc_realizacion = ifelse(porc_realizacion == "NaN%",
                                     "0%", porc_realizacion),
           porc_conciliacion = ifelse(porc_conciliacion == "NaN%", 
                                      "0%", porc_conciliacion)) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], 
              by = c("iep" = "organismo")) %>% 
    select(circunscripcion, organismo=organismo_descripcion, everything(), -iep) %>% 
    arrange(desc(circunscripcion)) %>% 
    janitor::adorn_totals("row") %>% 
    rename_all(.funs = stringr::str_replace_all, pattern = "_", replacement=" ")
  
  resultado
}