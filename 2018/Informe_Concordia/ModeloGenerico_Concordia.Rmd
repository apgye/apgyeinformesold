---
title: "Informe"
subtitle: 
author: "Área Planificación, Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("informe.R")
source("utils.R")
```

```{r echo=FALSE, include=FALSE}
# Parámetros del informe
desagregacion_mensual = T
start_date <- "2018-02-01" 
end_date <- "2018-11-01" 
```


```{r echo=FALSE, include=FALSE}
# Poblacion
poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam")) %>% 
  filter(circunscripcion == "Concordia") %>% filter(fuero != "Penal")

jdos_cco <- poblacion_total %>% 
  filter(grepl("cco", organismo)) %>% 
  filter(str_detect(materia, "cco"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_fam <- poblacion_total %>% 
  filter(grepl("fam", organismo)) %>% 
  filter(str_detect(materia, "fam"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_paz <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria == "1" | is.na(categoria))

jdos_lab <- poblacion_total %>% 
  filter(str_detect(organismo, "lab"), tipo != "cam")

cam_civil <- poblacion_total %>% 
  filter(str_detect(materia, "cco"), tipo == "cam")

cam_lab <- poblacion_total %>% 
  filter(str_detect(materia, "lab"), tipo == "cam")
```

\pagebreak

## Juzgados Civil y Comercial

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados_todos(
  poblacion = jdos_cco,
  start_date = start_date,
  end_date = end_date) %>%
  kable(caption = "Causas Iniciadas",
      align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(3, "8cm") %>%
  row_spec(0) %>%
  landscape()
```


### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_cco <- resoluciones_cco(
  poblacion = jdos_cco,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) 
  
resoluciones_cco %>% 
  filter(!organismo == "-") %>% 
  outputTable(caption = "Resoluciones y Vencimientos") %>%
  row_spec(0, angle = 90) %>%
  landscape()

```

### Sentencias por Tipo `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_cco_xtres(
  poblacion = jdos_cco,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Sentencias por Tipo") %>%
  row_spec(0, angle = 90) 
```

<!-- ### Audiencias Realizadas  -->

<!-- En la siguiente tabla se muestran las audiencias realizadas e informadas por cada organismo en el período considerado. -->

<!-- ```{r} -->
<!-- audienciasCiviles( -->
<!--   poblacion = jdos_cco, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Audiencias Realizadas") %>%  -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape() -->
<!-- ``` -->

\pagebreak

## Juzgados de Familia

<!-- ### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- iniciados_todos( -->
<!--   poblacion = jdos_fam, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date) %>% -->
<!--   kable(caption = "Causas Iniciadas", -->
<!--       align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>% -->
<!--   column_spec(3, "12cm") %>% -->
<!--   row_spec(0) %>% -->
<!--   landscape() -->
<!-- ``` -->


### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
jdo_fam <- apgyeRStudio::primariasOrganismo("jdofam*con", start_date, end_date)$CADR1C$tabla() %>%
  group_by(iep, data_interval_start, data_interval_end) %>%
  process() %>% .$result %>%
  ungroup() %>% 
  mutate(iep = case_when(    iep == "jdofam0101con" ~ "jdofam0100con",
    iep == "jdofam0102con" ~ "jdofam0300con",
    iep == "jdofam0201con" ~ "jdofam0200con",
    iep == "jdofam0202con" ~ "jdofam0300con", 
    iep == "jdofam0100con" ~ "jdofam0100con",
    iep == "jdofam0200con" ~ "jdofam0200con",
    iep == "jdofam0300con" ~ "jdofam0300con")) %>% 
  mutate(mes = months(data_interval_start)) %>% 
  mutate(mes = factor(mes, levels = c("febrero", "marzo", "abril", "mayo", 
                                      "junio", "julio", "agosto", "septiembre", 
                                      "octubre", "noviembre", "diciembre"),
                      ordered = TRUE)) %>% 
  group_by(iep, mes) %>% 
  summarise_if(is.numeric, sum, na.rm = T) %>% 
  do(janitor::adorn_totals(.)) %>% 
  ungroup() %>% 
  select(organismo = iep, mes, causas_adespacho, causas_resueltas, 
         a_termino = res_atermino, luego_1erVencim = res_luego1venc, 
         luego_2doVencim = res_luego2venc, sentencias = res_xsentencia, 
         autos = res_xauto, sin_clasif =  res_xindeter) %>% 
  rowwise() %>% 
  mutate(resol_vencidas = str_c(round(sum(luego_1erVencim, luego_2doVencim, 
                                          na.rm = T)/causas_resueltas*100, 
                                      digits = 1), "%")) %>% 
  left_join(jdos_fam[, c("organismo", "organismo_descripcion", 
                          "circunscripcion")], 
            by = "organismo") %>% 
  mutate(mes = ifelse(mes == "-", "Total", mes)) %>% 
  select(circunscripcion, organismo_descripcion, everything(), -organismo) 

outputTable(jdo_fam, caption = "Causas Resueltas") %>% 
  row_spec(0, angle = 90) %>%
  landscape()
  
```

<!-- \pagebreak -->

<!-- ### Audiencias por tipo - `r getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r} -->
<!-- aud_xtipo <- audifam() %>% audifam_resumen_xjdo()   -->
<!-- aud_xtipo %>%  -->
<!--   filter(organismo %in% jdos_fam$organismo_descripcion) %>%  -->
<!--   outputTable(caption = "Audiencias por tipo") %>% -->
<!--   landscape() -->
<!-- ``` -->

\newpage

## Juzgados Paz 1 Categoría

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciados_todos(
  poblacion = jdos_paz,
  start_date = start_date,
  end_date = end_date) %>%
  kable(caption = "Causas Iniciadas",
      align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(3, "12cm") %>%
  row_spec(0) %>%
  landscape()
```

### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_paz <- resoluciones_paz(
  poblacion = jdos_paz,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) 

resoluciones_paz %>% 
  filter(!organismo == "-") %>% 
  outputTable(caption = "Resoluciones y Vencimientos") %>%
  row_spec(0, angle = 90)  %>%
  landscape()
```

\newpage

## Juzgados Laborales

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}

iniciados_todos(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date) %>%
  kable(caption = "Causas Iniciadas",
      align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  column_spec(3, "12cm") %>%
  row_spec(0) %>%
  landscape()
```

### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_lab <- resoluciones_lab(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual)

resoluciones_lab %>% 
  filter(!organismo == "-") %>% 
  outputTable(caption = "Resoluciones y Vencimientos") %>%
  row_spec(0, angle = 90) %>%
  landscape()

```

### Sentencias por Tipo `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_lab_xtres(
  poblacion = jdos_lab,
  start_date = start_date,
  end_date = end_date,
  desagregacion_mensual = desagregacion_mensual) %>%
  outputTable(caption = "Sentencias por Tipo") %>%
  row_spec(0, angle = 90)
```


<!-- ### Audiencias Realizadas -->

<!-- En la siguiente tabla se muestran las audiencias realizadas e informadas por cada organismo en el período considerado. -->

<!-- ```{r} -->
<!-- audienciasLaborales( -->
<!--   poblacion = jdos_lab, -->
<!--   start_date = start_date, -->
<!--   end_date = end_date, -->
<!--   desagregacion_mensual = desagregacion_mensual) %>% -->
<!--   outputTable(caption = "Audiencias Realizadas") %>% -->
<!--   row_spec(0, angle = 90) %>% -->
<!--   landscape() -->
<!-- ``` -->


\newpage

## Segunda Instancia Civil y Comercial

### Causas Iniciadas - 1er semestre 2018

```{r}
iniciadosCITIPJ_1er_semestre(
  poblacion = cam_civil) %>%
      outputTable(caption = "Causas Iniciadas 1er semestre 2018") %>%
      column_spec(2, width = "2cm") %>%
      column_spec(4, width = "5cm") %>%
      row_spec(0, angle = 90)
```

\pagebreak

### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}
resoluciones_cco_2 <- resoluciones_cco_2(
  poblacion = cam_civil,
  start_date = start_date,
  end_date = end_date,
  operacion = "CADR2C",
  desagregacion_mensual = desagregacion_mensual)

resoluciones_cco_2 %>%
  filter(!organismo == "-") %>%
  outputTable(caption = "Resoluciones y Vencimientos") %>%
  row_spec(0, angle = 90)
```

\newpage

## Segunda Instancia Laboral

### Causas Iniciadas - `r getDataIntervalStr(start_date, end_date)`

```{r}
iniciadosCITIPJ_1er_semestre(
  poblacion = cam_lab) %>%
      outputTable(caption = "Causas Iniciadas 1er semestre 2018") %>%
      column_spec(2, width = "2cm") %>%
      column_spec(4, width = "5cm") %>%
      row_spec(0, angle = 90)
```


\pagebreak

### Causas Resueltas - `r getDataIntervalStr(start_date, end_date)`

```{r}

resoluciones_lab_2 <- resoluciones_lab_2(
  poblacion = cam_lab,
  start_date = start_date,
  end_date = end_date,
  operacion = "CADR2L",
  desagregacion_mensual = desagregacion_mensual)

resoluciones_lab_2 %>%
  filter(!organismo == "-") %>%
  outputTable(caption = "Resoluciones y Vencimientos") %>%
  row_spec(0, angle = 90)
```


<!-- \pagebreak -->

<!-- # Anexo Metodológico -->

<!-- La información procesada para este informe surge de listados primarios de datos presentados por cada organismo de manera digital. Dichos listados primarios permanecen inalterados para consulta a tavés del módulo *Presentaciones*. -->



