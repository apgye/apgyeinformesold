library(readr)
library(dplyr)

entidad_operacion <- read_csv("../apgyeJusEROrganization/inst/apgye/entidad_operacion.csv")
entidades <- read_csv("../apgyeJusEROrganization/inst/apgye/entidades.csv")

# Para Estela - instalacion de listado cetal_xl (feb 2019)
Org_a_Aplicar <- entidad_operacion %>%
  filter(grepl("camcco|camlab|salcco|sallab", organismo)) %>%
  .$organismo %>% unique()

entid <- entidades %>%
  filter(organismo %in% Org_a_Aplicar) %>%
  select(1:2,4:5) %>% View()

# --- Para Circulares 8 Feb 2019 ---
Org_excluir <- c("aghstj0000pna")

# 20190208: Comunicación General.
entidades_gral <- entidades %>%
  filter(!is.na(email_oficial)) %>%
  filter(!grepl("Civil", fuero)) %>%
  filter(!organismo %in% Org_excluir)

# 20190208: Comunicación Civiles.
entidades_civiles <- entidades %>%
  filter(!is.na(email_oficial)) %>%
  filter(grepl("Civil", fuero)) %>%
  filter(grepl("cco", materia)) %>%
  filter(grepl("jdo|cam|sal", tipo)) %>%
  filter(!organismo %in% Org_excluir)

write.table(entidades_gral, "20190208_entidades_gral.csv", sep = ",", row.names = F, col.names = T)
write.table(entidades_civiles, "20190208_entidades_civiles.csv", sep = ",", row.names = F, col.names = T)

write.table(entidades, "20190212_entidades.csv", sep = ",", row.names = F, col.names = T, fileEncoding = "ISO-8859-1")


# 20190328 - Comunicación: "STJER-APGE - Recordatorio sobre registración en materia Violencia."
# Enviado: jueves 28/03/2019 10:05

# Buscar materia FAM, incluyendo Civ con Fam
organismos_inactivos <- c("jdofam0001uru", "jdofam0002uru",
                          "jdofam0101con", "jdofam0102con",
                          "jdofam0201con", "jdofam0202con",
                          "jdofam0001gch", "jdofam0002gch")

entidades <- entidades %>%
  filter(!organismo %in% organismos_inactivos) %>% 
  filter(tipo == "jdo") %>% 
  filter(grepl("fam", materia))

View(entidades)
entidades[16,3] <- "jdofliamens2-uru@jusentrerios.gov.ar"

View(entidades$email_oficial)

