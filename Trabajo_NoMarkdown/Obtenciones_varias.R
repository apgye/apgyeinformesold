library(dplyr)
library(apgyeOperationsJusER)
library(apgyeDataAccess)
library(apgyeDSL)

result <- DB_PROD() %>% 
  apgyeTableData(CADRC) %>%
  apgyeDSL::interval("2017-10-01", "2018-01-01") %>% 
  filter(iep == "campen0000pna") %>% 
  collect()
  
process()


View(result)  
# resolpt
# mes 9:  8
# mes 10: 10
# mes 11: 10
# mes 12: 10

# resolpa
# mes 9:  3
# mes 10: 4
# mes 11: 0
# mes 12: 12

# CADRC
# mes 10: 47
# mes 11: 61
# mes 12: 28


